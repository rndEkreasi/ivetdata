$(document).ready(function(){

	var base_url = window.location.origin;
	var api_url = 'https://api-3A0A0751-6F1C-43F0-934B-6DEDBDD61009.sendbird.com/v3';
	var api_token = "03ae920eca433f3dd7636eebf658478648144ad1";
	var appId = '3A0A0751-6F1C-43F0-934B-6DEDBDD61009';
	var uniqId = Math.round(new Date().getTime() + (Math.random() * 100));
	var idCS = '1870';

	setInterval(function() {
		if ($('#contact-cs').length > 0) {
			var channel_url = $('#contact-cs').data('url');
			var user_id = $('input[name=user_id]').val();
			if (channel_url != '') {
				var settings = {
				  "url": base_url+"/Sendbird/getUnreadmsg",
				  "method": "POST",
			  	"data": {
				  	"channel_url":channel_url,
				  	"user_id":user_id
					},
				};
				$.ajax(settings).done(function (response) {
				  if (response != '0') {
				  	$('.unread_cs').text(response);
				  	$('.sendbird_notifCS').fadeIn();
				  }
				});
			}
		}

		if ($('#contact-cs-login').length > 0) {
			var channel_url = $('#contact-cs-login').data('url');
			var email = $('#contact-cs-login').data('email');
			if (channel_url != '' && email != '') {
				var settings = {
				  "url": base_url+"/Sendbird/getUnreadmsg",
				  "method": "POST",
			  	"data": {
				  	"channel_url":channel_url,
				  	"user_id":email
					},
				};
				$.ajax(settings).done(function (response) {
				  if (response != '0') {
				  	$('.unread_cs').text(response);
				  	$('.sendbird_notifCS').fadeIn();
				  }
				});
			}
		}

	}, 20*1000);

	$('#contact-cs').click(function(){
  	$('.sendbird_notifCS').fadeOut();
		var channel_url_cs = $(this).data('url');
		if (channel_url_cs != '') {
			sbWidget.showChannel(channel_url_cs);
		} else {
			$('.fix_loading').fadeIn();
			var uid = idCS;
			var USER_ID = $('input[name=user_id]').val();
			var users = [USER_ID,uid];
			var settings = {
			  "url": base_url+"/Sendbird/getUsers",
			  "method": "POST",
		  	"data": {
			  	"uid":users
				},
			};
			$.ajax(settings).done(function (done) {
			  var done = JSON.parse(done);
			  console.log(done);
			  if (done.status_code != '200') {
					$('#alert-sendbird .error_message').text(done.error_message);
					$('.fix_loading').fadeOut();
					$('#alert-sendbird').fadeIn();
					setTimeout(function(){
						$('#alert-sendbird').fadeOut();
					}, 5000); 
			  } else {
					$.ajax({
						"url": base_url+"/Sendbird/getChannel",
						"method": "POST",
				  	"data": {
					  	"owner_id":USER_ID,
							"vet_id":uid,
						},
					}).done(function (result) {
					  	var obj = JSON.parse(result);
					  	console.log(obj);
					  	if (obj.status_code == '200') {
								sbWidget.showChannel(obj.channel_url);
								$('.fix_loading').fadeOut();
					  	} else {
								createChannel(USER_ID,uid,uniqId);
					  	}
					});
			  }
			});
		}
	});

	$('.chat-vet').click(function(){
		$('.fix_loading').fadeIn();
		var USER_ID = $(this).data('profileid');
		USER_ID = String(USER_ID);
		var uid = $(this).data('id');
		uid = String(uid);
		var uniqId = Math.round(new Date().getTime() + (Math.random() * 100));
		var base_url = window.location.origin;
		var users = [USER_ID,uid];
		var settings = {
		  "url": base_url+"/Sendbird/getUsers",
		  "method": "POST",
	  	"data": {
		  	"uid":users
			},
		};
		$.ajax(settings).done(function (done) {
		  var done = JSON.parse(done);
		  console.log(done);
		  if (done.status_code != '200') {
				$('#alert-sendbird .error_message').text(done.error_message);
				$('.fix_loading').fadeOut();
				$('#alert-sendbird').fadeIn();
				setTimeout(function(){
					$('#alert-sendbird').fadeOut();
				}, 5000); 
		  } else {
				$.ajax({
					"url": base_url+"/Sendbird/getChannel",
					"method": "POST",
			  	"data": {
				  	"owner_id":USER_ID,
						"vet_id":uid,
					},
				}).done(function (result) {
				  	var obj = JSON.parse(result);
				  	console.log(obj);
				  	if (obj.status_code == '200') {
						sbWidget.showChannel(obj.channel_url);
						$('.fix_loading').fadeOut();
				  	} else {
						createChannel(USER_ID,uid,uniqId);
				  	}
				});
		  }
		});
	});

	function createChannel(USER_ID,uid,uniqId){
		var user_ids = [USER_ID,uid];
		var settings = {
			"url": api_url+"/group_channels",
			"method": "POST",
			"timeout": 0,
			"headers": {
			    "Api-Token": api_token,
			    "Content-Type": "application/json"
			},
		  	"data": JSON.stringify({
			  	"name":"Chat To Vet",
				"channel_url":uniqId,
				"cover_url":"https://sendbird.com/main/img/cover/cover_08.jpg",
				"custom_type":"Chat With Vet",
				"inviter_id":USER_ID,
				"user_ids":user_ids
			}),
		};

		$.ajax(settings).done(function (response) {
		  	console.log(response);
			var base_url = window.location.origin;
			$.ajax({
				"url": base_url+"/Sendbird/insertChannel",
				"method": "POST",
			  	"data": {
				  "owner_id":USER_ID,
					"vet_id":uid,
					"channel_url":uniqId,
				},
			}).done(function (result) {
			  	var obj = JSON.parse(result);
			  	// console.log(obj);
			  	if (obj.status_code == '200') {
						if ($(".sendbird-container").length > 0) {
		    			$('#contact-cs-login').attr('data-url',uniqId);
		    			$('#contact-cs-login').attr('data-email',USER_ID);
							$('.close-formSendbird').trigger( "click" );
						}
						sbWidget.showChannel(uniqId);
						$('.fix_loading').fadeOut();
			  	} else {
			  		deleteChannel(uniqId)
						$('.fix_loading').fadeOut();
			  	}
			});
		});
	}

	function deleteChannel(uniqId){
		var settings = {
		  "url": api_url+"/group_channels/"+uniqId,
		  "method": "DELETE",
		  "timeout": 0,
		  "headers": {
		    "Api-Token": api_token,
		    "Content-Type": "application/json"
		  }
		};

		$.ajax(settings).done(function (response) {
		  console.log(response);
		});
	}

	// SENDBIRD BEFORE LOGIN
		$('.close-formSendbird').click(function(){
			$('.panel-success').fadeOut();
			$('#contact-cs-login').fadeOut();
			setTimeout(function(){
				$(".sendbird-container").css("top", "75%");
					$('#contact-cs-login').fadeIn();
			}, 500);
		});

		$('#contact-cs-login').click(function(){
				var channel_url = $(this).data('url');
				var email = $(this).data('email');
				var nickname = $(this).data('nickname');
				if (channel_url != '') {
				  sbWidget.startWithConnect(appId, email, nickname, function() {
				  	// isUserMessage('345','test');
				  });
					sbWidget.showChannel(channel_url);
				} else {
					$('.panel-success').fadeIn();
					$(".sendbird-container").css("top", "18%");
				}
		});
		$( ".sendbird-container" ).submit(function( event ) {
		  event.preventDefault();
		  $('.sendbird_loading').fadeIn();
			var formData = $('.sendbird-container').serializeArray();
			var formData_arr = {};

	    $.map(formData, function(n, i){
	        formData_arr[n['name']] = n['value'];
	    });

	    var vet = formData_arr.checkbox_vet != undefined ? true : false ;
	    var pet = formData_arr.checkbox_pet != undefined ? true : false ;
	    var nickname = formData_arr.nickname;
	    if (vet == true ) {
	    	nickname = formData_arr.nickname+' (Vet)';
	    } else if (pet == true){
	    	nickname = formData_arr.nickname+' (Pet Owner)';
	    } else if (pet == true && vet == true){
	    	nickname = formData_arr.nickname+' (Pet Owner & Vet)';
	    }
	    var email = formData_arr.email;
	    $.cookie("sendbird_id", JSON.stringify(formData_arr));
		  sbWidget.startWithConnect(appId, email, nickname, function() {
		  	// isUserMessage('345','test');
		  });
	    $.ajax({
				"url": base_url+"/Sendbird/getChannel",
				"method": "POST",
		  	"data": {
			  	"vet_id":idCS,
					"owner_id":email,
				},
			}).done(function (result) {
			  	var obj = JSON.parse(result);
			  	console.log(obj);
			  	if (obj.status_code == '200') {
						sbWidget.showChannel(obj.channel_url);
						$('.close-formSendbird').trigger( "click" );
						$('.fix_loading').fadeOut();
	    			$('#contact-cs-login').attr('url',obj.channel_url);
	    			$('#contact-cs-login').attr('email',email);
	    			$('#contact-cs-login').attr('nickname',nickname);
			  	} else {
						createChannel(email,idCS,uniqId);
			  	}
			});
		});
	// SENDBIRD BEFORE LOGIN

	

});