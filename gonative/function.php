<?php
/**
 *
 * GONATIVE APPLICATION ver 1 for temantrader.com
 * =============== Main Config PHP File Script ================
 * based on https://gonative.io/docs
 *
 * Composed by : Asep Rusmana
 * Launch Time : Des 2017     
 *     
 *
**/

error_reporting(0);

function offline_msg(){
    echo "Database is offline";
	return;
}

function doConnect() {
	global $config;	
	$link = mysqli_connect($config["db_server"], $config["db_user"], $config["db_password"]) or die(mysqli_error());
	mysqli_select_db($link,$config["db_name"]) or die(mysqli_error());	
	return $link;
}

function access_log($ip,$message,$http_method,$query_string,$http_user_agent,$user_name) {
  $link = doConnect();
  $sql = "INSERT INTO tbl_logs(id,ip_address,message,http_method,query_string,http_user_agent,user_name,created_on) VALUES('','$ip','$message','$http_method','$query_string','$http_user_agent','$user_name',NOW())";
  $res = mysqli_query($link,$sql);
  return $res;
}

function get_stock(){
	$link = doConnect();
	$sql ="SELECT * FROM GJ9RQ4Rpwh_stockpick";
	$res = mysqli_query($link,$sql);
	return $res;
}

function check_user($user){
	$link = doConnect();
	$sql ="SELECT * FROM tbl_membertetra WHERE user_line='$user' LIMIT 1";
	$res = mysqli_query($link,$sql);
	return $res;
}

function check_email($email){
	$link = doConnect();
	$sql ="SELECT * FROM tbl_membertetra WHERE user_email='$email' LIMIT 1";
	$res = mysqli_query($link,$sql);
	return $res;
}

function update_id($id,$sess){
	$link = doConnect();
	$sql ="UPDATE tbl_membertetra SET playerid='$id' WHERE sessionid='$sess' LIMIT 1";
	$res = mysqli_query($link,$sql);
	return $res;
}

?>
