<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Email Sender
 */
class EmailSender
{
    private $ci; // CI Instance

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->ci->load->library('email');

        $this->ci->load->config('email');
        $this->ci->email->initialize($this->ci->config->item('email'));
    }

    public function send($email,$subject,$text)
    {
        if(stristr($subject,"@yahoo")||stristr($subject,"@ymail")||stristr($subject,"@yahoo")){
            $this->ci->load->config('email_yahoo');
            $this->ci->email->initialize($this->ci->config->item('email'));
        }
        $this->ci->email->from('noreply@ivetdata.com', 'iVetData');
        $this->ci->email->to($email);
        // $this->ci->email->cc('another@another-example.com');
        // $this->ci->email->bcc('them@their-example.com');

        $this->ci->email->subject($subject);
        $this->ci->email->message($text);

        $this->ci->email->send();
    }
}