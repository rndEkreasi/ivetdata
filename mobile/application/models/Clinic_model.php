<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinic_model extends CI_Model {

	function data($number,$offset,$idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);		
		};
		$this->db->where('front','1');
		$this->db->order_by('pet','DESC');
		return $query = $this->db->get('tbl_clinic',$number,$offset)->result();		
	}
 
	function jumlah_data($idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('front','1');
		return $this->db->get('tbl_clinic')->num_rows();
	}
	

	function clinicdisplay(){		
		$this->db->where('front','1');
		return $this->db->get('tbl_clinic')->num_rows();
	}

	function clinicdata($number,$offset){
		$this->db->where('front','1');
		return $query = $this->db->get('tbl_clinic',$number,$offset)->result();		
	}

	function allclinicshow(){
		$this->db->from('tbl_clinic');
		$this->db->where('front','1');
		$query = $this->db->get();
		return $query->result();
	}

	public function searchname($name){
		$this->db->from('tbl_clinic');
		$this->db->like('nameclinic',$name);
		$this->db->where('front','1');
		$this->db->order_by('idclinic','desc');
		$query = $this->db->get();
		$result1 = $query->result();
		$this->db->from('tbl_member');
		$this->db->like('name',$name);
		$this->db->where('role','1');
		$query2 = $this->db->get();
		$result2 = $query2->result();
		$i=0;
		$row=array();
		foreach($result1 as $val){
		  $row[$i]['idclinic']=$val->idclinic;
		  $row[$i]['nameclinic']=$val->nameclinic;
		  $row[$i]['picture']=$val->picture;
		  $row[$i]['address']=$val->address;
		  $row[$i]['city']=$val->city;
		  $row[$i]['phone']=$val->phone;
		  $row[$i]['clinic']=1;
		  $i++;
		}
		foreach($result2 as $val){
		  $row[$i]['idclinic']=$val->uid;
		  $row[$i]['nameclinic']=$val->name;
		  $row[$i]['picture']="";
		  $row[$i]['address']=$val->address;
		  $row[$i]['city']=$val->city;
		  $row[$i]['phone']=$val->phone;
		  $row[$i]['clinic']=0;
		  $i++;
		}
		return $row;
	}

    public function listcat(){
        $this->db->from('tbl_category');
        $this->db->where('type','clinic');
        $query = $this->db->get();
		return $query->result();
    }
    
    public function searchcat($cat,$combo){
		$this->db->from('tbl_clinic');
		$this->db->where("substr(category,'".$cat."',1)",1);
		//$this->db->where('front','1');
		if($combo<>''){
		    $this->db->where("(nameclinic LIKE '%".$combo."%' OR city LIKE '%".$combo."%' OR address LIKE '%".$combo."%' OR province LIKE '%".$combo."%' OR country LIKE '%".$combo."%')");
		}
		$this->db->order_by('idclinic','desc');
		$query = $this->db->get();
		$result = $query->result();
		$i=0;
		$row=array();
		$chk=FALSE;
		foreach($result as $val){
		  $row[$i]['idclinic']=$val->idclinic;
		  $row[$i]['nameclinic']=$val->nameclinic;
		  $row[$i]['picture']=$val->picture;
		  $row[$i]['address']=$val->address;
		  $row[$i]['city']=$val->city;
		  $row[$i]['phone']=$val->phone;
		  $row[$i]['clinic']=1;
		  $i++;
		}
		return $row;
	}
	
	public function search($name,$city){
		$this->db->from('tbl_clinic');
		if($name==$city){
    		$this->db->where("(nameclinic LIKE '%".$name."%' OR city LIKE '%".$city."%' OR address LIKE '%".$city."%' OR province LIKE '%".$city."%' OR country LIKE '%".$city."%')");
		}else{
		    if($name<>''){
        		$this->db->like('nameclinic',$name);
		    }
    		$this->db->where("(city LIKE '%".$city."%' OR address LIKE '%".$city."%' OR province LIKE '%".$city."%' OR country LIKE '%".$city."%')");
		}
		//$this->db->where('front','1');
		$this->db->order_by('idclinic','desc');
		$this->db->group_by('idclinic');
		$query = $this->db->get();
		$result1 = $query->result();
		$this->db->from('tbl_member');
		if($name==$city){
    		$this->db->where("role='1' AND (name LIKE '%".$name."%' OR city LIKE '%".$city."%' OR address LIKE '%".$city."%' OR country LIKE '%".$city."%')");
		}else{
		    if($name<>''){
    		    $this->db->like('name',$name);
		    }
    		$this->db->where("(city LIKE '%".$city."%' OR address LIKE '%".$city."%' OR country LIKE '%".$city."%')");
		}
		$this->db->where('role','1');
		$this->db->group_by('uid');
		$query2 = $this->db->get();
		$result2 = $query2->result();
		$i=0;
		$row=array();
		foreach($result1 as $val){
		  $row[$i]['idclinic']=$val->idclinic;
		  $row[$i]['nameclinic']=$val->nameclinic;
		  $row[$i]['picture']=$val->picture;
		  $row[$i]['address']=$val->address;
		  $row[$i]['city']=$val->city;
		  $row[$i]['phone']=$val->phone;
		  $row[$i]['clinic']=1;
		  $i++;
		}
		foreach($result2 as $val){
		  $this->db->where('idclinic',$val->idclinic);
		  $this->db->from('tbl_clinic');
		  $query3 = $this->db->get();
		  $result3 = $query3->result();
		  if(count($result3)==0){
    		  $row[$i]['idclinic']=$val->uid;
    		  $row[$i]['nameclinic']=$val->name;
    		  $row[$i]['picture']=$val->photo;
    		  $row[$i]['address']=$val->address;
    		  $row[$i]['city']=$val->city;
    		  $row[$i]['phone']=$val->phone;
    		  $row[$i]['clinic']=0;
		  }else{
		      $row[$i]['idclinic']=$result3[0]->idclinic;
    		  $row[$i]['nameclinic']=$val->name;
    		  $row[$i]['picture']=$val->photo;
    		  $row[$i]['address']=$result3[0]->address;
    		  $row[$i]['city']=$result3[0]->city;
    		  $row[$i]['phone']=$result3[0]->phone;
    		  $row[$i]['clinic']=1;
		  }
		  $i++;
		}
		return $row;
	}
	
	public function searchid($idclinic){
		$this->db->from('tbl_clinic');
		$this->db->where('idclinic',$idclinic);
		$this->db->limit('1');
		$query = $this->db->get();
		$result = $query->result();
		if(count($result)>0){
		      $row[0]['idclinic']=$result[0]->idclinic;
    		  $row[0]['nameclinic']=$result[0]->nameclinic;
    		  $row[0]['picture']=$result[0]->picture;
    		  $row[0]['address']=$result[0]->address;
    		  $row[0]['city']=$result[0]->city;
    		  $row[0]['phone']=$result[0]->phone;
    		  $row[0]['clinic']=1;
		 }else{
		     $row = $result;
		 }
		return $row;
	}

	public function detailclinic($idclinic){
		$this->db->from('tbl_clinic');
		$this->db->where('idclinic',$idclinic);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	public function customerbyemail($email){
		$table1 = 'tbl_customer';
		$table2 = 'tbl_clinic';
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->where('email',$email);
		$this->db->where('idclinic>',0);
		//$this->db->join($table2, ''.$table1.'.idclinic = '.$table1.'.idclinic');
		$query = $this->db->get();
		return $query->result();
	}

    public function customerbyidmember($idmember){
		$table1 = 'tbl_customer';
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->where('uid',$idmember);
		$this->db->where('status',0);
		$this->db->where('idclinic>',0);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function customerbyidclinic($idmember,$idclinic){
		$table1 = 'tbl_customer';
		$this->db->select('*');
		$this->db->from($table1);
		$this->db->where('uid',$idmember);
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function searchall(){
		$this->db->from('tbl_clinic');
		$this->db->where('status','0');
		$this->db->order_by('nameclinic','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function searchcountry($country){
		$this->db->from('tbl_clinic');
		$this->db->where('country',$country);
		$this->db->where('status','0');
		$this->db->order_by('nameclinic','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function searchprov($prov){
		$query = $this->db->query('SELECT * FROM tbl_clinic WHERE status=0 AND `city` IN('.$prov.') ORDER BY nameclinic ASC');
		return $query->result();
	}
	
	public function searchcity($city){
		$this->db->from('tbl_clinic');
		$this->db->where('city',$city);
		$this->db->where('status','0');
		$this->db->order_by('nameclinic','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getpending($idclinic){
		$this->db->from('tbl_customer');
		$this->db->where('idclinic',"-".$idclinic);
		$this->db->where('status','0');
		$this->db->order_by('idcustomer','asc');
		$query = $this->db->get();
		return $query->result();
	}

}