<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends CI_Model {

	public function listshop($number,$offset){
		// $this->db->order_by('id','random');
		// $query = $this->db->query("select * from tbl_clinicservice where cat='medicine' and idclinic in (select idclinic from tbl_clinic where shop=1) ORDER BY RAND()");
		// return $query->result();

		$this->db->select('*');
		//$this->db->select('tbl_clinicservice.picture,tbl_clinicservice.nameservice,tbl_clinicservice.price,tbl_clinicservice.id,tbl_clinicservice.idclinic,tbl_clinic.nameclinic');
		//$this->db->from('tbl_clinicservice');
		//$this->db->where('tbl_clinicservice.nameservice',$item);
		$this->db->where('tbl_clinic.shop','1');
		//$this->db->order_by('tbl_clinicservice.id','random');
		$this->db->join('tbl_clinic', 'tbl_clinicservice.idclinic = tbl_clinic.idclinic','left');
		//$this->db->limit($offset);
		if(isset($_GET['id'])){
		    $this->db->like('type',",".$_GET['id'].",");
		}
		return $query = $this->db->get('tbl_clinicservice',$number)->result();
		//return $query->result();
	}

	public function search($item){
		//$this->db->order_by('id','random');
		// $this->db->where('nameservice',$item);
		// $query = $this->db->query("select * from tbl_clinicservice where cat='medicine' and idclinic in (select idclinic from tbl_clinic where shop=1) ORDER BY RAND()");
		// return $query->result();

		//$this->db->select('*');
		$this->db->select('tbl_clinicservice.picture,tbl_clinicservice.nameservice,tbl_clinicservice.price,tbl_clinicservice.id,tbl_clinicservice.idclinic,tbl_clinic.nameclinic');
		$this->db->from('tbl_clinicservice');
		$this->db->like('tbl_clinicservice.nameservice',$item);
		$this->db->where('tbl_clinic.shop','1');
		$this->db->join('tbl_clinic', 'tbl_clinicservice.idclinic=tbl_clinic.idclinic','left');
		//$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	public function serviceshop($number,$offset){
		// $this->db->order_by('id','random');
		// $query = $this->db->query("select * from tbl_clinicservice where cat='medicine' and idclinic in (select idclinic from tbl_clinic where shop=1) ORDER BY RAND()");
		// return $query->result();

		$this->db->select('*');
		//$this->db->select('tbl_clinicservice.picture,tbl_clinicservice.nameservice,tbl_clinicservice.price,tbl_clinicservice.id,tbl_clinicservice.idclinic,tbl_clinic.nameclinic');
		//$this->db->from('tbl_clinicservice');
		//$this->db->where('tbl_clinicservice.nameservice',$item);
		$this->db->where('tbl_clinic.shop','1');
		$this->db->where('tbl_clinicservice.cat','service');
		//$this->db->order_by('tbl_clinicservice.id','random');
		$this->db->join('tbl_clinic', 'tbl_clinicservice.idclinic = tbl_clinic.idclinic','left');
		//$this->db->limit($offset);
		if(isset($_GET['id'])){
		    $this->db->like('type',",".$_GET['id'].",");
		}
		return $query = $this->db->get('tbl_clinicservice',$number)->result();
		//return $query->result();
		
		//$query = $this->db->query("select * from tbl_clinicservice where cat='service' and idclinic in (select idclinic from tbl_clinic where shop=1)");
		///return $query->result();
		//$query = $this->db->get();
		//return $query->result();
	}

	public function clinicshop(){
		$this->db->select('idclinic');
		$this->db->from('tbl_clinic');
		$this->db->where('shop','1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result_array();
	}

	function countproduct(){

		$this->db->order_by('id','random');
		$query = $this->db->query("select * from tbl_clinicservice where cat='medicine' and idclinic in (select idclinic from tbl_clinic where shop=1) ORDER BY RAND()");
		return $query->num_rows();

  //       // $this->db->where('uid',$profileid);
  //       // $this->db->where('status <','2');
        
  //       // return $this->db->get('tbl_invoice')->num_rows();

  //       $this->db->select('tbl_clinicservice.id');
		// $this->db->from('tbl_clinicservice');
		// //$this->db->where('tbl_clinicservice.nameservice',$item);
		// $this->db->where('tbl_clinic.shop','1');
		// $this->db->order_by('tbl_clinicservice.id','random');
		// $this->db->join('tbl_clinic', 'tbl_clinicservice.idclinic=tbl_clinic.idclinic','left');
		// //$this->db->limit('1');
		// return $this->db->get('tbl_clinicservice')->num_rows();

    }

	function mydata($number,$offset,$profileid){
        $this->db->order_by('idinv','desc');
        $this->db->where('uid',$profileid);
        $this->db->where('status <','2');
        
        return $query = $this->db->get('tbl_invoice',$number,$offset)->result();        
    }

    function myjumlah_data($profileid){

        $this->db->where('uid',$profileid);
        $this->db->where('status <','2');
        
        return $this->db->get('tbl_invoice')->num_rows();
    }

    public function idinvoice($idinvoice){
    	//$this->db->select('idclinic');
		$this->db->from('tbl_invoice');
		$this->db->where('invoice',$idinvoice);
		$this->db->order_by('idinv','desc');
		$this->db->limit('1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result();

    }

    public function cart($invoice){
    	$this->db->select('tbl_cart.nama_item,tbl_cart.id_cart,tbl_cart.idclinic,tbl_cart.dateadd,tbl_cart.idproduk,tbl_cart.qty,tbl_cart.price,tbl_cart.totalprice,tbl_clinicservice.picture');
		$this->db->from('tbl_cart');
		$this->db->where('tbl_cart.invoice_temp',$invoice);
		$this->db->join('tbl_clinicservice', 'tbl_cart.idproduk=tbl_clinicservice.id','left');
		$this->db->order_by('tbl_cart.id_cart','desc');
		//$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
    }

    public function totalcart($invoice){
		$this->db->select_sum('totalprice');
		$this->db->where('invoice_temp',$invoice);
		$this->db->where('status','1');
		$result = $this->db->get('tbl_cart')->row();  
		return $result->totalprice;
    }

    public function totalqty($invoice){
		$this->db->select_sum('qty');
		$this->db->where('invoice_temp',$invoice);
		$this->db->where('status','1');
		$result = $this->db->get('tbl_cart')->row();  
		return $result->qty;
    }

    public function detailcart($id_cart){
    	$this->db->from('tbl_cart');
		$this->db->where('id_cart',$id_cart);
		//$this->db->order_by('idinv','desc');
		$this->db->limit('1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result();
    }

    public function cekinvoicesettle($idinvoice){
    	$this->db->from('tbl_invoice');
		$this->db->where('invoice',$idinvoice);
		$this->db->order_by('idinv','desc');
		$this->db->limit('1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result();
    }

    public function insertinvoice($insertinvoiceuser){
       $this->db->insert('tbl_invoice',$insertinvoiceuser);
    }
    
    public function listcat(){
        $this->db->from('tbl_category');
        $this->db->where('type','shop');
        $query = $this->db->get();
		return $query->result();
    }
    
    public function listcatservice(){
        $this->db->from('tbl_category');
        $this->db->where('type','service');
        $query = $this->db->get();
		return $query->result();
    }

    public function itemcart($invoice){
        $this->db->select('nama_item AS item,qty,price,idclinic');
		$this->db->from('tbl_cart');
		$this->db->where('invoice_temp',$invoice);
		$this->db->where('status','1');
		$query = $this->db->get();  
		return $query->result();
    }

    
}