<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function all(){
		$this->db->from('tbl_transaction');
		//$this->db->where('status',$profileid);
		//$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();		
	}

	function data($number,$offset){
		$this->db->order_by('idtrx','desc');
		// if($role < '2'){
		// 	$this->db->where('idclinic',$idclinic);
		// };
		// $this->db->where('status','0');

		return $query = $this->db->get('tbl_transaksi',$number,$offset)->result();		
	}
 
	function jumlah_data(){
		// if($role < '2'){
		// 	$this->db->where('idclinic',$idclinic);
		// };
		// $this->db->where('status','0');
		return $this->db->get('tbl_transaksi')->num_rows();
	}

	public function detailprofile($uid){
		$this->db->from('tbl_member');
		$this->db->where('uid',$uid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();		
	}

	public function detailtrx($idtrx){
		$this->db->from('tbl_transaksi');
		$this->db->where('idtrx',$idtrx);
		$query = $this->db->get();
		return $query->result();
	}

	public function detailservice($idservice){
		$this->db->from('tbl_service');
		$this->db->where('idservice',$idservice);
		$query = $this->db->get();
		return $query->result();
	}

}