<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pet_model extends CI_Model {

	public function cekpetid($rfid){
		$this->db->from('tbl_pet');
		$this->db->where('rfid',$rfid);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function all(){
		$this->db->from('tbl_pet');		
		$query = $this->db->get();
		return $query->result();
	}
	public function allnohp($limit){
		$this->db->from('tbl_pet');	
		$this->db->where('nohp','');
		$this->db->where('status','0');
		$this->db->limit($limit);	
		$query = $this->db->get();
		return $query->result();
	}
	public function cekpetbyname($vetclinic,$namepet){
		$this->db->from('tbl_pet');	
		$this->db->where('idclinic',$vetclinic);
		$this->db->like('namapet',$namepet);
		$this->db->where('status','0');
		//$this->db->limit($limit);	
		$query = $this->db->get();
		return $query->result();
	}

	public function cekpetbyphone($phone,$vetclinic){
		$this->db->from('tbl_pet');	
		$this->db->where('nohp',$phone);
		$this->db->where('idclinic',$vetclinic);
		$this->db->where('status','0');
		//$this->db->limit($limit);	
		$query = $this->db->get();
		return $query->result();
	}

    public function cekpetbyemail($email,$vetclinic){
		$this->db->from('tbl_pet');	
		$this->db->where('idclinic',$vetclinic);
		$this->db->like('email',$email);
		$this->db->where('status','0');
		//$this->db->limit($limit);	
		$query = $this->db->get();
		return $query->result();
	}
	
	public function pettype(){
		$this->db->from('tbl_pettype');
		$query = $this->db->get();
		return $query->result();
	}

	public function getcustomer($idclinic,$email){
		$this->db->from('tbl_customer');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('email',$email);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	public function getcustomerhp($idclinic,$phone){
		$this->db->from('tbl_customer');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('nohp',$phone);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	function data($number,$offset,$idclinic,$role){
	    if(isset($_GET['order'])){
	        if($_GET['order']=='asc'){
	            $order = 'asc';
	        }else{
	            $order = 'desc';
	        }
	        $sort = "namapet";
	    }else{
	        $sort = "idpet";
	        $order = 'desc';
	    }
		$this->db->order_by($sort,$order);
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('status','0');
		return $query = $this->db->get('tbl_pet',$number,$offset)->result();		
	}
 
	function jumlah_data($idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('status','0');
		return $this->db->get('tbl_pet')->num_rows();
	}

	public function petpage($profileid){
		$this->db->from('tbl_pet');
		$this->db->where('input_by',$profileid);
		$this->db->limit('20');
		$this->db->where('status','0');
		$this->db->order_by('idpet','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detailpet($idpet){
		$this->db->from('tbl_pet');
		$this->db->where('idpet',$idpet);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function detailmicrochip($idpet){
		$this->db->from('tbl_pet');
		$this->db->where('rfid',$idpet);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function detailmicrochipbyowner($idpet,$email){
		$this->db->from('tbl_pet');
		$this->db->where('rfid',$idpet);
		$this->db->where('email',$email);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function logpet($idpet){
		$this->db->from('tbl_logpet');
		$this->db->where('idpet',$idpet);
		$this->db->where('status','0');
		$this->db->order_by('idlog','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function logvaccine($idpet){
		$this->db->from('tbl_vaccine');
		$this->db->where('idpet',$idpet);
		$this->db->where('status','0');
		$this->db->where('category','vaccine');
		$this->db->order_by('idvacc','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detailvaccine($idvacc){
		$this->db->from('tbl_vaccine');
		$this->db->where('idvacc',$idvacc);
		$this->db->where('status','0');
		$this->db->where('category','vaccine');
		$query = $this->db->get();
		return $query->result();
	}

	public function lastvaccine($idpet){
		$this->db->from('tbl_vaccine');
		$this->db->where('idpet',$idpet);
		$this->db->where('category','vaccine');
		$this->db->limit('1');
		$this->db->order_by('idvacc','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function ceklogid($idlog,$idpet){
		$this->db->from('tbl_logpet');
		$this->db->where('idlog',$idlog);
		$this->db->where('idpet',$idpet);
		$this->db->where('status','0');
		$this->db->limit('1');
		$this->db->order_by('idlog','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function ceklogidall($idpet){
		$this->db->from('tbl_logpet');
		$this->db->where('idpet',$idpet);
		$this->db->where('status','0');
		$this->db->limit('1');
		$this->db->order_by('idlog','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function datacustomer($idowner){
		$this->db->from('tbl_customer');
		$this->db->where('idcustomer',$idowner);
		$query = $this->db->get();
		return $query->result();
	}

	public function detailclinic($idclinic){
		$this->db->from('tbl_clinic');
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	public function detailvet($idvet){
		$this->db->from('tbl_member');
		$this->db->where('uid',$idvet);
		$query = $this->db->get();
		return $query->result();
	}

	function search_blog($title){
        $this->db->like('breed', $title , 'both');
        $this->db->order_by('breed', 'ASC');
        $this->db->limit(10);
        return $this->db->get('tbl_breed')->result();
    }

    public function byowner($idowner){
    	$this->db->from('tbl_pet');
		$this->db->where('idowner',$idowner);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
    }

    public function byowneremail($email,$idclinic,$role){
    	$this->db->from('tbl_pet');
		$this->db->where('email',$email);
		$this->db->where('status','0');
		if($role>0){
		    $this->db->where('idclinic',$idclinic);
		}
		$query = $this->db->get();
		return $query->result();
    }
    
    public function byownerid($idowner,$idclinic,$role){
    	$this->db->from('tbl_pet');
		$this->db->where('idowner',$idowner);
		$this->db->where('status','0');
		if($role>0){
		    $this->db->where('idclinic',$idclinic);
		}
		$query = $this->db->get();
		return $query->result();
    }

    public function byownerphone($phone){
    	$this->db->from('tbl_pet');
		$this->db->where('nohp',$phone);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
    }
	
    public function byidmember($idmember){
    	$this->db->from('tbl_pet');
		$this->db->where('idowner',$idmember);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
    }
    
	public function detailmicrochipbyidmember($idpet,$idmember){
		$this->db->from('tbl_pet');
		$this->db->where('rfid',$idpet);
		$this->db->where('idowner',$idmember);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function getdiary($idpet){
    	$this->db->from('tbl_notes');
		$this->db->where('idpet',$idpet);
		$query = $this->db->get();
		return $query->result();
    }
}