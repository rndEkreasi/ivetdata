<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_Model {

	public function clinic($idclinic,$role){
		$this->db->from('tbl_clinicservice');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->order_by('id','desc');
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	function data($number,$offset,$idclinic,$role){
		$this->db->order_by('id','desc');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->order_by('id','asc');
		return $query = $this->db->get('tbl_clinicservice',$number,$offset)->result();		
	}
 
	function jumlah_data($idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->order_by('id','desc');
		return $this->db->get('tbl_clinicservice')->num_rows();
	}

	function search_blog($title,$idclinic){
        $this->db->like('nameservice', $title , 'both');
        $this->db->where('idclinic',$idclinic);
        $this->db->order_by('nameservice', 'ASC');
        $this->db->limit(10);
        return $this->db->get('tbl_clinicservice')->result();
    }

    public function detailservice($idservice){
		$this->db->from('tbl_clinicservice');
		$this->db->where('id',$idservice);
		$query = $this->db->get();
		return $query->result();
	}

	public function searchservice($nameservice,$idclinic){
    	$this->db->from('tbl_clinicservice');
		$this->db->where('nameservice',$nameservice);
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
    }

    public function cekservice($nameservice,$idclinic){
    	$this->db->from('tbl_clinicservice');
		$this->db->where('nameservice',$nameservice);
		$this->db->where('idclinic',$idclinic);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
    }

    //ajax
    var $table = 'tbl_clinicservice';
	var $column_order = array('id', 'nameservice','qty','cat','price'); //set column field database for datatable orderable
	var $column_search = array('nameservice','qty','cat','price'); //set column field database for datatable searchable 
	var $order = array('id' => 'desc'); // default order 
	private function _get_datatables_query($idclinic)
	{
		
		//add custom filter here
		if($this->input->post('id'))
		{
			$this->db->where('id', $this->input->post('id'));
		}
		if($this->input->post('nameservice'))
		{
			$this->db->where('nameservice', $this->input->post('nameservice'));
		}
		if($this->input->post('qty'))
		{
		 	$this->db->like('qty', $this->input->post('qty'));
		}
		if($this->input->post('cat'))
		{
			$this->db->like('cat', $this->input->post('cat'));
		}
		if($this->input->post('price'))
		{
			$this->db->like('price', $this->input->post('price'));
		}

		$this->db->from($this->table);
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables($idclinic)
	{
		$this->_get_datatables_query($idclinic);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered($idclinic)
	{
		$this->_get_datatables_query($idclinic);
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($idclinic)
	{
		$this->db->from($this->table);
		$this->db->where('idclinic',$idclinic);
		return $this->db->count_all_results();
	}

	public function get_list_countries($idclinic)
	{
		$this->db->select('cat');
		$this->db->from($this->table);
		$this->db->where('idclinic',$idclinic);
		$this->db->order_by('cat','asc');
		$query = $this->db->get();
		$result = $query->result();

		$countries = array();
		foreach ($result as $row) 
		{
			$countries[] = $row->cat;
		}
		return $countries;
	}
	//end ajaxsearch

	public function getvets($idclinic){
		$this->db->from('tbl_member');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('manageto !=','3');
		$query = $this->db->get();
		return $query->result();
	}

}