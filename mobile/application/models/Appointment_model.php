<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment_model extends CI_Model {

	public function myappoint($profileid){
		$today = date('Y-m-d H:i:s',strtotime('-1 days'));
		$this->db->select('*');
		//$today = date('d')
		$this->db->from('tbl_vaccine');
		$this->db->where('tbl_vaccine.idowner',$profileid);
		$this->db->where('tbl_vaccine.status','0');
		$this->db->where('tbl_vaccine.nextdate >', $today);
		$this->db->join('tbl_pet', 'tbl_pet.idpet=tbl_vaccine.idpet', 'left');
    	$this->db->join('tbl_clinic', 'tbl_clinic.idclinic=tbl_vaccine.idclinic', 'left');
    	$this->db->join('tbl_member', 'tbl_member.uid=tbl_vaccine.idvet', 'left');
		$this->db->order_by('tbl_vaccine.nextdate','desc');
		$this->db->limit('10');
		$query = $this->db->get();
		return $query->result();
		
	}

	public function detailappoint($idvacc){
		$this->db->select('tbl_pet.namapet,tbl_vaccine.description,tbl_vaccine.idclinic,tbl_vaccine.idvacc,tbl_vaccine.idowner,tbl_vaccine.nextdate,tbl_member.uid,tbl_member.name');
		//$today = date('d')
		$this->db->from('tbl_vaccine');
		$this->db->where('tbl_vaccine.idvacc',$idvacc);
		$this->db->join('tbl_pet', 'tbl_pet.idpet=tbl_vaccine.idpet', 'left');
    	$this->db->join('tbl_clinic', 'tbl_clinic.idclinic=tbl_vaccine.idclinic', 'left');
    	$this->db->join('tbl_member', 'tbl_member.uid=tbl_vaccine.idvet', 'left');
		$this->db->order_by('tbl_vaccine.nextdate','desc');
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	public function listpet($email){
		$this->db->from('tbl_pet');
		$this->db->where('email',$email);
		$this->db->where('status','0');
		//$this->db->order_by('nextdate','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function listvet($idclinic){
		$this->db->from('tbl_member');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('status','1');
		//$this->db->order_by('nextdate','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function idinvoice($idinvoice){
    	//$this->db->select('idclinic');
		$this->db->from('tbl_invoice');
		$this->db->where('invoice',$idinvoice);
		$this->db->order_by('idinv','desc');
		$this->db->limit('1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result();

    }

}