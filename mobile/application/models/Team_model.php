<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_model extends CI_Model {

	public function totalvets($idclinic){
		$this->db->from('tbl_member');
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	public function cekuid($uid,$email,$clinic){
		$this->db->from('tbl_member');
		$this->db->where('uid',$uid);
		$this->db->where('email',$email);
		$this->db->where('idclinic',$clinic);
		$query = $this->db->get();
		return $query->result();
	}

	public function buyteam($idclinic,$idprofile){
		$this->db->from('tbl_member');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('uid !=',$idprofile);

		$query = $this->db->get();
		return $query->result();
	}

}