<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

	public function totalvets($idclinic){
		$this->db->from('tbl_dokter');
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	function data($number,$offset,$idclinic,$role){
		$this->db->order_by('idinv','desc');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
			$this->db->where('status','1');
		};
        $this->db->order_by('nameclient','desc');
		return $query = $this->db->get('tbl_invoice',$number,$offset)->result();		
	}
 
	function jumlah_data($idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
			$this->db->where('status','1');
		};
		return $this->db->get('tbl_invoice')->num_rows();
	}

    function mydata($number,$offset,$profileid){
        $this->db->order_by('idinv','desc');
        $this->db->where('uid',$profileid);
        $this->db->where('status','1');
        
        return $query = $this->db->get('tbl_invoice',$number,$offset)->result();        
    }

    function myjumlah_data($profileid){

        $this->db->where('uid',$profileid);
        $this->db->where('status','1');
        
        return $this->db->get('tbl_invoice')->num_rows();
    }

	public function detailinv($invoice){
		$this->db->from('tbl_invoice');
		$this->db->where('invoice',$invoice);
        $this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}

	public function detailinvoice($idinvoice){
		$this->db->from('tbl_invoice');
		$this->db->where('idinv',$idinvoice);
		$query = $this->db->get();
		return $query->result();
	}

    public function detailclinic($clinicinv){
        $this->db->from('tbl_clinic');
        $this->db->where('idclinic',$clinicinv);
        $query = $this->db->get();
        return $query->result();
    }

	public function iteminv($invoice,$totalqty){
		$this->db->from('tbl_invoiceitem');
		$this->db->where('invoice',$invoice);
		$this->db->order_by('iditem','desc');
		$this->db->limit($totalqty);
		$query = $this->db->get();
		return $query->result();
	}

	public function sendemail($email,$nameclient,$phoneclient,$totalpriceinv,$iteminvoice,$invoice,$namaclinic,$addressclinic,$cityclinic,$countryclinic,$invdate,$diskon,$tax){
		//Sent email;
					$subject = 'Thanks for your payment Invoice no #'.$invoice.' ';
					$this->load->library('email');
					$message = '
								<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Detail Invoice #'.$invoice.'</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="https://www.ivetdata.com/assets/images/logo.png" style="width:100%; max-width:300px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Invoice : #'.$invoice.'<br>
                                Created : '.$invdate.'<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                            	Invoice From :<br>
                                '.$namaclinic.'<br>
                                '.$addressclinic.'<br>
                                '.$cityclinic.', '.$countryclinic.'
                            </td>
                        </tr>
                        <tr>
                            <td>
                            	Invoice To:<br>
                                '.$nameclient.'.<br>
                                '.$email.'<br>
                                '.$phoneclient.'
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>           
            
            
            <tr class="heading">
                <td>
                    Item
                </td>

                <td>
                    Qty
                </td>
                
                <td>
                    Price
                </td>
            </tr>';

			foreach ($iteminvoice as $item) { 
               $price = $item->price;
               $qty = $item->qty;
               $totalprice = $price * $qty;
            $message .='<tr class="item">
                <td width=50%>
                    '.$item->item.'
                </td>
                <td width=20%> '.number_format($qty).'</td>
                
                <td width=30%>
                    Rp.'.number_format($price).'
                </td>
            </tr>';
            };
            $message .='<tr>
            	<td></td>
                <td class="text-right"><strong>Sub Total</strong></td>
                <td><span>Rp. '.number_format($totalpriceinv + $diskon - $tax).' </span></td>
            </tr>
            <tr>
                <td></td>
                <td class="text-right"><strong>Discount</strong></td>
                <td><span>Rp. '.number_format($diskon).'</span></td>
            </tr>
            <tr>
            	<td></td>
                <td class="text-right"><strong>Tax</strong></td>
                <td><span>Rp. '.number_format($tax).'</span></td>
            </tr>
            <tr class="total">
                <td></td>
                <td><strong>Grand Total</strong></td>                
                <td> Rp. '.number_format($totalpriceinv).'</td>
            </tr>
        </table>
    </div>
</body>
</html>								';
					$text = $message;
			        //$email = $user_email;
			        //$this->load->view('testemail_view');
			        $this->load->library('EmailSender.php');
			        $this->emailsender->send($email,$subject,$text);


	}

    public function insertitem($dataitem){
        $this->db->insert('tbl_invoiceitem', $dataitem);
    }

}