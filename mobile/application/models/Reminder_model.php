<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder_model extends CI_Model {

	function remindervacc($idclinic,$role){
		$today = date('Y-m-d');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('nextdate>=',$today);
		$this->db->where('DATEDIFF(nextdate,DATE(NOW()))<=',7);
		$this->db->where('status','0');
		//$this->db->where('front','1');
		return $this->db->get('tbl_vaccine')->num_rows();
	}

	function datavaccine($number,$offset,$idclinic,$role){
		$today = date('Y-m-d');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);		
		};
		$this->db->where('status','0');
		$this->db->where('nextdate>=',$today);
		$this->db->where('DATEDIFF(nextdate,DATE(NOW()))<=',7);
		$this->db->order_by('nextdate', 'asc');
		return $query = $this->db->get('tbl_vaccine',$number,$offset)->result();		
	}


	public function detailcustomer($idcustomer){
		$this->db->from('tbl_customer');
		$this->db->where('idcustomer',$idcustomer);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function petowner($idpet){
		$this->db->from('tbl_pet');
		$this->db->where('idpet',$idpet);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function datamember($uidowner){
		$this->db->from('tbl_member');
		$this->db->where('uid',$uidowner);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}
	
	function remindermed($idclinic,$role){
		$today = date('Y-m-d');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('nextdate>=',$today);
		$this->db->where('DATEDIFF(nextdate,DATE(NOW()))<=',7);
		$this->db->where('status','0');
		//$this->db->where('front','1');
		return $this->db->get('tbl_logpet')->num_rows();
	}
	
	function datamed($number,$offset,$idclinic,$role){
		$today = date('Y-m-d');
		$this->db->select('a.idlog AS idvacc,a.idpet AS idpet,a.treatment AS treatment,b.idowner AS idowner,b.namapet AS pet,a.tgllog AS vaccdate,a.nextdate AS nextdate');
		$this->db->join('tbl_pet b','a.idpet=b.idpet');
		$this->db->join('tbl_clinic c','b.idclinic=c.idclinic');
		if($role < '2'){
			$this->db->where('b.idclinic',$idclinic);		
		};
		$this->db->where('a.status','0');
		$this->db->where('a.nextdate>=',$today);
		$this->db->where('DATEDIFF(a.nextdate,DATE(NOW()))<=',7);
		$this->db->order_by('a.nextdate', 'asc');
		return $query = $this->db->get('tbl_logpet a',$number,$offset)->result();		
	}

}