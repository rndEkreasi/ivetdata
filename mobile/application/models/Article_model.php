<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends CI_Model {
	function data($number,$offset){
		$this->db->order_by('idarticle','desc');
		//$this->db->where('status','0');
		return $query = $this->db->get('tbl_article',$number,$offset)->result();		
	}
 
	function jumlah_data(){
		//$this->db->where('status','0');
		return $this->db->get('tbl_article')->num_rows();
	}

	public function detail($slug){
		$this->db->from('tbl_article');
		$this->db->where('slug',$slug);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	public function detailarticle($idarticle){
		$this->db->from('tbl_article');
		$this->db->where('idarticle',$idarticle);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function articlehome(){
		$this->db->from('tbl_article');
		$this->db->order_by('idarticle','desc');
		$this->db->where('status','0');
		$this->db->limit(3);
		$query = $this->db->get();
		return $query->result();
	}
}