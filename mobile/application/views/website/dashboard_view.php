            <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url()  ?>">Dashboard</a>
                                </li>
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-4">
                            <h3 class="dashboard-page-title">Dashboard
                                <small>main page</small>
                            </h3>
                        </div>
                        <div class="col-md-8 text-md-right">
                            <h3 class="sparklines-title">
                                <sup>Today Earnings:</sup>

                                Rp. <?php // echo number_format($revtoday[0]->jumlah); ?>

                                <!-- <span class="sparklines" data-values="670,350,135,-170,-324,-386,-468,-200,55,375,520,270,790,-670,-350,135,170,324,386,468,10,55,375,520,270,790" data-type="bar" data-line-color="#eeb269" data-neg-color="#dc5753" data-height="30" data-bar-width="2">
                                </span> -->

                            </h3>

                            <h3 class="sparklines-title">
                                <sup>Yesterday Earn: </sup>
                                Rp. <?php echo number_format($revyesterday[0]->jumlah) ?>

                                <!-- <span class="sparklines" data-values="670,350,135,-170,-324,386,-468,-10,55,375,520,-270,790,670,-350,135,170,324,386,468,10,-55,-375,-520,270,790" data-type="bar" data-line-color="#4db19e" data-neg-color="#007ebd" data-height="30" data-bar-width="2">
                                </span> -->
                            </h3>

                        </div>

                    </div>
                    <!-- .row -->


                    <div class="row">
                        <div class="col-lg-3 col-sm-6">

                            <div class="teaser warning_bg_color counter-background-teaser text-center">
                                <span class="counter counter-background" data-from="0" data-to="<?php echo $countdoctor ?>" data-speed="2100">0</span>
                                <h3 class="counter highlight" data-from="0" data-to="<?php echo $countdoctor ?>" data-speed="2100"><?php echo $countdoctor ?></h3>
                                <p>Customer</p>
                            </div>

                        </div>

                        <div class="col-lg-3 col-sm-6">

                            <div class="teaser danger_bg_color counter-background-teaser text-center">
                                <span class="counter counter-background" data-from="0" data-to="<?php echo $countpet ?>" data-speed="1500">0</span>
                                <h3 class="counter highlight" data-from="0" data-to="<?php echo $countpet ?>" data-speed="1500">0</h3>
                                <p>Pets</p>
                            </div>

                        </div>

                        <div class="col-lg-3 col-sm-6">

                            <div class="teaser success_bg_color counter-background-teaser text-center">
                                <span class="counter counter-background" data-from="0" data-to="<?php echo $countorder ?>" data-speed="1900">0</span>
                                <h3 class="counter highlight" data-from="0" data-to="<?php echo $countorder ?>" data-speed="1900"><?php echo $countorder ?></h3>
                                <p>Order</p>
                            </div>

                        </div>

                        <div class="col-lg-3 col-sm-6">

                            <div class="teaser info_bg_color counter-background-teaser text-center">
                                <span class="counter counter-background" data-from="0" data-to="15" data-speed="1800">0</span>
                                <h3 class="counter-wrap highlight" data-from="0" data-to="<?php echo $totalrev[0]->jumlah/1000 ?>" data-speed="1800">
                                    <small>Rp.</small>
                                    <span class=""><?php echo number_format($totalrev[0]->jumlah )?></span>
                                    <small class="counter-add"></small>
                                </h3>
                                <p>Total Revenue</p>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <!-- Yearly Visitors -->
                        <!-- <div class="col-xs-12 col-md-6">
                            <div class="with_border with_padding">
                                <h4>Visitor Statistics</h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-yearly-visitors"></canvas>
                                </div>
                            </div>
                        </div> -->
                        <!-- .col-* -->

                        <!-- Yearly Visitors -->
                        <div class="col-xs-12 col-md-12">
                            <div class="with_border with_padding">
                                <h4>Sales Report</h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-visitors-sels"></canvas>
                                </div>
                                <!-- 
                    Pie Chart for new visitors. Uncomment if need 
                    <div>
                        <canvas class="canvas-chart-pie-visitors"></canvas>
                    </div>
                    -->
                            </div>
                        </div>
                        <!-- .col-* -->

                    </div>
                    <!-- .row -->

                    


                    


                    <div class="row">

                        <!-- Monthly Visitors -->
                        <div class="col-xs-12 col-md-6">
                            <div class="with_border with_padding">
                                <h4>Customer Report</h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-monthly-visitors"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- .col-* -->


                        <!-- Monthly Visitors -->
                        <div class="col-xs-12 col-md-6">
                            <div class="with_border with_padding">
                                <h4>Pet Report</h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-conversions"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->


                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar -->
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -->
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>

</body>

</html>