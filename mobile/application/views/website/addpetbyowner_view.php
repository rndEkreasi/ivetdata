<!-- link for jquery style -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- link for bootstrap style -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="<?php echo base_url() ?>assets/js/country/assets/js/geodatasource-cr.min.js"></script>
    <link rel="stylesheet" href="assets/css/geodatasource-countryflag.css">

    <!-- link to all languages po files -->
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ar/LC_MESSAGES/ar.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/cs/LC_MESSAGES/cs.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/da/LC_MESSAGES/da.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/de/LC_MESSAGES/de.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/en/LC_MESSAGES/en.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/es/LC_MESSAGES/es.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/fr/LC_MESSAGES/fr.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/it/LC_MESSAGES/it.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ja/LC_MESSAGES/ja.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ko/LC_MESSAGES/ko.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ms/LC_MESSAGES/ms.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/nl/LC_MESSAGES/nl.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/pt/LC_MESSAGES/pt.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ru/LC_MESSAGES/ru.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/sv/LC_MESSAGES/sv.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/vi/LC_MESSAGES/vi.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-cn/LC_MESSAGES/zh-cn.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-tw/LC_MESSAGES/zh-tw.po" />

    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/country/assets/js/Gettext.js"></script>

<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb darklinks">
                            <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                            <li class="active">Add New Pet</li>
                        </ol>
                    </div>
                    <!-- .col-* -->
                    <div class="col-md-6 text-md-right">
                         <!--<span> <?php echo date('D d, M Y');?></span>-->
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </section>
        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Add Pet</h3>
                            <a href="<?php echo base_url() ?>pet/all" class="icon-tab theme_button color2">Pet List</a>
                        </div>
                    </div>
                    <!-- .row -->
                    <form class="form-horizontal" action="<?php echo base_url() ?>pet/addprocessbyowner" method="post" enctype="multipart/form-data" style="padding:10px;">
                        <div class="row">
                            <div class="col-md-8">
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error; ?></div>
                                <?php } ?>       

                                <div class="with_padding">
                                    <h5>Pet Info</h5> 
                                    <hr>
                                    <div class="row form-group">                                        
                                        <label class="col-lg-3 control-label">Microchip : </label>
                                        <div class="col-lg-9">
                                            <select name="microchip" class="form-control" id="chooseuniq" required>                    
                                                <option value="0">Select Option</option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>                                                
                                            </select>                                                                                      
                                        </div>
                                    </div>
                                    <div class="row form-group" id="microchip">                                        
                                        <label class="col-lg-3 control-label">Microchip ID : </label>
                                        <div class="col-lg-9">
                                            <input type="number" name="rfid" pattern=".{15,16}" value="<?php if (isset($rfid)){ echo $rfid; } ?>" class="form-control active" placeholder="Enter Microchip ID">
                                            <span style="font-size: 10px;line-height: 109%;display: inline-block;margin: 5px 0px 0px;">Please input microchip number if you want the pet to be searchable in the public pet database. Otherwise leave it blank.</span>
                                        </div>
                                    </div>
                                    <!-- <div class="row form-group" id="uniqid">                                        
                                        <label class="col-lg-3 control-label">Uniq ID : </label>
                                        <div class="col-lg-9">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text">@</div>
                                                </div>
                                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Username">
                                              </div>
                                            <input type="hidden" name="clinicid" style="width: 20%;float: left;" class="form-control active" value="<?php echo $idclinic ?>" readonly required>
                                            <input type="number" name="rfiduniq" pattern=".{3,11}" class="form-control active" placeholder="Masukan angka unik 3-10 Angka" style="width: 80%;float: left;">
                                        </div>
                                    </div> -->
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Pet's Name* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="namepet" value="<?php if (isset($namepet)){ echo $namepet; } ?>" class="form-control active"  required>                                        
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Type* : </label>
                                        <div class="col-lg-9">
                                           <select name="type" class="form-control" required>
                                                <?php if (isset($type)){ ?>
                                                    <option value="<?php echo $type ?>"><?php echo ucfirst($type) ?></option>
                                                <?php } ?>
                                                <option value="Etc">Select Pet Type</option>
                                                <?php foreach ($pettype as $type) { ?>
                                                    <option value="<?php echo $type->type ?>"><?php echo $type->type ?></option>
                                                <?php } ?>                                                
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Breed* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="breed" id="breed" value="<?php if (isset($breed)){ echo $breed; } ?>" class="form-control active" required>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Gender* : </label>
                                        <div class="col-lg-9">
                                            <?php if (isset($gender)){ 
                                                if($gender == 'male'){ ?>
                                                    <input type="radio" name="gender" value="male" checked=""  > Male    <input type="radio" name="gender" value="female"> Female
                                                <?php }else{ ?>
                                                    <input type="radio" name="gender" value="male"> Male    <input type="radio" name="gender" value="female" checked=""> Female
                                                <?php }
                                            }else{ ?>
                                                <input type="radio" name="gender" value="male" checked=""  > Male    <input type="radio" name="gender" value="female"> Female
                                            <?php } ?>
                                            

                                            <!-- <select name="gender" class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Neutered* : </label>
                                        <div class="col-lg-9">
                                            <input type="radio" name="neutered" value="1">Yes <input type="radio" name="neutered" value="0" checked="" >No

                                            <!-- <select name="gender" class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Color* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="color" value="<?php if (isset($color)){ echo $color; } ?>" class="form-control active" required >
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Date Of Birth* : </label>
                                        <div class="col-lg-9">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" name="datebirth" value="<?php if (isset($datebirth)){ echo $datebirth; } ?>" class="js-date form-control" required>
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Special Marking : </label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control" name="note"><?php if (isset($note)){ echo $note; } ?></textarea>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Country Origin* : </label>
                                        <div class="col-lg-9">
                                            <!-- <input type="text" name="country" value="<?php if (isset($country)){ echo $country; } ?>" class="form-control active"> -->
                                            <select class="form-control gds-cr" name="country" country-data-region-id="gds-cr-one" data-language="en" required="" country-data-default-value="ID"></select>
                                        </div>
                                    </div>

                                    
                                   <!--  <div class="row form-group">
                                        <label class="col-lg-3 control-label">Characteristic : </label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control" placeholder="Add characteristic of this pet" name="description"><?php if (isset($description)){ echo $description; } ?></textarea>
                                        </div>
                                    </div> -->                                    

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"></label>
                                        <div class="col-lg-9">
                                            * Mandatory, must be filled.
                                        </div>
                                    </div>
                                    

                                    <div class="row">

                                        <div class="col-sm-12 text-right">
                                            <button type="submit" class="theme_button wide_button">Save Pet</button>
                                        </div>
                                    </div>
                                    <!-- .row  -->

                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->
                        </div>
                        <!-- .row  -->


                    </form>

                </div>
                <!-- .container -->
            </section>
        
    </div>
</div>
</section>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- <script src="//geodata.solutions/includes/countrystatecity.js"></script> -->
    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#breed').autocomplete({
                source: "<?php echo site_url('pet/breedlist');?>",
      
                select: function (event, ui) {
                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });
            $('#microchip').hide();
            // $('#uniqid').hide();
            $("#chooseuniq").change(function(){
                if($('#chooseuniq').val() == '1') {
                    $('#microchip').show(); 
                    $('#uniqid').hide();
                    var uniqid = document.getElementById("uniqid");  
                    uniqid.removeAttribute("required");
                }else {
                    $('#microchip').hide(); 
                    $('#uniqid').show();
                    var microchip= document.getElementById("microchip");  
                    microchip.removeAttribute("required");
                } 
            });
            

            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    $('#address').val(ui.item.address);
                    $('#city').val(ui.item.city);
                    //price.val(ui.item.label);                     
                }                
            });
 
        });
    </script>


</body>

</html>