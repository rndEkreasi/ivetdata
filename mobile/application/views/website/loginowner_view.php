<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $title ?></title>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="https://ivetdata.com/assets/images/icon.png"/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css" class="color-switcher-link">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dashboard.css" class="color-switcher-link">
    <script src="<?php echo base_url() ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131768019-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131768019-1');
</script>


    <!--[if lt IE 9]>
        <script src="<?php echo base_url() ?>assets/js/vendor/html5shiv.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/vendor/respond.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <![endif]-->

</head>

<body class="admin">
    <!--[if lt IE 9]>
        <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="<?php echo base_url() ?>assets/http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="rt-icon2-cross2"></i>
            </span>
        </button>
        <div class="widget widget_search">
            <form method="get" class="searchform search-form form-inline" action="./">
                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form>
        </div>
    </div>

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
        <ul class="list-unstyled">
            <li>Message To User</li>
        </ul>
        -->

        </div>
    </div>
    <!-- eof .modal -->

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="admin_contact_modal">
        <!-- <div class="ls with_padding"> -->
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="with_padding contact-form" method="post" action="./">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Contact Admin</h3>
                            <div class="contact-form-name">
                                <label for="name">Full Name
                                    <span class="required">*</span>
                                </label>
                                <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="contact-form-subject">
                                <label for="subject">Subject
                                    <span class="required">*</span>
                                </label>
                                <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
                            </div>
                        </div>

                        <div class="col-sm-12">

                            <div class="contact-form-message">
                                <label for="message">Message</label>
                                <textarea aria-required="true" rows="6" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-12 text-center">
                            <div class="contact-form-submit">
                                <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button wide_button color1">Send Message</button>
                                <button type="reset" id="contact_form_reset" name="contact_reset" class="theme_button wide_button">Clear Form</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- eof .modal -->

    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->
            <section class="ls section_padding_top_100 section_padding_bottom_100 section_full_height">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 to_animate">
                            <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>assets/images/logo.png"></a>
                            <div class="with_padding">

                                <h4 class="text-center">
                                    Pet Owner
                                </h4>
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>

                                <hr class="bottommargin_30">
                                <div class="wrap-forms">
                                    <form action="<?php echo base_url() ?>welcome/auth_proses" method="post">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group has-placeholder">
                                                    <label for="login-email">Email address</label>
                                                    <i class="grey fa fa-envelope-o"></i>
                                                    <input type="email" class="form-control" id="login-email" name="user_email" placeholder="Email Address" required>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group has-placeholder">
                                                    <label for="login-password">Password</label>
                                                    <i class="grey fa fa-pencil-square-o"></i>
                                                    <input type="password" class="form-control" name="user_passnya" id="login-password" placeholder="Password" required>
                                                    <input type="hidden" name="clinic" value="0">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="checkbox">
                                                    <input type="checkbox" id="remember_me_checkbox">
                                                    <label for="remember_me_checkbox">Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="theme_button block_button color1">Log In</button><br>
                                       <!-- <a href="<?php echo base_url() ?>register" class="theme_button block_button color3">Register as Pet Owner</a> -->
                                    </form>
                                </div>

                                <div class="darklinks text-center topmargin_20">

                                    <a role="button" data-toggle="collapse" href="#signin-resend-password" aria-expanded="false" aria-controls="signin-resend-password">
                                        Forgot your password?
                                    </a>

                                </div>
                                <div class="collapse form-inline-button" id="signin-resend-password">
                                    <form class="form-inline topmargin_20" action="<?php echo base_url() ?>login/sendreset" method="post">
                                        <div class="form-group">
                                            <label class="sr-only">Enter your e-mail</label>
                                            Enter your email address to reset your password.
                                            <input type="email" name="email" class="form-control" placeholder="Email Address">
                                            <input type="hidden" name="clinic" value="0">
                                        </div>
                                        <button type="submit" class="theme_button with_icon" style="margin: 24px 0px 0px;">
                                            <i class="fa fa-share"></i>
                                        </button>
                                    </form>
                                </div>


                            </div>
                            <!-- .with_border -->

                            <p class="divider_20 text-center">
                    Not registered ? <a href="<?php echo base_url() ?>register/">Register as Pet Owner</a>.<br>
                    or go back to <a href="<?php echo base_url() ?>">Home</a>
                </p>

                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->


    


    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar -->
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -->
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>

</body>

</html>