<body>
    <!--<div role="main" class="main">-->
        <div>
		<section class="section bg-color-light border-0 m-0 p-0" >
			<div class="row">
						<div class="col-12 mb-0">
							<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/joinpet.jpg" style="width: 100%">
						</div>
			</div>
			<div class="container">
					<div class="row">
						<div class="col-12 col-md-6 mb-5 mb-0 ml-0 mr-0 p-0">
							<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/whyus.jpg" style="width: 100%; margin-bottom: -23.5px;"><br />
							<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/product.jpg" style="width: 100%">
							
						</div>
						<div class="col-12 col-md-6 mb-0 mb-lg-0 ml-0 mr-0 p-0">
							<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/service.jpg" style="width: 100%"><br />
							<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/listpet.jpg" style="width: 100%">
						</div>
					</div>
						<!-- <div class="col-md-4 col-lg-4 col-12 mb-5 mb-md-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/listpet.jpg">
								<!-- <div class="circle c02"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico010.png"></div>
									<h4 style="text-align:center;"><br />PET&#39;S MEDBOOKS<br /><br /></h4>
								<div class="feature-box-info pl-0 pr-3">

								<p class="disrupt d000">Share your beloved pet's medical timeline on vaccinations, dietary requirements, and medications to your trusted Vet in our Network.</p>

								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4 col-12 mb-5 mb-lg-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/whyus.jpg">
								<!-- <div class="circle c03"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico011.png"></div>
									<h4 style="text-align:center;">CONSULTATION,<br/>APPOINTMENT<br/>&amp;REMINDER</h4>
								<div class="feature-box-info pl-0 pr-3">

								<p class="disrupt d000">Talk to our Vet directly in our App and schedule an appointment for a general check-up, vaccination, or other Vet Services with an auto-reminder feature.</p>
								</div> 
							</div>
						</div>
					</div>
					<div class="row d-flex justify-content-center">
						<div class="col-md-4 col-lg-4 col-12 mb-5 mb-lg-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/service.jpg">
								<!-- <div class="circle c04"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico012.png"></div>
								<h4 style="text-align:center;">ONLINE PET PHARMACY<br/>MARKET PLACE</h4>
								<div class="feature-box-info pl-0 pr-3">
									<p class="disrupt d000">Browse availability for pet medications and supplements through a comprehensive market place.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4 col-12 mb-5 mb-lg-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/arevet/product.jpg">
								<!-- <div class="circle c05"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico013.png"></div>
									<h4 style="text-align:center;">INVOICING RECORDS<br/>&amp; PAYMENT GATEWAY</h4>
								<div class="feature-box-info pl-0 pr-3">
									<p class="disrupt d000">Automatic storage of all invoices done at the Vet Clinics and Pharmacy for safe-keeping.</p>
								</div>
							</div>
						</div> -->
					</div>
				</div>

			</section>
			<!--end copy-->
			<!--

			Copy from here
			For partner

			-->
			<section class="section bg-color-light border-0 m-0 p-0" style="">
				<div class="container m-0" style="max-width: 100%; padding: 50px 0px 50px; background-color: #1c2023;">
					<!-- <div class="row">
						<div class="col-md-4 col-lg-4 col-12 mb-5 mb-lg-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<div class="circle c01"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico014.png"></div>
								<h4 style="text-align:center;">Vet Clinical Solution Management</h4>
								<div class="feature-box-info pl-0 pr-3">
									<p class="disrupt d000">Get our safe &amp; secure online tools to handle day-to-day clinic administrative processes in managing client's medical records, consultations, appointments &amp; reminders, stock &amp; inventory, and invoicing &amp; payments that can be accessible at anytime and anywhere.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4 col-12 mb-5 mb-lg-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<div class="circle c02"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico015.png"></div>
								<h4 style="text-align:center;">Advanced Analytics and Diagnostic</h4>
								<div class="feature-box-info pl-0 pr-3">
									<p class="disrupt d000">Create a holistic overview on each client's condition through our analytics dashboard, providing a more efficient and accurate information, diagnosis, and treatment for better customer service and comprehensive research &amp; development to create a healthier pet community.</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4 col-12 mb-5 mb-lg-0">
							<div style="text-align:center;" class="feature-box feature-box-style-5 custom-feature-box-1 flex-column appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
								<div class="circle c03"><img src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ico016.png"></div>
								<h4 style="text-align:center;">Value-Added Services</h4>
								<div class="feature-box-info pl-0 pr-3">
									<p class="disrupt d000">Gain more clients through our comprehensive Public Vet Network and become a trusted agent to your clients by connecting their beloved pets back to your clients should there be any lost &amp; found cases, using our Microchip &amp; Database identification.</p>
								</div>
							</div>
						</div>						
					</div> -->

					<div class="row text-center" style="">
							<div class="col">
								<a href="/register/vets" class="btn btn-tertiary font-weight-semibold custom-btn-style-1 text-4 py-3 px-5">JOIN NOW</a>
							</div>
						</div>

				</div>

			</section>

