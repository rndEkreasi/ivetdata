
				<section class="section bg-color-quaternary custom-padding-3 border-0 my-0">
					<div class="container">
						<div class="row mb-3">
							<div class="col">
								
								<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal appear-animation" data-appear-animation="fadeInUpShorter">COMPANY HISTORY</h2>
								<div class="row align-items-center">
									<div class="col-lg-3 mb-4 mb-lg-0">
										<div class="tabs tabs-vertical tabs-left tabs-navigation custom-tabs-navigation-1 border-0 mb-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
											<ul class="nav nav-tabs col-sm-3">
												<li class="nav-item mb-0 active">
													<a class="nav-link custom-primary-font text-center active" href="#tabsNavigation1" data-toggle="tab">2018</a>
												</li>
												<li class="nav-item mb-0">
													<a class="nav-link custom-primary-font text-center" href="#tabsNavigation3" data-toggle="tab">2019</a>
												</li>
												<!--<li class="nav-item mb-0">
													<a class="nav-link custom-primary-font text-center" href="#tabsNavigation4" data-toggle="tab">Feb-2019</a>
												</li>
												<li class="nav-item mb-0">
													<a class="nav-link custom-primary-font text-center" href="#tabsNavigation5" data-toggle="tab">Mar-2019</a>
												</li>
												<li class="nav-item mb-0">
													<a class="nav-link custom-primary-font text-center" href="#tabsNavigation6" data-toggle="tab">Apr-2019</a>
												</li>
												<li class="nav-item mb-0">
													<a class="nav-link custom-primary-font text-center" href="#tabsNavigation7" data-toggle="tab">May-2019</a>
												</li>-->
											</ul>
										</div>
									</div>
									<div class="col-lg-9">
										<div class="tab-pane tab-pane-navigation custom-tab-pane-navigation-1 active" id="tabsNavigation1">
											<div class="row align-items-center">
												<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0" style="text-align:center;">
													<img src="new/images/history-2018.jpg" class="img-fluid appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300" width="100%" />
												</div>
												<div class="col-lg-6 col-xl-7 pl-lg-5 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
													<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Company Established</h3>
													<p class="mb-0">Pet Identification Regulations are trending across SE Asia with new Bill Passed in Jakarta (Indonesia) and Makati City (Philippines) both in 2016. Thailand made Pets Registration mandatory in April 2018  while “Dog National ID” is released in Jakarta in Oct 2018. iVetData is commenced in Aug 2018 to develop the First Public Pet Database in Indonesia. iVetData platform went LIVE after internal development.</p>
												</div>
											</div>
										</div>
										<div class="tab-pane tab-pane-navigation" id="tabsNavigation3">
											<div class="row align-items-center">
												<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0" style="text-align:center;">
													<img src="new/images/history-2019.jpg" class="img-fluid" width="100%" />
												</div>
												<div class="col-lg-6 col-xl-7 pl-lg-5">
													<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Vet App Go Live</h3>
													<p class="mb-0">iVetData has signed an exclusive collaboration with the Indonesian Veterinary Medical Association (PDHI) and Datamars in early 2019. With dedication to the Vet Industry, iVetData was selected as a Speaker in BVSE 2019 Vet Seminar and one of Participants in Royal Canin Symposiums both in Jakarta and Surabaya. In June 2019, iVetData received an Award from PDHI as Partner that help them achieve Industry 4.0 goals.</p>
												</div>
											</div>
										</div>
										<!--<div class="tab-pane tab-pane-navigation" id="tabsNavigation4">
											<div class="row align-items-center">
												<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0" style="text-align:center;">
													<img src="https://ivetdata.s3.ap-southeast-1.amazonaws.com/d9c779b68a6101f1eb7ce33c6d3daaaa.jpeg" class="img-fluid" width="100%" />
												</div>
												<div class="col-lg-6 col-xl-7 pl-lg-5">
													<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Partnership with PDHI and DATAMARS established</h3>
													<p class="mb-0">We have signed exclusive collaboration contracts with the Indonesian Veterinary Medical Association (PDHI) and Datamars on Pet Database and Microchip sales.</p>
												</div>
											</div>
										</div>
										<div class="tab-pane tab-pane-navigation" id="tabsNavigation5">
											<div class="row align-items-center">
												<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0" style="text-align:center;">
													<img src="new/images/history-5.jpg" class="img-fluid" width="100%" />
												</div>
												<div class="col-lg-6 col-xl-7 pl-lg-5">
													<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Reached 100 vets</h3>
													<p class="mb-0">From collaboration with PDHI and participation in Bekasi Vet Expo, our recognition in the Vet community has just begun as we achieved 100 users. We were invited as a Speaker in BVSE 2019 Vet Seminar.</p>
												</div>
											</div>
										</div>
										<div class="tab-pane tab-pane-navigation" id="tabsNavigation6">
											<div class="row align-items-center">
												<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0" style="text-align:center;">
													<img src="new/images/history-6.jpg" class="img-fluid" width="100%" />
												</div>
												<div class="col-lg-6 col-xl-7 pl-lg-5">
													<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Partnership with ROYAL CANIN established</h3>
													<p class="mb-0">We have signed a collaboration contract with Royal Caninto promote Digitalization in the Vet Industry. We participated in Royal CaninSymposiums both in Jakarta and Surabaya. From 10 DoorPrizeWinners, 5 chose our product.</p>
												</div>
											</div>
										</div>
										<div class="tab-pane tab-pane-navigation" id="tabsNavigation7">
											<div class="row align-items-center">
												<div class="col-lg-6 col-xl-5 mb-4 mb-lg-0"style="text-align:center;">
													<img src="new/images/history-2.jpg" class="img-fluid" width="100%" />
												</div>
												<div class="col-lg-6 col-xl-7 pl-lg-5">
													<h3 class="font-weight-semibold text-transform-none custom-fontsize-2">Pet owner app & vet atlas beta released 200 + microchips sold</h3>
													<p class="mb-0">We have released Pet Owner App that connects to their Pets’ Medical Record and Invoices from their Vets.</p>
												</div>
											</div>
										</div>-->
										
									</div>
								</div>


							</div>
						</div>
					</div>
				</section>