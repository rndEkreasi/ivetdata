        <div class="pages">
          <div data-page="dashboard-owner-mypets-log-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <?php foreach ($datalog as $log) { ?>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="#" onclick="window.location='<?php echo base_url() ?>pet/detail/?idpet=<?php echo $_GET['idpet'] ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title"><?php echo $namepet ?> [<?php echo date('d M Y',strtotime($log->tgllog)); ?>]</h2>
                <div class="page_single layout_fullwidth_padding">
                  <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                  <h3 class="page_subtitle">Medical History Detail</h3>
                  <table class="custom_table mb-3">
                    <thead>
                      <tr>
                        <th>DESCRIPTION</th>
                        <th>VALUE</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Date</td>
                        <td><?php echo date('d M Y',strtotime($log->tgllog)); ?></td>
                      </tr>
                      <tr>
                        <td>Vet</td>
                        <td><?php echo $log->namevets; ?></td>
                      </tr>
                      <tr>
                        <td>Diagnose</td>
                        <td><?php echo str_replace(",",", ",$log->diagnose); ?></td>
                      </tr>
                      <tr>
                        <td>Treatment</td>
                        <td><ul><?php if($log->treatment<>""){$value = explode("<br>",$log->treatment);foreach($value as $val){ ?>
                                                        <li><?php if(trim($val)<>"")echo $val; ?></li>
                            <?php } } ?></ul></td>
                      </tr>
                      <tr>
                        <td>Anamnesis</td>
                        <td><?php echo $log->anamnesis; ?></td>
                      </tr>
                      </table>
                      <?php $show = FALSE ?>
                      <div id="showbasic">
                      <table class="custom_table mb-3"><thead>
                      <tr>
                                                        <td colspan="2"><strong>Basic Checkup</strong></td>
                                                    </tr></thead><tbody>
                                                    <?php
                                                    $weight = $log->weight;
                                                    if($weight == ''){ ?>
                                                    <?php }else{ $show=TRUE;?>
                                                    <tr>
                                                        <td>Weight </td>
                                                        <td><?php echo $log->weight; ?> Kg</td>
                                                    </tr>
                                                    <?php }?>
                                                    <?php
                                                    $temperature = $log->temperature;
                                                    if($temperature == ''){ ?>
                                                    <?php }else{ $show=TRUE;?>
                                                    <tr>
                                                        <td>Temperature </td>
                                                        <td><?php echo $log->temperature; ?> ' Celcius</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $heartrate = $log->heartrate;
                                                    if($heartrate == ''){ ?>
                                                    <?php }else{ $show=TRUE;?>
                                                    <tr>
                                                        <td>Heart Rate </td>
                                                        <td><?php echo $log->heartrate; ?> beats/minute</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $resprate= $log->resprate;
                                                    if($resprate == ''){ ?>
                                                    <?php }else{ $show=TRUE;?>
                                                    <tr>
                                                        <td>Respiration Rate </td>
                                                        <td><?php echo $log->resprate; ?> breaths/minute</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $bcs = $log->bcs;
                                                    if($bcs == ''){ ?>

                                                    <?php }else{ $show=TRUE;?>
                                                    <tr>
                                                        <td>BCS </td>
                                                        <td><?php echo $log->bcs; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $food= $log->food;
                                                    if($food == ''){ ?>
                                                    <?php }else{ $show=TRUE;?>
                                                    <tr>
                                                        <td>Food </td>
                                                        <td><?php echo $log->food; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($role > '0'){ ?>
                                                    <tr>
                                                        <td colspan="2"><strong>Physical Exam</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Eyes and Ears </td>
                                                        <td><?php echo $log->eyes; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Skin and Coat </td>
                                                        <td><?php echo $log->skin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Respiratory System </td>
                                                        <td><?php echo $log->resp; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Circulation System </td>
                                                        <td><?php echo $log->circ; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Digestive System </td>
                                                        <td><?php echo $log->dige; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Urinary System </td>
                                                        <td><?php echo $log->urin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nervous System </td>
                                                        <td><?php echo $log->nerv; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Musculoskeletal System </td>
                                                        <td><?php echo $log->musc; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Other notes </td>
                                                        <td><?php echo $log->othernotes; ?></td>
                                                    </tr>
                                                    <?php } ?></tbody>
                                                    </table></div>
                                                   <?php if($show==FALSE){ ?><script>document.getElementById('showbasic').style.visibility='hidden';</script><?php } ?>
                                                    <div id="showdiag">
                                                    <table class="custom_table mb-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="2"><strong>Diagnostic Tools Result</strong></th>
                                                    </tr></thead><tbody>
                                                    <?php 
                                                            $show=false;
                                                            $hema = $log->hema;
                                                            $jmlhema = strlen($hema);
                                                            if($jmlhema < 50){
                                                                $urlhema = base_url().'upload/pet/medrec/'.$hema;
                                                            }else{
                                                                $show=TRUE;
                                                                $urlhema = $hema; 
                                                            }
                                                            $blood = $log->blood;
                                                            $jmlblood = strlen($blood);
                                                            if($jmlblood < 50){
                                                                $urlblood = base_url().'upload/pet/medrec/'.$blood;
                                                            }else{
                                                                $show=TRUE;
                                                                $urlblood = $blood; 
                                                            }
                                                         $ultrasono = $log->ultrasono;
                                                            $jmlultrasono = strlen($ultrasono);
                                                            if($jmlultrasono < 50){
                                                                $urlultrasono = base_url().'upload/pet/medrec/'.$ultrasono;
                                                            }else{
                                                                $show=TRUE;
                                                                $urlultrasono = $ultrasono; 
                                                            }
                                                            $xray = $log->xray;
                                                            $jmlxray = strlen($xray);
                                                            if($jmlxray < 50){
                                                                $urlxray = base_url().'upload/pet/medrec/'.$xray;
                                                            }else{
                                                                $show=TRUE;
                                                                $urlxray = $xray; 
                                                            }
                                                            $otherdiag = $log->otherdiag;
                                                            $jmlotherdiag = strlen($otherdiag);
                                                            if($jmlotherdiag < 50){
                                                                $urlotherdiag = base_url().'upload/pet/medrec/'.$otherdiag;
                                                            }else{
                                                                $show=TRUE;
                                                                $urlotherdiag = $otherdiag; 
                                                            }
                                                        ?>
                                                    <?php if($hema==''&&$log->hemanotes==''){
                                                    }else{ $show=TRUE; ?>
                                                    <tr>
                                                        <td>Hematology </td>
                                                        <td><?php if($hema<>''){  ?>
                                                            <img src="<?php echo $urlhema ?>" style="max-width: 300px;" data-toggle="modal" data-target="#hemamodal">
                                                            <div class="modal fade" id="hemamodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Hematology</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlhema ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div><br><?php } ?>
                                                        <?php echo $log->hemanotes; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if($blood==''&&$log->bloodnotes==''){

                                                }else{ $show=TRUE; ?>
                                                    <tr>
                                                        <td>Blood Chemistry </td>
                                                        <td><?php if($blood<>''){ ?>
                                                            <img src="<?php echo $urlblood ?>" style="max-width: 300px;"  data-toggle="modal" data-target="#bloodmodal">
                                                            <div class="modal fade" id="bloodmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Blood Chemistry</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlblood ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        <br><?php } ?>
                                                        <?php echo $log->bloodnotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($ultrasono==''&&$log->ultrasononotes==''){

                                                    }else{ $show=TRUE; ?>
                                                    <tr>
                                                        <td>Ultrasonography </td>
                                                        <td><?php if($ultrasono<>''){ ?>
                                                            <img src="<?php echo $urlultrasono ?>" style="max-width: 300px;" data-toggle="modal" data-target="#ultrasonopic">
                                                            <div class="modal fade" id="ultrasonopic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Ultrasonography</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlultrasono ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        <br><?php } ?>
                                                        <?php echo $log->ultrasononotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($xray==''&&$log->xraynotes==''){

                                                        }else{ $show=TRUE; ?>
                                                    <tr>
                                                        <td>X-ray </td>
                                                        <td><?php if($xray<>''){ ?>
                                                            <img src="<?php echo $urlxray ?>" style="max-width: 300px;" data-toggle="modal" data-target="#xraypic">
                                                        <!-- Modal -->
                                                                <div class="modal fade" id="xraypic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">X-ray</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlxray ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        <br><?php } ?>
                                                        <?php echo $log->xraynotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($otherdiag == ''){

                                                    }else{ $show=TRUE; ?>
                                                    <tr>
                                                        <td>Other Diagnostic </td>
                                                        <td>
                                                                <img src="<?php echo $urlotherdiag ?>" style="max-width: 300px;" data-toggle="modal" data-target="#otherpic">
                                                                <!-- Modal -->
                                                                <div class="modal fade" id="otherpic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Other Diagnostic</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlotherdiag ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                            <br>
                                                            <?php echo $log->otherdiagnotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                    </tbody>
                  </table></div>
                  <?php if($show==FALSE){ ?><script>document.getElementById('showdiag').style.visibility='hidden';</script><?php } ?>
                </div>
              </div>
              <?php } ?>
             