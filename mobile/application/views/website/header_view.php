<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <title><?php echo $title ?></title>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="One-Stop Solution for your Companion Animals!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="https://ivetdata.com/assets/images/icon.png"/>
    <link rel="apple-touch-icon" href="/assets/images/ivet.png">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css" class="color-switcher-link">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131768019-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131768019-1');
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>



<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c792599a726ff2eea5a1b14/default';;
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!--End of Tawk.to Script-->

    <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <![endif]-->

</head>

<body>
    <!--[if lt IE 9]>
        <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="rt-icon2-cross2"></i>
            </span>
        </button>
        <div class="widget widget_search">
            <form method="get" class="searchform search-form form-inline" action="./">
                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form>
        </div>
    </div>

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
        <ul class="list-unstyled">
            <li>Message To User</li>
        </ul>
        -->

        </div>
    </div>
    <!-- eof .modal -->

    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->

            <section class="page_topline with_search ls ms section_padding_10 table_section">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 text-center text-sm-left header_top">
                            <!-- <div class="col-sm-6 page_social_icons greylinks" style="width: 180px;float: left;">
                                    <a class="social-icon rounded-icon border-icon soc-facebook" href="https://www.facebook.com/ivetdata/" target="_new" title="Facebook iVetData"></a>
                                    <a class="social-icon rounded-icon border-icon soc-twitter" href="https://twitter.com/ivetdata" target="_new" title="Twitter iVetData"></a>
                                    <a class="social-icon rounded-icon border-icon soc-instagram" href="https://www.instagram.com/ivetdata/" target="_new" title="IG iVetData"></a>
                            </div> -->
                            <!-- <div style="font-size: 10px;font-family: arial;margin: 0px 0px -51px;display: inline-block;color: #000;padding: 0px 53px;">In association with</div> -->
                            <a href="https://pdhi.or.id" target="_new"><img src="<?php echo base_url() ?>assets/images/pdhi.png" style="width: 210px;margin: 0px 0px 0px;"></a>
                            <!-- <a href="https://datamars.com" target="_new"><img src="<?php echo base_url() ?>assets/images/logo_datamars.png" style="width: 108px;margin: 5px 6px 5px;"></a> -->

                            
                        </div>
                        <div class="col-sm-7 text-center text-sm-right">
                            <?php if (isset($this->session->userdata['logged_in'])) { ?>
                                <a href="<?php echo base_url() ?>dashboard" class="theme_button color3">Dashboard</a>
                                <a href="<?php echo base_url() ?>welcome/logout" class="theme_button color1">Log Out</a>
                            <?php }else{ ?>
                                <a href="<?php echo base_url() ?>login/owner" class="theme_button color3">Pet Owner</a>
                                <a href="<?php echo base_url() ?>login" class="theme_button color1">Vet / Clinic Login</a>
                               <!--  <a href="https://app.ivetdata.com/login/owner" class="theme_button color3">Pet Owner</a> 
                                <a href="https://app.ivetdata.com/login" class="theme_button color1">Vet / Clinic</a> -->
							<?php } ?>
						</div>
						<div class="col-sm-1 text-center text-sm-right" style="white-space:nowrap;">&nbsp;
							<!-- <a href="<?php echo base_url() ?>">ENG</a> | <a href="<?php echo base_url() ?>?lang=id">ID</a> --.
                            <!-- <div id="google_translate_element" class="col-sm-6" style="width: 300px;height: 41px;overflow: hidden;margin: 0px auto -21px;"></div> -->
                            <!-- <div class="widget widget_search">
                                <form method="get" class="searchform form-inline" action="./">
                                    <div class="form-group-wrap">
                                        <div class="form-group margin_0">
                                            <label class="sr-only" for="topline-search">Search for:</label>
                                            <input id="topline-search" type="text" value="" name="search" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="theme_button">Search</button>
                                    </div>
                                </form>
                            </div> -->

                        </div>
                    </div>
                </div>
            </section>

            <section class="page_toplogo table_section table_section_md ls section_padding_top_15 section_padding_bottom_15">
				<header class="page_header header_white toggler_left with_top_border item_with_border">
					<div class="header_mainmenu display_table_cell" style="margin: -70px 0px 17px;">
						<!-- main nav start -->
						<div class="container header_container">
						<nav class="mainmenu_wrapper row">
							<div class="col-sm-4 text-md-center text-sm-left text-md-left header_column ">
								<a href="<?php echo base_url() ?>" class="logo top_logo">
									<img src="<?php echo base_url() ?>assets/images/logo.png" alt="">
								</a>                            
							</div>
							<div class="col-md-8 text-center text-md-right header_column">
								<ul class="mainmenu nav sf-menu">
									<li class="logo_sf-menu">
										<a href="<?php echo base_url() ?>" class="mobile searchmob">
												<!-- IVETDATA -->
												<img src="<?php echo base_url() ?>assets/images/logo-white.png" class="mobile" style="max-width: 90%;">
										</a>
									</li>
									<!--<li class="mobile searchmob">
										<form class="form-inline mobile" method="get" action="<?php echo base_url() ?>pet/search/">
											<div class="form-group mobile" style="width: 100% !important;">
												<label class="sr-only mobile" for="exampleInputEmail3">Pet ID</label>
												<input type="text" style="width: 96% !important;background: #fff;margin: 0px 0px 0px 8px;" class="form-control mobile" name="idpet" id="exampleInputEmail3" placeholder="Microchip ID">
												<button type="submit" class="theme_button color1 mobile" style="height: 50px; line-height: 0px !important;">Find</button>
											</div>  
										</form>
									</li>-->
									<!--<h3 class="dark_bg_color mobile" style="width: 130%;margin:24px -10px 8px;padding: 19px;font-size: 24px;">Menu</h3>-->
									<?php if(isset($page)){
										$page = $page;
									}else{
										$page = 'home';
									} ?>
									<li <?php if ($page == 'home') {?> class="active" <?php } ?> >
										<a href="<?php echo base_url() ?>">
										<i class="fa fa-th-large mobile"></i>Home</a>                                            
									</li>

									<li <?php if ($page == 'about_us') {?> class="active" <?php } ?> >
										<a href="<?php echo base_url() ?>about_us"><i class="fa fa-th-large mobile"></i>About Us</a>     
										 <!-- <ul>
											<li>
												<a href="<?php echo base_url() ?>#service">Services</a>                                            
											</li> -->
											<!-- <li>
												<a href="<?php echo base_url() ?>about_us">Vision & Mission</a>                                            
											</li> -->
										   <!--  <li>
												<a href="<?php echo base_url() ?>">FAQ's</a>                                            
											</li> -->
											<!-- <li>
												<a href="<?php echo base_url() ?>pricing">Pricing</a>                                            
											</li>
										</ul>  -->                                      
									</li>
									<li <?php if ($page == 'news') {?> class="active" <?php } ?> >
										<a href="<?php echo base_url() ?>blog/all"><i class="fa fa-th-large mobile"></i>News & Article</a>                                            
									</li>
									<!-- <li>
										<a href="https://drive.google.com/file/d/1yYl7zkuuwxkr0vEVlZvC5bTwyScOwq1a/view" target="_new">Shop</a>
									</li> -->

									<li <?php if ($page == 'vets') {?> class="active" <?php } ?> >
										<a href="<?php echo base_url() ?>vets"><i class="fa fa-th-large mobile"></i>Vet Search</a>                                            
									</li>  
									<!-- <li>
										<a href="<?php echo base_url() ?>contact/vetoncall"><i class="fa fa-th-large mobile"></i>Vet On Call</a>
									</li>  -->
									<li <?php if ($page == 'contact') {?> class="active" <?php } ?> >
										<a href="<?php echo base_url() ?>contact"><i class="fa fa-th-large mobile"></i>Contact Us</a>                                            
									</li>
									<!-- <img src="<?php echo base_url() ?>assets/images/giraffe.png" class="mobile"> -->
								</ul>
							</div>		
						</nav>
						</div>
						<!-- eof main nav -->
						<!-- header toggler -->
						<span class="toggle_menu">
							<span></span>
						</span>
					</div>

					<!--
					<div class="header_right_buttons display_table_cell text-right">
						<div class="col-sm-8 text-center text-sm-left">
							<div class="col-sm-6 page_social_icons greylinks" style="width: 180px;float: left;">
									<a class="social-icon rounded-icon border-icon soc-facebook" href="https://www.facebook.com/ivetdata/" target="_new" title="Facebook iVetData"></a>
									<a class="social-icon rounded-icon border-icon soc-twitter" href="https://twitter.com/ivetdata" target="_new" title="Twitter iVetData"></a>
									<a class="social-icon rounded-icon border-icon soc-instagram" href="https://www.instagram.com/ivetdata/" target="_new" title="IG iVetData"></a>
							</div>
					-->
							<!-- <div id="google_translate_element" class="col-sm-6" style="width: 162px;height: 51px;overflow: hidden;"></div> -->
					<!--
					</div>

					</div>
					-->		
				</header>
							<!-- <div class="inline-teasers-wrap" style="width: 300px;margin: 0px 0px 0px 0px;">
					<div class="small-teaser text-left">
						<form class="form-inline" method="get" action="<?php echo base_url() ?>pet/search/">
							<div class="form-group" style="width: 100% !important;">
								<label class="sr-only" for="exampleInputEmail3">Pet ID</label>
								<input type="text" style="width: 300px !important;" class="form-control" name="idpet" id="exampleInputEmail3" placeholder="PET ID">
								<button type="submit" class="theme_button color1">Find</button>
							</div>  
						</form>

					</div>
				</div> -->

			<!-- <hr class="mobile"> -->
            </section>

            