  <?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,%0A+Mountain+View,+CA&key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 9c419fc6-508e-42c8-b1d8-c24ab2b7349f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
?>
<section class="ls page_portfolio section_padding_bottom_75">
<div class="col-sm-12 text-center">
	<h2 class="section_header with_icon icon_color">Vet Search</h2>
</div>
</section>
<div id="map" style="height: 100%;min-height:250px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
      //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;
        
        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var url = markerElem.getAttribute('url');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>
</section>
<section class="ls page_portfolio section_padding_top_50 section_padding_bottom_75">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h4>Vets and Clinic List</h4>

							<div class="isotope_container isotope row masonry-layout columns_margin_bottom_20" style="position: relative; height: 100%;">

								<?php 
                if(count($searchvet) == '0'){ ?>
                  <h4>Not Found</h4>

                <?php }else{
                  foreach ($searchvet as $detail) { ?>

    								<div class="isotope-item clinic col-lg-4 col-md-6 col-sm-12">

    									<!-- <article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden" style="height:255px;">
    										<div class="item-media">
                          <a href="<?php echo base_url() ?>vets/detail/?vets=<?php echo $detail->idclinic ?>">
    											<img src="<?php echo base_url() ?>assets/images/petshop.png" alt=""> 
                        </a>
    										</div>-->
    										<div class="item-content">
    											
    											<h4 class="entry-title">
    												<a href="<?php echo base_url() ?>vets/detail/?vets=<?php echo $detail->idclinic ?>"><?php echo $detail->nameclinic; ?></a>
    											</h4>
    											<p class="margin_0">
    												<!-- Clients can simply schedule their hard drive destruction online and through our website. -->
    												<?php 
    												//$descibe = strip_tags($detail->description);
    												echo $detail->address;
    												//echo substr($descibe, 0, 100); ?>
    												<?php echo $detail->city; ?><br>
    												<?php echo $detail->phone; ?><br>
    											</p>
    											<!-- <a href="<?php echo base_url() ?>blog/detail/<?php /// echo $detail->slug; ?>" class="read-more"></a> -->
    										</div>
    									</article>

    								</div>

								<?php }
                } ?>

							</div>
							<!-- eof .isotope_container.row -->

							<!-- <div class="row">
								<div class="col-sm-12 text-center">
									<?php // echo $this->pagination->create_links(); ?>
									<!-- <img src="<?php echo base_url() ?>assets/img/loading.png" alt="" class="fa-spin"> --
								</div>
							</div> -->

						</div>
					</div>
				</div>
			</section>
        <footer class="page_footer ds parallax section_padding_top_100 section_padding_bottom_65 columns_padding_25">
               
            </footer>

            <section class="ls page_copyright section_padding_15">
                <div class="container">
                   
					<!--<div class="row">
						<div class="col-sm-12 col-md-2 col-lg-3 empty"></div>
						<div class="col-sm-12 col-md-8 col-lg-6 text-center">
							<ul class="footer_nav">
								<li><a href="<?php echo base_url() ?>termsconditions.html">Terms &amp; Conditions</a></li>
								<li>|</li>
								<li><a href="">FAQ</a></li>
								<li>|</li>
								<li><a href="<?php echo base_url() ?>contact">Contact</a></li>
								<li>|</li>
								<li><a href="https://play.google.com/store/apps/details?id=io.gonative.android.ppnyxd"">Download our app</a></li>
							</ul>    
						</div>-->
						<div class="col-sm-12 col-md-2 col-lg-3 empty"></div>
					</div> 
					<div class="row">
                        <div class="col-sm-12 text-center">
                            <p>&copy; Copyright <?php echo date('Y') ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>


</body>

</html>