        <div class="pages">
          <div data-page="dashboard-owner-appointments" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';">
                      <img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" onclick="history.back(1);" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Appointment</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <?php // var_dump($listvet); ?>
                    <form action="<?php echo base_url() ?>appointment/editprocess" method="post">
                      <div class="form_row required">
                        <label>Pet Name:</label>
                        <input type="text" name="petname" value="<?php echo $dataappoint[0]->namapet ?>" class="form_input" disabled />
                      </div>
                      <div class="form_row required">
                        <label>Vet Name:</label>
                        <select name="idvet" class="form_select">
                            <option value="<?php echo $dataappoint[0]->uid ?>"><?php echo $dataappoint[0]->name ?></option>
                            <?php
                            foreach ($listvet as $vet) { ?>
                            <option value="<?php echo $vet->uid ?>"><?php echo $vet->name ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form_row required">
                        <label>Appointment Date:</label>
                        <div class="datetimepicker_contain  mb-3">
                          <input type="date" name="nextdate" class="datetimepicker-input" id="datetimepicker-doad" placeholder="Select Date.." value="<?php echo date('Y-m-d', strtotime($dataappoint[0]->nextdate)) ?>">
                          &nbsp;Hour <select name="hour"><?php for($i=0;$i<24;$i++){ ?><option value="<?php if($i<10) echo "0" ?><?php echo $i ?>" <?php $hr=date('H', strtotime($dataappoint[0]->nextdate));if($hr==$i||$hr=="0".$i){ echo "selected";}?> >
                          <?php if($i<10) echo "0" ?><?php echo $i ?></option><?php } ?>
                          </select>
                          &nbsp;Minute <select name="minute"><?php for($i=0;$i<61;$i++){ ?><option value="<?php if($i<10) echo "0" ?><?php echo $i ?>" <?php $hr=date('i', strtotime($dataappoint[0]->nextdate));if($hr==$i||$hr=="0".$i){ echo "selected";}?> >
                              <?php if($i<10) echo "0" ?><?php echo $i ?></option><?php } ?></select>
                        </div>
                      </div>
                      <div class="form_row">
                        <label>Note:</label>
                        <textarea name="description" class="form_textarea" rows="" cols=""><?php echo $dataappoint[0]->description ?></textarea>
                        <input type="hidden" name="idvacc" value="<?php echo $dataappoint[0]->idvacc ?>">

                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Update Appointment" />
                    </form>
                  </div>
                </div>
              </div>
              <!-- <?php // include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div> -->