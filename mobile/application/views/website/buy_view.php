<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">Service & Medicine</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
            

            <section class="intro_section page_mainslider ls ms pricing py-5">  
    <div class="container">
        <div class="row">                               
                    <div class="col-md-12">
                      
                        <!-- tabs content start here -->
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <form action="<?php echo base_url() ?>dashboard/buyprocess" method="post">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Choose Your Monthly Subscription</h4>
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                                <div class="col">                            
                                <div id="accordion2">
                                    <?php foreach ($dataservice as $service){
                                        $idservice = $service->idservice;
                                        $status = $service->status;
                                        if($idservice=='2'){
                                            $pilih = 'checked="checked"';
                                            $active = 'show';
                                        }else{
                                            $pilih = '';
                                            $active='';
                                        }
                                     ?>

                                     <div class="col-lg-4">
                                        <div class="card mb-5 mb-lg-0" style="padding: 10px;">
                                          <div class="card-body">
                                            <!-- <input type="radio" class="form-control" <?php if ($status == '2') { echo 'disabled'; } ?> name="service" <?php echo $pilih ?> value="<?php echo $service->idservice;?>" style="float: left;width: 70px;padding: 0px;box-shadow: none;margin: -4px 0px 0px 0px;"> -->
                                            <h5 class="<?php if ($status == '1') { ?>card-title text-muted <?php } ?>text-uppercase text-center"><?php if ($status>1) { echo "<br />"; }?><?php echo $service->name ?></h5>
                                            <?php if ($status == '1') { ?><h6 class="card-price text-center">Rp. <?php echo number_format($service->price) ?><span class="period">/ <?php echo $service->month ?> month</span></h6><?php } ?>
                                            <hr>
                                            <ul class="fa-ul" style="text-align: left;">
                                              <?php echo $service->description ?>
                                            </ul>
                                            <div class="" style="text-align: center;padding: 10px 0px;">
                                                <?php if ($status == '1'){ ?>
                                                    <!-- <a href="#" class="icon-tab theme_button color2" style="width: 100%;"> Buy</a> -->
                                                    <a href="<?php echo base_url() ?>dashboard/buyselect/?idservice=<?php echo $service->idservice ?>" class="icon-tab theme_button color2" style="width: 100%;"> Buy</a>
                                                    <!-- <input type="submit" style="margin: 10px 0px;font-size: 20px;" class="btn gradient border-0 z-3" value="Buy">  -->
                                                <?php }else{ ?>
                                                    <a href="#" class="icon-tab theme_button" style="width: 100%;"> Coming Up Soon</a>
                                                <?php } ?>
                                                    
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <?php } ?>                                  
                                </div> 
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- <label>Discount Code : <span id="msg"></span></label>
                                        <input type="text" name="discount" id="discountcode" class="form-control" value=""> -->                                        
                                    </div>                                
                                    
                                </div>
                              </form>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                </div>
              

            </div>
</section>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#searchservice').autocomplete({
                source: "<?php echo site_url('service/allsearch/?');?>",
      
                select: function (event, ui) {
                    $('[name="nameservice"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });

            $('#discountcode').keyup(function(event) {
              // skip for arrow keys
                $('#msg').html('<span style="color: red;"><i class="fa fa-close"></i> Invalid</span>');
                $.ajax({
                       type: "POST",
                       url: "<?php echo base_url() ?>dashboard/cekcode",
                       dataType:'json',
                       cache : false,
                       data: {code: $('#discountcode').val()},
                       success: function(response) {
                        console.log(response); 
                        $('#msg').html('<span style="color: green;"><i class="fa fa-check"></i>Valid</span>');
                        //alert(response)

                        // if (response == true){
                        //     console.log(response);
                        //     $('#msg').html('<span style="color: green;">Valid</span>');
                        // }else {
                        //     console.log(response);
                        //     $('#msg').html('<span style="color:red;">Code Invalid</span>');
                        // }  
                    }
                });
            });
 
        });
    </script>


</body>

</html>