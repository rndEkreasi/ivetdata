<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="/dashboard">Dashboard</a>
                                </li>
                                <li class="active">Vet Search</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> Mon 22, Jul 2019</span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="ls section_padding_top_50 section_padding_left_50 section_padding_bottom_50 columns_padding_10" >
<div class="row">
            <div class="col-md-12">
                <h3>&nbsp;&nbsp;Vet / Clinic Search</h3>
            </div>
            <!-- .col-* -->                        
        </div></section>            
<?php if(isset($_GET['vets'])){ ?>
<div class="row">
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,%0A+Mountain+View,+CA&key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 9c419fc6-508e-42c8-b1d8-c24ab2b7349f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
?>
<!--<body>
        <div role="main" class="main">
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section>-->

    <div id="map" style="height: 100%;min-height:250px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
      //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;
        
        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              //var url = markerElem.getAttribute('url');
              var url = '?vets='+id;
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>
</div>

<section class="ls page_portfolio section_padding_top_50 section_padding_bottom_75">
				<div class="container">
					<div class="row">
                        <div class="col" align="center">
							<h4>Vets Detail</h4>

							<div class="isotope_container>

								<?php 
                                $detail = $this->Clinic_model->detailclinic($_GET['vets']);
								?>								
								

								<div class="isotope-item">
									<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
										<div>
										<?php if($detail[0]->picture==''){ ?>
											<img src="<?php echo base_url() ?>assets/images/petshop.png" alt="">
										<?php }else{ ?>
										    <img src="<?php echo $detail[0]->picture ?>assets/images/petshop.png" alt="">
										<?php } ?>
										</div>
										<div class="item-content">
											
											<h4 class="entry-title">
												<a href=""><?php  echo $detail[0]->nameclinic; ?></a>
											</h4>
											<p class="margin_0">
												<!-- Clients can simply schedule their hard drive destruction online and through our website. -->
												<?php echo $detail[0]->address; ?>,<br>
												<?php echo $detail[0]->city; ?><br>
												<?php echo $detail[0]->phone; ?><br>
											</p>
										</div>
									</article>

								</div>
							</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</section> 
<?php }else{ ?>	
	    <!-- Theme new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-elements.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-blog.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-shop.css">

		<!-- Current Page new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/settings.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/layers.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/navigation.css">
		
		<!-- Demo new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/demos/demo-insurance.css">

		<!-- Skin new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/skins/skin-insurance.css"> 

		<!-- Theme Custom new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/custom.css">

		<!-- Head Libs -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/modernizr/modernizr.min.js"></script>
			<section id="find" class="parallax section section-parallax custom-padding-3 my-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" data-image-src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/map.png">
					<div class="container">
						<div class="row justify-content-end">
							<div class="col-md-12 col-lg-7 col-xl-7">
								<div class="card bg-color-light border-0 appear-animation" data-appear-animation="fadeInUpShorter">
									<div class="card-body p-5">
										<h2 class="custom-primary-font font-weight-normal text-6 mb-3">FIND YOUR PET'S VET</h2>
										<p class="pb-2 mb-4">Click Search to find vets in our network</p>
										<form action="/vets/search" method="post">
											<div class="form-row">
												<div class="form-group col-md-4 pr-md-0">
													<input type="text" class="form-control custom-left-rounded-form-control" name="name" value="" placeholder="Find Pet ID" >
												</div>
												<div class="form-group col-md-4 pl-md-0 pr-md-0">
													<input type="text" class="form-control custom-middle-rounded-form-control" name="vet" value="" placeholder="Find Vet / Clinic" >
												</div>
												<div class="form-group col-md-4 pl-md-0">
													<input type="text" class="form-control custom-right-rounded-form-control" name="city" value="" placeholder="Find by City" >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col mb-0">
										            <p>If you found lost pet please enter their pet ID here to contact it's vet</p>
													<input type="submit" value="SEARCH" class="btn btn-secondary custom-btn-style-1 py-3 text-4 w-100">
												</div>
											</div>
										</form>
									</div>
									<div class="card-footer bg-color-primary border-0 custom-padding-2">
										<div class="row">
											<div class="col-md-6 mb-5 mb-md-0">
												<div class="feature-box align-items-center">
													<div class="feature-box-icon bg-color-tertiary">
														<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
													</div>
													<div class="feature-box-info">
														<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
														<a href="mailto:cs@ivetdata.com" class="text-color-light custom-fontsize-2">cs@ivetdata.com</a>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="feature-box align-items-center">
													<div class="feature-box-icon bg-color-tertiary">
														<i class="fas fa-phone text-3"></i>
													</div>
													<div class="feature-box-info">
														<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
														<a href="tel:+62-21-2977-951" class="text-color-light custom-fontsize-2">+62-21-2977-951</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				        <!-- new/vendor -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery/jquery.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/popper/umd/popper.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/common/common.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vide/jquery.vide.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vivus/vivus.min.js"></script>
        
        <!-- Theme Base, Components and Settings -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.js"></script>
		
		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.init.js"></script>
<?php } ?>
<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>


</body>

</html>