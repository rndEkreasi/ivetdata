        <div class="pages">
          <div data-page="dashboard-owner-invoice" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">SHOPPING CART</h2>
                <div class="page_single layout_fullwidth_padding">
                  <h4 class="checkout_title">YOUR DETAILS</h4>
                  <div class="contactform">
                    <form class="cmxform" id="ContactForm" method="post" action="">
                      <label>Name:</label>
                      <input type="text" name="ContactName" id="ContactName" value="" class="form_input required" />
                      <label>Email:</label>
                      <input type="text" name="ContactEmail" id="ContactEmail" value="" class="form_input required email" />
                      <label>Message:</label>
                      <textarea name="ContactComment" id="ContactComment" class="form_textarea textarea required" rows=""
                        cols=""></textarea>
                    </form>
                  </div>
                  <h4 class="checkout_title">ORDER DETAILS</h4>
                  <div class="order_item">
                    <div class="order_item_title"><span class="mr-1">4 X</span> Inpepsa</div>
                    <div class="order_item_price text-right">Rp. 75,000</div>
                  </div>
                  <div class="order_item">
                    <div class="order_item_title"><span class="mr-1">1 X</span> Prednisolone</div>
                    <div class="order_item_price text-right">Rp. 500,000</div>
                  </div>
                  <div class="order_item">
                    <div class="order_item_title"><span class="mr-1">1 X</span> Vanguard Plus 5/CV</div>
                    <div class="order_item_price text-right">Rp. 250,000</div>
                  </div>
                  <h4 class="checkout_title">SELECT PAYMENT</h4>
                  <div class="contactform">
                    <div class="checkout_select">
                      <label class="label-radio item-content">
                        <input type="radio" name="payment" value="Paypal" checked="checked">
                        <div class="item-inner">
                          <div class="item-title">Paypal</div>
                        </div>
                      </label>
                      <label class="label-radio item-content">
                        <input type="radio" name="payment" value="Card">
                        <div class="item-inner">
                          <div class="item-title">Credit Card</div>
                        </div>
                      </label>
                      <label class="label-radio item-content">
                        <input type="radio" name="payment" value="Pickup">
                        <div class="item-inner">
                          <div class="item-title">At pickup</div>
                        </div>
                      </label>
                    </div>
                  </div>
                  <h4 class="checkout_title">SELECT SHIPPING</h4>
                  <div class="contactform">
                    <div class="checkout_select">
                      <label class="label-radio item-content">
                        <input type="radio" name="my-radio" value="Post" checked="checked">
                        <div class="item-inner">
                          <div class="item-title">Standard Post <span>($5)</span></div>
                        </div>
                      </label>
                      <label class="label-radio item-content">
                        <input type="radio" name="my-radio" value="Courier">
                        <div class="item-inner">
                          <div class="item-title">Express Courier <span>($25)</span></div>
                        </div>
                      </label>
                      <label class="label-radio item-content">
                        <input type="radio" name="my-radio" value="store">
                        <div class="item-inner">
                          <div class="item-title">In store pickup <span>(FREE)</span></div>
                        </div>
                      </label>
                    </div>
                  </div>
                  <h4 class="checkout_title">TOTAL</h4>
                  <div class="carttotal_full">
                    <div class="carttotal_row_full">
                      <div class="carttotal_left">CART TOTAL</div>
                      <div class="carttotal_right">Rp. 1,050,000</div>
                    </div>
                    <div class="carttotal_row_full">
                      <div class="carttotal_left">VAT (15%)</div>
                      <div class="carttotal_right">Rp. 157,500‬</div>
                    </div>
                    <div class="carttotal_row_full">
                      <div class="carttotal_left">SHIPPING</div>
                      <div class="carttotal_right">Rp. 14,000</div>
                    </div>
                    <div class="carttotal_row_last">
                      <div class="carttotal_left">TOTAL</div>
                        <div class="carttotal_right">Rp. 1,221,500‬</div>
                    </div>
                  </div>
                  <a href="#" class="button_full btyellow" onclick="window.location='<?php echo base_url() ?>cart/success';">SEND ORDER</a>
                </div>
              </div>
