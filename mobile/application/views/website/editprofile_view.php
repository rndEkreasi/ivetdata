<!-- link for jquery style -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- link for bootstrap style -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="<?php echo base_url() ?>assets/js/country/assets/js/geodatasource-cr.min.js"></script>
    <link rel="stylesheet" href="assets/css/geodatasource-countryflag.css">

    <!-- link to all languages po files -->
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ar/LC_MESSAGES/ar.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/cs/LC_MESSAGES/cs.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/da/LC_MESSAGES/da.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/de/LC_MESSAGES/de.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/en/LC_MESSAGES/en.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/es/LC_MESSAGES/es.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/fr/LC_MESSAGES/fr.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/it/LC_MESSAGES/it.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ja/LC_MESSAGES/ja.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ko/LC_MESSAGES/ko.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ms/LC_MESSAGES/ms.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/nl/LC_MESSAGES/nl.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/pt/LC_MESSAGES/pt.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ru/LC_MESSAGES/ru.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/sv/LC_MESSAGES/sv.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/vi/LC_MESSAGES/vi.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-cn/LC_MESSAGES/zh-cn.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-tw/LC_MESSAGES/zh-tw.po" />

    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/country/assets/js/Gettext.js"></script>

<section class="ls with_bottom_border">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<ol class="breadcrumb darklinks">
					<li>
						<a href="<?php echo base_url() ?>dashboard">Dashboard</a>
					</li>
					<li class="active">Edit Profile</li>
				</ol>
			</div>
			<!-- .col-* -->
			<div class="col-md-6 text-md-right">
				 <!--<span> <?php echo date('D d, M Y');?></span>-->
			</div>
			<!-- .col-* -->
		</div>
		<!-- .row -->
	</div>
	<!-- .container -->
</section>

<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
	<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<h3>Edit My Profile</h3>
				</div>
			</div>
			<!-- .row -->
			<div class="row">
				<div class="col-xs-12">
					<?php if (isset($error)){ ?>
                                    	<div class="alert alert-danger"><?php echo $error; ?></div>
	                                <?php } ?>

	                                <?php if (isset($success)){ ?>
	                                    <div class="alert alert-success"><?php echo $success; ?></div>
	                                <?php } ?>
	                <div class="row">

						<div class="col-md-8">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li class="active">
									<a href="#tab4" role="tab" data-toggle="tab">
										<i class="rt-icon2-user"></i> My Profile</a>
								</li>
								<?php if ($manageto == '1'){ ?>
								<li>
									<a href="#tab5" role="tab" data-toggle="tab">
										<i class="fa fa-paw"></i> Clinic</a>
								</li>
								<?php } ?>
								<li>
									<a href="#tab6" role="tab" data-toggle="tab">
										<i class="rt-icon2-key"></i>Password</a>
								</li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content top-color-border">
								<div class="tab-pane fade in active" id="tab4">
									<h4>User's info</h4>
										<hr>
										<form class="form-horizontal" method="post" action="<?php echo base_url() ?>profile/updateinfo" enctype="multipart/form-data">
											<div class="row form-group">											
													<label class="col-lg-3 control-label" for="user-profile-avatar">Profile Picture</label>
													<div class="col-lg-9">
													    <img src="<?php echo $profilephoto; ?>" id="myImg" class="img-responsive" >
														<br /><br />
														<input type="file" id="inputimage" name="profilepic">
														<p class="help-block">Image resolution size must be 300px x 300px</p>
														<div style="visibility:hidden; margin-left:30px; margin-top:10px;" id="conbutton">
														<input type="image" border="1" src="/assets/images/rotate_right.png" onclick="rotateimg('myImg');return false;" />&nbsp;
														<input type="image" border="1"  src="/assets/images/reset.png" onclick="i=0;$('#myImg').rotate(0);$('#rotate').val(0);return false;" />&nbsp;
														<input type="image" border="1"  src="/assets/images/rotate_left.png" onclick="rotateimgrev('myImg');return false;" />
														</div>
													</div>
												</div>	
											<div class="row form-group">
												<label class="col-lg-3 control-label">Full Name:</label>
												<div class="col-lg-9">
													<input type="text" name="fullname" value="<?php echo $nama; ?>" class="form-control" required>
												</div>
											</div>
											<?php if ($role != '0'){ ?>
											<div class="row form-group">
												<label class="col-lg-3 control-label">KTP:</label>
												<div class="col-lg-9">
													<input type="text" name="ktp" value="<?php echo $licenseid; ?>" class="form-control">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-lg-3 control-label">KTA PDHI:</label>
												<div class="col-lg-9">
													<input type="text" name="kta" value="<?php echo $kta; ?>" class="form-control">
												</div>
											</div>
											<?php } else{ ?>
											<input type="hidden" name="ktp" value="0" class="form-control">
											<input type="hidden" name="kta" value="0" class="form-control">
											<?php } ?>
											
											<div class="row form-group">
												<label class="col-lg-3 control-label">Phone number:</label>
												<div class="col-lg-9">
													<input type="tel" name="phone" value="<?php echo $phone; ?>" class="form-control" >
													<?php // if ($role == '0'){ echo 'Make sure this phone number is correct to identify your pet';} ?>
												</div>
											</div>
											<div class="row form-group">
												<label class="col-lg-3 control-label">E-mail address:</label>
												<div class="col-lg-9">
													<input type="email" class="form-control" disabled="disabled" name="email" value="<?php echo $email; ?>">
												</div>
											</div>											
											<div class="row form-group">
												<label class="col-lg-3 control-label">Address:</label>
												<div class="col-lg-9">
													<input type="text" name="homeaddress" value="<?php echo $homeaddress; ?>" class="form-control" >
													<?php // if ($role == '0'){ echo 'Make sure this address is correct to identify your home address';} ?>
												</div>
											</div>
											<div class="row form-group">
												<label class="col-lg-3 control-label">City:</label>
												<div class="col-lg-9">
													<input type="text" name="homecity" value="<?php echo $city; ?>" class="form-control" >
													<?php // if ($role == '0'){ echo 'Make sure this address is correct to identify your city';} ?>
												</div>
											</div>
											<div class="row form-group">
												<label class="col-lg-3 control-label">Country:</label>
												<div class="col-lg-9">
													<input type="text" name="homecountry" value="<?php echo $country; ?>" class="form-control" >
													<?php // if ($role == '0'){ echo 'Make sure this address is correct to identify your country';} ?>
												</div>
											</div>
											<div class="row form-group">
											    <input type="hidden" value="no" name="rotate" id="rotate" />
												<input type="submit" class="theme_button wide_button" value="Update Profile">
											</div>
									</form>
								</div>
								<div class="tab-pane fade" id="tab5">
									<h4>Edit Clinic:</h4>
											<hr>
											<form class="form-horizontal" method="post" action="<?php echo base_url() ?>profile/updateclinic" enctype="multipart/form-data">
												<div class="row form-group">											
													<label class="col-lg-3 control-label" for="user-profile-avatar">Clinic Logo's</label>
													<div class="col-lg-9">
														<img src="<?php echo $logoclinic; ?>" style="width: 350px;"><br><br>
														<input type="file" id="user-profile-avatar" name="logoclinic">
														<p class="help-block">Image resolution size must be 300px x 300px</p>
													</div>
												</div>												
												<div class="row form-group">
													<label class="col-lg-3 control-label">Clinic Name:</label>
													<div class="col-lg-9">
														<input type="text" name="nameclinic" value="<?php echo $nameclinic ?>" class="form-control" required>
														<input type="hidden" name="idclinic" value="<?php echo $idclinic ?>">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Address</label>
													<div class="col-lg-9">
														<textarea class="form-control" name="address"><?php echo $address ?></textarea>
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Phone</label>
													<div class="col-lg-9">
														<textarea class="form-control" name="phone2"><?php echo $phoneclinic ?></textarea>
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">City</label>
													<div class="col-lg-9">
														<input type="textarea" class="form-control" name="city" value="<?php echo $cityclinic ?>">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Country</label>
													<div class="col-lg-9">
														<input type="textarea" class="form-control" name="country" value="<?php echo $countryclinic ?>">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Postal Code</label>
													<div class="col-lg-9">
														<input type="textarea" class="form-control"name="postalcode" value="<?php echo $postalcode ?>">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">SIP</label>
													<div class="col-lg-9">
														<input type="textarea" class="form-control" name="sip" value="<?php echo $sip ?>">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Coordinate</label>
													<div class="col-lg-9">
														Get Latidude And Longitude <span onclick="getLocation()" style="text-decoration: underline;">Click Here</span>
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Latidude</label>
													<div class="col-lg-9">
														<input type="textarea" class="form-control" id="lat" name="lat" value="<?php echo $lat ?>">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Longitude</label>
													<div class="col-lg-9">
														<input type="textarea" class="form-control" id="long" name="long" value="<?php echo $long ?>">
													</div>
												</div>
												<!-- <div class="row form-group">
													<!-- <img src="<?php echo $license; ?>" style="width: 100%;"> -
													<label class="col-lg-3 control-label">License / National ID:</label>
													<div class="col-lg-9">
														<input type="text" name="licenseid" value="<?php echo $licenseid ?>" class="form-control">
													</div>
												</div>
												<div class="row form-group">
													<label class="col-lg-3 control-label">Upload New License:</label>
													<div class="col-lg-9">
														<input type="file" id="user-profile-avatar" name="licensepic">
														
													</div>
												</div> -->
												<?php if($public_info<>''){ ?><div class="row form-group">
													<label class="col-lg-3 control-label"><br>Display Information in Public Pet Database :</label>
													<div class="col-lg-9" style="border-radius:15px; border:1px solid #ccc;background: #6600;"><br>
														<input type="checkbox" name="permission[]" value="0" <?php if($public_info[0]==1)echo "checked"; ?>> Clinic's Name<br>
														<input type="checkbox" name="permission[]" value="1" <?php if($public_info[1]==1)echo "checked"; ?>> Vet's Name<br>
														<input type="checkbox" name="permission[]" value="2" <?php if($public_info[2]==1)echo "checked"; ?>> Clinic's Phone<br>
														<input type="checkbox" name="permission[]" value="3" <?php if($public_info[3]==1)echo "checked"; ?>> Clinic's Email<br>
														<input type="checkbox" name="permission[]" value="4" <?php if($public_info[4]==1)echo "checked"; ?>> Clinic's Address<br><br>
													</div>
												</div>
												<?php } ?>
												<div class="row form-group">
													<button type="submit" class="theme_button wide_button">Update Clinic</button>
												</div>
											</form>
								</div>
								<div class="tab-pane fade" id="tab6">
									<h4>Update Password</h4>
											<hr>
											<form class="form-horizontal" method="post" action="<?php echo base_url() ?>profile/updatepass" enctype="multipart/form-data">
												<div class="form-group">
													<label class="col-lg-3 control-label">New password:</label>
													<div class="col-lg-9">
														<input type="password" class="form-control" name="newpass">
													</div>
												</div>
												<div class="form-group">
													<label class="col-lg-3 control-label">Repeat New password:</label>
													<div class="col-lg-9">
														<input type="password" class="form-control" name="repeatpass">
													</div>
												</div>
												<div class="form-group">
													<button type="submit" class="theme_button wide_button">Update Password</button>
												</div>
											</form>
								</div>
							</div>

						</div>

					</div>
					<!--.row-->
					
					<!-- .row main columns -->
				</div>
				<!-- .container -->
			</section>

			<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                        <!-- <div class="col-sm-6 text-sm-right">
                            <p class="grey">PT. IVET DATA GLOBAL </p>
                        </div> -->
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar 
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker 
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts 
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map 
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts 
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>
	<script src="/assets/js/jquery-3.4.1.min.js"></script>
	<script src="/assets/js/vendor/jquery.facedetection.min.js" ></script>
	<script src="/assets/js/jQueryRotate.js"></script>
	<script>
    	var i = 0;
    	
    	function rotateimg(e){
    		i = i + 90;
    		$('#'+e).rotate(i);
    		$('#rotate').val(i);
    		if(i==360)i=0;
    	}
    	
    	function rotateimgrev(e){
    		i = i - 90;
    		$('#'+e).rotate(i);
    		$('#rotate').val(i);
    		if(i==-360)i=0;
    	}
    	
    	function handleLocalFile(file) {
    		if (file.type.match(/image.*/)) {
    			var reader = new FileReader();
    			reader.onload = function (e) {
    				//detectNewImage(e.target.result, async);
    				document.getElementById("myImg").src=reader.result;
    				//document.getElementById("myImg").style.width='350px';
    				//document.getElementById("myImg").style.height='350px';
    				document.getElementById("conbutton").style.visibility='visible';
    				i=0;			
    			};
    			reader.readAsDataURL(file);
    		}
    	}
    
    	document.getElementById("inputimage").addEventListener("change", function (e) {
    		var files = this.files;
    		if (files.length)
    			handleLocalFile(files[0]);
    	});
    
    	$(document).ready(function(){
    		$("#myImg").on('DOMSubtreeModified',function(){
    			$('#myImg').faceDetection({
    				complete: function (faces) {
    					console.log(faces);
    					if(faces.length<1){
    						$("#myImg").rotate(90);
    						$("#rotate").val(90);
    						i = 90;
    					}
    				}
    			});
    		});
    	});
	</script>
    <script>
        jQuery(document).ready(function(){
            jQuery().invoice({
                addRow : "#addRow",
                delete : ".delete",
                parentClass : ".item-row",

                price : ".price",
                qty : ".qty",
                total : ".total",
                totalQty: "#totalQty",

                subtotal : "#subtotal",
                discount: "#discount",
                shipping : "#shipping",
                grandTotal : "#grandTotal"
            });
            jQuery( "#breed" ).autocomplete({
              source: "<?php echo site_url('pet/breedlist/?');?>"
            });

            $('input.number').keyup(function(event) {
              // skip for arrow keys
              if(event.which >= 37 && event.which <= 40) return;

              // format number
              $(this).val(function(index, value) {
                return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
              });
            });
            
        });
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function showPosition(position) {
            var poslat = position.coords.latitude;
            var poslong = position.coords.longitude;
            $('#lat').val(poslat);
            $('#long').val(poslong);
        }
    </script>

</body>

</html>