<html>
<head>
<title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">

		<!-- vendor css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-elements.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-blog.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-shop.css">

		<!-- Current Page new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/settings.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/layers.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/navigation.css">
		
		<!-- Demo new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/demos/demo-insurance.css">

		<!-- Skin new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/skins/skin-insurance.css"> 

		<!-- Theme Custom new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/custom.css">

		<!-- Head Libs -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/modernizr/modernizr.min.js"></script>    
</head>  
<body>
        <div role="main" class="main">
			<!-- <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Search Pet</h1>
						</div>
					</div>
				</div>
			</section> -->
<?php 
if($ada == '0'){ ?>
	                   <!-- <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				            <div class="container">
			            		<div class="row">
					            	<div class="col-md-12 align-self-center p-static order-2 text-center">
						            	<h1 class="custom-primary-font text-11 font-weight-light">PET INFO</h1>
					            	</div>
				            	</div>
			            	</div>
		            	</section>-->
						<div class="container">
						    <div class="row p-3">
						        <div class="col-12 id_not_found p-5 text-center">
						            <img class="mb-3 img-fluid" src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/empty.png" /><br />
						            <h3 class="mb-3" align="center">Oops, we can't find your PET ID</h3>
						            <p class="mb-0" style="line-height: 1.15em;"><a href="/searchnew">SEARCH</a> again</p>
						        </div>
						    </div>
						</div>

<?php }else{ ?>
			<!--<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">PET INFO</h1>
						</div>
					</div>
				</div>
			</section>-->
			<section class="section bg-color-light border-0 my-0">
				<div class="container microchip_card">
					<div class="row">
					    <div class="col-12 mb-2">
    					    <?php if($pet_info[0]==1){?>
        						<!--<div>
        							<strong><i class="fa fa-flag"></i> Pet Name :</strong>
        						</div>-->
        						<div>
        							<h2><?php echo $petname ?></h2>
        						</div>
    						<?php } ?>
					    </div>
					    </div>
					<div class="row">
							<div class="col-12 col-md-4 mb-3 mb-md-0">
								<img src="<?php echo $petphoto ?>" alt="" class="img-fluid petphoto" width="100%">
							</div>	
							<div class="col-12 col-md-8">
							    <div class="container pl-md-0">
									<div class="row mb-1 ml-0 pl-0">
            						    <div class="col-12 pl-0" style="margin-left:-15px;">
            						        <h3 class="clinic_info_title">Contact Vet</h3>
            						    </div>
            						</div>
									<div class="row mb-1">
										<?php if($pet_info[1]==1){?>
										    <div class="col-12 pet_id_div clinic_info_item">
										       <strong><i class="fa fa-paw"></i> Pet ID :</strong><br>
    										    <label class="clinic_info_font"><?php echo $rfid ?></label>
										    </div>
										  	<?php } ?>
								    </div>
                                            <!--<?php if($pet_info[2]==1){?><strong><i class="fa fa-medkit"></i> Last Vaccination :</strong><br> Defensor, <?php echo date('d M, Y H:i:s',strtotime('2018-11-11 10:34:12')) ?> <?php if (count($lastvaccine) == '0'){
                                                                                                                                                echo '-';
                                                 }else{ ?>
                                                 <?php echo $lastvaccine[0]->vaccine; ?> - <?php echo date('d M, Y',strtotime($lastvaccine[0]->datevacc)) ?>
                                                 <?php } ?><br> <?php } ?>
                                                <h3 style="margin: 20px 0px 5px;">Vets Info</h3>-->
            						
									<div class="row mb-1">
										<div class="col-12 clinic_info_item">
											<?php if($clinic_info[0]==1){?><strong><i class="rt-icon2-health"></i>Clinic :</strong><br>
											    <label class="clinic_info_font"><?php echo $clinic ?></label>
											<?php } ?>
										</div>
									</div>
									<div class="row mb-1">
									    <div class="col-12 clinic_info_item">
										    <?php if($clinic_info[1]==1){?><strong><i class="rt-icon2-health"></i> Vet Name :</strong><br>
    										    <label class="clinic_info_font"><?php echo $namevet ?></label>
    										<?php } ?>
										</div>
									</div>
									<div class="row mb-1">
										<div class="col-12 clinic_info_item">
										    <?php if($clinic_info[2]==1){?><strong><i class="rt-icon2-phone"></i> Phone :</strong><br>
											    <label class="clinic_info_font"><?php echo $phonevet ?></label>
											<?php } ?>
										</div>
									</div>
									<div class="row mb-1">
										<div class="col-12 clinic_info_item">
										    <?php if($clinic_info[3]==1){?><strong><i class="rt-icon2-mail"></i> Email :</strong><br>
										        <label class="clinic_info_font"><?php echo $email ?></label>
										    <?php } ?>
										</div>
									</div>
									<div class="row mb-1">
									    <div class="col-12 clinic_info_item">
										    <?php if($clinic_info[4]==1){?><strong><i class="rt-icon2-pin-alt"></i> Address :</strong><br>
										        <label class="clinic_info_font"><?php echo $address ?></label>
										    <?php } ?>
										</div>
									</div>
								</div>	
						    </div>	<button style="width:30%;margin:10px;" class="btn btn-secondary mt-2" onclick="self.location.href='/searchnew">SEARCH AGAIN</button>	
					</div>
				</div>
			</section>

	<?php } ?>
			<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid" style="background-color:#2f363c;">
                    <div class="row">
                        <div class="col-sm-12"><br />
                            <p class="grey" style="text-align: center;color:white;font-size:10pt;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
        <!-- new/vendor -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery/jquery.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/popper/umd/popper.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/common/common.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vide/jquery.vide.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vivus/vivus.min.js"></script>
        
        <!-- Theme Base, Components and Settings -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.js"></script>
		
		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.init.js"></script>
</body>
</html>
	