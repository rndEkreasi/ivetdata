        <div class="pages">
          <div data-page="dashboard-owner-vetsearch" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" class="backto" onclick="window.location='<?php echo base_url() ?>dashboard';"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">SEARCH VET</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="map-container mb-3">
                    <?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,%0A+Mountain+View,+CA&key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 9c419fc6-508e-42c8-b1d8-c24ab2b7349f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
?>
<!--<body>
        <div role="main" class="main">
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section>-->
    <div id="map" style="height: 10%;min-height:250px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
      //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;
        
        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              //var url = markerElem.getAttribute('url');
              var url = '?vets='+id;
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>
                  </div>
                  <div class="searchbox mb-3">
                    <form method="post" action="<?php echo base_url() ?>owner/vetSearch">
                      <input type="text" name="combo" value="<?php echo $this->input->post("combo") ?>" placeholder="Search" />
                      <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                  </div>
                  <div class="custom-accordion table-accordion mb-3">
                    <div class="accordion-item">
                      <div class="accordion-item-toggle">
                        <i class="icon icon-plus">+</i>
                        <i class="icon icon-minus">-</i>
                        <span>Choose Category</span>
                      </div>
                      <div class="accordion-item-content bgcolor-lightgrey">
                        <div class="main-nav category-filters icons_31">
                         <?php if(count($categories)==0){  ?>
                         <h2>No Categories</h2>
                         <?php }else{ ?>
                         <ul>
                             <?php foreach($categories as $val){ ?>
                            <li>
                                <a href="#" onclick="window.location='<?php echo base_url() ?>owner/VetSearch?category=<?php echo $val->id ?>&search=<?php echo $this->input->post('combo') ?>';">
                                <div class="category_icon mb-1">
                                    <img src="<?php echo base_url() ?>images/icons/specialties/<?php echo $val->category ?>.png"/>
                                </div><span><?php echo $val->title ?></span></a>
                            </li>
                            <?php } ?>
                          </ul>
                          <?php } ?>
                    </form>
                        </div>
                      </div>
                    </div>
                  </div>
            <?php if($searchvet){ ?>
              <div class="list-block">
                  <?php  if($message<>""){echo $message;} ?>
                <ul class="posts dovsitems">
                <?php for ($i=0;$i<count($searchvet);$i++) { ?>
                      <li class="swipeout">
                        <div >
                          <div class="post_entry">
                            <div class="post_thumb">
                                <?php if($searchvet[$i]['picture']=="") {echo '<img src="'.base_url().'assets/images/petshop.png" width="100%" >';
    								  }else {echo '<img src='.$searchvet[$i]['picture'].' width="100%" >';}
    							?><br />
                            </div>
                            <div class="post_details">
                              <h4><?php echo $searchvet[$i]['nameclinic']; ?></h4>
                              <div class="mb-2"><?php echo $searchvet[$i]['address']; ?></div>
                              <?php if($searchvet[$i]['phone']<>''){ ?>
                              <p><a href="#" onclick="window.location='tel:<?php echo $searchvet[$i]['phone'] ?>';" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a></p>
                              <?php } ?>
                              <?php if(isset($this->session->userdata['logged_in'])){ ?>
                              <button class="btn btn-modern btn-primary" onclick="window.location='<?php echo base_url() ?>pet/addvetbyowner?id=<?php echo $searchvet[$i]['idclinic'] ?>';" >Add into My List</button><br />
                              <button class="btn btn-modern btn-primary" style="margin-top: 5px;" onclick="window.location='<?php echo base_url() ?>appointment/add?idclinic=<?php echo $searchvet[$i]['idclinic'] ?>';" >Make Appointment</button>
                              <br /><br />
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </li>
                <?php }  ?>
                    </ul>
                    <!-- <div class="loadmore loadmore-dovsitems">LOAD MORE</div>
                    <div class="showless showless-dovsitems">END</div> -->
                  </div>
            <?php  }else{  ?>
                    <div class="list-block"><h3 align="center"><?php echo $message ?></h3></div>
            <?php  }        ?>
                </div>
              </div>
              <div class="footer-bar" >
