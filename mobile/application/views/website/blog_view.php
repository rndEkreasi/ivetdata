<!-- <section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="highlight">News & Article</h2>
				<ol class="breadcrumb darklinks">
					<li>
						<a href="<?php echo base_url() ?>">
							Home
						</a>
					</li>
					<li class="active">
						<a href="#">News & Article</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section> -->
<section class="ls page_portfolio section_padding_top_50 section_padding_bottom_75">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color">News & Article</h2>
							<p class="small-text">Keeping up with iVet Data</p>
						</div>
					
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="isotope_container isotope row masonry-layout columns_margin_bottom_20" style="position: relative; height: 6579px;">

								<?php foreach ($allarticle as $detail) { 
									$featuredimg = $detail->featuredimg;
									$status = $detail->status;
								    $jmlfeaturedimg = strlen($featuredimg);
								    if($jmlfeaturedimg < 50){
								    	$urlfeaturedimg = base_url().'upload/article/'.$featuredimg;
								    }else{
								        $urlfeaturedimg = $featuredimg; 
								    }
								    if($status == '1'){

								    }else{ ?>

								    								
								

								<div class="isotope-item col-lg-4 col-md-6 col-sm-12" style="position: absolute; left: 0%; top: 0px;">

									<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
										<div class="item-media">
											<img src="<?php echo $urlfeaturedimg ?>" alt="">
										</div>
										<div class="item-content">
											<p class="text-center item-meta">
												<span class="entry-date highlightlinks">
													<a href="#" rel="bookmark">
														<?php
															$datepublish = date('M d, Y', strtotime($detail->publishdate));
														 ?>
														<time class="entry-date" >
															<!-- March 9, 2017 -->
															<?php echo $datepublish ?>
														</time>
													</a>
												</span>
											</p>
											<h4 class="entry-title" style="font-size: 18px;text-align: left;">
												<a href="<?php echo base_url() ?>blog/detail/<?php echo $detail->slug; ?>"><?php echo $detail->title; ?></a>
											</h4>
											<p class="margin_0" style="text-align: left;font-size: 14px;">
												<!-- Clients can simply schedule their hard drive destruction online and through our website. -->
												<?php 
												$descibe = strip_tags($detail->description);

												echo substr($descibe, 0, 100); ?>...
											</p>
											<a href="<?php echo base_url() ?>blog/detail/<?php echo $detail->slug; ?>" class="read-more"></a>
										</div>
									</article>

								</div>

								<?php } 
								}?>

							</div>
							<!-- eof .isotope_container.row -->

							<div class="row">
								<div class="col-sm-12 text-center">
									<?php echo $this->pagination->create_links(); ?>
									<!-- <img src="<?php echo base_url() ?>assets/img/loading.png" alt="" class="fa-spin"> -->
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>