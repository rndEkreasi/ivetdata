
                <div class="links-row">
                  <a href="#" onclick="window.location='<?php echo base_url() ?>welcome/terms';">Terms &amp; Conditions</a>
                  <a href="#" onclick="window.location='<?php echo base_url() ?>welcome/privacy';">Privacy Policy</a>
                  <a href="#" onclick="window.location='<?php echo base_url() ?>welcome/faq';">FAQ</a>
                    <div class="copyright">&copy; IVETDATA 2019</div>
                </div>

              </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Tawkto Popup -->
    <div class="popup popup-tawkto">
      <div class="content-block">
        <h4>CONTACT US</h4>
          <iframe frameborder="0" src="https://tawk.to/chat/5c792599a726ff2eea5a1b14/default" scrolling="no" height="440" ></iframe>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="<?php echo base_url() ?>images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/framework7.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/email.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/circlemenu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/audio.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/my-app.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/custom.js"></script>    
    </body></html>