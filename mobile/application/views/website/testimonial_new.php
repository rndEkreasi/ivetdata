				<section class="section bg-color-light custom-padding-3 border-0 my-0">
					<div class="container">
						<div class="row">
							<div class="col">
								<h2 class="custom-primary-font text-center custom-fontsize-3 font-weight-normal mb-5 appear-animation" data-appear-animation="fadeInUpShorter">TESTIMONIALS</h2>
								<div class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="300"fadeInUpShorter>
									<div class="owl-carousel custom-carousel-style-2 mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 2}, '992': {'items': 2}, '1200': {'items': 2}}, 'margin': 40, 'loop': true, 'nav': false, 'dots': true}">
                                    <?php 
                                            $data = file_get_contents("https://dev.ivetdata.com/api/testimoni");
                                            $result = json_decode($data,true);
                                            foreach($result as $row){
                                    ?>
										<div>
											<div class="testimonial custom-testimonial-style-1">
												<div class="row" align="center">
													<div class="col-lg-3 px-5 px-lg-3 mb-4 mb-lg-0">
														<img src="<?php echo $row['url']; ?>" class="img-fluid rounded-circle" alt="" style="width:50%;" />
													</div>
													<div class="col-lg-9" style="margin-left:25%;width:50%;">
														<blockquote>
															<p class="mb-3"><?php echo $row['comment']; ?></p>
														</blockquote>
														<div class="testimonial-author">
															<strong class="text-2 mb-0"><?php  echo $row['name']; ?></strong>
															<div class="custom-rate-style">
																<i class="fas fa-star <?php if($row['rates']>0){ ?>active<?php }  ?>"></i>
																<i class="fas fa-star <?php if($row['rates']>1){ ?>active<?php }  ?>"></i>
																<i class="fas fa-star <?php if($row['rates']>2){ ?>active<?php }  ?>"></i>
																<i class="fas fa-star <?php if($row['rates']>3){ ?>active<?php }  ?>"></i>
																<i class="fas fa-star <?php if($row['rates']>4){ ?>active<?php }  ?>"></i>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</section>