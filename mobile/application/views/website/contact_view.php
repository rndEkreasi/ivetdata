<section class="ls page_portfolio section_padding_bottom_75">
<div class="col-sm-12 text-center">
	<h2 class="section_header with_icon icon_color">Contact Us</h2>
</div>
</section>
<section class="ls ms  background_cover section_padding_top_100 section_padding_bottom_130">
	<div class="container">
    	<div class="row">
    		<div class="col-md-8" style="margin: 0px 0px 30px;">
    			<!-- <img src="<?php echo base_url() ?>assets/images/map.png"> -->
    			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5006459913434!2d106.74596365063513!3d-6.197484162418824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f7156127b7d7%3A0xc48e9837c9530479!2sTaman+Kebon+Jeruk+Pharmacy!5e0!3m2!1sen!2sid!4v1550115159632" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    		</div>
    		<div class="col-md-4">
    			<ul class="list1 no-bullets no-top-border no-bottom-border">

								<li>
									<div class="media">
										<div class="media-left">
											<i class="rt-icon2-shop highlight2 fontsize_18"></i>
										</div>
										<div class="media-body">
											<h5 class="media-heading grey">Indonesia Address:</h5>
											Ruko Kebon Jeruk A9-3, Jakarta Barat 11620<br>
											Telephone: +62212977951
										</div>
									</div>
								</li>
								<li>
									<div class="media">
										<div class="media-left">
											<i class="rt-icon2-shop highlight2 fontsize_18"></i>
										</div>
										<div class="media-body">
											<h5 class="media-heading grey">US Address:</h5>
											1660 River Oak Drive, Roswell GA 30075<br>
											Telephone : (646) 820-6388
										</div>
									</div>
								</li>
								<li>
									<div class="media">
										<div class="media-left">
											<i class="rt-icon2-mail highlight2 fontsize_18"></i>
										</div>
										<div class="media-body greylinks">
											<h5 class="media-heading grey">Email:</h5>
											<a href="mailto:your@mail.com">contact@ivetdata.com</a>
										</div>
									</div>
								</li>
							</ul>
    		</div>
   		</div>
    </div>
</section>
