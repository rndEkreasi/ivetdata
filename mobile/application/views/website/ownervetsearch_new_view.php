<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="/dashboard">Dashboard</a>
                                </li>
                                <li class="active">Vet Search</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> Mon 22, Jul 2019</span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="ls section_padding_top_50 section_padding_left_50 section_padding_bottom_50 columns_padding_10" >
<div class="row">
            <div class="col-md-12">
                <h3>&nbsp;&nbsp;Vet / Clinic Search</h3>
            </div>
            <!-- .col-* -->                        
        </div></section>            
<section>
      <iframe frameborder="0" src="/searchnew" scrolling="no" onload="resizeIframe(this);"></iframe>
</section>


        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

<script>
        function iframeAutoScale() {
          
          'use strict';
          
          var
            iframes = document.getElementsByTagName('iframe'),
            index = 0;
        
          if (iframes.length > 0) {
            for (index = 0; index < iframes.length; index++) {
              var
                iframe = iframes[index],
                parent = iframe.parentElement,
                parentPadding = parseInt(window.getComputedStyle(parent, null).getPropertyValue('padding-left')) + parseInt(window.getComputedStyle(parent, null).getPropertyValue('padding-right')),
                parentWidth = parent.clientWidth - parentPadding,
                ratio = 0.75,	// default ratio (4:3)
                width = iframe.clientWidth,
                height = iframe.clientHeight;
        
              // overwrite default ratio if width and height attributes are not set
              if (width !== undefined && height !== undefined) {
                ratio = height / width;
              }
              
              iframe.setAttribute('width', parentWidth);
              iframe.setAttribute('height', parentWidth * ratio);
            }
          }
        }
        
        // onload
        document.addEventListener('DOMContentLoaded', function () { 
          iframeAutoScale();
        }, false);
        
        // on window resize
        window.onresize = function(event) {
          iframeAutoScale();
        };
    
    function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        //obj.contentWindow.location.reload();
    }
    
</script>

</body>

</html>