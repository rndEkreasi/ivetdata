        <div class="pages">
          <div data-page="dashboard-owner-petshop" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div id="pages_maincontent">
                <a href="#" onclick="window.location='<?php echo base_url() ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a> 
                <h2 class="page_title">PET <?php if(stristr($_SERVER['REQUEST_URI'],"service")){ ?>SERVICES<?php }else{ ?>SHOP<?php } ?> 
                  <!-- <a href="dashboard-owner-cart.php"><i class="fas fa-shopping-cart my-cart-badge"></i></a> -->
                  <?php if (isset($error)){ ?>
                    <div class="alert alert-danger error"><?php echo $error; ?></div>
                  <?php } ?> 
                  <?php if (isset($success)){ ?>
                    <div class="alert success"><?php echo $success ; ?></div>
                  <?php } ?> 

                </h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="searchbox mb-3">
                    <form action="<?php echo base_url() ?>shop/search/" method="post" enctype="multipart/form-data">
                      <input type="text" name="item" value="" placeholder="Search" />
                      <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </form>
                  </div>
                  <div class="custom-accordion table-accordion mb-4">
                    <div class="accordion-item">
                      <div class="accordion-item-toggle">
                        <i class="icon icon-plus">+</i>
                        <i class="icon icon-minus">-</i>
                        <span>Choose Category</span>
                      </div>
                      <div class="accordion-item-content bgcolor-lightgrey">
                        <div class="main-nav category-filters icons_31">
                          <ul>
                        <?php 
                            if(stristr($_SERVER['REQUEST_URI'],"service")){
                                $dt_cat = $this->Shop_model->listcatservice(); 
                            }else{
                                $dt_cat = $this->Shop_model->listcat(); 
                            }
                            foreach($dt_cat as $val){
                        ?>
                            <li>
                              <a href="#" onclick="window.location='?id=<?php echo $val->id ?>';">
                                <div class="category_icon mb-1"><img src="<?php 
                                    if(stristr($_SERVER['REQUEST_URI'],"service")){  
                                        echo base_url() ?>images/icons/services/<?php echo $val->category;
                                    }else{
                                        echo base_url() ?>images/icons/categories/<?php echo $val->category;
                                    }
                                ?>.png"/></div>
                                <span><?php echo $val->title ?></span>
                              </a>
                            </li>
                        <?php  }   ?>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="shop_items productsw">
            <?php 
                foreach ($dataproduct as $product) { 
            ?>
                    <li class="itemproduct<?php echo $product->id; ?>">
                      <div class="shop_thumb"><a href="#">
                        <?php
                        $imageproduct = $product->picture;
                        if($imageproduct == ''){ ?>
                           <img src="<?php echo base_url() ?>assets/images/default-shop.png" alt="" title="" />
                        <?php }else{ ?>
                            <img src="<?php echo $imageproduct ?>" alt="" title="" />
                        <?php }?>
                      
                       </a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="#"><?php echo $product->nameservice ?></a></h4>
                        <div class="mb-2">by: <?php echo $product->nameclinic ?></div>
                        <div class="shop_item_price mb-2">Rp. <?php echo number_format($product->price)?></div>
                        <form id="myform" method="POST" action="<?php echo base_url() ?>shop/invoicetemp">
                        <div class="item_qnty_shop quantityInput">
                          
                            <input type="button" value="-" class="sub qntyminusshop" field="quantity1" />
                            <!-- <button type="button" id="sub" class="sub qntyminusshop">-</button> -->
                            <input type="text" name="qty" value="1" min="1" class="qntyshop" />
                            <!-- <button type="button" id="add" class="add qntyplusshop">+</button> -->
                            <input type="button" value="+" class="add qntyplusshop" field="quantity" />
                            <input type="hidden" name="idproduk" value="<?php echo $product->id ?>">
                            <input type="hidden" name="nama_item" value="<?php echo $product->nameservice ?>">
                            <input type="hidden" name="price" value="<?php echo $product->price ?>">
                            <input type="hidden" name="idclinic" value="<?php echo $product->idclinic ?>">

                          
                        </div>
                        <div class="actionnya product-add-to-cart">
                          <button type="submit" class="icon-tab theme_button color3 button my-cart-btn" data-id="<?php echo $product->id ?>"  data-name="<?php echo $product->nameservice ?>" data-clinic="<?php echo $product->idclinic ?>" data-summary="<?php echo $product->nameservice ?>" data-price="<?php echo $product->price ?>" data-quantity="1" data-image="<?php echo base_url() ?>assets/images/default-shop.png" id="addtocart">Add to cart</button>
                        </div>
                        </form>
                        <!-- <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a> -->
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="<?php echo base_url() ?>images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <?php  } ?>
                  </ul>
                  <div class="shop_pagination">
                    <?php // if(!isset($error))echo $this->pagination->create_links(); ?>
                    <!-- <a href="shop.html" class="prev_shop">PREV PAGE</a>
                    <span class="shop_pagenr">1/37</span>
                    <a href="shop-page2.html" class="next_shop">NEXT PAGE</a> -->
                  </div>
                </div>
              </div>
              

        