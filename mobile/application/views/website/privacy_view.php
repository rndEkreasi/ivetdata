<div class="pages">
  <div data-page="privacy" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="#" onclick="window.location='<?php echo base_url() ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title">Privacy Policy</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
<p>
    1. Anda mengerti bahwa ketika menggunakan Akun IVet Data, data personal
    Anda digunakan untuk verifikasi keabsahan dan keamanan bersama untuk tujuan
    operasi Akun IVet Data secara internal melalui aplikasi kami. Anda mengakui
    bahwa kami dapat diminta untuk mengungkapkan data personal Anda kepada
    setiap otoritas (termasuk otoritas pengadilan atau moneter) berdasarkan
    hukum dan peraturan perundang-undangan yang berlaku.
</p>
<p>
    2. Kami dapat mengumpulkan Informasi Personal yang dapat digunakan untuk
    menghubungi atau mengidentifikasi Anda. Informasi Personal adalah atau
    dapat digunakan untuk: (a) menyediakan dan meningkatkan Layanan kami, (b)
    mengatur penggunaan Layanan, (c) lebih mengenal kebutuhan dan kepentingan
    Anda, (d) untuk melakukan personalisasi dan meningkatkan pengalaman Anda,
    dan (e) untuk menggunakan pembaharuan software dan pengumuman produk.
</p>
<p>
    3. Kami tidak akan membocorkan atau membuka Data personal Anda kepada pihak
    lain tanpa persetujuan sebelumnya dari Anda. Kami dengan ini berjanji
    sebagai berikut:
</p>
<p>
    1. Untuk memperlakukan Data personal dengan secara rahasia dan tidak
    membukanya Data tersebut (atau suatu porsi darinya) ke setiap orang
    kecuali: kepada pegawai Kami berdasarkan need-to-know, dengan syarat bahwa
    dalam hal tersebut terjadi, Kami akan memberitahukan orang tersebut
    mengenai Ketentuan ini dan sifat rahasia dari Data;
</p>
<p>
    2. Apabila suatu pembukaan harus dilaksanakan sesuai dengan adanya
    permintaan pengadilan atau perintah pemerintah lainnya, dengan ketentuan
    bahwa, apabila situasinya memungkinkan, Kami akan memberitahukan
    pemberitahuan mengenai pembukaan tersebut;
</p>
<p>
    3. Data personal Anda dijaga berdasarkan Kebijakan Privasi Kami. Kebijakan
    Privasi dianggap sebagai bagian yang tidak terpisahkan dari Ketentuan
    Penggunaan ini dan persetujuan Anda atas Ketentuan Penggunaan ini merupakan
    penerimaan Anda atas Kebijakan Privasi Kami.
</p>
<p>
    4. Kewajiban di atas tidak berlaku kepada informasi yang: (a) terdapat
    dalam kekuasaan kami sebelum tanggal dimana Data tersebut diberitahukan
    kepada kami oleh anda; (b) telah atau akan menjadi diketahui secara umum
    melalui publikasi, penggunaan secara komersial atau lainnya selain daripada
    konsekuensi dari pelanggaran atas Ketentuan ini; (c) telah dikembangkan
    secara independen oleh kami; dan/atau (d) telah diterima secara sah dari
    pihak ketiga yang tidak terikat atas ketentuaan kerahasiaan dan melanggar
    Ketentuan ini;
</p>
<p>
    5. Kami mereservasi hak untuk mengumpulkan informasi menggunakan logging
    dan cookies, seperti alamat IP, yang dapat sewaktu-waktu dikorelasikan
    dengan Informasi Personal. Kami akan menggunakan informasi untuk mengawasi
    dan menganalisa penggunaan Layanan, untuk administrasi Layanan secara
    teknis, untuk menambah fungsi dari Layanan kami dan untuk memverifikasi
    apakah pengguna memiliki otoritas yang diperlukan untuk Layanan melakukan
    sesuai permintaan mereka.
</p>
<p>
    6. Kami dapat menggunakan data secara agregat termasuk namun tidak terbatas
    pada analisa dan meningkatkan layanan kami.
</p>
<p>
    7. Kami dapat memindahkan dan mengalihkan Data Anda dalam hal terjadinya
    merjer, akuisisi, atau penjualan dari seluruh atau sebagian dari asset
    Kami, tanpa memberikan pemberitahuan sebelumnya kepada Anda. Anda dengan
    ini memberikan persetujuan dan kesepatannya apabila pemindahan tersebut
    terjadi.
</p>
<p>
    8. Sebagai suatu pengguna yang terdaftar, Anda dapat memeriksa,
    memperbaharui, memperbaiki atau menghapus Informasi Personal yang
    disediakan saat pendaftaran atau profil akun dengan merubah Pengaturan
    Akun. Apabila Anda secara pribadi merubah informasi yang dapat
    diinformasikan, atau apabila Anda tidak menginginkan jasa Kami, Anda dapat
    memperbaharui atau menghapusnya dengan melakukan perubahan pada Pengaturan
    Akun Anda. Terkadang kami menyimpan salinan dari informasi Anda apabila
    dipersyaratkan oleh hukum.
</p>
<p>
    9. Kami akan menyimpan informasi Anda selama akun Anda aktif atau
    sebagaimana yang Kami butuhkan untuk menyediakan Anda dengan layanan.
    Apabila anda berniat untuk membatalkan akun Anda atau meminta Kami untuk
    tidak lagi menggunakan informasi Anda untuk memberikan Anda dengan
    pelayanan, Anda dapat menghapus akun Anda. Konsisten dengan persyaratan
    ini, Kami akan menghapus informasi Anda segera pada saat diminta. Akan
    tetapi mohon diketahui bahwa terdapat risiko dari server dan versi cadangan
    Kami yang mungkin ada setelah penghapusan. Sebagai tambahan, kami tidak
    menghapus dari server kami fail yang Anda miliki bersama dengan pengguna
    lain.
</p>

          </div>
        </div>
      </div>
      <div class="footer-bar">