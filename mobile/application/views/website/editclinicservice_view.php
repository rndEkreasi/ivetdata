            <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>service/all">My Service</a>
                                </li>
                                <li class="active">Edit Service</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ($cat == 'service'){
                            $titlenya = 'Service Name'; ?>
                                <h3>Edit Service</h3>
                            <?php } else {
                            $titlenya = 'Item Name'; ?>
                                <h3>Edit Medical Item</h3>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- .row -->
                    <?php if (isset($success)){ ?>
                        <div class="alert alert-success"><?php echo $success ?></div>
                    <?php } ?>
                    <?php if (isset($error)){ ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">

                            
                            <div class="with_padding">

                                <form action="<?php echo base_url() ?>service/editpost" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo $titlenya ?></label>
                                        <input type="text" class="form-control" value="<?php echo $servicename ?>" id="judul" name="servicename" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Sell Price (Rp.) :</label>
                                        <input type="text" class="form-control number" value="<?php echo number_format($serviceprice) ?>"  name="serviceprice" required>
                                        <input type="hidden" name="idservice" value="<?php echo $idservice ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Purchase Price (Rp.):</label>
                                       <input type="text" class="form-control number" id="judul" name="origprice" value="<?php echo number_format($origprice) ?>" required>
                                    </div>
                                    <?php if ($cat == 'service'){ ?>
                                        <div class="form-group">
                                       <label for="exampleInputEmail1">Service Fee:</label>
                                       <input type="text" class="form-control number" id="fee" value="<?php echo number_format($fee) ?>" name="fee" required>
                                       <input type="hidden" name="stocks" value="0">
                                       <input type="hidden" name="unit" value="">
                                    </div> 
                                    <?php }else{ ?>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Stocks (Quantity) :</label>
                                       <input type="text" class="form-control number" id="judul" value="<?php
                                       if($qty < '0'){
                                        echo '0';
                                       }else{
                                        echo number_format($qty);
                                       }?>" name="stocks" required>
                                       <input type="hidden" name="fee" value="0">
                                    </div> 
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Unit :</label>
                                       <input type="text" class="form-control" id="unit" <?php                                       
                                       if($unit == ''){ ?>
                                        value="" placeholder="ml/kg/cc/gr"
                                      <?php }else{ ?>
                                        value="<?php echo $unit ?>"
                                       <?php }?> name="unit" required>
                                    </div>
                                    <?php }?>
                                    
                                                        
                                    <button type="submit" class="theme_button" style="float: right;">Update</button>
                                </form>
                            </div>
                            <!-- .with_border -->                           
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->


                </div>
                <!-- .container -->
            </section>


<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; Copyrights <?php echo date('Y'); ?> PT. iVet Data Global. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#searchservice').autocomplete({
                source: "<?php echo site_url('service/allsearch/?');?>",
      
                select: function (event, ui) {
                    $('[name="nameservice"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });

            $('input.number').keyup(function(event) {
              // skip for arrow keys
              if(event.which >= 37 && event.which <= 40) return;

              // format number
              $(this).val(function(index, value) {
                return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
              });
            });
 
        });
    </script>


</body>

</html>
            