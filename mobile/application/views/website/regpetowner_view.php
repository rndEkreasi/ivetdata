<script src="<?php echo base_url() ?>assets/js/country/assets/js/geodatasource-cr.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/country/assets/css/geodatasource-countryflag.css">
    <!-- link to all languages po files -->
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ar/LC_MESSAGES/ar.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/cs/LC_MESSAGES/cs.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/da/LC_MESSAGES/da.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/de/LC_MESSAGES/de.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/en/LC_MESSAGES/en.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/es/LC_MESSAGES/es.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/fr/LC_MESSAGES/fr.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/it/LC_MESSAGES/it.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ja/LC_MESSAGES/ja.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ko/LC_MESSAGES/ko.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ms/LC_MESSAGES/ms.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/nl/LC_MESSAGES/nl.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/pt/LC_MESSAGES/pt.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ru/LC_MESSAGES/ru.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/sv/LC_MESSAGES/sv.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/vi/LC_MESSAGES/vi.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-cn/LC_MESSAGES/zh-cn.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-tw/LC_MESSAGES/zh-tw.po" />

    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/country/assets/js/Gettext.js"></script>

<section class="intro_section page_mainslider ls ms">  
    <div class="container">
        <div class="row">                               
                    <div class="col-md-12">
                      
                        
                        <!-- tabs content start here -->
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                              <?php } ?>
                              <?php if (isset($success)){ ?>
                                <div class="alert alert-success"><?php echo $success ?></div>
                              <?php } ?>

                              <script type="text/javascript">
                              var onloadCallback = function() {
                                grecaptcha.render('contactform', {
                                  'sitekey' : '6LdW-pkUAAAAAFW_bF0JjIVbnEcmt7ooOXgRHr7a'
                                });
                              };
                            </script>

                                <form action="<?php echo base_url() ?>register/registerpetowner_process" method="post" enctype="multipart/form-data">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Pet Owner Registration</h4>
                                   <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Full Name*</span>
                                        </div>
                                        <input type="text" class="form-control" name="name" placeholder="Full Name" value="<?php if (isset($name)){ echo $name; } ?>" aria-label="Name" required>
                                    </div> 
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Email*</span>
                                        </div>
                                        <input type="email" class="form-control" name="email" value="<?php if (isset($user_email)){ echo $user_email; } ?>" aria-label="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Phone*</span>
                                        </div>
                                        <input type="text" class="form-control" name="phone" value="<?php if (isset($phone)){ echo $phone; } ?>" aria-label="phone" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Password*</span>
                                        </div>
                                        <input type="password" class="form-control" name="user_passnya" aria-label="Username" required">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Confirm Password*</span>
                                        </div>
                                        <input type="password" class="form-control" name="user_passnya2" aria-label="Username" required><br>
                                    </div>
                                    <h5>Additional information</h5>                                   
                                    <!-- <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Address*</span>
                                        </div>
                                        <div id="locationField">
                                            <textarea class="form-control" name="address" aria-label="city" id="autocomplete"
                                                 placeholder="Enter your address"
                                                 onFocus="geolocate()" required><?php if (isset($address)){ echo $address; } ?></textarea>
                                        </div>
                                        
                                    </div> -->                                   
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Country*</span>
                                        </div>                                        
                                        <select class="form-control gds-cr" country-data-region-id="gds-cr-one" data-language="en" name="country" country-data-default-value="ID">
                                            <?php if (isset($country)) { ?>                                                 
                                                 <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                                <?php  }else{ ?>
                                                    <option value="">Select Country</option>
                                                <?php } ?>
                                        </select>
                                         <!-- <select class="form-control" name="country" aria-label="country">
                                          <?php foreach ($datacountry as $country) { ?>
                                               <option value="<?php echo $country->namecountry?>" <?php if($country->namecountry=="Indonesia") echo "selected=selected"; ?>> <?php echo $country->namecountry?></option>
                                        <?php } ?>
                                        </select> -->
                                    </div> 
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Province*</span>
                                        </div>
                                        <!-- <input type="text" class="form-control active" id="city" name="province" aria-label="province" value="<?php if (isset($province)) { echo $province; } ?>" required> -->
                                        <select class="form-control" id="gds-cr-one" name="province">
                                            <?php if (isset($province)) { ?>
                                                 <option value="<?php echo $province; ?>"><?php echo $province; ?></option>
                                                <?php }else{ ?>
                                                    <option value="">Select Province</option>
                                                <?php } ?>
                                        </select>
                                        <!-- <select class="form-control" name="country" aria-label="country">
                                          <?php foreach ($datacountry as $country) { ?>
                                               <option value="<?php echo $country->namecountry?>"> <?php echo $country->namecountry?></option>
                                        <?php } ?>
                                        </select> -->
                                    </div>  
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">City*</span>
                                        </div>
                                        <input type="text" class="form-control active" id="city" name="city" aria-label="city" value="<?php if (isset($city)) { echo $city; } ?>" required>
                                        <!-- <select name="city" class="form-control cities order-alpha" id="cityId">
                                            <?php // if (isset($city)) { ?>
                                                 <!-- echo $city;  
                                                 <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
                                                <?php // }else{ ?>
                                                    <option value="">Select City</option>
                                                <?php // } ?>
                                        </select> -->
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Postal Code</span>
                                        </div>
                                        <input type="text" class="form-control" name="postalcode" value="<?php if (isset($postalcode)){ echo $postalcode; } ?>" aria-label="postalcode">
                                    </div>                                  
                                   

                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"></span>
                                        </div>
                                        <input type="checkbox" class="" name="term" aria-label="Username" required> I Agree to iVetdata <a href="<?php echo base_url() ?>termsconditions.html" target="_new">Terms &amp; Conditions</a><br><br>
                                        * Mandatory, must be filled.
                                    </div>
                                </div>
                                <div class="row mx-0 justify-content-end no-gutters">
                                    <div class="col-6"><br><br>  
                                        <div id="contactform"></div><br><br>                                     
                                        <input type="submit" class="btn btn-block theme_button gradient border-0 z-3" value="Register Now">                                       
                                    </div>
                                </div>
                              </form>
                              <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
                                    async defer>
                                </script>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                    <div class="col-md-3">
                    </div>
                    </div>
        </div>
    </div>
              
</section>

<footer class="page_footer ds parallax section_padding_top_100 section_padding_bottom_65 columns_padding_25">
                <div class="container">
                    <div class="row">

                         <!-- <div class="col-md-4 col-sm-12 text-center to_animate" data-animation="fadeInUp">
                            <div class="widget topmargin_30">
                                <h3>In Collaboration</h3>
                                <img src="<?php echo base_url() ?>assets/images/pdhi.png">
                            </div>
                        </div> -->
                        <div class="col-md-6 col-sm-12 text-center to_animate" data-animation="fadeInUp">
                            <div class="widget">
                                <a href="<?php echo base_url() ?>" class="logo bottommargin_20">
                                    <img src="<?php echo base_url() ?>assets/images/logo-white.png" alt="">
                                </a>
                                
                                <div class="grey topmargin_30">
                                    <!-- <div class="media small-teaser inline-block margin_0">
                                        <div class="media-left media-middle">
                                            <i class="fa fa-map-marker highlight3" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body media-middle">
                                            253 Adams Ave, Iowa
                                        </div>
                                    </div>
                                    <br>
                                    <div class="media small-teaser inline-block margin_0">
                                        <div class="media-left media-middle">
                                            <i class="fa fa-phone highlight3" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body media-middle">
                                            8 800 269 8469
                                        </div>
                                    </div> -->
                                    <br>
                                    <div class="media small-teaser inline-block margin_0">
                                        <div class="media-left media-middle">
                                            <i class="fa fa-envelope highlight3" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body media-middle darklinks color3">
                                            <a href="mailto:contact@ivetdata.com">contact@ivetdata.com</a>
                                        </div>
                                    </div>
                                    <div class="media small-teaser inline-block margin_0">
                                        <div class="media-left media-middle">
                                            <i class="fa fa-internet-explorer highlight3" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body media-middle darklinks color3">
                                            <a href="#">https://ivetdata.com</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="darklinks topmargin_25">
                                    <a class="social-icon rounded-icon border-icon soc-facebook" href="https://www.facebook.com/ivetdata/" target="_new" title="Facebook iVetData"></a>
                                    <a class="social-icon rounded-icon border-icon soc-twitter" href="https://twitter.com/ivetdata" target="_new" title="Twitter iVetData"></a>
                                    <a class="social-icon rounded-icon border-icon soc-instagram" href="https://www.instagram.com/ivetdata/" target="_new" title="IG iVetData"></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 text-center to_animate" data-animation="fadeInUp">
                            <div class="widget topmargin_30">
                                <h3>Contact Form</h3>
                                <form class="contact-form topmargin_45" method="post" action="<?php echo base_url() ?>contact/send_email">
                                    <p class="form-group">
                                        <label for="footer-name">Name <span class="required">*</span></label>
                                        <i class="fa fa-user highlight3" aria-hidden="true"></i>
                                        <input type="text" aria-required="true" size="30" value="" name="name" id="footer-name" class="form-control" placeholder="Full Name" required>
                                    </p>
                                    <p class="form-group">
                                        <label for="footer-email">Phone <span class="required">*</span></label>
                                        <i class="fa fa-phone highlight3" aria-hidden="true"></i>
                                        <input type="text" aria-required="true" size="30" value="" name="phone" id="footer-phone" class="form-control" placeholder="Phone" required>
                                    </p>
                                    <p class="form-group">
                                        <label for="footer-email">Email <span class="required">*</span></label>
                                        <i class="fa fa-envelope highlight3" aria-hidden="true"></i>
                                        <input type="email" aria-required="true" size="30" value="" name="email" id="footer-email" class="form-control" placeholder="Email Address" required>
                                    </p>
                                                <p class="form-group">
                                        <label for="footer-message">Message</label>
                                        <i class="fa fa-comment highlight3" aria-hidden="true"></i>
                                        <textarea aria-required="true" rows="3" cols="45" name="message" id="footer-message" class="form-control" placeholder="Message..."></textarea required>
                                    </p>
                                        <p class="footer_contact-form-submit topmargin_20">
                                        <button type="submit" id="footer_contact_form_submit" name="contact_submit" class="theme_button color3 wide_button">Send Now</button>
                                        <a href="https://m.me/ivetdata" class="theme_button color4 wide_button">Chat on Fb Messenger</a>
                                    </p>
                                </form>
                            </div>
                        </div>                        

                    </div>
                </div>
            </footer>

            <section class="ls page_copyright section_padding_15">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <p>&copy; Copyright <?php echo date('Y') ?> PT. iVet Data Global. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
    <!--<script src="//geodata.solutions/includes/countrystatecity.js"></script> -->
    <!-- eof #canvas -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <!-- <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
   

<!--     <script>
// This sample uses the Autocomplete widget to help the user select a
// place, then it retrieves the address components associated with that
// place, and then it populates the form fields with those details.
// This sample requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script
// src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;

var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields('address_components');

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&libraries=places&callback=initAutocomplete"
        async defer></script> -->
  </body>
</html>


</body>

</html>