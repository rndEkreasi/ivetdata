<div class="pages">
  <div data-page="privacy" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="#" onclick="window.location='<?php echo base_url() ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title">Frequently Asked Questions</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
            <div class="custom-accordion faq-accordion">
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>What is iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData is an interactive Pocket Vet service that makes it easy for Pet Owners to connect to their Vets and get comprehensive reports, appointments, and consultations through a Nation-wide Vet Network under Perhimpunan Dokter Hewan Indonesia (PDHI).
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>What can I do with iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData helps you keep records and analyze your Pet historical data stored in dashboard. You can use Vet Atlas to find the nearest Vet, consult and make appointments, without the need to wait at the physical location.  iVetData can also provides you with Public Pet Database that can be used tor practitioners, shelters, and general public to verify and locate ownership of Pets, especially in the case of missing Pets. You can also use iVetData to generate reports or to explore data pertaining any medical and/or services made to your Vet.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>How do I get started with iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    To get started with iVetData, simply log into the iVet Data Console and add your Pet details. You can then start querying data in Vet Search to connect to your Vet. 
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>How do you access iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData can be accessed via any browser both in Desktop or Mobile, and Google Play Store.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>What is the underlying technology behind iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData is an in-house Client Management Solution developed through Web and App programming using secure cloud-base server with certified SSL and encryption technology.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>How can I get in touch with iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData can be contacted via email at contact@ivetdata.com, Chat Box, Facebook, and Instagram.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bar">