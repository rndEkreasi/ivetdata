<div class="pages">
  <div data-page="news-single" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="#" onclick="history.back(1);" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title"><?php echo $detailarticle[0]->title;  ?></h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
            <center><img src="<?php echo $detailarticle[0]->featuredimg;  ?>" class="img-fluid"></center>
            <?php  echo str_replace("Arial","'Source Sans Pro', sans-serif",$detailarticle[0]->description); ?>
            <span class="post_date"><?php echo date("d M Y",strtotime($detailarticle[0]->publishdate));   ?></span>
            <span class="post_author"> <a href="#">admin</a></span>
            <!-- <span class="post_comments"><a href="#">0</a></span> -->
          </div>
          <!-- <a href="#" data-popup=".popup-social" class="button_full btmint open-popup">SHARE THIS POST</a>
          <div class="buttons-row">
            <a href="#tab1" class="tab-link active button">Leave a comment</a>
            <a href="#tab2" class="tab-link button">Comments</a>
          </div> -->
          <div class="tabs-animated-wrap">
            <div class="tabs">
              <!-- <div id="tab1" class="tab active">
                <div class="contactform">
                  <form id="CommentForm" method="post" action="">
                    <label>Name:</label>
                    <input type="text" name="CommentName" id="CommentName" value="" class="form_input" />
                    <label>Email:</label>
                    <input type="text" name="CommentEmail" id="CommentEmail" value="" class="form_input" />
                    <label>Comment:</label>
                    <textarea name="Comment" id="Comment" class="form_textarea" rows="" cols=""></textarea>
                    <input type="submit" name="submit" class="form_submit" id="submit" value="Submit" />
                  </form>
                </div>
              </div> -->
              <!-- <div id="tab2" class="tab">
                <ul class="comments">
                  <li class="comment_row">
                    <div class="comm_avatar"><img src="images/icons/black/user.png" alt="" title="" border="0" /></div>
                    <div class="comm_content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam velit sapien, eleifend in by <a
                          href="#">John Doe</a></p>
                    </div>
                  </li>
                  <li class="comment_row">
                    <div class="comm_avatar"><img src="images/icons/black/user.png" alt="" title="" border="0" /></div>
                    <div class="comm_content">
                      <p>Consectetur adipiscing elit. Nam velit sapien, eleifend in by <a href="#">John Doe</a></p>
                    </div>
                  </li>
                  <li class="comment_row">
                    <div class="comm_avatar"><img src="images/icons/black/user.png" alt="" title="" border="0" /></div>
                    <div class="comm_content">
                      <p>Nam velit sapien, eleifend in by <a href="#">John Doe</a></p>
                    </div>
                  </li>
                  <div class="clear"></div>
                </ul>
              </div> -->
            </div>
          </div>
        </div>
      </div>
      