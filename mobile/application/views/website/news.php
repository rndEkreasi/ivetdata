<div class="pages">
  <div data-page="news" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <h2 class="page_title">Latest News</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="list-block">
            <ul class="posts newsitems">
              <?php foreach($allarticle as $row){  ?>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb">
                    <img src="<?php 
                    
                            $photo = $row->featuredimg;

                            if (stristr($photo,"upload")){
                                if(!file_exists(".".$photo)){
                                    echo base_url()."images/news.png" ;
                                }else{
                                    echo $photo;
                                }
                            } else {
                                echo $photo;
                            }
 
                    ?>" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="#" onclick="window.location='<?php echo base_url() ?>news/detail/<?php echo $row->slug;  ?>'"><b><?php echo $row->title ?></b></a></div>
                      <h2><a href="#" onclick="window.location='<?php echo base_url() ?>news/detail/<?php echo $row->slug;  ?>'"><?php  echo substr(strip_tags($row->description),0,132); ?></a></h2>
                    </div>
                    <div class="post_swipe"><img src="<?php echo base_url() ?>images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="#" class="action1" onclick="window.open('https://tawk.to/chat/5c792599a726ff2eea5a1b14/default');"><img src="<?php echo base_url() ?>images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1" onclick="window.open('https://www.facebook.com/ivetdata')"><img
                      src="<?php echo base_url() ?>images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1" onclick="window.open('mailto:contact@ivetdata.com')"><img
                      src="<?php echo base_url() ?>images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <?php } ?>
            </ul>
            <!-- <div class="loadmore loadmore-newsitems">LOAD MORE</div>
            <div class="showless showless-newsitems">END</div> -->
          </div>
        </div>
      </div>
      <div class="footer-bar">