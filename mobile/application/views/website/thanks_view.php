			<section class="ls ms section_404 background_cover section_padding_top_100 section_padding_bottom_130">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<div class="inline-block text-center">
								<p class="not_found">
						<span class="highlight2" style="font-size: 80px">Thank You</span>
					</p>
								<h3>Thank you! You are now subscribed! Please login! </h3>
								
								<p>
						<a href="<?php echo base_url() ?>login" class="theme_button color3 wide_button">Login</a>
					</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			