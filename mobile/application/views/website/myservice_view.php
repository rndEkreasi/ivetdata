<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">Service & Medicine</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>Service & Medicine</h3>
                            <?php if($manageto = array('1','3')){ ?>                            
                            <button class="icon-tab theme_button color1" data-toggle="modal" data-target="#addservice">+ Add Service</button>
                            <button class="icon-tab theme_button color2" data-toggle="modal" data-target="#addmedicine">+ Add Medicine</button>
                            <a href="<?php echo base_url() ?>service/exportservicelist" class="icon-tab theme_button color3" target="_blank">Export List</a>
                            <?php } ?>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table id="table" class="table table-striped table-bordered" cellspacing="10" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No<br /><i class="fa fa-sort"></i></th>
                                            <th>Name<br /><i class="fa fa-sort"></i></th>
                                            <th>Stock<br /><i class="fa fa-sort"></i></th>
                                            <th>Category<br /><i class="fa fa-sort"></i></th>
                                            <th style="pointer-events:none;">Price</th>
                                            <th style="pointer-events:none;">Unit</th>
                                            <?php if($manageto = array('1','3')){ ?> 
                                                <th style="pointer-events:none;">Action</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>


            <!-- Modal -->
<div class="modal fade" id="addservice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding: 15px 15px 60px;"> 
       <form action="<?php echo base_url() ?>service/addservice" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleInputEmail1">Service Name :</label>
                <input type="text" class="form-control" id="judul" name="nameservice" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Sell Price (Rp.) :</label>
               <input type="text" class="form-control number" id="judul" placeholder="Example 1000000" name="price" required>
               <input type="hidden" name="cat" value="service">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Purchase Price (Rp.):</label>
               <input type="text" class="number form-control " id="judul" placeholder="Example 750000" name="origprice" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Service Fee : </label>
               <input type="text" class="number form-control" id="fee" placeholder="Example 250000" value="" name="fee" required>
            </div>
            <button type="submit" class="theme_button" style="float: right;">Post</button>
        </form>
        </div>
      </div>
  </div>
</div>

<div class="modal fade" id="addmedicine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Medical Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div class="modal-body" style="padding: 15px 15px 60px;"> 
       <form action="<?php echo base_url() ?>service/addprocess" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleInputEmail1">Item Name :</label>
                <input type="text" class="form-control" id="judul" name="nameservice" autocomplete="off" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Sell Price (Rp.) :</label>
               <input type="text" class="number form-control" id="judul" name="price" required>
               <input type="hidden" name="cat" value="medicine">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Purchase Price (Rp.):</label>
               <input type="text" class="number form-control" id="judul" name="origprice" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Unit:</label>
               <input type="text" class="form-control" id="unit" name="unit" placeholder="ml/kg/cc/gr" autocomplete="off" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Stocks (Quantity) :</label>
               <input type="text" class="number form-control" id="judul" name="stocks" required>
            </div>
            <button type="submit" class="theme_button" style="float: right;">Add</button>
        </form>
        </div>
      </div>
  </div>
</div>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
<script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/ajax/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/ajax/datatables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/ajax/datatables/js/dataTables.bootstrap.min.js')?>"></script>
<script src="<?php echo base_url() ?>assets/js/main.js"></script>

<script type="text/javascript">

var table;



    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [[0,"desc"]], //Initial no order.
        "searchable": true,
        "aaSorting": [[0, "desc"]],

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('service/ajax_list')?>",
            "type": "POST",
            "datasrc": "data",
        },


        "columns": [
        { "data": 0 },
        { "data": 1 },
        { "data": 4 },
        { "data": 3 },
        { "data": 2 },
        { "data": 5 },
        <?php if($manageto = array('1','3')){ ?> { "data": 6 }, <?php } ?>
        ],

    });


</script>

</body>

</html>