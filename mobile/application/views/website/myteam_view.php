<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">My Team</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>Team List</h3>
                            <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#adduser">+ Add Vet Staff</button>
                            <button class="icon-tab theme_button color4" data-toggle="modal" data-target="#addstaff">+ Add Admin Staff</button>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> -->
                                               <!--  <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search phone">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button> -->
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                            
                                        </tr>
                                        <?php foreach ($datavets as $vet) {
                                            // $idcustomer = $customer->idcustomer;
                                            // $countpet = $this->Customer_model->petcustomer($idcustomer);

                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <a href="#"><?php echo $vet->name ?></a>
                                               
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="#"><?php echo $vet->email ?></a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $vet->phone ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle">
                                                <?php
                                                $rolemanage = $vet->manageto;
                                                if($rolemanage == '1'){
                                                    echo 'Clinic Owner';
                                                }if($rolemanage == '2'){
                                                    echo 'Vet';
                                                }if($rolemanage == '3'){
                                                    echo 'Admin Staff';
                                                } ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php if($rolemanage == '1'){

                                                }else{ ?>
                                                    <a href="<?php echo base_url() ?>team/edit/?uid=<?php echo $vet->uid ?>&email=<?php echo $vet->email ?>&idclinic=<?php echo $vet->idclinic ?>" class="icon-tab theme_button color3">Edit</a>                                                    
                                                    <button class="icon-tab theme_button color4" data-toggle="modal" data-target="#deleteuid<?php echo $vet->uid ?>"> Delete </button>
                                                <?php } ?>
                                                <div class="modal fade" id="deleteuid<?php echo $vet->uid ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Are you sure delete <?php echo $vet->name ?> ? </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CLOSE</button>
                                                        <a href="<?php echo base_url() ?>team/deleteuser/?uid=<?php echo $vet->uid ?>&email=<?php echo $vet->email ?>&idclinic=<?php echo $vet->idclinic ?>" class="icon-tab theme_button color4">Delete</a>                                                    
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

            <div class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Vet Staff</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" style="padding: 15px 15px 60px;"> 
                   <form action="<?php echo base_url() ?>Team/adduser" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Access Vet Staff:</label><br>
                            <ul>
                               <li>Access dashboard only vet own</li>
                               <li>Pet database: Full Access</li>
                               <li>Pet owner : Full Access</li>
                               <li>Stock Inventory : View Only</li>
                               <li>Invoice : Full Access</li>
                           </ul>
                            <!-- <div class="table-responsive">
                              <input type="radio" name="access" value="owner"> Clinic Owner |   
                            <input type="radio" name="access" value="vet"> Vet Staff |  
                            <input type="radio" name="access" value="admin"> Admin Staff<br><br>
                              <table class="table table-striped table-bordered">
                                <tbody>
                                  <tr>
                                    <th>Dashboard</th>
                                    <th>Pet Record</th>
                                    <th>Medical Record</th>
                                    <th>Owner Data</th>
                                    <th>Stock & inventory</th>
                                    <th>Invoicing</th>
                                  </tr>
                                  <tr class="item-editable">
                                    <td><input type="radio" name="dashboard" value="1"> Full</td>
                                    <td><input type="radio" name="pet" value="1"> Full</td>
                                    <td><input type="radio" name="medical" value="1"> Full</td>
                                    <td><input type="radio" name="owner" value="1"> Full</td>
                                    <td><input type="radio" name="stock" value="1"> Full</td>
                                    <td><input type="radio" name="invoice" value="1"> Full</td>
                                  </tr>
                                  <tr class="item-editable">
                                    <td><input type="radio" name="dashboard" value="2"> Only Own</td>
                                    <td><input type="radio" name="pet" value="2"> Only Own</td>
                                    <td><input type="radio" name="medical" value="2"> Only Own</td>
                                    <td><input type="radio" name="owner" value="2"> Only Own</td>
                                    <td><input type="radio" name="stock" value="2"> Only Own</td>
                                    <td><input type="radio" name="invoice" value="2"> Only Own</td>
                                  </tr>
                                  <tr class="item-editable">
                                    <td><input type="radio" name="dashboard" value="0"> None</td>
                                    <td><input type="radio" name="pet" value="0"> None</td>
                                    <td><input type="radio" name="medical" value="0"> None</td>
                                    <td><input type="radio" name="owner" value="0"> None</td>
                                    <td><input type="radio" name="stock" value="0"> None</td>
                                    <td><input type="radio" name="invoice" value="0"> None</td>
                                  </tr>
                                </tbody>
                              </table>
                            </div><br> -->
                           
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name* :</label>
                            <input type="text" class="form-control" id="judul" name="name" required>
                        </div>                        
                        <div class="form-group">
                           <label for="exampleInputEmail1">Email* :</label>
                           <input type="text" class="form-control " id="email" placeholder="Email for login" name="email" required>
                        </div>
                        <div class="form-group">
                           <label for="exampleInputEmail1">Phone* :</label>
                           <input type="text" class="form-control " id="phone" placeholder="Phone Number" name="phone" required>
                        </div>
                        <div class="form-group">
                           <label for="exampleInputEmail1">Password*: </label>
                           <input type="password" class="form-control" id="fee" placeholder="" value="" name="password" required>
                           <input type="hidden" name="rolemanage" value="2">
                        </div>
                        
                        <button type="submit" class="theme_button" style="float: right;">Add Team</button>
                    </form>
                    </div>
                  </div>
              </div>
            </div>

            <div class="modal fade" id="addstaff" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Admin Staff</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" style="padding: 15px 15px 60px;"> 
                   <form action="<?php echo base_url() ?>Team/adduser" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Role Admin Staff:</label>
                           <ul>
                               <li>Dashboard : None</li>
                               <li>Pet database: Full access register pet, view only medical record, </li>
                               <li>Pet owner : View only</li>
                               <li>Stock Inventory : Full Access</li>
                               <li>Invoice : Full Access</li>
                           </ul>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name* :</label>
                            <input type="text" class="form-control" id="namenya" name="name" required>
                        </div>                        
                        <div class="form-group">
                           <label for="exampleInputEmail1">Email* :</label>
                           <input type="text" class="form-control " id="emailnya" placeholder="Email for login" name="email" required>
                        </div>
                        <div class="form-group">
                           <label for="exampleInputEmail1">Phone* :</label>
                           <input type="text" class="form-control " id="phonenya" placeholder="Phone Number" name="phone" required>
                        </div>
                        <div class="form-group">
                           <label for="exampleInputEmail1">Password*: </label>
                           <input type="password" class="form-control" id="fee" placeholder="" value="" name="password" required>
                           <input type="hidden" name="rolemanage" value="3">
                        </div>
                        
                        <button type="submit" class="theme_button" style="float: right;">Post</button>
                    </form>
                    </div>
                  </div>
              </div>
            </div>


