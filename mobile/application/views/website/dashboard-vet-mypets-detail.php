        <div class="pages">
          <div data-page="dashboard-vet-mypets-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>assets/images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="#" onclick="window.location='<?php echo base_url() ?>dashboard/petowner'" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <br ?><div class="custom-image" align="center"><img src="<?php echo $petphoto ?>" alt="" title="" /></div>
                <h2 class="page_title"><?php echo $namepet ?> <a href="<?php echo base_url() ?>pet/edit/?idpet=<?php echo $idpet ?>" onclick="window.location='<?php echo base_url() ?>pet/edit/?id=<?php echo $idpet ?>';"><i class="fas fa-pen"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row">
                      <a href="#tabinfo-pet" class="tab-link active button">Pet Info</a>
                      <a href="#tabinfo-diary" class="tab-link button">Pet Diary</a>
                      <a href="#tabinfo-clinic" class="tab-link button">Clinic Info</a>
                  </div>
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabinfo-pet" class="tab active">
                        <ul class="iconed-list">
                          <li><i class="fas fa-paw mr-1"></i><b class="mr-2">Microchip ID:</b><?php
                              if($rfid == '0'){ ?>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                              <?php }else{ 
                                 echo '<span>'.$rfid.'</span>' ; 
                              }?>
                          </li>
                          <li><i class="fas fa-cat mr-1"></i><b class="mr-2">Type:</b><span></strong> <?php echo $tipe; ?></span></li>
                          <li><i class="fas fa-venus-mars mr-1"></i><b class="mr-2">Gender:</b><span><?php echo $gender; ?></span></li>
                          <li><i class="fas fa-arrows-alt mr-1"></i><b class="mr-2">Breed:</b><span><?php echo $breed; ?></span></li>
                          <li><i class="fas fa-palette mr-1"></i><b class="mr-2">Color:</b><span><?php echo $color; ?></span></li>
                          <li><i class="fas fa-birthday-cake mr-1"></i><b class="mr-2">Date of Birth:</b><span><?php echo date('d M Y', strtotime($datebirth)); ?></span></li>
                          <li><i class="fas fa-calendar-alt mr-1"></i><b class="mr-2">Age:</b><span><?php echo $age; ?></span></li>
                          <li><i class="fas fa-map-marked-alt mr-1"></i><b class="mr-2">Country of Origin:</b><span><?php echo $country; ?></span></li>
                          <li><i class="fas fa-syringe mr-1"></i><b class="mr-2">Last Vaccination:</b><span><?php if (count($lastvaccine) == '0'){
                                          echo '-';
                                                                }else{ ?>
                                                                     <?php echo $lastvaccine[0]->vaccine; ?> - <?php echo date('d M, Y',strtotime($lastvaccine[0]->datevacc)) ?>
                                                                <?php } ?></span></li>
                        </ul>
                      </div>
                      <div id="tabinfo-diary" class="tab">
                        <ul class="timeline-list">
                          <li class="timeline-list-add">
                            <a data-popup=".popup-diary" class="open-popup close-panel simple-link mb-3" href="#" 
                            onclick="document.getElementById('iddairy').value='0';document.getElementById('idpet').value='<?php echo $this->input->get('idpet') ?>';">
                            <span class="btn btn-sm btn-secondary btn-round mr-1"><i class="fas fa-plus"></i></span> Add New Diary Entry</a>
                          </li><br />
                        <?php  $dt_diary = $this->Pet_model->getdiary($this->input->get('idpet')); ?>
                        <?php if(count($dt_diary)>0) { ?>
                        <?php foreach($dt_diary as $val){ ?>
                          <li>
                            <div class="timeline-list-index"><?php echo date("d M, Y",strtotime($val->date)); ?></div>
                            <div class="timeline-flex">
                              <div class="timeline-info mb-2">
                                <p class="mb-1"><?php echo $val->notes; ?></p>
                              </div>
                              <div class="timeline-buttons mb-2">
                                <a data-popup=".popup-diary" class="open-popup close-panel btn btn-sm btn-round btn-primary mr-1" href="#" 
                                onclick="document.getElementById('iddairy').value='<?php echo $val->id ?>';document.getElementById('idpet').value='<?php echo $this->input->get('idpet') ?>';document.getElementById('message').value='<?php echo $val->notes ?>';">
                                <i class="fas fa-pen"></i></a>
                                <a class="btn btn-sm btn-round btn-light" href="#" onclick="if(confirm('Delete selected diary ?')){window.location='<?php echo base_url() ?>pet/diary/?act=delete&id=<?php echo $val->id ?>&idpet=<?php echo $this->input->get('idpet') ?>';}else{return;}">
                                <i class="fas fa-trash"></i></a>
                              </div>
                            </div>
                          </li>
                         <?php } } ?>
                        </ul>
                      </div>
                      <div id="tabinfo-clinic" class="tab" align="center" >
                        <?php if($namaclinic==""){ ?>
                        <br /><br /><button class="btn btn-modern btn-primary" style="background-color:#e64d5f;" onclick="window.location='<?php echo base_url() ?>owner/VetSearch';" >Add Clinic</button><br /><br />
                        <?php }else{ ?>
                        <ul class="iconed-list">
                          <li><i class="fas fa-clinic-medical mr-1"></i><b class="mr-2">Clinic Name:</b><span><?php echo $namaclinic; ?></span></span></li>
                          <li><i class="fas fa-map-marker-alt mr-1"></i><b class="mr-2">Address:</b><span><?php echo $addressclinic; ?></span></li>
                          <li><i class="fas fa-city mr-1"></i><b class="mr-2">City:</b><span><?php echo $cityclinic; ?></span></li>
                          <li><i class="fas fa-globe-asia mr-1"></i><b class="mr-2">Country:</b><span><?php echo $countryclinic; ?></span></li>
                          <li><i class="fas fa-envelope mr-1"></i><b class="mr-2">Email:</b><span><a href="mailto:<?php echo $emailclinic; ?>" class="simple-link"><?php echo $emailclinic; ?></a></span></li>
                          <li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><a href="tel:<?php echo $phoneclinic; ?>" class="simple-link"><?php echo $phoneclinic; ?></a></span></li>
                        </ul>
                        <?php  }  ?>
                      </div>
                    </div>
                  </div>
                  <div class="buttons-row">
                      <a href="#tabhistory-medical" class="tab-link active button">Medical History</a>
                      <a href="#tabhistory-vaccine" class="tab-link button">Vaccine History</a>
                  </div>
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabhistory-medical" class="tab active">
                        <ul class="timeline-list">
                          
                          

                          <?php foreach ($logpet as $log) { ?>
                            <li>
                              <div class="timeline-list-index"><a href="#"  onclick="window.location='<?php echo base_url() ?>pet/logdetail/?idlog=<?php echo $log->idlog ?>&idpet=<?php echo $log->idpet ?>';" class="simple-link"><?php echo date('d M, Y ',strtotime($log->tgllog,strtotime('+7 hour'))) ?></a></div>
                              <div class="timeline-list-sub">By: <?php echo $log->namevets; ?></div>
                              <p><?php if($log->diagnose == ''){
                                      echo $log->anamnesis;
                                      }else{
                                      echo $log->diagnose;
                                      } ?></p>
                            </li>
                          <?php } ?>

                          <li>
                            <div class="timeline-list-index"><a class="simple-link" href="#" ><?php echo date('d M, Y ',strtotime($dateadd,strtotime('+7 hour'))) ?></a></div><div class="timeline-list-sub">Registered Date</div>
                          </li>
                         
                        </ul>
                      </div>
                      <div id="tabhistory-vaccine" class="tab">
                        <ul class="timeline-list">
                          <?php foreach ($logvaccine as $vacc) { ?>
                            <li>
                              <div class="timeline-list-index"><a href="#" onclick="window.location='<?php echo base_url() ?>pet/vaccdetail/?idvacc=<?php echo $vacc->idvacc ?>&idpet=<?php echo $this->input->get('idpet') ?>';" class="simple-link"><?php $tglvacc = date('d M, Y',strtotime($vacc->datevacc)); echo $tglvacc ;?></a></div>
                              <div class="timeline-list-sub">By: <?php echo $vacc->vetname; ?></div>
                              <p><?php echo $vacc->vaccine; ?></p>
                            </li>
                          <?php } ?>
                          <li>
                            <div class="timeline-list-index"><a class="simple-link" href="#" ><?php echo date('d M, Y ',strtotime($dateadd,strtotime('+7 hour'))) ?></a></div><div class="timeline-list-sub">Registered Date</div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              