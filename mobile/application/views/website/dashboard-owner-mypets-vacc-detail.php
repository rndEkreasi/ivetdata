        <div class="pages">
          <div data-page="dashboard-owner-mypets-log-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <?php foreach ($datavacc as $log) { ?>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="#" onclick="window.location='<?php echo base_url() ?>pet/detail/?idpet=<?php echo $_GET['idpet'] ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title"><?php echo $namepet ?> [<?php echo date('d M Y',strtotime($log->datevacc)); ?>]</h2>
                <div class="page_single layout_fullwidth_padding">
                  <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                  <h3 class="page_subtitle">Vaccine History Detail</h3>
                  <table class="custom_table mb-3">
                    <thead>
                      <tr>
                        <th>DESCRIPTION</th>
                        <th>VALUE</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Date</td>
                        <td><?php echo date('d M Y',strtotime($log->datevacc)); ?></td>
                      </tr>
                      <tr>
                        <td>Vet</td>
                        <td><?php echo $log->vetname; ?></td>
                      </tr>
                      <tr>
                        <td>Vaccine</td>
                        <td><ul><?php if($log->vaccine<>""){$value = explode("<br>",$log->vaccine);foreach($value as $val){ ?><li><?php echo $val; ?></li>
						<?php } } ?></ul></td>
                      </tr>
                      <tr>
                        <td>Injection Site</td>
                        <td>                        <?php
                                                        if($log->description == ''){
                                                            echo '-';
                                                        }else{
                                                            echo $log->description;
                                                        } ?></td>
                      </tr>
                                                        <?php
                                                        $nextdate = $log->nextdate;                                                                                                
                                                        if($nextdate == '0000-00-00 00:00:00'){
                                                            $fixdate = date('Y-m-d H:i:s');
                                                        }else{
                                                            $fixdate = $nextdate;
                                                        } ?>
                      <tr>
                        <td>Next Date</td>
                        <td><?php echo date('m/d/Y', strtotime($fixdate)); ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php } ?>