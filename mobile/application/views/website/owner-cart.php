        <div class="pages">
          <div data-page="dashboard-owner-invoice" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">SHOPPING CART</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="cartcontainer">
                    <div class="cart_item" id="cartitem1">
                      <div class="item_title"><span>01.</span> Inpepsa</div>
                      <div class="item_price">Rp. 75,000</div>
                      <div class="item_thumb"><a href="shop-item.html" class="close-panel"><img src="<?php echo base_url() ?>images/uploads/thumbs/inpepsa.jpg" alt="" title="" /></a></div>
                      <div class="item_qnty">
                        <form id="myform" method="POST" action="#">
                          <label>QUANTITY</label>
                          <input type="button" value="-" class="qntyminus" field="quantity1" />
                          <input type="text" name="quantity1" value="4" class="qnty" />
                          <input type="button" value="+" class="qntyplus" field="quantity1" />
                        </form>
                      </div>
                      <a href="#" class="item_delete" id="cartitem1"><img src="<?php echo base_url() ?>images/icons/black/trash.png" alt="" title="" /></a>
                    </div>
                    <div class="cart_item" id="cartitem2">
                      <div class="item_title"><span>02.</span> Prednisolone</div>
                      <div class="item_price">Rp. 500,000</div>
                      <div class="item_thumb"><a href="shop-item.html" class="close-panel"><img src="<?php echo base_url() ?>images/uploads/thumbs/prednisolone.jpg" alt="" title="" /></a></div>
                      <div class="item_qnty">
                        <form id="myform" method="POST" action="#">
                          <label>QUANTITY</label>
                          <input type="button" value="-" class="qntyminus" field="quantity2" />
                          <input type="text" name="quantity2" value="1" class="qnty" />
                          <input type="button" value="+" class="qntyplus" field="quantity2" />
                        </form>
                      </div>
                      <a href="#" class="item_delete" id="cartitem2"><img src="<?php echo base_url() ?>images/icons/black/trash.png" alt="" title="" /></a>
                    </div>
                    <div class="cart_item" id="cartitem3">
                      <div class="item_title"><span>03.</span> Vanguard Plus 5/CV</div>
                      <div class="item_price">Rp. 250,000</div>
                      <div class="item_thumb"><a href="shop-item.html" class="close-panel"><img src="<?php echo base_url() ?>images/uploads/thumbs/vanguardplus.jpg" alt="" title="" /></a></div>
                      <div class="item_qnty">
                        <form id="myform" method="POST" action="#">
                          <label>QUANTITY</label>
                          <input type="button" value="-" class="qntyminus" field="quantity3" />
                          <input type="text" name="quantity3" value="1" class="qnty" />
                          <input type="button" value="+" class="qntyplus" field="quantity3" />
                        </form>
                      </div>
                      <a href="#" class="item_delete" id="cartitem3"><img src="<?php echo base_url() ?>images/icons/black/trash.png" alt="" title="" /></a>
                    </div>
                    <div class="carttotal">
                      <div class="carttotal_row">
                        <div class="carttotal_left">CART TOTAL</div>
                        <div class="carttotal_right">Rp. 1,050,000</div>
                      </div>
                      <div class="carttotal_row">
                        <div class="carttotal_left">TAX (15%)</div>
                        <div class="carttotal_right">Rp. 157,500‬</div>
                      </div>
                      <div class="carttotal_row_last">
                        <div class="carttotal_left">TOTAL</div>
                        <div class="carttotal_right">Rp. 1,207,500‬</div>
                      </div>
                    </div>
                    <a href="#" class="button_full btyellow" onclick="window.location='<?php echo base_url() ?>cart/checkout';">PROCEED TO CHECKOUT</a>
                  </div>
                </div>
              </div>
