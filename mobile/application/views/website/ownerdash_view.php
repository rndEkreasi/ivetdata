        <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Pet</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo $namaowner ?>'s Pet List</h3>
                            <a href="<?php echo base_url() ?>pet/addbyowner" class="icon-tab theme_button color3">+ Add Pet</a>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error;unset($_SESSION['error']); ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success;unset($_SESSION['success']); ?></div>
                                  <?php } ?>
                            <div class=" with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                           <form method="get" class="" action="">
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="rfid" class="form-control" placeholder="Search MICROCHIP ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->

                                <?php if(count($datapet)>0){ ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No </th>
                                            <th>Microchip ID</th>
                                            <th>Pet Name</th>
                                            <th>Type</th>
                                            <th>Breed</th>
                                            <th>Age</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php 
                                        $ij = 1;
                                        foreach ($datapet as $pet) { 
                                            $tipe = $pet->tipe;
                                            $photo = $pet->photo;
                                            
                                            if($photo =='' ){
                                                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            }else{
                                                $petphoto = $photo;
                                            }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td><?php echo $ij++ ?></td>
                                            <td class="media-middle">
                                                <?php $microchip = $pet->rfid;
                                                if($microchip == '0'){ ?>
                                                    <a href="https://tawk.to/chat/5c792599a726ff2eea5a1b14/default">Buy Microchip</a>
                                                <?php } else{ ?>
                                                    <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>"><?php echo $pet->rfid ?></a>
                                                <?php }?>
                                                
                                               
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <?php echo $pet->namapet ?>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $pet->tipe; ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle" style="">
                                                <?php echo $pet->breed ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php 
                                                    $datebirth = $pet->datebirth; 
                                                    $agey = date_diff(date_create($datebirth), date_create('now'))->y;
                                                    $agem = date_diff(date_create($datebirth), date_create('now'))->m;
                                                    echo $agey.' Year, '.$agem.' Month ';
                                                ?>
                                            </td>
                                            <td class="media-middle">
                                            <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>" class="icon-tab theme_button color2">View</a>
                                            <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deleteservice599"> Delete </button>
                                                <div class="modal fade" id="deleteservice599" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"><?php echo $pet->namapet ?> will be deleted <br>Are you sure?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">×</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                    <a href="/pet/delete/?idpet=<?php echo $pet->idpet ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div><?php } ?>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php // echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>
            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                        <!-- <div class="col-sm-6 text-sm-right">
                            <p class="grey">PT. IVET DATA GLOBAL </p>
                        </div> -->
                    </div>
                </div>
            </section>