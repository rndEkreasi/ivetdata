        <div class="pages">
          <div data-page="dashboard-owner-appointments" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';">
                      <img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" onclick="history.back(1);" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">APPOINTMENTS LIST 
                  <a id="add" href="#" onclick="window.location='<?php echo base_url() ?>appointment/add';" class=""><i class="fas fa-plus" style="font-size:25px;color:#82b808;"></i></a>
                </h2>
                
                <div class="page_single layout_fullwidth_padding">
                  <?php if(isset($success)){ ?>
                    <div class="success"><?php echo $success ?></div>
                  <?php }?>
                  <?php if(isset($error)){ ?>
                    <div class="error"><?php echo $error ?></div>
                  <?php }?>
                  <div class="list-block">
                    <!-- <div class="searchbox mb-3">
                      <form>
                        <input type="text" name="search" value="" placeholder="Search" />
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </form>
                    </div> -->
                    <ul class="posts doaitems">
                    <?php if(count($dataappoint)==0){ ?><script>window.location="<?php echo base_url() ?>appointment/add";</script><?php } ?>
                     <?php
                      //var_dump($dataappoint);
                      foreach ($dataappoint as $appoint ) {
                     ?>
                        <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3><?php echo $appoint->namapet ?></h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><?php echo date("d M Y",strtotime($appoint->nextdate)) ?><br /><?php echo date("H:i",strtotime($appoint->nextdate)) ?></p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p><b>Vet: <?php echo $appoint->name ?></b></p>
                                    <p class="pb-0"><?php echo $appoint->nameclinic ?><br><?php echo $appoint->address ?></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p>
                                      <a href="tel:<?php echo $appoint->phone ?>" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:<?php echo $appoint->email ?>" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="<?php echo base_url() ?>images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <!-- <a href="#" onclick="window.location='<?php echo base_url() ?>appointment/detail/<?php echo $appoint->idvacc ?>'" class="action1"><i class="fas fa-eye"></i></a> -->
                          <a href="#" onclick="window.location='<?php echo base_url() ?>appointment/edit/<?php echo $appoint->idvacc ?>'" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="#" onclick="window.location='<?php echo base_url() ?>appointment/delete/<?php echo $appoint->idvacc ?>'" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <?php 
                      }
                    ?>
                    </ul>
                   <!--  <div class="loadmore loadmore-doaitems">LOAD MORE</div>
                    <div class="showless showless-doaitems">END</div> -->
                  </div>
                </div>
              </div>
