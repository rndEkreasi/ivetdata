<div class="pages">
  <div data-page="contact" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>'"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <h2 class="page_title">Contact Us</h2>
        <div class="page_single layout_fullwidth_padding">

          <h2 id="Note"></h2>
          <div class="contactform mb-4">
            <form class="cmxform" id="ContactForm" method="post" action="">
              <label>Name:</label>
              <input type="text" name="ContactName" id="ContactName" value="" class="form_input required" />
              <label>Email:</label>
              <input type="text" name="ContactEmail" id="ContactEmail" value="" class="form_input required email" />
              <label>Message:</label>
              <textarea name="ContactComment" id="ContactComment" class="form_textarea textarea required" rows=""
                cols=""></textarea>
              <input type="submit" name="submit" class="form_submit" id="submit" value="Send" />
              <input class="" type="hidden" name="to" value="cs@ivetdata.com" />
              <input class="" type="hidden" name="subject" value="Contact form message" />
              <label id="loader" style="display:none;"><img src="images/loader.gif" alt="Loading..."
                  id="LoadingGraphic" /></label>
            </form>
          </div>

          <h3>Our Location</h3>

          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5011824922385!2d106.74599881476905!3d-6.197412795513501!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f722935113d1%3A0xaddb3d5b571d682f!2siVetData+%C2%AE!5e0!3m2!1sen!2sid!4v1564997002074!5m2!1sen!2sid"
            width="100%" height="200" frameborder="0" style="border:0">
          </iframe>

          <p class="mt-2 mb-4">
            Adress: Ruko Kebon Jeruk A9-3, West Jakarta 11630 <br />
            Email: <a class="simple-link" href="mailto:cs@ivetdata.com">cs@ivetdata.com</a> <br />
            Mobile: <a class="simple-link" href="tel:+90045656777">+900 456 567 77</a>
          </p>

          <a href="tel:+900 456 567 77" class="button_full btyellow external">Call Us Now!</a>

          <div class="clear"></div>
        </div>
      </div>
     