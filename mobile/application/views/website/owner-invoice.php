        <div class="pages">
          <div data-page="dashboard-owner-invoice" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" onclick="window.location='<?php echo base_url() ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title"><?php echo $nama ?>'s invoice list</h2>
                <?php if (isset($error)){ ?>
                  <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php } ?>
                <div class="page_single layout_fullwidth_padding">
                  <div class="list-block">
                    <div class="searchbox mb-3">
                      <form>
                        <input type="text" name="search" value="" placeholder="Search" />
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </form>
                    </div>
                    <ul class="posts doiitems">
                      <?php foreach ($datasales as $invoice) {
                        $status = $invoice->status;
                        if($status == '0'){
                          $statusfix = 'Unpaid';
                        }else{
                          $statusfix = 'Paid';
                        }
                       ?>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" onclick="window.location='<?php echo base_url() ?>owner/detailinvoice/?invoice=<?php echo $invoice->invoice ?>';"><?php echo $invoice->invoice; ?></a></h4>
                                  <p><b>Rp. <?php echo number_format($invoice->totalprice) ?></b> - <?php echo $statusfix ?></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><?php echo date('d M Y',strtotime($invoice->invdate)) ?></p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="<?php echo base_url() ?>images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="#" onclick="window.location='<?php echo base_url() ?>owner/detailinvoice/?invoice=<?php echo $invoice->invoice ?>';" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="#" onclick="window.location='<?php echo base_url() ?>owner/deleteinvoice/?invoice=<?php echo $invoice->invoice ?>';" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <?php } ?>
                    </ul>
                    <!-- <div class="loadmore loadmore-doiitems">LOAD MORE</div>
                    <div class="showless showless-doiitems">END</div>-->
                  </div>
                </div>
              </div>
