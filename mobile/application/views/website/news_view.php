<body>
            <div role="main" class="main">
				
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">News</h1>
						</div>
					</div>
				</div>
			</section>
			<section class="section bg-color-quaternary custom-padding-3 border-0 my-0">
					<div class="container">
						<div class="row justify-content-center">
						    <?php foreach($allarticle as $row){  ?>
							    <div class="col-md-6 col-lg-4 mb-5">
								    <article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
									    <div class="thumb-info-wrapper m-0">
										    <a href="/news/detail/<?php echo $row->slug;  ?>"><img src="<?php if(file_get_contents($row->featuredimg)){echo $row->featuredimg;}else{echo base_url().'images/news.png'}  ?>" class="img-fluid"></a>
									    </div>
									    <div class="thumb-info-caption custom-padding-4 d-block">
										    <span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo date("d M Y",strtotime($row->publishdate));   ?></span>
										    <h3 class="custom-primary-font text-transform-none text-5 mb-3"><a href="/news/detail/<?php echo $row->slug;  ?>" class="text-decoration-none custom-link-style-1"><?php echo substr($row->title,0,40)."<span style='white-space:nowrap;'> . . .</span>";  ?></a></h3>
										    <span class="thumb-info-caption-text text-3 p-0 m-0"><?php  echo substr(strip_tags($row->description),0,132); ?> <span style='white-space:nowrap;'>. . .</span>
									    </div>
								    </article>
							    </div>
							<?php   }  ?>
						</div>
					</div>
				</section>
