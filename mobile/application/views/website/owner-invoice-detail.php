        <div class="pages">
          <div data-page="dashboard-owner-invoice-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                   <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Detail -->
              <?php foreach ($datainvoice as $invoice) { 
                $status = $invoice->status;

                ?>
              <div class="invoice-detail" id="pages_maincontent">
                <a href="#" onclick="window.location='<?php echo base_url() ?>owner';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>

                <h2 class="page_title pt-3">Invoice: #<?php echo $invoice->invoice; ?></h2>
                <div class="invoice">
                  <div class="invoice_info mb-4">
                    <div class="invoice_info_top mb-3"><span>Rp. <?php echo number_format($invoice->totalprice) ?></span></div>
                    <div class="invoice_info_left">
                      <!-- <div class="invoice_info_name">Invoice from:</div>
                      <div class="invoice_from">
                        <div class="image"><img src="<?php echo base_url() ?>images/clinics/1.png" /></div>
                        <span>Tail Mail's Clinic</span>
                      </div> -->
                    </div>
                    <div class="invoice_info_right">
                      <div class="invoice_info_name">Invoice to:</div>
                      <div class="invoice_to">
                        <div><b>Name:</b><span><?php echo $invoice->nameclient; ?></span></div>
                        <div><b>Phone:</b><span><a href="tel:<?php echo $invoice->phoneclient; ?>" class="simple-link"><?php echo $invoice->phoneclient; ?></a></span></div>
                        <div><b>Email:</b><span><a href="mailto:<?php echo $invoice->emailclient; ?>" class="simple-link"><?php echo $invoice->emailclient; ?></a></span></div>
                        <div><b>Date:</b><span><?php echo date('d M Y', strtotime($invoice->invdate))?></span></div>
                        <!-- <div><b>Pet Name:</b><span>Jasmine</span></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="page_single layout_fullwidth_padding">
                    <div class="mb-3">Payment Status: 
                      <?php if ($status == '1'){ ?>
                        <span class="label label-green">PAID</span>
                      <?php }if($status == '0'){ ?>
                        <span class="label label-danger">UNPAID</span>
                      <?php } ?>
                    </div>
                      
                    <table class="custom_table mb-3">
                      <thead>
                        <tr>
                          <th>ITEM</th>
                          <th>PRICE</th>
                          <th>QTY</th>
                          <th>TOTAL</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($iteminvoice as $item ) { 
                          $price = $item->price;
                          $qty = $item->qty;
                          $totalprice = $price * $qty;                                               
                        ?> 
                        <tr>
                          <td><?php echo $item->item ?></td>
                          <td class="text-right" nowrap><b>Rp. <?php echo number_format($price) ; ?></b></td>
                          <td><?php echo number_format($qty) ;  ?></td>
                          <td class="text-right" nowrap>Rp. <?php echo number_format($totalprice); ?></td>
                        </tr>
                        <?php } ?>
                       <!--  <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Sub Total</b></td>
                          <td class="text-right" nowrap>Rp. 350,000</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Discount</b></td>
                          <td class="text-right" nowrap>Rp. 0</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Tax</b></td>
                          <td class="text-right" nowrap>Rp. 0</td>
                        </tr> -->
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Grand Total</b></td>
                          <td class="text-right" nowrap>Rp. <?php echo number_format($invoice->totalprice) ?></td>
                        </tr>
                      </tbody>
                    </table>
                    <p>Note*: </p>
                    <?php } ?>
                  </div>
                </div>
              </div>
