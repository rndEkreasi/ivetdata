        <div class="pages">
          <div data-page="dashboard-owner-appointments" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';">
                      <img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
            <?php if(count($dataclinic)==0){ echo "<script>alert('Please add your clinic\'s list first');window.location='".base_URL()."pet/addvetbyowner';</script>"; } ?>  
            <?php if(count($datapet)==0){ echo "<script>alert('Please add your pet\'s list first');window.location='".base_URL()."pet/addbyowner';</script>"; } ?>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" onclick="history.back(1);" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Add Appointment</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form name="form1" class="form-horizontal" action="<?php echo base_url() ?>appointment/addprocess" method="post">
                      <div class="form_row required">
                        <label>Clinic Name:</label>
                        <select name="idclinic" class="form_select" style="width:90%;" onchange="window.location='?idclinic=' + this.options[this.selectedIndex].value;" required>
                          <?php 
                          if(isset($_GET['idclinic'])){
                              $detailclinic = $this->Clinic_model->detailclinic($idclinic);
                            ?>
                        <option value="<?php echo $detailclinic[0]->idclinic ?>"><?php echo $detailclinic[0]->nameclinic ?></option>
                        <?php }else{ ?>
                        <option value="0">--- Please select Clinic ---</option>
                        <?php
                          foreach ($dataclinic as $clinic) { 
                                $idclinic = $clinic->idclinic;
                                $detailclinic = $this->Clinic_model->detailclinic($idclinic);
                                if(count($detailclinic)==0) break;
                          ?>
                            <option value="<?php echo $idclinic ?>" <?php if(isset($_GET['idclinic'])&&$_GET['idclinic']==$idclinic){ echo "selected"; } ?>>
                            <?php echo $detailclinic[0]->nameclinic ?></option>
                          <?php } } ?>
                        </select>
                        &nbsp;&nbsp;<a href="#" onclick="window.location='<?php echo base_url() ?>owner/VetSearch';"><i class="fas fa-plus" style="font-size:25px;color:#82b808;"></i></a>
                      </div>
                      <div class="form_row required">
                        <label>Select Vet:</label>
                        <select name="idvet" class="form_select" style="width:90%;" required>
                            <option value="0">--- Please select vet ---</option>
                          <?php
                            foreach ($listvet as $vet) { ?>
                            <option value="<?php echo $vet->uid ?>"><?php echo $vet->name ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form_row required">
                        <label>Pet Name:</label>
                        <select name="idpet" class="form_select" style="width:90%;" required>
                            <option value="0">--- Please select your pet ---</option>
                          <?php foreach ($datapet as $pet) { ?>
                            <option value="<?php echo $pet->idpet ?>"><?php echo $pet->namapet ?></option>
                          <?php } ?>
                        </select>
                        &nbsp;&nbsp;<a href="#" onclick="window.location='<?php echo base_url() ?>pet/addbyowner';"><i class="fas fa-plus" style="font-size:25px;color:#82b808;"></i></a>
                      </div>
                      <div class="form_row required">
                        <label>Appointment Time:</label>
                        <div class="datetimepicker_contain mb-3">
                          <input type="date" value="<?php echo date('m/d/Y') ?>" name="nextdate" class="datetimepicker-input" id="datetimepicker-doaa" placeholder="Select Date.." required>
                          &nbsp;Hour <select name="hour"><?php for($i=0;$i<24;$i++){ ?><option value="<?php if($i<10) echo "0" ?><?php echo $i ?>"><?php if($i<10) echo "0" ?><?php echo $i ?></option><?php } ?></select>
                          &nbsp;Minute <select name="minute"><?php for($i=0;$i<61;$i++){ ?><option value="<?php if($i<10) echo "0" ?><?php echo $i ?>"><?php if($i<10) echo "0" ?><?php echo $i ?></option><?php } ?></select>
                        </div>
                      </div>
                      <div class="form_row">
                        <label>Note:</label>
                        <textarea name="description" class="form_textarea" rows="" cols=""></textarea>
                        <input type="hidden" name="idclinic" value="<?php echo $idclinic; ?>">
                      </div>
                      <input type="button" name="addappointment" class="form_submit" onclick="document.form1.submit();" value="Add Appointment" />
                    </form>
                  </div>
                </div>
              </div>
             <!--  <?php // include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div> -->