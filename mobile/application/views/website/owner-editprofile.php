<style>
    #rotater {
          transition: all 0.3s ease;
          border: 0.0625em solid black;
          border-radius: 3.75em;
    }
</style>
        <div class="pages">
          <div data-page="dashboard-owner-editprofile" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="<?php echo base_url() ?>" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit My Profile</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <div class="buttons-row">
                        <a href="#tabeditowner-info" class="tab-link active button"><i class="fas fa-user mr-2"></i> My Profile</a>
                        <a href="#tabeditowner-password" class="tab-link button"><i class="fas fa-key mr-2"></i> Password</a>
                    </div>
                    <div class="tabs-simple">
                      <div class="tabs">
                        <div id="tabeditowner-info" class="tab active">
                          <form method="post" action="<?php echo base_url() ?>profile/updateinfo" enctype="multipart/form-data">
                            <div class="form_row">
                              <label>Profile Picture (max. size 5MB):</label>
                              <div class="preview-image" align="center">
                                  <table width="100%"><tr>
                                  <td valign="top">
                                <div style="visibility:hidden;display: inline-block;white-space: nowrap;" id="conbutton">
    								<img onclick="rotateimg('myImg');"  src="<?php echo base_url() ?>images/rotate_right.png" />
    								<img onclick="i=0;resetimg('myImg');document.getElementById('rotate').value='0';" src="<?php echo base_url() ?>images/reset.png" />
    								<img onclick="rotateimgrev('myImg');" src="<?php echo base_url() ?>images/rotate_left.png"  /><br /><br /><br /><br /><br /><br />
    							</div></td>
    							<td><img src="<?php echo $profilephoto ?>" id="myImg" alt="Profile Picture" title="Profile Picture" style="margin-left:-20px;margin-bottom:25px;" /></td>
                                  </tr></table>
                                  
                              </div>
                              <input type="file" name="profilepic" id="inputimage" value="" class="form_input" />
                              <p>Image resolution size must be 300px x 300px</p>
                            </div>
                            <div class="form_row">
                              <label>Full Name:</label>
                              <input type="text" name="fullname" value="<?php echo $nama ?>" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Phone Number:</label>
                              <input type="text" name="phone" value="<?php echo $phone ?>" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Email Address:</label>
                              <input type="text" name="email" value="<?php echo $email ?>" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Address:</label>
                              <textarea name="homeaddress" class="form_textarea" rows="" cols=""><?php echo $homeaddress ?></textarea>
                            </div>
                            <div class="form_row">
                              <label>City:</label>
                              <input type="text" name="homecity" value="<?php echo $city ?>" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Country:</label>
                             <input type="text" name="homecountry" value="<?php echo $country ?>" class="form_input" />
                            </div>
                            <div class="form_row">
							<label>Display Information in Public Pet Database :</label>
								<input type="checkbox" name="permission[]" value="0" <?php if($public_info[0]==1)echo "checked"; ?>>Full Name<br>
								<input type="checkbox" name="permission[]" value="1" <?php if($public_info[1]==1)echo "checked"; ?>>Phone<br>
								<input type="checkbox" name="permission[]" value="2" <?php if($public_info[2]==1)echo "checked"; ?>>Email<br><br>
							</div>
                            <input type="hidden" name="rotate" id="rotate" value="0" />
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Profile" />
                          </form>
                        </div>
                        <div id="tabeditowner-password" class="tab">
                          <form method="post" action="<?php echo base_url() ?>profile/updatepass" enctype="multipart/form-data">
                            <div class="form_row">
                              <label>Old Password:</label>
                              <div class="input-custom"><input type="password" name="oldpass" value="" class="form_input required" placeholder="Old Password" id="showoldpasswordinput" /><i class="iconbtn fas fa-eye" id="showoldpasswordtrigger" onClick="showPassword('showoldpasswordinput', 'showoldpasswordtrigger')"></i></div>
                            </div>
                            <div class="form_row">
                              <label>New Password:</label>
                              <div class="input-custom"><input type="password" name="newpass" value="" class="form_input required" placeholder="New Password" id="shownewpasswordinput" /><i class="iconbtn fas fa-eye" id="shownewpasswordtrigger" onClick="showPassword('shownewpasswordinput', 'shownewpasswordtrigger')"></i></div>
                            </div>
                            <div class="form_row">
                              <label>Repeat Password:</label>
                              <div class="input-custom"><input type="password" name="repeatpass" value="" class="form_input required" placeholder="Repeat Password" id="showpasswordrepeatinput" /><i class="iconbtn fas fa-eye" id="showpasswordrepeattrigger" onClick="showPassword('showpasswordrepeatinput', 'showpasswordrepeattrigger')"></i></div>
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Password" />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
	<script>
    	var i = 0;
    	
    	function rotateimg(e){
    		i = i + 90;
    		document.getElementById(e).style.transform="rotate(" + i + "deg)";
    		document.getElementById('rotate').value=i;
    		if(i==360)i=0;
    	}
    	
    	function rotateimgrev(e){
    		i = i - 90;
    		document.getElementById(e).style.transform="rotate(" + i + "deg)";
    		document.getElementById('rotate').value=i;
    		if(i==-360)i=0;
    	}
    	
    	function resetimg(e){
    		i = 0 - i;
    		document.getElementById(e).style.transform="rotate(" + i + "deg)";
    		document.getElementById('rotate').value=i;
    		if(i==-360)i=0;
    	}
    	
    	function handleLocalFile(file) {
    		if (file.type.match(/image.*/)) {
    			var reader = new FileReader();
    			reader.onload = function (e) {
    				//detectNewImage(e.target.result, async);
    				document.getElementById("myImg").src=reader.result;
    				document.getElementById("myImg").style.marginLeft='0px';
    				document.getElementById("myImg").style.marginBottom='0px';
    				//document.getElementById("myImg").style.width='350px';
    				//document.getElementById("myImg").style.height='350px';
    				document.getElementById("conbutton").style.visibility='visible';
    				i=0;			
    			};
    			reader.readAsDataURL(file);
    		}
    	}
    
    	document.getElementById("inputimage").addEventListener("change", function (e) {
    		var files = this.files;
    		if (files.length)
    			handleLocalFile(files[0]);
    	});
    
    	
	</script>
