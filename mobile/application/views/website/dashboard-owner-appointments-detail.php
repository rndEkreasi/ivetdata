        <div class="pages">
          <div data-page="dashboard-owner-appointments" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';">
                      <img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" onclick="history.back(1);" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Appointment</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">

                    <form>
                      <div class="form_row required">
                        <label>Pet Name:</label>
                        <input type="text" name="petname" value="<?php echo $dataappoint[0]->namapet ?>" class="form_input" disabled />
                      </div>
                      <div class="form_row required">
                        <label>Vet Name:</label>
                        <select name="idvet" class="form_select">
                            <option value="<?php echo $dataappoint[0]->uid ?>"><?php echo $dataappoint[0]->name ?></option>
                        </select>
                      </div>
                      <div class="form_row required">
                        <label>Appointment Date:</label>
                        <div class="datetimepicker_contain datetimepicker_disabled mb-3">
                          <input type="date" class="datetimepicker-input" id="datetimepicker-doad" placeholder="Select Date.." value="<?php echo date('Y-m-d', strtotime($dataappoint[0]->nextdate)) ?>">
                        </div>
                      </div>
                      <div class="form_row">
                        <label>Note:</label>
                        <textarea name="note" class="form_textarea" rows="" cols="" disabled><?php echo $dataappoint[0]->description ?></textarea>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- <?php // include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div> -->