        <div class="pages">
          <div data-page="dashboard-vet-mypets-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>assets/images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent" ><br /><br />
<?php if($ada == '0'){ ?>
	                   <!-- <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				            <div class="container">
			            		<div class="row">
					            	<div class="col-md-12 align-self-center p-static order-2 text-center">
						            	<h1 class="custom-primary-font text-11 font-weight-light">PET INFO</h1>
					            	</div>
				            	</div>
			            	</div>
		            	</section>-->
						<div class="container">
						    <div width="100%">
						        <center><br /><br />
						            <img class="mb-3 img-fluid" src="/assets/images/empty.png" /><br />
						            <h3 class="mb-3" align="center">Oops, we can't find your PET ID</h3>
						            <p class="mb-0" style="line-height: 1.15em;" align="center"><a href="mailto:cs@ivetdata.com">Contact us</a> if you need help</p>
						        </center>
						    </div>
						</div>

<?php }else{ ?>                  
                <a href="<?php echo base_url() ?>" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                <div class="custom-image"><img src="<?php echo $petphoto ?>" alt="" title="" /></div>
                <h2 class="page_title"><?php echo $petname ?></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabinfo-pet" class="tab active">
                        <ul class="iconed-list">
                          <li><i class="fas fa-paw mr-1"></i><b class="mr-2">Microchip ID:</b><?php
                              if($rfid == '0'){ ?>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                              <?php }else{ 
                                 echo '<span>'.$rfid.'</span>' ; 
                              }?>
                          </li>
                          <li><i class="fas fa-cat mr-1"></i><b class="mr-2">Clinic:</b><span></strong> <?php echo $clinic ?></span></li>
                            <?php if($public_info[0]==1){ ?><li><i class="fas fa-user mr-1"></i><b class="mr-2"><?php if($isclinic){ ?>Vet's Name<?php }else{ ?>Owner's Name:<?php } ?></b><span><?php echo $namevet; ?></span></li><? } ?>
                            <?php if($public_info[1]==1){ ?><li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><?php echo $phonevet; ?></span></li><?php } ?>
                            <?php if($public_info[2]==1){ ?> <li><i class="fas fa-calendar-alt mr-1"></i><b class="mr-2">Email:</b><span><?php echo $email; ?></span></li><?php } ?>
                          <li><i class="fas fa-map-marked-alt mr-1"></i><b class="mr-2">Address:</b><span><?php echo $address; ?></span></li>
                          
                        </ul>
                      </div>

                    </div>
                  </div>
                  <?php } ?>
                  </div>
                </div>
              </div>
              <div class="footer-bar">
