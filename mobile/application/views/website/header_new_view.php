<?php 
    //var_dump($this->session->userdata());
    if (isset($this->session->userdata['logged_in'])) {
        if($this->session->userdata['logged_in']['role']>0)header("Location: ".base_url()."welcome/logoutowner"); 
    }
?>
<!DOCTYPE html>
<html>
	<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>Welcome to Ivetdata.com</title>	

	<meta name="keywords" content="Pets, Pet Owners, Vets" />
	<meta name="description" content="One-Stop Solution for your Companion Animals!">
	<meta name="author" content="ivetdata.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/icon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/images/ivet.png">
	
	<link rel="stylesheet" href="<?php echo base_url() ?>css/framework7.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/fontawesome/css/all.min.css"> <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/bootstrap/bootstrap4classes.css"> <!-- Bootstrap 4 Classes -->
    <link rel="stylesheet" href="<?php echo base_url() ?>style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>custom.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>css/swipebox.css" />
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900' rel='stylesheet' type='text/css'>
  </head>
<body id="mobile_wrap">
    <div class="statusbar-overlay"></div>
    <div class="panel-overlay"></div>
    <div class="views">
      <div class="view view-main">
        <div class="navbarpages nobg">
          <ul id="navs" class="navtopleft" data-open="−" data-close="+">
            <li><a href="#" onclick="window.location='<?php echo base_url() ?>';"><i class="fas fa-home"></i></a></li>
            <li><a href="#" onclick="window.location='<?php echo base_url() ?>about';"><i class="fas fa-info"></i></a></li>
            <li><a href="#" onclick="window.open('https://tawk.to/chat/5c792599a726ff2eea5a1b14/default');" target="_blank"><i class="fas fa-phone"></i></a></li>
            <li><a href="#" onclick="window.location='<?php echo base_url() ?>news';"><i class="far fa-newspaper"></i></a></li>
          </ul>
          <div class="navbar_logo_right">
            <div class="logo_image"><a href="#" onclick="window.location='<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://".$_SERVER['HTTP_HOST'];?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
          </div>

        </div>

