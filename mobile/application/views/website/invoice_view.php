<section class="intro_section page_mainslider ls ms">  
    <div class="container">
        <div class="row">  
                    <div class="col-md-12">
                      <?php if (isset($error)){ ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                      <?php } ?>
                      <?php if (isset($success)){ ?>
                        <div class="alert alert-success"><?php echo $success ?></div>
                      <?php } ?>
                        <ul class="nav nav-tabs login-tabs mt-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link border-white text-white active show" data-toggle="tab" href="#signin" role="tab" aria-selected="true">Invoice Payment</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link border-white text-white" href="<?php echo base_url() ?>register/petowner" aria-selected="false">Pet Owner</a>
                            </li> -->
                        </ul>
                        <!-- tabs content start here -->
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <form action="<?php echo base_url() ?>Register/service" method="post">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Invoice Payment</h4>
                                <div class="col">                            
                                <div id="accordion2">                                    
                                    
                                    <div class="card mb-1 border-0 rounded-0">
                                        <div class="card-header bg-primary rounded-0 py-2" id="headingOne>">
                                            <a href="" class=" text-white" data-toggle="collapse" data-target="#service1" aria-expanded="true" aria-controls="service">
                                            Detail Invoice
                                        </a>
                                        </div>

                                        <div id="service1" class="collapse show" data-parent="#accordion2">
                                            <div class="card-body">                                                
                                                <strong>Inv : #<?php echo $invoice ?></strong><br>
                                                Name : <?php echo $name ?><br>
                                                Item : <?php echo $item ?><br>
                                                Price : Rp. <?php echo number_format($price) ?><br>
                                                Unik Code : Rp. <?php echo number_format($unik) ?><br>
                                                <strong>Total Price : Rp. <?php echo number_format($totalprice) ?></strong><br><br>
                                                Please Transfer to <br>
                                                Bank : BCA - PT. Ivet Data Indonesia<br>
                                                No Acc : 123-456-789<br><br>
                                                <a href="#" class="btn btn-primary">Pay Now</a>.
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="uid" value="<?php echo $uid ?>">
                                    
                                </div> 
                                    
                                </div>
                                <div class="row mx-0 justify-content-end no-gutters">
                                    <div class="col-6">
                                        <a href="<?php echo base_url() ?>welcome" class="btn btn-block gradient border-0 z-3">Login</a>
                                    </div>
                                    <!-- <div class="col-6">                                        
                                        <input type="submit" class="btn btn-block gradient border-0 z-3" value="Register">                                        
                                    </div> -->
                                </div>
                              </form>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                </div>
    </section>