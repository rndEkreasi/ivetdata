<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active"><?php echo $text ?></li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
               
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo $text ?></h3>
                            <a href="<?php echo base_url() ?>reminder/vaccine" class="icon-tab theme_button color2"></i>Vaccine Reminder</a>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <!-- <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> --
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> --
                                            </form>
                                        </div>

                                    </div> -->
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Reminder Date</th>
                                            <th>Name Pet</th>
                                            <th>Last Vaccine</th>
                                            <th>Owner</th>                                            
                                            <th>Email</th>
                                            <!-- <th>Chip Available</th> -->
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php
                                         $ij = $from + 1 ;
                                         foreach ($remindervacc as $vacc) { 
                                            // $tipe = $clinic->tipe;
                                            // $photo = $clinic->photo;
                                            
                                            // if($photo =='' ){
                                            //     $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            // }else{
                                            //     $petphoto = $photo;
                                            // }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <?php echo $ij++ ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <?php
                                                $nextdate = $vacc->nextdate;
                                                $newdate = date('D, d M Y',strtotime($nextdate));
                                                 echo $newdate; ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <h5><?php
                                                    $idpet = $vacc->idpet; ?>
                                                    <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $idpet ?>">
                                                    <?php 
                                                    $datapet = $this->Pet_model->detailpet($idpet);
                                                    $namepet = $datapet[0]->namapet;
                                                     echo $namepet ?></a>
                                                </h5>
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <?php echo $vacc->vaccine ?>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                          <?php                                                             
                                                            echo $vacc->nameowner; ?>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->emailowner; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->phoneowner; ?>
                                            </td>
                                            <td class="media-middle">
                                                <!-- <a href="<?php echo base_url() ?>reminder/emailsendvacc/?id=<?php echo $vacc->idvacc ?>" class="theme_button color2">Sent Email</a> -->
                                                <a href="tel:<?php echo $vacc->phoneowner; ?>" class="theme_button color3">Call</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; Copyrights <?php echo date('Y'); ?> PT. iVet Data Global. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>


</body>

</html>