      <div class="pages">
          <div data-page="dashboard-owner-mypets" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>

            <!-- template sections -->
            <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" class="backto" onclick="window.location='<?php echo base_url() ?>dashboard';"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title=""></a>
                <h2 class="page_title" align="center">RESET PASSWORD</h2><br /><br />
                <form action="<?php echo base_url() ?>login/updatepass" method="post">
                <input type="hidden" name="email" value="<?php echo $email ?>">
                <input type="hidden" name="token" value="<?php echo $token ?>">
                <input type="hidden" name="clinic" value="<?php echo $role ?>">
                <input type="hidden" name="uid" value="<?php echo $uid ?>">
            <?php if (isset($error)){ ?>
                <div class="alert alert-danger text-center"><?php echo $error ?></div>
              <?php } ?>
              <?php if (isset($success)){ ?>
                <div class="alert alert-success text-center"><?php echo $success ?></div>
              <?php } ?>
                <div class="page_single layout_fullwidth_padding" align="center">
                    <div class="form_row">
                        <label>New Password</label><br /><br />
                        <div class="input-custom"><input type="password" name="newpass" value="" class="form_input required" id="shownewpasswordinput" />
                    </div><br />
                     <div class="form_row">
                        <label>Confirm New Password</label><br /><br />
                         <div class="input-custom"><input type="password" class="form_input required" name="newpass2" id="login-password" ></div>
                </div><br />
                         <button type="submit" class="btn btn-primary nav-link border-white text-white">Update Password</button><br /><br />
                   </form>
                            <!-- .with_border -->
            </div>
            <!-- .col-* -->
        </div>
                    <!-- .row -->
    </div>
<div class="footer-bar">

    
