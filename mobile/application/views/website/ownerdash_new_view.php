       <div class="pages">
          <div data-page="dashboard-owner-mypets" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
            <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="#" class="backto" onclick="window.location='<?php echo base_url() ?>dashboard';"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title=""></a>
                    
                <h2 class="page_title"><?php echo $namaowner ?>'s PET LIST <a id="add" href="#" onclick="window.location='<?php echo base_url() ?>pet/addbyowner';"><i class="fas fa-plus" style="font-size:25px;color:#82b808;"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <?php if (isset($error)){ ?>
                            <div class="error"><?php echo $error;unset($_SESSION['error']); ?></div>
                    <?php } ?>
                    <?php if (isset($success)){ ?>
                            <div class="alert success"><?php echo $success;unset($_SESSION['success']); ?></div>
                    <?php } ?>
                  <div class="buttons-row">
                      <a href="#tabmypets-pets" class="tab-link active button" onclick=document.getElementById("add").setAttribute("onclick","'window.location=<?php echo base_url() ?>pet/addbyowner'") />My Pets</a>
                      <a href="#tabmypets-clinics" class="tab-link button" onclick=document.getElementById("add").setAttribute("onclick","window.location='<?php echo base_url() ?>pet/addvetbyowner'") />My Clinics</a>
                  </div>
                  <div class="tabs-simple">
                    <div class="tabs">
                      <div id="tabmypets-pets" class="tab active">
                        <div class="list-block">
                          <!-- <div class="searchbox mb-3">
                            <form method="get">
                               <input type="text" name="rfid" placeholder="Search">
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div> -->
                          <ul class="posts dompitems">
                                        <?php 
                                        $ij = 1;
                                        foreach ($datapet as $pet) { 
                                            $tipe = $pet->tipe;
                                            $photo = $pet->photo;
                                            
                                            if($photo =='' ){
                                                $petphoto = base_url().'images/'.$tipe.'_default.png';
                                            }else{
                                                $petphoto = $photo;
                                            }
                                        ?>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><a href="#" onclick="window.location='<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>';"><img src="<?php echo $petphoto ?>" alt="" title="" width="500" /></a></div>
                                  <div class="post_details">
                                    <h4><a href="#" onclick="window.location='<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>';"><?php echo $pet->namapet ?></a></h4>
                                    <span><?php if($pet->rfid==0){ ?><button class="btn btn-modern btn-primary" onclick="window.open('https://tawk.to/chat/5c792599a726ff2eea5a1b14/default');" target="_blank">Get Microchip</button><?php }else{ ?>
                                    <a href="#" onclick="window.location='<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>';"><?php echo $pet->rfid;} ?></a></span>
                                  </div>
                                  <div class="post_swipe"><img src="<?php echo base_url() ?>images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="#" onclick="window.location='<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>';" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="#" onclick="window.location='<?php echo base_url() ?>pet/edit/?id=<?php echo $pet->idpet ?>';" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="#" onclick="window.location='<?php echo base_url() ?>pet/delete/?idpet=<?php echo $pet->idpet ?>';" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <?php }  ?>
                          </ul><br /><br />
                          <!-- <div class="loadmore loadmore-dompitems">LOAD MORE</div>
                          <div class="showless showless-dompitems">END</div> -->
                        </div>
                      </div>
                      <div id="tabmypets-clinics" class="tab">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <!-- <form method="get">
                              <input type="text" name="rfid" placeholder="Search MICROCHIP ID">
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form> -->
                          </div>
                          <ul class="posts domcitems">
                            <?php
                                 foreach ($dataclinic as $clinic) { 
                                            $idclinic = $clinic->idclinic;
                                            $detailclinic = $this->Clinic_model->detailclinic($idclinic);
                                            if(count($detailclinic)==0) break;
                                            $nameclinic = $detailclinic[0]->nameclinic;
                                            $address = $detailclinic[0]->address;
                                            $city = $detailclinic[0]->city;
                                            $phoneclinic = $detailclinic[0]->phone;
                                            $emailclinic = $detailclinic[0]->email;
                                            $picture = $detailclinic[0]->picture;
                                            //var_dump($picture);
                            ?>
                            <!-- <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_full">
                                    <h3><?php echo $nameclinic ?></h3>
                                    <p><?php echo $address ?></p>
                                    <?php if($phoneclinic<>''){ ?>
                                    <p><a href="#" onclick="window.location='tel:<?php echo $phoneclinic ?>';" class="simple-link"><?php echo $phoneclinic ?></a></p>
                                    <?php } ?>
                                    <?php if($emailclinic<>''){ ?>
                                    <p><a href="#" onclick="window.location='mailto:<?php echo $emailclinic ?>';" class="simple-link"><?php echo $emailclinic ?></a></p>
                                    <?php } ?>
                                    <p><a href="/vets/delete/?idclinic=<?php echo $idclinic ?>" class="btn btn-modern btn-primary" onclick="confirm('Are you sure you want to delete <?php echo $nameclinic ?>');">Delete</a>
                                    &nbsp;</p>
                                  </div>
                                </div>
                              </div>
                            </li> -->
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <?php if($picture == ''){ ?>
                                    <div class="post_thumb"><img src="<?php echo base_url() ?>assets/images/petshop.png" alt="" title="" /></div>

                                  <?php }else{ ?>
                                    <div class="post_thumb"><img src="<?php echo $picture ?>" alt="" title="" /></div>
                                  <?php } ?>
                                  
                                  <div class="post_details">
                                    <h4><?php echo $nameclinic ?></h4>
                                    <p><?php echo $address ?></p>
                                    <?php if($phoneclinic<>''){ ?>
                                    <p><a href="#" onclick="window.location='tel:<?php echo $phoneclinic ?>';" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>&nbsp;
                                    <?php } ?>
                                    <?php if($emailclinic<>''){ ?>
                                    <a href="#" onclick="window.location='mailto:<?php echo $emailclinic ?>';" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a></p>
                                    <?php } ?>
                                    <p><a href="#" onclick="window.location='<?php echo base_url() ?>appointment/add/?idclinic=<?php echo $idclinic ?>';" class="btn btn-modern btn-primary">Make Appointment</a></p>
                                  </div>
                                </div>
                                <script>
                                    function chk(nm,id){
                                        if(confirm('Delete ' + nm + ' from your clinic list ?')){
                                            window.location='<?php echo base_url() ?>vets/delete/?idclinic=' + id;
                                        }else{
                                            return;
                                        }
                                    }
                                </script>
                            <a class="btn btn-sm btn-round btn-light" href="#" onclick="chk('<?php echo addslashes($nameclinic) ?>',<?php echo $idclinic ?>);" >
                            <i class="fas fa-trash"></i></a>
                             </div>
                            </li>
                            <?php 
                                 }
                            ?>
                          </ul><br /><br />
                          <!--<div class="loadmore loadmore-domcitems">LOAD MORE</div>
                          <div class="showless showless-domcitems">END</div>-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>