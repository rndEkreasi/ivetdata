<section class="intro_section page_mainslider ls ms pricing py-5">  
    <div class="container">
        <div class="row">                               
                    <div class="col-md-12">
                      
                        <!-- tabs content start here -->
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <form action="<?php echo base_url() ?>Register/service" method="post">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Choose your subscription</h4>
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                                <div class="col">                            
                                <div id="accordion2">
                                    <?php foreach ($dataservice as $service){
                                        $idservice = $service->idservice;
                                        if($idservice=='1'){
                                            $pilih = 'checked="checked"';
                                            $active = 'show';
                                        }else{
                                            $pilih = '';
                                            $active='';
                                        }
                                     ?>

                                     <div class="col-lg-4">
                                        <div class="card mb-5 mb-lg-0">
                                          <div class="card-body">
                                            <h5 class="card-title text-muted text-uppercase text-center"><?php echo $service->name ?></h5>
                                            <h6 class="card-price text-center">Rp. <?php echo number_format($service->price) ?><span class="period">/ <?php echo $service->month ?> month</span></h6>
                                            <hr>
                                            <ul class="fa-ul">
                                              <?php echo $service->description ?>
                                            </ul>
                                            <!-- <a href="<?php echo base_url() ?>register/vets/?service=<?php echo $service->idservice ?>" class="btn btn-block btn-primary text-uppercase">Register</a> -->
                                            <div class="" style="text-align: center;padding: 20px 0px;">
                                                <input type="radio" class="" name="service" <?php echo $pilih ?> value="<?php echo $service->idservice;?>"><span>Select this subscription</span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <?php } ?>
                                    
                                    <!-- <div class="card mb-1 border-0 rounded-0">
                                        <div class="card-header bg-primary rounded-0 py-2" id="headingOne<?php echo $service->idservice;?>">
                                            <a href="#" class="text-white nav-link border-white text-white active show" data-toggle="collapse" data-target="#service<?php echo $service->idservice;?>" aria-expanded="true" aria-controls="service<?php echo $service->idservice;?>">
                                            <?php echo $service->name;?> - Rp.<?php echo number_format($service->price); ?>
                                        </a>
                                        </div>

                                        <div id="service<?php echo $service->idservice;?>" class="collapse <?php echo $active ?>" data-parent="#accordion2">
                                            <div class="card-body">                                                
                                                <?php echo $service->description;?><br>
                                                <input type="radio" name="service" <?php echo $pilih ?> value="<?php echo $service->idservice;?>"><span>Select this subscription</span>
                                            </div>
                                        </div>
                                    </div> -->
                                    <?php // } ?>
                                    <input type="hidden" name="uid" value="<?php echo $uid ?>">
                                    
                                </div> 
                                    
                                </div>
                                <div class="row mx-0 justify-content-end no-gutters">
                                    <div class="col-6">                                        
                                        <input type="submit" style="height: 60px;margin: 40px 0px;font-size: 25px;" class="btn btn-block gradient border-0 z-3" value="Register">                                        
                                    </div>
                                </div>
                              </form>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                </div>
              

            </div>
</section>