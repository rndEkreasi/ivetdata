        <div class="pages">
          <div data-page="about" class="page no-toolbar no-navbar">
            <div class="page-content page-smallfooter">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <div id="pages_maincontent">
                <h2 class="page_title">About Us</h2>
                <!-- Slider -->
                <div class="swiper-container-pages swiper-init" data-effect="slide" data-pagination=".swiper-pagination">
                  <div class="swiper-wrapper">

                    <div class="swiper-slide">
                      <img src="<?php echo base_url() ?>images/about/about-1.jpg" alt="" title="" />
                    </div>
                    <div class="swiper-slide">
                      <img src="<?php echo base_url() ?>images/about/about-1.jpg" alt="" title="" />
                    </div>
                    <div class="swiper-slide">
                      <img src="<?php echo base_url() ?>images/about/about-1.jpg" alt="" title="" />
                    </div>
                  </div>
                  <div class="swiper-pagination"></div>
                </div>
                <div class="page_single layout_fullwidth_padding">
                  <div class="post_single">
                    <p>
                      PT. IVET DATA GLOBAL (est. 2018) is headquartered in Jakarta, Indonesia as a pioneer in Pet Database and Clinic CRM in the region, endorsed by Indonesian Veterinary Medical Association (PDHI) in Indonesia. We are also the first company to partner with Datamars SA to introduce country-coded microchips which follow ICAR (the International Committee for Animal Recording) ISO11784/11785 standards. Formed to accommodate the growing demand for digital solutions in veterinary practice and to support the new Government regulation on pet’s "National ID” identification, we believe in providing new technology for Pet Owners and Vets that are comprehensive, affordable, and secure.
                    </p>
                    <div class="custom-features">
                      <ul class="cf-group cf-group-green">
                        <li class="cf-item cf-title"><img src="<?php echo base_url() ?>images/icons/features/connectivity.png" class="cf-icon" /><span>Fast Vet Connectivity</span></li>
                        <li class="cf-item"><span>Real Time Medical Records with Analytics</span></li>
                        <li class="cf-item"><span>Comprehensive Vet Network</span></li>
                        <li class="cf-item"><span>24-Hour Customer Service</span></li>
                      </ul>
                      <ul class="cf-group cf-group-red">
                        <li class="cf-item cf-title"><img src="<?php echo base_url() ?>images/icons/features/accessibility.png" class="cf-icon" /><span>Accessible Anytime, Anywhere</span></li>
                        <li class="cf-item"><span>All Pet Records in One App</span></li>
                        <li class="cf-item"><span>Convenient Vet / Pet Services Online</span></li>
                        <li class="cf-item"><span>Easy Profile Update</span></li>
                      </ul>
                      <ul class="cf-group cf-group-orange">
                        <li class="cf-item cf-title"><img src="<?php echo base_url() ?>images/icons/features/security.png" class="cf-icon" /><span>Easy &amp; Secure Technology</span></li>
                        <li class="cf-item"><span>Cloud-based Technology</span></li>
                        <li class="cf-item"><span>Secured Pet Owner's Data Privacy</span></li>
                        <li class="cf-item"><span>SSL Encryption</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="footer-bar">
