<html>
<head>
<title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">

		<!-- vendor css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-elements.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-blog.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-shop.css">

		<!-- Current Page new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/settings.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/layers.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/navigation.css">
		
		<!-- Demo new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/demos/demo-insurance.css">

		<!-- Skin new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/skins/skin-insurance.css"> 

		<!-- Theme Custom new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/custom.css">

		<!-- Head Libs -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/modernizr/modernizr.min.js"></script>    
</head>    
<body>
   <!-- <div role="main" class="main">-->
   <div>
          <!--  <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section> -->
			<section id="find" class="parallax section section-parallax custom-padding-3 my-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" data-image-src="new/images/map.png">
					<div class="container">
						<div class="row justify-content-end">
							<div class="col-md-12 col-lg-7 col-xl-7">
								<div class="card bg-color-light border-0 appear-animation" data-appear-animation="fadeInUpShorter">
									<div class="card-body p-5">
										<h2 class="custom-primary-font font-weight-normal text-6 mb-3">FIND YOUR PET'S VET</h2>
										<p class="pb-2 mb-4">Click Search to find vets in our network</p>
										<form action="/vets/searchnew" method="post" >
											<div class="form-row">
												<div class="form-group col-md-4 pr-md-0">
													<input type="text" class="form-control custom-left-rounded-form-control" name="name" value="" placeholder="Find Pet ID" >
												</div>
												<div class="form-group col-md-4 pl-md-0 pr-md-0">
													<input type="text" class="form-control custom-middle-rounded-form-control" name="vet" value="" placeholder="Find Vet / Clinic" >
												</div>
												<div class="form-group col-md-4 pl-md-0">
													<input type="text" class="form-control custom-right-rounded-form-control" name="city" value="" placeholder="Find by City" >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col mb-0">
										            <p>If you found lost pet please enter their pet ID here to contact it's vet</p>
													<input type="submit" value="SEARCH" class="btn btn-secondary custom-btn-style-1 py-3 text-4 w-100">
												</div>
											</div>
										</form>
									</div>
									<div class="card-footer bg-color-primary border-0 custom-padding-2">
										<div class="row">
											<div class="col-md-6 mb-5 mb-md-0">
												<div class="feature-box align-items-center">
													<div class="feature-box-icon bg-color-tertiary">
														<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
													</div>
													<div class="feature-box-info">
														<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
														<a href="mailto:cs@ivetdata.com" class="text-color-light custom-fontsize-2">cs@ivetdata.com</a>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="feature-box align-items-center">
													<div class="feature-box-icon bg-color-tertiary">
														<i class="fas fa-phone text-3"></i>
													</div>
													<div class="feature-box-info">
														<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
														<a href="tel:+62-21-2977-951" class="text-color-light custom-fontsize-2">+62-21-2977-951</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> <br />
				<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid" style="background-color:#2f363c;">
                    <div class="row">
                        <div class="col-sm-12"><br />
                            <p class="grey" style="text-align: center;color:white;font-size:10pt;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>
        <!-- new/vendor -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery/jquery.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/popper/umd/popper.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/common/common.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vide/jquery.vide.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vivus/vivus.min.js"></script>
        
        <!-- Theme Base, Components and Settings -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.js"></script>
		
		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.init.js"></script>
</body>
</html>
