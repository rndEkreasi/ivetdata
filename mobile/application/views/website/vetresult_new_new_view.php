<html>
<head>
<title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">

		<!-- vendor css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-elements.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-blog.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-shop.css">

		<!-- Current Page new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/settings.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/layers.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/navigation.css">
		
		<!-- Demo new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/demos/demo-insurance.css">

		<!-- Skin new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/skins/skin-insurance.css"> 

		<!-- Theme Custom new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/custom.css">

		<!-- Head Libs -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/modernizr/modernizr.min.js"></script>    
</head>    
<body>
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,%0A+Mountain+View,+CA&key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 9c419fc6-508e-42c8-b1d8-c24ab2b7349f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
?>
<!--<body>
        <div role="main" class="main">
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section>-->
    <div id="map" style="height: 10%;min-height:250px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
      //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;
        
        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              //var url = markerElem.getAttribute('url');
              var url = '?vets='+id;
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>
                <?php

                if(isset($_GET['vets'])){
                        $detail = $this->Clinic_model->detailclinic($_GET['vets']);
                        if(count($detail)>0){
                            $searchvet = array();
                            $searchvet[0]['idclinic']=$detail[0]->idclinic;
                            $searchvet[0]['nameclinic']=$detail[0]->nameclinic;
                            $searchvet[0]['picture']=$detail[0]->picture;
                            $searchvet[0]['address']=$detail[0]->address;
                            $searchvet[0]['city']=$detail[0]->city;
                            $searchvet[0]['phone']=$detail[0]->phone;
                            $searchvet[0]['clinic']=1;
                        }else{
                            $searchvet= array();
                        }
                }
                
                ?>
    <?php if(!$searchvet){ ?><br /><h4 align="center"><?php echo $message; ?></h4><br /><?php exit; }  ?>

    <?php if($searchvet){ ?>
        <section class="ls page_portfolio pt-53 pb-7">
				<div class="container">
					<div class="row">
                    <div class="col-sm-12"><br />
							<?php if(isset($_POST['vet'])){?><h4 class="text-center">Vets and Clinic List - <?php echo $_POST['vet']." ".$_POST['city']; ?></h4><br /><?php } ?>

							<div class="isotope_container isotope row masonry-layout mb-3" style="position: relative;">
    <?php
                for ($i=0;$i<count($searchvet);$i++) { ?>

    								<div class="isotope-item clinic col-lg-4 col-md-6 col-sm-12">

    									<!-- <article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden" style="height:255px;">
    										<div class="item-media">
                                                <a href="<?php echo base_url() ?>vets/detail/?vets=<?php echo $searchvet[$i]['idclinic'] ?>">
    											<img src="<?php echo base_url() ?>assets/images/petshop.png" alt=""> 
                                                </a>
    										</div>-->
    										<div class="item-content">
    											
    											<h4 class="entry-title" style="color:#e54d5f;">
    												<?php echo $searchvet[$i]['nameclinic']; ?>
    											</h4>
    											<div>
    											    <?php if($searchvet[$i]['picture']=="") {echo '<img class="mb-1" style="width:60px;" src="/assets/images/petshop.png" alt="">';}
    											    else {echo '<img style="width:140px;" src='.$searchvet[$i]['picture'].' alt="">';
    											    }
    											    ?>
    											</div>
    											<p class="margin_0">
    												<!-- Clients can simply schedule their hard drive destruction online and through our website. -->
    												<?php 
    												//$descibe = strip_tags($detail->description);
    												echo $searchvet[$i]['address'];
    												//echo substr($descibe, 0, 100); ?>
    												<?php echo $searchvet[$i]['city']; ?><br>
    												<?php echo $searchvet[$i]['phone']; ?><br>
    												<?php if(isset($this->session->userdata['logged_in'])){ ?>
    												
    												    
                                            <button class="btn btn-modern btn-primary" data-toggle="modal" data-target="#clinic<?php echo $searchvet[$i]['idclinic'] ?>"> Add as My Clinic </button>
                                                <div class="modal fade" id="clinic<?php echo $searchvet[$i]['idclinic'] ?>" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" style="display: none;" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content" style="margin-top:40%;">
												<div class="modal-header">
													<h4 class="modal-title" id="defaultModalLabel">Are you sure you want to add <?php echo $searchvet[$i]['nameclinic'] ?> into your clinic list ?</h4>
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												</div>
												<div class="modal-body"><form action="/pet/addvetprocessbyowner" method="post"><input type="hidden" name="clinic" value="<?php echo $searchvet[$i]['idclinic'] ?>" />
													<?php 
													$profileid  = ($this->session->userdata['logged_in']['id']);
                                    				$dataprofile = $this->Home_model->detailprofile($profileid);
                                    				$role = $dataprofile[0]->role;
                                    				$nama = $dataprofile[0]->name;
                                    				$email = $dataprofile[0]->email;
                                    				$phone = $dataprofile[0]->phone;		
                                    				$country = $dataprofile[0]->country;
                                    				$city = $dataprofile[0]->city;
                                    				$photo = $dataprofile[0]->photo;
                                    				$expdate = $dataprofile[0]->expdate;
                                    				if($photo == '' ){
                                    				    $profilephoto = base_url().'assets/images/team/05.jpg';
                                    				}else{
                                    				    $profilephoto = $photo;
                                    				};
													$datapet = $this->Pet_model->byowneremail($email,0,$role);   
													if(count($datapet)>0){
													?>
													<p>You can choose one or more of your pets below to connect to "<?php echo $searchvet[$i]['nameclinic'] ?>" :</p>
													<?php foreach($datapet as $pet){ ?>
													<input type="checkbox" name="idpet[]" value="<?php echo $pet->idpet ?>">&nbsp;&nbsp;<label><?php echo $pet->namapet ?></label><br /> 
													<?php     } ?>
													<?php  }  ?>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-light" >OK</button>
													<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
													
												</div></form>
											</div>
										</div>
									</div></form>
    												<?php } ?>
    											</p>
    											<!-- <a href="<?php echo base_url() ?>blog/detail/<?php /// echo $detail->slug; ?>" class="read-more"></a> -->
    										</div>
    									</article>

    								</div>

								<?php } ?>

							</div>
							<!-- eof .isotope_container.row -->

							<!-- <div class="row">
								<div class="col-sm-12 text-center">
									<?php // echo $this->pagination->create_links(); ?>
									<!-- <img src="<?php echo base_url() ?>assets/img/loading.png" alt="" class="fa-spin"> --
								</div>
							</div> -->

						</div>
					</div>
				</div>
			</section>
			<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid" style="background-color:#2f363c;">
                    <div class="row">
                        <div class="col-sm-12"><br />
                            <p class="grey" style="text-align: center;color:white;font-size:10pt;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
         <?php   } ?>
        <!-- new/vendor -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery/jquery.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/popper/umd/popper.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/common/common.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vide/jquery.vide.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/vivus/vivus.min.js"></script>
        
        <!-- Theme Base, Components and Settings -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.js"></script>
		
		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page new/vendor and Views -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/js/theme.init.js"></script>
</body>
</html>
