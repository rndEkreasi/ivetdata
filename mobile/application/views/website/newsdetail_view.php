<body>
            <div role="main" class="main">
				
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="detailarticle">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">News</h1>
						</div>
					</div>
				</div>
			</section>
			<section class="section custom-padding-3 border-0 my-0">
				<div class="container">
					<div class="detailarticle">
						<div class="col">
									    <span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo date("d M Y",strtotime($detailarticle[0]->publishdate));   ?></span>
									    <div class="post-event-content">
										    <h3 class="custom-primary-font text-transform-none text-5 mb-3"><?php echo $detailarticle[0]->title;  ?></h3>
										    <center><img src="<?php echo $detailarticle[0]->featuredimg;  ?>" class="img-fluid"></center><br /><br />
    									    <?php  echo $detailarticle[0]->description; ?>
									    </div>
						    </div>
						</div>
					</div>
				</section>
