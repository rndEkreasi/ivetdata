<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Customers</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                           <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <h3>Total Pet owner : <?php if(!isset($error)>0){echo number_format($all_customer);}else{echo "0";} ?></h3>
                            <a href="<?php echo base_url() ?>customer/add" class="icon-tab theme_button color1">+ Add Pet Owner</a>
                            <a href="/customer/exportcustomerlist<?php if(isset($_GET['customername'])) echo "?customername=".$_GET['customername']."&phone=".$_GET['phone']; ?>" class="icon-tab theme_button color3" target="_blank">Export List</a>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                           <form method="get" class="" action="<?php echo base_url() ?>customer/result/">
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="" type="text" value="" name="customername" class="form-control customername" placeholder="Insert customer name" style="float: left;width: 50%">
                                                    <input id="widget-search" type="text" value="" name="phone" class="form-control choosecustomer ui-autocomplete-input" placeholder="search phone number" style="float: left;width: 50%" autocomplete="off">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No </th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>phone</th>
                                            <th>Pet</th>
                                            <th>Address</th>
                                            <th>Action</th>                                       
                                        </tr>
                                        <?php 
                                        $ij = $all_customer-$from;
                                        if(isset($error))$datacustomer=array();
                                        foreach ($datacustomer as $customer) {
                                            $idcustomer = $customer->idcustomer;
                                            $phone = $customer->nohp;
                                            $nama = $customer->nama;
                                            $email = $customer->email;
                                            if($email==""){
                                                $countpet = $this->Customer_model->petcustomeridowner($idcustomer,$idclinic);
                                            }else{
                                                $countpet = $this->Customer_model->petcustomeremail($email,$idclinic);
                                            }
                                            //$countpet = $this->Customer_model->petcustomerphone($phone,$idclinic);


                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <?php echo $ij-- ?>                                               
                                            </td>
                                            <td class="media-middle">
                                                <a href="<?php echo base_url() ?>customer/edit/?idcustomer=<?php echo $idcustomer ?>"><?php echo $customer->nama ?></a>                                              
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="mailto:<?php echo $customer->email ?>"><?php echo $customer->email ?></a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5><?php echo $customer->nohp ?></h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle" align="center">
                                                <?php if(count($countpet) == '0'){ ?>
                                                    -
                                                <?php }else{ ?>
                                                    <a href="<?php echo base_url() ?>pet/owner/?id=<?php echo $idcustomer ?>"><?php echo count($countpet); ?></a>
                                                <?php } ?>
                                                
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $customer->address.', '.$customer->city?>
                                            </td>
                                           
                                            <td class="media-middle" style="white-space: nowrap;display: inline-block;">
                                                <a href="<?php echo base_url() ?>customer/edit/?idcustomer=<?php echo $idcustomer ?>" class="icon-tab theme_button color2"> Edit </a>
                                                <?php if($manageto == '1'){ ?>
                                                <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deletecustomer<?php echo $customer->idcustomer ?>"> Delete </button>
                                                <div class="modal fade" id="deletecustomer<?php echo $customer->idcustomer ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Are you sure ? All data for <?php echo $customer->nama ?> will be deleted.</h5>

                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                        <a href="<?php echo base_url() ?>customer/delete/?idcustomer=<?php echo $customer->idcustomer ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <?php } ?>
                                            </td>
                                           
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php if(!isset($error))echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

        <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    //price.val(ui.item.label);                     
                }                
            });
 
        });
    </script>


</body>

</html>