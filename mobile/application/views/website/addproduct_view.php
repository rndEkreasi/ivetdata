 <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ad8cbhz7hy8flomantnx8ehdj6a4lpjuuqs226xxs9yytcjy"></script>
<script>tinymce.init({ selector:'textarea.tiny' });</script>
            <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>">Dashboard</a>
                                </li>
                                <li class="active">Add New Product</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Add New Product</h3>
                        </div>
                    </div>
                    <!-- .row -->
                    <?php if (isset($success)){ ?>
                        <div class="alert alert-success"><?php echo $success ?></div>
                    <?php } ?>
                    <?php if (isset($error)){ ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">

                            
                            <div class="with_border with_padding">

                                <form action="<?php echo base_url() ?>product/addpost" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Name :</label>
                                        <input type="text" class="form-control" id="judul" name="productname" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Price (Rp.) :</label>
                                        <input type="text" class="form-control" id="judul" name="price" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Stock :</label>
                                        <input type="text" class="form-control" id="stock" name="stocks" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">SKU :</label>
                                        <input type="text" class="form-control" id="stock" name="sku" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Photo :</label>
                                        <input type="file" class="form-control" id="photo" name="photo" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Description :</label>
                                        <textarea name="description" class="tiny" rows="15" form-control"></textarea>
                                    </div>                                                        
                                    <button type="submit" class="theme_button">Post</button>
                                </form>
                            </div>
                            <!-- .with_border -->                           
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->


                </div>
                <!-- .container -->
            </section>

