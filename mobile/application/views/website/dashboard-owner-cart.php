        <div class="pages">
          <div data-page="dashboard-owner-petshop-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="#" onclick="history.back(1);" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title=""></a> 
                <h2 class="page_title">SHOPPING CART
                  <a id="add" href="#" onclick="window.location='<?php echo base_url() ?>shop/all';" class=""><i class="fas fa-plus" style="font-size:25px;color:#82b808;"></i></a>
                </h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="cartcontainer">
                    <?php 
                    //var_dump($invoice);
                    if($invoice != '0'){
                      if($totalcart !== NULL){
                        $i = 1;
                        foreach ($datacart as $cart) { ?>
                          <div class="cart_item" id="cartitem1">
                          <div class="item_title"><span><?php echo $i++ ?>.</span> <?php echo $cart->nama_item ?></div>
                          <div class="item_price">Rp. <?php echo number_format($cart->totalprice); ?></div>
                          <div class="item_thumb"><a href="#" class="close-panel">
                            <?php
                            $imageproduct = $cart->picture;
                            if($imageproduct == ''){ ?>
                               <img src="<?php echo base_url() ?>assets/images/default-shop.png" alt="" title="" />
                            <?php }else{ ?>
                                <img src="<?php echo $imageproduct ?>" alt="" title="" />
                            <?php }?>
                            
                          </a></div>
                          <div class="item_qnty">
                            <form id="myform" method="POST" action="#">
                              <label>QUANTITY</label>
                              <!-- <input type="button" value="-" class="qntyminus" field="quantity1"> -->
                              <input type="text" disabled="" name="quantity1" value="<?php echo $cart->qty ?> x Rp.<?php echo number_format($cart->price); ?>" class="qnty" style="width: auto;margin: 0px auto;">
                              <!-- <input type="button" value="+" class="qntyplus" field="quantity1"> -->
                            </form>
                          </div>
                          <a href="<?php echo base_url() ?>shop/deleteitem/<?php echo $cart->id_cart ?>" onclick="window.location='<?php echo base_url() ?>shop/deleteitem/<?php echo $cart->id_cart ?>';" class="item_delete" id="cartitem1"><img src="<?php echo base_url() ?>images/icons/black/trash.png" alt="" title=""></a>
                        </div>
                    <?php } ?>
                    
                    <div class="carttotal">
                      <!-- <div class="carttotal_row">
                        <div class="carttotal_left">CART TOTAL</div>
                        <div class="carttotal_right">Rp. 1,050,000</div>
                      </div>
                      <div class="carttotal_row">
                        <div class="carttotal_left">TAX (15%)</div>
                        <div class="carttotal_right">Rp. 157,500&#8236;</div>
                      </div> -->
                      <div class="carttotal_row_last">
                        <div class="carttotal_left">TOTAL</div>
                        <div class="carttotal_right">Rp. <?php echo number_format($totalcart) ?>&#8236;</div>
                      </div>
                    </div>
                    <a href="#" onclick="window.open('<?php echo base_url() ?>shop/checkout');window.location('<?php echo base_url() ?>shop');"  class="button_full btyellow">PROCEED TO CHECKOUT</a>
                  <?php 
                }else{?>
                  <a href="#" onclick="window.location='<?php echo base_url() ?>shop/all';" class="button_full btyellow">Shop Now</a>
                <?php }
                }else{ ?>
                    <a href="#" onclick="window.location='<?php echo base_url() ?>shop/all';" class="button_full btyellow">Shop Now</a>
                  <?php } ?>
                  </div>
                </div>
              </div>
             <!--  <?php  include 'headerdash_view.php' ?>
            </div>
          </div>
        </div>
 -->