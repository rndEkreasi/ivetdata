<section class="intro_section page_mainslider ls ms">  
    <div class="container">
        <div class="row">  
                    <div class="col-md-12">
                      
                        <!-- tabs content start here -->
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <form action="<?php echo base_url() ?>Register/uploadid_process" method="post" enctype="multipart/form-data">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Please, upload your ID Card</h4>
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                                   <div class="form-group" style="padding:0px 20px 50px;">                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">KTP Number </span>
                                        </div>                                        
                                        <input type="text" class="form-control" placeholder="ID Number" name="idlicense" aria-label="Name" required>
                                    </div>
                                   <div class="form-group" style="padding:0px 20px 50px;">                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload KTP</span>
                                        </div>                                        
                                        <input type="file" class="form-control" placeholder="License card" name="licensepic" aria-label="Name">
                                        <input type="hidden" name="uid" value="<?php echo $uid; ?>">
                                    </div>

                                    <!-- <div style="margin:30px 20px -6px;"><label>National ID Card</label><br></div>
                                    <div class="input-group" >                                        
                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="material-icons">place</i></span>
                                        </div>
                                        <input type="file" class="form-control" placeholder="National ID card" name="name" aria-label="Name">
                                        
                                    </div> -->
                                    
                                </div>
                                <div class="row mx-0 justify-content-end no-gutters">
                                    <div class="col-6">                                        
                                        <input type="submit" class="btn btn-block gradient border-0 z-3" value="Upload License">                                        
                                    </div>
                                </div>
                              </form>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                    <div class="col-md-3">
                    </div>
                    </div>
                </div>
            </div>
        </section>