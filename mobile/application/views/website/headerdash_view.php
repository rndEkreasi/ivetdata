            <div class="footer-bar">
                <div class="swiper-container-toolbar swiper-toolbar swiper-init swiper-container-horizontal" data-effect="slide" data-slides-per-view="4" data-slides-per-group="3" data-space-between="0" data-pagination=".swiper-pagination-toolbar">
                  <div class="swiper-pagination-toolbar"><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
                  <div class="swiper-wrapper">
                    <div class="swiper-slide toolbar-icon swiper-slide-active" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>dashboard/petowner';" class=""><i class="fas fa-paw"></i>My Pets</a></div>
                  <div class="swiper-slide toolbar-icon"><a href="#" onclick="window.location='<?php echo base_url() ?>appointment/';"><i class="fas fa-briefcase-medical"></i>Appointment</a></div>
                    <div class="swiper-slide toolbar-icon swiper-slide-next" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>owner/vetSearch';"><i class="fas fa-user-friends"></i>Vet Services</a></div>
                    <div class="swiper-slide toolbar-icon" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>shop/service';"><i class="fas fa-store"></i>Pet Services</a></div>
                    <div class="swiper-slide toolbar-icon" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>shop/all';"><i class="fas fa-store"></i>Pet Shop</a></div>
                    <div class="swiper-slide toolbar-icon" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>shop/cart';"><i class="fas fa-shopping-cart"></i>Shopping Cart</a></div>
                    <div class="swiper-slide toolbar-icon" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>owner';"><i class="fas fa-receipt"></i>Invoice</a></div>
                    <div class="swiper-slide toolbar-icon" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>owner/editprofile';"><i class="fas fa-user"></i>Edit Profile</a></div>
                    <div class="swiper-slide toolbar-icon" style="width: 361px;"><a href="#" onclick="window.location='<?php echo base_url() ?>welcome/logoutowner';"><i class="fas fa-sign-out-alt"></i>Sign Out</a></div>
                  </div>
                </div>
                <script>document.getElementById("pages_maincontent").style.bottom="120px";</script>
                