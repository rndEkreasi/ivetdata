			<?php $lang = $this->input->get('lang'); ?>
			<section id="about" class="ls section_padding_top_50 section_padding_bottom_0">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<?php if ($lang == 'id'){ ?>
								<h2 class="section_header with_icon icon_color">
								Tentang iVet Data
							</h2>
							<p class="small-text">One Stop Solution for Your Companion Animals</p>
							<?php }else{ ?>
								<h2 class="section_header with_icon icon_color">
								About iVet Data
							</h2>
							<p class="small-text">One Stop Solution for Your Companion Animals</p>
							<?php } ?>
							<img src="<?php echo base_url() ?>assets/images/adorable.jpg" alt="" />
						</div>
					</div>
					<div class="row" style="margin-top: 25px;">		
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>			
						<div class="col-md-8 col-sm-12">
						<?php if ($lang == 'id'){ ?>
							<p>
								Daftarkan identifikasi hewan peliharaan Anda di pet database iVetData untuk solusi komprehensif yang dapat mengelola informasi pasien dan klien Anda yang terhubung ke Dokter Hewan berlisensi secara lokal dan global!</p>

							<p>iVetData menghubungkan pet database dan CRM Vets sendiri yang dapat diakses di mana saja dan kapan saja berbasis cloud yang aman!</p>
							
							<p>
								Akses Mudah ke rekam medis Hewan Peliharaan dengan Sistem Manajemen untuk pelanggan. Dilengkapi pendataan untuk Pemilik Hewan Peliharaan, invoice, dan Inventaris.
							</p>
							<p>
								Sekarang semua orang dapat menikmati pet database bersama yang komprehensif di seluruh negeri dan di seluruh dunia yang dapat ditelusuri oleh Pet Microchip ID dengan informasi yang tersedia yaitu Nama Pet, Nama Dokter Hewan, Alamat Klinik, dan Detail Kontak.
							</p>
							<p>
								Hanya dokter hewan berlisensi yang terdaftar melalui situs web ini yang dapat memverifikasi kepemilikan hewan peliharaan dan untuk mengakses kondisi medis hewan peliharaan untuk diagnosis yang lebih komprehensif!
							</p>

						<?php }else{?>
							<p>
							<b>PT IVET DATA GLOBAL</b> (est. 2018) headquartered in Jakarta, Indonesia as a pioneer in Pet Database and Clinic CRM in the region, endorsed by Indonesian Veterinary Medical Association (PDHI) in Indonesia.
							</p>
							<p>
								We are also the first company to partner with Datamars SA to introduce country-coded microchips which follow ICAR (the International Committee for Animal Recording) ISO11784/11785 standards. 
							</p> 
							<p>
								Formed to accommodate the growing demand for digital solutions in veterinary practice and to support the new Government regulation on pet’s "National ID” identification, we believe in providing new technology for Pet Owners and Vets that are comprehensive, affordable, and secure.
							</p>

						<?php }?>
							<!-- <a href="<?php echo base_url() ?>about_us" class="theme_button color1 wide_button">Read More</a> -->
						</div>
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>
					</div>
					</div>
				</div>
			</section>
			

			<section id="mission" class="ls section_padding_top_100 columns_margin_bottom_20">
				<div class="container">
					<div class="row">
						
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>
						<div class="col-md-8 col-sm-12">
							<?php if ($lang == 'id'){ ?>

								<h2 class="section_header with_icon icon_color4">
								Latar Belakang
							</h2>
							<p class="small-text text-center">Visi &amp; Missi</p>
							<p>
								Terdapat sekitar 20.000 dokter hewan di Indonesia dengan 11 Universitas yang menawarkan gelar Dokter Hewan namun sedikit / tanpa akses ke Pet Microchip Database &amp; Clinic CRM yang komprehensif.
							</p>
							<p>
								 Metode dokumentasi saat ini adalah melalui pengarsipan dengan kertas yang tidak efisien dan mahal.  Misi kami adalah membangun Database Hewan Peliharaan pertama di Indonesia yang dapat dibagikan antara dokter hewan berlisensi untuk meningkatkan efisiensi, transparansi, dan keakuratan saat mendokumentasikan data Hewan Peliharaan dan ID, bio, dan catatan medis mereka.
							</p>
							<p>
								PT IVET DATA GLOBAL didirikan pada tahun 2018 untuk mengakomodasi meningkatnya permintaan solusi digital untuk praktik dokter hewan serta peraturan baru Gubernur Jakarta untuk mengatur kepemilikan hewan peliharaan dengan menyediakan Database “ID Nasional” Pet untuk memantau dan melacak hewan pembawa rabies agar sesuai dengan Peraturan Standar Internasional.
							</p>

							<?php }else{ ?>

							<h2 class="section_header with_icon icon_color4 text-center">
								Our Disruptive Solution
							</h2>
							<p class="small-text text-center">
								Pet Database, Vet CRM, &amp; Pet Owner’s App							
							</p>
							<img src="<?php echo base_url() ?>assets/images/mission.png" alt="" class="top-overlap-small" style="margin: 0 0 40px 0;" />
							<p>
								Successful Integration between 3 major elements: Pet Database, Vet CRM, and Pet Owner’s App; in which supported by promotions of Pet Microchips, collaborations with various Vet Associations, and partnerships with various Pet Care Producers.
							</p>
							<div class="row">
								<div class="col-md-4 col-sm-12 text-center disrupt_col">
									<div class="circle c01"><img src="<?php echo base_url() ?>assets/images/microchip.png" /></div>
									<h5 style="margin: 20px 0 15px;">MICROCHIP DATABASE</h5>
									<p class="disrupt d001">Certified country-coded microchip accessible via public pet database</p>
									<p class="disrupt d002">Secure pet owner&#39;s data privacy by using vet agents</p>
								</div>
								<div class="col-md-4 col-sm-12 text-center disrupt_col">
									<div class="circle c02"><img src="<?php echo base_url() ?>assets/images/crm.png" /></div>
									<h5 style="margin: 20px 0 15px;">VET CRM</h5>
									<p class="disrupt d003">Safe &amp; secure medical records with analytics</p>
									<p class="disrupt d004">Less admin cost due to automation</p>
									<p class="disrupt d005">Better customer service</p>
								</div>
								<div class="col-md-4 col-sm-12 text-center disrupt_col">
									<div class="circle c03"><img src="<?php echo base_url() ?>assets/images/app.png" /></div>
									<h5 style="margin: 20px 0 15px;">PET OWNER'S APP</h5>
									<p class="disrupt d006">All pet records in one app</p>
									<p class="disrupt d007">Convenient vet/pet services online</p>
									<p class="disrupt d008">Easy profile update</p>
								</div>
							</div>
							
							<?php } ?>

							<!-- <p class="topmargin_40">
					<a href="<?php echo base_url() ?>about_us" class="theme_button color2 wide_button">
						Read More
					</a>
				</p> -->
						</div>
						
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>
					</div>
				</div>
			</section>
			<section id="nextdev" class="ls section_padding_top_100 columns_margin_bottom_20">
				<div class="container">
					<div class="row">
						
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>
						<div class="col-md-8 col-sm-12 text-center">
							<?php if ($lang == 'id'){ ?>

								<h2 class="section_header with_icon icon_color4">
								Our Strategic Partners
							</h2>
							<p class="small-text"></p>
							<p>
								
							</p>

							<?php }else{ ?>

							<h2 class="section_header with_icon icon_color2 text-center">
								Our Strategic Partners
							</h2>
						</div>
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>
							<div class="row">
								<div class="col-sm-12 col-md-4 text-center">
									<div style="height: 163px; line-height: 163px; padding: 10px;"><img src="<?php echo base_url() ?>assets/images/logo-pdhi.png" /></div>
									<p class="disrupt d000">Perhimpunan Dokter Hewan Indonesia &#40;Indonesian Veterinary Medical Association&#41; is a non-profit, professional organization representing 52 veterinary chapters in various regions in Indonesia. The association was established on 9 January 1953 with 20,000 current members. Its activities include providing Vet Membership Licenses, supporting Vet Continuing Education &amp; Accreditation, and Maintaining Professional Code of Conduct for its members.<br/><br/>The association is headquartered in Jakarta and is a member to the Federation of Asian Veterinary Association.</p>
								</div>
								<div class="col-sm-12 col-md-4 text-center">
									<div style="height: 163px; line-height: 163px; padding: 10px;"><img src="<?php echo base_url() ?>assets/images/logo-datamars_petlink.png" /></div>
									<p class="disrupt d000">Datamars SA, is a global leader in the production of unique-identification solutions specializing in radio frequency identification technology to companion animal (microchips, microchip readers, companion software, and reunification products), livestock (visual ear tags, electronic ear tags, boluses, livestock readers, and accessories), and textile identification (laundry chips, readers, antennas, and ultra-high frequency products) markets.<br/><br/>The company also provides pre and post-sales support services and animal database under PetLink.net. Datamars is headquartered in Switzerland and has 10 branches all around the world.
</p>
								</div>
								<div class="col-sm-12 col-md-4 text-center">
									<div style="height: 163px; line-height: 163px; padding: 10px;"><img src="<?php echo base_url() ?>assets/images/logo-royalcanin.png" /></div>
									<p class="disrupt d000">Royal Canin S.A.S. is a global leader in the production of pet health nutrition products and food products for dogs, kittens, puppies, and cats for pet owners, breeders and professionals, and veterinarians through veterinary hospitals and pet specialty retailers.<br/><br/>The company was founded in 1967 and is based in Aimargues, France with additional manufacturing facilities in Missouri and South Dakota. It also has plants in the United States and Canada. As of September 18, 2002, Royal Canin S.A.S. operates as a subsidiary of Mars Chocolat France SAS.
</p>
								</div>
							</div>
							
							<?php } ?>

					</div>
				</div>
			</section>
			
			<!-- <section id="blog" class="ls section_padding_100">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color3">
								From Our Blog
							</h2>
							<p class="small-text">Our latest news</p>
						</div>
					</div>
					<div class="row columns_margin_bottom_20">
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/05.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 12, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Shankle doner ribeye ham hock shank</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/06.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 13, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">T-bone capicola kevin pancetta</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/07.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 14, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Beef tri-tip brisket jerky boudin</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
					</div>
				</div>
			</section> -->
