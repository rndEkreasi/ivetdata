<div class="pages">
    <div data-page="dashboard-owner" class="page no-toolbar no-navbar">
        <div class="page-content">
            <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                    <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
            </div>
            <!-- List -->
              <div id="pages_maincontent">
                <div class="success_message">
                  <span>Page Not Found</span>
                  <img src="<?php echo base_url() ?>images/404-red.png" alt="" title="" />
                  <p>Click <a class="simple-link" href="#" onclick="window.location='<?php echo base_url() ?>';">here</a> to go back.</p>
                </div>
              </div>

<?php if (!isset($this->session->userdata['logged_in'])) { ?><div class="footer-bar"><?php } ?>
			