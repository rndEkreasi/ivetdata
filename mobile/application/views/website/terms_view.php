<div class="pages">
  <div data-page="terms" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="#" onclick="window.location='<?php echo base_url() ?>';"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="#" onclick="window.location='<?php echo base_url() ?>';" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title">TERMS &amp; CONDITIONS</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
<p>
    1. Aplikasi IVet Data adalah aplikasi perangkat lunak yang dapat diakses
    melalui Ponsel Pintar dan Komputer yang dapat digunakan pengguna untuk
    menyimpan dan mengelola data medis.
</p>
<p>
    2. PT. IVET DATA GLOBAL, “Kami”, “Kita” adalah PT. IVet Data Global, sebuah
    perseroan terbatas yang didirikan dan beroperasi secara sah berdasarkan
    hukum Republik Indonesia dan berdomisili di Jakarta Barat, Indonesia yang
    memfasilitasi pengelolaan rekam medis hewan antara pasien dan penyedia
    layanan kesehatan (seperti dokter dan klinik).
</p>
<p>
    3. Ponsel Pintar adalah telepon selular genggam dengan sistem operasi
    Android/iOs dan/atau sistem operasi lainnya yang dapat digunakan untuk
    menjalankan Aplikasi IVet Data.
</p>
<p>
    4. Rekam Medis adalah berkas yang berisi catatan dan dokumen tentang
    identitas pasien, pemeriksaan, pengobatan, tindakan dan pelayanan lain yang
    telah diberikan kepada pasien.
</p>
<p>
    5. Layanan IVet Data adalah layanan yang tersedia di IVet Data termasuk
    namun tidak terbatas pada:
</p>
<p>
    1. Penyimpanan dan pengelolaan rekam medis.
</p>
<p>
    2. Sarana komunikasi dan pembagian data antara pasien dan dokter atau pihak
    ketiga lainnya.
</p>
<p>
    3. Layanan lainnya yang dapat kami tambahkan dari waktu ke waktu
    sebagaimana disetujui oleh pihak berwenang terkait.
</p>
<p>
    4. Penyedia Layanan Kesehatan adalah orang atau institusi yang telah
    mendapat izin praktek dari otoritas yang berwenang termasuk namun tidak
    terbatas pada dokter dan klinik.
</p>
<p>
    <strong>PENDAFTARAN AKUN</strong>
</p>
<p>
    1. Sebelum anda menggunakan layanan IVet Data, Anda wajib untuk melakukan
    pendaftaran melalui Aplikasi IVet Data yang telah diunduh ke Ponsel Pintar
    atau Komputer Anda. Anda wajib untuk menyampaikan informasi yang benar,
    tepat dan terbaru dari diri Anda. Dengan melakukan pendaftaran, Anda
    menyetujui dan menerima Layanan IVet Data. Penerimaan tersebut berarti
    bahwa anda telah menerima seluruh Ketentuan dengan tanpa syarat dan tanpa
    adanya pengeculian dan oleh karenanya sepakat untuk terikat oleh Ketentuan
    termasuk setiap amandemen atau perubahan yang mungkin dibuat IVet Data atas
    Ketentuan ini dikemudian hari.
</p>
<p>
    1. Dengan mendaftarkan diri Anda ke IVet Data, Anda menyetujui bahwa
    penyedia layanan kesehatan (klinik, rumah sakit dan lain-lain) yang bekerja
    sama dengan IVet Data, dimana Anda juga pasiennya, berhak untuk menyimpan
    data rekam medis Anda di IVet Data.
</p>
<p>
    2. Anda bertanggung jawab atas akurasi dan kebaharuan informasi yang Anda
    sediakan kepada Kami. Data yang Anda sediakan akan disimpan di IVet Data
    dan hanya dapat diakses oleh Anda dan penyedia layanan kesehatan yang
    bersangkutan. Kami tidak bertanggung jawab atas setiap kerugian yang
    diderita oleh Anda yang disebabkan oleh kegagalan Anda untuk memperbaharui
    informasi Anda melalui sistem kami.
</p>
<p>
    3. Untuk pendaftaran akun melalui Aplikasi IVet Data, Anda akan memilih
    kata sandi dan detil informasi login lainnya untuk identifikasi diri Anda.
    Anda dilarang untuk mengungkapkan informasi login Anda kepada pihak ketiga
    manapun, kecuali apabila diperlukan berdasarkan hukum dan peraturan
    perundang-undangan yang berlaku (yang mana hal tersebut harus disampaikan
    kepada kami secara tertulis dengan segera). Anda setuju untuk menanggung
    setiap resiko terkait pengungkapan informasi login Anda kepada pihak ketiga
    manapun dan bertanggung jawab secara penuh atas setiap konsekuensi yang
    berkaitan dengan itu.
</p>
<p>
    4. Anda menyatakan dan menjamin bahwa Anda adalah individu yang sah secara
    hukum untuk melakukan tindakan hukum untuk masuk ke dalam perjanjian yang
    mengikat berdasarkan hukum Republik Indonesia, secara khusus Ketentuan
    Penggunaan, untuk menggunakan IVet Data. Jika Anda tidak memenuhi ketentuan
    tersebut namun tetap mengakses IVet Data, Anda menyatakan dan menjamin
    bahwa pembukaan akun Anda dan aktivitas lain dalam IVet Data telah
    disetujui oleh orang tua atau pengampu Anda. Anda mengesampingkan setiap
    hak berdasarkan hukum untuk membatalkan atau mencabut setiap dan seluruh
    persetujuan yang Anda berikan berdasarkan Ketentuan Penggunaan pada waktu
    Anda dianggap oleh hukum telah dewasa. Anda secara penuh melepaskan kami
    dan petugas kami dari kerugian atau konsekuensi yang timbul sehubungan
    dengan hal tersebut.
</p>
<p>
    <strong> </strong>
</p>
<p>
    <strong>KETENTUAN PENGGUNAAN</strong>
</p>
<p>
    1. IVet Data memberikan Layanan yang memungkinkan Anda untuk berbagi Data
    dengan dokter, praktisi kesehatan, penasihat kesehatan, atau pihak ketiga
    yang ditunjuk manapun yang Anda inginkan untuk mengetahui rekam medis atau
    kondisi medis anda. Penggunaan Data dan Layanan beserta dengan jasa dan
    fiturnya diatur oleh Ketentuan ini.
</p>
<p>
    2. Layanan tersebut dapat berubah dari waktu ke waktu sebagaimana kami
    memperbaiki, memodifikasi dan menambahkan fitur tambahan lainnya. Kami
    dapat menghentikan, menangguhkan, merubah, atau menghilangkan Layanan pada
    setiap waktu tanpa adanya pemberitahuan kepada Anda. Penggunaan Anda secara
    berkelanjutan atas IVet Data setelah modifikasi, variasi dan/atau perubahan
    atas Ketentuan Penggunaan merupakan persetujuan dan penerimaan Anda atas
    modifikasi, variasi dan/atau perubahan tersebut.
</p>
<p>
    3. Anda hanya dapat menggunakan Aplikasi ketika Anda telah mendaftar pada
    Aplikasi tersebut. Setelah Anda berhasil mendaftarkan diri, Aplikasi akan
    memberikan Anda suatu akun pribadi yang dapat diakses dengan kata sandi
    yang Anda pilih.
</p>
<p>
    4. Anda memahami dan setuju bahwa penggunaan Aplikasi oleh Anda akan tunduk
    pula pada Kebijakan Privasi kami sebagaimana dapat dirubah dari waktu ke
    waktu. Dengan menggunakan Aplikasi, Anda juga memberikan persetujuan
    sebagaimana dipersyaratkan berdasarkan Kebijakan Privasi kami.
</p>
<p>
    5. Mohon menginformasikan kepada kami jika Anda tidak lagi memiliki kontrol
    atas akun Anda, sebagai contoh akun Anda dengan cara bagaimanapun diretas
    (hack) atau telepon Anda dicuri, sehingga kami dapat membatalkan akun Anda
    dengan sebagaimana mestinya. Mohon diperhatikan bahwa Anda bertanggung
    jawab atas penggunaan akun Anda dan Anda mungkin dapat dimintakan tanggung
    jawabnya meskipun jika akun Anda tersebut disalahgunakan oleh orang lain.
</p>
<p>
    6. Ketentuan ini berlaku bagi Anda dan setiap sub-akun yang dibuka
    berdasarkan Ketentuan ini. Keputusan pihak ketiga untuk membuka sub-akun di
    bawah Ketentuan ini akan dianggap sebagai penerimaan, tanpa setiap
    pemesanan, ketentuan yang ditetapkan di bawah Ketentuan ini. Oleh karena
    itu, istilah "Anda" dalam Persyaratan ini harus merujuk kepada Anda dan
    termasuk setiap anggota sub-akun Anda.
</p>
<p>
    <strong>TANGGUNG JAWAB</strong>
</p>
<p>
    1. Anda bertanggung jawab secara pribadi atas setiap aktivitas yang
    menggunakan akun Anda, untuk menjaga Data dan pembagian Data yang Anda
    lakukan kepada suatu pihak ketiga. Anda wajib untuk dengan segera
    memberitahukan IVet Data mengenai adanya penggunaan akun Anda secara tidak
    berwenang.
</p>
<p>
    2. Anda bertanggung jawab untuk menjaga kata sandi yang Anda gunakan untuk
    mengakses Layanan dan Anda sepakat untuk tidak memberitahukan kata sandi
    Anda kepada pihak ketiga manapun.
</p>
<p>
    3. Anda bertanggung jawab secara penuh atas keaslian, akurasi, kelengkapan
    dan/atau legalitas dari setiap jenis Data yang Anda berikan kepada IVet
    Data. Anda diwajibkan untuk senantiasa memeriksa dan memberikan konfirmasi
    apakah Data yang diberikan sudah benar.
</p>
<p>
    4. Anda secara tegas mengesampingkan dan melepaskan Kami dari setiap dan
    semua kewajiban, tuntutan atau kerugian yang timbul dari atau dengan cara
    apapun sehubungan dengan Penyedia Layanan Kesehatan (dokter, praktisi
    kesehatan). Perusahaan tidak akan menjadi pihak dalam sengketa, negosiasi
    sengketa antara anda dan Penyedia Layanan Kesehatan. Anda secara tegas
    mengesampingkan dan melepaskan Kami dari setiap dan semua kewajiban,
    tuntutan, penyebab tindakan, atau kerusakan yang timbul dari penggunaan
    Layanan, perangkat lunak dan/atau Aplikasi, atau dengan cara apapun terkait
    dengan Penyedia Layanan Kesehatan yang diperkenalkan kepada Anda melalui
    Aplikasi.
</p>
<p>
    5. Anda akan membebaskan IVet Data dari segala tuntutan apapun, jika IVet
    Data tidak dapat melaksanakan perintah dari Anda baik sebagian maupun
    seluruhnya karena kejadian-kejadian atau sebab-sebab di luar kekuasaan atau
    kemampuan IVet Data, termasuk namun tidak terbatas pada segala gangguan
    virus computer, bencana alam, perang, huru-hara, keadaan peralatan, sistem
    atau transmisi yang tidak berfungsi, gangguan listrik, gangguan
    telekomunikasi dan kebijakan pemerintah.
</p>
<p>
    6. Anda bertanggung jawab atas keamanan Ponsel Pintar dan/atau Komputer
    Anda yang digunakan untuk mengakses Akun IVet Data dengan secara wajar
    menjaga dan menyediakan memori penyimpanan yang cukup untuk mencegah setiap
    kegagalan atau gangguan atas setiap proses Layanan IVet Data yang
    disebabkan oleh kegagalan fungsi Ponsel Pintar dan/atau Komputer.
</p>
<p>
    <strong>KEKAYAAN INTELEKTUAL</strong>
</p>
<p>
    1. IVet Data, termasuk nama dan logonya, kode, desain, teknologi, model
    bisnis, dilindungi oleh hak cipta, merek dan hak kekayaan intelektual
    lainnya yang tersedia berdasarkan hukum Republik Indonesia. Kami (dan pihak
    yang memperoleh lisensi dari kami, jika berlaku) memiliki seluruh hak dan
    kepentingan atas IVet Data, termasuk seluruh hak kekayaan intelektual yang
    berhubungan dengannya. Syarat dan Ketentuan ini tidak dan dengan cara
    apapun tidak akan dianggap sebagai pemberian izin kepada Anda untuk
    menggunakan setiap hak kekayaan intelektual kami sebagaimana disebutkan di
    atas.
</p>
<p>
    2. Ketentuan ini tidak memberikan Anda hak, titel, atau kepentingan pada
    Layanan, perangkat lunak (software), atau isi dari Layanan. Kami dapat
    menggunakan umpan balik (feedback), komentar, atau saran-saran yang Anda
    kirimkan kepada kami tanpa adanya kewajiban apapun terhadap Anda. Perangkat
    lunak (software) dan teknologi lainnya yang kami gunakan untuk memberikan
    Layanan dilindungi oleh hak cipta, hak merek dari hukum Republik Indonesia.
</p>
<p>
    <strong>GANTI RUGI</strong>
</p>
<p>
    1. Dengan menggunakan Aplikasi ini, Anda setuju bahwa Anda akan membela,
    memberikan ganti rugi dan membebaskan kami, pemberi lisensi, afiliasi, dan
    masing-masing dari petugas, direktur, komisaris, karyawan, pengacara dan
    agen Kami dari dan terhadap setiap dan semua klaim, biaya, kerusakan,
    kerugian, kewajiban dan biaya (termasuk biaya dan ongkos pengacara) yang
    timbul dari atau sehubungan dengan:
</p>
<p>
    1. Penggunaan Layanan dan/atau Aplikasi oleh Anda, hubungan Anda sebagai
    penyedia layanan kesehatan dan pasien Anda, penyedia pihak ketiga, mitra,
    pemasang iklan dan/atau sponsor;
</p>
<p>
    2. Pelanggaran atas atau tidak dipatuhinya salah satu Ketentuan Penggunaan
    atau peraturan perundang-undangan yang berlaku, baik yang disebutkan di
    sini atau tidak;
</p>
<p>
    3. Pelanggaran Anda terhadap hak-hak pihak ketiga, termasuk penyedia
    layanan pihak ketiga yang diatur melalui Aplikasi;
</p>
<p>
    4. Penggunaan atau penyalahgunaan Aplikasi.
</p>
<p>
    2. Kewajiban pembelaan dan pemberian ganti rugi ini akan tetap berlaku
    walaupun Ketentuan dan penggunaan Layanan oleh Anda telah berakhir.
</p>
<p>
    <strong>HUKUM YANG BERLAKU DAN JURISDIKSI</strong>
</p>
<p>
    1. Ketentuan ini akan diatur, diartikan, dan diinterpretasikan berdasarkan
    hukum Republik Indonesia. Setiap dan seluruh perselisihan yang timbul dari
    penggunaan layanan kami akan tunduk pada yurisdiksi eksklusif di Pengadilan
    Negeri Jakarta Barat.
</p>
<p>
    2. Setiap sengketa, kontroversi atau perbedaan yang dapat timbul diantara
    Para Pihak atau terkait dengan atau sehubungan dengan Ketentuan ini, atau
    mengenai konstruksi, pengakhiran, atau pelanggaran terhadap Ketentuan ini,
    akan diselesaikan secara baik-baik oleh Para Pihak. Apabila sengketa,
    kontroversi atau perbedaan tersebut tidak dapat diselesaikan dalam waktu
    tiga puluh (30) hari sejak pemberitahuan tertulis diberitahukan kepada
    salah satu pihak kepada pihak liannya, maka Para Pihak sepakat untuk
    memasukkan sengketa tersebut ke Pengadilan Negeri Jakarta Barat.
</p>
<p>
    3. Ketentuan Penggunaan ini dibuat dalam Bahasa Inggris dan Bahasa
    Indonesia, yang keduanya merupakan versi yang mengikat Anda dengan kami.
    Dalam hal terjadi inkonsistensi antara versi Bahasa Inggris dan versi
    Bahasa Indonesia, versi Bahasa Indonesia akan berlaku.
</p>
<p>
    <strong>PASAL 9 - PENGAKHIRAN</strong>
</p>
<p>
    1. Akun IVet Data dapat ditutup karena hal-hal sebagai berikut:
</p>
<p>
    1. Permintaan oleh Anda;
</p>
<p>
    2. Kebijakan Kami berdasarkan hukum dan peraturan perundang-undangan yang
    berlaku;
</p>
<p>
    3. Keadaan Kahar terjadi selama 3 (tiga) bulan atau lebih secara
    berturut-turut; dan/ataualasan sehubungan dengan Pemblokiran Akun.
</p>
<p>
    2. Akun IVet Data Anda akan diakhiri secara otomatis atau ditutup ketika
    Anda menutup Aplikasi IVet Data Anda.
</p>
<p>
    3. Jika Anda tidak menggunakan Akun IVet Data Anda untuk jangka waktu
    minimum selama 6/12 bulan berturut-turut, kami dapat memblokir Akun IVet
    Data Anda. Untuk mengaktifkan kembali akun tersebut, Anda dapat mengajukan
    keluhan kepada Kami. Setelah melakukan verifikasi atas informasi personal
    Anda, kami dapat, berdasarkan diskresi kami, menentukan untuk mengakhiri
    atau melanjutkan pemblokiran akun.
</p>
<p>
    <strong>LAIN-LAIN</strong>
</p>
<p>
    1. Ketentuan ini adalah keseluruhan perjanjian antara Para Pihak sehubungan
    dengan subyek dari Ketentuan ini dan akan menggantikan seluruh perjanjian
    atau pengaturan sebelumnya. Ketentuan ini akan berlaku secara penuh dan
    mengikat sejak tanggal Anda menerima Ketentuan ini sampai Anda mengakhiri
    akun atau kami mengakhiri akun Anda di IVet Data.
</p>
<p>
    2. Apabila, pada setiap saat, suatu ketentuan dari Ketentuan ini terbukti
    menjadi atau menjadi illegal, tidak sah, atau tidak dapat dilaksanakan
    berdasarkan suatu hukum dari suatu jurisdiksi, tidak satupun legalitas,
    keabsahan atau pelaksaaan dari ketentuan lainnya terkena dampak atau
    menjadi tidak sah karenanya berdasarkan hukum dari suatu jurisdiksi.
</p>
<p>
    3. Anda tidak diperkenankan dan Anda sepakat untuk tidak mengalihkan setiap
    hak dan kewajiban yang anda miliki berdasarkan Ketentuan ini tanpa adanya
    persetujuan sebelumnya dari IVet Data. Kami memiliki hak untuk mengalihkan
    setiap hak dan kewajiban yang dimiliki berdasarkan Ketentuan ini kepada
    afiliasinya atau sehubungan dengan merjer, akuisisi, reorganisasi
    perusahaan, atau penjualan seluruh atau sebagian besar asset tanpa
    memberikan pemberitahuan. Segala usaha lainnya untuk mengalihkan adalah
    batal.
</p>
<p>
    4. Layanan kami dapat diinterupsi oleh kejadian atau hal tertentu di luar
    kewenangan dan kontrol kami (Keadaan Kahar), termasuk namun tidak terbatas
    pada bencana alam, gangguan listrik, gangguan telekomunikasi, kebijakan
    pemerintah, dan hal-hal lainnya yang di luar kewenangan dan kontrol kami.
    Anda oleh karenanya setuju untuk melepaskan kami dari setiap klaim, jika
    kami tidak dapat memenuhi instruksi Anda melalui Akun IVet Data baik
    sebagian maupun seluruhnya karena Keadaan Kahar. Jika Keadaan Kahar terus
    berlanjut untuk jangka waktu lebih dari 3 (tiga) bulan berturut-turut kami
    dapat menutup Akun IVet Data Anda.
</p>
<p>
    5. Keselamatan dan keamanan online Anda merupakan hal terpenting bagi kami.
    Kami memberlakukan standar keamanan yang wajar untuk melindungi data Anda
    yang sedang dalam proses pengiriman, ketika disimpan dan penggunaan Anda
    atas Akun IVet Data dan/atau Layanan IVet Data. Meskipun demikian, kami
    hendak menekankan kepada Anda bahwa tidak ada sistem yang tidak dapat
    ditembus dan hal ini dapat berakibat pada meningkatnya resiko atas
    informasi Anda dan penggunaan Akun IVet Data dan/atau Layanan IVet Data.
    Oleh karenanya, Anda setuju untuk melepaskan kami dari klaim apapun yang
    timbul sehubungan dengan virus, kerusakan, gangguan, atau bentuk lain dari
    gangguan sistem, termasuk akses tanpa otorisasi oleh pihak ketiga. Kami
    menganjurkan Anda untuk memberitahu kami segera jika Anda mengalami
    gangguan sistem apapun sebagaimana disebutkan di atas sehingga kami dapat
    berusaha memperbaiki gangguan tersebut.
</p>

          </div>
        </div>
      </div>
      <div class="footer-bar">