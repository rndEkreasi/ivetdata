<section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_40 section_padding_bottom_40">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<!-- <h2 class="highlight">News & Article</h2> -->
				<ol class="breadcrumb darklinks">
					<li>
						<a href="<?php echo base_url() ?>">
							Clinic Booking
						</a>
					</li>
					<li class="active">
						<a href="#"><?php echo $nameclinic ?></a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section>			
			<section id="about" class="ls section_padding_top_50 section_padding_bottom_100">
				<div class="container">
					<?php foreach ($dataclinic as $clinic) {
						$pictureclinic = $clinic->picture;
					?>					
					<div class="row">
						<div class="ds with_background transp_black_bg with_padding">

								<form class="contact-form row columns_padding_10" method="post" action="./">

									<div class="col-sm-6">
										<div class="form-group">
											<label for="name">Full Name
												<span class="required">*</span>
											</label>
											<i class="fa fa-user highlight3" aria-hidden="true"></i>
											<input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="phone">Phone Number
												<span class="required">*</span>
											</label>
											<i class="fa fa-phone highlight3" aria-hidden="true"></i>
											<input type="text" aria-required="true" size="30" value="" name="phone" id="phone" class="form-control" placeholder="Phone Number">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="email">Email address
												<span class="required">*</span>
											</label>
											<i class="fa fa-envelope highlight3" aria-hidden="true"></i>
											<input type="email" aria-required="true" size="30" value="" name="email" id="email" class="form-control" placeholder="Email Address">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="subject">Subject
												<span class="required">*</span>
											</label>
											<i class="fa fa-flag highlight3" aria-hidden="true"></i>
											<input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="subject">Date
												<span class="required">*</span>
											</label>
											<div class="form-group">
			                                    <div class="input-group date" data-provide="datepicker">
			                                        <input type="text" name="datevacc" value="<?php echo date('m/d/Y') ?>" class="form-control" id="datetimepicker1" required>
			                                        <div class="input-group-addon">
			                                            <span class="glyphicon glyphicon-th"></span>
			                                        </div>
			                                    </div>
			                                </div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label for="subject">Subject
												<span class="required">*</span>
											</label>
											<i class="fa fa-flag highlight3" aria-hidden="true"></i>
											<input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
										</div>
									</div>
									<div class="col-sm-12">

										<div class="form-group">
											<label for="message">Message</label>
											<i class="fa fa-comment highlight3" aria-hidden="true"></i>
											<textarea aria-required="true" rows="3" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea>
										</div>
									</div>

									<div class="col-sm-12 bottommargin_0">

										<div class="contact-form-submit topmargin_10">
											<button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button color3 wide_button margin_0">Send Now</button>
										</div>
									</div>

								</form>

							</div>
					</div>
					<?php } ?>
				</div>
			</section>

			

			
			<!-- <section id="blog" class="ls section_padding_100">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color3">
								From Our Blog
							</h2>
							<p class="small-text">Our latest news</p>
						</div>
					</div>
					<div class="row columns_margin_bottom_20">
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/05.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 12, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Shankle doner ribeye ham hock shank</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/06.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 13, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">T-bone capicola kevin pancetta</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/07.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 14, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Beef tri-tip brisket jerky boudin</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
					</div>
				</div>
			</section> -->
 <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init --->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                    defaultDate: "11/1/2013",
                    disabledDates: [
                        moment("12/25/2013"),
                        new Date(2013, 11 - 1, 21),
                        "11/22/2013 00:53"
                    ]
                });
        });
    </script>