<div class="pages">
  <div data-page="news" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="home.php"><img src="<?php echo base_url() ?>assets/images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <h2 class="page_title">Latest news</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="list-block">
            <ul class="posts newsitems">
              <?php foreach ($allarticle as $article) { ?>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="<?php echo $article->featuredimg ?>" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php"><?php echo $article->title ?></a></div>
                      <!-- <h2><a href="news-single.php"><?php echo $article->title ?></a></h2> -->
                    </div>
                    <div class="post_swipe"><img src="<?php echo base_url() ?>assets/images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="<?php echo base_url() ?>assets/images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="<?php echo base_url() ?>assets/images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="<?php echo base_url() ?>assets/images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <?php } ?>
              
            </ul>
            <div class="loadmore loadmore-newsitems">LOAD MORE</div>
            <div class="showless showless-newsitems">END</div>
          </div>
        </div>
      </div>
      <?php include 'layout/footer-bar.php' ?>
    </div>
  </div>
</div>