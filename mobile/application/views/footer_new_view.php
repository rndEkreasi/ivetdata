
                <div class="links-row">
                  <a href="#" onclick="window.location='<?php echo base_url() ?>/welcome/terms';">Terms &amp; Conditions</a>
                  <a href="#" onclick="window.location='<?php echo base_url() ?>/welcome/privacy';">Privacy Policy</a>
                  <a href="#" onclick="window.location='<?php echo base_url() ?>/welcome/faq';">FAQ</a>
                    <div class="copyright">&copy; IVETDATA 2019</div>
                </div>

              </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Login Popup -->
    <div class="popup popup-login">
      <div class="content-block">
        <h4>LOGIN</h4>
        <div class="loginform">
          <form id="LoginForm" method="post" action="<?php echo base_url() ?>welcome/auth_proses">
            <div class="input-custom"><input type="text" name="user_email" value="" class="form_input required" placeholder="username" /></div>
            <div class="input-custom"><input type="password" name="user_passnya" value="" class="form_input required" placeholder="password" /></div>
            <input type="hidden" name="clinic" value="0" />
            <div class="forgot_pass"><a href="#" data-popup=".popup-forgot" class="open-popup">Forgot Password?</a></div>
            <!-- <input type="submit" name="submit" class="form_submit" id="submit_login" value="LOGIN" /> -->
            <input type="submit" class="form_submit" id="submit_login" value="LOGIN">
          </form>
          <div class="signup_bottom">
            <p>Don't have an account?</p>
            <a href="#" data-popup=".popup-signup" class="open-popup">REGISTER</a>
          </div>
        </div>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="<?php echo base_url() ?>images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <!-- Register Popup -->
    <div class="popup popup-signup">
      <div class="content-block">
        <h4>REGISTER <span class="skewered-image"><img src="images/logogram.png" /></span></h4>
        <div class="loginform">
          <form id="RegisterForm" method="post" action="<?php echo base_url() ?>register/registerpetowner_process">
            <div class="input-custom"><input type="text" name="name" value="" class="form_input required" placeholder="Username" /></div>
            <div class="input-custom"><input type="text" name="email" value="" class="form_input required" placeholder="Email" /></div>
            <div class="input-custom"><input type="password" name="user_passnya" value="" class="form_input required" placeholder="Password" id="showpasswordinput" /><i class="iconbtn fas fa-eye" id="showpasswordtrigger" onClick="showPassword('showpasswordinput', 'showpasswordtrigger')"></i></div>
            <div class="input-custom"><input type="password" name="user_passnya2" value="" class="form_input required" placeholder="Repeat Password" id="showpasswordrepeatinput" /><i class="iconbtn fas fa-eye" id="showpasswordrepeattrigger" onClick="showPassword('showpasswordrepeatinput', 'showpasswordrepeattrigger')"></i></div>
            <div class="checkbox-custom mb-3"><input type="checkbox"/> Agree with our <a class="simple-link" href="<?php echo base_url() ?>termsconditions.html">Terms &amp; Conditions</a> and <a class="simple-link" href="/termsconditions.html">Privacy Policy</a></div>
            <input type="submit" name="submit" class="form_submit" id="submit_register" value="REGISTER" />
          </form>
        </div>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <!-- Forgot Password Popup -->
    <div class="popup popup-forgot">
      <div class="content-block">
        <h4>FORGOT PASSWORD</h4>
        <div class="loginform">
          <form id="ForgotForm" method="post" action="<?php echo base_url() ?>login/sendreset">
            <input type="text" name="Email" value="" class="form_input required" placeholder="email" />
            <input type="submit" name="submit" class="form_submit" id="submit_resetpassword" value="RESET PASSWORD" />
          </form>
          <div class="signup_bottom">
            <p>Check your email and follow the instructions to reset your password.</p>
          </div>
        </div>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="<?php echo base_url() ?>images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/framework7.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/email.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/circlemenu.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/audio.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/my-app.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/custom.js"></script>
    
  </body>
</html>