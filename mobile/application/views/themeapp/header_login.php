<!doctype html>
<html lang="en" class="md">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/img/f7-icon-square.png">
    <link rel="icon" href="<?php echo base_url() ?>assets/img/f7-icon.png">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css" type="text/css">


    <title>iVetData.com - Login</title>
</head>
<body class="color-theme-blue" >
    <!-- <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div> -->
    <div class="wrapper"  style="background-image: linear-gradient(to right, #fa709a 0%, #fee140 100%);">
        <!-- page main start -->
        <div class="page">
            <div class="page-content h-100 ">
                <!-- <div class="background color-dark"><img src="<?php echo base_url() ?>assets/img/background.png" alt=""></div> -->
                <div class="row mx-0">
                    <div class="col">
                        <img src="<?php echo base_url() ?>assets/img/logo.png" alt="" class="login-logo">
                        <h1 class="login-title"><small>Welcome to,</small><br>iVetData.com</h1>
                    </div>
                </div>