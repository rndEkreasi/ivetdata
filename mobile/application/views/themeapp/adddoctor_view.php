        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <!-- <figure><img src="<?php echo base_url() ?>assets/img/logo.png" alt=""></figure> iVetdata</a> -->
                    Add Pet Doctor <?php echo $nameclinic; ?></a>
                </div>
                <!-- <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                </div> -->
            </header>
            <div class="page-content">
                               
                <div class="tab-content h-100" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-find" role="tabpanel" aria-labelledby="nav-find-tab">
                        <div class="content-sticky-footer circle-background">
                            <div class="row m-0">
                                <div class="col mt-3">
                                    <div class="input-group searchshadow">
                                        <input type="text" class="form-control bg-white" placeholder="Find Pet id..." aria-label="">
                                        <div class="input-group-append">
                                            <button type="button" class="input-group-text "><i class="material-icons">search</i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>                           
                            
                            <h1 class="block-title color-dark text-center" style="font-size: 28px;color: #fff !important;line-height: 100%;">Add Pet Doctor</h1>
                            <div class="row">                                
                            <div class="col-12">
                                
                                <form action="<?php echo base_url() ?>doctor/addprocess" method="post" enctype="multipart/form-data">
                                <div class="card rounded-0 border-0 mb-3">
                                    
                                    <div class="card-body">
                                        <?php if (isset($error)){ ?>
                                            <div class="alert alert-danger"><?php echo $error; ?></div>
                                        <?php } ?>
                                        <div class="form-group ">
                                            <label>Name Doctor</label>
                                            <input type="text" name="namedoctor" value="<?php if (isset($namedoctor)){ echo $namedoctor; } ?>" class="form-control active"  required>
                                        </div>
                                        <div class="form-group ">
                                            <label>Phone</label>
                                            <input type="text" name="phone" value="<?php if (isset($phone)){ echo $phone; } ?>" class="form-control active"required >
                                        </div>
                                        <div class="form-group ">
                                            <label>Email</label>
                                            <input type="text" name="email" value="<?php if (isset($email)){ echo $email; } ?>" class="form-control active" required >
                                        </div>
                                    </div>
                                    <div class="card-footer p-0 border-0">
                                        <button class="btn btn-primary btn-block btn-lg rounded-0">Send</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                            
                            
                        </div>

                    </div>
                   
                </div>

            </div>
        </div>
        <!-- page main ends -->

    </div>


