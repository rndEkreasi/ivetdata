    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="<?php echo base_url() ?>assets/vendor/cookie/jquery.cookie.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="<?php echo base_url() ?>assets/vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- page specific script -->
    <script>
        $(window).on('load', function() {

            /*Swiper carousel */
            $('.nav-tabs a[href="#nav-cart"]').click(function() {
                setTimeout(function() {
                    var mySwiper4 = new Swiper('.carstcarousel', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        }
                    });
                }, 800)

            });
            /*Swiper carousel */
            $('.nav-tabs a[href="#nav-profile"]').click(function() {
                setTimeout(function() {
                    var mySwiper5 = new Swiper('.userprofilehome', {
                        slidesPerView: 2,
                        spaceBetween: 0,
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        }
                    });
                }, 800)

            });
            /*Swiper carousel */
            var mySwiper3 = new Swiper('.mostviewed', {
                slidesPerView: 2,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });

            /*Swiper carousel */
            var mySwiper2 = new Swiper('.categories', {
                slidesPerView: 'auto',
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
            /*Swiper carousel */
            var mySwiper1 = new Swiper('.homepage-restaurant', {
                slidesPerView: 1,
                spaceBetween: 15,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
            /* tooltip */
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });

    </script>

    
</body>

</html>