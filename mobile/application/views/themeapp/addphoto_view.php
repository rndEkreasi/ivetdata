  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/crop2/dist_files/jquery.imgareaselect.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/crop2/dist_files/jquery.form.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/crop2/dist_files/imgareaselect.css">
<script src="<?php echo base_url() ?>assets/js/crop2/functions.js"></script>

        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <!-- <figure><img src="<?php echo base_url() ?>assets/img/logo.png" alt=""></figure> iVetdata</a> -->
                    Add data <?php  echo $nameclinic; ?></a>
                </div>
                <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                </div>
            </header>
            <div class="page-content">
                               
                <div class="tab-content h-100" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-find" role="tabpanel" aria-labelledby="nav-find-tab">
                        <div class="content-sticky-footer circle-background">
                            <div class="row m-0">
                                <div class="col mt-3">
                                    <div class="input-group searchshadow">
                                        <input type="text" class="form-control bg-white" placeholder="Find Pet id..." aria-label="">
                                        <div class="input-group-append">
                                            <button type="button" class="input-group-text "><i class="material-icons">search</i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>                           
                            
                            <h1 class="block-title color-dark text-center" style="font-size: 31px;color: #fff !important;line-height: 100%;">Add Pet Photo</h1>
                            <div class="row">                                
                            <div class="col-12">
                                <form action="<?php echo base_url() ?>pet/addphoto" method="post" enctype="multipart/form-data">
                                <div class="card rounded-0 border-0 mb-3">
                                    <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                                    <!-- <div class="card-body">
                                        <div class="form-group ">
                                            <label>Upload Photo</label>
                                            <input type="file" name="test[image]" id="sample_input" class="form-control active"> 
                                            <input type="hidden" name="idpet" value="<?php echo $idpet ?>">
                                        </div>                                        
                                    </div>
                                    <div class="card-footer p-0 border-0">
                                        <button class="btn btn-primary btn-block btn-lg rounded-0">Send</button>
                                    </div> -->
                                    <div class="input-group" style="padding:0px 20px 50px;">                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="material-icons">person</i></span>
                                        </div>                                        
                                        <input type="file" class="form-control" placeholder="Pet Photo" name="petphoto" aria-label="Name">
                                        <input type="hidden" name="uid" value="<?php echo $uid; ?>">
                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>
                            
                            
                        </div>

                    </div>
                   
                </div>

            </div>
        </div>
        <!-- page main ends -->

    </div>


