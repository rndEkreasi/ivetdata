        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <figure><img src="<?php  echo base_url() ?>assets/img/logo.png" alt=""></figure> Detail Pet</a>
                </div>
                <!-- <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                    <a href="javascript:void(0)" class="menu-right"><i class="material-icons">person</i></a>
                </div> -->
            </header>
            <div class="page-content circle-background">
                <div class="content-sticky-footer"> 
                    <?php foreach ($datapet as $pet) {
                        $namepet = $pet->namapet;
                        $photo = $pet->photo;
                        $tipe = $pet->tipe;
                        $rfid = $pet->rfid;
                        $description = $pet->description;
                        if($photo =='' ){
                            $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                        }else{
                            $petphoto = $photo;
                        }
                    } ?>                
                    <div class="w-100">
                        <div class="carosel">
                            <div class="swiper-container swiper-init swipermultiple">
                                <div class="swiper-pagination"></div>
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block ">
                                            <div class="image" style="background-image: url(<?php echo $petphoto ?>);background-position: center;height: 280px;background-size: cover;"></div>
                                            <!-- <img src="<?php  echo $petphoto ?>" alt=""> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="text-center mt-0 mb-4" style="margin:-58px 0px 20px !important;"><?php echo $namepet ?></h2>
                    <h5 class="text-center" style="margin: -16px 0px 15px;">Pet ID : <?php echo $rfid ?></h5>
                    <div class="row mx-0">
                        <div class="col text-center">
                            <p><?php echo $description ?></p>
                        </div>
                        <div class="col-12">
                            <div class="card mb-3 wizard">
                                <div class="card-header p-0">
                                    <ul class="nav nav-tabs tabs-md nav-justified" role="tablist">
                                        <li role="presentation" class="nav-item">
                                            <a href="#step13" class="nav-link border-primary active show" data-toggle="tab" aria-controls="step13" role="tab" title="Step 1" aria-selected="true"><i class="material-icons">person</i></a>
                                        </li>
                                        <li role="presentation" class="nav-item">
                                            <a href="#step23" class="nav-link border-primary" data-toggle="tab" aria-controls="step23" role="tab" title="Step 2" aria-selected="false"><i class="material-icons">healing</i></a></a>
                                        </li>                                        
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active show" role="tabpanel" id="step13">
                                            <strong>Owner Detail</strong><br>
                                            <?php foreach ($dataowner as $owner) { ?>
                                            Name : <?php echo $owner->nama ?><br>
                                            Email :  <?php echo $owner->email ?><br>
                                            City : <?php echo $owner->city ?><br>
                                            Address : <?php echo $owner->address ?>
                                            <?php } ?><br><br>

                                            <a href="mailto:<?php echo $owner->email ?>" class="btn btn-outline-primary mb-2"><i class="material-icons">mail</i> Send Email</a> 
                                            <a href="tel:<?php echo $owner->nohp ?>" class="btn btn-outline-primary mb-2"><i class="material-icons">phone</i> Phone</a>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="step23">
                                            <strong>Health History</strong>
                                            <br>
                                            <div class="itemhis alert alert-info">
                                                <div class="time">Saturday, 19 Nov 2018 13.40 WIB</div>
                                                <div class="text">Pemberian vaksin anti virus</div>
                                                <div class="doby">Dr. Rinita 
                                                    <br>Muffins Petshop - Jakarta
                                                </div>
                                            </div>
                                            <div class="itemhis alert alert-info">
                                                <div class="time">Selasa, 21 Oktober 2018 08.40 WIB</div>
                                                <div class="text">Periksa rutin kesehatan anti rabies dan penyakit lainnya</div>
                                                <div class="doby">Dr. Anjanis 
                                                    <br>Muffins Petshop - Jakarta
                                                </div>
                                            </div>
                                            <div class="itemhis alert alert-info">
                                                <div class="time">Senin, 21 Agustus 2018 12.40 WIB</div>
                                                <div class="text">Pemberian vaksin anti rabies</div>
                                                <div class="doby">Dr. Rinita 
                                                    <br>Muffins Petshop - Jakarta
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                
            </div>
        </div>
        <!-- page main ends -->

    </div>

