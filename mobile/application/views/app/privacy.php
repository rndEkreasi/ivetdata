<div class="pages">
  <div data-page="privacy" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="home.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title">Privacy Policy</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
            <h4>
              Collection and Use of Personal Information
            </h4>
            <p>
              Personal information is data that can be used to identify or contact a single person.
            </p>
            <p>
              You may be asked to provide your personal information anytime you are in contact with Apple or an Apple affiliated company. Apple and its affiliates may share this personal information with each other and use it consistent with this Privacy Policy. They may also combine it with other information to provide and improve our products, services, content, and advertising. You are not required to provide the personal information that we have requested, but, if you chose not to do so, in many cases we will not be able to provide you with our products or services or respond to any queries you may have.
            </p>
            <p>
              Here are some examples of the types of personal information Apple may collect and how we may use it:
            </p>
            <h5 class="pb-2 pt-3">
              What personal information we collect
            </h5>
            <ul class="bullet-list">
              <li>
                When you create an Apple ID, apply for commercial credit, purchase a product, download a software update, register for a class at an Apple Retail Store, connect to our services, contact us including by social media or participate in an online survey, we may collect a variety of information, including your name, mailing address, phone number, email address, contact preferences, device identifiers, IP address, location information, credit card information and profile information where the contact is via social media.
              </li>
              <li>
                When you share your content with family and friends using Apple products, send gift certificates and products, or invite others to participate in Apple services or forums, Apple may collect the information you provide about those people such as name, mailing address, email address, and phone number. Apple will use such information to fulfill your requests, provide the relevant product or service, or for anti-fraud purposes.
              </li>
              <li>
                In certain jurisdictions, we may ask for a government issued ID in limited circumstances including when setting up a wireless account and activating your device, for the purpose of extending commercial credit, managing reservations, or as required by law.
              </li>
            </ul>
            <h5 class="pb-2 pt-3">
              How we use your personal information
            </h5>
            <p>
              We may process your personal information: for the purposes described in this Privacy Policy, with your consent, for compliance with a legal obligation to which Apple is subject or when we have assessed it is necessary for the purposes of the legitimate interests pursued by Apple or a third party to whom it may be necessary to disclose information.
            </p>
            <ul class="bullet-list">
              <li>
                The personal information we collect allows us to keep you posted on Apple’s latest product announcements, software updates, and upcoming events. If you don’t want to be on our mailing list, you can opt-out anytime by updating your preferences.
              </li>
              <li>
                We also use personal information to help us create, develop, operate, deliver, and improve our products, services, content and advertising, and for loss prevention and anti-fraud purposes. We may also use your personal information for account and network security purposes, including in order to protect our services for the benefit of all our users, and pre-screening or scanning uploaded content for potentially illegal content, including child sexual exploitation material. Where we use your information for anti-fraud purposes it arises from the conduct of an online transaction with us. We limit our uses of data for anti-fraud purposes to those which are strictly necessary and within our assessed legitimate interests to protect our customers and our services. For certain online transactions we may also validate the information provided by you with publicly accessible sources.
              </li>
              <li>
                We may use your personal information, including date of birth, to verify identity, assist with identification of users, and to determine appropriate services. For example, we may use date of birth to determine the age of Apple ID account holders.
              </li>
              <li>
                From time to time, we may use your personal information to send important notices, such as communications about purchases and changes to our terms, conditions, and policies. Because this information is important to your interaction with Apple, you may not opt out of receiving these communications.
              </li>
              <li>
                We may also use personal information for internal purposes such as auditing, data analysis, and research to improve Apple’s products, services, and customer communications.
              </li>
              <li>
                If you enter into a sweepstake, contest, or similar promotion we may use the information you provide to administer those programs.
              </li>
              <li>
                If you apply for a position at Apple or we receive your information in connection with a potential role at Apple, we may use your information to evaluate your candidacy and to contact you. If you are a candidate, you will receive more information about how Apple handles candidate personal information at the time of application.
              </li>
            </ul>
            <h4 class="pt-4">
              Collection and Use of Non-Personal Information
            </h4>
            <p>
              We also collect data in a form that does not, on its own, permit direct association with any specific individual. We may collect, use, transfer, and disclose non-personal information for any purpose. The following are some examples of non-personal information that we collect and how we may use it:
            </p>
            <ul class="bullet-list">
              <li>
                We may collect information such as occupation, language, zip code, area code, unique device identifier, referrer URL, location, and the time zone where an Apple product is used so that we can better understand customer behavior and improve our products, services, and advertising.
              </li>
              <li>
                We may collect information regarding customer activities on our website, iCloud services, our iTunes Store, App Store, Mac App Store, App Store for Apple TV and iBooks Stores and from our other products and services. This information is aggregated and used to help us provide more useful information to our customers and to understand which parts of our website, products, and services are of most interest. Aggregated data is considered non‑personal information for the purposes of this Privacy Policy.
              </li>
              <li>
                We may collect and store details of how you use our services, including search queries. This information may be used to improve the relevancy of results provided by our services. Except in limited instances to ensure quality of our services over the Internet, such information will not be associated with your IP address.
              </li>
              <li>
                With your explicit consent, we may collect data about how you use your device and applications in order to help app developers improve their apps.
              </li>
            </ul>
            <p>
              If we do combine non-personal information with personal information the combined information will be treated as personal information for as long as it remains combined.
            </p>
          </div>
        </div>
      </div>
      <?php include 'layout/footer-bar.php' ?>
    </div>
  </div>