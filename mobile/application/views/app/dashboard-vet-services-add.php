        <div class="pages">
          <div data-page="dashboard-vet-owners-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-services.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Add Service / Medicine</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row">
                      <a href="#tabaddsvc-service" class="tab-link active button">Add Service</a>
                      <a href="#tabaddsvc-medicine" class="tab-link button">Add Medicine</a>
                  </div>
                  <div class="tabs-simple">
                    <div class="tabs">
                      <div id="tabaddsvc-service" class="tab active">
                        <div class="editform">
                          <form>
                            <div class="form_row">
                              <label>Service Photo (max. size 5MB):</label>
                              <div class="preview-image"></div>
                              <input type="file" name="servicephoto" value="" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Service Name:</label>
                              <input type="text" name="servicename" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Sell Price (Rp.):</label>
                              <input type="text" name="sellprice" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Purchase Price (Rp.):</label>
                              <input type="text" name="purchaseprice" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Service Fee (Rp.):</label>
                              <input type="text" name="servicefee" value="" class="form_input" />
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Add Service" />
                          </form>
                        </div>
                      </div>
                      <div id="tabaddsvc-medicine" class="tab">
                        <div class="editform">
                          <form>
                            <div class="form_row">
                              <label>Medicine Photo (max. size 5MB):</label>
                              <div class="preview-image"></div>
                              <input type="file" name="medicinephoto" value="" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Medicine Name:</label>
                              <input type="text" name="medicinename" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Sell Price (Rp.):</label>
                              <input type="text" name="sellprice" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Purchase Price (Rp.):</label>
                              <input type="text" name="purchaseprice" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Stock (Quantity):</label>
                              <input type="text" name="stock" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Unit:</label>
                              <input type="text" name="unit" value="" class="form_input" />
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Add Medicine" />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>