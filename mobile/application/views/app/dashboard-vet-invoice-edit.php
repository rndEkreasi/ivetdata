        <div class="pages">
          <div data-page="dashboard-vet-mypets-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-invoice.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Invoice #1662931295</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Phone:</label>
                        <input type="text" name="phone" value="+628119722321" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Name:</label>
                        <input type="text" name="name" value="Wenna" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Pet Name:</label>
                        <select name="petname" class="form_select">
                          <option value="jasmine" selected>Jasmine</option>
                          <option value="chiky">Chiky</option>
                          <option value="killy">Killy</option>
                          <option value="kitty">Kitty</option>
                        </select>
                      </div>
                      <div class="form_row required">
                        <label>Email:</label>
                        <input type="text" name="email" value="wenna@ivetdata.com" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Dokter:</label>
                        <select name="doctor" class="form_select">
                          <option value="30" selected>Drh. Tommy Soekiato</option>
                          <option value="363">Drh Diana</option>
                          <option value="381">Drh Bambang</option>
                          <option value="387">Maroko</option>
                          <option value="553">Drh Marco</option>
                          <option value="563">Asep Rusmana</option>
                        </select>
                      </div>
                      <div class="form_row required list-block mb-5">
                        <label>Invoice Date:</label>
                        <input type="date" name="invoice_date" value="2019-07-21" class="form_input" />
                      </div>
                      <table class="custom_table input_table mb-3">
                        <thead>
                          <tr>
                            <th>ITEM</th>
                            <th>PRICE</th>
                            <th>QTY</th>
                            <th>TOTAL</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><input type="text" name="item1" value="Ultrasound" /></td>
                            <td><input type="text" name="price1" value="200000" /></td>
                            <td><input type="text" name="qty1" value="1" /></td>
                            <td class="text-right" nowrap>Rp. 200,000</td>
                          </tr>
                        <tbody>
                          <tr>
                            <td><input type="text" name="item2" value="Infus NaCl, asering, glukosa" /></td>
                            <td><input type="text" name="price2" value="50000" /></td>
                            <td><input type="text" name="qty2" value="1" /></td>
                            <td class="text-right" nowrap>Rp. 50,000</td>
                          </tr>
                        <tbody>
                          <tr>
                            <td><input type="text" name="item3" value="Consultation" /></td>
                            <td><input type="text" name="price3" value="100000" /></td>
                            <td><input type="text" name="qty3" value="1" /></td>
                            <td class="text-right" nowrap>Rp. 100,000</td>
                          </tr>
                          <tr>
                            <td colspan="4">
                              <a href="#" class="btn btn-sm btn-primary">Add Item</a>
                              <a href="dashboard-vet-services.php" class="btn btn-sm btn-secondary">List of Service &amp; Medicine</a>
                            </td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Sub Total</b></td>
                            <td class="text-right" nowrap>Rp. 350,000</td>
                          </tr>
                          <tr>
                            <td>Total Quantity: 3 Units</td>
                            <td class="text-right" colspan="2"><b>Discount</b></td>
                            <td class="text-right" nowrap><input type="text" name="discount" /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Tax (%)</b></td>
                            <td class="text-right" nowrap><input type="text" name="tax" placeholder="5-10" value="10" /> Rp. 35,000</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Grand Total</b></td>
                            <td class="text-right" nowrap>Rp. 385,000</td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="form_row">
                        <label>Note:</label>
                        <textarea name="note" class="form_textarea" rows="" cols=""></textarea>
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Update Invoice" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>