        <div class="pages">
          <div data-page="dashboard-owner-invoice" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div id="pages_maincontent">
                <div class="success_message">
                  <span>Thank You!</span>
                  <img src="images/icons/lineicons/delivery-red.png" alt="" title="" />
                  <p>Your order is on the way.</p>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>