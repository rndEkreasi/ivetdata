        <div class="pages">
          <div data-page="dashboard-vet-mypets-log-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-mypets-log-detail.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Medical History</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <table class="custom_table form_table mb-3 editform">
                        <tbody>
                          <tr>
                            <td>Date Medical *</td>
                            <td><input type="date" name="date" value="2019-05-20" class="form_input" /></td>
                          </tr>
                          <tr>
                            <td>Vets</td>
                            <td><input type="text" name="vetname" value="Drh. Tommy Soekiato" class="form_input" /></td>
                          </tr>
                          <tr>
                            <td>Diagnosis *</td>
                            <td><textarea name="diagnosis" class="form_textarea" rows="" cols="" ><?= '<p>Clean</p>' ?></textarea></td>
                          </tr>
                          <tr>
                            <td>Treatment</td>
                            <td>
                              <div class="row">
                                <div class="col col-75">
                                  <input type="text" name="treatment1" value="Fraktur Surgery" class="form_input" />
                                </div>
                                <div class="col col-25">
                                  <button class="btn btn-table btn-light"><i class="fas fa-trash-alt mr-1"></i></button>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col col-100">
                                  <button class="btn btn-table btn-secondary"><i class="fas fa-plus mr-1"></i> Add Item</button>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td>Anamnesis *</td>
                            <td><textarea name="anamnesis" class="form_textarea" rows="" cols="" ><?= '<p>Medical Check</p>' ?></textarea></td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="custom-accordion table-accordion mb-3">
                        <div class="accordion-item">
                          <div class="accordion-item-toggle">
                            <i class="icon icon-plus">+</i>
                            <i class="icon icon-minus">-</i>
                            <span>Basic Checkup</span>
                          </div>
                          <div class="accordion-item-content">
                            <table class="custom_table form_table mb-3 editform">
                              <tbody>
                                <tr>
                                  <td>Weight</td>
                                  <td>
                                    <div class="row">
                                      <div class="col col-50">
                                        <input type="text" name="weight" value="10" class="form_input" />
                                      </div>
                                      <div class="col col-50">Kg</div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Temperature</td>
                                  <td>
                                    <div class="row">
                                      <div class="col col-50">
                                        <input type="text" name="weight" value="32" class="form_input" />
                                      </div>
                                      <div class="col col-50">Celcius</div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Heart Rate</td>
                                  <td>
                                    <div class="row">
                                      <div class="col col-50">
                                        <input type="text" name="heartrate" value="43" class="form_input" />
                                      </div>
                                      <div class="col col-50">beats/minute</div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Respiration Rate</td>
                                  <td>
                                    <div class="row">
                                      <div class="col col-50">
                                        <input type="text" name="respirationrate" value="22" class="form_input" />
                                      </div>
                                      <div class="col col-50">breaths/minute</div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>BCS</td>
                                  <td>
                                    <div class="row">
                                      <div class="col col-50">
                                        <input type="text" name="bcs" value="10" class="form_input" />
                                      </div>
                                      <div class="col col-50"></div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Food</td>
                                  <td><textarea name="anamnesis" class="form_textarea" rows="" cols="" ><?= 'Lack of meat' ?></textarea></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="custom-accordion table-accordion mb-3">
                        <div class="accordion-item">
                          <div class="accordion-item-toggle">
                            <i class="icon icon-plus">+</i>
                            <i class="icon icon-minus">-</i>
                            <span>Physical Exam</span>
                          </div>
                          <div class="accordion-item-content">
                            <table class="custom_table form_table mb-3 editform">
                              <tbody>
                                <tr>
                                  <td>Eyes and Ears</td>
                                  <td><input type="text" name="eyesandears" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Skin and Coat</td>
                                  <td><input type="text" name="skinandcoat" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Respiratory System</td>
                                  <td><input type="text" name="respiratorysystem" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Circulation System</td>
                                  <td><input type="text" name="circulationsystem" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Digestive System</td>
                                  <td><input type="text" name="digestivesystem" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Urinary System</td>
                                  <td><input type="text" name="urinarysystem" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Nervous System</td>
                                  <td><input type="text" name="nervoussystem" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Musculoskeletal System</td>
                                  <td><input type="text" name="musculoskeletalsystem" value="" class="form_input" /></td>
                                </tr>
                                <tr>
                                  <td>Other notes</td>
                                  <td><input type="text" name="othernotes" value="" class="form_input" /></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="custom-accordion table-accordion mb-3">
                        <div class="accordion-item">
                          <div class="accordion-item-toggle">
                            <i class="icon icon-plus">+</i>
                            <i class="icon icon-minus">-</i>
                            <span>Diagnostic Tools Result</span>
                          </div>
                          <div class="accordion-item-content">
                            <table class="custom_table form_table mb-3 editform">
                              <tbody>
                                <tr>
                                  <td>Hematology</td>
                                  <td>
                                    <div class="form_row">
                                      <div class="preview-image"><img src="images/uploads/hematology.png" alt="" title="" /></div>
                                      <input type="file" name="hematology" value="" class="form_input" />
                                      <textarea name="hematology-text" class="form_textarea" rows="" cols="" ></textarea>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Blood Chemistry</td>
                                  <td>
                                    <div class="form_row">
                                      <div class="preview-image"></div>
                                      <input type="file" name="bloodchemistry" value="" class="form_input" />
                                      <textarea name="bloodchemistry-text" class="form_textarea" rows="" cols="" ></textarea>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Ultrasonography</td>
                                  <td>
                                    <div class="form_row">
                                      <div class="preview-image"></div>
                                      <input type="file" name="ultrasonography" value="" class="form_input" />
                                      <textarea name="ultrasonography-text" class="form_textarea" rows="" cols="" ></textarea>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>X-ray</td>
                                  <td>
                                    <div class="form_row">
                                      <div class="preview-image"></div>
                                      <input type="file" name="xray" value="" class="form_input" />
                                      <textarea name="xray-text" class="form_textarea" rows="" cols="" ></textarea>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Other Diagnostic</td>
                                  <td>
                                    <div class="form_row">
                                      <div class="preview-image"></div>
                                      <input type="file" name="otherdiagnostic" value="" class="form_input" />
                                      <textarea name="otherdiagnostic-text" class="form_textarea" rows="" cols="" ></textarea>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <p>
                      * Mandatory, must be filled.
                      </p>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Edit Medical History" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>