<div class="pages">
          <div data-page="dashboard-vet-appointments" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-vet.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">APPOINTMENTS LIST <a href="dashboard-vet-appointments-add.php"><i class="fas fa-plus"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="list-block">
                    <div class="searchbox mb-3">
                      <form>
                        <input type="text" name="search" value="" placeholder="Search" />
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </form>
                    </div>
                    <ul class="posts dvaitems">
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3>Chiky</h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>12 Jul 2019<br>13:00</p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p class="pt-1 pb-0"><b>Owner: Wenna</b></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p class="pb-0">
                                      <a href="tel:+628119722321" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:wenna@ivetdata.com" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-appointments-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="dashboard-vet-appointments-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-appointments-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3>Mello</h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>15 Jul 2019<br>13:00</p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p class="pt-1 pb-0"><b>Owner: Wenna</b></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p class="pb-0">
                                      <a href="tel:+628119722321" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:wenna@ivetdata.com" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-appointments-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="dashboard-vet-appointments-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-appointments-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3>Chiky</h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>12 Jul 2019<br>13:00</p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p class="pt-1 pb-0"><b>Owner: Wenna</b></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p class="pb-0">
                                      <a href="tel:+628119722321" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:wenna@ivetdata.com" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-appointments-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="dashboard-vet-appointments-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-appointments-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3>Mello</h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>15 Jul 2019<br>13:00</p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p class="pt-1 pb-0"><b>Owner: Wenna</b></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p class="pb-0">
                                      <a href="tel:+628119722321" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:wenna@ivetdata.com" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-appointments-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="dashboard-vet-appointments-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-appointments-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3>Chiky</h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>12 Jul 2019<br>13:00</p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p class="pt-1 pb-0"><b>Owner: Wenna</b></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p class="pb-0">
                                      <a href="tel:+628119722321" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:wenna@ivetdata.com" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-appointments-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="dashboard-vet-appointments-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-appointments-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h3>Mello</h3>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>15 Jul 2019<br>13:00</p>
                                </div>
                              </div>
                              <div class="infocard mb-2">
                                <div class="row">
                                  <div class="col col-60">
                                    <p class="pt-1 pb-0"><b>Owner: Wenna</b></p>
                                  </div>
                                  <div class="col col-40 text-right">
                                    <p class="pb-0">
                                      <a href="tel:+628119722321" class="btn btn-sm btn-secondary"><i class="fas fa-phone"></i></a>
                                      <a href="mailto:wenna@ivetdata.com" class="btn btn-sm btn-secondary"><i class="fas fa-envelope"></i></a>
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-appointments-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                          <a href="dashboard-vet-appointments-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-appointments-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                    </ul>
                    <div class="loadmore loadmore-dvaitems">LOAD MORE</div>
                    <div class="showless showless-dvaitems">END</div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>