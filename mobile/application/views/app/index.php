<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
    <link href="images/apple-touch-startup-image-320x460.png" media="(device-width: 320px)"
      rel="apple-touch-startup-image">
    <link href="images/apple-touch-startup-image-640x920.png"
      media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
    <title>iVetData</title>
    <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="plugins/fontawesome/css/all.min.css"> <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap4classes.css"> <!-- Bootstrap 4 Classes -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="custom.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900' rel='stylesheet' type='text/css'>
  </head>

  <body id="mobile_wrap">
    <div class="statusbar-overlay"></div>
    <div class="panel-overlay"></div>
    <div class="views">
      <div class="view view-main">
        <div class="navbarpages nobg">
          <ul id="navs" class="navtopleft" data-open="−" data-close="+">
            <li><a href="home.php"><i class="fas fa-home"></i></a></li>
            <li><a href="about.php"><i class="fas fa-info"></i></a></li>
            <li><a href="contact.php"><i class="fas fa-phone"></i></a></li>
            <li><a href="news.php"><i class="far fa-newspaper"></i></a></li>
          </ul>
        </div>
        <div class="pages">
          <div data-page="index" class="page homepage">
            <div class="page-content page-smallfooter">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- FullBG -->
              <div class="fullbg" style="background-image:url(images/slides/slide-1.jpg);">
                <div class="fullbg-cover">
                  <div class="fullbg-content">
                    <div class="tabs-custom">
                      <div class="buttons-row">
                        <a href="#tabhome-microchip" class="tab-link active button mr-1"><i class="fas fa-paw mr-1"></i>Microchip</a>
                        <a href="#tabhome-vetsearch" class="tab-link button"><i class="fas fa-user-friends mr-1"></i>Vets</a>
                      </div>
                      <div class="tabs-simple mb-3">
                        <div class="tabs">
                          <div id="tabhome-microchip" class="tab active">
                            <form class="search-form">
                                <div class="form-row mb-3">
                                  <input type="text" name="name" value="" class="form-input" placeholder="Microchip ID" />
                                  <button class="btn btn-primaryred">Search</i></button>
                                </div>
                                <div class="form-row">
                                  <p>Enter the microchip ID above and click search. Enter only the 9, 10 or 15 character microchip number, with no punctuation or spaces.</p>
                                </div>
                            </form>
                          </div>
                          <div id="tabhome-vetsearch" class="tab">
                            <form class="search-form">
                              <div class="form-row mb-3">
                                <input type="text" name="name" value="" class="form-input" placeholder="Vet Name" />
                                <input type="text" name="name" value="" class="form-input" placeholder="City" />
                                <button class="btn btn-primaryred">Find</button>
                              </div>
                              <div class="form-row">
                                <p>Enter the name of the vet or the city that you want to search.</p>
                              </div>
                            </form>
                          </div> 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col btns-row">
                          <a data-popup=".popup-signup" class="open-popup close-panel btn btn-secondary btn-lg">REGISTER</a>
                          <a data-popup=".popup-login" class="open-popup close-panel btn btn-primary btn-lg">LOGIN</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar.php' ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Login Popup -->
    <div class="popup popup-login">
      <div class="content-block">
        <h4>LOGIN</h4>
        <div class="loginform">
          <form id="LoginForm" method="post">
            <div class="input-custom"><input type="text" name="Username" value="" class="form_input required" placeholder="username" /></div>
            <div class="input-custom"><input type="password" name="Password" value="" class="form_input required" placeholder="password" /></div>
            <div class="forgot_pass"><a href="#" data-popup=".popup-forgot" class="open-popup">Forgot Password?</a></div>
            <!-- <input type="submit" name="submit" class="form_submit" id="submit_login" value="LOGIN" /> -->
            <a href="dashboard-owner.php" class="form_submit" id="submit_login">LOGIN</a>
          </form>
          <div class="signup_bottom">
            <p>Don't have an account?</p>
            <a href="#" data-popup=".popup-signup" class="open-popup">REGISTER</a>
          </div>
        </div>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <!-- Register Popup -->
    <div class="popup popup-signup">
      <div class="content-block">
        <h4>REGISTER <span class="skewered-image"><img src="images/logogram.png" /></span></h4>
        <div class="loginform">
          <form id="RegisterForm" method="post">
            <div class="input-custom"><input type="text" name="Username" value="" class="form_input required" placeholder="Username" /></div>
            <div class="input-custom"><input type="text" name="Email" value="" class="form_input required" placeholder="Email" /></div>
            <div class="input-custom"><input type="password" name="Password" value="" class="form_input required" placeholder="Password" id="showpasswordinput" /><i class="iconbtn fas fa-eye" id="showpasswordtrigger" onClick="showPassword('showpasswordinput', 'showpasswordtrigger')"></i></div>
            <div class="input-custom"><input type="password" name="Repeat Password" value="" class="form_input required" placeholder="Repeat Password" id="showpasswordrepeatinput" /><i class="iconbtn fas fa-eye" id="showpasswordrepeattrigger" onClick="showPassword('showpasswordrepeatinput', 'showpasswordrepeattrigger')"></i></div>
            <div class="checkbox-custom mb-3"><input type="checkbox"/> Agree with our <a class="simple-link" href="terms.php">Terms &amp; Conditions</a> and <a class="simple-link" href="privacy.php">Privacy Policy</a></div>
            <input type="submit" name="submit" class="form_submit" id="submit_register" value="REGISTER" />
          </form>
        </div>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <!-- Forgot Password Popup -->
    <div class="popup popup-forgot">
      <div class="content-block">
        <h4>FORGOT PASSWORD</h4>
        <div class="loginform">
          <form id="ForgotForm" method="post">
            <input type="text" name="Email" value="" class="form_input required" placeholder="email" />
            <input type="submit" name="submit" class="form_submit" id="submit_resetpassword" value="RESET PASSWORD" />
          </form>
          <div class="signup_bottom">
            <p>Check your email and follow the instructions to reset your password.</p>
          </div>
        </div>
        <div class="close_popup_button">
          <a href="#" class="close-popup"><img src="images/icons/black/menu_close.png" alt="" title="" /></a>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/framework7.js"></script>
    <script type="text/javascript" src="js/jquery.swipebox.js"></script>
    <script type="text/javascript" src="js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="js/email.js"></script>
    <script type="text/javascript" src="js/circlemenu.js"></script>
    <script type="text/javascript" src="js/audio.min.js"></script>
    <script type="text/javascript" src="js/my-app.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
  </body>
</html>