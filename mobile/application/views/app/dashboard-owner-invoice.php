        <div class="pages">
          <div data-page="dashboard-owner-invoice" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">WENNA LOJAYA'S INVOICE LIST</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="list-block">
                    <div class="searchbox mb-3">
                      <form>
                        <input type="text" name="search" value="" placeholder="Search" />
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </form>
                    </div>
                    <ul class="posts doiitems">
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662931295</a></h4>
                                  <p><b>Rp. 350,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>12 Jul 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662951924</a></h4>
                                  <p><b>Rp. 1,330,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>11 Jul 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662984368</a></h4>
                                  <p><b>Rp. 1,150,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>09 Jul 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662915066</a></h4>
                                  <p><b>Rp. 120,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>05 Jul 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662920430</a></h4>
                                  <p><b>Rp. 160,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>23 May 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662969555</a></h4>
                                  <p><b>Rp. 210,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>21 May 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662966565</a></h4>
                                  <p><b>Rp. 100,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>20 May 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662988477</a></h4>
                                  <p><b>Rp. 310,000</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>07 May 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4><a class="simple-link" href="dashboard-vet-invoice-detail.php">1662922021</a></h4>
                                  <p><b>Rp. 199,500</b></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p>18 Apr 2019</p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-owner-invoice-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                        </div>
                      </li>
                    </ul>
                    <div class="loadmore loadmore-doiitems">LOAD MORE</div>
                    <div class="showless showless-doiitems">END</div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>