        <div class="pages">
          <div data-page="dashboard-owner-mypets" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">WENNA LOJAYA'S PET LIST <a href="dashboard-owner-mypets-add.php"><i class="fas fa-plus"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row">
                      <a href="#tabmypets-pets" class="tab-link active button">My Pets</a>
                      <a href="#tabmypets-clinics" class="tab-link button">My Clinics</a>
                  </div>
                  <div class="tabs-simple">
                    <div class="tabs">
                      <div id="tabmypets-pets" class="tab active">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <form>
                              <input type="text" name="search" value="" placeholder="Search" />
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div>
                          <ul class="posts dompitems">
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo1.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-owner-mypets-detail.php">Chiky</a></h4>
                                    <span>123-456-789</span>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-owner-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-owner-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-owner-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo2.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-owner-mypets-detail.php">Pelong</a></h4>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-owner-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-owner-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-owner-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo3.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-owner-mypets-detail.php">Jasmine</a></h4>
                                    <span>123-456-789</span>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-owner-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-owner-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-owner-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo4.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-owner-mypets-detail.php">Archie</a></h4>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-owner-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-owner-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-owner-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo5.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-owner-mypets-detail.php">Bonnie</a></h4>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-owner-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-owner-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-owner-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo6.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-owner-mypets-detail.php">Chiko</a></h4>
                                    <span>123-456-789</span>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-owner-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-owner-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-owner-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                          </ul>
                          <div class="loadmore loadmore-dompitems">LOAD MORE</div>
                          <div class="showless showless-dompitems">END</div>
                        </div>
                      </div>
                      <div id="tabmypets-clinics" class="tab">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <form>
                              <input type="text" name="search" value="" placeholder="Search" />
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div>
                          <ul class="posts domcitems">
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/clinics/1.png" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Tail Mail's Clinic</h4>
                                    <p>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</p>
                                    <p><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                  </div>
                                </div>
                              </div>
                            </li>
                          </ul>
                          <div class="loadmore loadmore-domcitems">LOAD MORE</div>
                          <div class="showless showless-domcitems">END</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>