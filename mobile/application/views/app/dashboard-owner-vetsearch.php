        <div class="pages">
          <div data-page="dashboard-owner-vetsearch" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">SEARCH VET</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="map-container mb-3">
                    <img src="images/placeholders/map.png" />
                  </div>
                  <div class="searchbox mb-3">
                    <form>
                      <input type="text" name="search" value="" placeholder="Search" />
                      <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </form>
                  </div>
                  <div class="custom-accordion table-accordion mb-3">
                    <div class="accordion-item">
                      <div class="accordion-item-toggle">
                        <i class="icon icon-plus">+</i>
                        <i class="icon icon-minus">-</i>
                        <span>Choose Category</span>
                      </div>
                      <div class="accordion-item-content bgcolor-lightgrey">
                        <div class="main-nav category-filters icons_31">
                          <ul>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/examination.png"/></div>
                                <span>Examination</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/vaccination.png"/></div>
                                <span>Vaccination</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/spayingandcastrating.png"/></div>
                                <span>Spaying &amp; Castrating</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/functionaldiagnostics.png"/></div>
                                <span>Functional Diagnostics</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/dentistry.png"/></div>
                                <span>Dentistry</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/ophthalmology.png"/></div>
                                <span>Ophthalmology</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/surgery.png"/></div>
                                <span>Surgery</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/dayandnightclinic.png"/></div>
                                <span>Day &amp; Night Clinic</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/dermatology.png"/></div>
                                <span>Dermatology</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/nailbeakandteethtrimming.png"/></div>
                                <span>Nail, Beak &amp; Teeth Trimming</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/pharmacy.png"/></div>
                                <span>Pharmacy</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/microchipping.png"/></div>
                                <span>Microchipping</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/laborinduction.png"/></div>
                                <span>Labor Induction</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/housecalls.png"/></div>
                                <span>House Calls</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="list-block">
                    <ul class="posts dovsitems">
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry">
                            <div class="post_thumb"><img src="images/profiles/profile5.jpg" alt="" title="" /></div>
                            <div class="post_details">
                              <h4>Diana</h4>
                              <div class="mb-2">Tail Mail's Clinic</div>
                              <p>Jln Kelapa Puan No 10</p>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry">
                            <div class="post_thumb"><img src="images/profiles/profile4.jpg" alt="" title="" /></div>
                            <div class="post_details">
                              <h4>Nana</h4>
                              <div class="mb-2">Tail Mail's Clinic</div>
                              <p>Jln Kelapa Puan No 10</p>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry">
                            <div class="post_thumb"><img src="images/profiles/profile3.jpg" alt="" title="" /></div>
                            <div class="post_details">
                              <h4>Mike Helde</h4>
                              <div class="mb-2">Tail Mail's Clinic</div>
                              <p>Jln Kelapa Puan No 10</p>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry">
                            <div class="post_thumb"><img src="images/profiles/profile2.jpg" alt="" title="" /></div>
                            <div class="post_details">
                              <h4>Hida</h4>
                              <div class="mb-2">Tail Mail's Clinic</div>
                              <p>Jln Kelapa Puan No 10</p>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry">
                            <div class="post_thumb"><img src="images/profiles/profile1.jpg" alt="" title="" /></div>
                            <div class="post_details">
                              <h4>Martha</h4>
                              <div class="mb-2">Tail Mail's Clinic</div>
                              <p>Jln Kelapa Puan No 10</p>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div class="loadmore loadmore-dovsitems">LOAD MORE</div>
                    <div class="showless showless-dovsitems">END</div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>