        <div class="pages">
          <div data-page="dashboard-vet-subscription-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Subscription Detail -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-subscription.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Subscription</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Phone:</label>
                        <input type="text" name="phone" value="+6281214939955" class="form_input" disabled />
                      </div>
                      <div class="form_row required">
                        <label>Name:</label>
                        <input type="text" name="name" value="Drh. Tommy Soekiato" class="form_input" disabled />
                      </div>
                      <div class="form_row required mb-4">
                        <label>Email:</label>
                        <input type="text" name="email" value="contact@ivetdata.com" class="form_input" disabled />
                      </div>
                      <table class="custom_table input_table mb-3">
                        <thead>
                          <tr>
                            <th>ITEM</th>
                            <th>MONTH</th>
                            <th>PRICE</th>
                            <th>TOTAL</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Basic Subscription</td>
                            <td>
                              <select name="subscription">
                                <option value="">Select</option>
                                <option value="3 Month">3 Month</option>
                                <option value="6 Month">6 Month</option>
                                <option value="12 Month">12 Month</option>
                                <option value="24 Month">24 Month</option>
                              </select>
                            </td>
                            <td>Rp. 250,000</td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                        <tbody>
                          <tr>
                            <td>Diana <b class="label label-sm label-blue">Admin Staff</b></td>
                            <td></td>
                            <td>Rp.55,000 /month</td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td>Mike Helde <b class="label label-sm label-green">Vet</b></td>
                            <td></td>
                            <td>Rp.100,000 /month</td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td>Martha <b class="label label-sm label-green">Vet</b></td>
                            <td></td>
                            <td>Rp.100,000 /month</td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td>Nana <b class="label label-sm label-green">Vet</b></td>
                            <td></td>
                            <td>Rp.100,000 /month</td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td>Hida <b class="label label-sm label-blue">Admin Staff</b></td>
                            <td></td>
                            <td>Rp.55,000 /month</td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td class="text-right" colspan="3"><b>Sub Total</b></td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td class="text-right" colspan="3"><b>Additional Staff</b></td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Grand Total</b></td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td class="text-right" colspan="3"><b>Discount Code</b></td>
                            <td class="text-right" nowrap><input type="text" name="discount" /></td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="form_row required">
                        <label>Pay Method:</label>
                        <select name="type" class="form_select">
                          <option value="MANDIRI">Bank Mandiri</option>
                          <option value="BNI">Bank Negara Indonesia</option>
                          <option value="BRI">Bank Rakyat Indonesia</option>
                        </select>
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Buy Subscription" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>