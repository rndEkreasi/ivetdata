        <div class="pages">
          <div data-page="dashboard-vet-services-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-services.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Service</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <div class="form_row">
                        <label>Service Photo (max. size 5MB):</label>
                        <div class="preview-image"><img src="images/uploads/comprehensivebloodtest.jpg" alt="" title="" /></div>
                        <input type="file" name="servicephoto" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Service Name:</label>
                        <input type="text" name="servicename" value="Comprehensive Blood Test" class="form_input" />
                      </div>
                      <div class="form_row">
                        <label>Sell Price (Rp.):</label>
                        <input type="text" name="sellprice" value="1,000,000" class="form_input" />
                      </div>
                      <div class="form_row">
                        <label>Purchase Price (Rp.):</label>
                        <input type="text" name="purchaseprice" value="2,000,000" class="form_input" />
                      </div>
                      <div class="form_row">
                        <label>Service Fee (Rp.):</label>
                        <input type="text" name="servicefee" value="1,000,000" class="form_input" />
                      </div>
                      <button class="form_submit btn-secondary"><i class="fas fa-lock"></i> Unpublish</button>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Update Service" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>