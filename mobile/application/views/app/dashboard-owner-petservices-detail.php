        <div class="pages">
          <div data-page="dashboard-owner-petservices-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="dashboard-owner-petservices.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Fraktur Surgery</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="shop_item">
                    <div class="shop_thumb mb-4">
                      <a rel="gallery-3" href="images/uploads/fracturesurgery.jpg" title="Photo title" class="swipebox"><img src="images/uploads/fracturesurgery.jpg" alt="" title="" /></a>
                      <div class="shop_item_price">Rp. 500,000</div>
                      <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/white/love.png" alt="" title="" /></a>
                      <a href="#" data-popup=".popup-social" class="open-popup shopfriend"><img src="images/icons/white/users.png" alt="" title="" /></a>
                    </div>
                    <div class="buttons-row">
                        <a href="#tabshopitem-iteminfo" class="tab-link active button">Product Info</a>
                        <a href="#tabshopitem-sellerinfo" class="tab-link button">Seller Info</a>
                    </div>
                    <div class="tabs-simple mb-3">
                      <div class="tabs">
                        <div id="tabshopitem-iteminfo" class="tab active">
                          <div class="shop_item_details">
                            <h3>PRODUCT DESCRIPTION</h3>
                            <p>Prednisolone is a steroid medication used to treat certain types of allergies, inflammatory conditions, autoimmune disorders, and cancers. Some of these conditions include adrenocortical insufficiency, high blood calcium, rheumatoid arthritis, dermatitis, eye inflammation, asthma, and multiple sclerosis.</p>
                            <p><strong>CATEGORY:</strong> MEDICINE</p>
                          </div>
                        </div>
                        <div id="tabshopitem-sellerinfo" class="tab">
                          <div class="shop_item_details">
                            <ul class="iconed-list">
                              <li><i class="fas fa-clinic-medical mr-1"></i><b class="mr-2">Name:</b><span>Drh. Tommy Soekiato</span></span></li>
                              <li><i class="fas fa-map-marker-alt mr-1"></i><b class="mr-2">Address:</b><span>Jln. Cut Mutiah Blok D No.7 Bekasi</span></li>
                              <li><i class="fas fa-city mr-1"></i><b class="mr-2">City:</b><span>Bekasi</span></li>
                              <li><i class="fas fa-globe-asia mr-1"></i><b class="mr-2">Country:</b><span>Indonesia</span></li>
                              <li><i class="fas fa-envelope mr-1"></i><b class="mr-2">Email:</b><span><a href="mailto:contact@ivetdata.com" class="simple-link">contact@ivetdata.com</a></span></li>
                              <li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><a href="tel:+681214939955" class="simple-link">+681214939955</a></span></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                    <h3>SELECT QUANTITY</h3>
                    <div class="item_qnty_shopitem">
                      <form id="myform" method="POST" action="#">
                        <input type="button" value="-" class="qntyminusshop" field="quantity" />
                        <input type="text" name="quantity" value="1" class="qntyshop" />
                        <input type="button" value="+" class="qntyplusshop" field="quantity" />
                      </form>
                    </div>
                    <a href="cart.html" class="button_full btyellow">ADD TO CART</a>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>