        <div class="pages">
          <div data-page="dashboard-vet-team-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-team.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Vet</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="notes mb-3">
                    <p class="mb-1">Vet Staff can access:</p>
                    <ul class="simple-list mb-1">
                      <li>Access Vet's own dashboard</li>
                      <li>Pet database: Full Access</li>
                      <li>Pet owner : Full Access</li>
                      <li>Stock Inventory : View Only</li>
                      <li>Invoice : Full Access</li>
                    </ul>
                  </div>
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Name:</label>
                        <input type="text" name="teamvetname" value="Nana" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Email:</label>
                        <input type="text" name="teamvetemail" value="nana@ivetdata.com" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Phone:</label>
                        <input type="text" name="teamvetphone" value="+629813840304" class="form_input" />
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Update Vet" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>