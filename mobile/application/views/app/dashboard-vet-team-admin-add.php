        <div class="pages">
          <div data-page="dashboard-vet-team-add" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Add -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-team.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Add Admin Staff</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="notes mb-3">
                    <p class="mb-1">Admin Staff can access:</p>
                    <ul class="simple-list mb-1">
                      <li>Dashboard : None</li>
                      <li>Pet database: Full access register pet, view only medical record</li>
                      <li>Pet owner : View only</li>
                      <li>Stock Inventory : Full Access</li>
                      <li>Invoice : Full Access</li>
                    </ul>
                  </div>
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Name:</label>
                        <input type="text" name="teamvetname" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Email:</label>
                        <input type="text" name="teamvetemail" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Phone:</label>
                        <input type="text" name="teamvetphone" value="" class="form_input" />
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Add Admin Staff" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>