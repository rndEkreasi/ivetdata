        <div class="pages">
          <div data-page="dashboard-vet-mypets-vacc-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-mypets-vacc-detail.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Vaccination History</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <table class="custom_table form_table mb-3 editform">
                        <tbody>
                          <tr>
                            <td>Date Vaccine *</td>
                            <td><input type="date" name="date" value="2019-07-27" class="form_input" /></td>
                          </tr>
                          <tr>
                            <td>Vets</td>
                            <td><input type="text" name="vetname" value="Drh. Tommy Soekiato" class="form_input" /></td>
                          </tr>
                          <tr>
                            <td>Vaccine</td>
                            <td><input type="text" name="vaccine" value="Cazitel Plus for Cat 4 kg" class="form_input" /></td>
                          </tr>
                          <tr>
                            <td>Injection Site</td>
                            <td><textarea name="diagnosis" class="form_textarea" rows="" cols="" ><?= '<p>Neck</p>' ?></textarea></td>
                          </tr>
                          <tr>
                            <td>Date Vaccine *</td>
                            <td><input type="date" name="date" value="2020-07-31" class="form_input" /></td>
                          </tr>
                        </tbody>
                      </table>
                      <p>
                      * Mandatory, must be filled.
                      </p>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Edit Vaccination History" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>