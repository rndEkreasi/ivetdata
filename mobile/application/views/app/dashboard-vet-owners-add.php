<div class="pages">
          <div data-page="dashboard-vet-owners-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-mypets.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Add Customer</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Customer's Name:</label>
                        <input type="text" name="ownerfullname" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Phone Number:</label>
                        <input type="text" name="ownerphone" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Email Address:</label>
                        <input type="text" name="owneremail" value="" class="form_input" />
                      </div>
                      <div class="form_row">
                        <label>Address:</label>
                        <textarea name="owneraddress" class="form_textarea" rows="" cols=""></textarea>
                      </div>
                      <div class="form_row">
                        <label>City:</label>
                        <input type="text" name="ownercity" value="" class="form_input" />
                      </div>
                      <div class="form_row">
                        <label>Country:</label>
                        <select name="ownercountry" class="form_select">
                          <option value="indonesia">Indonesia</option>
                          <option value="singapore">Singapore</option>
                          <option value="japan">Japan</option>
                          <option value="skorea">South Korea</option>
                          <option value="vietnam">Vietnam</option>
                          <option value="philippines">Philippines</option>
                          <option value="mongolia">Mongolia</option>
                        </select>
                      </div>
                      <p>
                      * Mandatory, must be filled.
                      </p>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Add Customer" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>