        <div class="pages">
          <div data-page="dashboard-owner-mypets-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="dashboard-owner-mypets.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <div class="custom-image"><img src="images/photos/photo1.jpg" alt="" title="" /></div>
                <h2 class="page_title">Chiky <a href="dashboard-owner-mypets-edit.php"><i class="fas fa-pen"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row">
                      <a href="#tabinfo-pet" class="tab-link active button">Pet Diary</a>
                      <a href="#tabinfo-owner" class="tab-link button">Owner Info</a>
                      <a href="#tabinfo-clinic" class="tab-link button">Clinic Info</a>
                  </div>
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabinfo-pet" class="tab active">
                        <ul class="timeline-list">
                          <li class="timeline-list-add">
                            <a class="simple-link mb-3" href="dashboard-owner-mypets-diary-add.php"><span class="btn btn-sm btn-secondary btn-round mr-1"><i class="fas fa-plus"></i></span> Add New Diary Entry</a>
                          </li>
                          <li>
                            <div class="timeline-list-index">09 Jul, 2019</div>
                            <div class="timeline-flex">
                              <div class="timeline-info mb-2">
                                <p class="mb-1">Tried new food today! Non-organic, but, very healthy.</p>
                              </div>
                              <div class="timeline-buttons mb-2">
                                <a class="btn btn-sm btn-round btn-primary mr-1" href="dashboard-owner-mypets-diary-edit.php"><i class="fas fa-pen"></i></a>
                                <a class="btn btn-sm btn-round btn-light" href="dashboard-owner-mypets-diary-delete.php"><i class="fas fa-trash"></i></a>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="timeline-list-index">30 May, 2019</div>
                            <div class="timeline-flex">
                              <div class="timeline-info mb-2">
                                <p class="mb-1">Chiky's tongue got cut today while I as trimming her.</p>
                              </div>
                              <div class="timeline-buttons mb-2">
                                <a class="btn btn-sm btn-round btn-primary mr-1" href="dashboard-owner-mypets-diary-edit.php"><i class="fas fa-pen"></i></a>
                                <a class="btn btn-sm btn-round btn-light" href="dashboard-owner-mypets-diary-delete.php"><i class="fas fa-trash"></i></a>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="timeline-list-index">21 May, 2019</div>
                            <div class="timeline-flex">
                              <div class="timeline-info mb-2">
                                <p class="mb-1">Chiky met some new friends today at her morning walk. One of them was a bit shy, but they're both very friendly.</p>
                              </div>
                              <div class="timeline-buttons mb-2">
                                <a class="btn btn-sm btn-round btn-primary mr-1" href="dashboard-owner-mypets-diary-edit.php"><i class="fas fa-pen"></i></a>
                                <a class="btn btn-sm btn-round btn-light" href="dashboard-owner-mypets-diary-delete.php"><i class="fas fa-trash"></i></a>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div class="timeline-list-index">17 Jul, 2018</div>
                            <div class="timeline-flex">
                              <div class="timeline-info mb-2">
                                <p class="mb-1">Registered pet to iVetdata.com</p>
                              </div>
                              <div class="timeline-buttons mb-2">
                                <a class="btn btn-sm btn-round btn-primary mr-1" href="dashboard-owner-mypets-diary-edit.php"><i class="fas fa-pen"></i></a>
                                <a class="btn btn-sm btn-round btn-light" href="dashboard-owner-mypets-diary-delete.php"><i class="fas fa-trash"></i></a>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div id="tabinfo-owner" class="tab">
                        <ul class="iconed-list">
                          <li><i class="fas fa-user mr-1"></i><b class="mr-2">Name:</b><span>Wenna</span></span></li>
                          <li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><a href="tel:+628119722321" class="simple-link">+628119722321</a></span></li>
                          <li><i class="fas fa-envelope mr-1"></i><b class="mr-2">Email:</b><span><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></span></li>
                          <li><i class="fas fa-map-marker-alt mr-1"></i><b class="mr-2">Address:</b><span>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</span></li>
                          <li><i class="fas fa-city mr-1"></i><b class="mr-2">City:</b><span>Batam</span></li>
                          <li><i class="fas fa-globe-asia mr-1"></i><b class="mr-2">Country:</b><span>Indonesia</span></li>
                        </ul>
                      </div>
                      <div id="tabinfo-clinic" class="tab">
                        <ul class="iconed-list">
                          <li><i class="fas fa-clinic-medical mr-1"></i><b class="mr-2">Clinic Name:</b><span>Tail Mail's Clinic</span></span></li>
                          <li><i class="fas fa-map-marker-alt mr-1"></i><b class="mr-2">Address:</b><span>Jln Kelapa Puan No 10</span></li>
                          <li><i class="fas fa-city mr-1"></i><b class="mr-2">City:</b><span>Batam</span></li>
                          <li><i class="fas fa-globe-asia mr-1"></i><b class="mr-2">Country:</b><span>Indonesia</span></li>
                          <li><i class="fas fa-envelope mr-1"></i><b class="mr-2">Email:</b><span><a href="mailto:contact@ivetdata.com" class="simple-link">contact@ivetdata.com</a></span></li>
                          <li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><a href="tel:+6281214939954" class="simple-link">+6281214939954</a></span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="buttons-row">
                      <a href="#tabhistory-medical" class="tab-link active button">Medical History</a>
                      <a href="#tabhistory-vaccine" class="tab-link button">Vaccine History</a>
                  </div>
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabhistory-medical" class="tab active">
                        <ul class="timeline-list">
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-owner-mypets-log-detail.php" class="simple-link">09 Jul, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Parvo</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-owner-mypets-log-detail.php" class="simple-link">30 May, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Parvovirus</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-owner-mypets-log-detail.php" class="simple-link">21 May, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Skabies</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-owner-mypets-log-detail.php" class="simple-link">17 Jul, 2018</a></div>
                            <div class="timeline-list-sub"><span>Register Date</span></div>
                            <p>Registered pet to iVetdata.com</p>
                          </li>
                        </ul>
                      </div>
                      <div id="tabhistory-vaccine" class="tab">
                        <ul class="timeline-list">
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-owner-mypets-vacc-detail.php" class="simple-link">24 Apr, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Parvovirus</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-owner-mypets-vacc-detail.php" class="simple-link">14 Feb, 2018</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Rabies</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>