<div class="pages">
  <div data-page="news" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <h2 class="page_title">Latest news</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="list-block">
            <ul class="posts newsitems">
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="images/photos/photo15.jpg" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php">PET CARE</a></div>
                      <h2><a href="news-single.php">Persiapan untuk Memiliki Anak Anjing Baru</a></h2>
                    </div>
                    <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="images/photos/photo13.jpg" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php">PET TECH</a></div>
                      <h2><a href="news-single.php">Fungsi Microchip pada Hewan Ternak</a></h2>
                    </div>
                    <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="images/photos/photo14.jpg" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php">PET FACTS</a></div>
                      <h2><a href="news-single.php">Manfaat Hewan Peliharaan bagi Manusia</a></h2>
                    </div>
                    <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="images/photos/photo12.jpg" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php">EVENTS</a></div>
                      <h2><a href="news-single.php">PRESS RELEASE KEGIATAN HALAL BI HALAL PB PDHI 1440 H</a></h2>
                    </div>
                    <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="images/photos/photo10.jpg" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php">EVENTS</a></div>
                      <h2><a href="news-single.php">Event : Serba - Serbi Microchip</a></h2>
                    </div>
                    <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
              <li class="swipeout">
                <div class="swipeout-content item-content">
                  <div class="post_entry">
                    <div class="post_thumb"><img src="images/photos/photo11.jpg" alt="" title="" /></div>
                    <div class="post_details">
                      <div class="post_category"><a href="news.php">PET FACTS</a></div>
                      <h2><a href="news-single.php">Hewan Peliharaan</a></h2>
                    </div>
                    <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                  </div>
                </div>
                <div class="swipeout-actions-right">
                  <a href="news-single.php" class="action1"><img src="images/icons/black/message.png" alt=""
                      title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/like.png" alt="" title="" /></a>
                  <a href="#" class="action1 open-popup" data-popup=".popup-social"><img
                      src="images/icons/black/contact.png" alt="" title="" /></a>
                </div>
              </li>
            </ul>
            <div class="loadmore loadmore-newsitems">LOAD MORE</div>
            <div class="showless showless-newsitems">END</div>
          </div>
        </div>
      </div>
      <?php include 'layout/footer-bar.php' ?>
    </div>
  </div>
</div>