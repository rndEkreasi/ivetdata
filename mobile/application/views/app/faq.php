<div class="pages">
  <div data-page="privacy" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="home.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title">Frequently Asked Questions</h2>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
            <div class="custom-accordion faq-accordion">
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>What is iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData is an interactive query service that makes it easy to analyze data in Amazon S3 using standard SQL. Athena is serverless, so there is no infrastructure to setup or manage, and you can start analyzing data immediately. You don’t even need to load your data into Athena, it works directly with data stored in S3. To get started, just log into the Athena Management Console, define your schema, and start querying. iVetData uses Presto with full standard SQL support and works with a variety of standard data formats, including CSV, JSON, ORC, Apache Parquet and Avro. While iVetData is ideal for quick, ad-hoc querying and integrates with Amazon QuickSight for easy visualization, it can also handle complex analysis, including large joins, window functions, and arrays.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>What can I do with iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData helps you analyze data stored in Amazon S3. You can use Athena to run ad-hoc queries using ANSI SQL, without the need to aggregate or load the data into Athena. iVetData can process unstructured, semi-structured, and structured data sets. Examples include CSV, JSON, Avro or columnar data formats such as Apache Parquet and Apache ORC. iVetData integrates with Amazon QuickSight for easy visualization. You can also use iVetData to generate reports or to explore data with business intelligence tools or SQL clients, connected via an ODBC or JDBC driver.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>How do I get started with iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    To get started with iVetData, simply log into the AWS Management Console for Athena and create your schema by writing DDL statements on the console or by using a create table wizard. You can then start querying data using a built-in query editor. Athena queries data directly from Amazon S3 so there’s no loading required.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>How do you access iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData can be accessed via the AWS Management Console, an API, or an ODBC or JDBC driver. You can programmatically run queries, add tables or partitions using the ODBC or JDBC driver.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>What is the underlying technology behind iVetData?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData uses Presto with full standard SQL support and works with a variety of standard data formats, including CSV, JSON, ORC, Avro, and Parquet. Athena can handle complex analysis, including large joins, window functions, and arrays. Because iVetData uses Amazon S3 as the underlying data store, it is highly available and durable with data redundantly stored across multiple facilities and multiple devices in each facility. Learn more about Presto here.
                  </p>
                </div>
              </div>
              <div class="accordion-item">
                <div class="accordion-item-toggle">
                  <i class="icon icon-plus">+</i>
                  <i class="icon icon-minus">-</i>
                  <span>How does iVetData store table definitions and schema?</span>
                </div>
                <div class="accordion-item-content">
                  <p>
                    iVetData uses a managed Data Catalog to store information and schemas about the databases and tables that you create for your data stored in Amazon S3. In regions where AWS Glue is available, you can upgrade to using the AWS Glue Data Catalog with iVetData. In regions where AWS Glue is not available, Athena uses an internal Catalog.
                  </p>
                  <p>
                    You can modify the catalog using DDL statements or via the AWS Management Console. Any schemas you define are automatically saved unless you explicitly delete them. Athena uses schema-on-read technology, which means that your table definitions applied to your data in S3 when queries are being executed. There’s no data loading or transformation required. You can delete table definitions and schema without impacting the underlying data stored on Amazon S3.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include 'layout/footer-bar.php' ?>
    </div>
  </div>