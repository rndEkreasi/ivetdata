        <div class="pages">
          <div data-page="dashboard-vet-mypets" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-vet.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <div class="buttons-row row-clear">
                    <a href="#tablist-pet" class="tab-link active button">Pet List</a>
                    <a href="#tablist-owner" class="tab-link button">Owner List</a>
                </div>
                <div class="tabs-simple mb-5">
                  <div class="tabs">
                    <div id="tablist-pet" class="tab active">
                      <h2 class="page_title">Total Pet Record: 40 <a href="dashboard-vet-mypets-add.php"><i class="fas fa-plus"></i></a></h2>
                      <div class="page_single layout_fullwidth_padding">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <form>
                              <input type="text" name="search" value="" placeholder="Search" />
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div>
                          <ul class="posts dvmpitems">
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo1.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-vet-mypets-detail.php">Chiky</a></h4>
                                    <span>123-456-789</span>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-vet-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo2.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-vet-mypets-detail.php">Pelong</a></h4>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-vet-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo3.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-vet-mypets-detail.php">Jasmine</a></h4>
                                    <span>123-456-789</span>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-vet-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo4.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-vet-mypets-detail.php">Archie</a></h4>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-vet-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo5.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-vet-mypets-detail.php">Bonnie</a></h4>
                                    <a href="#" class="btn btn-sm btn-primary">Get Microchip</a>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-vet-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/photos/photo6.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4><a href="dashboard-vet-mypets-detail.php">Chiko</a></h4>
                                    <span>123-456-789</span>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-mypets-detail.php" class="action1"><i class="fas fa-eye"></i></a>
                                <a href="dashboard-vet-mypets-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-mypets-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                          </ul>
                          <div class="loadmore loadmore-dvmpitems">LOAD MORE</div>
                          <div class="showless showless-dvmpitems">END</div>
                        </div>
                      </div>
                    </div>
                    <div id="tablist-owner" class="tab">
                      <h2 class="page_title">Number of Owners: 5 <a href="dashboard-vet-owners-add.php"><i class="fas fa-plus"></i></a></h2>
                      <div class="page_single layout_fullwidth_padding">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <form>
                              <input type="text" name="search" value="" placeholder="Search" />
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div>
                          <ul class="posts dvoitems">
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/profiles/profile1.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Wenna</h4>
                                    <p class="pb-1"><a href="tel:+628119722321" class="simple-link">+628119722321</a></p>
                                    <p class="pb-1"><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></p>
                                    <p class="pb-1"><a href="dashboard-vet-mypets-detail.php" class="btn btn-sm btn-primary">7 pets</a></p>
                                    <p class="pb-3">Jl Taman Anggrek, Jakarta</p>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-owners-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-owners-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/profiles/profile2.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Elly</h4>
                                    <p class="pb-1"><a href="tel:+6281144805" class="simple-link">+6281144805</a></p>
                                    <p class="pb-1"><a href="mailto:elly@gmail.com" class="simple-link">elly@gmail.com</a></p>
                                    <p class="pb-1"><a href="dashboard-vet-mypets-detail.php" class="btn btn-sm btn-primary">3 pets</a></p>
                                    <p class="pb-3">Taman kencana</p>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-owners-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-owners-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/profiles/profile3.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Roy Ang</h4>
                                    <p class="pb-1"><a href="tel:+628118744804" class="simple-link">+628118744804</a></p>
                                    <p class="pb-1"><a href="mailto:rioadityaang@gmail.com" class="simple-link">rioadityaang@gmail.com</a></p>
                                    <p class="pb-1"><a href="dashboard-vet-mypets-detail.php" class="btn btn-sm btn-primary">17 pets</a></p>
                                    <p class="pb-3">Jl Taman Crystal 2 No 33</p>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-owners-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-owners-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/profiles/profile4.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Diana</h4>
                                    <p class="pb-1"><a href="tel:+62811944444" class="simple-link">+62811944444</a></p>
                                    <p class="pb-1"><a href="mailto:diana@ivetdata.com" class="simple-link">diana@ivetdata.com</a></p>
                                    <p class="pb-1"><a href="dashboard-vet-mypets-detail.php" class="btn btn-sm btn-primary">2 pets</a></p>
                                    <p class="pb-3">Bogor</p>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-owners-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-owners-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry">
                                  <div class="post_thumb"><img src="images/profiles/profile5.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <h4>Hidayati</h4>
                                    <p class="pb-1"><a href="tel:+6285697117560" class="simple-link">+6285697117560</a></p>
                                    <p class="pb-1"><a href="mailto:hidadu@rocketmail.com" class="simple-link">hidadu@rocketmail.com</a></p>
                                    <p class="pb-1"><a href="dashboard-vet-mypets-detail.php" class="btn btn-sm btn-primary">4 pets</a></p>
                                    <p class="pb-3">Jl Taman Anggrek</p>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-owners-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-owners-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                          </ul>
                          <div class="loadmore loadmore-dvoitems">LOAD MORE</div>
                          <div class="showless showless-dvoitems">END</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>