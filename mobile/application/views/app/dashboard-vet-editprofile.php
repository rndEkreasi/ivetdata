        <div class="pages">
          <div data-page="dashboard-vet-editprofile" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit My Profile</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <div class="buttons-row">
                        <a href="#tabeditvet-info" class="tab-link active button"><i class="fas fa-user mr-2"></i> My Profile</a>
                        <a href="#tabeditvet-clinic" class="tab-link button"><i class="fas fa-clinic-medical mr-2"></i> My Clinic</a>
                        <a href="#tabeditvet-password" class="tab-link button"><i class="fas fa-key mr-2"></i> Password</a>
                    </div>
                    <div class="tabs-simple">
                      <div class="tabs">
                        <div id="tabeditvet-info" class="tab active">
                          <form>
                            <div class="form_row">
                              <label>Profile Picture (max. size 5MB):</label>
                              <div class="preview-image"><img src="images/profiles/profile3.jpg" alt="" title="" /></div>
                              <input type="file" name="profilepicture" value="" class="form_input" />
                              <p>Image resolution size must be 300px x 300px</p>
                            </div>
                            <div class="form_row">
                              <label>Full Name:</label>
                              <input type="text" name="vetfullname" value="Drh. Tommy Soekiato" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>KTP:</label>
                              <input type="text" name="ktp" value="872364726347" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>KTA PDHI:</label>
                              <input type="text" name="ktapdhi" value="123456789" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Phone Number:</label>
                              <input type="text" name="vetphone" value="+681214939955" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Email Address:</label>
                              <input type="text" name="vetemail" value="contact@ivetdata.com" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Address:</label>
                              <textarea name="vetaddress" class="form_textarea" rows="" cols="">Jln. Cut Mutiah Blok D No.7 Bekasi</textarea>
                            </div>
                            <div class="form_row">
                              <label>City:</label>
                              <input type="text" name="vetcity" value="Bekasi" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Country:</label>
                              <select name="vetcountry" class="form_select">
                                <option value="indonesia" selected>Indonesia</option>
                                <option value="singapore">Singapore</option>
                                <option value="japan">Japan</option>
                                <option value="skorea">South Korea</option>
                                <option value="vietnam">Vietnam</option>
                                <option value="philippines">Philippines</option>
                                <option value="mongolia">Mongolia</option>
                              </select>
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Profile" />
                          </form>
                        </div>
                        <div id="tabeditvet-clinic" class="tab ">
                          <form>
                            <div class="form_row">
                              <label>Clinic Logo (max. size 5MB):</label>
                              <div class="preview-image"><img src="images/clinics/1.png" alt="" title="" /></div>
                              <input type="file" name="profilepicture" value="" class="form_input" />
                              <p>Image resolution size must be 300px x 300px</p>
                            </div>
                            <div class="form_row">
                              <label>Clinic Name:</label>
                              <input type="text" name="clinicname" value="Tail Mail's Clinic" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Address:</label>
                              <textarea name="clinicaddress" class="form_textarea" rows="" cols="">Jln Kelapa Puan No 10</textarea>
                            </div>
                            <div class="form_row">
                              <label>Phone Number:</label>
                              <input type="text" name="clinicphone" value="+681214939954" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>City:</label>
                              <input type="text" name="cliniccity" value="Bekasi" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Country:</label>
                              <select name="cliniccountry" class="form_select">
                                <option value="indonesia" selected>Indonesia</option>
                                <option value="singapore">Singapore</option>
                                <option value="japan">Japan</option>
                                <option value="skorea">South Korea</option>
                                <option value="vietnam">Vietnam</option>
                                <option value="philippines">Philippines</option>
                                <option value="mongolia">Mongolia</option>
                              </select>
                            </div>
                            <div class="form_row">
                              <label>Postal Code:</label>
                              <input type="text" name="postalcode" value="17113" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>SIP:</label>
                              <input type="text" name="sip" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Latitude:</label>
                              <input type="text" name="sip" value="-6.268518" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Longitude:</label>
                              <input type="text" name="sip" value="106.976868" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Display Information in Public Pet Database:</label>
                              <div class="form_row_right">
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="clinicname">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Clinic's Name</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="vetname">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Vet's Name</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="clinicphone">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Clinic's Phone</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="clinicemail">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Clinic's Email</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="clinicaddress">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Clinic's Address</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Clinic" />
                          </form>
                        </div>
                        <div id="tabeditvet-password" class="tab">
                          <form>
                            <div class="form_row">
                              <label>Old Password:</label>
                              <div class="input-custom"><input type="password" name="Old Password" value="" class="form_input required" placeholder="Old Password" id="showoldpasswordinput" /><i class="iconbtn fas fa-eye" id="showoldpasswordtrigger" onClick="showPassword('showoldpasswordinput', 'showoldpasswordtrigger')"></i></div>
                            </div>
                            <div class="form_row">
                              <label>New Password:</label>
                              <div class="input-custom"><input type="password" name="New Password" value="" class="form_input required" placeholder="New Password" id="shownewpasswordinput" /><i class="iconbtn fas fa-eye" id="shownewpasswordtrigger" onClick="showPassword('shownewpasswordinput', 'shownewpasswordtrigger')"></i></div>
                            </div>
                            <div class="form_row">
                              <label>Repeat Password:</label>
                              <div class="input-custom"><input type="password" name="Repeat Password" value="" class="form_input required" placeholder="Repeat Password" id="showpasswordrepeatinput" /><i class="iconbtn fas fa-eye" id="showpasswordrepeattrigger" onClick="showPassword('showpasswordrepeatinput', 'showpasswordrepeattrigger')"></i></div>
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Password" />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>