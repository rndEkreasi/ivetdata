<div class="pages">
  <div data-page="news-single" class="page no-toolbar no-navbar page-bged">
    <div class="page-content page-smallfooter">
      <div class="navbarpages nobg">
        <div class="navbar_logo_right">
          <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
        </div>
      </div>
      <div id="pages_maincontent">
        <a href="news.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
        <h2 class="blog_title">Fungsi Microchip pada Hewan Ternak</h2>
        <!-- Slider -->
        <div class="swiper-container-pages swiper-init" data-effect="slide" data-pagination=".swiper-pagination">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <img src="images/news/news-1.jpg" alt="" title="" />
            </div>
            <div class="swiper-slide">
              <img src="images/news/news-2.jpg" alt="" title="" />
            </div>
            <div class="swiper-slide">
              <img src="images/news/news-3.jpg" alt="" title="" />
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
        <div class="page_single layout_fullwidth_padding">
          <div class="post_single">
            <p>
            Pet Database harus dibuat agar microchip berfungsi dengan baik sehingga para petani tidak perlu menggunakan branding atau stok chipping mikro untuk mencegah pencurian, kata Wakil Ketua Peternak Sapi Federated Wayne Langford.
            </p>
            <p>
            Cara terbaik untuk meminimalkan pencurian sapi perah adalah memiliki sistem audit peternakan yang baik, pemantauan stok yang baik dan rekonsiliasi, katanya.
            </p>
            <p>
            Tetapi, peternak sapi perah Golden Bay mempertanyakan sistem pelacakan identifikasi hewan nasional dan mengapa sistem tersebut tidak beroperasi secara maksimal karena masih terdapat pencurian sapi perah Mid Canterbury baru-baru ini, termasuk 500 sapi yang hilang dari satu peternakan.
            </p>
            <p>
            "Jika sistem dapat dioperasikan sepenuhnya dengan benar oleh para peternak maka sistem pelacakan identifikasi hewan nasional tidak akan mengalami masalah." kata Langford.
            </p>
            <p>
            “Kami membutuhkan sistem pet database yang lebih baik. Jika kita ingin sistem bekerja dengan baik, kita harus berusaha."
            </p>
            <p>
            “Mengapa kita perlu memiliki dua pet database, satu dari swasta dan satu untuk pemerintah? Seharusnya kita bisa mengintegrasikan semua sistem. Agar memungkinkan peternak untuk bekerja hanya dengan satu basis data. ”
            </p>
            <p>
            Microchip perlu dilakukan agar sistem pemantauan identifikasi hewan dan audit pertanian diterapkan dan dikelola dengan baik.
            </p>
            <p>
            “Kami melihat microchip sebagai salah satu cara pentingnya memiliki sistem audit yang baik, pemantauan stok yang baik, dan rekonsiliasi. Jika sebuah peternakan tidak tahu berapa banyak sapi yang diperah setiap minggu dan tidak dapat menghitung setiap sapi setiap hari maka ia perlu melihat sistemnya dan mengubahnya” kata Langford.
            </p>
            <p>
            Pada peternakan skala besar, pemilik pertanian bertanggung jawab dan harus memiliki sistem yang dapat mereka periksa kapan saja, katanya.
            </p>
            <p>
            Sistem ini didirikan untuk menerapkan sistem keterlacakan seumur hidup yang efektif untuk produk ternak dan hewan.
            </p>
            <p>
            Microchip tidak termasuk kartu sim atau unit GPS atau perangkat keras lain yang dapat digunakan untuk melacak hewan.
            </p>
            <p>
            "Oleh karena itu, kami tidak memiliki kemampuan untuk menentukan lokasi hewan ini," kata juru bicara dar OSPRI itu.
            </p>
            <p>
            Dokter hewan dari Canterbury, Glen Beeman, mengatakan microchip layak dipertimbangkan.
            </p>
            <p>
            "Teknologi telah berubah sedemikian rupa sehingga layak pada ternak, hanya perlu ditempatkan di suatu tempat yang bukan lokasi penyembelihan, misalnya di pangkal telinga atau ekor. Microchip merupakan pilihan yang sangat praktis.  Pemindai menjadi lebih mudah tersedia dan lebih murah. ”
            </p>
            <p>
            Beeman mengatakan pada tingkat komersial ia berharap perusahaan yang memproduksi chip akan datang ke peternakannya.
            </p>
            <p>
            "Jelas untuk microchip empat juta sapi akan dihargai sesuai dan  lebih murah daripada satu juta, perusahaan akan melihat pada kesempatan itu."
            </p>
            <p>
            Prosedurnya sangat cepat dan mudah, seperti saat  vaksinasi.
            </p>
            <p>
            Untuk manajemen yang efektif dan praktis, sistem basis data terpusat perlu dibangun, katanya.
            </p>
            <p>
            Sumber : <a class="simple-link break-all" href="https://farmersweekly.co.nz/topic/solutions/view/introducing-rdg-technologies">https://farmersweekly.co.nz/topic/solutions/view/introducing-rdg-technologies</a>
            </p>
            <span class="post_date">24.02.2015</span>
            <span class="post_author"> <a href="#">admin</a></span>
            <span class="post_comments"><a href="#">0</a></span>
          </div>
          <a href="#" data-popup=".popup-social" class="button_full btmint open-popup">SHARE THIS POST</a>
          <div class="buttons-row">
            <a href="#tab1" class="tab-link active button">Leave a comment</a>
            <a href="#tab2" class="tab-link button">Comments</a>
          </div>
          <div class="tabs-animated-wrap">
            <div class="tabs">
              <div id="tab1" class="tab active">
                <div class="contactform">
                  <form id="CommentForm" method="post" action="">
                    <label>Name:</label>
                    <input type="text" name="CommentName" id="CommentName" value="" class="form_input" />
                    <label>Email:</label>
                    <input type="text" name="CommentEmail" id="CommentEmail" value="" class="form_input" />
                    <label>Comment:</label>
                    <textarea name="Comment" id="Comment" class="form_textarea" rows="" cols=""></textarea>
                    <input type="submit" name="submit" class="form_submit" id="submit" value="Submit" />
                  </form>
                </div>
              </div>
              <div id="tab2" class="tab">
                <ul class="comments">
                  <li class="comment_row">
                    <div class="comm_avatar"><img src="images/icons/black/user.png" alt="" title="" border="0" /></div>
                    <div class="comm_content">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam velit sapien, eleifend in by <a
                          href="#">John Doe</a></p>
                    </div>
                  </li>
                  <li class="comment_row">
                    <div class="comm_avatar"><img src="images/icons/black/user.png" alt="" title="" border="0" /></div>
                    <div class="comm_content">
                      <p>Consectetur adipiscing elit. Nam velit sapien, eleifend in by <a href="#">John Doe</a></p>
                    </div>
                  </li>
                  <li class="comment_row">
                    <div class="comm_avatar"><img src="images/icons/black/user.png" alt="" title="" border="0" /></div>
                    <div class="comm_content">
                      <p>Nam velit sapien, eleifend in by <a href="#">John Doe</a></p>
                    </div>
                  </li>
                  <div class="clear"></div>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php include 'layout/footer-bar.php' ?>
    </div>
  </div>