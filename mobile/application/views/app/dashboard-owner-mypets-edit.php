        <div class="pages">
          <div data-page="dashboard-owner-mypets-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-owner-mypets.php" class="backto"><img src="<?php echo base_url() ?>images/icons/black/back.png" alt="" title="" /></a>
                
                <h2 class="page_title">Edit Pet Data</h2>

                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error; ?></div>
                                <?php } ?>

                                <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success; ?></div>
                                <?php } ?>

                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <?php foreach ($datapet as $pet) {
                        $gender = $pet->gender;                       
                        $neutered = $pet->neutered;
                        $microchip = $pet->microchip; 
                        $idclinic = $pet->idclinic;
                     ?> 
                    <form class="form-horizontal" action="<?php echo base_url() ?>pet/editprocess" method="post" enctype="multipart/form-data">
                      <!-- <div class="buttons-row">
                          <a href="#tabeditpet-petinfo" class="tab-link active button">Pet Info</a>
                          <a href="#tabeditpet-ownerinfo" class="tab-link button">Owner Info</a>
                      </div> -->
                      <div class="tabs-simple">
                        <div class="tabs">
                          <div id="tabeditpet-petinfo" class="tab active">
                            <div class="form_row">
                              <label>Microchip ID:</label>
                              <input type="text" name="rfid" value="<?php echo $pet->rfid ?>" class="form_input"/>
                            </div>
                            <div class="form_row required">
                              <label>Pet Name:</label>
                              <input type="text" name="namepet" value="<?php echo $pet->namapet; ?>" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Type:</label>
                              <select name="type" class="form_select" name="type">
                                <option value="<?php echo $pet->tipe; ?>"><?php $tipe = $pet->tipe; echo $tipe;?></option>
                                <?php foreach ($pettype as $type) { ?>
                                   <option value="<?php echo $type->type ?>"><?php echo $type->type ?></option>
                                <?php } ?> 
                              </select>
                            </div>
                            <div class="form_row required">
                              <label>Breed:</label>
                              <input type="text" name="breed" value="<?php echo $pet->breed; ?>" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Colour:</label>
                              <input type="text" name="color" value="<?php echo $pet->color; ?>" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Gender:</label>
                              <div class="form_row_right">
                                <label class="label-radio item-content">
                                  <input type="radio" name="gender" value="male" <?php if ($gender == 'male') { echo 'checked'; } ?>>
                                  <div class="item-inner">
                                    <div class="item-title">Male</div>
                                  </div>
                                </label>
                                <label class="label-radio item-content">
                                  <input type="radio" name="gender" value="female" <?php if ($gender == 'female') { echo 'checked'; } ?>>
                                  <div class="item-inner">
                                    <div class="item-title">Female</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div class="form_row required">
                              <label>Neutered:</label>
                              <div class="form_row_right">
                                <label class="label-radio item-content">
                                  <input type="radio" name="neutered" value="Yes" <?php if ($neutered == '1') { echo 'checked'; } ?>>
                                  <div class="item-inner">
                                    <div class="item-title">Yes</div>
                                  </div>
                                </label>
                                <label class="label-radio item-content">
                                  <input type="radio" name="neutered" value="No" <?php if ($neutered == '0') { echo 'checked'; } ?>>
                                  <div class="item-inner">
                                    <div class="item-title">No</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div class="form_row required">
                              <label>Date of Birth:</label>
                              <input type="date" name="dateofbirth" value="<?php echo date('d/m/Y',strtotime($pet->datebirth)); ?>" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Display Information in Public Pet Database:</label>
                              <div class="form_row_right">
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic[]" value="petname" <?php if($pet->public_info[0]==1) echo "checked"; ?>>
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Pet's Name</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic[]" value="petid" <?php if($pet->public_info[1]==1)echo "checked"; ?>>
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Pet's ID</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic[]" value="petlastvaccine" <?php if($pet->public_info[2]==1)echo "checked"; ?>>
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Pet's Last Vaccine</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div class="form_row">
                              <?php
                                                $photo = $pet->photo;
                                                if($photo =='' ){
                                                    $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                                }else{
                                                    $petphoto = $photo;
                                                };
                                             ?>
                              <label>Pet Photo (max. size 5MB):</label>
                              <div class="preview-image"><img src="<?php echo $petphoto ?>" alt="" title="" /></div>
                              <input type="file" name="petphoto" value="" class="form_input" />
                              <input type="hidden" name="idpet" value="<?php $idpet = $pet->idpet; echo $idpet; ?>">
                              <input type="hidden" value="no" name="rotate" id="rotate" />
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Update Pet" />
                    </form>
                  <?php } ?>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>