        <div class="pages">
          <div data-page="dashboard-owner-mypets-diary-edit" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-owner-mypets-detail.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit Diary</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Date:</label>
                        <input type="date" name="dateofbirth" value="2019-08-21" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Pet:</label>
                        <select name="type" class="form_select">
                          <option value="chiky">Chiky</option>
                          <option value="pelong">Pelong</option>
                          <option value="jasmine">Jasmine</option>
                          <option value="archie">Archie</option>
                          <option value="bonnie">Bonnie</option>
                        </select>
                      </div>
                      <div class="form_row required">
                        <label>Diary Entry:</label>
                        <textarea name="diaryentry" class="form_textarea" rows="" cols="" >Tried a new food!</textarea>
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Update Diary" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>