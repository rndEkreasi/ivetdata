        <div class="pages">
          <div data-page="dashboard-owner-mypets-add" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="<?php echo base_url() ?>images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-owner-mypets.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Add Pet</h2>
                <?php if (isset($error)){ ?>
                  <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php } ?>  
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form class="form-horizontal" action="<?php echo base_url() ?>pet/addprocessbyowner" method="post" enctype="multipart/form-data">
                      <!-- <div class="buttons-row">
                          <a href="#tabeditpet-petinfo" class="tab-link active button">Pet Info</a>
                          <a href="#tabeditpet-ownerinfo" class="tab-link button">Owner Info</a>
                      </div> -->
                      <div class="tabs-simple">
                        <div class="tabs">
                          <div id="tabeditpet-petinfo" class="tab active">
                            <div class="form_row">
                              <label>Microchip ID:</label>
                              <input type="text" name="rfid" pattern=".{15,16}" value="<?php if (isset($rfid)){ echo $rfid; } ?>" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Pet Name:</label>
                              <input type="text" name="namepet" value="<?php if (isset($namepet)){ echo $namepet; } ?>" value="" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Type:</label>
                              <select name="type" class="form_select">
                                <?php if (isset($type)){ ?>
                                                    <option value="<?php echo $type ?>"><?php echo ucfirst($type) ?></option>
                                                <?php } ?>
                                                <option value="Etc">Select Pet Type</option>
                                                <?php foreach ($pettype as $type) { ?>
                                                    <option value="<?php echo $type->type ?>"><?php echo $type->type ?></option>
                                                <?php } ?>    
                              </select>
                            </div>
                            <div class="form_row required">
                              <label>Breed:</label>
                              <input type="text" name="breed" id="breed" value="<?php if (isset($breed)){ echo $breed; } ?>" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Colour:</label>
                              <input type="text" name="color" value="<?php if (isset($color)){ echo $color; } ?>" class="form_input" />
                            </div>
                            <div class="form_row required">
                              <label>Gender:</label>
                              <div class="form_row_right">
                                <label class="label-radio item-content">
                                  <input type="radio" name="gender" value="Male" checked>
                                  <div class="item-inner">
                                    <div class="item-title">Male</div>
                                  </div>
                                </label>
                                <label class="label-radio item-content">
                                  <input type="radio" name="gender" value="Female">
                                  <div class="item-inner">
                                    <div class="item-title">Female</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div class="form_row required">
                              <label>Neutered:</label>
                              <div class="form_row_right">
                                <label class="label-radio item-content">
                                  <input type="radio" name="neutered" value="Yes">
                                  <div class="item-inner">
                                    <div class="item-title">Yes</div>
                                  </div>
                                </label>
                                <label class="label-radio item-content">
                                  <input type="radio" name="neutered" value="No">
                                  <div class="item-inner">
                                    <div class="item-title">No</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div class="form_row required">
                              <label>Date of Birth:</label>
                              <input type="date" name="dateofbirth" value="" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Display Information in Public Pet Database:</label>
                              <div class="form_row_right">
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="petname">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Pet's Name</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="petid">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Pet's ID</div>
                                  </div>
                                </label>
                                <label class="label-checkbox item-content">
                                  <input type="checkbox" name="displaypublic" value="petlastvaccine">
                                  <div class="item-media">
                                    <i class="icon icon-form-checkbox"></i>
                                  </div>
                                  <div class="item-inner">
                                    <div class="item-title">Pet's Last Vaccine</div>
                                  </div>
                                </label>
                              </div>
                            </div>
                            <div class="form_row">
                              <label>Pet Photo (max. size 5MB):</label>
                              <div class="preview-image"></div>
                              <input type="file" name="petphoto" value="" class="form_input" />
                            </div>
                          </div>
                          <div id="tabeditpet-ownerinfo" class="tab">
                            <p>Please update your information from "Edit Profile" button on the navigation bar</p>
                            <div class="form_row">
                              <label>Customer Name:</label>
                              <input type="text" name="ownername" value="Wenna" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Phone:</label>
                              <input type="text" name="ownerphone" value="+628119722321" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Email:</label>
                              <input type="text" name="owneremail" value="wenna@ivetdata.com" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Address:</label>
                              <textarea name="owneraddress" class="form_textarea" rows="" cols="" disabled >Jl Cerita Indah 77</textarea>
                            </div>
                            <div class="form_row">
                              <label>City:</label>
                              <input type="text" name="ownercity" value="Jakarta" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Country:</label>
                              <select name="ownercountry" class="form_select" disabled>
                                <option value="indonesia">Indonesia</option>
                                <option value="singapore">Singapore</option>
                                <option value="japan">Japan</option>
                                <option value="skorea">South Korea</option>
                                <option value="vietnam">Vietnam</option>
                                <option value="philippines">Philippines</option>
                                <option value="mongolia">Mongolia</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Add Pet" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>