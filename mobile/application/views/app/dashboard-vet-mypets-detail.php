        <div class="pages">
          <div data-page="dashboard-vet-mypets-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-mypets.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <div class="custom-image"><img src="images/photos/photo1.jpg" alt="" title="" /></div>
                <h2 class="page_title">Chiky <a href="dashboard-vet-mypets-edit.php"><i class="fas fa-pen"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row">
                      <a href="#tabinfo-pet" class="tab-link active button">Pet Info</a>
                      <a href="#tabinfo-owner" class="tab-link button">Owner Info</a>
                      <a href="#tabinfo-clinic" class="tab-link button">Clinic Info</a>
                  </div>
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabinfo-pet" class="tab active">
                        <ul class="iconed-list">
                          <li><i class="fas fa-microchip mr-1"></i><b class="mr-2">Microchip ID:</b><a href="#" class="btn btn-sm btn-primary">Get Microchip</a></li>
                          <li><i class="fas fa-cat mr-1"></i><b class="mr-2">Type:</b><span>Dog</span></li>
                          <li><i class="fas fa-arrows-alt mr-1"></i><b class="mr-2">Breed:</b><span>Mixed Breed</span></li>
                          <li><i class="fas fa-palette mr-1"></i><b class="mr-2">Color:</b><span>Brown</span></li>
                          <li><i class="fas fa-birthday-cake mr-1"></i><b class="mr-2">Date of Birth:</b><span>2013-04-11</span></li>
                          <li><i class="fas fa-calendar-alt mr-1"></i><b class="mr-2">Age:</b><span>6 Years, 3 Months</span></li>
                          <li><i class="fas fa-map-marked-alt mr-1"></i><b class="mr-2">Country of Origin:</b><span>Indonesia</span></li>
                          <li><i class="fas fa-syringe mr-1"></i><b class="mr-2">Last Vaccination:</b><span>Parvovirus - 24 Apr, 2019</span></li>
                        </ul>
                      </div>
                      <div id="tabinfo-owner" class="tab">
                        <ul class="iconed-list">
                          <li><i class="fas fa-user mr-1"></i><b class="mr-2">Name:</b><span>Wenna</span></span></li>
                          <li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><a href="tel:+628119722321" class="simple-link">+628119722321</a></span></li>
                          <li><i class="fas fa-envelope mr-1"></i><b class="mr-2">Email:</b><span><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></span></li>
                          <li><i class="fas fa-map-marker-alt mr-1"></i><b class="mr-2">Address:</b><span>Jl. Laksamana Bintan, Sungai Panas, Kec. Bengkong, Kota Batam, Kepulauan Riau 29444</span></li>
                          <li><i class="fas fa-city mr-1"></i><b class="mr-2">City:</b><span>Batam</span></li>
                          <li><i class="fas fa-globe-asia mr-1"></i><b class="mr-2">Country:</b><span>Indonesia</span></li>
                        </ul>
                      </div>
                      <div id="tabinfo-clinic" class="tab">
                        <ul class="iconed-list">
                          <li><i class="fas fa-clinic-medical mr-1"></i><b class="mr-2">Clinic Name:</b><span>Tail Mail's Clinic</span></span></li>
                          <li><i class="fas fa-map-marker-alt mr-1"></i><b class="mr-2">Address:</b><span>Jln Kelapa Puan No 10</span></li>
                          <li><i class="fas fa-city mr-1"></i><b class="mr-2">City:</b><span>Batam</span></li>
                          <li><i class="fas fa-globe-asia mr-1"></i><b class="mr-2">Country:</b><span>Indonesia</span></li>
                          <li><i class="fas fa-envelope mr-1"></i><b class="mr-2">Email:</b><span><a href="mailto:contact@ivetdata.com" class="simple-link">contact@ivetdata.com</a></span></li>
                          <li><i class="fas fa-phone mr-1"></i><b class="mr-2">Phone:</b><span><a href="tel:+6281214939954" class="simple-link">+6281214939954</a></span></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="buttons-row">
                      <a href="#tabhistory-medical" class="tab-link active button">Medical History</a>
                      <a href="#tabhistory-vaccine" class="tab-link button">Vaccine History</a>
                  </div>
                  <div class="tabs-simple mb-5">
                    <div class="tabs">
                      <div id="tabhistory-medical" class="tab active">
                        <ul class="timeline-list">
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-vet-mypets-log-detail.php" class="simple-link">09 Jul, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Parvo</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-vet-mypets-log-detail.php" class="simple-link">30 May, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Parvovirus</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-vet-mypets-log-detail.php" class="simple-link">21 May, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Skabies</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-vet-mypets-log-detail.php" class="simple-link">17 Jul, 2018</a></div>
                            <div class="timeline-list-sub"><span>Register Date</span></div>
                            <p>Registered pet to iVetdata.com</p>
                          </li>
                        </ul>
                      </div>
                      <div id="tabhistory-vaccine" class="tab">
                        <ul class="timeline-list">
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-vet-mypets-vacc-detail.php" class="simple-link">24 Apr, 2019</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Parvovirus</p>
                          </li>
                          <li>
                            <div class="timeline-list-index"><a href="dashboard-vet-mypets-vacc-detail.php" class="simple-link">14 Feb, 2018</a></div>
                            <div class="timeline-list-sub">By: Drh. Tommy Soekiato</div>
                            <p>Rabies</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>