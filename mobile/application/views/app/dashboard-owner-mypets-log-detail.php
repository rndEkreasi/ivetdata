        <div class="pages">
          <div data-page="dashboard-owner-mypets-log-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="dashboard-owner-mypets-detail.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Chiky [09 Jul, 2019]</h2>
                <div class="page_single layout_fullwidth_padding">
                  <h3 class="page_subtitle">Medical History Detail</h3>
                  <table class="custom_table mb-3">
                    <thead>
                      <tr>
                        <th>DESCRIPTION</th>
                        <th>VALUE</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Date</td>
                        <td>09/07/2019</td>
                      </tr>
                      <tr>
                        <td>Vet</td>
                        <td>Drh. Tommy Soekiato</td>
                      </tr>
                      <tr>
                        <td>Diagnose</td>
                        <td>Clean</td>
                      </tr>
                      <tr>
                        <td>Treatment</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Anamnesis</td>
                        <td>Medical Check</td>
                      </tr>
                      <tr>
                        <td colspan="2"><strong>Basic Checkup</strong></td>
                      </tr>
                      <tr>
                        <td colspan="2"><strong>Physical Exam</strong></td>
                      </tr>
                      <tr>
                        <td>Eyes and Ears</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Skin and Coat</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Respiratory System</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Circulation System</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Digestive System</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Urinary System</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Nervous System</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Musculoskeletal System</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Other Notes</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td colspan="2"><strong>Diagnostics Tools Result</strong></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>