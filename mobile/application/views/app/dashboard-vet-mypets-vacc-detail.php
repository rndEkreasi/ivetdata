        <div class="pages">
          <div data-page="dashboard-vet-mypets-vacc-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Details -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-mypets-detail.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Chiky [24 Apr, 2019] <a href="dashboard-vet-mypets-vacc-edit.php"><i class="fas fa-pen"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <h3 class="page_subtitle">Vaccination History Detail</h3>
                  <table class="custom_table mb-3">
                    <thead>
                      <tr>
                        <th>DESCRIPTION</th>
                        <th>VALUE</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Date</td>
                        <td>24/04/2019</td>
                      </tr>
                      <tr>
                        <td>Vet</td>
                        <td>Drh. Tommy Soekiato</td>
                      </tr>
                      <tr>
                        <td>Vaccine</td>
                        <td>Cazitel Plus for Cat 4 kg</td>
                      </tr>
                      <tr>
                        <td>Injection Site</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Next Date</td>
                        <td>31/07/2020</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>