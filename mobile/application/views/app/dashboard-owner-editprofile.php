        <div class="pages">
          <div data-page="dashboard-owner-editprofile" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Edit My Profile</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <div class="buttons-row">
                        <a href="#tabeditowner-info" class="tab-link active button"><i class="fas fa-user mr-2"></i> My Profile</a>
                        <a href="#tabeditowner-password" class="tab-link button"><i class="fas fa-key mr-2"></i> Password</a>
                    </div>
                    <div class="tabs-simple">
                      <div class="tabs">
                        <div id="tabeditowner-info" class="tab active">
                          <form>
                            <div class="form_row">
                              <label>Profile Picture (max. size 5MB):</label>
                              <div class="preview-image"><img src="images/profiles/profile1.jpg" alt="" title="" /></div>
                              <input type="file" name="profilepicture" value="" class="form_input" />
                              <p>Image resolution size must be 300px x 300px</p>
                            </div>
                            <div class="form_row">
                              <label>Full Name:</label>
                              <input type="text" name="ownerfullname" value="Wenna Lojaya" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Phone Number:</label>
                              <input type="text" name="ownerphone" value="+628119722321" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Email Address:</label>
                              <input type="text" name="owneremail" value="wenna@ivetdata.com" class="form_input" disabled />
                            </div>
                            <div class="form_row">
                              <label>Address:</label>
                              <textarea name="owneraddress" class="form_textarea" rows="" cols="">Jl Cerita Indah 77</textarea>
                            </div>
                            <div class="form_row">
                              <label>City:</label>
                              <input type="text" name="ownercity" value="Jakarta" class="form_input" />
                            </div>
                            <div class="form_row">
                              <label>Country:</label>
                              <select name="ownercountry" class="form_select">
                                <option value="indonesia" selected>Indonesia</option>
                                <option value="singapore">Singapore</option>
                                <option value="japan">Japan</option>
                                <option value="skorea">South Korea</option>
                                <option value="vietnam">Vietnam</option>
                                <option value="philippines">Philippines</option>
                                <option value="mongolia">Mongolia</option>
                              </select>
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Profile" />
                          </form>
                        </div>
                        <div id="tabeditowner-password" class="tab">
                          <form>
                            <div class="form_row">
                              <label>Old Password:</label>
                              <div class="input-custom"><input type="password" name="Old Password" value="" class="form_input required" placeholder="Old Password" id="showoldpasswordinput" /><i class="iconbtn fas fa-eye" id="showoldpasswordtrigger" onClick="showPassword('showoldpasswordinput', 'showoldpasswordtrigger')"></i></div>
                            </div>
                            <div class="form_row">
                              <label>New Password:</label>
                              <div class="input-custom"><input type="password" name="New Password" value="" class="form_input required" placeholder="New Password" id="shownewpasswordinput" /><i class="iconbtn fas fa-eye" id="shownewpasswordtrigger" onClick="showPassword('shownewpasswordinput', 'shownewpasswordtrigger')"></i></div>
                            </div>
                            <div class="form_row">
                              <label>Repeat Password:</label>
                              <div class="input-custom"><input type="password" name="Repeat Password" value="" class="form_input required" placeholder="Repeat Password" id="showpasswordrepeatinput" /><i class="iconbtn fas fa-eye" id="showpasswordrepeattrigger" onClick="showPassword('showpasswordrepeatinput', 'showpasswordrepeattrigger')"></i></div>
                            </div>
                            <input type="submit" name="submit" class="form_submit" id="submit" value="Update Password" />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>