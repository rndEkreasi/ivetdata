        <div class="pages">
          <div data-page="dashboard-vet-mypets-add" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Edit -->
              <div id="pages_maincontent">
                <a href="dashboard-vet-invoice.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">Add Invoice</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="editform">
                    <form>
                      <div class="form_row required">
                        <label>Phone:</label>
                        <input type="text" name="phone" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Name:</label>
                        <input type="text" name="name" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Pet Name:</label>
                        <select name="petname" class="form_select">
                          <option value="chiky">Chiky</option>
                          <option value="killy">Killy</option>
                          <option value="kitty">Kitty</option>
                        </select>
                      </div>
                      <div class="form_row required">
                        <label>Email:</label>
                        <input type="text" name="email" value="" class="form_input" />
                      </div>
                      <div class="form_row required">
                        <label>Dokter:</label>
                        <select name="doctor" class="form_select">
                          <option value="30">Drh. Tommy Soekiato</option>
                          <option value="363">Drh Diana</option>
                          <option value="381">Drh Bambang</option>
                          <option value="387">Maroko</option>
                          <option value="553">Drh Marco</option>
                          <option value="563">Asep Rusmana</option>
                        </select>
                      </div>
                      <div class="form_row required list-block mb-5">
                        <label>Invoice Date:</label>
                        <input type="date" name="invoice_date" value="" class="form_input" />
                      </div>
                      <table class="custom_table input_table mb-3">
                        <thead>
                          <tr>
                            <th>ITEM</th>
                            <th>PRICE</th>
                            <th>QTY</th>
                            <th>TOTAL</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><input type="text" name="item1" /></td>
                            <td><input type="text" name="price1" /></td>
                            <td><input type="text" name="qty1" /></td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td colspan="4">
                              <a href="#" class="btn btn-sm btn-primary">Add Item</a>
                              <a href="dashboard-vet-services.php" class="btn btn-sm btn-secondary">List of Service &amp; Medicine</a>
                            </td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Sub Total</b></td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                          <tr>
                            <td>Total Quantity: 0 Units</td>
                            <td class="text-right" colspan="2"><b>Discount</b></td>
                            <td class="text-right" nowrap><input type="text" name="discount" /></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Tax (%)</b></td>
                            <td class="text-right" nowrap><input type="text" name="tax" placeholder="5-10" /> Rp. 0</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td class="text-right" colspan="2"><b>Grand Total</b></td>
                            <td class="text-right" nowrap>Rp. 0</td>
                          </tr>
                        </tbody>
                      </table>
                      <div class="form_row">
                        <label>Note:</label>
                        <textarea name="note" class="form_textarea" rows="" cols=""></textarea>
                      </div>
                      <input type="submit" name="submit" class="form_submit" id="submit" value="Create Invoice" />
                    </form>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>