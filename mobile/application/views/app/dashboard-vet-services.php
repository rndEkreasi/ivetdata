        <div class="pages">
          <div data-page="dashboard-vet-services" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-vet.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">SERVICES &amp; MEDICINES LIST <a href="dashboard-vet-services-add.php"><i class="fas fa-plus"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row">
                      <a href="#tabsvc-service" class="tab-link active button">Services</a>
                      <a href="#tabsvc-medicine" class="tab-link button">Medicines</a>
                  </div>
                  <div class="tabs-simple">
                    <div class="tabs">
                      <div id="tabsvc-service" class="tab active">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <form>
                              <input type="text" name="search" value="" placeholder="Search" />
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div>
                          <ul class="posts dvsitems">
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/fracturesurgery.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Fraktur Surgery</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 5,000,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/laparotomy.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Big Laparotomy Operation [11-25 kg]</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-green"><i class="fas fa-eye mr-1"></i> Published</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 3,250,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/laparotomy.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Big Laparotomy Operation [5-10 kg]</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-green"><i class="fas fa-eye mr-1"></i> Published</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 3,000,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/microchip.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Microchip</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 300,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/hematology.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Hematology</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 350,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/bloodtest.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Comprehensive blood test</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 1,000,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/laparotomy.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Consultation</h4>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 100,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-services-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-services-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                          </ul>
                          <div class="loadmore loadmore-dvsitems">LOAD MORE</div>
                          <div class="showless showless-dvsitems">END</div>
                        </div>
                      </div>
                      <div id="tabsvc-medicine" class="tab">
                        <div class="list-block">
                          <div class="searchbox mb-3">
                            <form>
                              <input type="text" name="search" value="" placeholder="Search" />
                              <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </form>
                          </div>
                          <ul class="posts dvmitems">
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/prednisolone.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Prednisolone</h4>
                                        <p class="pb-1">Stock: 10</p>
                                        <p class="pb-3">Unit: Gr</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-green"><i class="fas fa-eye mr-1"></i> Published</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 500,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/sponge.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Sponge</h4>
                                        <p class="pb-1">Stock: 12</p>
                                        <p class="pb-3">Unit: Gr</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-green"><i class="fas fa-eye mr-1"></i> Published</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 33,580</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/inpepsa.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Inpepsa</h4>
                                        <p class="pb-1">Stock: 0</p>
                                        <p class="pb-3">Unit: 100 mL</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-green"><i class="fas fa-eye mr-1"></i> Published</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 75,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/defensor3.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Defensor 3</h4>
                                        <p class="pb-1">Stock: 40</p>
                                        <p class="pb-3">Unit: -</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-green"><i class="fas fa-eye mr-1"></i> Published</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 70,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/vanguardplus.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Vanguard Plus 5/CV</h4>
                                        <p class="pb-1">Stock: 40</p>
                                        <p class="pb-3">Unit: -</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 250,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/metronidazole.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Metronidazole</h4>
                                        <p class="pb-1">Stock: 78</p>
                                        <p class="pb-3">Unit: -</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 20,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                            <li class="swipeout">
                              <div class="swipeout-content item-content">
                                <div class="post_entry post_entry_100">
                                  <div class="post_thumb"><img src="images/uploads/thumbs/vitaminb.jpg" alt="" title="" /></div>
                                  <div class="post_details">
                                    <div class="row">
                                      <div class="col col-50">
                                        <h4 class="pb-2">Vit B comp</h4>
                                        <p class="pb-1">Stock: 1</p>
                                        <p class="pb-3">Unit: -</p>
                                      </div>
                                      <div class="col col-50 text-right">
                                        <p class="pb-2"><span class="status status-lightgray"><i class="fas fa-lock mr-1"></i> Private</span></p>
                                        <p class="pb-3">Harga:<br/><b>Rp. 20,000</b></p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                                </div>
                              </div>
                              <div class="swipeout-actions-right">
                                <a href="dashboard-vet-medicines-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                                <a href="dashboard-vet-medicines-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </li>
                          </ul>
                          <div class="loadmore loadmore-dvmitems">LOAD MORE</div>
                          <div class="showless showless-dvmitems">END</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>