<div class="pages">
          <div data-page="dashboard-vet-team" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div class="custom-list custom-list-pet" id="pages_maincontent">
                <a href="dashboard-vet.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">MY TEAM</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="buttons-row row-clear mb-3">
                      <a href="dashboard-vet-team-vet-add.php" class="btn btn-sm btn-secondary mr-1 mb-1"><i class="fas fa-plus mr-1"></i>Add New Vet</a>
                      <a href="dashboard-vet-team-admin-add.php" class="btn btn-sm btn-primary mb-1"><i class="fas fa-plus mr-1"></i>Add New Admin Staff</a>
                  </div>
                  <div class="list-block">
                    <div class="searchbox mb-3">
                      <form>
                        <input type="text" name="search" value="" placeholder="Search" />
                        <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                      </form>
                    </div>
                    <ul class="posts dvtitems">
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4>Drh. Tommy Soekiato</h4>
                                  <p><a href="mailto:contact@ivetdata.com">contact@ivetdata.com</a></p>
                                  <p><a href="tel:+6281214939955">+6281214939955</a></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><b class="label label-lightgray">Clinic Owner</b></p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4>Diana</h4>
                                  <p><a href="mailto:diana@ivetdata.com">diana@ivetdata.com</a></p>
                                  <p><a href="tel:+62812345678">+62812345678</a></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><b class="label label-blue">Admin Staff</b></p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-team-admin-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-team-admin-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4>Mike Helde</h4>
                                  <p><a href="mailto:mike.helde@gmail.com">mike.helde@gmail.com</a></p>
                                  <p><a href="tel:+6281194155712">+6281194155712</a></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><b class="label label-green">Vet</b></p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-team-vet-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-team-vet-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4>Martha</h4>
                                  <p><a href="mailto:martha@ivetdata.com">martha@ivetdata.com</a></p>
                                  <p><a href="tel:+6287881144804">+6287881144804</a></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><b class="label label-green">Vet</b></p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-team-vet-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-team-vet-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4>Nana</h4>
                                  <p><a href="mailto:nana@ivetdata.com">nana@ivetdata.com</a></p>
                                  <p><a href="tel:+629813840304">+629813840304</a></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><b class="label label-green">Vet</b></p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-team-vet-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-team-vet-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                      <li class="swipeout">
                        <div class="swipeout-content item-content">
                          <div class="post_entry post_entry_full">
                            <div class="post_full">
                              <div class="row">
                                <div class="col col-60">
                                  <h4>Hida</h4>
                                  <p><a href="mailto:hida@yahoo.com">hida@yahoo.com</a></p>
                                  <p><a href="tel:+628677543356">+628677543356</a></p>
                                </div>
                                <div class="col col-40 text-right">
                                  <p><b class="label label-blue">Admin Staff</b></p>
                                </div>
                              </div>
                            </div>
                            <div class="post_swipe"><img src="images/swipe_more.png" alt="" title="" /></div>
                          </div>
                        </div>
                        <div class="swipeout-actions-right">
                          <a href="dashboard-vet-team-admin-edit.php" class="action1"><i class="fas fa-pen"></i></a>
                          <a href="dashboard-vet-team-admin-delete.php" class="action1"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </li>
                    </ul>
                    <div class="loadmore loadmore-dvtitems">LOAD MORE</div>
                    <div class="showless showless-dvtitems">END</div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>