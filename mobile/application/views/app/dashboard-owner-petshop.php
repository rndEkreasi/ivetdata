        <div class="pages">
          <div data-page="dashboard-owner-petshop" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">PET SHOP <a href="dashboard-owner-cart.php"><i class="fas fa-shopping-cart"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="searchbox mb-3">
                    <form>
                      <input type="text" name="search" value="" placeholder="Search" />
                      <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </form>
                  </div>
                  <div class="custom-accordion table-accordion mb-4">
                    <div class="accordion-item">
                      <div class="accordion-item-toggle">
                        <i class="icon icon-plus">+</i>
                        <i class="icon icon-minus">-</i>
                        <span>Choose Category</span>
                      </div>
                      <div class="accordion-item-content bgcolor-lightgrey">
                        <div class="main-nav category-filters icons_31">
                          <ul>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/catsupplies.png"/></div>
                                <span>Cat Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/dogsupplies.png"/></div>
                                <span>Dog Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/birdsupplies.png"/></div>
                                <span>Bird Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/pondarrangement.png"/></div>
                                <span>Pond Arrangement</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/aquariumsupplies.png"/></div>
                                <span>Aquarium Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/reptilesupplies.png"/></div>
                                <span>Reptile Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/rodentsupplies.png"/></div>
                                <span>Rodent Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/exoticpetsupplies.png"/></div>
                                <span>Exotic Pet Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/horsesupplies.png"/></div>
                                <span>Horse Supplies</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/pestcontrol.png"/></div>
                                <span>Pest Control</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/topbrands.png"/></div>
                                <span>Top Brands</span>
                              </a>
                            <li>
                              <a href="dashboard-owner-petshop.php">
                                <div class="category_icon mb-1"><img src="images/icons/categories/sale.png"/></div>
                                <span>Sale</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="shop_items">
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petshop-detail.php"><img src="images/uploads/thumbs/inpepsa.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petshop-detail.php">Inpepsa</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 75,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petshop-detail.php"><img src="images/uploads/thumbs/prednisolone.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petshop-detail.php">Prednisolone</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 500,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity2" />
                            <input type="text" name="quantity2" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity2" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petshop-detail.php"><img src="images/uploads/thumbs/sponge.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petshop-detail.php">Sponge</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 33,580</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity3" />
                            <input type="text" name="quantity3" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity3" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petshop-detail.php"><img src="images/uploads/thumbs/defensor3.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petshop-detail.php">Defensor 3</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 70,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity4" />
                            <input type="text" name="quantity4" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity4" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petshop-detail.php"><img src="images/uploads/thumbs/vanguardplus.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petshop-detail.php">Vanguard Plus 5/CV</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 250,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity5" />
                            <input type="text" name="quantity5" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity5" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                  </ul>
                  <div class="shop_pagination">
                    <a href="shop.html" class="prev_shop">PREV PAGE</a>
                    <span class="shop_pagenr">1/37</span>
                    <a href="shop-page2.html" class="next_shop">NEXT PAGE</a>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>