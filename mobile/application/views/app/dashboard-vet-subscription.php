        <div class="pages">
          <div data-page="dashboard-vet-subscription" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Subscription -->
              <div id="pages_maincontent">
                <h2 class="page_title">Choose Your Monthly Subscription</h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="cards">
                    <div class="card mb-4">
                      <div class="cardhead mb-3">
                        <div class="cardsub">BASIC</div>
                        <div class="cardtitle">Rp. 250,000 <span class="cardtitlesml">/ 1 month</span></div>
                      </div>
                      <div class="cardbody px-1 mb-2">
                        <ul class="checkmarklist mb-3">
                          <li>Pet Record</li>
                          <li>Owner Record</li>
                          <li>Stock &amp; Inventory</li>
                          <li>Invoicing</li>
                          <li>Public Vet Search</li>
                          <li>24hr Customer Support</li>
                        </ul>
                      </div>
                      <div class="cardfooter">
                        <a class="btn btn-secondary" href="dashboard-vet-subscription-detail.php">Buy</a>
                      </div>
                    </div>
                    <div class="card mb-4">
                      <div class="cardhead mb-3">
                        <div class="cardtitle">PREMIUM</div>
                      </div>
                      <div class="cardbody px-1 mb-2">
                        <ul class="checkmarklist mb-3">
                          <li>Pet Record</li>
                          <li>Owner Record</li>
                          <li>Stock &amp; Inventory</li>
                          <li>Invoicing</li>
                          <li>Public Vet Search</li>
                          <li>24hr Customer Support</li>
                          <li>Basic Accounting</li>
                          <li>Payment Gateway (Bank Transfer, Credit Card, eMoney)</li>
                          <li>Bulk Upload</li>
                        </ul>
                      </div>
                      <div class="cardfooter">
                        <a class="btn btn-primary" disabled>Coming Soon</a>
                      </div>
                    </div>
                    <div class="card mb-4">
                      <div class="cardhead mb-3">
                        <div class="cardtitle">CORPORATE</div>
                      </div>
                      <div class="cardbody px-1 mb-2">
                        <ul class="checkmarklist mb-3">
                          <li>Pet Record</li>
                          <li>Owner Record</li>
                          <li>Stock &amp; Inventory</li>
                          <li>Invoicing</li>
                          <li>Public Vet Search</li>
                          <li>24hr Customer Support</li>
                          <li>Basic Accounting</li>
                          <li>Payment Gateway (Bank Transfer, Credit Card, eMoney)</li>
                          <li>Bulk Upload</li>
                          <li>Offline Server Integration</li>
                          <li>Excel Spreadsheet Export</li>
                        </ul>
                      </div>
                      <div class="cardfooter">
                        <a class="btn btn-primary" disabled>Coming Soon</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-vet.php' ?>
            </div>
          </div>
        </div>