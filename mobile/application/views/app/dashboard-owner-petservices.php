        <div class="pages">
          <div data-page="dashboard-owner-petservices" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- List -->
              <div id="pages_maincontent">
                <a href="dashboard-owner.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title">PET SERVICES <a href="dashboard-owner-cart.php"><i class="fas fa-shopping-cart"></i></a></h2>
                <div class="page_single layout_fullwidth_padding">
                  <div class="searchbox mb-3">
                    <form>
                      <input type="text" name="search" value="" placeholder="Search" />
                      <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                    </form>
                  </div>
                  <div class="custom-accordion table-accordion mb-4">
                    <div class="accordion-item">
                      <div class="accordion-item-toggle">
                        <i class="icon icon-plus">+</i>
                        <i class="icon icon-minus">-</i>
                        <span>Choose Category</span>
                      </div>
                      <div class="accordion-item-content bgcolor-lightgrey">
                        <div class="main-nav category-filters icons_31">
                          <ul>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/examination.png"/></div>
                                <span>Examination</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/vaccination.png"/></div>
                                <span>Vaccination</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/spayingandcastrating.png"/></div>
                                <span>Spaying &amp; Castrating</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/functionaldiagnostics.png"/></div>
                                <span>Functional Diagnostics</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/dentistry.png"/></div>
                                <span>Dentistry</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/ophthalmology.png"/></div>
                                <span>Ophthalmology</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/surgery.png"/></div>
                                <span>Surgery</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/dayandnightclinic.png"/></div>
                                <span>Day &amp; Night Clinic</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/dermatology.png"/></div>
                                <span>Dermatology</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/nailbeakandteethtrimming.png"/></div>
                                <span>Nail, Beak &amp; Teeth Trimming</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/pharmacy.png"/></div>
                                <span>Pharmacy</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/microchipping.png"/></div>
                                <span>Microchipping</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/laborinduction.png"/></div>
                                <span>Labor Induction</span>
                              </a>
                            </li>
                            <li>
                              <a href="dashboard-owner-vetsearch.php">
                                <div class="category_icon mb-1"><img src="images/icons/specialties/housecalls.png"/></div>
                                <span>House Calls</span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="shop_items">
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/fracturesurgery.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Fraktur Surgery</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 5,000,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/laparotomy.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Big Laparotomy Operation [11-25 kg]</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 3,250,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/laparotomy.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Big Laparotomy Operation [5-10 kg]</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 3,000,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/microchip.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Microchip</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 300,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/hematology.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Hematology</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 350,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/bloodtest.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Comprehensive blood test</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 1,000,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                    <li>
                      <div class="shop_thumb"><a href="dashboard-owner-petservices-detail.php"><img src="images/uploads/thumbs/laparotomy.jpg" alt="" title="" /></a></div>
                      <div class="shop_item_details shop_with_favbtn">
                        <h4><a href="dashboard-owner-petservices-detail.php">Consultation</a></h4>
                        <div class="mb-2">by: Tail Mail's Clinic</div>
                        <div class="shop_item_price mb-2">Rp. 100,000</div>
                        <div class="item_qnty_shop">
                          <form id="myform" method="POST" action="#">
                            <input type="button" value="-" class="qntyminusshop" field="quantity1" />
                            <input type="text" name="quantity1" value="1" class="qntyshop" />
                            <input type="button" value="+" class="qntyplusshop" field="quantity1" />
                          </form>
                        </div>
                        <a href="dashboard-owner-cart.php" id="addtocart">ADD TO CART</a>
                        <a href="#" data-popup=".popup-social" class="open-popup shopfav"><img src="images/icons/black/love.png" alt="" title="" /></a>
                      </div>
                    </li>
                  </ul>
                  <div class="shop_pagination">
                    <a href="shop.html" class="prev_shop">PREV PAGE</a>
                    <span class="shop_pagenr">1/37</span>
                    <a href="shop-page2.html" class="next_shop">NEXT PAGE</a>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>