        <div class="pages">
          <div data-page="home" class="page no-toolbar no-navbar">
            <div class="page-content page-smallfooter">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- FullBG -->
              <div class="fullbg" style="background-image:url(images/slides/slide-1.jpg);">
                <div class="fullbg-cover">
                  <div class="fullbg-content">
                    <div class="tabs-custom">
                      <div class="buttons-row">
                        <a href="#tab-microchip" class="tab-link active button mr-1"><i class="fas fa-paw mr-1"></i>Microchip</a>
                        <a href="#tab-vetsearch" class="tab-link button"><i class="fas fa-user-friends mr-1"></i>Vets</a>
                      </div>
                      <div class="tabs-simple mb-3">
                        <div class="tabs">
                          <div id="tab-microchip" class="tab active">
                            <form class="search-form">
                                <div class="form-row mb-3">
                                  <input type="text" name="name" value="" class="form-input" placeholder="Microchip ID" />
                                  <button class="btn btn-primaryred">Search</i></button>
                                </div>
                                <div class="form-row">
                                  <p>Enter the microchip ID above and click search. Enter only the 9, 10 or 15 character microchip number, with no punctuation or spaces.</p>
                                </div>
                            </form>
                          </div>
                          <div id="tab-vetsearch" class="tab">
                            <form class="search-form">
                              <div class="form-row mb-3">
                                <input type="text" name="name" value="" class="form-input" placeholder="Vet Name" />
                                <input type="text" name="name" value="" class="form-input" placeholder="City" />
                                <button class="btn btn-primaryred">Find</button>
                              </div>
                              <div class="form-row">
                                <p>Enter the name of the vet or the city that you want to search.</p>
                              </div>
                            </form>
                          </div> 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col btns-row">
                          <a data-popup=".popup-signup" class="open-popup close-panel btn btn-secondary btn-lg">REGISTER</a>
                          <a data-popup=".popup-login" class="open-popup close-panel btn btn-primary btn-lg">LOGIN</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar.php' ?>
            </div>
          </div>
        </div>