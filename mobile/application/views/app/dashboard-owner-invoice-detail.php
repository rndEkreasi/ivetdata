        <div class="pages">
          <div data-page="dashboard-owner-invoice-detail" class="page no-toolbar no-navbar page-bged">
            <div class="page-content">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- Detail -->
              <div class="invoice-detail" id="pages_maincontent">
                <a href="dashboard-owner-invoice.php" class="backto"><img src="images/icons/black/back.png" alt="" title="" /></a>
                <h2 class="page_title pt-3">Invoice: #1662931295</h2>
                <div class="invoice">
                  <div class="invoice_info mb-4">
                    <div class="invoice_info_top mb-3"><span>Rp. 350,000</span></div>
                    <div class="invoice_info_left">
                      <div class="invoice_info_name">Invoice from:</div>
                      <div class="invoice_from">
                        <div class="image"><img src="images/clinics/1.png" /></div>
                        <span>Tail Mail's Clinic</span>
                      </div>
                    </div>
                    <div class="invoice_info_right">
                      <div class="invoice_info_name">Invoice to:</div>
                      <div class="invoice_to">
                        <div><b>Name:</b><span>Wenna</span></div>
                        <div><b>Phone:</b><span><a href="tel:+628119722321" class="simple-link">+628119722321</a></span></div>
                        <div><b>Email:</b><span><a href="mailto:wenna@ivetdata.com" class="simple-link">wenna@ivetdata.com</a></span></div>
                        <div><b>Date:</b><span>12 Jul 2019</span></div>
                        <div><b>Pet Name:</b><span>Jasmine</span></div>
                      </div>
                    </div>
                  </div>
                  <div class="page_single layout_fullwidth_padding">
                    <div class="mb-3">Payment Method: <span class="label label-green">Cash</span></div class="mb-3">
                    <table class="custom_table mb-3">
                      <thead>
                        <tr>
                          <th>ITEM</th>
                          <th>PRICE</th>
                          <th>QTY</th>
                          <th>TOTAL</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Ultrasound</td>
                          <td class="text-right" nowrap><b>Rp. 200,000</b></td>
                          <td>1</td>
                          <td class="text-right" nowrap>Rp. 200,000</td>
                        </tr>
                        <tr>
                          <td>Infus NaCl, asering, glukosa</td>
                          <td class="text-right" nowrap><b>Rp. 50,000</b></td>
                          <td>1</td>
                          <td class="text-right" nowrap>Rp. 50,000</td>
                        </tr>
                        <tr>
                          <td>Consultation</td>
                          <td class="text-right" nowrap><b>Rp. 100,000</b></td>
                          <td>1</td>
                          <td class="text-right" nowrap>Rp. 100,000</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Sub Total</b></td>
                          <td class="text-right" nowrap>Rp. 350,000</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Discount</b></td>
                          <td class="text-right" nowrap>Rp. 0</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Tax</b></td>
                          <td class="text-right" nowrap>Rp. 0</td>
                        </tr>
                        <tr>
                          <td></td>
                          <td class="text-right" colspan="2"><b>Grand Total</b></td>
                          <td class="text-right" nowrap>Rp. 350,000</td>
                        </tr>
                      </tbody>
                    </table>
                    <p>Note*: </p>
                  </div>
                </div>
              </div>
              <?php include 'layout/footer-bar-petowner.php' ?>
            </div>
          </div>
        </div>