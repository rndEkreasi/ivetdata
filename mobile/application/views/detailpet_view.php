<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">        
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb darklinks">
                        <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                        <li class="active">Pet Detail</li>
                    </ol>
                    </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
        </section> 

        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                            <h3><?php echo $namepet ?> - 
                                <small>Microchip ID : <?php
                                if($rfid == '0'){
                                    echo 'Buy Microchip';
                                }else{ 
                                 echo $rfid ; } ?></small>
                            </h3>
                            <!-- <a href="<?php echo base_url() ?>pet/addhistory/?id=<?php echo $idpet ?>" class="icon-tab theme_button color3" data-toggle="modal" data-target="#exampleModal"> + Medical History</a> -->
                            <?php if ($role == '0'){ ?>
                                <a href="<?php echo base_url() ?>pet/edit/?id=<?php echo $idpet ?>" class="icon-tab theme_button color2"></i>Edit</a>
                            <?php }else{ ?>
                                <?php if($manageto < '3'){?>
                                    <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#addmedical"> + Medical History</button>
                                    <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#addvaccine"> + Vaccine History</button>
                                    <!-- <a href="<?php echo base_url() ?>pet/transfer/?id=<?php echo $idpet ?>" class="icon-tab theme_button color1"></i>Transfer</a> -->
                                <?php } ?>
                                <?php if($edit == '1'){ 
                                    if($manageto < '3'){?>
                                    <a href="<?php echo base_url() ?>pet/edit/?id=<?php echo $idpet ?>" class="icon-tab theme_button color2"></i>Edit</a>
                                <?php } 
                            }?>                            
                                <a href="<?php echo base_url() ?>sales/invoice/?idowner=<?php echo $idowner ?>&idpet=<?php echo $idpet ?>" class="icon-tab theme_button color3"></i>Add Bill</a>
                            <?php } ?>
                            
                        </div>
                    </div>
                    <!-- .row -->


                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <!-- User Statistics -->
                                <div class="col-xs-12 col-md-4">
                                    <div class="with_border with_padding">
                                        <img src="<?php echo $petphoto ?>" style="width: 100%;">
                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- col-* -->
                                <!-- User Info -->
                                <div class="col-xs-12 col-md-8">
                                    <div class="with_border with_padding">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <h3>Pet Info</h3>

                                                <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-paw"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey"> Microchip ID : </strong><?php
                                if($rfid == '0'){
                                    echo 'Buy Microchip';
                                }else{ 
                                 echo $rfid ; } ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-flag"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name : </strong> <?php echo $namepet; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="fa fa-briefcase"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Type : </strong> <?php echo $tipe; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="fa fa-arrows-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Gender : </strong> <?php echo $gender; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="fa fa-arrows-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Breed : </strong> <?php echo $breed; ?>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-paint-brush"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Color : </strong> <?php echo $color; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-signal"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Date of Birth : </strong> <?php echo $datebirth; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-signal"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Age : </strong> <?php echo $age; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="fa fa-map"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Country Origin : </strong> <?php echo $country; ?>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-medkit"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Last Vaccination : </strong><br>
                                                                <?php if (count($lastvaccine) == '0'){
                                                                    echo '-';
                                                                }else{ ?>
                                                                     <?php echo $lastvaccine[0]->vaccine; ?> - <?php echo date('d M, Y',strtotime($lastvaccine[0]->datevacc)) ?>
                                                                <?php } ?>
                                                               <br>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <?php // if($edit == '1'){ ?>
                                                <h3>Owner Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <?php // foreach ($dataowner as $owner) { ?>                                                       
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name : </strong> <?php echo $datapet[0]->namapemilik ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-phone"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Phone : </strong> <?php echo $datapet[0]->nohp ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="rt-icon2-mail"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Email : </strong> <?php echo $datapet[0]->email ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="rt-icon2-pin-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Address : </strong> <?php echo $datapet[0]->address ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">City : </strong> <?php echo $datapet[0]->city ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Country : </strong> <?php echo $datapet[0]->country ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php // } ?>                                                    
                                                </ul>
                                            <?php // }else{ ?>
                                                <h3>Clinic Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">  
                                                            <div class="media-body media-middle">
                                                                <div class="teaser_icon label-info fontsize_16"><i class="rt-icon2-health"></i></div>                                           
                                                            </div>                                                          
                                                            <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                                                <strong class="grey">Clinic name : </strong> <?php echo $namaclinic; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-body media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16"><i class="rt-icon2-pin-alt"></i></div>                                           
                                                            </div>
                                                            <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                                                <strong class="grey">Address : </strong> <?php echo $addressclinic; ?> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">City : </strong> <?php echo $cityclinic; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Country : </strong> <?php echo $countryclinic; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-body media-middle">
                                                                <div class="teaser_icon label-success fontsize_16"><i class="rt-icon2-mail"></i></div>                                           
                                                            </div>
                                                            <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                                                <strong class="grey">Email : </strong> <a href="mailto:<?php echo $emailclinic; ?>"> <?php echo $emailclinic; ?></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-body media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16"><i class="rt-icon2-phone"></i></div>                                           
                                                            </div>
                                                            <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                                                <strong class="grey">Phone : </strong> <a href="tel:<?php echo $phoneclinic; ?>"> <?php echo $phoneclinic; ?></a>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                            <?php // } ?>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- col-* -->
                            </div>
                            <!-- .row -->


                            <div class="row">

                                <!-- Latest Comments -->
                                <?php // if(count($logvaccine)>0){ ?>
                                <div class="col-xs-12 col-md-6">
                                <?php // }else{ ?>
                                <!--<div class="col-xs-12 col-md-12">-->
                                <?php //} ?>
                                    <div class="with_border with_padding">
                                        <h4>
                                             Medical History
                                        </h4>
                                        <div class="container mt-5 mb-5">
                                            <div class="row">
                                                    <ul class="timeline">
                                                        <?php foreach ($logpet as $log) {
                                                            $idcliniclog = $log->idclinic;

                                                            //var_dump($idvet);
                                                         ?>
                                                        <li>
                                                            <?php if ($myclinic != $idcliniclog){ ?>
                                                                <?php if ($role == '0'){
                                                                    if($email == $emailowner){ ?>
                                                                       <a href="<?php echo base_url() ?>pet/logdetail/?idlog=<?php echo $log->idlog; ?>&idpet=<?php echo $log->idpet  ?>">By : <?php echo $log->namevets; ?></a>
                                                                        <a href="<?php echo base_url() ?>pet/logdetail/?idlog=<?php echo $log->idlog;?>&idpet=<?php echo $log->idpet  ?>" class="float-right" style="float: right;"><?php echo date('d M, Y',strtotime($log->tgllog,strtotime('+7 hour'))) ?></a>
                                                                   <?php }
                                                                }else{ ?>
                                                                <a href="#" style="color: grey;">By : <?php echo $log->namevets; ?></a>
                                                                <a href="#" class="float-right" style="float: right;color: grey;"><?php echo date('d M, Y ',strtotime($log->tgllog,strtotime('+7 hour'))) ?></a>
                                                                <?php } ?>
                                                                
                                                            <?php }else{ ?>                                                                
                                                                <a href="<?php echo base_url() ?>pet/logdetail/?idlog=<?php echo $log->idlog; ?>&idpet=<?php echo $log->idpet  ?>">By : <?php echo $log->namevets; ?></a>
                                                                <a href="<?php echo base_url() ?>pet/logdetail/?idlog=<?php echo $log->idlog;?>&idpet=<?php echo $log->idpet  ?>" class="float-right" style="float: right;"><?php echo date('d M, Y',strtotime($log->tgllog,strtotime('+7 hour'))) ?></a>
                                                            <?php } ?>
                                                            
                                                            <p><?php
                                                            if($log->diagnose == ''){
                                                                echo $log->anamnesis;
                                                            }else{
                                                                echo $log->diagnose;
                                                            }

                                                        ?></p>
                                                        </li>
                                                        <?php } ?>
                                                        <li>
                                                            <a href="#">Register Date</a>
                                                            <a href="#" class="float-right" style="float: right;"><?php echo date('d M, Y',strtotime($dateadd)) ?></a>
                                                            <p>Registered pet to iVetdata.com</p>
                                                        </li>
                                                    </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- .col-* -->
                            <?php // if(count($logvaccine)>0){ ?>
                                <!-- Latest Comments -->
                                <div class="col-xs-12 col-md-6">
                                    <div class="with_border with_padding">
                                        <h4>
                                            Vaccine History
                                        </h4>
                                        <div class="container mt-5 mb-5">
                                            <div class="row">
                                                    <ul class="timeline">                                                        
                                                        <?php foreach ($logvaccine as $vacc) {
                                                                $idvet = $vacc->idvet;
                                                                $idvacc = $vacc->idvacc;
                                                                if($profileid != $idvet){
                                                                    $edit = 'readonly';                                                                    
                                                                    $editbutton = '';
                                                                    $deletebutton = '';
                                                                    $action = '#';
                                                                }else{
                                                                    $edit = '';
                                                                    $editbutton = '<input type="submit"  class="icon-tab theme_button color3" value="Update">';
                                                                    $deletebutton = '<a href="'.base_url().'pet/deletevaccine/?idvacc='.$idvacc.'&idpet='.$rfid.'" class="icon-tab theme_button color4">Delete</a>';
                                                                    $action = base_url().'pet/updatevaccine';
                                                                }
                                                        ?>
                                                        <li>
                                                            <!-- <a href="#" data-toggle="modal" data-target="#detailvacc<?php echo $vacc->idvacc; ?>">By : <?php echo $vacc->vetname; ?></a>
                                                            <a href="#" data-toggle="modal" data-target="#detailvacc<?php echo $vacc->idvacc; ?>" class="float-right" style="float: right;"><?php $tglvacc = date('d M, Y',strtotime($vacc->datevacc)); echo $tglvacc ;?></a> -->
                                                            <a href="/pet/vaccdetail/?idvacc=<?php echo $vacc->idvacc; ?>&idpet=<?php echo $_GET['idpet'] ?>&idvet=<?php echo $vacc->idvet; ?>" >By : <?php echo $vacc->vetname; ?></a>
                                                            <a href="/pet/vaccdetail/?idvacc=<?php echo $vacc->idvacc; ?>&idpet=<?php echo $_GET['idpet']?>&idvet=<?php echo $vacc->idvet; ?>" class="float-right" style="float: right;"><?php $tglvacc = date('d M, Y',strtotime($vacc->datevacc)); echo $tglvacc ;?></a>
                                                            <p><?php echo $vacc->vaccine; ?></p>
                                                            <div class="modal fade" id="detailvacc<?php echo $vacc->idvacc; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                              <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Detail Vaccine </h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                      <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                  </div>

                                                                    <div class="modal-body">
                                                                        <form action="<?php echo $action ?>" method="post">
                                                                        <nav class="mainmenu_side_wrapper medrecform">
                                                                            <ul class="menu-click">
                                                                                <li class="active-submenu"><a href="#" class="parent">Date Add</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <div class="form-group">
                                                                                                <div class="input-group date" data-provide="datepicker">
                                                                                                <input type="text" name="datevacc" <?php echo $edit; ?> value="<?php echo date('m/d/Y',strtotime($vacc->datevacc)) ?>" readonly class="js-date form-control" required>
                                                                                                            <div class="input-group-addon">
                                                                                                                <span class="glyphicon glyphicon-th"></span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="active-submenu"><a href="#" class="parent">Vaccine</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <div class="form-group">
                                                                                                <textarea class="form-control" <?php echo $edit; ?> id="vaccine" name="vaccine" required><?php echo $vacc->vaccine?>
                                                                                                <?php $value = explode("<br>",$log->treatment);foreach($value as $val){ echo $val."\n"; } ?>
                                                                                                </textarea>
                                                                                                <input type="hidden"  name="idvacc" value="<?php echo $vacc->idvacc?>">
                                                                                                <input type="hidden" name="rfid" value="<?php echo $rfid; ?> ">
                                                                                                <input type="hidden" name="idpet" value="<?php echo $vacc->idpet; ?> ">
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="active-submenu"><a href="#" class="parent">Description</a>
                                                                                    <ul>
                                                                                        <li>
                                                                                            <div class="form-group">
                                                                                                <textarea class="form-control" <?php echo $edit; ?> id="decription" name="description"><?php echo $vacc->description?></textarea>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="active-submenu"><a href="#" class="parent">Next Schedule</a>
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="form-group">
                                                                                            <div class="input-group date" data-provide="datepicker">
                                                                                                <?php 
                                                                                                $nextdate = $vacc->nextdate;                                                                                                
                                                                                                if($nextdate == '0000-00-00 00:00:00'){
                                                                                                    $fixdate = date('Y-m-d H:i:s');
                                                                                                }else{
                                                                                                    $fixdate = $nextdate;
                                                                                                }
                                                                                                 ?>
                                                                                                        <input type="text" name="nextdate" value="<?php echo date('m/d/Y', strtotime($fixdate)) ?>" class="js-date form-control" required>
                                                                                                        <div class="input-group-addon">
                                                                                                            <span class="glyphicon glyphicon-th"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                        </div>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                            </ul>
                                                                        </nav>
                                                                    </div>
                                                                  <div class="modal-footer">
                                                                    <!-- <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CLOSE</button> -->
                                                                    <?php echo $deletebutton ;?> <?php echo $editbutton ;?>                                                     
                                                                  </div>
                                                                  </form>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </li>
                                                        <?php } ?>
                                                    </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- .col-* -->
                            <?php // }  ?>
                                
                                <!-- .col-* -->

                            </div>
                            <!-- .row -->
                        </div>
                        <!-- .col-* left column -->

                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>  
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addvaccine" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Vaccine History</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url() ?>pet/addvaccine" method="post" enctype="multipart/form-data">
        <div class="modal-body">
            <nav class="mainmenu_side_wrapper medrecform">
                <ul class="menu-click">
                    <li class="active-submenu"><a href="#" class="parent">Date Add</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" name="datevacc" value="<?php echo date('m/d/Y') ?>" class="js-date form-control" required>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="active-submenu"><a href="#" class="parent">Vaccine*</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <table><tr class="item-row2">
                                        <td class="item-name" width="80%">
                                            <div class="delete-btn">
                                                <input type="text" class="chooseservice form-control item ui-autocomplete-input" placeholder="Vaccine" name="vaccine[]" autocomplete="off">
                                            </div>
                                            <input class="form-control qty" name="qty[]" placeholder="Quantity" type="hidden" required>
                                            <input class="form-control price" name="price[]" placeholder="Price" type="hidden" required></td>
                                        <td class="item-name" width="20%"><a id="addRow2" href="javascript:;" title="Add Item" class="icon-tab theme_button color2" style="color: #fff;">Add</a></td>
                                    </tr></table>
                                    <!--<textarea class="form-control" id="vaccine" name="vaccine" required></textarea>-->
                                    <input type="hidden" name="idpet" value="<?php echo $idpet ?>">
                                    <input type="hidden" name="rfid" value="<?php echo $rfid ?>">
                                    <input type="hidden" name="idowner" value="<?php echo $datapet[0]->idowner ?>">
                                    <input type="hidden" name="nameowner" value="<?php echo $datapet[0]->namapemilik ?>">
                                    <input type="hidden" name="emailowner" value="<?php echo $datapet[0]->email ?>">
                                    <input type="hidden" name="phoneowner" value="<?php echo $datapet[0]->nohp ?>">
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="active-submenu"><a href="#" class="parent">Description*</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <textarea class="form-control" id="decription" name="description"></textarea>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="active-submenu"><a href="#" class="parent">Next Schedule</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                                <input type="text" name="nextdate" value="<?php // echo date('m/d/Y', strtotime('+1 month')) ?>" class="js-date form-control" required>
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            * Mandatory, must be filled. 
        </div>
        <div class="modal-footer">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            <button type="submit" class="btn btn-primary">SAVE</button> -->
            <button type="button" class="icon-tab theme_button color3" data-dismiss="modal">CANCEL</button>
            <button type="submit" class="icon-tab theme_button color2">SAVE</button>
            
          </div>
    </form>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addmedical" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Medical History</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form action="<?php echo base_url() ?>pet/addmedical" method="post" enctype="multipart/form-data">
      <div class="modal-body">       
            <!-- <div class="form-group">
                <label for="exampleInputEmail1">Title :</label>
                <input type="text" class="form-control" id="title" name="title" required>
            </div> -->
            <nav class="mainmenu_side_wrapper medrecform">
                <ul class="menu-click">
                    <li><a href="#" class="parent">Date Add*</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                                <input type="text" name="dateadd" value="<?php echo date('m/d/Y') ?>" class="js-date form-control" required>
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="parent">Anamnesis*</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <textarea class="form-control" id="diagnose" name="anamnesis"></textarea>
                                    <input type="hidden" name="idpet" value="<?php echo $idpet ?>">
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="parent">Basic Checkup</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="width: 160px;">Weight :</label>
                                    <input type="text" class="form-control" id="title" value="" placeholder="0" name="weight" style="width: 100px;display: initial;margin: 0px 15px 0px 0px;"><span style="color: #fff;">Kg</span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="width: 160px;">Temperature : </label>
                                    <input type="text" class="form-control" id="title" value="" placeholder="0" name="temperature" style="width: 100px;display: initial;margin: 0px 15px 0px 0px;"><span style="color: #fff;">' Celcius</span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="width: 160px;">Heart Rate : </label>
                                    <input type="text" class="form-control" id="title" value="" placeholder="0" name="heartrate" style="width: 100px;display: initial;margin: 0px 15px 0px 0px;"><span style="color: #fff;">beats/minute</span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="width: 160px;">Respiration Rate :</label>
                                    <input type="text" class="form-control" id="title" value="" placeholder="0" name="resprate" style="width: 100px;display: initial;margin: 0px 15px 0px 0px;"><span style="color: #fff;">breaths/minute</span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="width: 160px;">BCS :</label>
                                    <input type="text" class="form-control" id="bcs" value="" placeholder="1-9" name="bcs" style="width: 100px;display: initial;margin: 0px 15px 0px 0px;"><!-- <span style="color: #fff;">breaths/minute</span> -->
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="width: 160px;">Food :</label>
                                    <textarea name="food" class="form-control"></textarea>
                                   
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="parent">Physical Exam</a>
                        <ul>
                            <li>
                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Eyes and Ears :</label>
                                    <textarea class="form-control" id="diagnose" name="eyes"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Skin and Coat :</label>
                                    <textarea class="form-control" id="diagnose" name="skin"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Respiratory System :</label>
                                    <textarea class="form-control" id="diagnose" name="resp"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Circulation System :</label>1
                                    <textarea class="form-control" id="diagnose" name="circ"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Digestive System :</label>
                                    <textarea class="form-control" id="diagnose" name="dige"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Urinary System :</label>
                                    <textarea class="form-control" id="diagnose" name="urin"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nervous System :</label>
                                    <textarea class="form-control" id="diagnose" name="nerv"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Musculoskeletal System :</label>
                                    <textarea class="form-control" id="diagnose" name="musc"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Other notes :</label>
                                    <textarea class="form-control" id="diagnose" name="othernotes"></textarea>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="parent">Diagnostic Tools Result </a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hematology : </label>
                                    <span>File type must .png or.jpg max size : 5 MB</span>
                                    <input type="file" name="hema" class="form-control">
                                    <textarea class="form-control" id="hemanotes" name="hemanotes" placeholder="Hematology notes" style="margin: 10px 0px;"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Blood Chemistry :</label>
                                    <span>File type must .png or.jpg max size : 5 MB</span>
                                    <input type="file" name="blood" class="form-control">
                                    <textarea class="form-control" name="bloodnotes" placeholder="Blood Chemistry notes"  style="margin: 10px 0px;"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ultrasonography :</label>
                                    <span>File type must .png or.jpg max size : 5 MB</span>
                                    <input type="file" name="ultrasono" class="form-control">
                                    <textarea class="form-control" name="ultrasononotes" placeholder="Ultrasonography notes"  style="margin: 10px 0px;"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">X-ray :</label>
                                    <input type="file" name="xray" class="form-control">
                                    <textarea class="form-control" name="xraynotes" placeholder="X-ray notes"  style="margin: 10px 0px;"></textarea>
                                </div>  
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Other Diagnostic :</label>
                                    <input type="file" name="otherdiag" class="form-control">
                                    <textarea class="form-control" name="otherdiagnotes" placeholder="Other Diagnostic notes"  style="margin: 10px 0px;"></textarea>
                                </div> 
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="parent">Diagnosis*</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <textarea class="form-control" id="diagnose" name="diagnose"></textarea>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="parent">Treatment</a>
                        <ul>
                            <li>
                                <!-- <div class="form-group">
                                    <textarea class="form-control" id="diagnose" name="treatment"></textarea>
                                </div> -->
                                <table><tr class="item-row"><td class="item-name" width="80%"><input type="text" class="chooseservice form-control item ui-autocomplete-input" placeholder="Treatment" name="treatment[]" autocomplete="off"><input class="form-control qty" name="qty[]" placeholder="Quantity" type="hidden" required><input class="form-control price" name="price[]" placeholder="Price" type="hidden" required></td><td class="item-name" width="20%"><a id="addRow" href="javascript:;" title="Add Item" class="icon-tab theme_button color2" style="color: #fff;">Add</a></td></tr></table>
                                
                            </li>
                        </ul>
                    </li>
                    <li class="active-submenu"><a href="#" class="parent">Next Schedule</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                                <input type="text" name="nextdate" value="<?php // echo date('m/d/Y', strtotime('+1 month')) ?>" class="js-date form-control">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- <li><a href="#" class="parent">Medicine</a>
                        <ul>
                            <li>
                                <div class="form-group">
                                    <textarea class="form-control" id="diagnose" name="drugs"></textarea>
                                </div>
                            </li>
                        </ul>
                    </li> -->
                </ul>                
            </nav>   
            * Mandatory, must be filled.  
        
      </div>
      <div class="modal-footer">        
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
        <button type="submit" class="btn btn-primary">SAVE</button> -->
        <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $idpet ?>" class="icon-tab theme_button color3">CANCEL</a>
        <button type="submit" class="icon-tab theme_button color2">SAVE</button>
      </div>
      </form>
    </div>
  </div>
</div>
</section>



<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->

<!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ad8cbhz7hy8flomantnx8ehdj6a4lpjuuqs226xxs9yytcjy"></script>
<script>tinymce.init({ selector:'textarea.tiny', menubar: false, });</script> -->

    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init --->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#breed').autocomplete({
                source: "<?php echo site_url('pet/breedlist');?>",
      
                select: function (event, ui) {
                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });
 
        });
    </script>
        <script>
    (function (jQuery) {

    $.opt = {};  // jQuery Object

    

    jQuery.fn.invoice = function (options) {
        var ops = jQuery.extend({}, jQuery.fn.invoice.defaults, options);
        $.opt = ops;

        var inv = new Invoice();
        inv.init();

        jQuery('body').on('click', function (e) {
            var cur = e.target.id || e.target.className;

            if (cur == $.opt.addRow.substring(1))
                inv.newRow();

            if (cur == $.opt.delete.substring(1))
                inv.deleteRow(e.target);

            inv.init();
        });

        jQuery('body').on('keyup', function (e) {
            inv.init();
        });

        return this;
    };
}(jQuery));

function Invoice() {
    self = this;
}

Invoice.prototype = {
    constructor: Invoice,

    init: function () {
        this.calcTotal();
        this.calcTotalQty();
        this.calcSubtotal();
        this.calcTaxpersen();
        //this.calcTax();
        this.calcGrandTotal();        
    },

    /**
     * Calculate total price of an item.
     *
     * @returns {number}
     */
    calcTotal: function () {
         jQuery($.opt.parentClass).each(function (i) {
             var row = jQuery(this);
             var total = row.find($.opt.price).val() * row.find($.opt.qty).val();

             //total = self.roundNumber(total,0);

             row.find($.opt.total).html(total);
         });

         return 1;
     },
    
    /***
     * Calculate total quantity of an order.
     *
     * @returns {number}
     */
    calcTotalQty: function () {
         var totalQty = 0;
         jQuery($.opt.qty).each(function (i) {
             var qty = jQuery(this).val();
             if (!isNaN(qty)) totalQty += Number(qty);
         });

         //totalQty = self.roundNumber(totalQty, 0);

         jQuery($.opt.totalQty).html(totalQty);
         jQuery($.opt.allQty).val(totalQty);

         return 1;
     },

    /***
     * Calculate subtotal of an order.
     *
     * @returns {number}
     */
    calcSubtotal: function () {
         var subtotal = 0;
         jQuery($.opt.total).each(function (i) {
             var total = jQuery(this).html();
             if (!isNaN(total)) subtotal += Number(total);
         });

         //subtotal = self.roundNumber(subtotal, 2);

         jQuery($.opt.subtotal).html(subtotal);

         return 1;
     },

    /**
     * Calculate grand total of an order.
     *
     * @returns {number}
     */  

    calcTaxpersen: function () {
       $('#taxpersen').keyup(function(){
          var persen = $('#taxpersen').val();
          var perseratus = persen/100;
          //$('#result').text($('#shares').val() * 1.5);
          //grandTotal = self.roundNumber(grandTotal, 2);
          var grandTotal3 = Number(jQuery($.opt.subtotal).html()) - Number(jQuery($.opt.discount).val());
          var valuepersen = grandTotal3 * perseratus;
          var totalpricefix = grandTotal3 + valuepersen;
          $('#tax').html(valuepersen);
          $('#taxsave').val(valuepersen);          
          // $('#grandTotal').html(totalpricefix);
          // $('#totalprice').val(totalpricefix);
          jQuery($.opt.grandTotal).html(totalpricefix);
          jQuery($.opt.totalPrice).val(totalpricefix);
          console.log(totalpricefix); 
          return 1;
        });    

        return 1;
    },  

    // calcTax: function () {
    //     var persen = 10/100;
    //     var Tax = Number(jQuery($.opt.subtotal).html()) * Number(persen);
    //                    ///+ Number(jQuery($.opt.shipping).val())
                      
    //     //grandTotal = self.roundNumber(grandTotal, 2);
    //     var grandTotal2 = Number(jQuery($.opt.subtotal).html()) + Number(jQuery($.opt.Tax).html());
    //     jQuery($.opt.Tax).html(Tax);
    //     jQuery($.opt.grandTotal).html(grandTotal2);
    //     jQuery($.opt.totalPrice).val(grandTotal2);
    //     //console.log(Tax);     

    //     return 1;
    // },

    calcGrandTotal: function () {
        var grandTotal = Number(jQuery($.opt.subtotal).html()) - Number(jQuery($.opt.discount).val()) + Number(jQuery($.opt.Tax).html());

        jQuery($.opt.grandTotal).html(grandTotal);
        jQuery($.opt.totalPrice).val(grandTotal);

        return 1;
    },

    /**
     * Add a row.
     *
     * @returns {number}
     */
    newRow: function () {
        jQuery(".item-row:last").after('<tr class="item-row"><td class="item-name" width="80%"><input type="text" class="chooseservice form-control item " placeholder="Treatment" type="text" name="treatment[]"><input class="form-control qty" name="qty[]" placeholder="Quantity" type="hidden" required><input class="form-control price" name="price[]" placeholder="Price" type="hidden" required></td><td class="item-name" width="20%"><a onclick="delete2();" href="javascript:;" title="Remove Row" class="icon-tab theme_button color3" style="color: #fff;">Delete</a></td></tr>');
        
        $("#discount").val('0');
        $("#taxpersen").val('');
        $("#tax").html('');

        if (jQuery($.opt.delete).length > 0) {
            jQuery($.opt.delete).show();
        }

        $('.chooseservice').autocomplete({

                source: "<?php echo site_url('service/allsearch/?');?>",                      
                select: function (event, ui) {
                    var row = $(this).closest('tr');
                    var price = row.find('.price');
                    var qty = row.find('.qty');

                    //$('[name="breed"]').val(ui.item.nameservice); 
                   //price.val(ui.item.price);
                    var harga = ui.item.price;
                    price.val(ui.item.description);
                    qty.val('1');
                    //console.log(harga);
                    //price.val(ui.item.label);                     
                }                
            });

        return 1;
    },

    /**
     * Delete a row.
     *
     * @param elem   current element
     * @returns {number}
     */
    deleteRow: function (elem) {
        jQuery(elem).parents($.opt.parentClass).remove();

        if (jQuery($.opt.delete).length < 1) {
            jQuery($.opt.delete).hide();
        }

        return 1;
    },

    /**
     * Round a number.
     * Using: http://www.mediacollege.com/internet/javascript/number/round.html
     *
     * @param number
     * @param decimals
     * @returns {*}
     */
    roundNumber: function (number, decimals) {
        var newString;// The new rounded number
        decimals = Number(decimals);

        if (decimals < 1) {
            newString = (Math.round(number)).toString();
        } else {
            var numString = number.toString();

            if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
                numString += ".";// give it one at the end
            }

            var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
            var d1 = Number(numString.substring(cutoff, cutoff + 1));// The value of the last decimal place that we'll end up with
            var d2 = Number(numString.substring(cutoff + 1, cutoff + 2));// The next decimal, after the last one we want

            if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
                if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
                    while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
                        if (d1 != ".") {
                            cutoff -= 1;
                            d1 = Number(numString.substring(cutoff, cutoff + 1));
                        } else {
                            cutoff -= 1;
                        }
                    }
                }

                d1 += 1;
            }

            if (d1 == 10) {
                numString = numString.substring(0, numString.lastIndexOf("."));
                var roundedNum = Number(numString) + 1;
                newString = roundedNum.toString() + '.';
            } else {
                newString = numString.substring(0, cutoff) + d1.toString();
            }
        }

        if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
            newString += ".";
        }

        var decs = (newString.substring(newString.lastIndexOf(".") + 1)).length;

        for (var i = 0; i < decimals - decs; i++)
            newString += "0";
        //var newNumber = Number(newString);// make it a number if you like

        return newString; // Output the result to the form field (change for your purposes)
    }
};

/**
 *  Publicly accessible defaults.
 */
jQuery.fn.invoice.defaults = {
    addRow: "#addRow",
    delete: "#delete",
    parentClass: ".item-row",
    
    addRow2: "#addRow2",
    delete2: "#delete2",
    parentClass: ".item-row2",

    price: ".price",
    qty: ".qty",
    total: ".total",
    totalQty: "#totalQty",

    subtotal: "#subtotal",
    discount: "#discount",
    shipping: "#shipping",
    grandTotal: "#grandTotal",
    totalPrice : "#totalPrice",
    allQty : "#allQty",
    Taxpersen: "#taxpersen",
    Tax:"#tax",
};
    </script>    

    <script>
    
        $('#addRow2').click(function(){
            jQuery(".item-row2:last").after('<tr class="item-row2"><td class="item-name" width="80%"><input type="text" class="chooseservice2 form-control item ui-autocomplete-input" placeholder="Vaccine" type="text" name="vaccine[]"><input class="form-control qty" name="qty[]" placeholder="Quantity" type="hidden" value="1"><input class="form-control price" name="price[]" placeholder="Price" type="hidden" value="0" ></td><td class="item-name" width="20%"><a id=' + $.opt.delete.substring(1) + ' href="javascript:;" title="Remove Row" class="icon-tab theme_button color3" style="color: #fff;">Delete</a></td></tr>');
            $('.chooseservice2').autocomplete({

                source: "<?php echo site_url('service/allsearch/?');?>",                      
                select: function (event, ui) {
                    var row = $(this).closest('tr');
                    var price = row.find('.price');
                    var qty = row.find('.qty');

                    //$('[name="breed"]').val(ui.item.nameservice); 
                   //price.val(ui.item.price);
                    var harga = ui.item.price;
                    price.val(ui.item.description);
                    qty.val('1');
                    //console.log(harga);
                    //price.val(ui.item.label);                     
                }                
            });

            return 1;
        });
            
        $(document).ready(function(){
        
            $().invoice({
                addRow : "#addRow",
                delete : "#delete",
                parentClass : ".item-row",
                
                addRow2 : "#addRow2",
                delete2 : "#delete2",
                parentClass : ".item-row2",

                price : ".price",
                qty : ".qty",
                total : ".total",
                totalQty: "#totalQty",

                subtotal : "#subtotal",
                discount: "#discount",
                shipping : "#shipping",
                grandTotal : "#grandTotal",
                totalPrice : "#totalPrice",
                allQty : "#allQty",
                Taxpersen: "#taxpersen",
                Tax:"#tax",
            }); 

            $('input.chooseservice').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('service/allsearch/?');?>",
      
                select: function (event, ui) {
                    var row = $(this).closest('tr');
                    var price = row.find('.price');
                    var qty = row.find('.qty');

                    //$('[name="breed"]').val(ui.item.label); 
                    //$('[name="description"]').val(ui.item.description);
                    var harga = ui.item.price;
                    price.val(ui.item.description);
                    qty.val('1');
                    //price.val(ui.item.label);                     
                }                
            });

            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    //price.val(ui.item.label);                     
                }                
            });       
            
        });
    </script>
    <script>
        function delete2(){
            jQuery(".item-row:last").remove();
        }
    </script>

</body>

</html>
