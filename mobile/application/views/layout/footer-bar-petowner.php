              <!-- Footer Bar -->
              <div class="footer-bar footer-bar-petowner">
                <div class="swiper-container-toolbar swiper-toolbar swiper-init" data-effect="slide" data-slides-per-view="4" data-slides-per-group="3" data-space-between="0" data-pagination=".swiper-pagination-toolbar">
                  <div class="swiper-pagination-toolbar"></div>
                  <div class="swiper-wrapper">
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-mypets.php"><i class="fas fa-paw"></i>My Pets</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-vetsearch.php"><i class="fas fa-user-friends"></i>Vet Search</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-petshop.php"><i class="fas fa-store"></i>Pet Shop</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-petservices.php"><i class="fas fa-briefcase-medical"></i>Pet Services</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-cart.php"><i class="fas fa-shopping-cart"></i>Shopping Cart</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-invoice.php"><i class="fas fa-receipt"></i>Invoice</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-editprofile.php"><i class="fas fa-user"></i>Edit Profile</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-owner-signout.php"><i class="fas fa-sign-out-alt"></i>Sign Out</a></div>
                  </div>
                </div>
                <div class="links-row">
                  <a href="terms.php">Terms &amp; Conditions</a>
                  <a href="privacy.php">Privacy Policy</a>
                  <a href="faq.php">FAQ</a>
                  <div class="copyright">&copy; 2019 iVet Data</div>
                </div>
              </div>