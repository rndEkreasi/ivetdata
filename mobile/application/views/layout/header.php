<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
    <link href="images/apple-touch-startup-image-320x460.png" media="(device-width: 320px)"
      rel="apple-touch-startup-image">
    <link href="images/apple-touch-startup-image-640x920.png"
      media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
    <title>varius - mobile template</title>
    <link rel="stylesheet" href="css/framework7.css">
    <link rel="stylesheet" href="plugins/fontawesome/css/all.min.css"> <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/bootstrap/bootstrap4classes.css"> <!-- Bootstrap 4 Classes -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="custom.css">
    <link type="text/css" rel="stylesheet" href="css/swipebox.css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900' rel='stylesheet' type='text/css'>
  </head>

  <body id="mobile_wrap">
    <div class="statusbar-overlay"></div>
    <div class="panel-overlay"></div>
    <div class="views">
      <div class="view view-main">
        <div class="pages">
          <div data-page="index" class="page homepage">
            <div class="page-content">
              <div class="navbarpages nobg">
                <ul id="navs" class="navtopleft" data-open="-" data-close="+">
                  <li><a href="index.php"><i class="fas fa-home"></i></a></li>
                  <li><a href="about.php"><i class="fas fa-info"></i></a></li>
                  <li><a href="contact.php"><i class="fas fa-phone"></i></a></li>
                  <li><a href="news.php"><i class="far fa-newspaper"></i></a></li>
                </ul>
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="index.php"><img src="images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>