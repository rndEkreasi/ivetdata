              <!-- Footer Bar -->
              <div class="footer-bar footer-bar-vet">
                <div class="swiper-container-toolbar swiper-toolbar swiper-init" data-effect="slide" data-slides-per-view="4" data-slides-per-group="3" data-space-between="0" data-pagination=".swiper-pagination-toolbar">
                  <div class="swiper-pagination-toolbar"></div>
                  <div class="swiper-wrapper">
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-mypets.php"><i class="fas fa-paw"></i>Pet Patient</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-schedule.php"><i class="fas fa-calendar-alt"></i>Schedule</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-invoice.php"><i class="fas fa-receipt"></i>Invoice</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-services.php"><i class="fas fa-briefcase-medical"></i>Shop Items</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-editprofile.php"><i class="fas fa-user"></i>Edit Profile</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-team.php"><i class="fas fa-users"></i>My Team</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-subscription.php"><i class="fas fa-money-check-alt"></i>Subscription</a></div>
                    <div class="swiper-slide toolbar-icon"><a href="dashboard-vet-signout.php"><i class="fas fa-sign-out-alt"></i>Sign Out</a></div>
                  </div>
                </div>
                <div class="links-row">
                  <a href="terms.php">Terms &amp; Conditions</a>
                  <a href="privacy.php">Privacy Policy</a>
                  <a href="faq.php">FAQ</a>
                  <div class="copyright">&copy; 2019 iVet Data</div>
                </div>
              </div>