<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
    <link href="images/apple-touch-startup-image-320x460.png" media="(device-width: 320px)"
      rel="apple-touch-startup-image">
    <link href="images/apple-touch-startup-image-640x920.png"
      media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
    <title>iVetData</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/framework7.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome/css/all.min.css"> <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap/bootstrap4classes.css"> <!-- Bootstrap 4 Classes -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/custom.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/css/swipebox.css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700,900' rel='stylesheet' type='text/css'>
  </head>

  <body id="mobile_wrap">
    <div class="statusbar-overlay"></div>
    <div class="panel-overlay"></div>
    <div class="views">
      <div class="view view-main">
        <div class="navbarpages nobg">
          <ul id="navs" class="navtopleft" data-open="−" data-close="+">
            <li><a href="<?php echo base_url() ?>"><i class="fas fa-home"></i></a></li>
            <li><a href="<?php echo base_url() ?>about"><i class="fas fa-info"></i></a></li>
            <li><a href="<?php echo base_url() ?>contact"><i class="fas fa-phone"></i></a></li>
            <li><a href="<?php echo base_url() ?>news"><i class="far fa-newspaper"></i></a></li>
          </ul>
        </div>
        <div class="pages">
          <div data-page="index" class="page homepage">
            <div class="page-content page-smallfooter">
              <div class="navbarpages nobg">
                <div class="navbar_logo_right">
                  <div class="logo_image"><a href="home.php"><img src="<?php echo base_url() ?>assets/images/logo_image_dark.png" alt="" title="" /></a></div>
                </div>
              </div>
              <!-- FullBG -->
              <div class="fullbg" style="background-image:url(<?php echo base_url() ?>assets/images/slides/slide-1.jpg);">
                <div class="fullbg-cover">
                  <div class="fullbg-content">
                    <div class="tabs-custom">
                      <div class="buttons-row">
                        <a href="#tabhome-microchip" class="tab-link active button mr-1"><i class="fas fa-paw mr-1"></i>Microchip</a>
                        <a href="#tabhome-vetsearch" class="tab-link button"><i class="fas fa-user-friends mr-1"></i>Vets</a>
                      </div>
                      <div class="tabs-simple mb-3">
                        <div class="tabs">
                          <div id="tabhome-microchip" class="tab active">
                            <form class="search-form">
                                <div class="form-row mb-3">
                                  <input type="text" name="name" value="" class="form-input" placeholder="Microchip ID" />
                                  <button class="btn btn-primaryred">Search</i></button>
                                </div>
                                <div class="form-row">
                                  <p>Enter the microchip ID above and click search. Enter only the 9, 10 or 15 character microchip number, with no punctuation or spaces.</p>
                                </div>
                            </form>
                          </div>
                          <div id="tabhome-vetsearch" class="tab">
                            <form class="search-form">
                              <div class="form-row mb-3">
                                <input type="text" name="name" value="" class="form-input" placeholder="Vet Name" />
                                <input type="text" name="name" value="" class="form-input" placeholder="City" />
                                <button class="btn btn-primaryred">Find</button>
                              </div>
                              <div class="form-row">
                                <p>Enter the name of the vet or the city that you want to search.</p>
                              </div>
                            </form>
                          </div> 
                        </div>
                      </div>
                      <div class="row">
                        <div class="col btns-row">
                          <a data-popup=".popup-signup" class="open-popup close-panel btn btn-secondary btn-lg">REGISTER</a>
                          <a data-popup=".popup-login" class="open-popup close-panel btn btn-primary btn-lg">LOGIN</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php // include 'layout/footer-bar.php' ?>
            