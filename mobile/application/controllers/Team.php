<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Team_model');
		$this->load->model('Setting_model');
		$this->load->model('Login_model');
		$this->load->model('Clinic_model');
		$this->load->model('Login_model');
    }

    public function all(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$team = $dataprofile[0]->team;
			$dataclinic = $this->Home_model->dataclinic($profileid);
			$idclinic = $dataclinic[0]->idclinic;
			$data = array(
				'title' => 'Team vets - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,'manageto' => $dataprofile[0]->manageto,				
				'nameclinic' => $dataclinic[0]->nameclinic,
				'team' => $team,
				'datavets' => $this->Team_model->totalvets($idclinic),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/myteam_view',$data);
			$this->load->view('website/footerdash_view',$data);

		}else{
			 redirect ('login');
		}
	}

	public function adduser(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$idclinic = $dataprofile[0]->idclinic;
				$detailclinic = $this->Clinic_model->detailclinic($idclinic);
				$nameclinic = $detailclinic[0]->nameclinic;
				$expdate = $dataprofile[0]->expdate;
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$this->form_validation->set_rules('name', 'Name', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_rules('password', 'Password', 'required');
				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
				$password = $this->input->post('password');
				$manageto = $this->input->post('rolemanage');
				if($manageto == '2'){
					$rolenya = 'Vet';
				}if($manageto == '3'){
					$rolenya = 'Admin Staff';
				}
				if ($this->form_validation->run() == FALSE){
					$this->session->set_flashdata('error', 'Please filled all mandatory field.');
					redirect ('Team/all');
				}else{
					$user_email = $email;
					$cekemail = $this->Login_model->read_user_information($user_email);
					$adaemail = count($this->Login_model->adaemail($user_email));
					if ( $adaemail > '0'){
						$this->session->set_flashdata('error', 'Email has been used please try another email');
						redirect ('Team/all');
					}else{
						//insert to table member
						$datamember = array(
							'name' => $name,
							'email' => $email,
							'phone' => $phone,
							'status' => '1',
							'role' => '1',
							'manageto' => $manageto,
							'password' => md5($password),
							'realpass' => $password,
							'tgl_reg' => date('Y-m-d h:i:s'),
							'unikcode' => md5($email),
							'country' => $country,
							'city' => $city,
							'expdate' => $expdate,
							'idclinic' => $idclinic,
						);
						$this->db->insert('tbl_member',$datamember);
						$uid = $this->db->insert_id();
					}
					//sent email
					$subject = 'Hi '.$name.', You are registered as '.$rolenya.' in '.$nameclinic.'';	
					$message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>

						<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">

							 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>

							 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$name.', </h1>

							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">This is detail login in https://ivetdata.com <br>
							 	Email : '.$email.'<br>
							 	Password : '.$password.'<br>
							 	Role : '.$rolenya.'<br>
							 	Clinic : '.$nameclinic.'
							 </p>

							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">

							 <a href="'.base_url().'login" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank""> Please Login </a> </p>
							 </td> </tr>

							 </tbody> </table> </td> </tr></tbody> </table>';
						$text = $message;
						//$email = $user_email;
					    //$this->load->view('testemail_view');
					    $this->load->library('EmailSender.php');
					    $this->emailsender->send($email,$subject,$text);

					    $this->session->set_flashdata('success', 'Congrats, '.$name.' has been registered as '.$rolenya.' in your clinic');
						redirect ('Team/all');

				}
				// $team = $dataprofile[0]->team;
				// $dataclinic = $this->Home_model->dataclinic($profileid);
				// $idclinic = $dataclinic[0]->idclinic;
				// $data = array(
				// 	'title' => 'Team vets - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,				
				// 	'nameclinic' => $dataclinic[0]->nameclinic,
				// 	'team' => $team,
				// 	'datavets' => $this->Team_model->totalvets($idclinic),
				// 	'success' => $this->session->flashdata('success'),
				// 	//'clinicfav' => $this->Home_model->clinicfav(),
					
				// );
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/myteam_view',$data);
				// $this->load->view('website/footerdash_view',$data);

			}else{
				 redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function edit(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$idclinic = $dataprofile[0]->idclinic;
				$detailclinic = $this->Clinic_model->detailclinic($idclinic);
				$nameclinic = $detailclinic[0]->nameclinic;
				$expdate = $dataprofile[0]->expdate;
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$uid = $this->input->get('uid');
				$email = $this->input->get('email');
				$clinic = $this->input->get('idclinic');
				$cekuid = $this->Team_model->cekuid($uid,$email,$clinic);
				//var_dump($cekuid);
				if(count($cekuid) == '0'){
					$this->session->set_flashdata('error', 'You cant edit this acccount ');
					redirect ('Team/all');
				}else{
					if($idclinic != $clinic){
						$this->session->set_flashdata('error', 'You cant edit this acccount ');
						redirect ('Team/all');
					}else{
						$data = array(
							'title' => 'Team vets - iVetdata.com',
							'nama' => $nama,
							'country' => $country,
							'city' => $city,
							'profilephoto' => $profilephoto,
							'role' => $role,
							'manageto' => $dataprofile[0]->manageto,
							'datastaff' => $cekuid,							
						);
						$this->load->view('website/headerdash_view',$data);
						$this->load->view('website/editteam_view',$data);
						$this->load->view('website/footerdash_view',$data);
					}

				}
				$this->form_validation->set_rules('name', 'Name', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_rules('password', 'Password', 'required');


			}else{
				 redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function edituser(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$idclinic = $dataprofile[0]->idclinic;
				$detailclinic = $this->Clinic_model->detailclinic($idclinic);
				$nameclinic = $detailclinic[0]->nameclinic;
				$expdate = $dataprofile[0]->expdate;
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$this->form_validation->set_rules('uid', 'Uid', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_rules('name', 'Name', 'required');
				$name = $this->input->post('name');
				$uid = $this->input->post('iduser');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
				$password = $this->input->post('password');
				$manageto = $this->input->post('rolemanage');
				$clinic = $this->input->post('idclinic');
				$cekuid = $this->Team_model->cekuid($uid,$email,$clinic);
				if($idclinic != $clinic){
					$this->session->set_flashdata('error', 'You cant edit this acccount ');
					redirect ('Team/all');
				}else{
					$dataupdate = array(
						'name' => $name,
						'email' => $email,
						'phone' => $phone,
						'manageto' => $manageto,
						'password' => md5($password),
						'realpass' => $password,							
						'unikcode' => md5($email),						
					);
					//var_dump($dataupdate);
					$this->db->where('uid', $uid);
					$this->db->update('tbl_member', $dataupdate);
					$this->session->set_flashdata('success', 'Congrats, Staff data has been updated');
					redirect ('team/all');
				}
			}else{
				 redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function deleteuser(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$idclinic = $dataprofile[0]->idclinic;
				$detailclinic = $this->Clinic_model->detailclinic($idclinic);
				$nameclinic = $detailclinic[0]->nameclinic;
				$expdate = $dataprofile[0]->expdate;
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$uid = $this->input->get('uid');
				$email = $this->input->get('email');
				$clinic = $this->input->get('idclinic');
				$cekuid = $this->Team_model->cekuid($uid,$email,$clinic);
				if($idclinic != $clinic){
					$this->session->set_flashdata('error', 'You cant edit this acccount ');
					redirect ('Team/all');
				}else{
					$dataupdate = array(
						'status' => '0',	
						'idclinic' => '0',
						'role' => '0',
						'manageto' => '0',
						'email' => 'delete'.$email,
					);
					//var_dump($dataupdate);
					$this->db->where('uid', $uid);
					$this->db->update('tbl_member', $dataupdate);
					$this->session->set_flashdata('success', 'Congrats, Staff data has been deleted');
					redirect ('team/all');
				}
			}else{
				 redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}
}