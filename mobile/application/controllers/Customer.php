<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Customer_model');
		$this->load->model('Login_model');
		$this->load->model('Pet_model');
    }

    public function all(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
		    if(count($dataclinic)>0){
			    $nameclinic = $dataclinic[0]->nameclinic;
			}else{
			    $nameclinic = 0;
			}
			//pagination
			$all_customer = $this->Customer_model->jumlah_data($idclinic,$role);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'customer/all/';
			$config['total_rows'] = $all_customer;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	
			$data = array(
				'title' => 'Customer - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'from' => $from,
				'idclinic' => $idclinic,
				'profilephoto' => $profilephoto,
				'role' => $role,'manageto' => $dataprofile[0]->manageto,				
				'nameclinic' => $nameclinic,
				'all_customer' => $all_customer,
				'datacustomer' => $this->Customer_model->data($config['per_page'],$from,$idclinic,$role),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/mycustomer_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			redirect ('login');
		}
    }

    public function edit(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idcustomer = $this->input->get('idcustomer');
			$datacustomer = $this->Customer_model->detailcustomer($idcustomer);
			$cliniccustomer = $datacustomer[0]->idclinic;
			if($idclinic != $cliniccustomer){
				$this->session->set_flashdata('error', 'You cannot edit this customer .');
				redirect ('customer/all');
			}else{
				//var_dump($datacustomer);
				$data = array(
					'title' => 'Customer - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'idcustomer' => $idcustomer,
					'customername' => $datacustomer[0]->nama,
					'customerphone' => $datacustomer[0]->nohp,
					'customeremail' => $datacustomer[0]->email,
					'customeraddress' => $datacustomer[0]->address,	
					'customercity' => $datacustomer[0]->city,
					'customercountry' => $datacustomer[0]->country,
					'datacountry' => $this->Login_model->country(),
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/editcustomer_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}	        
        }else{
			 redirect ('login');
		}
    }



    public function editprocess(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idcustomer = $this->input->post('idcustomer');
			$datacustomer = $this->Customer_model->detailcustomer($idcustomer);
			$cliniccustomer = $datacustomer[0]->idclinic;
			$uidcustomer = $datacustomer[0]->uid;
			//var_dump()
			if($idclinic != $cliniccustomer){
				$this->session->set_flashdata('error', 'You cannot edit this customer .');
				redirect ('customer/all');
			}else{
				$customername = $this->input->post('customername');
				$customerphone = $this->input->post('customerphone');
				$customeremail = $this->input->post('customeremail');
				$customeraddress = $this->input->post('customeraddress');
				$customercity = $this->input->post('customercity');
				$customercountry = $this->input->post('customercountry');
				$idpet = $this->input->post('idpet');
				$updatecustomer = array(
					'nama' => $customername,
					'email' => $customeremail,
					'nohp' => $customerphone,
					'city' => $customercity, 
					'address' => $customeraddress,
					'country' => $customercountry
				);
				$this->db->where('idcustomer',$idcustomer);
				$this->db->update('tbl_customer',$updatecustomer);

				//update pet table
				$updatepetcustomer = array(
					'namapemilik' => $customername,
					'email' => $customeremail,
					'nohp' => $customerphone,
					'city' => $customercity, 
					'address' => $customeraddress,
					'country' => $customercountry
				);
				//$this->db->where('idpet',$idpet);
				$this->db->where('idowner',$idcustomer);
				$this->db->update('tbl_pet',$updatepetcustomer);

				//update member table
				$updatemember = array(
					'name' => $customername,
					'email' => $customeremail,
					'phone' => $customerphone,
					'city' => $customercity, 
					'address' => $customeraddress,
					'country' => $customercountry
				);
				$this->db->where('uid',$uidcustomer);
				$this->db->update('tbl_member',$updatemember);

				$this->session->set_flashdata('success', 'Congrats, Data has been Updated');
				redirect ('customer/all');

				//var_dump($datacustomer);
				// $data = array(
				// 	'title' => 'Customer - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'idcustomer' => $idcustomer,
				// 	'customername' => $datacustomer[0]->nama,
				// 	'customerphone' => $datacustomer[0]->nohp,
				// 	'customeremail' => $datacustomer[0]->email,
				// 	'customeraddress' => $datacustomer[0]->address,	
				// 	'customercity' => $datacustomer[0]->city,
				// 	'customercountry' => $datacustomer[0]->country,
				// );
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/editcustomer_view',$data);
				// $this->load->view('website/footerdash_view',$data);
			}	        
        }else{
			 redirect ('login');
		}

    }

    public function delete(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idcustomer = $this->input->get('idcustomer');
			$datacustomer = $this->Customer_model->detailcustomer($idcustomer);
			$cliniccustomer = $datacustomer[0]->idclinic;
			if($idclinic != $cliniccustomer){
				$this->session->set_flashdata('error', 'You cannot delete this customer .');
				redirect ('customer/all');
			}else{
				//delete customer
				$dataupdate = array(					
					'status' => '1',
					'delete_by' => $nama.' - '.date('d M Y, H:i:s'),
				);
				$this->db->where('idcustomer',$idcustomer);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_customer', $dataupdate);

				//delete pet customer
				$deletepet = array(					
					'status' => '1',
					'delete_by' => $nama.' - '.date('d M Y, H:i:s'),
				);
				$this->db->where('idowner',$idcustomer);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_pet', $deletepet);

				$this->session->set_flashdata('success', 'Customer has been deleted.');
				redirect ('customer/all');
				// //var_dump($datacustomer);
				// $data = array(
				// 	'title' => 'Customer - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'idcustomer' => $idcustomer,
				// 	'customername' => $datacustomer[0]->nama,
				// 	'customerphone' => $datacustomer[0]->nohp,
				// 	'customeremail' => $datacustomer[0]->email,
				// 	'customeraddress' => $datacustomer[0]->address,	
				// 	'customercity' => $datacustomer[0]->city,
				// 	'customercountry' => $datacustomer[0]->country,
				// 	'datacountry' => $this->Login_model->country(),
				// );
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/editcustomer_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}	        
        }else{
			 redirect ('login');
		}
    }
    
    function allsearch(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
	        if (isset($_GET['term'])) {
	            $result = $this->Customer_model->search_blog($_GET['term'],$idclinic);
	            if (count($result) > 0) {
		             foreach ($result as $row){
					 	$result2 = $this->Pet_model->byownerphone($row->nohp);
					 	$arr_result2 = array();
						if (count($result2) > 0) {
							 foreach ($result2 as $row2)
							  $arr_result2[] = array(
									'idpet'     => $row2->idpet,
									'namapet'   => $row2->namapet,
							 );
						}
		                $arr_result[] = array(
		                    'label'         => $row->nohp,
		                    'description'   => $row->nama,
		                    'email'   => $row->email,
		                    'address'  => $row->address,
		                    'city' => $row->city,
		                    'petdata' => $arr_result2,
		                 );
		             }
	                echo json_encode($arr_result);
	            }
	        }
        }else{
			 redirect ('login');
		}
    }

    public function result(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;		
			$idclinic = $vetclinic;	
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$phone = $this->input->get('phone');
			$namecustomer = $this->input->get('customername');
			//$rfid = $idpet;
			//$datapet = $this->Pet_model->cekpetid($idpet);
			if($phone == ''){
				$datacustomer = $this->Customer_model->cekbyname($vetclinic,$namecustomer);
			}else{
				$datacustomer = $this->Customer_model->cekbyphone($phone,$vetclinic,$namecustomer);
			}
			if($namecustomer==""&&$phone==""){
			    $datacustomer = $this->Customer_model->dataname($config['per_page'],0,$idclinic,$role,$namecustomer);
			}
			$adacustomer = count($datacustomer);
			//var_dump($datapet);
			if($adacustomer == '0'){
				// $data = array(
				// 	'title' => 'Detail Pet - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'error' => 'Sorry, we cannot find pet id',
				// );
				// //$this->load->view('login_user');
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/nopet_view',$data);
				// $this->load->view('website/footerdash_view',$data);
				$this->session->set_flashdata('error', 'Sorry data customer not found.');
				if($role == '0'){
					redirect ('dashboard/customer/all');
				}else{
					redirect ('customer/all');
				}

			}else{
				//pagination
				$all_customer = $datacustomer;
				$this->load->library('pagination');
				$config['base_url'] = base_url().'customer/all/';
				$config['total_rows'] = count($all_customer);
				$config['per_page'] = 10;
				//styling pagination
				$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			    $config['full_tag_close'] = '</ul>';
			    $config['num_tag_open'] = '<li>';
			    $config['num_tag_close'] = '</li>';
			    $config['cur_tag_open'] = '<li class="active"><a href="#">';
			    $config['cur_tag_close'] = '</a></li>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';
			    $config['first_tag_open'] = '<li>';
			    $config['first_tag_close'] = '</li>';
			    $config['last_tag_open'] = '<li>';
			    $config['last_tag_close'] = '</li>';

			    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';


			    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    $config['next_tag_open'] = '<li>';
			    $config['next_tag_close'] = '</li>';

				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);
				
				$data = array(
					'title' => 'Customer - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'from' => '0',
					'idclinic' => $idclinic,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,				
					'nameclinic' => $dataclinic[0]->nameclinic,
					'all_customer' => count($datacustomer),
					'datacustomer' => $datacustomer,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/mycustomer_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}
	
	public function add(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};	
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);							
			$data = array(
				'title' => 'Add New Pet Owner - iVetdata.com',
				'nama' => $nama,
				//'country' => $country,
				//'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,'manageto' => $dataprofile[0]->manageto,
				'manageto' => $dataprofile[0]->manageto,
				'expdate' => $dataprofile[0]->expdate,
				'datacountry' => $this->Login_model->country(),
				'idclinic' => $idclinic,
				'pettype' => $this->Pet_model->pettype(),
				'nameclinic' => $dataclinic[0]->nameclinic,
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/addowner_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
		redirect ('login');
		}
	}
	
	public function addprocess(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			//dataheader
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);	
			//var_dump($dataclinic);
			$petclinic = $dataclinic[0]->pet;
			$nameclinic = $dataclinic[0]->nameclinic;
			$this->form_validation->set_rules('nameowner', 'nameowner', 'required');
			$nameowner = $this->input->post('nameowner');
			$phone= $this->input->post('phone');
			$email = $this->input->post('email');			
			$address = $this->input->post('address');
			$country = $this->input->post('country');
			$city = $this->input->post('city');
			$postalcode = $this->input->post('postalcode');
			if ($this->form_validation->run() == FALSE){
				$data = array(
						'title' => 'Add New Pet Owner - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						'profilephoto' => $profilephoto,
						'role' => $role,'manageto' => $dataprofile[0]->manageto,
						'expdate' => $dataprofile[0]->expdate,
						'datacountry' => $this->Login_model->country(),
						'idclinic' => $idclinic,
						'nameclinic' => $dataclinic[0]->nameclinic,
						'nameowner' => $nameowner,
						'phone' => $phone,
						'email' => $email,
						'address' => $address,
						'country' => $country,
						'city' => $city,
						'postalcode' => $postalcode,
					);
					//$this->load->view('login_user');
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/addowner_view',$data);

			}else{

			
			$customerclinic = $dataclinic[0]->customer;			
			$currentchip = $dataclinic[0]->chip;
			$adacustomerclinic = $this->Customer_model->petcustomeremailhp($phone,$email);
            if(count($adacustomerclinic)==0){
                
				$pass = 'ivetdata';
				$datamember = array(
					'name' => $nameowner,
					'email' => $email,
					'phone' => $phone,
					'status' => '1',
					'role' => '0',
					'manageto' => '0',
					'password' => md5($pass),
					'tgl_reg' => date('Y-m-d h:i:s'),
					'unikcode' => md5($email),										
				);
				$this->db->insert('tbl_member',$datamember);
				$uid = $this->db->insert_id();
				$datacustomer = array(
					'nama' => $nameowner,
					'nohp' => $phone,
					'email' => $email,
					'idclinic' => $idclinic,
					'city' => $city,
					'country' => $country,
					'address' => $address,	
					'uid' => $uid,
				);
				$this->db->insert('tbl_customer',$datacustomer);
				$idowner = $this->db->insert_id();

			    //sent email
			    $subject = 'Hi '.$nameowner.', You have been registered in '.$nameclinic.'';	
			    $message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
				<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
				 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
						 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$nameowner.', </h1>
						 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">Congratulations, Your pet has been registered in ivetdata.com <br>
						 	<strong>Pet Data</strong><br>
						 	Clinic : '.$nameclinic.'<br><br>
						 	<strong>Your Login Detail</strong><br>
						 	Email : '.$email.'<br>
						 	Please login and edit your password.
						 </p>
						 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">
						 <a href="'.base_url().'login/resetpass/?id='.$uid.'&email='.$email.'&token='.md5($email).'&reset=true" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank"">Login Now </a> </p>
						 </td> </tr>
				</tbody> </table> </td> </tr></tbody> </table>';
    			$text = $message;
				$this->load->library('EmailSender.php');
				$this->emailsender->send($email,$subject,$text);				

                //pagination
    			$all_customer = $this->Customer_model->jumlah_data($idclinic,$role);
    			$this->load->library('pagination');
    			$config['base_url'] = base_url().'customer/all/';
    			$config['total_rows'] = $all_customer;
    			$config['per_page'] = 10;
    			//styling pagination
    			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
    		    $config['full_tag_close'] = '</ul>';
    		    $config['num_tag_open'] = '<li>';
    		    $config['num_tag_close'] = '</li>';
    		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    		    $config['cur_tag_close'] = '</a></li>';
    		    $config['prev_tag_open'] = '<li>';
    		    $config['prev_tag_close'] = '</li>';
    		    $config['first_tag_open'] = '<li>';
    		    $config['first_tag_close'] = '</li>';
    		    $config['last_tag_open'] = '<li>';
    		    $config['last_tag_close'] = '</li>';
    
    		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
    		    $config['prev_tag_open'] = '<li>';
    		    $config['prev_tag_close'] = '</li>';
    
    
    		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
    		    $config['next_tag_open'] = '<li>';
    		    $config['next_tag_close'] = '</li>';
    
    			$from = $this->uri->segment(3);
    			$this->pagination->initialize($config);	
    			$data = array(
    				'title' => 'Customer - iVetdata.com',
    				'nama' => $nama,
    				'country' => $country,
    				'city' => $city,
    				'from' => $from,
    				'idclinic' => $idclinic,
    				'profilephoto' => $profilephoto,
    				'role' => $role,'manageto' => $dataprofile[0]->manageto,				
    				'nameclinic' => $nameclinic,
    				'all_customer' => $all_customer,
    				'datacustomer' => $this->Customer_model->data($config['per_page'],$from,$idclinic,$role),
    				'success' => 'Add New Pet Owner Success',
    			);

				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/mycustomer_view',$data);

			}else{
				$data = array(
					'title' => 'Add New Pet Owner - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'error' => 'Email or HP already exists.',
					'city' => $city,
					'profilephoto' => $profilephoto,
					'country' => $country,
					'address' => $address,
					'nameowner' => $nameowner,
					'phone' => $phone,
					'email' => $email,
					'nameclinic' => $dataclinic[0]->nameclinic,
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addowner_view',$data);

			}
		}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}		
	}

	public function exportcustomerlist(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$profilephoto = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;		
			$idclinic = $vetclinic;	
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			if(isset($_GET['customername'])){
			    $namecustomer = $_GET['customername'];
			}else{
			    $namecustomer = "";
			}
			if(isset($_GET['phone'])){
			    $phone = $_GET['phone'];
			}else{
			    $phone = "";
			}
			
			if($phone == ''){
				$datacustomer = $this->Customer_model->cekbyname($vetclinic,$namecustomer);
			}else{
				$datacustomer = $this->Customer_model->cekbyphone($phone,$vetclinic,$namecustomer);
			}
			if($namecustomer==""&&$phone==""){
			    $datacustomer = $this->Customer_model->dataname(10,0,$idclinic,$role,$namecustomer);
			}
			
			//pagination
			$all_customer = $this->Customer_model->jumlah_data($idclinic,$role);

			$data = array(
				'title' => 'Customer - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'from' => 0,
				'idclinic' => $idclinic,
				'profilephoto' => $profilephoto,
				'role' => $role,'manageto' => $dataprofile[0]->manageto,				
				'nameclinic' => $dataclinic[0]->nameclinic,
				'all_customer' => $all_customer,
				'datacustomer' => $datacustomer,
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);

			$this->load->view('website/export_customer',$data);

		}else{
			redirect ('login');
		}
    }
}