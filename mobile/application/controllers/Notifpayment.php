<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifpayment extends CI_Controller {	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
    }


    public function callxendit(){
		if ($_SERVER["REQUEST_METHOD"] === "POST") {
	        $data = file("php://input");
            $xml = "";
            foreach($data as $dt){
            	$xml = $xml.$dt;
            }
            $dt = json_decode($xml,true);
            if(count($dt)==0){
                echo '{"data":"INVALID JSON Format"}';
            }else{
                $this->db->where('invoice',$dt['external_id']);
                $this->db->update("tbl_transaksi",array('status'=>'1','tglapprove'=>date('Y-m-d H:i:s'),'approved'=>'system')); 
            	echo "OK";
            }
	    } else {
	        print_r("Cannot ".$_SERVER["REQUEST_METHOD"]." ".$_SERVER["SCRIPT_NAME"]);
	    }
	}

}