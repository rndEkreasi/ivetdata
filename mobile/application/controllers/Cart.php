<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Login_model');
		$this->load->model('Pet_model');
		$this->load->model('Customer_model');
		$this->load->model('Setting_model');
        $this->load->model('Service_model');
    }


	public function index(){
        $data = array(
            'title' => 'Welcome to Ivetdata.com',
            'page' => 'Search',
        );
        $this->load->view('website/header_new_view');
        //$this->load->view('website/landing_new_view',$data);
        $this->load->view('website/owner-cart');
        //$this->load->view('website/testimonial_new');
        //$this->load->view('website/latest_news');
        //$this->load->view('website/stat_new');
        //$this->load->view('website/pre_footer');
        $this->load->view('website/headerdash_view');
        $this->load->view('website/footer_new_view');

    }


    /**
     * @return object
     */
    public function checkout()
    {
        $this->load->view('website/header_new_view');
        //$this->load->view('website/landing_new_view',$data);
        $this->load->view('website/owner-cart-checkout');
        //$this->load->view('website/testimonial_new');
        //$this->load->view('website/latest_news');
        //$this->load->view('website/stat_new');
        //$this->load->view('website/pre_footer');
        $this->load->view('website/headerdash_view');
        $this->load->view('website/footer_new_view');
    }

    public function success(){
        $this->load->view('website/header_new_view');
        //$this->load->view('website/landing_new_view',$data);
        $this->load->view('website/owner-cart-success');
        //$this->load->view('website/testimonial_new');
        //$this->load->view('website/latest_news');
        //$this->load->view('website/stat_new');
        //$this->load->view('website/pre_footer');
        $this->load->view('website/headerdash_view');
        $this->load->view('website/footer_new_view');

    }
}