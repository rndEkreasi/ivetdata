<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Product_model');
    }

    public function add(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			if($role == '1'){				
				redirect ('dashboard/clinic');
			}else{
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->dataclinic($profileid);
				$idclinic = $dataclinic[0]->idclinic;

		    	$data = array(
						'title' => 'My New Product - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,				
						'nameclinic' => $dataclinic[0]->nameclinic,
						//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
						'success' => $this->session->flashdata('success'),
						'error' => $this->session->flashdata('error'),
						//'clinicfav' => $this->Home_model->clinicfav(),
						
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addproduct_view',$data);
				$this->load->view('website/footerdash_view',$data);
			}			
		}else{
			redirect ('login');
		}
    }

    public function addpost(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$dataclinic = $this->Home_model->dataclinic($profileid);
			$idclinic = $dataclinic[0]->idclinic;
			$productname = $this->input->post('productname');
			$price = $this->input->post('price');
			$stocks = $this->input->post('stocks');
			$sku = $this->input->post('sku');
			$description = $this->input->post('description');			
			//config photo
			$config['upload_path']   = './upload/product'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('photo')) {
			  	//$featured_imageupdate = $featured_image;
			   	$error_featured = $this->upload->display_errors();
			   	$data = array(
						'title' => 'My New Product - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,				
						'nameclinic' => $dataclinic[0]->nameclinic,
						//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
						'error' => $this->upload->display_errors(),
						//'clinicfav' => $this->Home_model->clinicfav(),
						
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addproduct_view',$data);
				$this->load->view('website/footerdash_view',$data);
			 //   	$this->session->set_flashdata('error', $error_featured);
				// redirect ('product/add');
			}else{
				$result1 = $this->upload->data();
				$filephoto = $result1['file_name'];
				//upload to aws
				$keyname = $filephoto;
				$getpng = FCPATH."upload/product/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$url_photo = $data['upload_data'];
				$datainsert = array(
					'productname' => $productname,
					'price' => $price,
					'stocks' => $stocks,
					'description' => $description,
					'status' => '1',
					'photo' => $url_photo,
					'sku' => $sku,
					'slug' => url_title($productname, 'dash', true),
				);
				$this->db->insert('tbl_product',$datainsert);
				$this->session->set_flashdata('success', 'Congrats, Product already inserted.');
				redirect ('product/add');
			}			
		}else{
			 redirect ('login');
		}
    }

}