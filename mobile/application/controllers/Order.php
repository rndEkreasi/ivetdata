<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		// $this->load->model('Customer_model');
		// $this->load->model('Clinic_model');
		$this->load->model('Order_model');
		$this->load->model('Setting_model');
		//$this->load->model('Login_model');
		//TEST SERVER
        //$params = array('server_key' => 'SB-Mid-server-KZa4C15_jfBKz1JDRH39cioy', 'production' => false);
        // Production server
        $params = array('server_key' => 'Mid-server-9SsIHhSfPMFHs2uWuxBCuTxB', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);

    }

    public function unpaid(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				//var_dump($role);
				if($role == '1'){
					redirect('dashboard/clinic');
				}if($role == '0'){
					redirect('dashboard/petowner');
				}if($role == '3'){
					redirect('dashboard/partner');
				}if($role == '2'){
					$nama = $dataprofile[0]->name;		
					$country = $dataprofile[0]->country;
					$city = $dataprofile[0]->city;
					$photo = $dataprofile[0]->photo;
					if($photo == '' ){
					    $profilephoto = base_url().'assets/images/team/05.jpg';
					}else{
					    $profilephoto = $photo;
					};
					$dataclinic = $this->Home_model->dataclinic($profileid);
					$idclinic = $dataclinic[0]->idclinic;
					//cek data trx
					//pagination
					$unpaid = $this->Order_model->jumlah_data();
					$this->load->library('pagination');
					$config['base_url'] = base_url().'order/unpaid/';
					$config['total_rows'] = $unpaid;
					$config['per_page'] = 10;
					//styling pagination
					$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
				    $config['full_tag_close'] = '</ul>';
				    $config['num_tag_open'] = '<li>';
				    $config['num_tag_close'] = '</li>';
				    $config['cur_tag_open'] = '<li class="active"><a href="#">';
				    $config['cur_tag_close'] = '</a></li>';
				    $config['prev_tag_open'] = '<li>';
				    $config['prev_tag_close'] = '</li>';
				    $config['first_tag_open'] = '<li>';
				    $config['first_tag_close'] = '</li>';
				    $config['last_tag_open'] = '<li>';
				    $config['last_tag_close'] = '</li>';

				    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
				    $config['prev_tag_open'] = '<li>';
				    $config['prev_tag_close'] = '</li>';


				    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
				    $config['next_tag_open'] = '<li>';
				    $config['next_tag_close'] = '</li>';
					$from = $this->uri->segment(3);
					$this->pagination->initialize($config);

					$data = array(
						'title' => 'Data Order - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,'manageto' => $dataprofile[0]->manageto,
						'from' => $from,
						'dataunpaid' => $this->Order_model->data($config['per_page'],$from),
						//'clinicfav' => $this->Home_model->clinicfav(),
						//'countdoctor' => $countdoctor,
						//'countpet' => $countpet,
						//'countorder' => $countorder,
						//'revtoday' => $revtoday,
						//'revyesterday' => $this->Home_model->revyesterday(),
						'totalrev' => $this->Home_model->totalrev(),
						'bulan12' => $this->Home_model->bulan12(),
						'success' => $this->session->flashdata('success'),
						'error' => $this->session->flashdata('error'),
					);
					//$this->load->view('login_user');
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/orderdash_view',$data);
				}
				//$this->load->view('footer_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		

    }


    public function approve(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				//var_dump($role);
				if($role == '1'){
					redirect('dashboard/clinic');
				}if($role == '0'){
					redirect('dashboard/petowner');
				}if($role == '3'){
					redirect('dashboard/partner');
				}if($role == '2'){
					$nama = $dataprofile[0]->name;		
					$country = $dataprofile[0]->country;
					$city = $dataprofile[0]->city;
					$photo = $dataprofile[0]->photo;
					if($photo == '' ){
					    $profilephoto = base_url().'assets/images/team/05.jpg';
					}else{
					    $profilephoto = $photo;
					};
					$dataclinic = $this->Home_model->dataclinic($profileid);
					$idclinic = $dataclinic[0]->idclinic;
					##cek data trx
					$idtrx = $this->input->get('idtrx');					
					$datatrx = $this->Order_model->detailtrx($idtrx);
					$status = $datatrx[0]->status;
					$affid = $datatrx[0]->affid;
					$uiduser = $datatrx[0]->uid;
					$uid = $uiduser;
					$datauser = $this->Order_model->detailprofile($uid);
					$emailuser = $datauser[0]->email;
					$nameuser = $datauser[0]->name;				
					if($status == '1'){
						$this->session->set_flashdata('error', 'This invoice has been approved.');
						redirect ('order/unpaid');
					}else{
						$idservice = $datatrx[0]->idservice;
						$dataservice = $this->Order_model->detailservice($idservice);
						$month = $dataservice[0]->month;
						$expdate = strtotime($datauser[0]->expdate);
						$expdate2 = $dataprofile[0]->expdate;
						// var_dump($month);
						// var_dump($expdate2);
						$today = strtotime(date('Y-m-d'));					

						if($expdate < $today){			 
							$tglendfix = date('Y-m-d',strtotime('+ '.$month.' months',$today));
						}else{
							$tglendfix = date('Y-m-d',strtotime('+ '.$month.' months',$expdate));
						}
						//update uid 
						$dataupdateuser = array(
							'expdate' => $tglendfix,
							'status' => '1',
						);
						$this->db->where('uid',$uiduser);
						$this->db->update('tbl_member',$dataupdateuser);

						//update clinic id
						$dataupdateclinic = array(							
							'status' => '1',
							'affid' => $affid,
						);
						$this->db->where('uid',$uiduser);
						$this->db->update('tbl_clinic',$dataupdateclinic);

						//udate trx
						$updatetrx = array(
							'status' => '1',
							'tglapprove' => date('Y-m-d H:i:s'),
							'approved' => $nama,
						);
						$this->db->where('idtrx',$idtrx);
						$this->db->update('tbl_transaksi',$updatetrx);
						$tglsent = date('d M Y',strtotime($tglendfix));
						//sent email
						$email = $emailuser;
						$subject = 'Congrats, your subscription is active until '.$tglsent;	
						$message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>

							 <tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">

							 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>

							 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$nameuser.', </h1>

							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">Thank you for purchasing subscription iVetdata.com. Your subscription is active until '.$tglsent.'. </p>

							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">

							 <a href="'.base_url().'login" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank""> Login </a> </p>



							 </td> </tr>

							 </tbody> </table> </td> </tr></tbody> </table>';
						$text = $message;
						//$email = $user_email;
					    //$this->load->view('testemail_view');
					    $this->load->library('EmailSender.php');
					    $this->emailsender->send($email,$subject,$text);
						// $data = array(
						// 	'uid' => $uid,
						// 	'dataservice' => $this->login_model->service(),
						// 	'title' => 'Select subscription - Ivetdata.com',		
						// );
						// $this->load->view('website/header_view',$data);
						// $this->load->view('website/service_view',$data);
						// $this->load->view('website/footer_view');
						$this->session->set_flashdata('success', 'Payment already approved.');
						redirect ('order/unpaid');
					}
					


					// $data = array(
					// 	'title' => 'Data Order - iVetdata.com',
					// 	'nama' => $nama,
					// 	'country' => $country,
					// 	'city' => $city,
					// 	'profilephoto' => $profilephoto,
					// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
					// 	'from' => $from,
					// 	'dataunpaid' => $this->Order_model->data($config['per_page'],$from),
					// 	'totalrev' => $this->Home_model->totalrev(),
					// 	'bulan12' => $this->Home_model->bulan12(),
					// );
					// //$this->load->view('login_user');
					// $this->load->view('website/headerdash_view',$data);
					// $this->load->view('website/orderdash_view',$data);
				}
				//$this->load->view('footer_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		

    }

}