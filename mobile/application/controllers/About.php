<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		//$this->load->model('login_model');
		//$this->load->model('Article_model');

    }

	public function index()
	{
	    $this->load->view('website/header_new_view');
		$this->load->view('website/about_new');
		if (isset($this->session->userdata['logged_in'])) {
		    $this->load->view('website/headerdash_view');
		    $this->load->view('website/footer_new_view');
		}else{
		    $this->load->view('website/footer_bar');
		}
	}
}
