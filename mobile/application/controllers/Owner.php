<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Login_model');
		$this->load->model('Clinic_model');
		$this->load->model('Pet_model');
		$this->load->model('Customer_model');
		$this->load->model('Setting_model');
        $this->load->model('Service_model');
        $this->load->model('Shop_model');
        $this->load->model('Sales_model');
    }


    /**
     * @return object
     */
    public function index(){
        if (isset($this->session->userdata['logged_in'])) {
            $profileid  = ($this->session->userdata['logged_in']['id']);
            $dataprofile = $this->Home_model->detailprofile($profileid);
            $role = $dataprofile[0]->role;          
            $nama = $dataprofile[0]->name;      
            $country = $dataprofile[0]->country;
            $city = $dataprofile[0]->city;
            $photo = $dataprofile[0]->photo;
            if($photo == '' ){
                $profilephoto = base_url().'assets/images/team/05.jpg';
            }else{
                $profilephoto = $photo;
            };
            // $dataclinic = $this->Home_model->dataclinic($profileid);
            // $idclinic = $dataclinic[0]->idclinic;
            //pagination
            $all_sales = $this->Shop_model->myjumlah_data($profileid);
            //var_dump($profileid);
            $this->load->library('pagination');
            $config['base_url'] = base_url().'sales/myinvoice/';
            $config['total_rows'] = $all_sales;
            $config['per_page'] = 10;
            //styling pagination
            $config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="#">';
            $config['cur_tag_close'] = '</a></li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';

            $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';


            $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $from = $this->uri->segment(3);
            $this->pagination->initialize($config); 
            $data = array(
                'title' => 'Invoice list - iVetdata.com',
                'nama' => $nama,
                'country' => $country,
                'city' => $city,
                'profilephoto' => $profilephoto,
                'role' => $role,
                'manageto' => $dataprofile[0]->manageto,
                'from' => $from,                
                //'nameclinic' => $dataclinic[0]->nameclinic,
                'all_sales' => $all_sales,
                'datasales' => $this->Shop_model->mydata($config['per_page'],$from,$profileid),
                'success' => $this->session->flashdata('success'),
                'error' => $this->session->flashdata('error'),
                //'clinicfav' => $this->Home_model->clinicfav(),
                
            );
            $this->load->view('website/header_new_view');
            //$this->load->view('website/landing_new_view',$data);
            $this->load->view('website/owner-invoice',$data);
            //$this->load->view('website/testimonial_new');
            //$this->load->view('website/latest_news');
            //$this->load->view('website/stat_new');
            //$this->load->view('website/pre_footer');
            $this->load->view('website/headerdash_view');
            $this->load->view('website/footer_new_view');                                                  
        }else{
             redirect ('login');
        }
        
    }
    
    public function detailinvoice(){
        if (isset($this->session->userdata['logged_in'])) {
            $profileid  = ($this->session->userdata['logged_in']['id']);
            $dataprofile = $this->Home_model->detailprofile($profileid);
            $role = $dataprofile[0]->role;
            $nama = $dataprofile[0]->name;      
            $country = $dataprofile[0]->country;
            $city = $dataprofile[0]->city;
            $photo = $dataprofile[0]->photo;
            if($photo == '' ){
                $profilephoto = base_url().'assets/images/team/05.jpg';
            }else{
                $profilephoto = $photo;
            };
            $invoice = $this->input->get('invoice');
            $datainvoice = $this->Sales_model->detailinv($invoice); 
            //var_dump($datainvoice);
            $uidinvoice = $datainvoice[0]->uid;
            if($uidinvoice == $profileid){
                $totalqty = $datainvoice[0]->totalqty;
                $iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
                $data = array(
                    'title' => 'Detail Invoice - iVetdata.com',
                    'nama' => $nama,
                    'country' => $country,
                    'city' => $city,
                    'profilephoto' => $profilephoto,
                    'role' => $role,    
                    'success' => $this->session->flashdata('success'),
                    'datainvoice' => $datainvoice,
                    'iteminvoice' => $iteminvoice,
                                //'clinicfav' => $this->Home_model->clinicfav(),                    
                    );
                    $this->load->view('website/header_new_view');
                    //$this->load->view('website/landing_new_view',$data);
                    $this->load->view('website/owner-invoice-detail',$data);
                    $this->load->view('website/headerdash_view');
                    $this->load->view('website/footer_new_view');
            }else{
                //var_dump($datainvoice);
                //$this->session->set_flashdata('error', 'Sorry the invoice not for you.');
                redirect ('owner');
            }        
        }else{
             redirect ('login');
        }

    }

    public function deleteinvoice(){
        if (isset($this->session->userdata['logged_in'])) {
            $profileid  = ($this->session->userdata['logged_in']['id']);
            $dataprofile = $this->Home_model->detailprofile($profileid);
            $role = $dataprofile[0]->role;
            $nama = $dataprofile[0]->name;      
            $country = $dataprofile[0]->country;
            $city = $dataprofile[0]->city;
            $photo = $dataprofile[0]->photo;
            if($photo == '' ){
                $profilephoto = base_url().'assets/images/team/05.jpg';
            }else{
                $profilephoto = $photo;
            };
            $invoice = $this->input->get('invoice');
            $datainvoice = $this->Sales_model->detailinv($invoice); 
            //var_dump($datainvoice);
            $uidinvoice = $datainvoice[0]->uid;
            $idinv = $datainvoice[0]->idinv;
            if($uidinvoice == $profileid){
                $dataupdateinv = array(
                    'status' => '2',
                );
                $this->db->where('idinv',$idinv);
                $this->db->delete('tbl_invoice');
                $this->session->set_flashdata('success', 'Invoice has been deleted.');
                redirect ('owner');

                // $totalqty = $datainvoice[0]->totalqty;
                // $iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
                // $data = array(
                //     'title' => 'Detail Invoice - iVetdata.com',
                //     'nama' => $nama,
                //     'country' => $country,
                //     'city' => $city,
                //     'profilephoto' => $profilephoto,
                //     'role' => $role,    
                //     'success' => $this->session->flashdata('success'),
                //     'datainvoice' => $datainvoice,
                //     'iteminvoice' => $iteminvoice,
                //                 //'clinicfav' => $this->Home_model->clinicfav(),                    
                //     );
                //     $this->load->view('website/header_new_view');
                //     //$this->load->view('website/landing_new_view',$data);
                //     $this->load->view('website/owner-invoice-detail',$data);
                //     $this->load->view('website/headerdash_view');
                //     $this->load->view('website/footer_new_view');
            }else{
                //var_dump($datainvoice);
                //$this->session->set_flashdata('error', 'Sorry the invoice not for you.');
                redirect ('owner');
            }        
        }else{
             redirect ('login');
        }

    }

    public function editProfile(){
        if (isset($this->session->userdata['logged_in'])) {
            $profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$manageto = $dataprofile[0]->manageto;
			$nama = $dataprofile[0]->name;		
			$phone = $dataprofile[0]->phone;
			$address = $dataprofile[0]->address;
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'images/avatar.png';
			}else{
			    $profilephoto = $photo;
			};
			if($role != '0'){
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);	
				$logoclinic = $dataclinic[0]->picture;
				if($logoclinic == '' ){
				    $logoclinicfix = base_url().'assets/images/team/06.jpg';
				}else{
				    $logoclinicfix = $logoclinic;
				};
				$nameclinic = $dataclinic[0]->nameclinic;
				$addressclinic = $dataclinic[0]->address;
				$phoneclinic = $dataclinic[0]->phone;
				$cityclinic = $dataclinic[0]->city;
				$countryclinic = $dataclinic[0]->country;
				$latclinic = $dataclinic[0]->lat;
				$longclinic = $dataclinic[0]->lang;
				$postalcode = $dataclinic[0]->postalcode;
			}else{
				$idclinic = '';
				$logoclinicfix = '';
				$nameclinic = '';
				$addressclinic = '';
				$phoneclinic = '';
				$cityclinic = '';
				$countryclinic = '';
				$latclinic = '';
				$longclinic = '';
				$postalcode = '';
			}
				
			//$clinicname = $dataclinic[0]->nameclinic;
			if($manageto == '1'){
				$datavet = $this->Home_model->datadokter($profileid);
				$licenseid = $datavet[0]->idlicense;
				$license = $datavet[0]->license;
				$kta = $datavet[0]->kta;
				$sip = $datavet[0]->sip;
				$public =  $dataclinic[0]->public_info;
			}else{
				$licenseid = '0';
				$license = '0';
				$kta = '0';
				$sip = '0';
				$public = $dataprofile[0]->public_info;
			}
			
			$data = array(
				'title' => 'Edit My Profile - iVetdata.com',
				'nama' => $nama,
				'phone' => $phone,
				'email' =>  $dataprofile[0]->email,
				'homeaddress' => $address,
				'country' => $country,
				'city' => $city,
				'license' => $license,
				'licenseid' => $licenseid,
				'kta' => $kta,
				'sip' => $sip,
				'logoclinic' => $logoclinicfix,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $manageto,				
				'nameclinic' => $nameclinic,
				'idclinic' => $idclinic,
				'address' => $addressclinic,
				'phoneclinic' => $phoneclinic,
				'cityclinic' => $cityclinic,
				'countryclinic' => $countryclinic,
				'lat' => $latclinic,
				'long' => $longclinic,
				'postalcode' => $postalcode,
				'dataprofile' => $dataprofile,
				'public_info' => $public,
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
			);
            $this->load->view('website/header_new_view');
            //$this->load->view('website/landing_new_view',$data);
            $this->load->view('website/owner-editprofile',$data);
            //$this->load->view('website/testimonial_new');
            //$this->load->view('website/latest_news');
            //$this->load->view('website/stat_new');
            //$this->load->view('website/pre_footer');
            $this->load->view('website/headerdash_view');
            $this->load->view('website/footer_new_view');
        }else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect(base_url());
		}
    }

    /**
     * @return object
     */
    public function vetSearch()
    {
        if(isset($_GET['category'])){
            $cat = $this->input->get('category');
            if(isset($_GET['search'])){
                $combo = $this->input->get('search');
            }else{
                $combo = "";
            }
            $searchvet = $this->Clinic_model->searchcat($cat,$combo);
            if(count($searchvet)>0){
	            $message = "";
	        }else{
	            $searchvet = false;
	            $message = "Oops, Data not found";
	        }
        }elseif(isset($_POST['name'])||isset($_POST['vet'])||isset($_POST['city'])||isset($_POST['combo'])){
            $id = $this->input->post('name');
    		$name = $this->input->post('vet');
    		$city = $this->input->post('city');
    		if(isset($_POST['combo'])){
    		    $name = $this->input->post('combo');
    		    $city = $this->input->post('combo');
    		}
    		if(strlen($name)>30||strlen($city)>30)die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
    		if($id<>""&&is_numeric($id)){
    		    echo "<script>self.location.href='/pet/search?idpet=".$id."';</script><h2>Please enable javascript</h2>";
    		}
    		if($city == ''){
    		    if($name==''){
    			    $searchvet = false;
    			    $message = "Oops, Data not found";
    		    }else{
    		        $searchvet = $this->Clinic_model->searchname($name);
    		        if(count($searchvet)>0){
    		            $message = "";
    		        }else{
    		           $searchvet = false; 
    		            $message = "Oops, Data not found";
    		        }
    		    }
    		}else{
    		    $searchvet = $this->Clinic_model->search($name,$city);
    		    if(count($searchvet)>0){
    	            $message = "";
    	        }else{
    	            $searchvet = false;
    	            $message = "Oops, Data not found";
    	        }
    		}
        }elseif(isset($_GET['vets'])){
                $name = $this->input->get('vets');
                $searchvet = $this->Clinic_model->searchid($name);
    		    if(count($searchvet)>0){
    	            $message = "";
    	        }else{
    	            $searchvet = false;
    	            $message = "Oops, Data not found";
    	        }
        }else{
            $searchvet = false;
            $message = "";
        }
        $dt_cat = $this->Clinic_model->listcat();
		$data = array(
    			'title' => 'Vets List - iVetdata.com',
    			'message' => $message,
    			'searchvet' => $searchvet,
    			'categories' => $dt_cat,
    			'page' => 'vets',
		);
        header("Cache-Control: max-age=3000, must-revalidate"); 
        $this->load->view('website/header_new_view');
        $this->load->view('website/owner-vetsearch',$data);
        if (isset($this->session->userdata['logged_in'])) {
            $this->load->view('website/headerdash_view');
            $this->load->view('website/footer_new_view');
        }else{
            $this->load->view('website/footer_new_view');
        }
    }
}