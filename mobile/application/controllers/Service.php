<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Service_model');
		$this->load->model('Setting_model');
    }

    public function index(){
    	$data = array(
			'title' => 'Service - iVetdata.com',
			
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_view',$data);
		$this->load->view('website/servicefront_view',$data);
		$this->load->view('website/footer_view',$data);
    }

    public function all(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
	    	if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);
				//pagination
				$all_myservice = $this->Service_model->jumlah_data($idclinic,$role);
				$this->load->library('pagination');
				$config['base_url'] = base_url().'service/all/';
				$config['total_rows'] = $all_myservice;
				$config['per_page'] = 10;
				//styling pagination
				$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			    $config['full_tag_close'] = '</ul>';
			    $config['num_tag_open'] = '<li>';
			    $config['num_tag_close'] = '</li>';
			    $config['cur_tag_open'] = '<li class="active"><a href="#">';
			    $config['cur_tag_close'] = '</a></li>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';
			    $config['first_tag_open'] = '<li>';
			    $config['first_tag_close'] = '</li>';
			    $config['last_tag_open'] = '<li>';
			    $config['last_tag_close'] = '</li>';

			    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';


			    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    $config['next_tag_open'] = '<li>';
			    $config['next_tag_close'] = '</li>';

				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);	

				//ajax
				$countries = $this->Service_model->get_list_countries($idclinic);

				$opt = array('' => 'Category');
				foreach ($countries as $country) {
					$opt[$country] = $country;
				}
				$data = array(
					'title' => 'My Service - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'from' => $from,
					'profilephoto' => $profilephoto,
					'role' => $role,	
					'manageto' => $dataprofile[0]->manageto,			
					'nameclinic' => $dataclinic[0]->nameclinic,
					'dataservice' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					//ajax
					'form_country' => form_dropdown('',$opt,'','id="cat" class="form-control"'),
					
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/myservice_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}else{

				 redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }

    public function search(){
    		$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$nameservice = $this->input->post('nameservice');
			//pagination
			$all_myservice = $this->Service_model->searchservice($nameservice,$idclinic);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'service/all/';
			$config['total_rows'] = count($all_myservice);
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	
			$data = array(
				'title' => 'Customer - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'from' => $from,
				'manageto' => $dataprofile[0]->manageto,				
				'nameclinic' => $dataclinic[0]->nameclinic,
				//'all_customer' => $all_customer,
				'dataservice' => $this->Service_model->searchservice($nameservice,$idclinic),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				'pagination' => 'no',
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/myservice_view',$data);
    }


    public function invoice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);

	    	$data = array(
					'title' => 'Create Invoice - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,	
					'manageto' => $dataprofile[0]->manageto,			
					'nameclinic' => $dataclinic[0]->nameclinic,
					//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
					'success' => $this->session->flashdata('success'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/invoiceclient_view',$data);
			$this->load->view('website/footerdash_view',$data);
		}else{
			 redirect ('login');
		}
    }

    public function add(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);

	    	$data = array(
					'title' => 'My New Service - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,	
					'manageto' => $dataprofile[0]->manageto,			
					'nameclinic' => $dataclinic[0]->nameclinic,
					//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
					'success' => $this->session->flashdata('success'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/addclinicservice_view',$data);
			$this->load->view('website/footerdash_view',$data);
		}else{
			 redirect ('login');
		}
    }

    public function addprocess(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$nameservice = $this->input->post('nameservice');
			$price = $this->input->post('price');
			$origprice = $this->input->post('origprice');
			$qty = $this->input->post('stocks');
			$cat = $this->input->post('cat');
			$unit = $this->input->post('unit');
			$cekservice = $this->Service_model->cekservice($nameservice,$idclinic);
			if(count($cekservice) > '0'){
				$idservice = $cekservice[0]->id;
				$this->session->set_flashdata('error', 'Sorry, '.$nameservice.' already inserted in your database please edit here');
				redirect ('service/edit/?idservice='.$idservice);
			}else{
				$datainsert = array(
					'nameservice' => $nameservice,
					'price' => intval(preg_replace('/[^\d.]/', '', $price)),
					'cat' => $cat,
					'idclinic' => $idclinic,
					'addby' => $nama,
					'origprice' => intval(preg_replace('/[^\d.]/', '', $origprice)),
					'qty' => intval(preg_replace('/[^\d.]/', '', $qty)),
					'unit' => $unit,
				);
				$this->db->insert('tbl_clinicservice',$datainsert);
				$this->session->set_flashdata('success', 'New medicine has been added successfully.');
				redirect ('service/all');
			}
			
		}else{
			 redirect ('login');
		}
    }


    public function addservice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$nameservice = $this->input->post('nameservice');
			$price = $this->input->post('price');
			$origprice = $this->input->post('origprice');
			$fee = $this->input->post('fee');
			$cat = $this->input->post('cat');
			
			$cekservice = $this->Service_model->cekservice($nameservice,$idclinic);
			if(count($cekservice) > '0'){
				$idservice = $cekservice[0]->id;
				$this->session->set_flashdata('error', 'Sorry, '.$nameservice.' already inserted in your database please edit here');
				redirect ('service/edit/?idservice='.$idservice);
			}else{
				$datainsert = array(
					'nameservice' => $nameservice,
					'price' => intval(preg_replace('/[^\d.]/', '', $price)),
					'cat' => $cat,
					'idclinic' => $idclinic,
					'addby' => $nama,
					'origprice' => intval(preg_replace('/[^\d.]/', '', $origprice)),
					'fee' => intval(preg_replace('/[^\d.]/', '', $fee)),
					
				);
				$this->db->insert('tbl_clinicservice',$datainsert);
				$this->session->set_flashdata('success', 'New service has been added successfully.');
				redirect ('service/all');
			}
			
		}else{
			 redirect ('login');
		}
    }

    public function edit(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$idservice = $this->input->get('idservice');
			$dataservice = $this->Service_model->detailservice($idservice);


	    	$data = array(
					'title' => 'My New Service - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,			
					'nameclinic' => $dataclinic[0]->nameclinic,
					'servicename' => $dataservice[0]->nameservice,
					'serviceprice' =>  $dataservice[0]->price,
					'origprice' => $dataservice[0]->origprice,
					'qty' => $dataservice[0]->qty,
					'cat' => $dataservice[0]->cat,
					'fee' => $dataservice[0]->fee,
					'unit'=> $dataservice[0]->unit,
					'idservice' => $idservice,
					//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/editclinicservice_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			 redirect ('login');
		}
    }

    public function editpost(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$nameservice = $this->input->post('servicename');
			$price = $this->input->post('serviceprice');
			$idservice = $this->input->post('idservice');
			$origprice = $this->input->post('origprice');
			$qty = $this->input->post('stocks');
			$fee = $this->input->post('fee');
			$unit =$this->input->post('unit');
			$dataservice = $this->Service_model->detailservice($idservice);
			// $idclinicservice = $dataservice[0]->idclinic;
			// if($idclinic != $idclinicservice){
			// 	$this->session->set_flashdata('error', 'You did not allowed to change.');
			// 	redirect ('service/all');
			// }else{
				// $cat = $this->input->post('cat');
				$dataupdate = array(
					'nameservice' => $nameservice,
					'price' =>intval(preg_replace('/[^\d.]/', '', $price)),		
					'origprice' => intval(preg_replace('/[^\d.]/', '', $origprice)),
					'qty' => intval(preg_replace('/[^\d.]/', '', $qty)),
					'fee' => intval(preg_replace('/[^\d.]/', '', $fee)),
					'unit' => $unit,		
					//'idclinic' => $idclinic,
					//'addby' => $nama,
				);
				$this->db->where('id',$idservice);
				$this->db->update('tbl_clinicservice',$dataupdate);
				$this->session->set_flashdata('success', 'Data has been updated successfully');
				redirect ('service/all');

			//}			
		}else{
			 redirect ('login');
		}
    }


    public function delete(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$idservice = $this->input->get('idservice');
			$dataservice = $this->Service_model->detailservice($idservice);
			$idclinicservice = $dataservice[0]->idclinic;
			$cat = $dataservice[0]->cat;
			if($idclinic != $idclinicservice){
				$this->session->set_flashdata('error', 'You did not allowed to delete.');
				redirect ('service/all');
			}else{
				// $cat = $this->input->post('cat');
				// $dataupdate = array(
				// 	'nameservice' => $nameservice,
				// 	'price' => $price,
				// );
				$this->db->where('id',$idservice);
				$this->db->delete('tbl_clinicservice');
				$this->session->set_flashdata('success', 'Data has been deleted successfully.');
				redirect ('service/all');

			}			
		}else{
			 redirect ('login');
		}
    }

    function allsearch(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
	        if (isset($_GET['term'])) {
	            $result = $this->Service_model->search_blog($_GET['term'],$idclinic);
	            if (count($result) > 0) {
		             foreach ($result as $row)
		              $arr_result[] = array(
		                    'label'         => $row->nameservice,
		                    'description'   => $row->price,
		             );
	                echo json_encode($arr_result);
	            }
	        }
        }else{
			 redirect ('login');
		}
    }

    public function ajax_list(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$manageto = $dataprofile[0]->manageto;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$list = $this->Service_model->get_datatables($idclinic);
			$data = array();
			if($this->input->post('order[0][dir]')=="asc"){
			    $no = $_POST['start'];
			}else{
			    $no = $this->Service_model->count_all($idclinic);
			    $no = $no - $_POST['start'] + 1;
			}
			foreach ($list as $customers) {
			    if($this->input->post('order[0][dir]')=="asc"){
			        $no++;
			    }else{
				    $no--;
			    }
				$action = '<div style="white-space: nowrap;display: inline-block;"><a href="'.base_url().'service/edit/?idservice='.$customers->id.'" class="icon-tab theme_button color2">Edit</a>&nbsp;<button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deleteservice'.$customers->id.'"> Delete </button></div>
				<div class="modal fade" id="deleteservice'.$customers->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Delete '.$customers->nameservice.' </h5><br>
                                                    Are you sure?<br>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>

                                                  </div>
                                                  <div class="modal-footer">                                                  	
                                                    <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                    <a href="'.base_url().'service/delete/?idservice='.$customers->id.'" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                  </div>
                                                </div>
                                              </div>
                                            </div>


				';
				$qty = $customers->qty;
				if($qty<=0){
					$qtyfix = 0;
				}else{
					$qtyfix = $qty;
				}
				$row = array();
				$row[] = $no;
				$row[] = $customers->nameservice;
				$row[] = 'Rp. '.number_format($customers->price);
				$row[] = $customers->cat;
				$row[] = $qtyfix;
				$row[] = $customers->unit;
                //$row[] = $customers->id;
				//$row[] = $customers->status;
				if($manageto = array('1','3'))$row[] = $action;
				// $row[] = $customers->city;
				// $row[] = $customers->country;
				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->Service_model->count_all($idclinic),
							"recordsFiltered" => $this->Service_model->count_filtered($idclinic),
							"data" => $data,
					);

			//output to json format
			echo json_encode($output);
		}else{
			 redirect ('login');
		}
	}
	
	public function exportservicelist(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
	    	if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$profilephoto = $dataprofile[0]->photo;
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);

				//pagination
				$all_myservice = $this->Service_model->jumlah_data($idclinic,$role);

				$data = array(
					'title' => 'My Service - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,	
					'manageto' => $dataprofile[0]->manageto,			
					'nameclinic' => $dataclinic[0]->nameclinic,
					'all_data' => $all_myservice,
					'dataservice' => $this->Service_model->data($all_myservice,0,$idclinic,$role),					
				);
				$this->load->view('website/export_service',$data);
			}else{

				 redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }
    
}