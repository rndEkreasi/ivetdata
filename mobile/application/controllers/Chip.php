<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chip extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');

    }

    public function add(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;	
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};		
			$data = array(
				'title' => 'Add Chip - iVetdata.com',
				'nama' => $nama,
				'profilephoto' => $profilephoto,
				'role' => $role,
				
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/addchip_view',$data);
			//$this->load->view('footer_view',$data);
		}else{

			 redirect ('welcome');
		}
    }

}