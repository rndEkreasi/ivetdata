<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');
		$this->load->model('Article_model');

    }
    
	public function index()
	{
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$dataclinic = $this->Home_model->dataclinic($profileid);
			$idclinic = $dataclinic[0]->idclinic;
			$cekdoctor = $this->Home_model->cekdoctor($idclinic,$role);
			$countpet = count($this->Home_model->pethomedash($idclinic,$role));
			$countorder = count($this->Home_model->orderdash($idclinic,$role));
			$revtoday = $this->Home_model->revtoday();
			$countdoctor = count($cekdoctor);
			$data = array(
				'title' => 'Home - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'role' => $role,
				'profilephoto' => $profilephoto,
				'datapet' => $this->Home_model->pethome(),
				'clinicfav' => $this->Home_model->clinicfav(),
				'countdoctor' => $countdoctor,
				'countpet' => $countpet,
				'countorder' => $countorder,
				'revtoday' => $revtoday,
				'revyesterday' => $this->Home_model->revyesterday(),
				'totalrev' => $this->Home_model->totalrev(),
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/dashboard_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			redirect ('login');
		}		
	}

	public function all(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			//pagination
			$all_article = $this->Article_model->jumlah_data();
			$this->load->library('pagination');
			$config['base_url'] = base_url().'article/all/';
			$config['total_rows'] = $all_article;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);		

			$data = array(
				'title' => 'All Article - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,	
				'allarticle' => $this->Article_model->data($config['per_page'],$from),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/allarticle_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			$this->session->set_flashdata('error', 'Please login');
			redirect ('login');
		}	
	}

	public function add(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;		
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};	
			$data = array(
				'title' => 'Add New Article - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,				
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/addarticle_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			$this->session->set_flashdata('error', 'Please login');
			redirect ('login');
		}	
	}

	public function postarticle(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;	
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$title = $this->input->post('title');
			$description = $this->input->post('description');
			//upload featuredimge
			$config['upload_path']   = './upload/article'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 1500; 
			// $config['max_width']     = 1800; 
			// $config['max_height']    = 1500;
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('featuredimg')) {
				$featuredimg = 'default-article.png';
				$urlimage = 'default-article.png';
			}else{
				$result1 = $this->upload->data();
		   		$featuredimg = $result1['file_name'];
		   		$keyname = $featuredimg;
				$getpng = FCPATH."upload/article/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$urlimage = $data['upload_data'];
			}
			$datainsert = array(
				'title' => $title,
				'slug' => url_title($title, 'dash', true),
				'description' => $description,
				'featuredimg' => $urlimage,
				'creator' => $nama,
				'publishdate' => date('Y-m-d H:i:s'),
			);
			$this->db->insert('tbl_article',$datainsert);
			$data = array(
				'title' => 'Add New Article - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'success' => 'Congrats, Article has been submited'				
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/addarticle_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			$this->session->set_flashdata('error', 'Please login');
			redirect ('login');
		}
	}

	public function edit(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;		
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};	
			$idarticle = $this->input->get('id');			
			$data = array(
				'title' => 'Edit Article - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'dataarticle' => $this->Article_model->detailarticle($idarticle),

			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/editarticle_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			$this->session->set_flashdata('error', 'Please login');
			redirect ('login');
		}	
	}

	public function updatearticle(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;	
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$title = $this->input->post('title');
			$description = $this->input->post('description');
			$idarticle = $this->input->post('idarticle');
			//upload featuredimge
			$config['upload_path']   = './upload/article'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 1500; 
			// $config['max_width']     = 1800; 
			// $config['max_height']    = 1500;
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('featuredimg')) {
				$featuredimg = 'default-article.png';
				$urlimage = 'default-article.png';
				$dataupdate = array(
					'title' => $title,
					'slug' => url_title($title, 'dash', true),
					'description' => $description,
					'creator' => $nama,
					'publishdate' => date('Y-m-d H:i:s'),
				);
				$this->db->where('idarticle',$idarticle);
				$this->db->update('tbl_article',$dataupdate);
			}else{
				$result1 = $this->upload->data();
		   		$featuredimg = $result1['file_name'];
		   		$keyname = $featuredimg;
				$getpng = FCPATH."upload/article/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$urlimage = $data['upload_data'];
				$dataupdate = array(
					'title' => $title,
					'slug' => url_title($title, 'dash', true),
					'description' => $description,
					'featuredimg' => $urlimage,
					'creator' => $nama,
					'publishdate' => date('Y-m-d H:i:s'),
				);
				$this->db->where('idarticle',$idarticle);
				$this->db->update('tbl_article',$dataupdate);
			}
			$this->session->set_flashdata('success', 'Congrats, Article has been updated');
			redirect ('article/all');
			
			// $data = array(
			// 	'title' => 'Update Article - iVetdata.com',
			// 	'nama' => $nama,
			// 	'country' => $country,
			// 	'city' => $city,
			// 	'profilephoto' => $profilephoto,
			// 	'role' => $role,
			// 	'success' => 'Congrats, Article has been updated'				
			// );
			// //$this->load->view('login_user');
			// $this->load->view('website/headerdash_view',$data);
			// $this->load->view('website/addarticle_view',$data);
			// //$this->load->view('footer_view',$data);
		}else{
			$this->session->set_flashdata('error', 'Please login');
			redirect ('login');
		}
	}
}