<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');
		$this->load->model('Clinic_model');
		$this->load->model('Reminder_model');

    }

    public function vaccine(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			
			//pagination
			$remindervacc = $this->Reminder_model->remindervacc($idclinic,$role);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'reminder/vaccine/';
			$config['total_rows'] = $remindervacc;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	
            
            //pagination 2
			$remindermed = $this->Reminder_model->remindermed($idclinic,$role);
			$config2['base_url'] = base_url().'reminder/vaccine/';
			$config2['total_rows'] = $remindermed;
			$config2['per_page'] = 10;
			//styling pagination
			$config2['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config2['full_tag_close'] = '</ul>';
		    $config2['num_tag_open'] = '<li>';
		    $config2['num_tag_close'] = '</li>';
		    $config2['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config2['cur_tag_close'] = '</a></li>';
		    $config2['prev_tag_open'] = '<li>';
		    $config2['prev_tag_close'] = '</li>';
		    $config2['first_tag_open'] = '<li>';
		    $config2['first_tag_close'] = '</li>';
		    $config2['last_tag_open'] = '<li>';
		    $config2['last_tag_close'] = '</li>';

		    $config2['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config2['prev_tag_open'] = '<li>';
		    $config2['prev_tag_close'] = '</li>';


		    $config2['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config2['next_tag_open'] = '<li>';
		    $config2['next_tag_close'] = '</li>';

			$from2 = $this->uri->segment(3);
			$this->pagination->initialize($config2);	
			
			$data = array(
				'title' => 'Reminder & Schedule - iVetdata.com',
				'from' => $from,
				'nama' => $nama,
				'country' => $country,
				'all_clinic' => $remindervacc,
				'idclinic' => $idclinic,
				'city' => $city,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'text' => 'Appointment Schedule',
				'profilephoto' => $profilephoto,
				'remindervacc' => $this->Reminder_model->datavaccine($config['per_page'],$from,$idclinic,$role),
				'success' => $this->session->flashdata('success'),
				'text2' => 'Consultation Reminder',
				'from2' => $from2,
				'all_clinic2' => $remindermed,
				'remindermed' => $this->Reminder_model->datamed($config2['per_page'],$from2,$idclinic,$role),
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/reminder_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{

			 redirect ('welcome');
		}
    }

    public function export(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$profilephoto = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			
			//pagination
			$remindervacc = $this->Reminder_model->remindervacc($idclinic,$role);
			$from = 0;

			$data = array(
				'title' => 'Appointment Schedule - iVetdata.com',
				'from' => $from,
				'nama' => $nama,
				'country' => $country,
				'all_clinic' => $remindervacc,
				'city' => $city,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'text' => 'Vaccine Reminder',
				'profilephoto' => $profilephoto,
				'remindervacc' => $this->Reminder_model->datavaccine($remindervacc,$from,$idclinic,$role),
				'success' => $this->session->flashdata('success'),
			);

			$this->load->view('website/export_reminder',$data);

		}else{

			 redirect ('welcome');
		}
    }
    
    public function exportmed(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$profilephoto = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			
			//pagination
			$remindervacc = $this->Reminder_model->remindermed($idclinic,$role);
			$from = 0;

			$data = array(
				'title' => 'Medical Reminder - iVetdata.com',
				'from' => $from,
				'nama' => $nama,
				'country' => $country,
				'all_clinic' => $remindervacc,
				'city' => $city,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'text' => 'Vaccine Reminder',
				'profilephoto' => $profilephoto,
				'remindervacc' => $this->Reminder_model->datamed($remindervacc,$from,$idclinic,$role),
				'success' => $this->session->flashdata('success'),
			);

			$this->load->view('website/export_reminder2',$data);

		}else{

			 redirect ('welcome');
		}
    }
    
    public function delete(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			//dataheader
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
            $idvacc = $this->input->get('id');
			$this->db->where('idvacc',$idvacc);
			$this->db->update('tbl_vaccine',array('status'=>'2'));
			$this->session->set_flashdata('success', 'Congrats, Data has been deleted.');
			// After that you need to used redirect function instead of load view such as 
			redirect("reminder/vaccine");
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}			
    }

    
}