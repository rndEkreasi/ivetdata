<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct();
         $this->load->database(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Shop_model');
		$this->load->model('Team_model');
		$this->load->model('Customer_model');
		$this->load->model('Clinic_model');
		$this->load->model('Pet_model');
		$this->load->model('Setting_model');
		$this->load->model('Login_model');
		$this->load->model('Sales_model');
		$this->load->model('Service_model');
		$this->load->model('Appointment_model');	
		// $this->load->model('Reminder_model');
		// //TEST SERVER
  //       $params = array('server_key' => 'SB-Mid-server-KZa4C15_jfBKz1JDRH39cioy', 'production' => false);
  //       // Production server
  //       //$params = array('server_key' => 'Mid-server-9SsIHhSfPMFHs2uWuxBCuTxB', 'production' => true);
		// $this->load->library('veritrans');
		// $this->veritrans->config($params);

    }

    public function index(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);
				$clinicshop = $this->Shop_model->clinicshop();
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe

					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,
						'dataappoint' => $this->Appointment_model->myappoint($profileid),
						'success' => $this->session->flashdata('success'),
						'error' => $this->session->flashdata('error'),
					);
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/dashboard-owner-appointments',$data);
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/footer_new_view',$data);

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }

    public function edit($idvacc){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe
					$dataappoint = $this->Appointment_model->detailappoint($idvacc);
					$idclinic = $dataappoint[0]->idclinic;
					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,
						'dataappoint' => $dataappoint,
						//'idclinic' => $idclinic,
						'listvet' => $this->Appointment_model->listvet($idclinic),
					);
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/dashboard-owner-appointments-edit',$data);
					$this->load->view('website/headerdash_view');
		    		$this->load->view('website/footer_new_view');

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }

    public function editprocess(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idvet = $this->input->post('idvet');
				$nextdate = $this->input->post('nextdate')." ".$this->input->post('hour').":".$this->input->post('minute').":00";
				$description = $this->input->post('description');
				$idvacc = $this->input->post('idvacc');
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				$dataappoint = $this->Appointment_model->detailappoint($idvacc);
				$idowner = $dataappoint[0]->idowner;
				// var_dump($idowner);
				// var_dump($profileid);
 				if ($idowner == $profileid) {
 					$dataupdate = array(
 						'idvet' => $idvet,
 						'nextdate' => $nextdate,
 						'description' => $description,
 					);
 					$this->db->where('idvacc',$idvacc);
 					$this->db->update('tbl_vaccine',$dataupdate);
 					$this->session->set_flashdata('success', 'Appointment has been updated.');
					redirect ('appointment');

				}else{
					$this->session->set_flashdata('error', 'You cannot update this appointment.');
					redirect ('appointment');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }

    public function detail($idvacc){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe

					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,
						'dataappoint' => $this->Appointment_model->detailappoint($idvacc),
					);
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/dashboard-owner-appointments-detail',$data);
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/footer_new_view',$data);

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }

    public function add(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				
				$clinicshop = $this->Shop_model->clinicshop();
				$dataclinic = $this->Home_model->datacustomer($profileid);
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe
					$idclinic = $this->input->get('idclinic');
					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						//'manageto' => $dataprofile[0]->manageto,
						'datapet' => $this->Appointment_model->listpet($email),
						'idclinic' => $idclinic,
						'dataclinic' => $dataclinic,
						'listvet' => $this->Appointment_model->listvet($idclinic),
					);
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/dashboard-owner-appointments-add',$data);
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/footer_new_view',$data);

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect (base_url());
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect (base_rul());
			}
		}else{
			redirect ('maintenance');
		}
    }


    public function addprocess(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$address = $dataprofile[0]->address;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				if ($role == '0') {
				    $idclinic = $this->input->post('idclinic');
					$idpet = $this->input->post('idpet');
					$idvet = $this->input->post('idvet');
					if($idclinic==0||$idclinic=="")die("<html><body><script>alert('Error : please choose clinic');history.back(1);</script><h2>Redirecting ...</h2></body></html>");
					if($idpet==0||$idpet=="")die("<html><body><script>alert('Error : please choose pet');history.back(1);</script><h2>Redirecting ...</h2></body></html>");
                    if($idvet==0||$idvet=="")die("<html><body><script>alert('Error : please choose vet');history.back(1);</script><h2>Redirecting ...</h2></body></html>");
					$nextdate = $this->input->post('nextdate')." ".$this->input->post('hour').":".$this->input->post('minute').":00";
					$description = $this->input->post('description');
					$idclinic = $this->input->post('idclinic');
					$datainsert = array(
						'idpet' => $idpet,
						'nextdate' => $nextdate,
						'description' => $description,
						'idclinic' => $idclinic,
						'idowner' => $profileid,
						'nameowner' => $nama,
						'emailowner' => $email,
						'phoneowner' => $phone,
						'category' => 'medical',
						'datevacc' => date('Y-m-d'),
						'idvet' => $idvet,
					);

					$this->db->insert('tbl_vaccine',$datainsert);
					$this->session->set_flashdata('success', 'Data successfully inserted');
					
					$dataclinic = array(
        			    'uid' => $profileid,
        			    'idclinic' => $idclinic,
        			    'nama'=> $nama,
        			    'nohp' => $phone,
        			    'email' => $email,
        			    'city'=> $city,
        			    'address' => $address,
        			    'country' => $country,
        			    'input_by' => '0',
        			    'status' => '0',
        			    'delete_by' =>'',
        			);

					$dt_cust = $this->Home_model->cekcust($idclinic,$profileid);
        			if(count($dt_cust)==0){
        			    $this->db->insert('tbl_customer',$dataclinic);
        			    $idcustomer = $this->db->insert_id();
        			}else{
        			    $idcustomer = $dt_cust[0]->idcustomer;
        			}
        			
            		$this->db->where('idpet',$idpet);
            		$this->db->update('tbl_pet',array('idclinic'=>$idclinic,'idowner'=>$idcustomer));

        			$dt_clinic = $this->Home_model->detailclinic($idclinic);
        			
        			if(count($dt_clinic)>0){
        				    $subject = 'Appointment Notification';
        				    $message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
        					<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
        					 <img height="70" src="https://app.ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
        							 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"><h1 style="font-size:20px;margin:0;color:#333"> Hello '.$dt_clinic[0]->nameclinic.', </h1>
        							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">'.$nama.' has make new appointment please check your dashboard login.</p><br /><br />
        							 </td> </tr>
        				</tbody> </table> </td> </tr></tbody> </table>';
            				$text = $message;

        				    $this->load->library('EmailSender.php');
        				    $this->emailsender->send($dt_clinic[0]->email,$subject,$text);		
        				}

					redirect ('appointment');

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect (base_url());
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect (base_url());
			}
		}else{
			redirect ('maintenance');
		}
    }


     public function delete($idvacc){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataappoint = $this->Appointment_model->detailappoint($idvacc);
				$idowner = $dataappoint[0]->idowner;
				// var_dump($idowner);
				// var_dump($profileid);
 				if ($idowner == $profileid) {
 					$dataupdate = array(
 						'status' => '2',
 					);
 					$this->db->where('idvacc',$idvacc);
 					$this->db->update('tbl_vaccine',$dataupdate);
 					$this->session->set_flashdata('success', 'Appointment has been deleted.');
					redirect ('appointment');

				}else{
					$this->session->set_flashdata('error', 'You canot delete this appointment.');
					redirect ('appointment');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }
}