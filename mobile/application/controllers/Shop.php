<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Shop_model');
		$this->load->model('Team_model');
		$this->load->model('Customer_model');
		$this->load->model('Clinic_model');
		$this->load->model('Pet_model');
		$this->load->model('Setting_model');
		$this->load->model('Login_model');
		$this->load->model('Sales_model');
		$this->load->model('Service_model');	
		// $this->load->model('Reminder_model');
		// //TEST SERVER
  //       $params = array('server_key' => 'SB-Mid-server-KZa4C15_jfBKz1JDRH39cioy', 'production' => false);
  //       // Production server
  //       //$params = array('server_key' => 'Mid-server-9SsIHhSfPMFHs2uWuxBCuTxB', 'production' => true);
		// $this->load->library('veritrans');
		// $this->veritrans->config($params);

    }

    public function all(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		$this->load->library('cart');
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);
				$clinicshop = $this->Shop_model->clinicshop();
				$all_product = $this->Shop_model->countproduct();
				$this->load->library('pagination');
				$config['base_url'] = base_url().'shop/all/';
				$config['total_rows'] = $all_product;
				$config['per_page'] = 10;
				$config['anchor_class'] = 'class="shop_pagenr" ';
				//styling pagination
				// $config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			 //    $config['full_tag_close'] = '</ul>';
			 //    $config['num_tag_open'] = '<li>';
			 //    $config['num_tag_close'] = '</li>';
			    // $config['cur_tag_open'] = '<a href="#" class="shop_pagenr">';
			    // $config['cur_tag_close'] = '</a>';
			    // $config['prev_tag_open'] = '<a href="#" class="prev_shop">';
			    // $config['prev_tag_close'] = '</a>';
			    // $config['first_tag_open'] = '<li>';
			    // $config['first_tag_close'] = '</li>';
			    // $config['last_tag_open'] = '<li>';
			    // $config['last_tag_close'] = '</li>';

			    // $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    // $config['prev_tag_open'] = '<li>';
			    // $config['prev_tag_close'] = '</li>';


			    //$config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    // $config['next_tag_open'] = '<a href="#" class="next_shop">';
			    // $config['next_tag_close'] = '</a>';

			    $config['first_tag_open'] = '<span class="firstlink">';
				$config['first_tag_close'] = '</span>';
				  
				$config['last_tag_open'] = '<span class="lastlink">';
				$config['last_tag_close'] = '</span>';
				  
				$config['next_tag_open'] = '<span class="nextlink">';
				$config['next_tag_close'] = '</span>';
				  
				$config['prev_tag_open'] = '<span class="prevlink">';
				$config['prev_tag_close'] = '</span>';

				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);	
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe

					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,
						'dataproduct' => $this->Shop_model->listshop($config['per_page'],$from),
						'from' => $from,
						'success' => $this->session->flashdata('success'),
						'error' => $this->session->flashdata('error'),
					);
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/dashboard-owner-petshop',$data);
					$this->load->view('website/headerdash_view');
		    		$this->load->view('website/footer_new_view');

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function service(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);
				$clinicshop = $this->Shop_model->clinicshop();
				$this->load->library('pagination');
				$config['base_url'] = base_url().'shop/all/';
				//$config['total_rows'] = $all_product;
				$config['per_page'] = 10;
				$config['anchor_class'] = 'class="shop_pagenr" ';
				//styling pagination
				// $config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			 //    $config['full_tag_close'] = '</ul>';
			 //    $config['num_tag_open'] = '<li>';
			 //    $config['num_tag_close'] = '</li>';
			    // $config['cur_tag_open'] = '<a href="#" class="shop_pagenr">';
			    // $config['cur_tag_close'] = '</a>';
			    // $config['prev_tag_open'] = '<a href="#" class="prev_shop">';
			    // $config['prev_tag_close'] = '</a>';
			    // $config['first_tag_open'] = '<li>';
			    // $config['first_tag_close'] = '</li>';
			    // $config['last_tag_open'] = '<li>';
			    // $config['last_tag_close'] = '</li>';

			    // $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    // $config['prev_tag_open'] = '<li>';
			    // $config['prev_tag_close'] = '</li>';


			    //$config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    // $config['next_tag_open'] = '<a href="#" class="next_shop">';
			    // $config['next_tag_close'] = '</a>';

			    $config['first_tag_open'] = '<span class="firstlink">';
				$config['first_tag_close'] = '</span>';
				  
				$config['last_tag_open'] = '<span class="lastlink">';
				$config['last_tag_close'] = '</span>';
				  
				$config['next_tag_open'] = '<span class="nextlink">';
				$config['next_tag_close'] = '</span>';
				  
				$config['prev_tag_open'] = '<span class="prevlink">';
				$config['prev_tag_close'] = '</span>';

				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);	
				//var_dump($clinicshop);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe

					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,
						'dataproduct' => $this->Shop_model->serviceshop($config['per_page'],$from),
						'service' => '1'
					);
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/dashboard-owner-petshop',$data);
					$this->load->view('website/headerdash_view');
		    		$this->load->view('website/footer_new_view');

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function orderprocess(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);
				//$dataproductasli = $this->Shop_model->listshop();
				if ($role == '0') {
					//cek tbl_subscribe
				$digits = 3;
				$item = $this->input->post('item');
				$iditem = $this->input->post('iditem');
				$price = $this->input->post('price');
				$qty = $this->input->post('qty');
				$idclinic = $this->input->post('idclinic');
				
				$totalprice = $this->input->post('totalprice');
				//var_dump($item);
				//var_dump($totalprice);
				$unik = rand(pow(10, $digits-1), pow(10, $digits)-1);
				$digits2 = 2;
				$unik2 = rand(pow(10, $digits2-1), pow(10, $digits2)-1);
				//$his = date('His');
				$invoice = 'IV'.$profileid.$unik.$unik2;
				$totalprice = preg_replace('~\.0+$~','',$totalprice);
				$tax = 10/100 * $totalprice;
				//$totalqty = preg_replace('~\.0+$~','',$totalqty);
				$countitem = count($item);
				$invdatefix = $this->input->post('invdate');
				//insert tbl invoice;
				//var_dump($invdatefix);
				
				//var_dump($item);
				
					$datainvoice = array(				
						'invdate' => date('Y-m-d H:i:s'),
						'uid' =>  $profileid,
						'nameclient' => $nama,
						'emailclient' => $email,
						'phoneclient' => $phone,
						//'idclinic' => $idclinic,
						//'idvet' => $idvet,
						'status' => '0',
						'totalprice' => $totalprice,
						'totalqty' => $countitem,
						'invoice' => $invoice,
						//'discount' => $discount,
						'methode' => 'shop',
						//'tax' => $tax,
						//'note' => $this->input->post('note'),
						//'idpet' => $idpet,
					);
					$this->db->insert('tbl_invoice',$datainvoice);

					foreach($iditem as $key=>$val){				
						//var_dump($dataitem);
						$dataitem = array(
								'invoice' => $invoice,
								'item' => $item[$key],
								'price' => $price[$key],
								'qty' => $qty[$key],
								'idclinic' => $idclinic[$key],	
								//'idvet' => $idvet,
								'tanggal' => date('Y-m-d H:i:s'),			
						);
						//var_dump($dataitem);
						$this->Sales_model->insertitem($dataitem);
						// if($val == ''){	
						// 	$this->Sales_model->insertitem($dataitem);
						// }else{
						// 	$this->Sales_model->insertitem($dataitem);
						// 	//$this->Sales_model->updateitem($val,$dataitem,$idclinic);
							
						// 	// //update qty
						// 	$nameservice = $val;
						// 	$cekservice = $this->Service_model->cekservice($nameservice);
						// 	if(count($cekservice) == '0'){
						// 		// $idservice = $cekservice[0]->id;
						// 		// $qtyawal = $cekservice[0]->qty;
						// 		// $qtybeli = $dataitem['qty'];
						// 		// $qtyfix = $qtyawal - $qtybeli;
						// 		// $dataupdate = array(
						// 		// 	'qty' => $qtyfix,
						// 		// );
						// 		// $this->db->where('id',$idservice);
						// 		// $this->db->update('tbl_clinicservice',$dataupdate);
						// 	}else{
						// 		$idservice = $cekservice[0]->id;
						// 		$qtyawal = $cekservice[0]->qty;
						// 		$qtybeli = $dataitem['qty'];
						// 		$qtyfix = $qtyawal - $qtybeli;
						// 		$dataupdate = array(
						// 			'qty' => $qtyfix,
						// 		);
						// 		$this->db->where('id',$idservice);
						// 		$this->db->update('tbl_clinicservice',$dataupdate);
						// 	}
						// 	//redirect ('sales/detail/?invoice='.$invoice);
							
						// }
					}
					//$this->session->set_flashdata('success', 'Congrats invoice sales has been inserted.');
					redirect ('shop/invoice/?invoice='.$invoice);
			//}

				}else{
					$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
					redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}

	}

	public function invoice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			// if ($role != '0') {
			// 	$idclinic = $dataprofile[0]->idclinic;
			// 	$dataclinic = $this->Home_model->detailclinic($idclinic);
			// 	$pictureclinic = $dataclinic[0]->picture;
			// }
			// else{
			// 	$idclinic = '0';
			// 	$dataclinic = '';
			// 	$pictureclinic = '';
			// }
			
			$invoice = $this->input->get('invoice');
			$datainvoice = $this->Sales_model->detailinv($invoice);	
			
			// if(count($datainvoice) == 0){
			// 	$this->session->set_flashdata('error', 'Sorry the invoice not found.');
			// 	if($role == '0'){
			// 		redirect ('sales/myinvoice');
			// 	}else{
			// 		redirect ('sales/all');
			// 	}
				
			// }else{
    			//$clinicinv = $datainvoice[0]->idclinic;
    			//$dataclinicinv = $this->Sales_model->detailclinic($clinicinv);
    			//$nameclinicinv = $dataclinicinv[0]->nameclinic;
    			//$pictureclinic = $dataclinicinv[0]->picture;
				//$noclinic = $datainvoice[0]->idclinic;
				//if($noclinic != $idclinic){					
				//	if($role == '0'){
						$uidinvoice = $datainvoice[0]->uid;
						if($uidinvoice == $profileid){
							$totalqty = $datainvoice[0]->totalqty;
							$iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
							$data = array(
								'title' => 'Detail Invoice - iVetdata.com',
								'nama' => $nama,
								'country' => $country,
								'city' => $city,
								'profilephoto' => $profilephoto,
								'role' => $role,	
								//'manageto' => $dataprofile[0]->manageto,
								//'nameclinic' => $nameclinicinv,
								//'pictureclinic' => $pictureclinic,		
								//'nameclinic' => $dataclinic[0]->nameclinic,
								//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
								'success' => $this->session->flashdata('success'),
								'datainvoice' => $datainvoice,
								'iteminvoice' => $iteminvoice,
								//'clinicfav' => $this->Home_model->clinicfav(),					
							);
							$this->load->view('website/headerdash_view',$data);
							$this->load->view('website/shopinvclient_view',$data);
							$this->load->view('website/footerdash_view',$data);

						}else{
							$this->session->set_flashdata('error', 'Sorry the invoice not for you.');
							redirect ('sales/myinvoice');
						}						
					//}else{
					//	$this->session->set_flashdata('error', 'Sorry the invoice not for your clinic.');
					//	redirect ('sales/all');
					//}
				// }else{
				// 	$totalqty = $datainvoice[0]->totalqty;
				// 	$iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
				// 	$data = array(
				// 		'title' => 'Detail Invoice - iVetdata.com',
				// 		'nama' => $nama,
				// 		'country' => $country,
				// 		'city' => $city,
				// 		'profilephoto' => $profilephoto,
				// 		'role' => $role,
				// 		'manageto' => $dataprofile[0]->manageto,		

				// 		'nameclinic' => $dataclinic[0]->nameclinic,
				// 		'pictureclinic' => $pictureclinic,	
				// 		//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
				// 		'success' => $this->session->flashdata('success'),
				// 		'error' => $this->session->flashdata('error'),
				// 		'datainvoice' => $datainvoice,
				// 		'iteminvoice' => $iteminvoice,
				// 		//'clinicfav' => $this->Home_model->clinicfav(),					
				// 	);
				// 	$this->load->view('website/headerdash_view',$data);
				// 	$this->load->view('website/viewinvclient',$data);
				// 	$this->load->view('website/footerdash_view',$data);
				// }
			//}	    	
		}else{
			 redirect ('login');
		}
    }

    public function myinvoice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			// $dataclinic = $this->Home_model->dataclinic($profileid);
			// $idclinic = $dataclinic[0]->idclinic;
			//pagination
			$all_sales = $this->Shop_model->myjumlah_data($profileid);
			//var_dump($profileid);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'sales/myinvoice/';
			$config['total_rows'] = $all_sales;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	
			$data = array(
				'title' => 'Sales list - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'from' => $from,				
				//'nameclinic' => $dataclinic[0]->nameclinic,
				'all_sales' => $all_sales,
				'datasales' => $this->Shop_model->mydata($config['per_page'],$from,$profileid),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/shopinvoice_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{

			redirect ('login');
		}
    }

    public function payprocess(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$fullname = $nama;
				$phone = $dataprofile[0]->phone;
				$address = 'Indonesia';
				$postalcode = '11111';
				$email = $dataprofile[0]->email;
				$idclinic = $dataprofile[0]->idclinic;
				//$country = $dataprofile[0]->country;
				$country = 'IDN';
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idinvoice = $this->input->post('invoice');
				$this->form_validation->set_rules('methode', 'Methode', 'required');
				// if ($this->form_validation->run() == FALSE){
				// 	$this->session->set_flashdata('error', 'Please select subcription.');
				// 	redirect ('dashboard/buy');

				// }else{				
					$datainvoice = $this->Shop_model->idinvoice($idinvoice);
					//var_dump($datainvoice);
					$price = $datainvoice[0]->totalprice;
					$qty = $datainvoice[0]->totalqty;
					//$namaservice = $dataservice[0]->name;
					//$discount = $this->input->post('discount');
					//$kode = $this->input->post('discount');					
							
					$fixprice = $price ;

					//midtrans
					$digits = 3;
					//$unik = rand(pow(10, $digits-1), pow(10, $digits)-1);
					$totalprice = $fixprice;
					$va_code = $profileid.$phone;
					//$bank_code = $this->input->post('methode');
					//$datauid = $this->login_model->cekuid($uid);
					$name = $nama;
					$invoice = $idinvoice;


					$paymentgateway = '1';
					$description = 'Payment Invoice '.$idinvoice.' ';

					if($paymentgateway == '0'){
						// $datatrx = array(
						// 	'uid' => $profileid,
						// 	'idclinic' => $idclinic,
						// 	//'service' => $service,
						// 	'nama' => $name,
						// 	'invoice' => $invoice,
						// 	'tipe' => 'subscription',
						// 	'tglbeli' => date('Y-m-d H:i:s'),
						// 	'jumlah' => $totalprice,
						// 	'description' => $description,
						// 	'idservice' => $idservice,
						// 	'affid' => $affid,
						// 	'kode' => $kode,
						// 	'va_code' => $va_code,
						// 	'bankcode' => $bank_code,
						// 	'pg' => '0',
						// );
						// $this->db->insert('tbl_transaksi',$datatrx);
						$data = array(
							'profilephoto' => $profilephoto,
							'role' => $role,'manageto' => $dataprofile[0]->manageto,
							'title' => 'Buy subscription - Ivetdata.com',
							'invoice' => $invoice,
							'price' => $price,
							'discount' => $valuediscount,
							'grandprice' => $fixprice,
							'unik' => $unik,
							'totalprice' => $totalprice,
							'description' => $description,
							'nama' => $name,
						);
						$this->load->view('website/headerdash_view',$data);
						$this->load->view('website/dashinvoice_view',$data);
						$this->load->view('website/footerdash_view',$data);

					}else{
						// $datatrx = array(
						// 	'uid' => $profileid,
						// 	'idclinic' => $idclinic,
						// 	//'service' => $service,
						// 	'nama' => $name,
						// 	'invoice' => $invoice,
						// 	'tipe' => 'subscription',
						// 	'tglbeli' => date('Y-m-d H:i:s'),
						// 	'jumlah' => $totalprice,
						// 	'description' => $description,
						// 	'idservice' => $idservice,
						// 	'affid' => $affid,
						// 	'kode' => $kode,
						// 	'va_code' => $va_code,
						// 	'bankcode' => $bank_code,
						// 	'pg' => '1',
						// );
						// $this->db->insert('tbl_transaksi',$datatrx);

						//devxendit
						//$xenditkey = 'xnd_development_OYyFfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
						//livexendit
						$xenditkey = 'xnd_production_OImCfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				
						  require 'vendor/autoload.php';

						  $options['secret_api_key'] = $xenditkey;

						  $xenditPHPClient = new XenditClient\XenditPHPClient($options);

						  $xenditPHPClient = new XenditClient\XenditPHPClient($options);

						  $external_id = $invoice;
						  $amount = $totalprice;
						  $payer_email = $email;
						  $description = $description;

						  $response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description);
						  header('Location: ' . $response['invoice_url']);
						  //print_r($response);

						  // $external_id = $phone;
						  // $bank_code = $bank_code;
						  // $name = str_replace(".", "", $nama);
						  // $expected_amount = $totalprice;
						  // $virtual_account_number = $va_code;

						  // $response = $xenditPHPClient->createCallbackVirtualAccount($external_id, $bank_code, $name, $expected_amount, $virtual_account_number);
						  // print_r($response);

						//MIDTRANS
						// $transaction_details = array(
						// 	'order_id' 			=> $invoice,
						// 	'gross_amount' 	=> $totalprice,
						// );

						// // Populate items
						// $items = [
						// 	array(
						// 		'id' 				=> $idservice,
						// 		'price' 		=> $totalprice,
						// 		'quantity' 	=> 1,
						// 		'name' 			=> $namaservice
						// 	)
						// ];

						// // Populate customer's billing address
						// $billing_address = array(
						// 	'first_name' 		=> $fullname,
						// 	//'last_name' 		=> "Gufron",
						// 	'address' 			=> $address,
						// 	'city' 					=> $city,
						// 	'postal_code' 	=> $postalcode,
						// 	'phone' 				=> $phone,
						// 	'country_code'	=> $country,
						// 	);

						// // Populate customer's shipping address
						// $shipping_address = array(
						// 	'first_name' 	=> $fullname,
						// 	//'last_name' 	=> "Watson",
						// 	'address' 		=> $address,
						// 	'city' 				=> $city,
						// 	'postal_code' => $postalcode,
						// 	'phone' 			=> $phone,
						// 	'country_code'=> $country,
						// 	);

						// // Populate customer's Info
						// $customer_details = array(
						// 	'first_name' 		=> $fullname,
						// 	//'last_name' 		=> "Gufron",
						// 	'email' 					=> $email,
						// 	'phone' 					=> $phone,
						// 	'billing_address' => $billing_address,
						// 	'shipping_address'=> $shipping_address
						// 	);

						// // Data yang akan dikirim untuk request redirect_url.
						// // Uncomment 'credit_card_3d_secure' => true jika transaksi ingin diproses dengan 3DSecure.
						// $transaction_data = array(
						// 	'payment_type' 			=> 'vtweb', 
						// 	'vtweb' 						=> array(
						// 		//'enabled_payments' 	=> ['credit_card'],
						// 		'credit_card_3d_secure' => true
						// 	),
						// 	'transaction_details'=> $transaction_details,
						// 	'item_details' 			 => $items,
						// 	'customer_details' 	 => $customer_details
						// );
					
						// try
						// {
						// 	$vtweb_url = $this->veritrans->vtweb_charge($transaction_data);
						// 	header('Location: ' . $vtweb_url);
						// } 
						// catch (Exception $e) 
						// {
				  //   		echo $e->getMessage();	
						// }
					}

					

					// var_dump($valuediscount);
					// echo '<br>';
					// var_dump($fixprice);
					// $dataclinic = $this->Home_model->dataclinic($profileid);
					// $idclinic = $dataclinic[0]->idclinic;
					// $data = array(				
					// 	'dataservice' => $this->Login_model->service(),
					// 	'nama' => $nama,
					// 	'clinic' => $dataclinic[0]->nameclinic,
					// 	'country' => $country,
					// 	'city' => $city,
					// 	'profilephoto' => $profilephoto,
					// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
					// 	'title' => 'Select subscription - Ivetdata.com',		
					// );
					// $this->load->view('website/headerdash_view',$data);
					// $this->load->view('website/buy_view',$data);
					// $this->load->view('website/footerdash_view');
			//	}

			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function dataproduct(){
		$dataproduct = $this->Shop_model->listshop();
		echo json_encode($dataproduct);
	}


	public function invoicetemp(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;
			$qty = $this->input->post('qty');
			$idproduk = $this->input->post('idproduk');
			$nama_item = $this->input->post('nama_item');
			$idclinic = $this->input->post('idclinic');
			$price = $this->input->post('price');
			if (isset($this->session->userdata['cart'])) {
				$idinvoice = $this->session->userdata['cart']['invoice'];
				$invoicesettle = $this->Shop_model->cekinvoicesettle($idinvoice);
				if(count($invoicesettle) == 1){
					$invoice = $profileid.rand(1, 1000000);
					$session_cart = array(
	    				'invoice' => $invoice,		
	    			);
	    			$this->session->set_userdata('cart', $session_cart);
				}else{
					$invoice = $idinvoice;
				}
			}else{
				$invoice = $profileid.rand(1, 1000000);
				$session_cart = array(
    				'invoice' => $invoice,		
    			);
    			$this->session->set_userdata('cart', $session_cart);
			}
			$datainsert = array(
				'invoice_temp' => $invoice,
				'idproduk' => $idproduk,
				'nama_item' => $nama_item,
				'qty' => $qty,
				'price' => $price,
				'totalprice' => $qty * $price,
				'status' => '1',
				'dateadd' => date('Y-m-d H:i:s'),
				'uid' => $profileid,
				'idclinic' => $idclinic,
			);
			$this->db->insert('tbl_cart',$datainsert);
			redirect('shop/cart');
			// $data_view = array(

			// )
			//var_dump($datainsert);
			//var_dump($invoice);
			
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function cart(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;
			$phone = $dataprofile[0]->phone;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$expdate = $dataprofile[0]->expdate;
			if (isset($this->session->userdata['cart'])) {
				$idinvoice = $this->session->userdata['cart']['invoice'];
				$invoicesettle = $this->Shop_model->cekinvoicesettle($idinvoice);
				if(count($invoicesettle) == 1){
					$invoice = $profileid.rand(1, 1000000);
					$session_cart = array(
	    				'invoice' => $invoice,		
	    			);
	    			$this->session->set_userdata('cart', $session_cart);
				}else{
					$invoice = $idinvoice;
				}
			}else{
				$invoice = '0';
			}
			$data = array(
				'title' => 'Home - iVetdata.com',
				'nama' => $nama,
				'email' => $email,
				'country' => $country,
				'city' => $city,
				//'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'invoice' => $invoice,
				'datacart' => $this->Shop_model->cart($invoice),
				'totalcart' => $this->Shop_model->totalcart($invoice),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
			);
			//$this->load->view('login_user');
			$this->load->view('website/header_new_view',$data);
			//$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/dashboard-owner-cart',$data);
			$this->load->view('website/headerdash_view');
		    $this->load->view('website/footer_new_view');
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function checkout(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;
			$phone = $dataprofile[0]->phone;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$expdate = $dataprofile[0]->expdate;
			//if (isset($this->session->userdata['cart'])) {
			$invoice = $this->session->userdata['cart']['invoice'];
			// }else{
			// 	$invoice = '0';
			// }
			$totalprice = $this->Shop_model->totalcart($invoice);
			$totalqty = $this->Shop_model->totalqty($invoice);
			//if($invoice > '0'){
				//insertinvoiceuser
				
				//var_dump($totalprice);
				//var_dump($invoice);
				$insertinvoiceuser = array(
					'invoice' => $invoice,
					'invdate' => date('Y-m-d H:i:s'),
					'uid' => $profileid,
					'nameclient' => $nama,
					'emailclient' => $email,
					'phoneclient' => $phone,
					'totalprice' => $totalprice,
					'totalqty' => $totalqty,
					'methode' => 'shop',
				);
				$this->Shop_model->insertinvoice($insertinvoiceuser);
				$this->db->last_query();

				//insertinvoiceitem
				$itemcart = $this->Shop_model->cart($invoice);
				//var_dump($itemcart);
				foreach ($itemcart as $key=>$val) {
					$dataitem = array(
						'invoice' => $invoice,
						'item' => $val->nama_item,
						'price' => $val->price,
						'qty' => $val->qty,
						'idclinic' => $val->idclinic,
						'idproduk' => $val->idproduk,	
						'tanggal' => $val->dateadd,
						'totalprice' => $val->totalprice,			
					);
					$this->Sales_model->insertitem($dataitem);
					//$this->db->insert('tbl_invoiceitem',$dataitem);
				};

				// //remove cart
				// $dataupdate = array(
				// 	'status' => '0',
				// );
				// $this->db->where('invoice_temp',$invoice);
				// $this->db->delete('tbl_cart');

				//devxendit
				$xenditkey = 'xnd_development_OYyFfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				//livexendit
				$xenditkey = 'xnd_production_OImCfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				
				require 'vendor/autoload.php';

				$options['secret_api_key'] = $xenditkey;

				$xenditPHPClient = new XenditClient\XenditPHPClient($options);

				$xenditPHPClient = new XenditClient\XenditPHPClient($options);

				$external_id = $invoice;
				$amount = $totalprice;
				$payer_email = $email;
				$description = 'Pembelian invoice #'.$invoice;
				$response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description);
				//var_dump($response);
				//header('Location: ' . $response['invoice_url']);
				//unset session cart
				// $sess_array = array(
				// 	'invoice' => '0',
				// );
				// $this->session->unset_userdata('cart', $sess_array);
				//$this->session->unset_userdata('cart', $sess_array);
				redirect ($response['invoice_url']);
				
			// }else{
			// 	//redirect ('shop/all');
			// }
			
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function removecart(){
		
		$this->session->unset_userdata('cart');
	}

	public function deleteitem($id_cart){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;
			$phone = $dataprofile[0]->phone;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			//var_dump($id_cart);
			$detailcart = $this->Shop_model->detailcart($id_cart);
			if(count($detailcart) == '0'){
				$this->session->set_flashdata('error', 'Item cannot found.');
					redirect ('shop/all');
			}else{
				$uid = $detailcart[0]->uid;
				// var_dump($uid);
				// var_dump($profileid);
				if($uid = $profileid){
					$this->db->where('id_cart', $id_cart);
					$this->db->delete('tbl_cart');
					$this->session->set_flashdata('success', 'item has been deleted');
					redirect (base_url().'shop/cart');

				}else{
					$this->session->set_flashdata('error', 'You cannot delete this item.');
					redirect ('shop/all');
				}
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function search(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;
			$phone = $dataprofile[0]->phone;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$item = $this->input->post('item');
			//var_dump($item);
			//var_dump($id_cart);
			$detailsearch = $this->Shop_model->search($item);
			//var_dump(count($detailsearch));
			if(count($detailsearch) == '0'){
				$this->session->set_flashdata('error', 'product not found.');
				redirect ('shop/all');
			}else{
				$data = array(
					'title' => 'Home - iVetdata.com',
					'nama' => $nama,
					'email' => $email,
					'country' => $country,
					'city' => $city,
					//'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'dataproduct' => $detailsearch,
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				//$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/dashboard-owner-petshop',$data);
				$this->load->view('website/headerdash_view');
		    	$this->load->view('website/footer_shop_view');
				
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
		
	}


}