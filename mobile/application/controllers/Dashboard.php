<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Team_model');
		$this->load->model('Customer_model');
		$this->load->model('Clinic_model');
		$this->load->model('Pet_model');
		$this->load->model('Setting_model');
		$this->load->model('Login_model');	
		$this->load->model('Reminder_model');
		// //TEST SERVER
  //       $params = array('server_key' => 'SB-Mid-server-KZa4C15_jfBKz1JDRH39cioy', 'production' => false);
  //       // Production server
  //       //$params = array('server_key' => 'Mid-server-9SsIHhSfPMFHs2uWuxBCuTxB', 'production' => true);
		// $this->load->library('veritrans');
		// $this->veritrans->config($params);

    }
    
	public function index(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				if(count($dataprofile)<1){
				    die("<script>alert('Please login again');self.location.href='/welcome/logoutowner';</script><h2>Please enable javascript</h2>");
				}
				$role = $dataprofile[0]->role;
				//var_dump($role);
				if($role == '1'){
					redirect('dashboard/clinic');
				}if($role == '0'){
					redirect('dashboard/petowner');
				}if($role == '3'){
					redirect('dashboard/partner');
				}if($role == '2'){
					$nama = $dataprofile[0]->name;
					$email = $dataprofile[0]->email;
					$country = $dataprofile[0]->country;
					$city = $dataprofile[0]->city;
					$photo = $dataprofile[0]->photo;
					if($photo == '' ){
					    $profilephoto = base_url().'assets/images/team/05.jpg';
					}else{
					    $profilephoto = $photo;
					};
					$dataclinic = $this->Home_model->dataclinic($profileid);
					if(count($dataclinic)>0){
					    $idclinic = $dataclinic[0]->idclinic;
					}else{
					    $idclinic = 0;
					}
					$cekdoctor = $this->Home_model->cekdoctor($idclinic,$role);
					$manageto = $dataprofile[0]->manageto;
					$countpet = count($this->Home_model->pethomedash($idclinic,$role,$manageto,$profileid));
					$countcustomer = count($this->Home_model->custhomedash($idclinic,$role,$manageto,$profileid));
					$countorder = count($this->Home_model->orderdashclinic($idclinic,$role));
					$revtoday = $this->Home_model->revtodayclinic($idclinic,$manageto,$profileid);
					$countdoctor = count($cekdoctor);
					$remindervacc = $this->Reminder_model->remindervacc($idclinic,$role);
					$data = array(
						'title' => 'Home - iVetdata.com',
						'nama' => $nama,
						'email' => $email,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,
						'datapet' => $this->Home_model->pethome(),
						'clinicfav' => $this->Home_model->clinicfav(),
						'countdoctor' => $countdoctor,
						'countpet' => $countpet,
						'countorder' => $countorder,
						'revtoday' => $revtoday,
						'revyesterday' => $this->Home_model->revyesterday(),
						'totalrev' => $this->Home_model->totalrev(),
						'bulan12' => $this->Home_model->bulan12(),
						'reminder' => $remindervacc,
					);
					//$this->load->view('login_user');

					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/footer_new_view',$data);
				}
				//$this->load->view('footer_view',$data);
			}else{

				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

    public function owner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;	
				$email = $dataprofile[0]->email;
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idclinic = $dataprofile[0]->idclinic;
				$this->load->view('website/header_new_view');
				$this->load->view('website/homedash_view');
				$this->load->view('website/headerdash_view');
				$this->load->view('website/footer_new_view');
			}else{

				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}
	
	public function clinic(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				if($role == '0'){
					redirect('dashboard/petowner');
				}if($role == '3'){
					redirect('dashboard/partner');
				}
				$manageto = $dataprofile[0]->manageto;
				if($manageto == '3'){
					redirect('pet/all');
				}
				$nama = $dataprofile[0]->name;	
				$email = $dataprofile[0]->email;
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);
				// if(count($dataclinic) == '0'){
				// 	$idclinic = $dataprofile[0]->idclinic;
				// }else{
				// 	$idclinic = $dataclinic[0]->idclinic;
				// }				
				$cekdoctor = $this->Home_model->cekdoctor($idclinic,$role);
				$countpet = count($this->Home_model->pethomedash($idclinic,$role,$manageto,$profileid));
				$countcustomer = count($this->Home_model->custhomedash($idclinic,$role,$manageto,$profileid));
				$countorder = count($this->Home_model->orderdashclinic($idclinic,$role));
				$revtoday = $this->Home_model->revtodayclinic($idclinic,$manageto,$profileid);
				$countdoctor = count($cekdoctor);
				$remindervacc = $this->Reminder_model->remindervacc($idclinic,$role);
				$remindermed = $this->Reminder_model->remindermed($idclinic,$role);
				$data = array(
					'title' => 'Home Clinic - iVetdata.com',
					'nama' => $nama,
					'email' => $email,
					'idclinic'=> $dataclinic[0]->idclinic,
					'clinic' => $dataclinic[0]->nameclinic,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'team' => $dataprofile[0]->team,
					'manageto' => $manageto,
					//'expdate' => $dataprofile[0]->expdate,
					'datapet' => $this->Home_model->pethome(),
					'clinicfav' => $this->Home_model->clinicfav(),
					'countdoctor' => $countdoctor,
					'countcustomer' => $countcustomer,
					'countpet' => $countpet,
					'countorder' => $countorder,
					'revtoday' => $revtoday,
					'revyesterday' => $this->Home_model->revyesterdayclinic($idclinic,$manageto,$profileid),
					'monthrev' => $this->Home_model->monthrevclinic($idclinic,$manageto,$profileid),
					'totalrev' => $this->Home_model->totalrevclinic($idclinic,$manageto,$profileid),
					'dailymonth' => $this->Home_model->dailymonth(),
					'revbulan' => $this->Home_model->revbulan($idclinic,$manageto,$profileid),

					'bulan12' => $this->Home_model->bulan12(),
					'revbulan12' => $this->Home_model->revbulan12($idclinic,$manageto,$profileid),
					'expdate' => $expdate,
					'reminder' => $remindervacc,
					'reminder2' => $remindermed,
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/clinicdash_view',$data);
				//$this->load->view('footer_view',$data);
			}else{

				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

	public function partner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;				
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);
				$idclinic = '0';
				//pagination
				$all_clinic = $this->Clinic_model->jumlah_data($idclinic,$role);
				$totalclinic = $this->Clinic_model->allclinicshow();
				$this->load->library('pagination');
				$config['base_url'] = base_url().'Dashboard/partner/';
				$config['total_rows'] = $all_clinic;
				$config['per_page'] = 10;
				//styling pagination
				$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			    $config['full_tag_close'] = '</ul>';
			    $config['num_tag_open'] = '<li>';
			    $config['num_tag_close'] = '</li>';
			    $config['cur_tag_open'] = '<li class="active"><a href="#">';
			    $config['cur_tag_close'] = '</a></li>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';
			    $config['first_tag_open'] = '<li>';
			    $config['first_tag_close'] = '</li>';
			    $config['last_tag_open'] = '<li>';
			    $config['last_tag_close'] = '</li>';

			    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';


			    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    $config['next_tag_open'] = '<li>';
			    $config['next_tag_close'] = '</li>';

				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);	
				$datapet = $this->Pet_model->byowneremail($email);
				$data = array(
					'title' => 'Partner Dashboard - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'from' => $from,
					'all_clinic' => count($totalclinic),
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'datacountry' => $this->Login_model->country(),
					'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
					//'datapet' => $datapet,
					'dataclinic' => $this->Clinic_model->data($config['per_page'],$from,$idclinic,$role),
					'namaowner' => $nama,
				);

				
				//var_dump($idowner);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				//var_dump($datapet);
				$this->load->view('website/allclinic_view',$data);
				//$this->load->view('website/footerdash_view',$data);
				//$this->load->view('footer_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

	public function petowner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;
				$phone = $dataprofile[0]->phone;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);

				// $idowner = $this->input->get('id');
				// $idcustomer = $idowner;
				// $dataowner = $this->Customer_model->detailcustomer($idcustomer);
				// $namaowner = $dataowner[0]->nama;
				if(isset($_GET['rfid'])&&$_GET['rfid']<>''){
				    $datapet = $this->Pet_model->detailmicrochipbyowner($_GET['rfid'],$email);
				}else{
				    $datapet = $this->Pet_model->byowneremail($email,0,$role);    
				}
				//$datapet = $this->Pet_model->byownerphone($phone);	
				
				//if(count($datapet)==0) $this->session->set_flashdata('error', 'Sorry pet not found.');
                if(count($datapet)==0) redirect(base_url().'pet/addbyowner');

				$data = array(
					'title' => 'Pet Owner - iVetdata.com',
					'nama' => $nama,
					'email' => $email,
					'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'datacountry' => $this->Login_model->country(),
					'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
					'datapet' => $datapet,
					'dataclinic' => $dataclinic,
					'namaowner' => $nama,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);

				
				//var_dump($idowner);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				//var_dump($datapet);
				$this->load->view('website/ownerdash_new_view',$data);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/footer_new_view',$data);
			}else{

				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

	public function buy(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->dataclinic($profileid);
				$idclinic = $dataclinic[0]->idclinic;
				$data = array(				
					'dataservice' => $this->Login_model->service(),
					'nama' => $nama,
					'clinic' => $dataclinic[0]->nameclinic,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'title' => 'Select subscription - Ivetdata.com',		
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/buy_view',$data);
				//$this->load->view('website/footerdash_view');

			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function cekcode(){
		$code = $this->input->post('code');		
		$datacode = $this->Home_model->cekkode($code);
		//var_dump($datacode);
		if(count($datacode) == '1'){
			//add the header here
		    header('Content-Type: application/json');
		    echo json_encode( $datacode );
			return true;
		}else{
			return false;
		}
	}

	public function buyselect(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$fullname = $nama;
				$phone = $dataprofile[0]->phone;
				$email = $dataprofile[0]->email;
				//$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->dataclinic($profileid);
				$idclinic = $dataclinic[0]->idclinic;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idservice = $this->input->get('idservice');
				//var_dump($idservice);
				$dataservice = $this->Home_model->idservice($idservice);
				//devxendit
				//$xenditkey = 'xnd_development_OYyFfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				//livexendit
				$xenditkey = 'xnd_production_OImCfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				$data = array(
					'nama' => $nama,
					'clinic' => $dataclinic[0]->nameclinic,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'idservice' => $idservice,
					'uid' => $profileid,
					'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'title' => 'Select subscription - Ivetdata.com',
					'namecustomer' => $dataprofile[0]->name,
					'emailcustomer' => $dataprofile[0]->email,
					'hpcustomer' => $dataprofile[0]->phone,
					'nameservice' => $dataservice[0]->name,
					'price' => $dataservice[0]->price,
					'allteam' => $this->Team_model->buyteam($idclinic,$profileid),
					'xenditkey' => $xenditkey,
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/invoiceservice_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
				
	}


	public function buyprocess(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$fullname = $nama;
				$phone = $dataprofile[0]->phone;
				$address = 'Indonesia';
				$postalcode = '11111';
				$email = $dataprofile[0]->email;
				$idclinic = $dataprofile[0]->idclinic;
				//$country = $dataprofile[0]->country;
				$country = 'IDN';
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idservice = $this->input->post('idservice');
				$this->form_validation->set_rules('methode', 'Methode', 'required');
				if ($this->form_validation->run() == FALSE){
					$this->session->set_flashdata('error', 'Please select subcription.');
					redirect ('dashboard/buy');

				}else{				
					$dataservice = $this->Home_model->idservice($idservice);
					$price = $this->input->post('totalpricenya');
					$qty = $this->input->post('qty');
					$namaservice = $dataservice[0]->name;
					$discount = $this->input->post('discount');
					$kode = $this->input->post('discount');					
					$datadiscount = $this->Home_model->detaildiscount($discount);
					if(count($datadiscount) == '0'){
						$discount = '0';
						$valuediscount = 0;
						$affid = 0;
						$kode = '';
					}else{
						$discount = ($datadiscount[0]->value) / 100;
						$valuediscount = $price * $discount;
						$affid = $datadiscount[0]->uid;
						$kode = $kode;
					}					
					$fixprice = $price - $valuediscount;
					$jmlstaff = count($this->input->post('uid')) - 1;

					//midtrans
					$digits = 3;
					$unik = rand(pow(10, $digits-1), pow(10, $digits)-1);
					$totalprice = $fixprice + $unik;
					$va_code = $profileid.$phone.$unik;
					$bank_code = $this->input->post('methode');
					//$datauid = $this->login_model->cekuid($uid);
					$name = $nama;
					$invoice = 'IVET'.$profileid.$unik;


					$paymentgateway = '1';
					$description = 'Subscription '.$namaservice.' '.$qty.' Month with '.$jmlstaff.' staff';

					if($paymentgateway == '0'){
						$datatrx = array(
							'uid' => $profileid,
							'idclinic' => $idclinic,
							//'service' => $service,
							'nama' => $name,
							'invoice' => $invoice,
							'tipe' => 'subscription',
							'tglbeli' => date('Y-m-d H:i:s'),
							'jumlah' => $totalprice,
							'description' => $description,
							'idservice' => $idservice,
							'affid' => $affid,
							'kode' => $kode,
							'va_code' => $va_code,
							'bankcode' => $bank_code,
							'pg' => '0',
						);
						$this->db->insert('tbl_transaksi',$datatrx);
						$data = array(
							'profilephoto' => $profilephoto,
							'role' => $role,'manageto' => $dataprofile[0]->manageto,
							'title' => 'Buy subscription - Ivetdata.com',
							'invoice' => $invoice,
							'price' => $price,
							'discount' => $valuediscount,
							'grandprice' => $fixprice,
							'unik' => $unik,
							'totalprice' => $totalprice,
							'description' => $description,
							'nama' => $name,
						);
						$this->load->view('website/headerdash_view',$data);
						$this->load->view('website/dashinvoice_view',$data);
						$this->load->view('website/footerdash_view',$data);

					}else{
						$datatrx = array(
							'uid' => $profileid,
							'idclinic' => $idclinic,
							//'service' => $service,
							'nama' => $name,
							'invoice' => $invoice,
							'tipe' => 'subscription',
							'tglbeli' => date('Y-m-d H:i:s'),
							'jumlah' => $totalprice,
							'description' => $description,
							'idservice' => $idservice,
							'affid' => $affid,
							'kode' => $kode,
							'va_code' => $va_code,
							'bankcode' => $bank_code,
							'pg' => '1',
						);
						$this->db->insert('tbl_transaksi',$datatrx);

						//devxendit
						//$xenditkey = 'xnd_development_OYyFfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
						//livexendit
						$xenditkey = 'xnd_production_OImCfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				
						  require 'vendor/autoload.php';

						  $options['secret_api_key'] = $xenditkey;

						  $xenditPHPClient = new XenditClient\XenditPHPClient($options);

						  $xenditPHPClient = new XenditClient\XenditPHPClient($options);

						  $external_id = $invoice;
						  $amount = $totalprice;
						  $payer_email = $email;
						  $description = $description;

						  $response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description);
						  header('Location: ' . $response['invoice_url']);
						  //print_r($response);

						  // $external_id = $phone;
						  // $bank_code = $bank_code;
						  // $name = str_replace(".", "", $nama);
						  // $expected_amount = $totalprice;
						  // $virtual_account_number = $va_code;

						  // $response = $xenditPHPClient->createCallbackVirtualAccount($external_id, $bank_code, $name, $expected_amount, $virtual_account_number);
						  // print_r($response);

						//MIDTRANS
						// $transaction_details = array(
						// 	'order_id' 			=> $invoice,
						// 	'gross_amount' 	=> $totalprice,
						// );

						// // Populate items
						// $items = [
						// 	array(
						// 		'id' 				=> $idservice,
						// 		'price' 		=> $totalprice,
						// 		'quantity' 	=> 1,
						// 		'name' 			=> $namaservice
						// 	)
						// ];

						// // Populate customer's billing address
						// $billing_address = array(
						// 	'first_name' 		=> $fullname,
						// 	//'last_name' 		=> "Gufron",
						// 	'address' 			=> $address,
						// 	'city' 					=> $city,
						// 	'postal_code' 	=> $postalcode,
						// 	'phone' 				=> $phone,
						// 	'country_code'	=> $country,
						// 	);

						// // Populate customer's shipping address
						// $shipping_address = array(
						// 	'first_name' 	=> $fullname,
						// 	//'last_name' 	=> "Watson",
						// 	'address' 		=> $address,
						// 	'city' 				=> $city,
						// 	'postal_code' => $postalcode,
						// 	'phone' 			=> $phone,
						// 	'country_code'=> $country,
						// 	);

						// // Populate customer's Info
						// $customer_details = array(
						// 	'first_name' 		=> $fullname,
						// 	//'last_name' 		=> "Gufron",
						// 	'email' 					=> $email,
						// 	'phone' 					=> $phone,
						// 	'billing_address' => $billing_address,
						// 	'shipping_address'=> $shipping_address
						// 	);

						// // Data yang akan dikirim untuk request redirect_url.
						// // Uncomment 'credit_card_3d_secure' => true jika transaksi ingin diproses dengan 3DSecure.
						// $transaction_data = array(
						// 	'payment_type' 			=> 'vtweb', 
						// 	'vtweb' 						=> array(
						// 		//'enabled_payments' 	=> ['credit_card'],
						// 		'credit_card_3d_secure' => true
						// 	),
						// 	'transaction_details'=> $transaction_details,
						// 	'item_details' 			 => $items,
						// 	'customer_details' 	 => $customer_details
						// );
					
						// try
						// {
						// 	$vtweb_url = $this->veritrans->vtweb_charge($transaction_data);
						// 	header('Location: ' . $vtweb_url);
						// } 
						// catch (Exception $e) 
						// {
				  //   		echo $e->getMessage();	
						// }
					}

					

					// var_dump($valuediscount);
					// echo '<br>';
					// var_dump($fixprice);
					// $dataclinic = $this->Home_model->dataclinic($profileid);
					// $idclinic = $dataclinic[0]->idclinic;
					// $data = array(				
					// 	'dataservice' => $this->Login_model->service(),
					// 	'nama' => $nama,
					// 	'clinic' => $dataclinic[0]->nameclinic,
					// 	'country' => $country,
					// 	'city' => $city,
					// 	'profilephoto' => $profilephoto,
					// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
					// 	'title' => 'Select subscription - Ivetdata.com',		
					// );
					// $this->load->view('website/headerdash_view',$data);
					// $this->load->view('website/buy_view',$data);
					// $this->load->view('website/footerdash_view');
				}

			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function saveinv(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;
				$fullname = $nama;
				$phone = $dataprofile[0]->phone;
				$address = 'Indonesia';
				$postalcode = '11111';
				$email = $dataprofile[0]->email;
				$idclinic = $dataprofile[0]->idclinic;
				//$country = $dataprofile[0]->country;
				$country = 'IDN';
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idservice = $this->input->post('service');
				$xenditkey = 'xnd_development_OYyFfL8g0rD6lcc8eUZGWKRN4Kj8IZ7xnK0Rxn9WTfLWgBAByhg';
				$this->form_validation->set_rules('service', 'Service', 'required');
				  require 'vendor/autoload.php';

				  $options['secret_api_key'] = $xenditkey;

				  $xenditPHPClient = new XenditClient\XenditPHPClient($options);

				  $external_id = 'demo_1475459775872';
				  $bank_code = 'BNI';
				  $name = 'Rika Sutanto';

				  $response = $xenditPHPClient->createCallbackVirtualAccount($external_id, $bank_code, $name);
				  print_r($response);

			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function test(){
		echo "blas";
	}
}