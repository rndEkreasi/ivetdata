<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pet extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Login_model');
		$this->load->model('Pet_model');
		$this->load->model('Customer_model');
		$this->load->model('Setting_model');
        $this->load->model('Service_model');
    }

    public function all(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
	    	if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				if($role == '0'){
					redirect ('dashboard/petowner');
				}
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);
				
				//pagination
				$all_pet = $this->Pet_model->jumlah_data($idclinic,$role);
				$this->load->library('pagination');
				$config['base_url'] = base_url().'pet/all/';
				$config['total_rows'] = $all_pet;
				$config['per_page'] = 10;
				//styling pagination
				$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			    $config['full_tag_close'] = '</ul>';
			    $config['num_tag_open'] = '<li>';
			    $config['num_tag_close'] = '</li>';
			    $config['cur_tag_open'] = '<li class="active"><a href="#">';
			    $config['cur_tag_close'] = '</a></li>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';
			    $config['first_tag_open'] = '<li>';
			    $config['first_tag_close'] = '</li>';
			    $config['last_tag_open'] = '<li>';
			    $config['last_tag_close'] = '</li>';

			    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';


			    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    $config['next_tag_open'] = '<li>';
			    $config['next_tag_close'] = '</li>';
				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);	

				$data = array(
					'title' => 'All Pets - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'idclinic' => $idclinic,
					'uid' => $profileid,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'manageto' => $dataprofile[0]->manageto,
					'team' => $dataprofile[0]->team,
					'expdate' => $dataprofile[0]->expdate,
					'all_pet' => $all_pet,
					'from' => $from,
					'datapet' => $this->Pet_model->data($config['per_page'],$from,$idclinic,$role),
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/petpage_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }


    
	public function add(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};	
				$idclinic = $dataprofile[0]->idclinic;
				if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
				$dataclinic = $this->Home_model->detailclinic($idclinic);							
				$data = array(
					'title' => 'Add Microchip - iVetdata.com',
					'nama' => $nama,
					//'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'datacountry' => $this->Login_model->country(),
					'idclinic' => $idclinic,
					'pettype' => $this->Pet_model->pettype(),
					'nameclinic' => $dataclinic[0]->nameclinic,
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
			}	
		}else{
			redirect ('maintenance');
		}
	}

	
	public function addprocess(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			//dataheader
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
			$dataclinic = $this->Home_model->detailclinic($idclinic);	
			//var_dump($dataclinic);
			$petclinic = $dataclinic[0]->pet;
			$nameclinic = $dataclinic[0]->nameclinic;
			$this->form_validation->set_rules('namepet', 'Username', 'required');
			$this->form_validation->set_rules('nameowner', 'nameowner', 'required');
			//data pet
			//$microchip = $this->input->post('microchip');
			// $microchip = '0';
			// if ($microchip == '1') {
			// 	$rfid = $this->input->post('rfid');
			// }if($microchip == '0'){
			// 	$rfid = $this->input->post('rfiduniq');
			// }
			$rfid = $this->input->post('rfid');
			if($rfid == ''){
				$microchip = '0';
				$rfid = '0';
			}else{
				$microchip = '1';
				$rfid = $rfid;
			}
			// var_dump($microchip);
			//var_dump($rfid);

			//$rfid = $this->input->post('rfid');			
			$namepet = $this->input->post('namepet');
			$type = $this->input->post('type');
			$color = $this->input->post('color');
			$description = $this->input->post('description');
			$nameowner = $this->input->post('nameowner');
			$phone= $this->input->post('phone');
			$email = $this->input->post('email');			
			$address = $this->input->post('address');
			$breed = $this->input->post('breed');
			$datebirth = $this->input->post('datebirth');
			$newbirth = date('Y-m-d',strtotime($datebirth));
			//$dataclinic = $this->Home_model->dataclinic($profileid);
			//$idclinic = $dataclinic[0]->idclinic;
			$country = $this->input->post('country');
			$gender = $this->input->post('gender');
			$neutered = $this->input->post('neutered');
			$note = $this->input->post('note');
			$city = $this->input->post('city');
			$postalcode = $this->input->post('postalcode');
			if ($this->form_validation->run() == FALSE){
				$data = array(
						'title' => 'Add Pet - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						//'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,'manageto' => $dataprofile[0]->manageto,
						'expdate' => $dataprofile[0]->expdate,
						'datacountry' => $this->Login_model->country(),
						'idclinic' => $idclinic,
						'pettype' => $this->Pet_model->pettype(),
						'nameclinic' => $dataclinic[0]->nameclinic,
						//'error' => 'Please input the mandatory field.',
						'microchip' => $microchip,
						'rfid' => $rfid,
						'namepet' => $namepet,
						'type' => $type,
						'color' => $color,
						'note' => $description,
						'nameowner' => $nameowner,
						'phone' => $phone,
						'email' => $email,
						'address' => $address,
						'breed' => $breed,
						'datebirth' => $datebirth,
						'country' => $country,
						'gender' => $gender,
						'neutered' => $neutered,
						'city' => $city,
						'postalcode' => $postalcode,
						'note' => $note,
					);
					//$this->load->view('login_user');
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/addpet_view',$data);

			}else{

			
			$customerclinic = $dataclinic[0]->customer;			
			$currentchip = $dataclinic[0]->chip;
			// if($rfid == ''){
			// 	$data = array(
			// 		'title' => 'Add Microchip - iVetdata.com',
			// 		'nama' => $nama,
			// 		'country' => $country,
			// 		//'city' => $city,
			// 		'profilephoto' => $profilephoto,
			// 		'role' => $role,'manageto' => $dataprofile[0]->manageto,
			// 		'datacountry' => $this->Login_model->country(),
			// 		'idclinic' => $idclinic,
			// 		'pettype' => $this->Pet_model->pettype(),
			// 		'nameclinic' => $dataclinic[0]->nameclinic,
			// 		'error' => 'Identification ID is empty, please fill Identification ID',
			// 		'microchip' => $microchip,
			// 		'namepet' => $namepet,
			// 		'type' => $type,
			// 		'color' => $color,
			// 		'note' => $description,
			// 		'nameowner' => $nameowner,
			// 		'phone' => $phone,
			// 		'email' => $email,
			// 		'address' => $address,
			// 		'breed' => $breed,
			// 		'datebirth' => $datebirth,
			// 		'country' => $country,
			// 		'gender' => $gender,
			// 		'neutered' => $neutered,
			// 		'city' => $city,
			// 		'postalcode' => $postalcode,
			// 		'note' => $note,
			// 	);
			// 	//$this->load->view('login_user');
			// 	$this->load->view('website/headerdash_view',$data);
			// 	$this->load->view('website/addpet_view',$data);
			// }else{
				if($microchip == '1'){
					$jumlahangkarfid = strlen($rfid);
				}else{
					$jumlahangkarfid = '20';
				}
				//var_dump($jumlahangkarfid);

				if($jumlahangkarfid < '15'){
					$data = array(
						'title' => 'Add Pet - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						//'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,'manageto' => $dataprofile[0]->manageto,
						'expdate' => $dataprofile[0]->expdate,
						'datacountry' => $this->Login_model->country(),
						'idclinic' => $idclinic,
						'pettype' => $this->Pet_model->pettype(),
						'nameclinic' => $dataclinic[0]->nameclinic,
						'error' => 'Please input the correct microchip number. Otherwise select "NO".',
						'microchip' => $microchip,
						'rfid' => $rfid,
						'namepet' => $namepet,
						'type' => $type,
						'color' => $color,
						'note' => $description,
						'nameowner' => $nameowner,
						'phone' => $phone,
						'email' => $email,
						'address' => $address,
						'breed' => $breed,
						'datebirth' => $datebirth,
						'country' => $country,
						'gender' => $gender,
						'neutered' => $neutered,
						'city' => $city,
						'postalcode' => $postalcode,
						'note' => $note,
					);
					//$this->load->view('login_user');
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/addpet_view',$data);
				}else{
			

			//cek RFID
			if($rfid == '0'){
				//$cekpetid = $this->Pet_model->cekpetid($rfid);
				$adapet = '0';
			}else{
				$cekpetid = $this->Pet_model->cekpetid($rfid);
				$adapet = count($cekpetid);
			}
			
			if($adapet == '0'){
				//$cekvalidchip = 
				//saving to customer
				//$cekcustomerclinic = $this->Pet_model->getcustomer($idclinic,$email);
				$cekcustomerclinic = $this->Pet_model->getcustomerhp($idclinic,$phone);
				$adacustomerclinic = count($cekcustomerclinic);
				if($adacustomerclinic == '0'){
					$user_email = $email;
					$adaemail = $this->Login_model->adaemail($user_email);
					if (count($adaemail) == '1'){
						$uid = $adaemail[0]->uid;
					}else{
						$pass = 'ivetdata';
						$datamember = array(
							'name' => $nameowner,
							'email' => $email,
							'phone' => $phone,
							'status' => '1',
							'role' => '0',
							'manageto' => '0',
							'password' => md5($pass),
							'tgl_reg' => date('Y-m-d h:i:s'),
							'unikcode' => md5($email),										
						);
						$this->db->insert('tbl_member',$datamember);
						$uid = $this->db->insert_id();
					}
					$datacustomer = array(
						'nama' => $nameowner,
						'nohp' => $phone,
						'email' => $email,
						'idclinic' => $idclinic,
						'city' => $city,
				//'profilephoto' => $profilephoto,
						'country' => $country,
						'address' => $address,	
						'uid' => $uid,
					);
					$this->db->insert('tbl_customer',$datacustomer);
					$idowner = $this->db->insert_id();
					$customerclinicfix = $customerclinic + 1; 
					//insertmember
					//add to db
				}else{
					$idowner = $cekcustomerclinic[0]->idcustomer;
					$customerclinicfix = $customerclinic + 0;
				};				

				$datapet = array(
					'rfid' => $rfid,
					'microchip' => $microchip,
					'idowner' => $idowner,
					'namapemilik' => $nameowner,
					'email' => $email,
					'nohp' => $phone,
					'tipe' => $type,
					'namapet' => $namepet,
					'city' => $city,
				//'profilephoto' => $profilephoto,
					'country' => $country,
					'address' => $address,
					'description' => $note,
					'input_by' => $profileid,
					'idclinic' => $idclinic,
					'breed' => $breed,
					'datebirth' => $newbirth,
					'color' => $color,
					'gender' => $gender,
					'neutered' => $neutered,
				);
				$this->db->insert('tbl_pet',$datapet);
				$idpet = $this->db->insert_id();
				$petclinicfix = $petclinic + 1;
				$chip = $currentchip - 1;
				$petclinicadd = array(
					'pet' => $petclinicfix,
					'customer' => $customerclinicfix,
					'chip' => $chip,
				);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_clinic',$petclinicadd);

                if($adacustomerclinic==0){
				    //sent email
				    //$subject = 'Hi '.$nameowner.', Your pet has been registered in '.$nameclinic.'';
				    $subject = 'Your Vet has registered you in iVetData';
				    $message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
					<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
					 <img height="70" src="https://app.ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
							 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$nameowner.', </h1>
							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">Hello '.$nameowner.',<br><br>You have been registered by '.$nameclinic.'!<br><br>View your Pet\'s medical record by activating your iVet Data account that links directly to your Vet\'s.<br><br>
							 	<strong>Your Login Detail</strong><br>
							 	Email : '.$email.'<br>
							 	Please login and edit your password.<br><br>
							 	Best Regards,<br>
							 	iVet Data Team<br>
							 </p>
							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">
							 <a href="'.base_url().'login/resetpass/?id='.$uid.'&email='.$email.'&token='.md5($email).'&reset=true" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank"">Login Now </a> </p>
							 </td> </tr>

				</tbody> </table> </td> </tr></tbody> </table>';
    				$text = $message;
    				//$email = $user_email;
    				//$this->load->view('testemail_view');
				
				    $this->load->library('EmailSender.php');
				    $this->emailsender->send($email,$subject,$text);		
				}


				$data = array(
					'title' => 'Home - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'success' => 'Add data pet is successful',
					'nameclinic' => $dataclinic[0]->nameclinic,
					'idpet' => $idpet,
					'uid' => $profileid,
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addphoto_view',$data);
				$this->load->view('website/footerdash_view',$data);
			}else{
				$data = array(
					'title' => 'Home - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'error' => 'This microchip ID is a duplicate to an existing Pet. Please double check the number.',
					'namepet' => $namepet,					
					'city' => $city,
					'profilephoto' => $profilephoto,
					'country' => $country,
					'address' => $address,
					'description' => $description,
					'nameowner' => $nameowner,
					'phone' => $phone,
					'email' => $email,
					'tipe' => $type,
					'breed' => $breed,
					'datebirth' => $datebirth,
					'pettype' => $this->Pet_model->pettype(),
					'nameclinic' => $dataclinic[0]->nameclinic,
					'color' => $color,
					'datebirth' => $datebirth,					
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			}	
			//}	
		}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}		
	}

	public function addphoto(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};	
			//config photo
			$config['upload_path']   = './upload/pet'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 10500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			$idpet = $this->input->post('idpet');
			$idclinic = $dataprofile[0]->idclinic;
			if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
			$dataclinic = $this->Home_model->detailclinic($idclinic);	
			if (!$this->upload->do_upload('petphoto')) {
				$data = array(
					'title' => 'Error Upload Image - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
				//'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'error' => $this->upload->display_errors(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
					'idpet' => $idpet,
					'uid' => $profileid,
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/addphoto_view',$data);
				$this->load->view('website/footerdash_view',$data);
			}else{			

				$result1 = $this->upload->data();
		   		$filephoto = $result1['file_name'];
		   		//upload to aws
				$keyname = $filephoto;
				$getpng = FCPATH."upload/pet/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$filephotourl = $data['upload_data'];

				$updatephoto = array(
					'photo' => $filephotourl ,
				);
				$this->db->where('idpet', $idpet);
				$this->db->update('tbl_pet',$updatephoto);
				$this->session->set_flashdata('success', 'Congratulations pet has been registered');
					// After that you need to used redirect function instead of load view such as 
				redirect("Pet/all");
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}	
	}

	public function edit(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};

			$idclinic = $dataprofile[0]->idclinic;
			if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$idpet = $this->input->get('id');
			$datapet = $this->Pet_model->detailpet($idpet);
            if($role>0){
		        if($datapet[0]->idclinic<>$idclinic)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
			}else{
			    if($datapet[0]->email==""){
			        if($profileid<>$datapet[0]->idowner)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
			    }else{
			        if($dataprofile[0]->email<>$datapet[0]->email)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
			    }
			}			
			//var_dump($datapet);
			$idcustomer = $datapet[0]->idowner;
			$datacustomer = $this->Customer_model->detailcustomer($idcustomer);
			//var_dump($datapet);							
			$data = array(
				'title' => 'Edit data pet - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,'manageto' => $dataprofile[0]->manageto,
				'expdate' => $dataprofile[0]->expdate,
				'pettype' => $this->Pet_model->pettype(),
				//'nameclinic' => $dataclinic[0]->nameclinic,
				'datapet' => $datapet,
				'success' => $this->session->flashdata('success'),
				'idcustomer' => $idcustomer,
				'customername' => $datapet[0]->namapemilik,
				'customerphone' => $datapet[0]->nohp,
				'customeremail' => $datapet[0]->email,
				'customeraddress' => $datapet[0]->address,	
				'customercity' => $datapet[0]->city,
				'customercountry' => $datapet[0]->country,
				'datacountry' => $this->Login_model->country(),
			);
			$this->load->view('website/header_new_view',$data);
			//var_dump($datapet);
			//$this->load->view('website/ownerdash_new_view',$data);
			$this->load->view('website/dashboard-owner-mypets-edit',$data);
			//$this->load->view('website/headerdash_view',$data);
		    $this->load->view('website/headerdash_view');
		    $this->load->view('website/footer_new_view');
			//$this->load->view('login_user');
			//$this->load->view('website/headerdash_view',$data);
			//$this->load->view('website/editpet_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}	
	}

	public function editprocess(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			//dataheader
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$microchip = $this->input->post('microchip');
			if ($microchip == '1') {
				$rfid = $this->input->post('rfid');
			}if($microchip == '0'){
				$rfid = $this->input->post('rfiduniq');
			}
			//data pet
			$idpet = $this->input->post('idpet');
			$rfid = $this->input->post('rfid');
			$namepet = $this->input->post('namepet');
			$type = $this->input->post('type');
			$color = $this->input->post('color');
			$description = $this->input->post('description');
			//$nameowner = $this->input->post('nameowner');
			//$idcustomer = $this->input->post('idcustomer');
			//$phone= $this->input->post('customerphone');
			//$email = $this->input->post('customeremail');			
			//$address = $this->input->post('customeraddress');
			//$city = $this->input->post('customercity');
			//$country = $this->input->post('customercountry');
			$breed = $this->input->post('breed');
			$gender = $this->input->post('gender');
			$neutered = $this->input->post('neutered');
			$datebirth = $this->input->post('dateofbirth');
			$newbirth = date('Y-m-d',strtotime($datebirth));
			$public_info = "0001111111";
			if(isset($_POST['displaypublic'])){
    			$permission = $this->input->post('displaypublic');
    			foreach($permission as $val){
    			    $public_info[$val]=1;
    			}
			}
			//config photo
			$config['upload_path']   = './upload/pet'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('petphoto')) {
			  	//$featured_imageupdate = $featured_image;
			   	$error_featured = $this->upload->display_errors();
			   	$petupdate = array(
			   		'rfid' => $rfid,					
					'tipe' => $type,
					'namapet' => $namepet,
					//'description' => $description,
					'input_by' => $profileid,
					'breed' => $breed,
					'datebirth' => $newbirth,
					'color' => $color,
					'gender' => $gender,
					'neutered' => $neutered, 
					//'namapemilik' => $nameowner,
					//'address' => $address,
					//'email' => $email,
					//'nohp' => $phone,
					'city' => $city,
					'country' => $country,
					'microchip' => $microchip,	
					'public_info' => $public_info,
			   	);
			   	$this->db->where('idpet',$idpet);
				$this->db->update('tbl_pet',$petupdate);

				if($role > 0){
				
					//updatedatacustomer
					// $updatecustomer = array(
					// 	'nama' => $nameowner,
					// 	'address' => $address,
					// 	'email' => $email,
					// 	'nohp' => $phone,
					// 	'city' => $city,
					// 	'country' => $country,
					// );
					// $this->db->where('idcustomer',$idcustomer);
					// $this->db->update('tbl_customer',$updatecustomer);
					
					// //update pet table
					// $updatepetcustomer = array(
					// 	'namapemilik' => $nameowner,
					// 	'email' => $email,
					// 	'nohp' => $phone,
					// 	'city' => $city, 
					// 	'address' => $address,
					// 	'country' => $country
					// );
					// //$this->db->where('idpet',$idpet);
					// $this->db->where('idowner',$idcustomer);
					// $this->db->update('tbl_pet',$updatepetcustomer);

					//update member table
					$updatemember = array(
						'name' => $nameowner,
						'email' => $email,
						'phone' => $phone,
						'city' => $city, 
						//'address' => $address,
						'country' => $country
					);
					$this->db->where('uid',$idcustomer);
					$this->db->update('tbl_member',$updatemember);

				}else{

				}
				
				
				$this->session->set_flashdata('success', 'Congrats, Data has been Updated '.$error_featured);
				//$this->session->set_flashdata('success', 'Congrats, Data has been Updated '.$error_featured);
				// After that you need to used redirect function instead of load view such as 
				redirect(base_url()."dashboard/petowner");		    	
			}else{
				$result1 = $this->upload->data();
		   		$filephoto = $result1['file_name'];
		   		//upload to aws
				$keyname = $filephoto;
				//$getpng = FCPATH."upload/pet/".$keyname;
				$getpng = base_url().'./upload/pet/'.$keyname;
				$degrees = $this->input->post('rotate');
				if($degrees<>0){
                    // File and rotation
                    $rotateFilename = $getpng; // PATH
                    $degrees = 360 - $degrees;
                    $fileType = strtolower(substr($keyname, strrpos($keyname, '.') + 1));
                    
                    if($fileType == 'png'){
                       $source = imagecreatefrompng($rotateFilename);
                       $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
                       // Rotate
                       $rotate = imagerotate($source, $degrees, $bgColor);
                       imagesavealpha($rotate, true);
                       imagepng($rotate,$rotateFilename);
                    
                    }
                    
                    if($fileType == 'jpg' || $fileType == 'jpeg'){
                       $source = imagecreatefromjpeg($rotateFilename);
                       // Rotate
                       $rotate = imagerotate($source, $degrees, 0);
                       imagejpeg($rotate,$rotateFilename);
                    }
				}				
				
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$filephotourl = $data['upload_data'];
				$updatephoto = array(
					'rfid' => $rfid,					
					'tipe' => $type,
					'namapet' => $namepet,
					//'description' => $description,
					'input_by' => $profileid,
					'breed' => $breed,
					'datebirth' => $newbirth,
					'color' => $color, 
					'photo' => $filephotourl,
					//'namapemilik' => $nameowner,
					// 'address' => $address,
					// 'email' => $email,
					// 'nohp' => $phone,
					'city' => $city,
					'country' => $country,
					'microchip' => $microchip,
				);
				$this->db->where('idpet', $idpet);
				$this->db->update('tbl_pet',$updatephoto);

				// //updatedatacustomer
				// $updatecustomer = array(
				// 	'nama' => $nameowner,
				// 	'address' => $address,
				// 	'email' => $email,
				// 	'nohp' => $phone,
				// 	'city' => $city,
				// 	'country' => $country,
				// );
				// $this->db->where('idcustomer',$idcustomer);
				// $this->db->update('tbl_customer',$updatecustomer);

				//update pet table
				// $updatepetcustomer = array(
				// 	'namapemilik' => $nameowner,
				// 	'email' => $email,
				// 	'nohp' => $phone,
				// 	'city' => $city, 
				// 	'address' => $address,
				// 	'country' => $country
				// );
				// //$this->db->where('idpet',$idpet);
				// $this->db->where('idowner',$idcustomer);
				// $this->db->update('tbl_pet',$updatepetcustomer);

				//update member table
				// $updatemember = array(
				// 	'name' => $nameowner,
				// 	'email' => $email,
				// 	'phone' => $phone,
				// 	'city' => $city, 
				// 	//'address' => $address,
				// 	'country' => $country
				// );
				// $this->db->where('uid',$idcustomer);
				// $this->db->update('tbl_member',$updatemember);
				
				$this->session->set_flashdata('success', 'Congrats, Data has been Updated');
				// After that you need to used redirect function instead of load view such as 
				redirect(base_url()."dashboard/petowner");
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function delete(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idpet = $this->input->get('idpet');
			$datapet = $this->Pet_model->detailpet($idpet);
			$idclinic = $datapet[0]->idclinic;
			if($vetclinic == $idclinic){
				//$edit = '1';
				$dataupdate = array(
					'status' => '1',
				);
				$this->db->where('idpet',$idpet);
				$this->db->update('tbl_pet',$dataupdate);
				$this->session->set_flashdata('success', 'Congrats, Pet has been deleted');
				redirect ('pet/all');
			}else{
				$this->session->set_flashdata('error', 'You canot delete this pet');
				redirect ('pet/all');
				//$edit = '0';
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function detail(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;
			$phone = $dataprofile[0]->phone;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$myclinic = $dataprofile[0]->idclinic;
			$idpet = $this->input->get('idpet');
			$rfid = $idpet;
			$datapet = $this->Pet_model->detailpet($idpet);
			$adapet = count($datapet);
			if($adapet == '0'){
				// $data = array(
				// 	'title' => 'Detail Pet - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'error' => 'Sorry, we cannot find pet id',
				// );
				// //$this->load->view('login_user');
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/nopet_view',$data);
				// $this->load->view('website/footerdash_view',$data);
				$this->session->set_flashdata('error', 'Sorry pet not found.');
				if($role == '0'){
					redirect ('/dashboard/petowner');
				}else{
					redirect ('pet/all');
				}

			}else{
				$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;
				$emailowner = $datapet[0]->email;
				$phoneowner = $datapet[0]->nohp;
				$gender = $datapet[0]->gender;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $country = $datapet[0]->country; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            if($agey==0){
	                $age =  $agem.' Month';
	            }else{
	                $age =  $agey.' Year, '.$agem.' Month';
	            }
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'images/'.$tipe.'_default.png';
	            }else{
	                //$petphoto = base_url().'upload/pet/'.$photo;
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				if($role>0){
				    if($vetclinic<>$idclinic)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
				}else{
				    if($emailowner==""){
				        if($profileid<>$datapet[0]->idowner)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
				    }else{
				        if($email<>$emailowner)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
				    }
				}
				$dataclinicpet = $this->Pet_model->detailclinic($idclinic);
				if(count($dataclinicpet) == '0'){
					$namaclinic = '';
					$phoneclinic = '';
					$addressclinic = '';
					$cityclinic = '';
					$countryclinic = '';
					$emailclinic = '';					
				}else{
					$namaclinic =  $dataclinicpet[0]->nameclinic;
					$phoneclinic = $dataclinicpet[0]->phone;
					$addressclinic = $dataclinicpet[0]->address;
					$cityclinic = $dataclinicpet[0]->city;
					$countryclinic = $dataclinicpet[0]->country;
					$emailclinic = $dataclinicpet[0]->email;
				}
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				if($role == '0'){
					if($email != $emailowner){
						$this->session->set_flashdata('error', 'Sorry this pet not yours.');
						redirect ('/dashboard/petowner');
					}	
				}
				
				$data = array(
					'title' => 'Detail Pet - iVetdata.com',
					'nama' => $nama,
					'email' => $email,
					'emailowner' => $emailowner,
					'country' => $country,
					'profileid' => $profileid,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'team' => $dataprofile[0]->team,
					'expdate' => $dataprofile[0]->expdate,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'gender' => $gender,
					'color' => $color,
					'edit' => $edit,
					'datebirth' => $datebirth,
					'age' => $age,
					'country' => $country,
					'petphoto' => $petphoto,
					'namaclinic' => $namaclinic,
					'phoneclinic' => $phoneclinic,
					'emailclinic' => $emailclinic,
					'addressclinic' => $addressclinic,
					'cityclinic' => $cityclinic,
					'countryclinic' => $countryclinic,

					'dateadd' => $datapet[0]->dateadd,
					'logpet' => $this->Pet_model->logpet($idpet),
					'logvaccine' => $this->Pet_model->logvaccine($idpet),
					'lastvaccine' => $this->Pet_model->lastvaccine($idpet),
					//'countdoctor' => $countdoctor,
					'datapet' => $datapet,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					'myclinic' => $myclinic,
					'idclinic' => $idclinic,
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view');
				//$this->load->view('website/homedash_view');
				$this->load->view('website/dashboard-vet-mypets-detail',$data);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/footer_new_view');
				//$this->load->view('website/detailpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function microchip(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;			
			$vetclinic = $dataprofile[0]->idclinic;
			$myclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idpet = $this->input->get('idpet');
			if($idpet == ''){
				$this->session->set_flashdata('error', 'Sorry pet not found.');
				if($role == '0'){
					redirect ('/dashboard/petowner');
				}else{
					redirect ('pet/all');
				}
			}
			$rfid = $idpet;
			$datapet = $this->Pet_model->detailmicrochip($idpet);
			$adapet = count($datapet);
			if($adapet == '0'){
				// $data = array(
				// 	'title' => 'Detail Pet - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'error' => 'Sorry, we cannot find pet id',
				// );
				// //$this->load->view('login_user');
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/nopet_view',$data);
				// $this->load->view('website/footerdash_view',$data);
				$this->session->set_flashdata('error', 'Sorry pet not found.');
				if($role == '0'){
					redirect ('/dashboard/petowner');
				}else{
					redirect ('pet/all');
				}

			}else{
				$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;
				$emailowner = $datapet[0]->email;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $country = $datapet[0]->country; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				$dataclinicpet = $this->Pet_model->detailclinic($idclinic);
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				// if($idowner != $profileid){
				// 	//if($email != $emailowner){
				// 		redirect ('dashboard/petowner');
				// 	//}	
				// }
				
				$data = array(
					'title' => 'Detail Pet - iVetdata.com',
					'nama' => $nama,
					'email' => $email,
					'emailowner' => $emailowner,
					'country' => $country,
					'profileid' => $profileid,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'country' => $country,
					'petphoto' => $petphoto,
					'namaclinic' => $dataclinicpet[0]->nameclinic,
					'phoneclinic' => $dataclinicpet[0]->phone,
					'addressclinic' => $dataclinicpet[0]->address,
					'cityclinic' => $dataclinicpet[0]->city,
					'countryclinic' => $dataclinicpet[0]->country,
					'emailclinic' => $dataclinicpet[0]->email,
					'dateadd' => $datapet[0]->dateadd,
					'logpet' => $this->Pet_model->logpet($idpet),
					'logvaccine' => $this->Pet_model->logvaccine($idpet),
					'lastvaccine' => $this->Pet_model->lastvaccine($idpet),
					//'countdoctor' => $countdoctor,
					'datapet' => $datapet,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					'myclinic' => $myclinic,
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/detailpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function result(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;			
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$phone = $this->input->get('phone');
			$emailpet = $this->input->get('emailpet');
			//$rfid = $idpet;
			//$datapet = $this->Pet_model->cekpetid($idpet);
			if($phone == ''){
				$datapet = $this->Pet_model->cekpetbyemail($emailpet,$vetclinic);
				if(count($datapet)==0)$datapet = $this->Pet_model->cekpetbyname($vetclinic,$emailpet);
			}else{
				$datapet = $this->Pet_model->cekpetbyphone($phone,$vetclinic,$emailpet);
			}
			
			$adapet = count($datapet);
			//var_dump($datapet);
			if($adapet == '0'){
				// $data = array(
				// 	'title' => 'Detail Pet - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'error' => 'Sorry, we cannot find pet id',
				// );
				// //$this->load->view('login_user');
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/nopet_view',$data);
				// $this->load->view('website/footerdash_view',$data);
				$this->session->set_flashdata('error', 'Sorry pet not found.');
				if($role == '0'){
					redirect ('/dashboard/petowner');
				}else{
					redirect ('pet/all');
				}

			}else{
				$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;
				$emailowner = $datapet[0]->email;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $country = $datapet[0]->country; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = base_url().'upload/pet/'.$photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				$dataclinicpet = $this->Pet_model->detailclinic($idclinic);
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				if($role == '0'){
					if($email != $emailowner){
						redirect ('/dashboard/petowner');
					}	
				}
				
				$data = array(
					'title' => 'Detail Pet - iVetdata.com',
					'nama' => $nama,
					'email' => $email,
					'emailowner' => $emailowner,
					'country' => $country,
					'profileid' => $profileid,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'country' => $country,
					'petphoto' => $petphoto,
					'namaclinic' => $dataclinicpet[0]->nameclinic,
					'phoneclinic' => $dataclinicpet[0]->phone,
					'addressclinic' => $dataclinicpet[0]->address,
					'cityclinic' => $dataclinicpet[0]->city,
					'emailclinic' => $dataclinicpet[0]->email,
					'dateadd' => $datapet[0]->dateadd,
					'logpet' => $this->Pet_model->logpet($idpet),
					'logvaccine' => $this->Pet_model->logvaccine($idpet),
					'lastvaccine' => $this->Pet_model->lastvaccine($idpet),
					//'countdoctor' => $countdoctor,
					'datapet' => $datapet,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/mypet_view',$data);
				//$this->load->view('website/detailpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function search(){
	    $isclinic = FALSE;
		$idpet = $this->input->get('idpet');
		if(strlen($idpet)>25)die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
		if($idpet == ''){
			$data = array(
				'title' => 'Search pet - iVetdata.com',	
				//'datapet' => $datapet,
				'ada' => '0',	
				//'rfid' => $rfid,
			);
			//$this->load->view('login_user');
			$this->load->view('website/header_new_view',$data);
			$this->load->view('website/searchpetfront_new_view',$data);
			$this->load->view('website/footer_new_view',$data);
		}else{
		
		
		$rfid = $idpet;
		$datapet = $this->Pet_model->cekpetid($rfid);
		if(count($datapet) == '0'){
			$data = array(
				'title' => 'Search pet - iVetdata.com',	
				//'datapet' => $datapet,
				'ada' => '0',	
				'rfid' => $rfid,
			);
			//$this->load->view('login_user');
			$this->load->view('website/header_new_view',$data);
			$this->load->view('website/searchpetfront_new_view',$data);
			$this->load->view('website/footer_new_view',$data);
			
		}else{
			
				$idclinic = $datapet[0]->idclinic;
				if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
				$dataclinic = $this->Pet_model->detailclinic($idclinic);
				if(count($dataclinic)==0){
				    $isclinic = FALSE;
				    $dataclinic = $this->Pet_model->detailvet($datapet[0]->idowner);
				    if(isset($dataclinic[0])){
				        $nameclinic = $dataclinic[0]->name;
				        $clinic_info = $dataclinic[0]->public_info;
				        $phonevet = $dataclinic[0]->phone;
    			        $email = $dataclinic[0]->email;
    			        $address = $dataclinic[0]->address;
    			        $city = $dataclinic[0]->city;
				    }else{
				        $dataclinic = $this->Pet_model->datacustomer($datapet[0]->idowner);
				        if(isset($dataclinic[0])){
				            $nameclinic = $dataclinic[0]->nameclinic;
				            $clinic_info = $dataclinic[0]->public_info;  
				            $phonevet =  $dataclinic[0]->nohp;
        			        $email = $dataclinic[0]->email;
        			        $address = $dataclinic[0]->address;
        			        $city = $dataclinic[0]->city;
				        }else{
				            $nameclinic = "";
				            $clinic_info = "1111111111";
				            $phonevet = "";
				            $email = "";
				            $address = "";
				            $city = "";
				        }
				    }
				}else{
				    $isclinic = TRUE;
				    $nameclinic = $dataclinic[0]->nameclinic;
				    $clinic_info = $dataclinic[0]->public_info;
				    $phonevet =  $dataclinic[0]->phone;
			        $email = $dataclinic[0]->email;
			        $address = $dataclinic[0]->address;
			        $city = $dataclinic[0]->city;
				}
				$vet = $datapet[0]->input_by;	
				$idpet = $datapet[0]->idpet;
				$datavet = $this->Pet_model->detailvet($idvet);
				$address =  $dataclinic[0]->address.' '.$dataclinic[0]->city.'';
				if($datavet[0]->role==0){
				    $isclinic=FALSE;
				    $clinic_info = $datavet[0]->public_info;
				}
				$tipe = $datapet[0]->tipe;
				$photo = $datapet[0]->photo;
				$petname = $datapet[0]->namapet ;
	 			$rfid = $datapet[0]->rfid;
			
			
	        if($photo =='' ){
	            $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	        }else{
	            $petphoto = $photo;
	        };	
			$data = array(
				'title' => 'Search pet - iVetdata.com',	
				'datapet' => $datapet,
				'ada' => '1',	
				'petname' => $petname,
				'rfid' => $rfid,
				'petphoto' => $petphoto,
				'namevet' => $datavet[0]->name,
				'isclinic' => $isclinic,
				'clinic' => $nameclinic,
				'phonevet' => $phonevet,
				'lastvaccine' => $this->Pet_model->lastvaccine($idpet),
				'address' => $address,
				'email' => $email,
				'public_info' => $datapet[0]->public_info,
				'clinic_info' => $clinic_info,
			);
			//$this->load->view('login_user');
			$this->load->view('website/header_new_view',$data);
			$this->load->view('website/searchpetfront_new_view',$data);
			if (isset($this->session->userdata['logged_in'])){
			    $this->load->view('website/headerdash_view',$data);
			}
			$this->load->view('website/footer_new_view',$data);
		}
		
		}
		
	}
	
	public function searchnew(){
		$idpet = $this->input->get('idpet');
		if(strlen($idpet)>25)die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
		if($idpet == ''){
			$data = array(
				'title' => 'Search pet - iVetdata.com',	
				//'datapet' => $datapet,
				'ada' => '0',	
				//'rfid' => $rfid,
			);
			//$this->load->view('login_user');
			//$this->load->view('website/header_new_view',$data);
			$this->load->view('website/searchpetfront_new__new_view',$data);
			//$this->load->view('website/footer_new_view',$data);
		}else{
		
		
		$rfid = $idpet;
		$datapet = $this->Pet_model->cekpetid($rfid);
		if(count($datapet) == '0'){
			$data = array(
				'title' => 'Search pet - iVetdata.com',	
				//'datapet' => $datapet,
				'ada' => '0',	
				'rfid' => $rfid,
			);
			//$this->load->view('login_user');
			//$this->load->view('website/header_new_view',$data);
			$this->load->view('website/searchpetfront_new_new_view',$data);
			//$this->load->view('website/footer_new_view',$data);
		}else{
			
				$idclinic = $datapet[0]->idclinic;
				if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
				$dataclinic = $this->Pet_model->detailclinic($idclinic);
				if(count($dataclinic)==0){
				    $dataclinic = $this->Pet_model->detailvet($datapet[0]->idowner);
				    if(isset($dataclinic[0])){
				        $nameclinic = $dataclinic[0]->name;
				        $clinic_info = $dataclinic[0]->public_info;
				        $phonevet = $dataclinic[0]->phone;
				    }else{
				        $dataclinic = $this->Pet_model->datacustomer($datapet[0]->idowner);
				        if(isset($dataclinic[0])){
				            $nameclinic = $dataclinic[0]->nameclinic;
				            $clinic_info = $dataclinic[0]->public_info;  
				            $phonevet =  $dataclinic[0]->nohp;
				        }else{
				            $nameclinic = "";
				            $clinic_info = "1111111111";
				            $phonevet = "";
				        }
				    }
				}else{
				    $nameclinic = $dataclinic[0]->nameclinic;
				    $clinic_info = $dataclinic[0]->public_info;
				    $phonevet =  $dataclinic[0]->phone;
				}
				$idvet = $datapet[0]->input_by;	
				$idpet = $datapet[0]->idpet;
				$datavet = $this->Pet_model->detailvet($idvet);
				$tipe = $datapet[0]->tipe;
				$photo = $datapet[0]->photo;
				$petname = $datapet[0]->namapet ;
	 			$rfid = $datapet[0]->rfid;
			
			
	        if($photo =='' ){
	            $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	        }else{
	            $petphoto = $photo;
	        };	
			$data = array(
				'title' => 'Search pet - iVetdata.com',	
				'datapet' => $datapet,
				'ada' => '1',	
				'petname' => $petname,
				'rfid' => $rfid,
				'petphoto' => $petphoto,
				'namevet' => $datavet[0]->name,
				'clinic' => $nameclinic,
				'phonevet' => $phonevet,
				'lastvaccine' => $this->Pet_model->lastvaccine($idpet),
				'address' => $dataclinic[0]->address.' '.$dataclinic[0]->city.'',
				'email' => $dataclinic[0]->email,
				'pet_info' => $datapet[0]->public_info,
				'clinic_info' => $clinic_info,
			);
			//$this->load->view('login_user');
			//$this->load->view('website/header_new_view',$data);
			$this->load->view('website/searchpetfront_new_new_view',$data);
			//$this->load->view('website/footer_new_view',$data);
		}
		
		}
		
	}

	public function addmedical(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$idclinic = $dataprofile[0]->idclinic;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idpet = $this->input->post('idpet');
			$detailpet = $this->Pet_model->detailpet($idpet);
			$rfid = $detailpet[0]->rfid;
			$diagnose = $this->input->post('diagnose');
			$jam = date(' H:i:s');
			$dateadd = $this->input->post('dateadd').$jam;
			$anamnesis = $this->input->post('anamnesis');
			$weight = $this->input->post('weight');
			$temperature = $this->input->post('temperature');
			$heartrate = $this->input->post('heartrate');
			$resprate = $this->input->post('resprate');
			$bcs = $this->input->post('bcs');
			$food = $this->input->post('food');
			$eyes = $this->input->post('eyes');
			$skin = $this->input->post('skin');
			$resp = $this->input->post('resp');
			$circ = $this->input->post('circ');
			$dige = $this->input->post('dige');
			$urin = $this->input->post('urin');
			$nerv = $this->input->post('nerv');
			$musc = $this->input->post('musc');
			$nextdate = $this->input->post('nextdate');
			if($nextdate==""){
				$nextdate = '0000-00-00 00:00:00';
			}else{
				$nextdate = date('Y-m-d H:i:s',strtotime($nextdate));
			}
			$othernotes = $this->input->post('othernotes');
			$hemanotes = $this->input->post('hemanotes');
			
			$bloodnotes = $this->input->post('bloodnotes');
			$ultrasononotes = $this->input->post('ultrasononotes');
			$xraynotes = $this->input->post('xraynotes');
			$othernotes = $this->input->post('othernotes');
			$otherdiagnotes = $this->input->post('otherdiagnotes');
			$treatment = $this->input->post('treatment');
			//$drugs = $this->input->post('drugs');
			$treattot = 0;
			$val = "";
			foreach($treatment as $value) {
				$val = $val . $value .  "<br>";
				$treattot++;
			}
			$treatment = substr($val,0,-4);

			//$detailinsert 
			$config['upload_path']   = './upload/pet/medrec'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			//upload hema
			if (!$this->upload->do_upload('hema')) {
				$pichemaaws = '';
			}else{
				$hema = $this->upload->data();
				$pichema = $hema['file_name'];
				//upload to aws
				$keyname = $pichema;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$pichemaaws = $data['upload_data'];

			};
			//upload blood
			if (!$this->upload->do_upload('blood')) {
				$picbloodaws = '';
			}else{
				$blood = $this->upload->data();
				$picblood = $blood['file_name'];
				//upload to aws
				$keyname = $picblood;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picbloodaws = $data['upload_data'];
			};
			//upload xray
			if (!$this->upload->do_upload('xray')) {
				$picxrayaws = '';
			}else{
				$xray = $this->upload->data();
				$picxray = $xray['file_name'];
				//upload to aws
				$keyname = $picxray;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picxrayaws = $data['upload_data'];
			};
			//upload Ultrasono
			if (!$this->upload->do_upload('ultrasono')) {
				$picultrasonoaws = '';
			}else{
				$ultrasono = $this->upload->data();
				$picultrasono = $ultrasono['file_name'];
				//upload to aws
				$keyname = $picultrasono;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picultrasonoaws = $data['upload_data'];
			};
			//upload Otherdiag
			if (!$this->upload->do_upload('otherdiag')) {
				$picotherdiagaws = '';
			}else{
				$otherdiag = $this->upload->data();
				$picotherdiag = $otherdiag['file_name'];
				//upload to aws
				$keyname = $picotherdiag;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picotherdiagaws = $data['upload_data'];
			};

			//inserttodb
			$datainsert = array(
				'idpet' => $idpet,
				'tgllog' => date('Y-m-d H:i:s',strtotime($dateadd)),
				'namevets' => $nama,
				//'title' => $this->input->post('title'),
				'diagnose' => $diagnose,
				'idclinic' => $idclinic,
				'idvets' => $profileid,
				'anamnesis' => $anamnesis,
				'weight' => $weight,
				'temperature' => $temperature,
				'heartrate' => $heartrate,
				'resprate' => $resprate,
				'bcs' => $bcs,
				'food' => $food,
				'eyes' => $eyes,
				'skin' => $skin,
				'resp' => $resp,
				'circ' => $circ,
				'dige' => $dige,
				'urin' => $urin,
				'nerv' => $nerv,
				'musc' => $musc,
				'othernotes' => $othernotes,
				'treatment' => $treatment,
				//'drugs' => $drugs,
				'hema' => $pichemaaws,
				'blood' => $picbloodaws,
				'xray' => $picxrayaws,
				'ultrasono' => $picultrasonoaws,
				'otherdiag' => $picotherdiagaws,
				'hemanotes' => $hemanotes,
				'bloodnotes' => $bloodnotes,
				'ultrasononotes' => $ultrasononotes,
				'xraynotes' => $xraynotes,
				'otherdiagnotes' => $otherdiagnotes,
				'nextdate' => $nextdate,
			);
			$this->db->insert('tbl_logpet',$datainsert);
			$idvet = $this->Home_model->datadokter($profileid);
			$datainsert = array(
			    'vaccine' => $treatment,
				'idpet' => $idpet,
				'idclinic' => $idclinic,
				'idvet' => $idvet[0]->iddokter,
				'vetname' => $nama,
				'datevacc' => date('Y-m-d H:i:s',strtotime($dateadd)),
				'description' => $diagnose,
				'status' => 0,
				'nextdate' => $nextdate,
				'idowner' => $profileid,
				'nameowner' => $dataprofile[0]->name,
				'emailowner' => $dataprofile[0]->email,
				'phoneowner' => $dataprofile[0]->phone,
				'category' => 'medical',
			);
			$this->db->insert('tbl_vaccine',$datainsert);
			$this->session->set_flashdata('success', 'Medical history has been added');
			if($treattot>0){
				echo '<form name="form1" action="/sales/invoice" method="post">';
				$treatment = $this->input->post('treatment');
				foreach($treatment as $value) {
					echo '<input type="hidden" class="chooseservice form-control item ui-autocomplete-input" placeholder="Treatment" name="treatment[]" autocomplete="off" value="'.$value.'">';			
				}				
				$prc = $this->input->post('price');
				foreach($prc as $value) {
					echo '<input class="form-control price" name="price[]" placeholder="Price" type="hidden" value="'.$value.'" required>"';
				}
				$idcustomer = $detailpet[0]->idowner;
				$detailcustomer = $this->Customer_model->detailcustomer($idcustomer);
				$namecustomer = $detailcustomer[0]->nama;
				$emailcustomer = $detailcustomer[0]->email;
				$hpcustomer = $detailcustomer[0]->nohp;
				$vetdata = $this->Pet_model->byownerphone($detailcustomer[0]->nohp);
				//$idpet = $pet->idpet;
				//$detailpet = $this->Pet_model->detailpet($idpet);
				$namapet = $detailpet[0]->namapet;
				if (count($detailpet)>0) {
					foreach ($detailpet as $pet){
					 	echo '<input type="hidden" name="petdata[]" value="'.$pet->idpet.';'.$pet->namapet.'" >';
					}
				}else{
					echo '<input type="hidden" name="petdata[]" value=" ; " >';
				}
				echo '<input type="hidden" name="namecustomer" value="'.$namecustomer.'" >';
				echo '<input type="hidden" name="emailcustomer" value="'.$emailcustomer.'" >';
				echo '<input type="hidden" name="idpet" value="'.$idpet.'" >';
				echo '<input type="hidden" name="namapet" value="'.$namapet.'" >';
				echo '<input type="hidden" name="autofill" value="1" >';
				echo '<input type="hidden" name="hpcustomer" value="'.$hpcustomer.'" >';
				echo "</form>";
				echo '<script>document.form1.submit();</script><h2>Please enable javascript</h2>';
			}else{
			    redirect ('/pet/detail/?idpet='.$idpet);
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	function breedlist(){
        if (isset($_GET['term'])) {
            $result = $this->Pet_model->search_blog($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->breed;
                echo json_encode($arr_result);
            }
        }
    }

    public function logdetail(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idlog = $this->input->get('idlog');
			$idpet = $this->input->get('idpet');
			//$rfid = $idpet;
			$datalog = $this->Pet_model->ceklogid($idlog,$idpet);
			//var_dump($datalog);
			$adalog = count($datalog);
			if($adalog == '0'){
				$data = array(
					'title' => 'Medical history - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'error' => 'Sorry, we can not find medical history',
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				$this->load->view('website/404_new',$data);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/footer_new_view',$data);

			}else{
				$datapet = $this->Pet_model->detailpet($idpet);
				if($role>0){
				    if($vetclinic<>$datapet[0]->idclinic)die("<script>alert('Sorry you can not access this page');self.location.href='/';</script><h2>Please enable javascript</h2>");
    			}else{
    			    if($datapet[0]->email==""){
    			        if($profileid<>$datapet[0]->idowner)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
    			    }else{
    			        if($dataprofile[0]->email<>$datapet[0]->email)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
    			    }
    			}
				//$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				$data = array(
					'title' => 'Detail Pet - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'idlog' => $idlog,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'petphoto' => $petphoto,
					'dateadd' => $datapet[0]->dateadd,
					'datalog' => $datalog,
					//'logpet' => $this->Pet_model->logpet($idpet),
					//'countdoctor' => $countdoctor,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				$this->load->view('website/dashboard-owner-mypets-log-detail',$data);
		        $this->load->view('website/headerdash_view');
		        $this->load->view('website/footer_new_view');
				//$this->load->view('website/detaillogpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function vaccdetail(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idvacc = $this->input->get('idvacc');
			$idpet = $this->input->get('idpet');
			//$rfid = $idpet;
			$datavacc = $this->Pet_model->lastvaccine($idpet);
			//var_dump($datalog);
			$adalog = count($datavacc);
			if($adalog == '0'){
				$data = array(
					'title' => 'Vaccine history - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'error' => 'Sorry, we can not find medical history',
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				$this->load->view('website/404_new',$data);
		        $this->load->view('website/headerdash_view');
		        $this->load->view('website/footer_new_view');

			}else{
				$datapet = $this->Pet_model->detailpet($idpet);
				if($role>0){
				    if($vetclinic<>$datapet[0]->idclinic)die("<script>alert('Sorry you can not access this page');self.location.href='/';</script><h2>Please enable javascript</h2>");
    			}else{
    			    if($datapet[0]->email==""){
    			        if($profileid<>$datapet[0]->idowner)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
    			    }else{
    			        if($dataprofile[0]->email<>$datapet[0]->email)die("<script>alert('Sorry, you are not authorized to see the page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
    			    }
    			}				
				//$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				$data = array(
					'title' => 'Detail Vaccine - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'idvacc' => $idvacc,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'petphoto' => $petphoto,
					'dateadd' => $datapet[0]->dateadd,
					'datavacc' => $datavacc,
					//'logpet' => $this->Pet_model->logpet($idpet),
					//'countdoctor' => $countdoctor,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				$this->load->view('website/dashboard-owner-mypets-vacc-detail',$data);
		        $this->load->view('website/headerdash_view');
		        $this->load->view('website/footer_new_view');
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}
	
	public function editlog(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idlog = $this->input->get('idlog');
			$idpet = $this->input->get('idpet');
			//$rfid = $idpet;
			$datalog = $this->Pet_model->ceklogid($idlog,$idpet);
			//var_dump($datalog);
			$adalog = count($datalog);
			if($adalog == '0'){
				$data = array(
					'title' => 'Medical history - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'error' => 'Sorry, we can not find medical history',
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/nopet_view',$data);
				$this->load->view('website/footerdash_view',$data);

			}else{
				$datapet = $this->Pet_model->detailpet($idpet);
				if($vetclinic<>$datapet[0]->idclinic||$role<1)die("<script>alert('Sorry you can not access this page');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
				//$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				$treatment = array();
				foreach ($datalog as $log) {
				    if(stristr($log->treatment,"<br>")){
					    $treatment = explode('<br>',$log->treatment);
				    }else{
				        $treatment[0] = $log->treatment;
				    }
				}
				$name_treat = array();
				$price_treat = array();
				$data_treat = array();
				if($treatment<>""){
    				for($i=0;$i<count($treatment);$i++){
    					$data_treat = $this->Service_model->cekservice(trim($treatment[$i],' '),$idclinic);
    					if(isset($data_treat[0])){
    					    $name_treat[$i] = $data_treat[0]->nameservice;
    					    $price_treat[$i] = $data_treat[0]->price;
    					}else{
    					    $name_treat[$i] = $treatment[$i];
    					    $price_treat[$i] = 0;
    					}
    				}
				}else{
				    $name_treat[0] = "";
					$price_treat[0] = 0;
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				$data = array(
					'title' => 'Edit Medical History - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'idlog' => $idlog,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'petphoto' => $petphoto,
					'dateadd' => $datapet[0]->dateadd,
					'datalog' => $datalog,
					'name_treat' => $name_treat,
					'price_treat' => $price_treat,
					//'logpet' => $this->Pet_model->logpet($idpet),
					//'countdoctor' => $countdoctor,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/editdetaillogpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	//updatelog
	public function updatelog(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};

			//datalog
			$idpet = $this->input->post('idpet');
			$idlog = $this->input->post('idlog');
			$detailpet = $this->Pet_model->detailpet($idpet);
			$rfid = $detailpet[0]->rfid;
			$diagnose = $this->input->post('diagnose');
			$jam = date('H:i:s');
			$dateadd = $this->input->post('datelog').$jam;
			$anamnesis = $this->input->post('anamnesis');
			$weight = $this->input->post('weight');
			$temperature = $this->input->post('temperature');
			$heartrate = $this->input->post('heartrate');
			$resprate = $this->input->post('resprate');
			$bcs = $this->input->post('bcs');
			$food = $this->input->post('food');
			$eyes = $this->input->post('eyes');
			$skin = $this->input->post('skin');
			$resp = $this->input->post('resp');
			$circ = $this->input->post('circ');
			$dige = $this->input->post('dige');
			$urin = $this->input->post('urin');
			$nerv = $this->input->post('nerv');
			$musc = $this->input->post('musc');
			$othernotes = $this->input->post('othernotes');
			$hemanotes = $this->input->post('hemanotes');
			
			$bloodnotes = $this->input->post('bloodnotes');
			$ultrasononotes = $this->input->post('ultrasononotes');
			$xraynotes = $this->input->post('xraynotes');
			$otherdiagnotes = $this->input->post('otherdiagnotes');
			$treat = $this->input->post('treatment');
			//$drugs = $this->input->post('drugs');
			$treattot = 0;
			$treatment = "";
			foreach($treat as $value) {
				$treatment .= $value . "<br>";
				$treattot++;
			}
			$treatment = substr($treatment,0,-4);

			//$detailinsert 
			$config['upload_path']   = './upload/pet/medrec'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			//upload hema
			if (!$this->upload->do_upload('hema')) {
				$pichemaaws = $this->input->post('urlhema');;
				//echo 'pichema error';
			}else{
				$hema = $this->upload->data();
				$pichema = $hema['file_name'];
				//upload to aws
				$keyname = $pichema;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$pichemaaws = $data['upload_data'];
			};
			//upload blood
			if (!$this->upload->do_upload('blood')) {
				$picbloodaws = $this->input->post('urlblood');;
			}else{
				$blood = $this->upload->data();
				$picblood = $blood['file_name'];
				//upload to aws
				$keyname = $picblood;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picbloodaws = $data['upload_data'];
			};
			//upload xray
			if (!$this->upload->do_upload('xray')) {
				$picxrayaws = $this->input->post('urlxray');
			}else{
				$xray = $this->upload->data();
				$picxray = $xray['file_name'];
				//upload to aws
				$keyname = $picxray;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picxrayaws = $data['upload_data'];
			};
			//upload Ultrasono
			if (!$this->upload->do_upload('ultrasono')) {
				$picultrasonoaws = $this->input->post('urlultrasono');;
			}else{
				$ultrasono = $this->upload->data();
				$picultrasono = $ultrasono['file_name'];
				//upload to aws
				$keyname = $picultrasono;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picultrasonoaws = $data['upload_data'];
			};
			//upload Otherdiag
			if (!$this->upload->do_upload('otherdiag')) {
				$picotherdiagaws = $this->input->post('urlotherdiag');;
			}else{
				$otherdiag = $this->upload->data();
				$picotherdiag = $otherdiag['file_name'];
				//upload to aws
				$keyname = $picotherdiag;
				$getpng = FCPATH."upload/pet/medrec/".$keyname;
				$this->load->library('aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$picotherdiagaws = $data['upload_data'];
			};

			//inserttodb
			$dataupdatelog = array(
				//'idpet' => $idpet,
				'tgllog' => date('Y-m-d H:i:s',strtotime($dateadd)),
				'namevets' => $nama,
				//'title' => $this->input->post('title'),
				'diagnose' => $diagnose,
				'idclinic' => $idclinic,
				'idvets' => $profileid,
				'anamnesis' => $anamnesis,
				'weight' => $weight,
				'temperature' => $temperature,
				'heartrate' => $heartrate,
				'resprate' => $resprate,
				'bcs' => $bcs,
				'food' => $food,
				'eyes' => $eyes,
				'skin' => $skin,
				'resp' => $resp,
				'circ' => $circ,
				'dige' => $dige,
				'urin' => $urin,
				'nerv' => $nerv,
				'musc' => $musc,
				'othernotes' => $othernotes,
				'treatment' => $treatment,
				//'drugs' => $drugs,
				'hema' => $pichemaaws,
				'blood' => $picbloodaws,
				'xray' => $picxrayaws,
				'ultrasono' => $picultrasonoaws,
				'otherdiag' => $picotherdiagaws,
				'hemanotes' => $hemanotes,
				'bloodnotes' => $bloodnotes,
				'ultrasononotes' => $ultrasononotes,
				'xraynotes' => $xraynotes,
				'othernotes' => $othernotes,
				'otherdiagnotes' => $otherdiagnotes,

			);
			$this->db->where('idlog',$idlog);
			$this->db->update('tbl_logpet',$dataupdatelog);
			$this->session->set_flashdata('success', 'Diagnose medical history has been updated');
			redirect ('pet/logdetail/?idlog='.$idlog.'&idpet='.$idpet);

		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function addvaccine(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			$idpet = $this->input->post('idpet');
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			//protect pet vaccine
			//$this->form_validation->set_rules('vaccine', 'Vaccine', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('nameowner', 'Name Owner', 'trim|required|xss_clean');
			//$this->form_validation->set_rules('phoneowner', 'Phone Owner', 'trim|required|xss_clean');
			//if ($this->form_validation->run() == FALSE){
				//$this->session->set_flashdata('error', 'Error, please provide the correct information.');
				//redirect ('pet/detail/?idpet='.$idpet);
			//}else{				
				$jam = date(' H:i:s');
				$vaccine = $this->input->post('vaccine');
				$vactot = 0;
				$val = "";
    			foreach($vaccine as $value) {
    				$val = $val . $value . "<br>";
    				$vactot++;
    			}
			    $vaccine = substr($val,0,-4);
				$description = $this->input->post('description');
				$tglvacc = $this->input->post('datevacc');
				//$vaccine = $this->input->post('vaccine');
				$rfid = $this->input->post('rfid');
				$datevacc = date('Y-m-d',strtotime($tglvacc));
				$idowner = $this->input->post('idowner');
				$nameowner = $this->input->post('nameowner');
				$emailowner = $this->input->post('emailowner');
				$phoneowner = $this->input->post('phoneowner');
				$nextdate = $this->input->post('nextdate');
				$nextvacc = date('Y-m-d',strtotime($nextdate));
				$datainsert = array(
					'idpet' => $idpet,
					'idclinic' => $idclinic,
					'idvet' => $profileid,
					'datevacc' => $datevacc,
					'vaccine' => $vaccine,
					'description' => $description,
					'vetname' => $nama,
					'idowner' => $idowner,
					'nameowner' => $nameowner,
					'emailowner'=> $emailowner,
					'phoneowner' => $phoneowner,
					'nextdate' => $nextvacc,
					'category' => 'vaccine',
				);
				$this->db->insert('tbl_vaccine',$datainsert);
				$this->session->set_flashdata('success', 'Congrats, Vaccine history has been inserted');
				redirect ('pet/detail/?idpet='.$idpet);
			//}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function updatevacc(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idpet = $this->input->post('idpet');
			$jam = date(' H:i:s');
			$vaccine = $this->input->post('vaccine');
			$vactot = 0;
    			foreach($vaccine as $value) {
    				$value = $value . "<br>";
    				$vactot++;
    			}
			$vaccine = substr($value,0,-4);
			$description = $this->input->post('description');
			$tglvacc = $this->input->post('datevacc');
			//$vaccine = $this->input->post('vaccine');
			$idvacc = $this->input->post('idvacc');
			//$rfid = $this->input->post('rfid');
			$datevacc = date('Y-m-d',strtotime($tglvacc));
			$nextdate = $this->input->post('nextdate');
			$nextvacc = date('Y-m-d',strtotime($nextdate));
			$dataupdate = array(				
				'datevacc' => $datevacc,
				'vaccine' => $vaccine,
				'vetname' => $nama,
				'description' => $description,
				'nextdate' => $nextvacc,
			);
			$this->db->where('idvacc',$idvacc);
			$this->db->update('tbl_vaccine',$dataupdate);
			$this->session->set_flashdata('success', 'Congrats, Vaccine history has been updated');
			redirect ('pet/detail/?idpet='.$idpet);
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function deletevacc(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idvacc = $this->input->post('idvacc');
			$detailvaccine = $this->Pet_model->detailvaccine($idvacc);
			$idvet = $detailvaccine[0]->idvet;
			$idpet = $detailvaccine[0]->idpet;
			$rfid = $this->input->post('idpet');
			if($profileid != $idvet){
				$this->session->set_flashdata('error', 'Sorry, you cannot delete this vaccine');
				redirect ('pet/detail/?idpet='.$idpet);
			}else{
				$dataupdate = array(				
					'status' => '2',
				);
				$this->db->where('idvacc',$idvacc);
				$this->db->update('tbl_vaccine',$dataupdate);
				$this->session->set_flashdata('success', 'Congrats, Vaccine history has been deleted');
				redirect ('pet/detail/?idpet='.$idpet);
			}			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}


	public function owner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;				
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};	
				$idclinic = $dataprofile[0]->idclinic;
				if(!is_numeric($idclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
				$dataclinic = $this->Home_model->detailclinic($idclinic);	
				$idowner = $this->input->get('id');
				$idcustomer = $idowner;
				$dataowner = $this->Customer_model->detailcustomer($idcustomer);
				if(count($dataowner)<1)die("<script>alert('Data not found');self.location.href='/';</script><h2>Please enable javascript</h2>");
				$namaowner = $dataowner[0]->nama;
				if($dataowner[0]->email==""){
				    $datapet = $this->Pet_model->byownerid($idcustomer,$idclinic,$role);   
				}else{
				    $datapet = $this->Pet_model->byowneremail($dataowner[0]->email,$idclinic,$role);
				}
				$data = array(
					'title' => 'Pet Owner - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'datacountry' => $this->Login_model->country(),
					'pettype' => $this->Pet_model->pettype(),
					'nameclinic' => $dataclinic[0]->nameclinic,
					'datapet' => $datapet,
					'namaowner' => $namaowner,
				);

				
				//var_dump($idowner);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				//var_dump($datapet);
				$this->load->view('website/mypet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
			}	
		}else{
			redirect ('maintenance');
		}
		

	}

	public function addphonepet(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$limit = $this->input->get('limit');
			$allpet = $this->Pet_model->allnohp($limit);
			echo count($allpet).'<br>';
			foreach ($allpet as $pet) {
					$idcustomer = $pet->idowner;
					$idpet = $pet->idpet;
					$dataowner =  $this->Customer_model->detailcustomer($idcustomer);
					$nohpcustomer = $dataowner[0]->nohp;
					$namaowner = $dataowner[0]->nama;
					$dataupdatepet = array(
						'nohp' => $nohpcustomer,
					);
					$this->db->where('idpet',$idpet);
					$this->db->update('tbl_pet',$dataupdatepet);
					echo $pet->namapet.' Sudah diupdate ke No Hp '.$nohpcustomer.' '.$namaowner.'<br>';
				
			}

		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}


	public function postbulkpet(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
	    	if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				if($role == '0'){
					redirect ('dashboard/petowner');
				}
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				//$this->form_validation->set_rules('datapet', 'File', 'required');
				$this->form_validation->set_rules('uid', 'uid', 'required');
				$this->form_validation->set_rules('idclinic', 'idclinic', 'required');
				if ($this->form_validation->run() == FALSE){
					$this->session->set_flashdata('error', 'You didnt select file.');
					redirect ('pet/all');
				}else{
					$uid = $this->input->post('uid');
					$idclinic = $this->input->post('idclinic');
					//$detailinsert 
					$config['upload_path']   = './upload/bulk'; 
					$config['allowed_types'] = 'csv|xlsx'; 
					$config['max_size']      = 5500; 
					$config['encrypt_name'] = TRUE;
					$config['quality'] = 50;  
					$this->load->library('upload', $config);
					//upload hema
					if (!$this->upload->do_upload('datapet')) {
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect ('pet/all');
					}else{
						$bulk = $this->upload->data();
						$filebulk = $bulk['file_name'];
						//upload to aws
						$keyname = $filebulk;
						$getpng = FCPATH."upload/bulk/".$keyname;
						$this->load->library('aws3');
						$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
						$data = array('upload_data' => $image_data['file_name']);
						$fileurl = $data['upload_data'];

						$datainsertbulk = array(
							'uid' => $uid,
							'idclinic' => $idclinic,
							'fileurl' => $fileurl,
							'datesend' => date('Y-m-d H:i:s'),
						);
						$this->db->insert('tbl_bulk_file',$datainsertbulk);

						$this->session->set_flashdata('success', 'File has been uploaded. Please wait for 24 hour for data to show up');
						redirect ('pet/all');
					};
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function deletelog(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
	    	if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$realpass = $dataprofile[0]->password;
				if($role == '0'){
					redirect ('dashboard/petowner');
				}
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);
				$idlog = $this->input->post('idlog');				
				$idpet = $this->input->post('idpet');
				$datalog = $this->Pet_model->ceklogid($idlog,$idpet);
				$cliniclog = $datalog[0]->idclinic;
				$password = $this->input->post('password');
				$passmd5 = md5($password);
				if($idclinic != $cliniclog){
					$this->session->set_flashdata('error', 'You are not allowed to delete this log.');
					redirect ('pet/logdetail/?idlog='.$idlog.'&idpet='.$idpet.'');
				}else{
					if($realpass != $passmd5){
						$this->session->set_flashdata('error', 'Password didnt match.');
						redirect ('pet/logdetail/?idlog='.$idlog.'&idpet='.$idpet.'');
					}else{
						$dataupdate = array(
							'status' => '1',
						);
						$this->db->where('idlog',$idlog);
						$this->db->update('tbl_logpet',$dataupdate);
						$this->session->set_flashdata('success', 'Medical history has been deleted');
						redirect ('pet/detail/?&idpet='.$idpet.'');
					}
				}
			}else{
				$this->session->set_flashdata('success', 'Please login first');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}

	public function addbyowner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};	
				// $idclinic = $dataprofile[0]->idclinic;
				// $dataclinic = $this->Home_model->detailclinic($idclinic);							
				$data = array(
					'title' => 'Add Pet - iVetdata.com',
					'nama' => $nama,
					//'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'datacountry' => $this->Login_model->country(),
					//'idclinic' => $idclinic,
					'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				$this->load->view('website/dashboard-owner-mypets-add',$data);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/footer_new_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
			}	
		}else{
			redirect ('maintenance');
		}
	}

	public function addvetbyowner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};	
				// $idclinic = $dataprofile[0]->idclinic;
				// $dataclinic = $this->Home_model->detailclinic($idclinic);							
				$data = array(
					'title' => 'Add Vet - iVetdata.com',
					'nama' => $nama,
					//'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'datacountry' => $this->Login_model->country(),
					//'idclinic' => $idclinic,
					'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view');
				$this->load->view('website/addvetbyowner_view');
				$this->load->view('website/headerdash_view');
				$this->load->view('website/footer_new_view');
			}else{
				$this->session->set_flashdata('success', 'Please login first');
			    redirect ('/login/owner');
			}	
		}else{
			redirect ('maintenance');
		}
	}
	
	public function addvetprocessbyowner(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);

			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$email = $dataprofile[0]->email;
			$address = $dataprofile[0]->address;
			$nohp = $dataprofile[0]->phone;
			$city = $dataprofile[0]->city;
			$country = $dataprofile[0]->country;
			
			if(is_null($nama))$nama=" ";
			if(is_null($email))$email=" ";
			if(is_null($address))$address=" ";
			if(is_null($nohp))$nohp=" ";
			if(is_null($city))$city=" ";
			if(is_null($country))$country=" ";
			if(is_null($role))$role="0";
			
			$idclinic = $this->input->post('clinic');
			$chk = $this->Home_model->detailclinic($idclinic);
			if(count($chk)==0)die("<script>alert('Error : Clinic is not found');self.location.href='/';</script><h3>Loading ...</h3>");
			//$chk = $this->Customer_model->cekbyidclinic($email,$profileid,$idclinic);
			//if(count($chk)>0)die("<script>alert('Error : Vet is already registered before');history.back();</script><h3>Loading ...</h3>");

			$dataclinic = array(
			    'uid' => $profileid,
			    'idclinic' => "-".$idclinic,
			    'nama'=> $nama,
			    'nohp' => $nohp,
			    'email' => $email,
			    'city'=> $city,
			    'address' => $address,
			    'country' => $country,
			    'input_by' => '0',
			    'status' => '0',
			    'delete_by' =>'',
			);
			
			$dt_cust = $this->Home_model->cekcust("-".$idclinic,$profileid);
			//$dt_cust = $this->Home_model->cekcust($idclinic,$profileid);
			if(!isset($dt_cust[0])){
			    $this->db->insert('tbl_customer',$dataclinic);
			    $idcustomer = $this->db->insert_id();
			}else{
			    $idcustomer = $dt_cust[0]->idcustomer;
			}
			
			if(!empty($_POST['idpet'])){
			    $petid = array();
                foreach($_POST['idpet'] as $idpet){
                    $petid[] = $idpet;
                }
    			$this->db->where_in('idpet',$petid);
    			$this->db->update('tbl_pet',array('idclinic'=>"-".$idclinic));
    			//$this->db->update('tbl_pet',array('idclinic'=>$idclinic,'idowner'=>$idcustomer));
            }
			
			$dt_clinic = $this->Home_model->detailclinic($idclinic);
			
			if(count($dt_clinic)>0){
				    //sent email
				    //$subject = 'Hi '.$nameowner.', Your pet has been registered in '.$nameclinic.'';
				    $subject = 'Add Vet Notification';
				    $message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
					<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
					 <img height="70" src="https://app.ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
							 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"><h1 style="font-size:20px;margin:0;color:#333"> Hello '.$dt_clinic[0]->nameclinic.', </h1>
							 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">'.$nama.' has added you as has added you as Vet in her Pet Owner\'s account. Her profile will appear in your Pet Owner\'s list if you confirm.<br /><br />
							 Please confirm the invitation by clicking the confirm button below. <br />
							 <a href="'.base_url().'vets/approve/?clinic='.$idclinic.'&token='.md5($email).'&confirm=true" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank""> Confirm </a><br /> </p></td> </tr>
				</tbody> </table> </td> </tr></tbody> </table>';
    				$text = $message;
    				//$email = $user_email;
    				//$this->load->view('testemail_view');
				
				    $this->load->library('EmailSender.php');
				    $this->emailsender->send($dt_clinic[0]->email,$subject,$text);		
				}
			
			$this->session->set_flashdata('success', 'Success, your request has been sent to vet');
			die("<script>alert('Success, your request has been sent to vet');self.location.href='".base_url()."dashboard/petowner';</script><h3>Loading ...</h3>");
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect (base_url());
		}
	}
	
	public function addprocessbyowner(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$dt_cust = $this->Home_model->datacustomer($profileid);
			if(isset($dt_cust[0])){
			    $idowner = $dt_cust[0]->idcustomer;
			}else{
			    $idowner = $profileid;
			}
			//dataheader
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};				
			
			$this->form_validation->set_rules('namepet', 'Pet Name', 'required');
			$this->form_validation->set_rules('type', 'Type', 'required');
			//data pet
			//$microchip = $this->input->post('microchip');
			// $microchip = '0';
			// if ($microchip == '1') {
			// 	$rfid = $this->input->post('rfid');
			// }if($microchip == '0'){
			// 	$rfid = $this->input->post('rfiduniq');
			// }
			$rfid = $this->input->post('rfid');
			if($rfid == ''){
				$microchip = '0';
				$rfid = '0';

			}else{
				$microchip = '1';
				$rfid = $rfid;
			}
			// var_dump($microchip);
			//var_dump($rfid);

			//$rfid = $this->input->post('rfid');			
			$namepet = $this->input->post('namepet');
			$type = $this->input->post('type');
			$color = $this->input->post('color');
			$description = '';
			$nameowner = $nama;
			$phone= $dataprofile[0]->phone;
			$email = $dataprofile[0]->email;			
			$address = $this->input->post('address');
			$breed = $this->input->post('breed');
			$datebirth = $this->input->post('dateofbirth');
			$public_info = "0001111111";
			if(isset($_POST['public_info'])){
    			$permission = $this->input->post('public_info');
    			foreach($permission as $val){
    			    $public_info[$val]=1;
    			}
			}

			//var_dump($datebirth)
			$newbirth = date('Y-m-d',strtotime($datebirth));
			//$dataclinic = $this->Home_model->dataclinic($profileid);
			//$idclinic = $dataclinic[0]->idclinic;
			$country = $dataprofile[0]->country;
			$gender = $this->input->post('gender');
			$neutered = $this->input->post('neutered');
			$note = $this->input->post('note');
			$city = $dataprofile[0]->city;
			$postalcode = $this->input->post('postalcode');
			if ($this->form_validation->run() == FALSE){
				$data = array(
						'title' => 'Add Pet - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						//'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,'manageto' => $dataprofile[0]->manageto,
						'expdate' => $dataprofile[0]->expdate,
						'datacountry' => $this->Login_model->country(),
						'idclinic' => $idclinic,
						'pettype' => $this->Pet_model->pettype(),
						//'nameclinic' => $dataclinic[0]->nameclinic,
						'error' => 'Please input the mandatory field.',
						'microchip' => $microchip,
						'rfid' => $rfid,
						'namepet' => $namepet,
						'type' => $type,
						'color' => $color,
						//'note' => $description,
						'nameowner' => $nameowner,
						'phone' => $phone,
						'email' => $email,
						'address' => $address,
						'breed' => $breed,
						'datebirth' => $newbirth,
						'country' => $country,
						'gender' => $gender,
						'neutered' => $neutered,
						'city' => $city,
						'postalcode' => $postalcode,
						'note' => $note,
					);
					//$this->load->view('login_user');
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					$this->load->view('app/dashboard-owner-mypets-add',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/footer_new_view',$data);

			}else{

				if($microchip == '1'){
					$jumlahangkarfid = strlen($rfid);
				}else{
					$jumlahangkarfid = '20';
				}
				//var_dump($jumlahangkarfid);

				if($jumlahangkarfid < '15'){
					$data = array(
						'title' => 'Add Pet - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						//'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,'manageto' => $dataprofile[0]->manageto,
						'expdate' => $dataprofile[0]->expdate,
						'datacountry' => $this->Login_model->country(),
						//'idclinic' => $idclinic,
						'pettype' => $this->Pet_model->pettype(),
						//'nameclinic' => $dataclinic[0]->nameclinic,
						'error' => 'Please input the correct microchip number.  Otherwise select "NO".',
						'microchip' => $microchip,
						'rfid' => $rfid,
						'namepet' => $namepet,
						'type' => $type,
						'color' => $color,
						'note' => $description,
						'nameowner' => $nameowner,
						'phone' => $phone,
						'email' => $email,
						'address' => $address,
						'breed' => $breed,
						'datebirth' => $datebirth,
						'country' => $country,
						'gender' => $gender,
						'neutered' => $neutered,
						'city' => $city,
						'postalcode' => $postalcode,
						'note' => $note,
					);
					//$this->load->view('login_user');
					//$this->load->view('login_user');
					$this->load->view('website/header_new_view',$data);
					$this->load->view('app/dashboard-owner-mypets-add',$data);
					//$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/footer_new_view',$data);
				}else{
			

			//cek RFID
			if($rfid == '0'){
				//$cekpetid = $this->Pet_model->cekpetid($rfid);
				$adapet = '0';
			}else{
				$cekpetid = $this->Pet_model->cekpetid($rfid);
				$adapet = count($cekpetid);
			}
			
			if($adapet == '0'){
				//config photo
				$config['upload_path']   = './upload/pet'; 
				$config['allowed_types'] = 'jpg|png|jpeg'; 
				$config['max_size']      = 10500; 
				$config['encrypt_name'] = TRUE;
				$config['quality'] = 50;  
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('petphoto')) {

					$filephotourl = '';
					
				}else{			

					$result1 = $this->upload->data();
			   		$filephoto = $result1['file_name'];
			   		//upload to aws
					$keyname = $filephoto;
					$getpng = FCPATH."upload/pet/".$keyname;
					$this->load->library('aws3');
					$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
					$data = array('upload_data' => $image_data['file_name']);
					$filephotourl = $data['upload_data'];

				}

				$datapet = array(
					'rfid' => $rfid,
					'microchip' => $microchip,
					'idowner' => $idowner,
					'namapemilik' => $nameowner,
					'email' => $email,
					'nohp' => $phone,
					'tipe' => $type,
					'namapet' => $namepet,
					'city' => $city,
				//'profilephoto' => $profilephoto,
					'country' => $country,
					//'address' => $address,
					//'description' => $note,
					'input_by' => $profileid,
					'breed' => $breed,
					'datebirth' => $newbirth,
					'color' => $color,
					'gender' => $gender,
					'neutered' => $neutered,
					'photo' => $filephotourl,
					'public_info' => $public_info,
				);
				$this->db->insert('tbl_pet',$datapet);
				$idpet = $this->db->insert_id();
				redirect('dashboard/petowner');
				// $petclinicfix = $petclinic + 1;
				// $chip = $currentchip - 1;
				// $petclinicadd = array(
				// 	'pet' => $petclinicfix,
				// 	'customer' => $customerclinicfix,
				// 	'chip' => $chip,
				// );
				// $this->db->where('idclinic',$idclinic);
				// $this->db->update('tbl_clinic',$petclinicadd);

				// //sent email
				// $subject = 'Hi '.$nameowner.', Your pet has been registered in '.$nameclinic.'';	
				// $message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
				// 	<tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
				// 	 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
				// 			 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$nameowner.', </h1>
				// 			 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">Congratulations, Your pet has been registered in ivetdata.com <br>
				// 			 	<strong>Pet Data</strong><br>
				// 			 	Name Pet : '.$namepet.'<br>
				// 			 	Clinic : '.$nameclinic.'<br><br>
				// 			 	<strong>Your Login Detail</strong><br>
				// 			 	Email : '.$email.'<br>
				// 			 	Password : ivetdata<br><br>
				// 			 	Please login and edit your password.
				// 			 </p>
				// 			 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">
				// 			 <a href="'.base_url().'login/owner" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank"">Login Now </a> </p>
				// 			 </td> </tr>

				// </tbody> </table> </td> </tr></tbody> </table>';
				// $text = $message;
				// //$email = $user_email;
				// //$this->load->view('testemail_view');
				// $this->load->library('EmailSender.php');
				// $this->emailsender->send($email,$subject,$text);			   


				// $data = array(
				// 	'title' => 'Home - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'expdate' => $dataprofile[0]->expdate,
				// 	'success' => 'Add data pet is successful',
				// 	//'nameclinic' => $dataclinic[0]->nameclinic,
				// 	'idpet' => $idpet,
				// 	'uid' => $profileid,
				// );
				// //$this->load->view('login_user');
				// //$this->load->view('login_user');
				// $this->load->view('website/header_new_view',$data);
				// $this->load->view('app/dashboard-owner-mypets-add',$data);
				// //$this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/footer_new_view',$data);
			}else{
				$data = array(
					'title' => 'Home - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'expdate' => $dataprofile[0]->expdate,
					'error' => 'This microchip ID is a duplicate to an existing Pet. Please double check the number.',
					'namepet' => $namepet,					
					'city' => $city,
					'profilephoto' => $profilephoto,
					'country' => $country,
					'address' => $address,
					'description' => $description,
					'nameowner' => $nameowner,
					'phone' => $phone,
					'email' => $email,
					'tipe' => $type,
					'breed' => $breed,
					'datebirth' => $datebirth,
					'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
					'color' => $color,
					'datebirth' => $datebirth,					
				);
				//$this->load->view('login_user');
				$this->load->view('website/header_new_view',$data);
				$this->load->view('app/dashboard-owner-mypets-add',$data);
				//$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/footer_new_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			}	
			//}	
		}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}		
	}
	
	public function editvacc(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if(!is_numeric($vetclinic))die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idvacc = $this->input->get('idvacc');
			$idpet = $this->input->get('idpet');
			//$rfid = $idpet;
			$datavacc = $this->Pet_model->lastvaccine($idpet);
			//var_dump($datalog);
			$adalog = count($datavacc);
			if($adalog == '0'){
				$data = array(
					'title' => 'Vaccine history - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'error' => 'Sorry, we can not find medical history',
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/nopet_view',$data);
				$this->load->view('website/footerdash_view',$data);

			}else{
				$datapet = $this->Pet_model->detailpet($idpet);
				//$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				if($vetclinic<>$idclinic||$role<1)die("<script>alert('Sorry, you are not authorized');self.location.href='/Pet/all';</script><h2>Please enable javascript</h2>");
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				$data = array(
					'title' => 'Detail Vaccine - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'idvacc' => $idvacc,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'petphoto' => $petphoto,
					'dateadd' => $datapet[0]->dateadd,
					'datavacc' => $datavacc,
					//'logpet' => $this->Pet_model->logpet($idpet),
					//'countdoctor' => $countdoctor,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/edit_vaccpet_view',$data);
				//$this->load->view('website/footerdash_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}
	
	public function exportpetlist(){
    	$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
	    	if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$idclinic = $dataprofile[0]->idclinic;
				$role = $dataprofile[0]->role;
				$nama = $dataprofile[0]->name;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$profilephoto = $dataprofile[0]->photo;
                if(isset($_GET['emailpet'])){
                    if($phone == ''){
            				$datapet = $this->Pet_model->cekpetbyemail($emailpet,$vetclinic);
            		}else{
            				$datapet = $this->Pet_model->cekpetbyphone($phone,$vetclinic,$namepet);
            		}       
            		$all_pet = count($datapet);
                }else{
                    $all_pet = $this->Pet_model->jumlah_data($idclinic,$role);
                    $datapet = $this->Pet_model->data($all_pet,0,$idclinic,$role);
                }
				$data = array(
					'title' => 'List of Pets - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'idclinic' => $idclinic,
					'uid' => $profileid,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'team' => $dataprofile[0]->team,
					'expdate' => $dataprofile[0]->expdate,
					'all_pet' => $all_pet,
					'datapet' => $datapet,
				);
				
				$this->load->view('website/export_pet',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
    }
    
    public function exportlogdetail(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idlog = $this->input->get('idlog');
			$idpet = $this->input->get('idpet');
			//$rfid = $idpet;
			if($idlog=="all"){
			    $datalog = $this->Pet_model->ceklogidall($idpet);
			}else{
			    $datalog = $this->Pet_model->ceklogid($idlog,$idpet);
			}
			//var_dump($datalog);
			$adalog = count($datalog);
			if($adalog == '0'){
				$data = array(
					'title' => 'Medical history - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,
					'error' => 'Sorry, we can not find medical history',
				);
				//$this->load->view('login_user');
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/nopet_view',$data);
				$this->load->view('website/footerdash_view',$data);

			}else{
				$datapet = $this->Pet_model->detailpet($idpet);
				//$idpet = $datapet[0]->idpet;
				$namepet = $datapet[0]->namapet;            
	            $tipe = $datapet[0]->tipe;
	            $rfid = $datapet[0]->rfid;
	            $description = $datapet[0]->description;
	            $breed = $datapet[0]->breed;
	            $color = $datapet[0]->color;
	            $datebirth = $datapet[0]->datebirth; 
	            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
	            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
	            $age =  $agey.' Year, '.$agem.' Month';
	            $photo = $datapet[0]->photo;
	            if($photo =='' ){
	                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
	            }else{
	                $petphoto = $photo;
	            };
				//var_dump($datapet);
				$idclinic = $datapet[0]->idclinic;
				$idowner = $datapet[0]->idowner;	
				if($vetclinic == $idclinic){
					$edit = '1';
				}else{
					$edit = '0';
				}
				//$datacustomer = $this->Pet_model->datacustomer($idowner);		
				// $cekdoctor = $this->Home_model->cekdoctor($idclinic);
				// $countdoctor = count($cekdoctor);
				$data = array(
					'title' => 'Detail Pet - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,'manageto' => $dataprofile[0]->manageto,				
					'datapet' => $datapet,
					'idpet' => $idpet,
					'idlog' => $idlog,
					'idpet' => $idpet,
					'namepet' => $namepet,
					'tipe' => $tipe,
					'rfid' => $rfid,
					'description' => $description,
					'breed' => $breed,
					'color' => $color,
					'edit' => $edit,
					'age' => $age,
					'petphoto' => $petphoto,
					'dateadd' => $datapet[0]->dateadd,
					'datalog' => $datalog,
					//'logpet' => $this->Pet_model->logpet($idpet),
					//'countdoctor' => $countdoctor,
					'dataowner' => $this->Pet_model->datacustomer($idowner),
					'idowner' => $idowner,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				$this->load->view('website/export_logdetail',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}

	public function service(){
        $data = array(
            'title' => 'Welcome to Ivetdata.com',
            'page' => 'Search',
        );
        $this->load->view('website/header_new_view');
        //$this->load->view('website/landing_new_view',$data);
        $this->load->view('website/owner-petservices');
        //$this->load->view('website/testimonial_new');
        //$this->load->view('website/latest_news');
        //$this->load->view('website/stat_new');
        //$this->load->view('website/pre_footer');
        $this->load->view('website/headerdash_view');
        $this->load->view('website/footer_new_view');

    }
    
    public function diary(){
        if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			if(isset($_GET['act'])&&$_GET['act']=='delete'){
			    $idpet = $this->input->get('idpet');
			    $this->db->where('id',$this->input->get('id')); 
			    $this->db->delete('tbl_notes'); 
			}else{
    			$id = $this->input->post('iddairy');
    			$idpet = $this->input->post('idpet');
    			$note = $this->input->post('message');
    			if($id==0){
    			    $this->db->insert('tbl_notes',array('notes'=>$note,'idpet'=>$idpet));
    			}else{
			        $this->db->where('id',$id);
					$this->db->update('tbl_notes',array('notes'=>$note));
    			}
			}
			redirect (base_url().'pet/detail/?idpet='.$idpet.'');
        }else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect (base_url());
		}
    }
    
}