// Initialize your app
var myApp = new Framework7({
	animateNavBackIcon: true,
	// Enable templates auto precompilation
	precompileTemplates: true,
	// Enabled pages rendering using Template7
	swipeBackPage: false,
	swipePanelOnlyClose: true,
	pushState: true,
	template7Pages: true,
	preloader : true,
	cache: true /* enable caching */
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
	// Enable dynamic Navbar
	dynamicNavbar: false,
});



$(document).ready(function () {
	circlemenu();
	$("#RegisterForm").validate();
	$("#LoginForm").validate();
	$("#ForgotForm").validate();
	$(".close-popup").click(function () {
		$("label.error").hide();
	});

	$(".close-popup").click(function () {
		$("label.error").hide();
	});

	// $('button.my-cart-btn').click(function () {
	//     myApp.alert('This is alert text!!!');
	// });

	$('.add').click(function () {
			//if ($(this).prev().val() < 3) {
	    	$(this).prev().val(+$(this).prev().val() + 1);
			//}
	});
	$('.sub').click(function () {
			if ($(this).next().val() > 1) {
	    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
			}
	});

	$(function () {
		var goToCartIcon = function($addTocartBtn){
        var $cartIcon = $(".my-cart-icon");
        var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
        $addTocartBtn.prepend($image);
        var position = $cartIcon.position();
        $image.animate({
            top: position.top,
                    left: position.left
                  }, 500 , "linear", function() {
                    $image.remove();
                  });
                }

                $('.my-cart-btn').myCart({
                  classCartIcon: 'my-cart-icon',
                  classCartBadge: 'my-cart-badge',
                  affixCartIcon: true,
                  checkoutCart: function(products) {
                    $.each(products, function(){
                      console.log(this);
                    });
                  },
                  clickOnAddToCart: function($addTocart){
                    goToCartIcon($addTocart);
                  },
                  getDiscountPrice: function(products) {
                    var total = 0;
                    $.each(products, function(){
                      total += this.quantity * this.price;
                    });
                    return total * 0.5;
                  }
                });

              });

	// $(".qntyplusshop").live("click", function() {
	//   var qInput = $(this).parents(".quantityInput");
	//   var qText = qInput.find(".quantityText");
	//   var qValue = parseInt((qText.val())? qText.val() : 0);
	//   qText.val(Math.min(qValue + 1, (qInput.attr("max"))? qInput.attr("max") : 0xffff));
	// });

	
});



$$(document).on('pageInit', function (e) {
	$("#RegisterForm").validate();
	$("#LoginForm").validate();
	$("#ForgotForm").validate();
	$(".close-popup").click(function () {
		$("label.error").hide();
	});
})

// Show preloader before Ajax request
$$('.open-preloader-indicator').on('click', function () {
	myApp.showPreloader();
	setTimeout(function () {
		myApp.hidePreloader();
	}, 5000);
});

myApp.onPageInit('index', function (page) {
	circlemenu();
})

myApp.onPageInit('news', function (page) {

	$(".posts.newsitems li").hide();
	size_li = $(".posts.newsitems li").size();
	x = 4;
	$('.posts.newsitems li:lt(' + x + ')').show();
	$('.loadmore-newsitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.newsitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-newsitems').hide();
			$('.showless-newsitems').show();
		}
	});

})

myApp.onPageInit('dashboard-owner-petshop', function (page) {

	console.log('Petshop');

})

myApp.onPageInit('dashboard-owner-mypets', function (page) {

	$(".posts.dompitems li").hide();
	size_li = $(".posts.dompitems li").size();
	x = 4;
	$('.posts.dompitems li:lt(' + x + ')').show();
	$('.loadmore-dompitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dompitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dompitems').hide();
			$('.showless-dompitems').show();
		}
	});

	$(".posts.domcitems li").hide();
	size_li_b = $(".posts.domcitems li").size();
	y = 4;
	$('.posts.domcitems li:lt(' + y + ')').show();
	$('.loadmore-domcitems').click(function () {
		y = (y + 1 <= size_li_b) ? y + 1 : size_li_b;
		$('.posts.domcitems li:lt(' + y + ')').show();
		if (y == size_li_b) {
			$('.loadmore-domcitems').hide();
			$('.showless-domcitems').show();
		}
	});

})

myApp.onPageInit('dashboard-owner-invoice', function (page) {

	$(".posts.doiitems li").hide();
	size_li = $(".posts.doiitems li").size();
	x = 4;
	$('.posts.doiitems li:lt(' + x + ')').show();
	$('.loadmore-doiitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.doiitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-doiitems').hide();
			$('.showless-doiitems').show();
		}
	});

})

myApp.onPageInit('dashboard-owner-vetsearch', function (page) {

	$(".posts.dovsitems li").hide();
	size_li = $(".posts.dovsitems li").size();
	x = 4;
	$('.posts.dovsitems li:lt(' + x + ')').show();
	$('.loadmore-dovsitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dovsitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dovsitems').hide();
			$('.showless-dovsitems').show();
		}
	});

	

})

myApp.onPageInit('dashboard-vet-mypets', function (page) {

	$(".posts.dvmpitems li").hide();
	size_li = $(".posts.dvmpitems li").size();
	x = 4;
	$('.posts.dvmpitems li:lt(' + x + ')').show();
	$('.loadmore-dvmpitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dvmpitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dvmpitems').hide();
			$('.showless-dvmpitems').show();
		}
	});

	$(".posts.dvoitems li").hide();
	size_li_b = $(".posts.dvoitems li").size();
	y = 4;
	$('.posts.dvoitems li:lt(' + y + ')').show();
	$('.loadmore-dvoitems').click(function () {
		y = (y + 1 <= size_li_b) ? y + 1 : size_li_b;
		$('.posts.dvoitems li:lt(' + y + ')').show();
		if (y == size_li_b) {
			$('.loadmore-dvoitems').hide();
			$('.showless-dvoitems').show();
		}
	});

})

myApp.onPageInit('dashboard-vet-invoice', function (page) {

	$(".posts.dviitems li").hide();
	size_li = $(".posts.dviitems li").size();
	x = 4;
	$('.posts.dviitems li:lt(' + x + ')').show();
	$('.loadmore-dviitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dviitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dviitems').hide();
			$('.showless-dviitems').show();
		}
	});

})

myApp.onPageInit('dashboard-vet-services', function (page) {

	$(".posts.dvsitems li").hide();
	size_li = $(".posts.dvsitems li").size();
	x = 4;
	$('.posts.dvsitems li:lt(' + x + ')').show();
	$('.loadmore-dvsitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dvsitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dvsitems').hide();
			$('.showless-dvsitems').show();
		}
	});

	$(".posts.dvmitems li").hide();
	size_li_b = $(".posts.dvmitems li").size();
	y = 4;
	$('.posts.dvmitems li:lt(' + y + ')').show();
	$('.loadmore-dvmitems').click(function () {
		y = (y + 1 <= size_li_b) ? y + 1 : size_li_b;
		$('.posts.dvmitems li:lt(' + y + ')').show();
		if (y == size_li_b) {
			$('.loadmore-dvmitems').hide();
			$('.showless-dvmitems').show();
		}
	});

})

myApp.onPageInit('dashboard-vet-team', function (page) {

	$(".posts.dvtitems li").hide();
	size_li = $(".posts.dvtitems li").size();
	x = 4;
	$('.posts.dvtitems li:lt(' + x + ')').show();
	$('.loadmore-dvtitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dvtitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dvtitems').hide();
			$('.showless-dvtitems').show();
		}
	});

})

myApp.onPageInit('contact', function (page) {
	$("#ContactForm").validate({
		submitHandler: function (form) {
			ajaxContact(form);
			return false;
		}
	});
})

myApp.onPageInit('dashboard-owner-appointments', function (page) {
	$(".posts.doaitems li").hide();
	size_li = $(".posts.doaitems li").size();
	x = 4;
	$('.posts.doaitems li:lt(' + x + ')').show();
	$('.loadmore-doaitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.doaitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-doaitems').hide();
			$('.showless-doaitems').show();
		}
	});
})

myApp.onPageInit('dashboard-vet-appointments', function (page) {
	$(".posts.dvaitems li").hide();
	size_li = $(".posts.dvaitems li").size();
	x = 4;
	$('.posts.dvaitems li:lt(' + x + ')').show();
	$('.loadmore-dvaitems').click(function () {
		x = (x + 1 <= size_li) ? x + 1 : size_li;
		$('.posts.dvaitems li:lt(' + x + ')').show();
		if (x == size_li) {
			$('.loadmore-dvaitems').hide();
			$('.showless-dvaitems').show();
		}
	});
})

myApp.onPageInit('dashboard-owner-appointments-add', function (page) {
	jQuery('#datetimepicker-doaa').datetimepicker({
		disabledDates: ['2019/08/20', '2019/08/21'],
		disabledWeekDays: [0, 6],
		format:'Y/m/d H:i',
		inline:true,
		lang:'en',
		minDate:0,
		maxDate:'+1970/02/02',
		minTime:'09:00',
		maxTime:'18:00',
	});
})

myApp.onPageInit('dashboard-owner-appointments-edit', function (page) {
	jQuery('#datetimepicker-doae').datetimepicker({
		defaultDate:'2019/09/10',
		defaultTime:'14:00',
		disabledDates: ['2019/08/20', '2019/08/21'],
		disabledWeekDays: [0, 6],
		format:'Y/m/d H:i',
		inline:true,
		lang:'en',
		minDate:0,
		maxDate:'+1970/02/02',
		minTime:'09:00',
		maxTime:'18:00',
	});
})

myApp.onPageInit('dashboard-owner-appointments-detail', function (page) {
	jQuery('#datetimepicker-doad').datetimepicker({
		defaultDate:'2019/09/10',
		defaultTime:'14:00',
		disabledDates: ['2019/08/20', '2019/08/21'],
		disabledWeekDays: [0, 6],
		format:'Y/m/d H:i',
		inline:true,
		lang:'en',
		minDate:0,
		maxDate:'+1970/02/02',
		minTime:'09:00',
		maxTime:'18:00',
	});
})

myApp.onPageInit('dashboard-vet-appointments-add', function (page) {
	jQuery('#datetimepicker-dvaa').datetimepicker({
		disabledDates: ['2019/08/20', '2019/08/21'],
		disabledWeekDays: [0, 6],
		format:'Y/m/d H:i',
		inline:true,
		lang:'en',
		minDate:0,
		maxDate:'+1970/02/02',
		minTime:'09:00',
		maxTime:'18:00',
	});
})

myApp.onPageInit('dashboard-vet-appointments-edit', function (page) {
	jQuery('#datetimepicker-dvae').datetimepicker({
		defaultDate:'2019/09/10',
		defaultTime:'14:00',
		disabledDates: ['2019/08/20', '2019/08/21'],
		disabledWeekDays: [0, 6],
		format:'Y/m/d H:i',
		inline:true,
		lang:'en',
		minDate:0,
		maxDate:'+1970/02/02',
		minTime:'09:00',
		maxTime:'18:00',
	});
})

myApp.onPageInit('dashboard-vet-appointments-detail', function (page) {
	jQuery('#datetimepicker-dvad').datetimepicker({
		defaultDate:'2019/09/10',
		defaultTime:'14:00',
		disabledDates: ['2019/08/20', '2019/08/21'],
		disabledWeekDays: [0, 6],
		format:'Y/m/d H:i',
		inline:true,
		lang:'en',
		minDate:0,
		maxDate:'+1970/02/02',
		minTime:'09:00',
		maxTime:'18:00',
	});
})
