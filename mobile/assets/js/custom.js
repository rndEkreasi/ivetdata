function showPassword(elemId, triggerId) {
  var target = document.getElementById(elemId);
  var trigger = $('#'+triggerId);
  if (target.type === "password") {
    target.type = "text";
    trigger.removeClass("fa-eye");
    trigger.addClass("fa-eye-slash");
  } else {
    target.type = "password";
    trigger.removeClass("fa-eye-slash");
    trigger.addClass("fa-eye");
  }
}