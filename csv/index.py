'''
    File name: index.py
    Author: Haofeng Liang
    Date created: 03/02/2019
    Date last modified: 03/18/2019
    Python Version: 3.6.4
'''

from flask import Flask, request, render_template, Response
import json
import petSQL

app = Flask(__name__)


def myresp(status, link, obj=None):
    rep = {"status": status}
    if obj:
        rep = {**rep, **obj}
    js = json.dumps(rep)
    res = Response(js, status=200, mimetype='application/json')
    res.headers['Link'] = link
    return res


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        req = request.get_json(force=True)
        # TODO: Data validation

        # TODO: Check whether there is a List (Before accepting a single row data)
        try:
            petSQL.insert_pet_objects(req)
            return myresp("Success", "", {"successMessage": "Inserted"})
        except:
            return myresp("ERROR", "", {"errorMessage": "An exception thrown when inserting data."})

    else:
        return render_template("index.html")


if __name__ == '__main__':
    app.run(debug=True)
