$("document").ready(function(){
	obj = {}
	
	function readFile(event){
		var reader = new FileReader();
  		reader.onload = function(file) {
		    var temp = file.target.result;
		    temp = $.csv.toObjects(temp);
          	obj = temp;
          	console.log(obj);
    	};
    	reader.readAsText(event);
	};

	function callback(data){
		console.log(data);
	};

	function postData(data, url){
		$.ajax({
		    type: "POST",
		    url: url,
		    data: JSON.stringify(obj),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    success: function(sucMsg){
		    	console.log(sucMsg);
		    },
		    failure: function(errMsg) {
		        alert(errMsg);
		    }
		});
	};
	
	$('#csvFile').change(function () {
    	readFile(this.files[0]);
	});

	$('#submit').on('click', function(){
		postData(obj, "");
	});

});