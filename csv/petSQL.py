'''
    File name: petSQL.py
    Author: Haofeng Liang
    Date created: 03/02/2019
    Date last modified: 03/18/2019
    Python Version: 3.6.4
'''

import mysql.connector


# NOTE: id in database is auto generated
ID_inDB = "id"
NAME_inDB = "name"
TYPE_inDB = "type"
AGE_inDB = "age"
BREED_inDB = "breed"

ID_inJSON = "PET ID"
NAME_inJSON = "PET NAME"
TYPE_inJSON = "TYPE"
AGE_inJSON = "AGE"
BREED_inJSON = "BREED"

mydb = None


def db_connect():
    global mydb
    mydb = mysql.connector.connect(
        host="localhost",
        user="ivet",
        passwd="ivet",
        database="ivet"
    )


def sql_to_insert():
    insert_sql = f"INSERT INTO pet({BREED_inDB}, {NAME_inDB}, {TYPE_inDB}, {AGE_inDB})" \
                 " VALUES(%s, %s, %s, %s)"
    return insert_sql


def val_to_insert(pet):
    try:
        val = (pet[BREED_inJSON], pet[NAME_inJSON], pet[TYPE_inJSON], pet[AGE_inJSON])
        return val
    except:
        print(f"Invalid value :: Must contain {BREED_inJSON} {NAME_inJSON} {TYPE_inJSON} {AGE_inJSON} \n"
              f"Got : {pet}")


# Insert Pet with fields provided
def insert_pet(pet_breed, pet_name, pet_type, age):
    pet = {
        BREED_inJSON: pet_breed,
        NAME_inJSON: pet_name,
        TYPE_inJSON: pet_type,
        AGE_inJSON: age
    }

    insert_pet_object(pet)


# Insert Pet as a JSON provided
def insert_pet_object(pet):
    print(pet)
    db_connect()
    sql = sql_to_insert()

    val = val_to_insert(pet)

    mycursor = mydb.cursor()
    mycursor.execute(sql, val)

    mydb.commit()
    print(mycursor.rowcount, "record inserted.")
    mycursor.close()
    mydb.close()
    # TODO: Return value


# Insert Pets as a List of JSON provided
def insert_pet_objects(pets):
    db_connect()
    sql = sql_to_insert()
    val = []
    for pet in pets:
        val.append(val_to_insert(pet))

    print(f"SQL is {sql}")
    print(f"Value is {val}")
    mycursor = mydb.cursor()
    mycursor.executemany(sql, val)

    mydb.commit()
    print(mycursor.rowcount, "record inserted.")
    mycursor.close()
    mydb.close()
    # TODO: Return value


# TODO: Implement select query to check existence
# def select_pet(pet):
#     db_connect()
#     sql = "SELECT * FROM pet WHERE "
#     val = []
#
#     for field in pet:
#         sql += f" '{field}' = %s"
#         val.append(pet[field])
#
#     print(f"SQL is {sql}")
#     print(f"Value is {val}")
#
#     mycursor = mydb.cursor()
#     mycursor.execute(sql, val)
#     result = mycursor.fetchall()
#     mycursor.close()
#     mydb.close()
#     print(result)
#     return result
