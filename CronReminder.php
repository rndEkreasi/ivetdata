<?php
defined('BASEPATH') OR define('BASEPATH','system');
defined('ENVIRONMENT') OR define('ENVIRONMENT','production');
include("application/config/database.php");
$conn = mysqli_connect($db['default']['hostname'],$db['default']['username'],$db['default']['password']) or die("Could not connect database");
mysqli_select_db($conn,$db['default']['database']) or die("Could not connect database");
$query = "SELECT idvacc AS id,a.nameowner AS owner,a.emailowner,b.phone AS nohp,c.namapet AS pet,datebirth,a.datevacc AS vacc1,a.nextdate AS vacc2,b.nameclinic AS clinic,b.address AS address FROM tbl_vaccine a JOIN tbl_clinic b ON a.idclinic=b.idclinic JOIN tbl_pet c ON a.idpet=c.idpet WHERE a.status='0' ";
if(isset($_GET['id'])){ 
    $query .= " AND a.idvacc=".$_GET['id'];
}else{
    $query .= " AND nextdate>=DATE(NOW()) AND (DATEDIFF(nextdate,DATE(NOW()))=7 OR DATEDIFF(nextdate,DATE(NOW()))=3 OR DATEDIFF(nextdate,DATE(NOW()))=1) ";
}
if(isset($_GET['id'])) $query .= " AND a.idvacc=".$_GET['id'];
$que = mysqli_query($conn,$query) or die($query);

if (mysqli_num_rows($que)>0){
	require_once("application/third_party/mailer/class.phpmailer.php");
	include("application/config/email.php");
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->CharSet = 'UTF-8';	
	$mail->Host       = $config['email']['smtp_host']; 
	$mail->SMTPDebug  = 0;                     
	$mail->SMTPAuth   = true;                  
	$mail->Port       = $config['email']['smtp_port'];                    
	$mail->Username   = $config['email']['smtp_user']; 
	$mail->Password   = $config['email']['smtp_pass'];  
	$i=0;
	while($row = mysqli_fetch_array($que)){
	    $data[$i]=$row;
	    $i++;
	}
	$cookie="";
	if(isset($_COOKIE["reminder"])){
        $dt=explode(",",$_COOKIE["reminder"]);
        $i=0;
        foreach($data as $value){
        	for($j=0;$j<count($dt);$j++){
        		if($dt[$j]==$value['id']){
        		    $email[$i]=false;
        		    //$cookie=$cookie.",".$value['id'];
        		    //echo $i.":sama-";
        		    //echo $dt[$j]."=".$value['id']."?";
        		    break;
        		}else{
        		    $email[$i]=true;
        		    //break;
        		    //echo $i.":beda-";
        		    //echo $dt[$j]."=".$value['id']."?";
        		}
        	}
        	$cookie=$cookie.",".$value['id'];
        	$i++;
    	}
	}else{
	    $i=0;
	   foreach($data as $value){
	       $email[$i]=true;
	       $cookie=$cookie.",".$value['id'];
	       $i++;
	   }
	}
    $cookie=substr($cookie,1);	
    //setcookie("reminder",$cookie,time()+86400);
	
	$i=0;
    foreach($data as $row){
    	if($email[$i]){
    	    $datebirth = $row['datebirth']; 
            $agey = date_diff(date_create($datebirth), date_create('now'))->y;
            $agem = date_diff(date_create($datebirth), date_create('now'))->m;
            if($agey==0){
                $age = $agem.' bulan ';
            }else{
                $age = $agey.' tahun, '.$agem.' bulan ';
            }
            echo "A reminder has been sent to this following email: $row[emailowner] <br /><br />";
            $message = "
    Hai $row[owner],<br /><br />Pesan email ini merupakan pengingat vaksin untuk hewan peliharaan Anda.<br />
    Nama Hewan : $row[pet]<br />Umur : ".$age."<br />
    Tanggal vaksin terakhir : ".date("D, d M Y",strtotime($row['vacc1']))."<br />
    Tanggal vaksin selanjutnya : ".date("D, d M Y",strtotime($row['vacc2']))."<br />
    Klinik<br />Nama Klinik : $row[clinic]<br />Alamat Klinik : $row[address]<br />
    Telpon Klinik : $row[nohp]<br /><br />
    Harap datang kembali pada tanggal ".date("D, d M Y",strtotime($row['vacc2']))." untuk melakukan vaksin rutin pada hewan peliharaan Anda.<br /><br />Terima kasih.<br /><br />
    ";	
    		echo $message;
    		$mail->SetFrom("noreply@ivetdata.com","iVetData"); 
    		$mail->Subject = "Vaccine Reminder from iVet Data"; 
    		$mail->AddAddress($row['emailowner'],$row['owner']);  
    		$mail->MsgHTML($message);
    		$mail->Send();
    	}else{
    	    echo "Email $row[emailowner] was already sent for id ".$row['id']." ... ";
    	}
    	$i++;
    }
}else{
	echo "Data not found ...
";
}   
?>
