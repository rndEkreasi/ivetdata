<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of AmazonS3
 *
 * @author wahyu widodo
 */
 
 include("./vendor/autoload.php");
 
 use Aws\S3\S3Client;
 
 class Aws3{
	
	private $S3;

	public function __construct(){
		$this->S3 = S3Client::factory([
			// 'key' => 'AKIAIL2YSRYPV3MOOYGQ',
			// 'secret' => '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',
			'region' => 'ap-southeast-1',
			'version' => 'latest',
			'credentials' => array(
			    'key' => 'AKIAIL2YSRYPV3MOOYGQ',
				'secret' => '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',
			)
		]);
	}	
	
	public function addBucket($bucketName){
		$result = $this->S3->createBucket(array(
			'Bucket'=>$bucketName,
			'LocationConstraint'=> 'ap-southeast-1'));
		return $result;	
	}
	
	public function sendFile($bucketName, $keyname,$getpng){
		$result = $this->S3->putObject(array(
				'Bucket' => $bucketName,
				'Key' => $keyname,
				//'SourceFile' => $filename['tmp_name'],
				'Body' => file_get_contents($getpng),
				'ContentType' => 'image/png',
				'StorageClass' => 'STANDARD',
				'ACL' => 'public-read'
		));
		return $result['ObjectURL']."\n";
	}

	public function ancolhari1($bucketName, $keyname1,$getpng1){
		$result = $this->S3->putObject(array(
				'Bucket' => $bucketName,
				'Key' => $keyname1,
				//'SourceFile' => $filename['tmp_name'],
				'Body' => file_get_contents($getpng1),
				'ContentType' => 'image/png',
				'StorageClass' => 'STANDARD',
				'ACL' => 'public-read'
		));
		return $result['ObjectURL']."\n";
	}

	public function ancolhari2($bucketName, $keyname2,$getpng2){
		$result = $this->S3->putObject(array(
				'Bucket' => $bucketName,
				'Key' => $keyname2,
				//'SourceFile' => $filename['tmp_name'],
				'Body' => file_get_contents($getpng2),
				'ContentType' => 'image/png',
				'StorageClass' => 'STANDARD',
				'ACL' => 'public-read'
		));
		return $result['ObjectURL']."\n";
	}

	public function ancolhari3($bucketName, $keyname3,$getpng3){
		$result = $this->S3->putObject(array(
				'Bucket' => $bucketName,
				'Key' => $keyname3,
				//'SourceFile' => $filename['tmp_name'],
				'Body' => file_get_contents($getpng3),
				'ContentType' => 'image/png',
				'StorageClass' => 'STANDARD',
				'ACL' => 'public-read'
		));
		return $result['ObjectURL']."\n";
	}	
	 
 }