<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	public function maintenance(){
		$this->db->from('tbl_setting');
		$this->db->where('description','maintenance');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

}
