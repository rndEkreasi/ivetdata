<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_model extends CI_Model {

	
	public function pettype(){
		$this->db->from('tbl_pettype');
		$query = $this->db->get();
		return $query->result();
	}

	public function getdoctor($idclinic,$email){
		$this->db->from('tbl_customer');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('email',$email);
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->result();
	}
	


}