<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends CI_Model {

	public function listshop(){
		// $this->db->select('id,nameservice,price,picture,idclinic');
		// $this->db->from('tbl_clinicservice');
		// $this->db->where('cat','medicine');
		// $this->db->where_in('idclinic',$clinicshop);
		// $this->db->limit('16');
		// $this->db->order_by('id','random');
		// $query = $this->db->get();
		// return $query->result();
		$query = $this->db->query("select * from tbl_clinicservice where cat='medicine' and idclinic in (select idclinic from tbl_clinic where shop=1)");
		return $query->result();
		//$query = $this->db->get();
		//return $query->result();
	}

	public function clinicshop(){
		$this->db->select('idclinic');
		$this->db->from('tbl_clinic');
		$this->db->where('shop','1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result_array();
	}

	function mydata($number,$offset,$profileid){
        $this->db->order_by('idinv','desc');
        $this->db->where('uid',$profileid);
        //$this->db->where('status','1');
        
        return $query = $this->db->get('tbl_invoice',$number,$offset)->result();        
    }

    function myjumlah_data($profileid){

        $this->db->where('uid',$profileid);
        //$this->db->where('status','1');
        
        return $this->db->get('tbl_invoice')->num_rows();
    }

    public function idinvoice($idinvoice){
    	//$this->db->select('idclinic');
		$this->db->from('tbl_invoice');
		$this->db->where('invoice',$idinvoice);
		$this->db->order_by('idinv','desc');
		$this->db->limit('1');
		//$this->db->limit('16');
		//$this->db->order_by('id','random');
		$query = $this->db->get();
		return $query->result();

    }
    
    public function itemcart($invoice){
        $this->db->select('nama_item AS item,qty,price,idclinic');
		$this->db->from('tbl_cart');
		$this->db->where('invoice_temp',$invoice);
		$this->db->where('status','1');
		$query = $this->db->get();  
		return $query->result();
    }

}