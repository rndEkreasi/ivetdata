<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendbird_model extends CI_Model {
	function data($number,$offset){
		$this->db->order_by('idarticle','desc');
		//$this->db->where('status','0');
		return $query = $this->db->get('tbl_article',$number,$offset)->result();		
	}
 
	function insert_channel($data){

		$this->db->insert('tbl_sendbird_channel',$data);
		$res = array(
			'status_code' => '200',
		);
		echo json_encode($res);
		// exit();
	}

	public function get_channel($owner_id,$vet_id){
		$this->db->from('tbl_sendbird_channel');
        $this->db->where("(owner_id = '".$owner_id."' AND vet_id = '".$vet_id."')");
		$query = $this->db->get();
		return $query->result();
	}

	public function getMember(){
		$this->db->from('tbl_member');
        $this->db->where("(uid > '278')");
		$query = $this->db->get();
		return $query->result();
	}

	public function getUser($uid){
		$this->db->from('tbl_member');
        $this->db->where("uid",$uid);
		$query = $this->db->get();
		return $query->result();
	}

	public function articlehome(){
		$this->db->from('tbl_article');
		$this->db->order_by('idarticle','desc');
		$this->db->where('status','0');
		$this->db->limit(3);
		$query = $this->db->get();
		return $query->result();
	}
}