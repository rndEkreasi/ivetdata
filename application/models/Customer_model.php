<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model {

	public function clinic($idclinic,$role){
		$this->db->from('tbl_customer');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('status','0');
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	function data($number,$offset,$idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('status','0');
		$this->db->order_by('idcustomer','desc');
		return $query = $this->db->get('tbl_customer',$number,$offset)->result();		
	}
 
	function jumlah_data($idclinic,$role){
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->where('status','0');
		return $this->db->get('tbl_customer')->num_rows();
	}

	public function petcustomer($idcustomer){
		$this->db->from('tbl_pet');
		$this->db->where('idowner',$idcustomer);
		$query = $this->db->get();
		return $query->result();
	}

	public function petcustomerphone($phone,$idclinic){
		$this->db->from('tbl_pet');
		$this->db->where('nohp',$phone);		
		$this->db->where('idclinic',$idclinic);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function petcustomernamehp($nama,$email){
		$this->db->from('tbl_pet');
		$this->db->where('namapemilik',$nama);
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function petcustomeremailhp($phone,$email){
		$this->db->from('tbl_customer');
		$this->db->where('nohp',$phone);
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function petcustomeremailonly($email){
		$this->db->from('tbl_customer');
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function petcustomeremail($email,$idclinic){
		$this->db->from('tbl_pet');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function petcustomeridowner($idowner,$idclinic){
		$this->db->from('tbl_pet');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('idowner',$idowner);
		$query = $this->db->get();
		return $query->result();
	}

	public function detailcustomer($idcustomer){
		$this->db->from('tbl_customer');
		$this->db->where('idcustomer',$idcustomer);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function cekcustomer($nohp,$idclinic){
		$this->db->from('tbl_customer');
		$this->db->where('nohp',$nohp);
		$this->db->where('status','0');
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}

	function search_blog($title,$idclinic){
        $this->db->like('nohp', $title , 'both');
        $this->db->where('idclinic',$idclinic);
        $this->db->order_by('nohp', 'ASC');
        $this->db->where('status','0');
        $this->db->limit(10);
        return $this->db->get('tbl_customer')->result();
    }


    public function datamember($uid){
		$this->db->from('tbl_member');
		$this->db->where('uid',$uid);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function cekbyname($vetclinic,$namecustomer){
		$this->db->from('tbl_customer');
		$this->db->like('nama',$namecustomer);
		$this->db->where('status','0');
		$this->db->where('idclinic',$vetclinic);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function cekbyidclinic($email,$uid,$idclinic){
		$this->db->from('tbl_customer');
		if($email==""){
		    $this->db->where('uid',$uid);
		}else{
		    $this->db->where('email',$email);
		}
		$this->db->where('idclinic',$idclinic);
		$query = $this->db->get();
		return $query->result();
	}
	
	function dataname($number,$offset,$idclinic,$role,$namecustomer){
		$this->db->order_by('idcustomer','desc');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		};
		$this->db->like('nama',$namecustomer);
		$this->db->where('status','0');
		return $query = $this->db->get('tbl_customer',$number,$offset)->result();		
	}

	public function cekbyphone($phone,$vetclinic,$namecustomer){
		$this->db->from('tbl_customer');
		//$this->db->like('nama',$namecustomer);
		$this->db->where('nohp',$phone);
		$this->db->where('status','0');
		$this->db->where('idclinic',$vetclinic);
		$query = $this->db->get();
		return $query->result();
	}


}