<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	public function detailprofile($profileid){
		$this->db->from('tbl_member');
		$this->db->where('uid',$profileid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();		
	}

	public function dataclinic($profileid){
		$this->db->from('tbl_clinic');
		$this->db->where('uid',$profileid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	public function datacustomer($profileid){
		$this->db->from('tbl_customer');
		$this->db->where('uid',$profileid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function detailclinic($idclinic){
		$this->db->from('tbl_clinic');
		$this->db->where('idclinic',$idclinic);
		//$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function pricing(){
		$this->db->from('tbl_service');
		$this->db->where('status','1');
		$query = $this->db->get();
		return $query->result();
	}

	public function pethome(){
		$this->db->from('tbl_pet');
		$this->db->limit(10);
		$this->db->order_by('idpet','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function clinicfav(){
		$this->db->from('tbl_clinic');
		$this->db->limit(5);
		$this->db->order_by('pet','desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function cekdoctor($idclinic,$role){
		$this->db->from('tbl_dokter');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function pethomedash($idclinic,$role,$manageto,$profileid){
		$this->db->from('tbl_pet');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
			if($manageto == '2'){
				$this->db->where('input_by',$profileid);
			}
		}
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function custhomedash($idclinic,$role,$manageto,$profileid){
		$this->db->from('tbl_customer');
		if($role < '2'){
			$this->db->where('idclinic',$idclinic);
			if($manageto == '2'){
				$this->db->where('input_by',$profileid);
			}
		}
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

    public function cekcust($idclinic,$profileid){
		$this->db->from('tbl_customer');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('uid',$profileid);
		$this->db->where('status','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function orderdash($idclinic,$role){
		$this->db->from('tbl_transaksi');
		// if($role < '2'){
		// 	$this->db->where('idclinic',$idclinic);
		// }
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}

	public function orderdashclinic($idclinic,$role){
		$this->db->from('tbl_invoice');
		$this->db->where('idclinic',$idclinic);
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}

	public function totalorder(){

	}

	public function revtoday(){
		//$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		$today = date('Y-m-d');
		$this->db->select_sum('jumlah');
	    $this->db->from('tbl_transaksi');
	    $this->db->where('status', '1');
		$this->db->where('date(tglbeli)', $today);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function revtodayclinic($idclinic,$manageto,$profileid){
		//$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		$today = date('Y-m-d');
		$this->db->select_sum('totalprice');
	    $this->db->from('tbl_invoice');
	    $this->db->where('status', '1');
	    if($manageto == '2'){
			$this->db->where('idvet',$profileid);
		}
	    $this->db->where('idclinic', $idclinic);
		$this->db->where('date(invdate)', $today);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function revyesterday(){
		//$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		$today = date('Y-m-d',strtotime('-1 day'));
		$this->db->select_sum('jumlah');
	    $this->db->from('tbl_transaksi');
	    $this->db->where('status', '1');
		$this->db->where('date(tglbeli)', $today);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}


	public function revyesterdayclinic($idclinic,$manageto,$profileid){
		//$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		$today = date('Y-m-d',strtotime('-1 day'));
		$this->db->select_sum('totalprice');
	    $this->db->from('tbl_invoice');
	    $this->db->where('status', '1');
	    $this->db->where('idclinic', $idclinic);
	    if($manageto == '2'){
			$this->db->where('idvet',$profileid);
		}
		$this->db->where('date(invdate)', $today);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function totalrev(){
		//$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		//$today = date('Y-m-d',strtotime('-1 day'));
		$this->db->select_sum('jumlah');
	    $this->db->from('tbl_transaksi');
	    $this->db->where('status', '1');
		//$this->db->where('date(tglbeli)', $today);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function totalrevclinic($idclinic,$manageto,$profileid){
		//$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		//$today = date('Y-m-d',strtotime('-1 day'));
		$month = date('m',strtotime('-1 month'));
		$year = date('Y');
		$this->db->select_sum('totalprice');
	    $this->db->from('tbl_invoice');
	    $this->db->where('status', '1');
	    $this->db->where('idclinic', $idclinic);
	    if($manageto == '2'){
			$this->db->where('idvet',$profileid);
		}
		//$this->db->where('date(tglbeli)', $today);
		$this->db->where('MONTH(invdate)', $month);
		$this->db->where('YEAR(invdate)', $year);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function monthrevclinic($idclinic,$manageto,$profileid){
		
		$month = date('m');
		$year = date('Y');
		$this->db->select_sum('totalprice');
	    $this->db->from('tbl_invoice');
	    $this->db->where('status', '1');
	    $this->db->where('idclinic', $idclinic);
	    if($manageto == '2'){
			$this->db->where('idvet',$profileid);
		}
		$this->db->where('MONTH(invdate)', $month);
		$this->db->where('YEAR(invdate)', $year);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function idservice($idservice){
	    $this->db->from('tbl_service');
		$this->db->where('idservice', $idservice);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function detaildiscount($discount){
		$this->db->from('tbl_discount');
		$this->db->where('code', $discount);
		$this->db->where('status', '1');
		$this->db->limit('1');
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}

	public function datadokter($profileid){
		$this->db->from('tbl_dokter');
		$this->db->where('uid', $profileid);
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();	
	}

	public function dailymonth(){
		for($i = 0; $i <=  date('t'); $i++)
		{
		   // add the date to the dates array
		   $dates[] = date('') . "" . date('') . "" . str_pad($i, 2, '0', STR_PAD_LEFT);
		}

		return $dates;
	}

	public function revbulan($idclinic,$manageto,$profileid){
		// for ($i = 0; $i <= 12; $i++) {
		//    // add the date to the dates array
		//    //$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		//    $bulan = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
		//    $bln = date('m',strtotime($bulan));
		//    $thn = date('Y',strtotime($bulan));
		//    //var_dump($bulan);
		//    $this->db->select_sum('totalprice');
	 //       $this->db->from('tbl_invoice');
	 //       $this->db->where('status', '1');
	 //       $this->db->where('idclinic', $idclinic);
		//    $this->db->where('MONTH(invdate)', $bln);
		//    $this->db->where('year(invdate)', $thn);
	 //       $query = $this->db->get();
	 //       $test = $query->result();
	 //       $jumlah[] = $test[0]->totalprice ;
	 //       if ($jumlah[$i] === NULL){
  //               $jumlah[$i] = '0';
  //           }
		// }		
		// return $jumlah;

		for($i = 0; $i <=  date('t'); $i++)
		{
		   // add the date to the dates array
		   $tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		   $this->db->select_sum('totalprice');
	       $this->db->from('tbl_invoice');
	       $this->db->where('status', '1');
	       $this->db->where('idclinic', $idclinic);
	       if($manageto == '2'){
				$this->db->where('idvet',$profileid);
			}
		   $this->db->where('date(invdate)', $tanggal);
			//$this->db->where('year(tgl_transaksi)', $tahun);
	       $query = $this->db->get();
	       $test = $query->result();
	       $jumlah[] = $test[0]->totalprice;
	       if ($jumlah[$i] === NULL){
                $jumlah[$i] = '0';
            }
		}
		
		return $jumlah;
	}


	public function bulan12(){
		for ($i = 0; $i <= 12; $i++) {
		   // add the date to the dates array
		   //$dates[] = date('') . "" . date('') . "" . str_pad($i, 2, '0', STR_PAD_LEFT);
			$month[] = date("M y", strtotime( date( 'Y-m-01' )." -$i months"));
		}

		return $month;
	}

	public function revbulan12($idclinic,$manageto,$profileid){
		for ($i = 0; $i <= 12; $i++) {
		   // add the date to the dates array
		   //$tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		   $bulan = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
		   $bln = date('m',strtotime($bulan));
		   $thn = date('Y',strtotime($bulan));
		   //var_dump($bulan);
		   $this->db->select_sum('totalprice');
	       $this->db->from('tbl_invoice');
	       $this->db->where('status', '1');
	       $this->db->where('idclinic', $idclinic);
		   $this->db->where('MONTH(invdate)', $bln);
		   $this->db->where('year(invdate)', $thn);
		   if($manageto == '2'){
				$this->db->where('idvet',$profileid);
			}
	       $query = $this->db->get();
	       $test = $query->result();
	       $jumlah[] = $test[0]->totalprice ;
	       if ($jumlah[$i] === NULL){
                $jumlah[$i] = '0';
            }
		}		
		return $jumlah;

		// for($i = 0; $i <=  date('t'); $i++)
		// {
		//    // add the date to the dates array
		//    $tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
		//    $this->db->select_sum('totalprice');
	 //       $this->db->from('tbl_invoice');
	 //       $this->db->where('status', '1');
	 //       $this->db->where('idclinic', $idclinic);
	 //       if($manageto == '2'){
		// 		$this->db->where('idvet',$profileid);
		// 	}
		//    $this->db->where('date(invdate)', $tanggal);
		// 	//$this->db->where('year(tgl_transaksi)', $tahun);
	 //       $query = $this->db->get();
	 //       $test = $query->result();
	 //       $jumlah[] = $test[0]->totalprice / 1000;
	 //       if ($jumlah[$i] === NULL){
  //               $jumlah[$i] = '0';
  //           }
		// }
		
		// return $jumlah;
	}

	public function cekkode($code){
	    $this->db->from('tbl_discount');
		$this->db->where('code', $code);
		$this->db->where('status', '1');
		//$this->db->where('year(tgl_transaksi)', $tahun);
	    $query = $this->db->get();
	    return $query->result();
	}
	
	public function datavet(){
		$this->db->from('tbl_member');
		$this->db->where('role', '1');
		$this->db->where('status','1');
	    $query = $this->db->get();
	    return $query->result();
	}

}