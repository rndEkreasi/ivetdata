<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function ceklogin($email,$passnya){
		$this->db->from('tbl_member');		
		$this->db->where('email', $email);
		$this->db->where('password',$passnya);
		//$this->db->limit('10');
		$query = $this->db->get();	
		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}		
	}


	public function read_user_information($user_email) {
		$this->db->from('tbl_member');
		$this->db->where('email',$user_email);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function adaemail($user_email){
		$this->db->from('tbl_member');
		$this->db->where('email',$user_email);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	public function adaphone($phone){
		$this->db->from('tbl_member');
		$this->db->where('phone',$phone);
		
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}
	public function cekuid($uid){
		$this->db->from('tbl_member');
		$this->db->where('uid',$uid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function cekuser($uid,$email){
		$this->db->from('tbl_member');
		$this->db->where('uid',$uid);
		$this->db->where('email',$email);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function adaclinic($uid){
		$this->db->from('tbl_clinic');
		$this->db->where('uid',$uid);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function country(){
		$this->db->from('tbl_country');
		$query = $this->db->get();
		return $query->result();
	}

	public function service(){
		$this->db->from('tbl_service');
		$this->db->where('status >','0');
		$query = $this->db->get();
		return $query->result();
	}

	public function dataservice($idservice){
		$this->db->from('tbl_service');
		$this->db->where('idservice',$idservice);
		$query = $this->db->get();
		return $query->result();
	}

	public function cektrx($order_id){
		$this->db->from('tbl_transaksi');
		$this->db->where('invoice',$order_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function cektoken($uid,$email,$token){
		$this->db->from('tbl_member');
		$this->db->where('email',$email);
		$this->db->where('uid',$uid);
		$this->db->where('unikcode',$token);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();
	}

	public function cekkta($kta){
		$this->db->from('tbl_kta');
		$this->db->where('nokta',$kta);
		$query = $this->db->get();
		return $query->result();
	}
}