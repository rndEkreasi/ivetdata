<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vets extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');
		$this->load->model('Clinic_model');
		$this->load->model('Login_model');
		$this->load->model('Setting_model');
		$this->load->model('Article_model');

    }

    public function index(){
		//pagination
			$all_article = $this->Clinic_model->clinicdisplay();
			$this->load->library('pagination');
			$config['base_url'] = base_url().'Vets/index/';
			$config['total_rows'] = $all_article;
			$config['per_page'] = 9;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);
			$data = array(
			'title' => 'Vets List - iVetdata.com',
			'allarticle' => $this->Clinic_model->clinicdata($config['per_page'],$from),
			'page' => 'vets',
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_new_view',$data);
		$this->load->view('website/vetlist_new',$data);
		$this->load->view('website/footer_new_view');
		//$this->load->view('footer_view',$data);			
	}

	public function vetsxml(){
		$dataclinic = $this->Clinic_model->allclinicshow();
		//var_dump($dataclinic);
		header("Content-type: text/xml");
		// Start XML file, echo parent node
		echo "<?xml version='1.0' ?>";
		echo '<markers>';
		$ind=0;
		// // Iterate through the rows, printing XML nodes for each
		// while ($row = @mysqli_fetch_assoc($dataclinic)){
		//   // Add to XML document node
		//   echo '<marker ';
		//   echo 'id="' . $row->idclinic . '" ';
		//   echo 'name="' . parseToXML($row['nameclininic']) . '" ';
		//   echo 'address="' . parseToXML($row['address']) . '" ';
		//   echo 'lat="' . $row['lat'] . '" ';
		//   echo 'lng="' . $row['lang'] . '" ';
		//  // echo 'type="' . $row['type'] . '" ';
		//   echo 'type="Resto"';
		//   echo '/>';
		//   $ind = $ind + 1;
		// }
		foreach ($dataclinic as $row) {
			$nameclinic = str_replace("&",'&amp;',$row->nameclinic);
			$nameclinicfix = str_replace('"','&quot;',$nameclinic);
			$phone = str_replace("&",'&amp;',$row->phone);
			$idclinic = $row->idclinic;
			echo '<marker ';
			echo 'id="' . $row->idclinic . '" ';
			echo 'name="' .$nameclinicfix.'" ';
			echo 'address="' . $row->address.', '.$row->city.'" ';
			echo 'lat="' . $row->lat . '" ';
			echo 'lng="' . $row->lang . '" ';
			echo 'type="bar" ';
			echo 'url="'.base_url().'vets/detail/?vets='.$idclinic.'"';
			echo '/>';
		}

		// End XML file
		echo '</markers>';
	}

	function parseToXML($htmlStr){
		$xmlStr=str_replace('<','&lt;',$htmlStr);
		$xmlStr=str_replace('>','&gt;',$xmlStr);
		$xmlStr=str_replace('"','&quot;',$xmlStr);
		$xmlStr=str_replace("'",'&#39;',$xmlStr);
		$xmlStr=str_replace("&",'&amp;',$xmlStr);
		return $xmlStr;
	}

	public function search(){
		$id = $this->input->post('name');
		$name = $this->input->post('vet');
		$city = $this->input->post('city');
		if(strlen($name)>30||strlen($city)>30)die("<script>alert('Please enter valid value');self.location.href='/';</script><h2>Please enable javascript</h2>");
		if($id<>""&&is_numeric($id))header('Location: /pet/search?idpet='.$id);
		if($city == ''){
		    if($name==''){
			    $searchvet = false;
			    $message = "Oops, Data not found";
		    }else{
		        $searchvet = $this->Clinic_model->searchname($name);
		        if(count($searchvet)>0){
		            $message = "success";
		        }else{
		           $searchvet = false; 
		            $message = "Oops, Data not found";
		        }
		    }
		}else{
		    $searchvet = $this->Clinic_model->search($name,$city);
		    if(count($searchvet)>0){
	            $message = "success";
	        }else{
	            $searchvet = false;
	            $message = "Oops, Data not found";
	        }
		}
		$data = array(
			'title' => 'Vets List - iVetdata.com',
			'message' => $message,
			'searchvet' => $searchvet,
			'page' => 'vets',
		);
        header("Cache-Control: max-age=3000, must-revalidate"); 
		$this->load->view('website/header_new_view',$data);
		$this->load->view('website/vetresult_new_view',$data);
		$this->load->view('website/footer_new_view');
	}
    
    public function searchnew(){
		$id = $this->input->post('name');
		$name = $this->input->post('vet');
		$city = $this->input->post('city');
		if(strlen($name)>30||strlen($city)>30)die("<script>alert('Please enter valid value');self.location.href='/searchnew';</script><h2>Please enable javascript</h2>");
		if($id<>""&&is_numeric($id)){
		    echo "<script>self.location.href='/pet/searchnew?idpet=".$id."';</script><h2>Please enable javascript</h2>";
		}
		if($city == ''){
		    if($name==''){
			    $searchvet = false;
			    $message = "Oops, Data not found";
		    }else{
		        $searchvet = $this->Clinic_model->searchname($name);
		        if(count($searchvet)>0){
		            $message = "success";
		        }else{
		           $searchvet = false; 
		            $message = "Oops, Data not found";
		        }
		    }
		}else{
		    $searchvet = $this->Clinic_model->search($name,$city);
		    if(count($searchvet)>0){
	            $message = "success";
	        }else{
	            $searchvet = false;
	            $message = "Oops, Data not found";
	        }
		}
		$data = array(
			'title' => 'Vets List - iVetdata.com',
			'message' => $message,
			'searchvet' => $searchvet,
			'page' => 'vets',
		);
        header("Cache-Control: max-age=3000, must-revalidate"); 
		//$this->load->view('website/header_new_view',$data);
		$this->load->view('website/vetresult_new_new_view',$data);
		//$this->load->view('website/footer_new_view');
	}
	
	public function mysearch(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;				
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$data = array(
					'title' => 'Vets Subscribe - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					//'datacountry' => $this->Login_model->country(),
					//'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
					//'datapet' => $datapet,
					'namaowner' => $nama,
					'page' => 'vets',
				);

				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/ownervetsearch_new_view',$data);
				//$this->load->view('website/footerdash_view',$data);

			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

	public function detail(){
		$idclinic = $this->input->get('vets');
		$dataclinic = $this->Clinic_model->detailclinic($idclinic);
		$namaclinic = $dataclinic[0]->nameclinic;
		//var_dump($dataclinic);
		$data = array(
			'title' => $namaclinic.' - iVetdata.com',
			'dataclinic' => $dataclinic,
			'nameclinic' => $namaclinic,
			'page' => 'vets',
		);
		$this->load->view('website/header_new_view',$data);
		$this->load->view('website/detailclinic_new_view',$data);
		$this->load->view('website/footer_new_view');
	}

	public function booking(){
		$idclinic = $this->input->get('clinic');
		$dataclinic = $this->Clinic_model->detailclinic($idclinic);
		$namaclinic = $dataclinic[0]->nameclinic;
		//var_dump($dataclinic);
		$data = array(
			'title' => $namaclinic.' - iVetdata.com',
			'dataclinic' => $dataclinic,
			'nameclinic' => $namaclinic,
			'page' => 'vets',
		);
		$this->load->view('website/header_view',$data);
		$this->load->view('website/bookingclinic_view',$data);
		//$this->load->view('website/footer_view');
	}

	public function vetsubs(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;				
				$nama = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				$dataclinic = $this->Home_model->datacustomer($profileid);
				
				$datapet = $this->Clinic_model->customerbyemail($email);
				//$datapet = $this->Clinic_model->customerbyidmember($profileid);
				//var_dump($datapet);
				$data = array(
					'title' => 'Vets Subscribe - iVetdata.com',
					'nama' => $nama,
					'profileid' => $profileid,
					'country' => $country,
					//'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'datacountry' => $this->Login_model->country(),
					'pettype' => $this->Pet_model->pettype(),
					//'nameclinic' => $dataclinic[0]->nameclinic,
					'datapet' => $datapet,
					'namaowner' => $nama,
				);

				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/ownersubsvet_view',$data);
				$this->load->view('website/footerdash_view',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

	public function vetsoncall(){
			$datamaintenance = $this->Setting_model->maintenance();
			$maintenance = $datamaintenance[0]->value;
			if($maintenance == '0'){
				if (isset($this->session->userdata['logged_in'])) {
					$profileid  = ($this->session->userdata['logged_in']['id']);
					$dataprofile = $this->Home_model->detailprofile($profileid);
					$role = $dataprofile[0]->role;				
					$nama = $dataprofile[0]->name;
					$email = $dataprofile[0]->email;		
					$country = $dataprofile[0]->country;
					$city = $dataprofile[0]->city;
					$photo = $dataprofile[0]->photo;
					$expdate = $dataprofile[0]->expdate;
					if($photo == '' ){
					    $profilephoto = base_url().'assets/images/team/05.jpg';
					}else{
					    $profilephoto = $photo;
					};
					$data = array(
						'title' => 'Vets Subscribe - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						//'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,	
						'manageto' => $dataprofile[0]->manageto,			
						
						'namaowner' => $nama,
						'page' => 'vets',
					);					
					//var_dump($idowner);
					//$this->load->view('login_user');
					$this->load->view('website/headerdash_view',$data);
					//var_dump($datapet);
					$this->load->view('website/vetoncall_view',$data);
					//$this->load->view('website/footerdash_view',$data);
				}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}
	}
	
	public function list_inquiry(){
		$country = $this->input->post('country');
		$province = $this->input->post('province');
		$city = $this->input->post('city');
	    $message = "<select class=form-control name=clinic required id=clinic><option value=0>Please select Clinic</option>";
		if(strtolower($country)==''){
		    $searchvet = $this->Clinic_model->searchall();
		}elseif(strtolower($province)=='all'){
		    $searchvet = $this->Clinic_model->searchcountry($country);
		}elseif(strtolower($city)=='all'){
		    $searchvet = $this->Clinic_model->searchprov($province);
		}else{
		    $searchvet = $this->Clinic_model->searchcity($city);
		}
		if(isset($_GET['id'])){
		    $id = $_GET['id'];
		}else{
		    $id = 0;
		}
		if(count($searchvet)>0){
            foreach($searchvet as $value){
                if($value->idclinic==$id){
                    $message .= "<option value='".$value->idclinic."' selected>".$value->nameclinic."</option>"; 
                }else{
                    $message .= "<option value='".$value->idclinic."'>".$value->nameclinic."</option>";  
                }
            }
	    }else{
	        $message .="<option value=-1>Data not found</option>";
	    }
        $message .= "</select>";
        echo $message;
	}
	
	public function delete(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$manageto = $dataprofile[0]->manageto;
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$idclinic = $this->input->get('idclinic');
			$vetclinic = $this->Home_model->cekcust($idclinic,$profileid);
			if($vetclinic[0]->idclinic == $idclinic){
				$this->db->where('uid',$profileid);
				$this->db->where('idclinic',$idclinic);
				$this->db->limit(1);
				$this->db->delete('tbl_customer');
				$this->session->set_flashdata('success', 'Congrats, Clinic has been deleted');
				$this->db->where('email',$email);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_pet',array('idclinic'=>'0'));
				redirect ('/vets/vetsubs');
			}else{
				$this->session->set_flashdata('error', 'You canot delete this pet');
				redirect ('/vets/vetsubs');
				//$edit = '0';
			}
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}
	
	public function pending(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$manageto = $dataprofile[0]->manageto;
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			
			//pagination
			$datapending = $this->Clinic_model->getpending($idclinic);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'reminder/vaccine/';
			$config['total_rows'] = count($datapending);
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	
            
			$data = array(
				'title' => 'Pet Owner Invitations - iVetdata.com',
				'from' => $from,
				'nama' => $nama,
				'profilephoto' => $profilephoto,
				'role' => $role,	
				'manageto' => $dataprofile[0]->manageto,			
				'all_clinic' => count($datapending),
				'idclinic' => $idclinic,
				'remindervacc' => $datapending,
				'text' => 'Pet Owner Invitations',
				'success' => $this->session->flashdata('success'),
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/pending_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}
	
	public function approve(){
		if(isset($_GET['token'])){
		    $token = $_GET['token'];
		    $idclinic = $_GET['clinic'];
		    $this->db->where('unikcode',$token);
		    $this->db->limit(1);
		    $this->db->from('tbl_member');
		    $query = $this->db->get();
		    $dt_member = $query->result();
		    if(count($dt_member)>0){
    		    $this->db->where('uid',$dt_member[0]->uid);
    		    $this->db->where('idclinic',"-".$idclinic);
    		    $this->db->limit(1);
    		    $this->db->update('tbl_customer',array('idclinic'=>$idclinic));
    		    $message = "Success, the request has been approved";
		    }else{
		        $message = "Error, data not found";
		    }
		    die("<html><body><script>alert('".$message."');self.location.href='https://app.ivetdata.com/'</script>Redirecting ...</body></html>");
		}
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$manageto = $dataprofile[0]->manageto;
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if(isset($_GET['id'])){
			    $idcustomer = $_GET['id'];
			    $idclinic = $_GET['clinic'];
			    $this->db->where('idcustomer',$idcustomer);
				$this->db->update('tbl_customer',array('idclinic'=>$idclinic));
			}
			redirect ('/vets/pending');
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}
	
	public function unapprove(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$manageto = $dataprofile[0]->manageto;
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if(isset($_GET['id'])){
			    $idcustomer = $_GET['id'];
			    $this->db->where('idcustomer',$idcustomer);
			    $this->db->limit(1);
				$this->db->delete('tbl_customer');
			}
			redirect ('/vets/pending');
		}else{
			$this->session->set_flashdata('success', 'Please login first');
			redirect ('login');
		}
	}
	
}

