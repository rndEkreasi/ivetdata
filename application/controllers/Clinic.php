<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clinic extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');
		$this->load->model('Clinic_model');

    }

    public function all(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$dataclinic = $this->Home_model->dataclinic($profileid);
			if(count($dataclinic)>0){
			    $idclinic = $dataclinic[0]->idclinic;
			}else{
			    $idclinic = 0;
			}
			//pagination
			$all_clinic = $this->Clinic_model->jumlah_data($idclinic,$role);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'clinic/all/';
			$config['total_rows'] = $all_clinic;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	

			$data = array(
				'title' => 'List of Clinic - iVetdata.com',
				'from' => $from,
				'nama' => $nama,
				'country' => $country,
				'all_clinic' => $all_clinic,
				'city' => $city,
				'role' => $role,
				'profilephoto' => $profilephoto,
				'dataclinic' => $this->Clinic_model->data($config['per_page'],$from,$idclinic,$role),
				'success' => $this->session->flashdata('success'),
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/allclinic_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{

			 redirect ('welcome');
		}
    }

    

}