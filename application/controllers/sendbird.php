<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendbird extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Sendbird_model');
		$this->load->model('Home_model');
		$this->load->model('Login_model');
		$this->load->model('Setting_model');

    }
    
	public function registSendbird()
	{

		$data = array('data'=>$this->Sendbird_model->getMember());

		$this->load->view('website/regist_sendbird',$data);

			
	}

	public function insertChannel()
	{
		$owner_id = $this->input->post('owner_id');
		$vet_id = $this->input->post('vet_id');
		$channel_url = $this->input->post('channel_url');


		$data = array(
			'owner_id' => $owner_id,
			'vet_id' => $vet_id,
			'channel_url' => $channel_url
	    );

		$this->Sendbird_model->insert_channel($data);
			
	}

	public function getChannel()
	{
		
		$owner_id = $this->input->post('owner_id');
		$vet_id = $this->input->post('vet_id');

		$data_channel = $this->Sendbird_model->get_channel($owner_id,$vet_id);
		$countData = count($data_channel);
		if ($countData > 0) {
			$res = array(
				'status_code' => '200',
				'channel_url' => $data_channel[0]->channel_url
			);
		} else {
			$res = array(
				'status_code' => '404'
			);
		}
		echo json_encode($res);
	}

	public function getUsers()
	{
		$uid = $this->input->post('uid');
		$res_arr = array();
		$error_code = array();
		foreach ($uid as $key => $value) :
			$url = "https://api-3A0A0751-6F1C-43F0-934B-6DEDBDD61009.sendbird.com/v3/users/".$value;
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					    "Api-Token: 03ae920eca433f3dd7636eebf658478648144ad1",
					  ));
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    /* execute request */
	    $result = curl_exec($ch);
	    $result = json_decode($result);
	    if (!isset($result->error)) {
	    	$res = array(
	    		'status_code' => '200',
	    		'user_id' => $result->user_id,
	    		'user_id' => $result->nickname,
	    	);
	    } else {
	    	$res = array(
	    		'status_code' => '404',
	    		'user_id' => $value,
	    		'error_message' => 'User not found, please contact administrator',
	    	);
	    }

	    array_push($res_arr, $res);
	    array_push($error_code, $res['status_code']);
		endforeach;

		if (in_array('404', $error_code)) {
			$res = array(
	    		'status_code' => '404',
	    		'error_message' => 'User not found, please contact administrator',
	    	);
		} else {
			$res = array(
	    		'status_code' => '200'
	    	);
		}
		
		echo json_encode($res);
    exit();	
	}


	public function customerService(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
				$profileid  = ($this->session->userdata['logged_in']['id']);
				$dataprofile = $this->Home_model->detailprofile($profileid);
				$role = $dataprofile[0]->role;				
				$name = $dataprofile[0]->name;
				$email = $dataprofile[0]->email;		
				$country = $dataprofile[0]->country;
				$city = $dataprofile[0]->city;
				$photo = $dataprofile[0]->photo;
				$expdate = $dataprofile[0]->expdate;
				if($photo == '' ){
				    $profilephoto = base_url().'assets/images/team/05.jpg';
				}else{
				    $profilephoto = $photo;
				};
				//$datapet = $this->Clinic_model->customerbyidmember($profileid);
				//var_dump($datapet);
				$data = array(
					'title' => 'Customer Service - iVetdata.com',
					'nama' => $name,
					'profileid' => $profileid,
					'country' => $country,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'datacountry' => $this->Login_model->country(),
					'nickname' => $name,
				);

				$this->load->view('sendbird_cs',$data);
			}else{
				$this->session->set_flashdata('success', 'Please login first.');
				redirect ('login');
			}
		}else{
			redirect ('maintenance');
		}		
	}

	public function getUnreadmsg(){
		
		$channel_url = $this->input->post('channel_url');
		$user_id = $this->input->post('user_id');

		$url = sendbird_apiurl."/group_channels/".$channel_url."/messages/unread_count?user_ids=".$user_id;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				    "Api-Token: ".sendbird_apitoken,
				  ));
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    /* execute request */
    $result = curl_exec($ch);
    $result = json_decode($result);
    $unread_msg = $result->unread->$user_id;
    $res = 0;
    if (isset($unread_msg)) {
    	$res = $unread_msg;
    }
		echo json_encode($res);
	}
}

