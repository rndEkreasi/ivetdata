<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		$this->load->library('recaptcha');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Article_model');

    }
    
	public function index(){
		$data = array(
			'title' => 'Contact Us - iVetdata.com',
			'page' => 'contact',		
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_new_view',$data);
		$this->load->view('website/contact_new',$data);
		$this->load->view('website/footer_new_view',$data);
	}

	public function vetoncall(){
		$data = array(
			'title' => 'Contact Us - iVetdata.com',			
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_view',$data);
		$this->load->view('website/vetoncall_view');
		$this->load->view('website/footer_view',$data);
	}

	public function send_email(){
		// $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
 
  //       $userIp=$this->input->ip_address();
     
  //       $secret = $this->config->item('google_secret');
   
  //       $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;
 
  //       $ch = curl_init(); 
  //       curl_setopt($ch, CURLOPT_URL, $url); 
  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
  //       $output = curl_exec($ch); 
  //       curl_close($ch);      
         
  //       $status= json_decode($output, true);
 
  //       if ($status['success']) {
  //           // print_r('Google Recaptcha Successful');
            // exit;
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
			$this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');
			if ($this->form_validation->run() == FALSE){
				echo 'you cant send email';
				// $data = array(
				// 	'title' => 'Contact Us - iVetdata.com',	
				// 	'error' => 	'Sorry you cannot sending the message',	
				// );
				// //$this->load->view('login_user');
				// $this->load->view('website/header_view',$data);
				// $this->load->view('website/thanks_email',$data);
				// $this->load->view('website/footer_view',$data);
			}else{
				//$this->load->spark('recaptcha-library/1.0.1');
				//$recaptcha = $this->input->post('g-recaptcha-response');
				$recaptcha = "1";
		        if (!empty($recaptcha)) {
		            $response = $this->recaptcha->verifyResponse($recaptcha);
		            $response['success']=true;
		            if (isset($response['success']) and $response['success'] === true) {
		                //echo "You got it!";
		                $name = $this->input->post('name');
						$email = 'contact@ivetdata.com';
						$emailnya = $this->input->post('email');
						$message = $this->input->post('message');
						$subjectnya = 'Email message from '.$name.' '.$emailnya;
						$this->load->library('email'); 
							// $query = $this->db->query("SELECT * FROM tbl_transaksi WHERE invoice='$invoice'");
							// $result = $query->result_array();
							// $member= $result[0]['member'];		
						   
						$subject = $subjectnya;	
						$text = $message;
					    //$email = $user_email;
					    //$this->load->view('testemail_view');
					    $this->load->library('EmailSender.php');
					    $this->emailsender->send($email,$subject,$text);
					    $data = array(
							'title' => 'Contact Us - iVetdata.com',	
							'success'		
						);
						//$this->load->view('login_user');
						$this->load->view('website/header_new_view',$data);
						$this->load->view('website/contact_new',$data);
						$this->load->view('website/footer_new_view',$data);
		            }
		        }else{
		        	redirect('welcome');
		        }
	            
			}
   //      }else{
   //          $this->session->set_flashdata('flashError', 'Sorry Google Recaptcha Unsuccessful!!');
   //          $data = array(
			// 	'title' => 'Contact Us - iVetdata.com',	
			// 	'error' => 	'Sorry Google Recaptcha Unsuccessful!!',	
			// );
			// //$this->load->view('login_user');
			// $this->load->view('website/header_view',$data);
			// $this->load->view('website/thanks_email',$data);
			// $this->load->view('website/footer_view',$data);
   //      }

		


	}
}