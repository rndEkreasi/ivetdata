<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Service_model');
		$this->load->model('Login_model');
		$this->load->model('Sales_model');
		$this->load->model('Customer_model');
		$this->load->model('Pet_model');
    }

    public function all(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			if($role == '0'){
				redirect ('sales/myinvoice');
			}
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
		    //pagination
			if(isset($_GET['invoice'])){
			    $query = $this->input->get('invoice');
			    $all_sales = $this->Sales_model->jumlah_search($query,$idclinic,$role);
			}else{
			    $all_sales = $this->Sales_model->jumlah_data($idclinic,$role);
			}
			
			$this->load->library('pagination');
			$config['base_url'] = base_url().'sales/all/';
			$config['total_rows'] = $all_sales;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	

			if(isset($_GET['invoice'])){
			    $datasales = $this->Sales_model->search($query,$config['per_page'],$from,$idclinic,$role);
			}else{
			    $datasales = $this->Sales_model->data($config['per_page'],$from,$idclinic,$role);
			}

			$data = array(
				'title' => 'Sales list - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'from' => $from,			
				'nameclinic' => $dataclinic[0]->nameclinic,
				'all_sales' => $all_sales,
				'datasales' => $datasales,
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/mysales_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{

			redirect ('login');
		}
    }

    public function myinvoice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;			
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			// $dataclinic = $this->Home_model->dataclinic($profileid);
			// $idclinic = $dataclinic[0]->idclinic;
			//pagination
			$all_sales = $this->Sales_model->myjumlah_data($profileid);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'sales/myinvoice/';
			$config['total_rows'] = $all_sales;
			$config['per_page'] = 10;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);	
			$data = array(
				'title' => 'Sales list - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'from' => $from,				
				//'nameclinic' => $dataclinic[0]->nameclinic,
				'all_sales' => $all_sales,
				'datasales' => $this->Sales_model->mydata($config['per_page'],$from,$profileid),
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				//'clinicfav' => $this->Home_model->clinicfav(),
				
			);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/mysales_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{

			redirect ('login');
		}
    }

    public function invoice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$idpet = $this->input->get('idpet');
			if(isset($_GET['idowner'])){
				$idcustomer = $this->input->get('idowner');
				$detailcustomer = $this->Customer_model->detailcustomer($idcustomer);
				$namecustomer = $detailcustomer[0]->nama;
				$emailcustomer = $detailcustomer[0]->email;
				$hpcustomer = $detailcustomer[0]->nohp;
			}else{
				//echo  'Gak ada';
				$namecustomer = '';
				$emailcustomer = '';
				$hpcustomer = '';
			};
			if(isset($_GET['idpet'])){
				$idpet = $this->input->get('idpet');
				$detailpet = $this->Pet_model->detailpet($idpet);
				$namapet = $detailpet[0]->namapet;
				$autofill = '1';
			}else{
				$idpet = '';
				$namapet = '';
				$autofill = '0';
			}

			$jumlahservice = count($this->Service_model->clinic($idclinic,$role));
	    	$data = array(
					'title' => 'Create Invoice - iVetdata.com',
					'nama' => $nama,
					'profileid' => $profileid,
					'country' => $country,
					'city' => $city,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,
					'namecustomer' => $namecustomer,
					'emailcustomer' => $emailcustomer,
					'hpcustomer' => $hpcustomer,			
					'nameclinic' => $dataclinic[0]->nameclinic,
					'jumlahservice' => count($this->Service_model->clinic($idclinic,$role)),
					'idpet' => $idpet,
					'autofill' => $autofill,
					'namapet' => $namapet,
					'vetlist' =>  $this->Service_model->getvets($idclinic),
					//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					
			);
			//var_dump($jumlahservice);
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/invoiceclient_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			 redirect ('login');
		}
    }

    public function detail(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			if ($role != '0') {
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);
				$pictureclinic = $dataclinic[0]->picture;
			}
			else{
				$idclinic = '0';
				$dataclinic = '';
				$pictureclinic = '';
			}
			
			$invoice = $this->input->get('invoice');
			$datainvoice = $this->Sales_model->detailinv($invoice);	
			
			if(count($datainvoice) == 0){
				$this->session->set_flashdata('error', 'Sorry the invoice not found.');
				if($role == '0'){
					redirect ('sales/myinvoice');
				}else{
					redirect ('sales/all');
				}
				
			}else{
    			$clinicinv = $datainvoice[0]->idclinic;
    			$dataclinicinv = $this->Sales_model->detailclinic($clinicinv);
    			$nameclinicinv = $dataclinicinv[0]->nameclinic;
    			$pictureclinic = $dataclinicinv[0]->picture;
				$noclinic = $datainvoice[0]->idclinic;
				if($noclinic != $idclinic){					
					if($role == '0'){
						$uidinvoice = $datainvoice[0]->uid;
						if($uidinvoice == $profileid){
							$totalqty = $datainvoice[0]->totalqty;
							$iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
							$data = array(
								'title' => 'Detail Invoice - iVetdata.com',
								'nama' => $nama,
								'country' => $country,
								'city' => $city,
								'profilephoto' => $profilephoto,
								'role' => $role,	
								'manageto' => $dataprofile[0]->manageto,
								'nameclinic' => $nameclinicinv,
								'pictureclinic' => $pictureclinic,		
								//'nameclinic' => $dataclinic[0]->nameclinic,
								//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
								'success' => $this->session->flashdata('success'),
								'datainvoice' => $datainvoice,
								'iteminvoice' => $iteminvoice,
								//'clinicfav' => $this->Home_model->clinicfav(),					
							);
							$this->load->view('website/headerdash_view',$data);
							$this->load->view('website/viewinvclient',$data);
							$this->load->view('website/footerdash_view',$data);

						}else{
							$this->session->set_flashdata('error', 'Sorry the invoice not for you.');
							redirect ('sales/myinvoice');
						}						
					}else{
						$this->session->set_flashdata('error', 'Sorry the invoice not for your clinic.');
						redirect ('sales/all');
					}
				}else{
					$totalqty = $datainvoice[0]->totalqty;
					$iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
					$data = array(
						'title' => 'Detail Invoice - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,
						'manageto' => $dataprofile[0]->manageto,		

						'nameclinic' => $dataclinic[0]->nameclinic,
						'pictureclinic' => $pictureclinic,	
						//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
						'success' => $this->session->flashdata('success'),
						'error' => $this->session->flashdata('error'),
						'datainvoice' => $datainvoice,
						'iteminvoice' => $iteminvoice,
						//'clinicfav' => $this->Home_model->clinicfav(),					
					);
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/viewinvclient',$data);
					$this->load->view('website/footerdash_view',$data);
				}
			}	    	
		}else{
			 redirect ('login');
		}
    }

    public function sendinvoice(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$idclinic = $dataclinic[0]->idclinic;
			$namaclinic =  $dataclinic[0]->nameclinic;
			$addressclinic = $dataclinic[0]->address;
			$cityclinic = $dataclinic[0]->city;
			$countryclinic = $dataclinic[0]->country;
			$invoice = $this->input->get('invoice');
			$datainvoice = $this->Sales_model->detailinv($invoice);			
			if(count($datainvoice) == 0){
				$this->session->set_flashdata('error', 'Sorry the invoice not found.');
				redirect ('sales/all');
			}else{
				$noclinic = $datainvoice[0]->idclinic;
				if($noclinic != $idclinic){
					$this->session->set_flashdata('error', 'Sorry the invoice not for your clinic.');
					redirect ('sales/all');
				}else{
					$totalqty = $datainvoice[0]->totalqty;
					$vetclinic = $datainvoice[0]->idclinic;
					$email = $datainvoice[0]->emailclient;
					//$email = 'jaynael@gmail.com';
					$nameclient = $datainvoice[0]->nameclient;
					$phoneclient = $datainvoice[0]->phoneclient;
					$totalpriceinv = $datainvoice[0]->totalprice;
					$diskon = $datainvoice[0]->discount;
					$tax = $datainvoice[0]->tax;
					$invdate = date('d M Y H:i:s',strtotime($datainvoice[0]->invdate));
					$iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
					$this->Sales_model->sendemail($email,$nameclient,$phoneclient,$totalpriceinv,$iteminvoice,$invoice,$namaclinic,$addressclinic,$cityclinic,$countryclinic,$invdate,$diskon,$tax);
					$this->session->set_flashdata('success', 'Invoice sent to '.$email);
					redirect ('sales/all');

					// $data = array(
					// 	'title' => 'Detail Invoice - iVetdata.com',
					// 	'nama' => $nama,
					// 	'country' => $country,
					// 	'city' => $city,
					// 	'profilephoto' => $profilephoto,
					// 	'role' => $role,			
					// 	'nameclinic' => $dataclinic[0]->nameclinic,
					// 	//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
					// 	'success' => $this->session->flashdata('success'),
					// 	'datainvoice' => $datainvoice,
					// 	'iteminvoice' => $iteminvoice,
					// 	//'clinicfav' => $this->Home_model->clinicfav(),					
					// );
					// $this->load->view('website/headerdash_view',$data);
					// $this->load->view('website/viewinvclient',$data);
					// $this->load->view('website/footerdash_view',$data);
				}
			}	    	
		}else{
			 redirect ('login');
		}
    }


    public function saveinv(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$namecustomer = $this->input->post('namecustomer');
			$nohp = $this->input->post('nohp');
			$emailcustomer = $this->input->post('emailcustomer');
			$pass = 'ivetdata';
			$totalprice = $this->input->post('totalprice');
			$discount = $this->input->post('discount');
			$totalqty = $this->input->post('allqty');
			$tax = $this->input->post('tax');
			$item = $this->input->post('item');
			$price = $this->input->post('price');
			$methode = $this->input->post('methode');
			$qty = $this->input->post('qty');
			$idvet = $this->input->post('idvet');
			$idpet = $this->input->post('petdata');
			$cekcustomer = $this->Customer_model->cekcustomer($nohp,$idclinic);
			if (count($cekcustomer) == '0'){
				$user_email = $emailcustomer;
				$phone = $nohp;
				$cekemail = $this->Login_model->adaphone($phone);
				$adaemail = count($cekemail);
				if ($adaemail == '1'){
					$uid = $cekemail[0]->uid;
				}else{
					$datamember = array(
						'name' => $namecustomer,
						'email' => $emailcustomer,
						'phone' => $nohp,
						'status' => '1',
						'role' => '0',
						'manageto' => '0',
						'password' => md5($pass),
						'tgl_reg' => date('Y-m-d h:i:s'),
						'unikcode' => md5($emailcustomer),										
					);
					$this->db->insert('tbl_member',$datamember);
					$uid = $this->db->insert_id();
				}
				//insertdatacustome
				$datacustomer = array(
					'nama' => $namecustomer,
					'nohp' => $nohp,
					'email' => $emailcustomer,
					'idclinic' => $idclinic,
					'uid' => $uid,		
				);
				$this->db->insert('tbl_customer',$datacustomer);
				$idcustomer = $this->db->insert_id();
			}else{
				$idcustomer = $cekcustomer[0]->idcustomer;
				$uid = $cekcustomer[0]->uid;
			};
			$digits = 3;
			$unik = rand(pow(10, $digits-1), pow(10, $digits)-1);
			$digits2 = 2;
			$unik2 = rand(pow(10, $digits2-1), pow(10, $digits2)-1);
			//$his = date('His');
			$invoice = $idclinic.$idcustomer.$unik.$unik2;
			$totalprice = preg_replace('~\.0+$~','',$totalprice);
			//$totalqty = preg_replace('~\.0+$~','',$totalqty);
			$countitem = count($item);
			$invdatefix = $this->input->post('invdate');
			//insert tbl invoice;
			//var_dump($invdatefix);
			
			//var_dump($item);
			if($item == ''){
				//echo 'no item';
				$this->session->set_flashdata('error', 'Error, Please add item and price.');
				redirect ('sales/invoice');
			}else{
				$datainvoice = array(				
					'invdate' => date('Y-m-d H:i:s',strtotime($invdatefix)),
					'uid' =>  $uid,
					'nameclient' => $namecustomer,
					'emailclient' => $emailcustomer,
					'phoneclient' => $nohp,
					'idclinic' => $idclinic,
					'idvet' => $idvet,
					'status' => '1',
					'totalprice' => $totalprice,
					'totalqty' => $countitem,
					'invoice' => $invoice,
					'discount' => $discount,
					'methode' => $methode,
					'tax' => $tax,
					'note' => $this->input->post('note'),
					'idpet' => $idpet,
				);
				$this->db->insert('tbl_invoice',$datainvoice);

				foreach($item as $key=>$val){				
					//var_dump($dataitem);
					$dataitem = array(
							'invoice' => $invoice,
							'item' => $val,
							'price' => $price[$key],
							'qty' => $qty[$key],
							'idclinic' => $idclinic,	
							'idvet' => $idvet,
							'tanggal' => date('Y-m-d H:i:s'),			
					);
					//$this->Sales_model->insertitem($dataitem);
					if($val == ''){	
						$this->Sales_model->insertitem($dataitem);
					}else{
						$this->Sales_model->insertitem($dataitem);
						//$this->Sales_model->updateitem($val,$dataitem,$idclinic);
						
						// //update qty
						$nameservice = $val;
						$cekservice = $this->Service_model->cekservice($nameservice,$idclinic);
						if(count($cekservice) == '0'){
							// $idservice = $cekservice[0]->id;
							// $qtyawal = $cekservice[0]->qty;
							// $qtybeli = $dataitem['qty'];
							// $qtyfix = $qtyawal - $qtybeli;
							// $dataupdate = array(
							// 	'qty' => $qtyfix,
							// );
							// $this->db->where('id',$idservice);
							// $this->db->update('tbl_clinicservice',$dataupdate);
						}else{
							$idservice = $cekservice[0]->id;
							$qtyawal = $cekservice[0]->qty;
							$qtybeli = $dataitem['qty'];
							$qtyfix = $qtyawal - $qtybeli;
							$dataupdate = array(
								'qty' => $qtyfix,
							);
							$this->db->where('id',$idservice);
							$this->db->update('tbl_clinicservice',$dataupdate);
						}
						//redirect ('sales/detail/?invoice='.$invoice);
						
					}				
				}
				//$this->session->set_flashdata('success', 'Congrats invoice sales has been inserted.');
				redirect ('sales/detail/?invoice='.$invoice);
			}

			//$this->session->set_flashdata('success', 'Congrats invoice sales has been inserted.');
			//redirect ('sales/detail/?invoice='.$invoice);


		}else{
			 redirect ('login');
		}
	}

	public function edit(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$invoice = $this->input->get('invoice');
			$datainvoice = $this->Sales_model->detailinv($invoice);			
			if(count($datainvoice) == 0){
				$this->session->set_flashdata('error', 'Sorry the invoice not found.');
				redirect ('sales/all');
			}else{
				$noclinic = $datainvoice[0]->idclinic;
				if($noclinic != $idclinic){
					$this->session->set_flashdata('error', 'Sorry the invoice not for your clinic.');
					redirect ('sales/all');
				}else{
					$totalqty = $datainvoice[0]->totalqty;
					$iteminvoice = $this->Sales_model->iteminv($invoice,$totalqty);
					$data = array(
						'title' => 'Detail Invoice - iVetdata.com',
						'nama' => $nama,
						'country' => $country,
						'city' => $city,
						'profilephoto' => $profilephoto,
						'role' => $role,			
						'nameclinic' => $dataclinic[0]->nameclinic,
						//'datacustomer' => $this->Service_model->data($config['per_page'],$from,$idclinic,$role),
						'success' => $this->session->flashdata('success'),
						'datainvoice' => $datainvoice,
						'iteminvoice' => $iteminvoice,
						//'clinicfav' => $this->Home_model->clinicfav(),					
					);
					$this->load->view('website/headerdash_view',$data);
					$this->load->view('website/editinvoiceclient_view',$data);
					$this->load->view('website/footerdash_view',$data);
				}
			}	    	
		}else{
			 redirect ('login');
		}
    }


    public function delete(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$idinvoice = $this->input->get('id');
			$datainvoice = $this->Sales_model->detailinvoice($idinvoice);			
			if(count($datainvoice) == 0){
				$this->session->set_flashdata('error', 'Sorry the invoice not found.');
				redirect ('sales/all');
			}else{
				$noclinic = $datainvoice[0]->idclinic;
				if($noclinic != $idclinic){
					$this->session->set_flashdata('error', 'Sorry the invoice not for your clinic.');
					redirect ('sales/all');
				}else{
					$dataupdate = array(
						'status' => '2',
						'removeby' => $nama,
					);
					$this->db->where('idinv',$idinvoice);
					$this->db->update('tbl_invoice',$dataupdate);
					$this->session->set_flashdata('success', 'Congrats, Invoice has been deleted.');
					redirect ('sales/all');					
				}
			}	    	
		}else{
			 redirect ('login');
		}
    }

    public function exportsaleslist(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$profilephoto = $dataprofile[0]->photo;
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);

			//pagination
			$all_sales = $this->Sales_model->jumlah_data($idclinic,$role);
			$datasales = $this->Sales_model->data($all_sales,0,$idclinic,$role);
			$i = 0;
			$iteminvoice = array();
			foreach($datasales as $sales){
			    $datainvoice = $this->Sales_model->detailinv($sales->invoice);
			    $totalqty = $datainvoice[0]->totalqty;
				$iteminvoice[$i] = $this->Sales_model->iteminv($sales->invoice,$totalqty);
			    $i++;
			}

			$data = array(
				'title' => 'Sales list - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $dataprofile[0]->manageto,
				'nameclinic' => $dataclinic[0]->nameclinic,
				'all_sales' => $all_sales,
				'datasales' => $datasales,
				'iteminvoice' => $iteminvoice,
			);
			
			$this->load->view('website/export_sales',$data);

		}else{

			redirect ('login');
			
		}
    }

}