<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Article_model');

    }
    
	public function all(){
		//pagination
			$all_article = $this->Article_model->jumlah_data();
			$this->load->library('pagination');
			$config['base_url'] = base_url().'news/all/';
			$config['total_rows'] = $all_article;
			$config['per_page'] = $all_article;
			//styling pagination
			$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
		    $config['full_tag_close'] = '</ul>';
		    $config['num_tag_open'] = '<li>';
		    $config['num_tag_close'] = '</li>';
		    $config['cur_tag_open'] = '<li class="active"><a href="#">';
		    $config['cur_tag_close'] = '</a></li>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';
		    $config['first_tag_open'] = '<li>';
		    $config['first_tag_close'] = '</li>';
		    $config['last_tag_open'] = '<li>';
		    $config['last_tag_close'] = '</li>';

		    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
		    $config['prev_tag_open'] = '<li>';
		    $config['prev_tag_close'] = '</li>';


		    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
		    $config['next_tag_open'] = '<li>';
		    $config['next_tag_close'] = '</li>';

			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);
		$data = array(
			'title' => 'News - iVetdata.com',
			'allarticle' => $this->Article_model->data($config['per_page'],$from),
			'page' => 'news',
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_new_view');
		$this->load->view('website/news_view',$data);
		$this->load->view('website/footer_new_view');
		//$this->load->view('footer_view',$data);			
	}

	public function detail($slug){
		$detailarticle = $this->Article_model->detail($slug);
		$idarticle = $detailarticle[0]->idarticle;
		$title = $detailarticle[0]->title;
		$data = array(
			'title' => $title.' - iVetdata.com',
			'detailarticle' => $this->Article_model->detail($slug),
            'page' => 'News',
			//'allarticle' => $this->Article_model->data($config['per_page'],$from),
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_new_view',$data);
		$this->load->view('website/newsdetail_view',$data);
		$this->load->view('website/footer_new_view');
	}

}

