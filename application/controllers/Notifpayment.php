<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifpayment extends CI_Controller {	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
         $this->load->model('Sales_model');
         $this->load->model('Shop_model');
         $this->load->model('Home_model');
    }

    public function payxendit(){
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $data = file_get_contents("php://input");
            //print_r("\n\$data contains the payment data \n\n");
            $jsondata = json_decode($data);
            //var_dump($jsondata);
            $invoice = $jsondata->external_id;
            $trxinvoice = $this->Sales_model->detailtrx($invoice);
            if(count($trxinvoice) == '1'){
                $status = $trxinvoice[0]->status;
                if($status == '1'){
                    print_r("Invoice sudah dibayar");
                }else{
                    $month = $trxinvoice[0]->month;
                    $staff = $trxinvoice[0]->staff;
                    $idclinic = $trxinvoice[0]->idclinic;
                    $idtrx = $trxinvoice[0]->idtrx;
                    //getmemberownerintbl
                    $clinicowner = $this->Sales_model->clinicowner($idclinic);
                    $expowner = $clinicowner[0]->expdate;
                    $newexpdate = date('Y-m-d',strtotime('+'.$month.' month',strtotime($expowner)));
                    //var_dump($newexpdate);
                    //print_r($newexpdate);

                    // //update member
                    $updatemember = array(
                        'expdate' => $newexpdate,
                        'idservice' => '2',
                    );
                    $this->db->where('idclinic',$idclinic);
                    $this->db->update('tbl_member',$updatemember);

                    //update invoice
                    $updatetrx = array(
                        'status' => '1',
                        'tglapprove' => date('Y-m-d H:i:s'),
                        'approved' => 'Otomatis VA',
                    );
                    $this->db->where('idtrx',$idtrx);
                    $this->db->update('tbl_transaksi',$updatetrx);


                    print_r("Transaksi berhasil diupdate");
                    //
                }
            }else{
                //payment user to clinic
                
            }

        }else{
            print_r("Cannot ".$_SERVER["REQUEST_METHOD"]." ".$_SERVER["SCRIPT_NAME"]);
        }
    }


    public function callxendit(){
		if ($_SERVER["REQUEST_METHOD"] === "POST") {
	        $data = file("php://input");
            $xml = "";
            foreach($data as $dt){
            	$xml = $xml.$dt;
            }
            $dt = json_decode($xml,true);
            if(count($dt)==0){
                echo '{"data":"INVALID JSON Format"}';
            }else{
                if(!isset($dt['external_id']))die("Error transaction not found");
                $invoice = $dt['external_id'];
                $dt_invoice = $this->Sales_model->detailinv($invoice);
                if(count($dt_invoice)==0){
                       $dt_invoice = $this->Sales_model->itemtransaksi($invoice);
                       if(count($dt_invoice)>0){
                            $idclinic = $dt_invoice[0]->idclinic;
                            $dataclinic = $this->Home_model->detailclinic($idclinic);
                            $namaclinic = $dataclinic[0]->nameclinic;
                            $addressclinic = $dataclinic[0]->address;
                            $cityclinic = $dataclinic[0]->city;
                            $countryclinic = $dataclinic[0]->country;
                            $email = $dataclinic[0]->email;
                            $phone = $dataclinic[0]->phone;
                            $invdate = date("Y-m-d H:i:s");
                            $diskon = 0;
                            $tax = 0;
                            $dataprofile = $this->Home_model->detailprofile($dt_invoice[0]->uid);
                            if(count($dataprofile)>0){
                                $name2 = $dataprofile[0]->name;
                                $email2 = $dataprofile[0]->email;
                                $phone2 = $dataprofile[0]->phone;
                            }else{
                                $name2 = $name;
                                $email2 = $email;
                                $phone2 = $phone;
                            }
                            $amount = $dt_invoice[0]->jumlah;
                            $this->db->where('invoice',$invoice);
                            $this->db->update("tbl_transaksi",array('status'=>'1','tglapprove'=>date('Y-m-d H:i:s'),'approved'=>'system')); 
                            $this->db->where('uid',$dt_invoice[0]->uid);
                            $this->db->update("tbl_member",array('idservice'=>'2','expdate'=>date('Y-m-d H:i:s',strtotime($dataprofile[0]->expdate)+466560000))); 
                        }else{
                            die("Error transaction not found");
                        }
                }else{
                        if($dt_invoice[0]->methode=="shop"){
                            $iteminvoice = $this->Shop_model->itemcart($invoice);
                        }else{
                            $iteminvoice = $this->Sales_model->iteminv($invoice,$dt_invoice[0]->totalqty);
                        }
                        $idclinic = $iteminvoice[0]->idclinic;
                        $dataclinic = $this->Home_model->detailclinic($idclinic);
                        $namaclinic = $dataclinic[0]->nameclinic;
                        $addressclinic = $dataclinic[0]->address;
                        $cityclinic = $dataclinic[0]->city;
                        $countryclinic = $dataclinic[0]->country;
                        $email = $dataclinic[0]->email;
                        $phone = $dataclinic[0]->phone;
                        $invdate = $dt_invoice[0]->invdate;
                        $diskon = $dt_invoice[0]->discount;
                        $tax = $dt_invoice[0]->tax;
                        $email2 = $dt_invoice[0]->emailclient;
                        $name2 = $dt_invoice[0]->nameclient;
                        $phone2 = $dt_invoice[0]->phoneclient;
                        $amount = $dt_invoice[0]->totalprice;
                        $this->db->where('invoice',$invoice);
                        $this->db->update("tbl_invoice",array('status'=>'1','invdate'=>date('Y-m-d H:i:s'))); 
                }
                $this->Sales_model->sendemail($email2,$name2,$phone2,$amount2,$iteminvoice,$invoice,$namaclinic,$addressclinic,$cityclinic,$countryclinic,$invdate,$diskon,$tax);
                $this->Sales_model->sendemail($email,$namaclinic,$phone,$amount2,$iteminvoice,$invoice,$namaclinic,$addressclinic,$cityclinic,$countryclinic,$invdate,$diskon,$tax);

            	echo "OK";
            }
	    } else {
	        print_r("Cannot ".$_SERVER["REQUEST_METHOD"]." ".$_SERVER["SCRIPT_NAME"]);
	    }
	}

}