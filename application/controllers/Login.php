<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');
		
		// Load session library
		$this->load->library('session');
		$this->load->library('Mobile_Detect');
		// Load database
		$this->load->model('Login_model');
		$this->load->model('Setting_model');
    }
	
	public function index(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
			//$this->load->view('login_user');
				redirect ('dashboard');
			}else{
				// $this->load->view('header_login');
				$data = array(
					'title' => 'Login Dashboard - ivetdata.com',
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
				);
				$this->load->view('website/login_new_view',$data);
			}
		}else{
			redirect ('maintenance');
		}
		
	}

	public function owner(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			$detect = new Mobile_Detect;
			if (isset($this->session->userdata['logged_in'])) {
			//$this->load->view('login_user');
				// if ( $detect->isMobile() ) {
 			// 		die ("<script>window.location='https://".$_SERVER['HTTP_HOST']."/mobile;'</script>Redirecting ...");
				// }else{
					redirect ('dashboard');
				//}
			}else{
				// $this->load->view('header_login');
				
				
				
			//	if ( $detect->isMobile() ) {
 			//		die ("<script>window.location='https://".$_SERVER['HTTP_HOST']."/mobile;'</script>Redirecting ...");
			//	}else{
					$data = array(
						'title' => 'Login Pet Owner - ivetdata.com',
						'success' => $this->session->flashdata('success'),
						'error' => $this->session->flashdata('error'),
						'detect' => $detect,
					);
					//$this->load->view('website/header_new_login',$data);
					$this->load->view('website/loginowner_new_view',$data);
			//	}
                
			}
		}else{
			redirect ('maintenance');
		}
		
	}

	public function regvets(){
		$data = array(
			'datacountry' => $this->login_model->country(),
		);
		$this->load->view('website/header_new_login');
		$this->load->view('website/regvets_view',$data);
		$this->load->view('website/footer_new_view',$data);
	}

	public function regowner(){
		$data = array(
			'datacountry' => $this->login_model->country(),
		);
		$this->load->view('website/header_new_login');
		$this->load->view('regowner_view',$data);
		$this->load->view('website/footer_new_view',$data);
	}
	
    public function forgot_pass(){
		$data = array(
			'datacountry' => $this->Login_model->country(),
		);
		$this->load->view('website/forgotpass_new_view');
		//$this->load->view('regowner_view',$data);
	}

	public function notfound(){		
		$this->load->view('404');
	}

	public function register_process(){
		$name = $this->input->post('name');
		$clinic = $this->input->post('clinic');
		$country = $this->input->post('country');
		$city = $this->input->post('city');
		//$address = $this->input->post('address');
		$address = "";
		$user_email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('user_passnya');
		$cekemail = $this->login_model->read_user_information($user_email);
		$adaemail = count($this->login_model->adaemail($user_email));
		//var_dump($adaemail);
		if ( $adaemail == '1'){
			$uid = $cekemail[0]->uid;
		}else{
			//add to db
			$datamember = array(
				'name' => $name,
				'email' => $user_email,
				'phone' => $phone,
				'status' => '1',
				'role' => '2',
				'password' => md5($this->input->post('user_passnya')),
				'tgl_reg' => date('Y-m-d h:i:s'),
				'unikcode' => md5($user_email),
				'country' => $country,
				'city' => $city,				
			);
			$this->db->insert('tbl_member',$datamember);
			$uid = $this->db->insert_id();
		};
		$cekclinic =  count($this->login_model->adaclinic($uid));
		if($cekclinic == '0'){
			$dataclinic = array(
				'uid' => $uid,
				'nameclinic' => $clinic,
				'address' => $address,
				'phone' => $phone,
				'email' => $user_email,
				'city' => $city,
				'country' => $country,
			);
			$this->db->insert('tbl_clinic',$dataclinic);
			$idclinic= $this->db->insert_id();
		};
		//add vets doctor
		$insetvets = array(
			'uid' => $uid,
			'idclinic' => $idclinic,
			'namedoctor' => $name,
			'nohp' => $phone,
			'email' => $user_email,
			'country' => $country,
			'city' => $city,
		);
		$this->db->insert('tbl_dokter',$insetvets);
		$data = array(
			'success' => 'Please upload your license and national id.',
			'uid' => $uid,
		);
		$this->load->view('website/header_login');
		$this->load->view('website/vetsupload_view', $data);
		$this->load->view('website/footer_new_view',$data);
		 
	}

	public function upload(){
		$data = array(
			'uid' => '1',
		);
		$this->load->view('website/header_new_login');
		$this->load->view('website/vetsupload_view', $data);
		$this->load->view('website/footer_new_view',$data);
	}

	public function service(){

		$data = array(
			'uid' => '1',
			'dataservice' => $this->login_model->service(),
		);
		$this->load->view('website/header_new_login');
		$this->load->view('website/service_view', $data);
		$this->load->view('website/footer_new_view',$data);
	}


	// Check for user login process
	public function auth_proses() {	
		$this->form_validation->set_rules('user_email', 'Email', 'valid_email|required');
		$this->form_validation->set_rules('user_passnya', 'Password', 'required');
		$url =  $this->input->post('url');
		$detect = new Mobile_Detect;	
		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
				$this->load->view('admin_page');
			}else{
			$data = array(
				'error' => 'your email is not found.',
				'detect' => $detect,
			);
			//	$this->load->view('header_login');
				$this->load->view('login_new_view', $data);
			}
		} else {
			$email = $this->input->post('user_email');
			$passnya = md5($this->input->post('user_passnya'));

			$result = $this->login_model->ceklogin($email,$passnya);
			//var_dump($passnya);
			if ($result == TRUE) {	
				$user_email = $this->input->post('user_email');
				$result = $this->login_model->read_user_information($user_email);
				//var_dump($result)
				if ($result != false) {
				$session_data = array(
					'user_email' => $result[0]->email,
					'id' => $result[0]->uid,
					// 'tgltrial' => $result[0]->tgltrial,
					// 'tglend' => $result[0]->tglend,
					// 'user_status' => $result[0]->user_status,
					// 'rurl'=> current_url(),				
				);
				// $tgltrial = strtotime($result[0]->tgltrial);
				// $tglend = strtotime($result[0]->tglend);
				// $tglakhir = date('d M Y', $tglend);
				// $today = strtotime(date('Y-m-d'));
				$user_status = $result[0]->status;
				//$layanan = $result[0]->layanan;
				//$today = strtotime(date('2017-07-01'));                         
				//$akses = $tglend - $today;
				if ($user_status == '0'){
					//echo "Gak Bisa Akses";	
					$data = array(
						'error' => 'Email anda belum di validasi, silahkan kirimkan email anda ke support@temantrader.com',
						'detect' => $detect,
					);
					$this->load->view('website/login_new_view', $data);
				}
				if ($user_status == '1'){
					//if ($tglend > $today){
						//echo"bisa akses";
						// Add user data in session
						$this->session->set_userdata('logged_in', $session_data);
						$sesslogid = session_id();
						if($this->input->post('remember_me'))
						{
							$this->load->helper('cookie');
							$cookie = $this->input->cookie('ci_session'); // we get the cookie
							$this->input->set_cookie('ci_session', $cookie, '2592000'); // and add one day to it's expiration
						}
						if (isset($this->session->userdata['logged_in'])) {
						$client  = @$_SERVER['HTTP_CLIENT_IP'];
						$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
						$remote  = $_SERVER['REMOTE_ADDR'];
					
						if(filter_var($client, FILTER_VALIDATE_IP))
						{
							$ip = $client;
						}
						elseif(filter_var($forward, FILTER_VALIDATE_IP))
						{
							$ip = $forward;
						}
						else
						{
							$ip = $remote;
						}

						$dataupdate = array(
							'ip' => $ip,
							'sesslog' => $sesslogid,
							'last_login' => date('Y-m-d H:i:s'),
						);
						$this->db->where('email',$user_email);
						$this->db->update('tbl_member',$dataupdate);
						
						redirect('dashboard');
							//var_dump($user_status);
							//redirect($this->session->userdata('rurl'));
						}
					// }else{
					// 	//echo "Gak Bisa Akses";
					// 	$url = base_url();	
					// 	$data = array(
					// 		'error' => 'Akses Anda telah habis pada <br>'.$tglakhir.', silahkan lakukan pembelian di <a href="'.$url.'layanan">Disini</a>',
					// 	);
					// 	$this->load->view('welcome_message', $data);				
					// }
				}
				
				// Add user data in session
				//$this->session->set_userdata('logged_in', $session_data);
				//$this->load->view('admin_page');
			}
		}else {
		$data = array(
			'error' => 'Yuor email and password is wrong, you can reset your password <a role="button" data-toggle="collapse" href="#signin-resend-password" aria-expanded="false" aria-controls="signin-resend-password"> here </a> ',
			'detect' => $detect,
		);
		//$this->load->view('header_login');
		$this->load->view('website/login_new_view', $data);
		}
		}
	}
	public function htmlemail(){
		$this->load->view('website/html_email');
	}

	public function sendreset(){
		$user_email = $this->input->post('email');
		$email = $user_email;
		$cekemail = $this->Login_model->adaemail($user_email);
		if(count($cekemail) == '0'){
			$this->session->set_flashdata('error', 'Email not found.');
			redirect ($_SERVER['HTTP_REFERER']);
		}else{
			$nama = $cekemail[0]->name;
			$uid = $cekemail[0]->uid;
			$unikcode = $cekemail[0]->unikcode;
			//Sent email;
			$subjectnya = 'Reset your password in ivetdata.com';
			$this->load->library('email'); 
			// $query = $this->db->query("SELECT * FROM tbl_transaksi WHERE invoice='$invoice'");
			// $result = $query->result_array();
			// $member= $result[0]['member'];		
		   
			$subject = $subjectnya;	
			$message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>

 <tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">

 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="Trello" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>

 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$nama.', </h1>

 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333"> We heard you need a password reset. Click the link below and you will be redirected to a secure site from which you can set a new password. </p>

 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">

 <a href="'.base_url().'login/resetpass/?id='.$uid.'&token='.$unikcode.'&email='.$user_email.'&reset=true" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank""> Reset Password </a> </p>



 </td> </tr>

 </tbody> </table> </td> </tr></tbody> </table>';
			$text = $message;
	        //$email = $user_email;
	        //$this->load->view('testemail_view');
	        $this->load->library('EmailSender.php');
	        $this->emailsender->send($email,$subject,$text);
	        $this->session->set_flashdata('success', 'Reset password link already sent to your email.');
	        echo "<script>alert('Reset password link already sent to your email.');window.location='".$_SERVER['HTTP_REFERER']."';</script><h3>Please enable javascript</h3>";
			//redirect ($_SERVER['HTTP_REFERER']);
		}
	}

	public function resetpass(){
	   if(!isset($_GET['id'])||!isset($_GET['email'])||!isset($_GET['token'])){
	        die("<script>alert('Data not found');self.location.href='/'</script>");
	    }
		$uid = $this->input->get('id');
		$email = $this->input->get('email');
		$token = $this->input->get('token');
		$cekemail = $this->Login_model->cektoken($uid,$email,$token);
		$role = $cekemail[0]->role;
		if(count($cekemail) == '0'){
			$this->session->set_flashdata('error', 'Token reset password not found');
			redirect ('login');
		}else{
			// $this->load->view('header_login');
			$data = array(
				'title' => 'Reset password - ivetdata.com',
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
				'uid' => $uid,
				'email' => $email,
				'token' => $token,
				'role' => $role,
			);
			$this->load->view('website/header_new_view',$data);
			$this->load->view('website/resetpass_new_view',$data);
			$this->load->view('website/footer_new_view',$data);
		}
	}

	public function updatepass(){
		$password1 = $this->input->post('password1');
		$password2 = $this->input->post('password2');
		$uid = $this->input->post('uid');
		$email = $this->input->post('email');
		$token = $this->input->post('token');
		$clinic = $this->input->post('clinic');
		if($password1 != $password2){
			$this->session->set_flashdata('error', 'Password did not match');
			redirect ('login/resetpass/?id='.$uid.'&token='.$token.'&email='.$email.'&reset=true');
		}else{
			$dataupdate = array(
				'password' => md5($password1),
			);
			$this->db->where('uid',$uid);
			$this->db->update('tbl_member',$dataupdate);
			$this->session->set_flashdata('success', 'Reset password success, please login.');
			if($clinic == '0' ){
				redirect ('login/owner');
			}else{
				redirect ('login');
			};			
		}
	}
	
	// Logout from admin page
	public function logout() {	
		// Removing session data
		$sess_array = array(
		'user_email' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['success'] = 'Successfully Logout';
		//$this->load->view('header_login');
		$this->load->view('website/login_new_view', $data);
		//redirect ('Welcome');
	}


}
