<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');
		$this->load->model('Doctor_model');

    }
    
	public function add()
	{
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;	
			$dataclinic = $this->Home_model->dataclinic($profileid);								
			$data = array(
				'title' => 'Add Pet Doctor - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'role' => $role,
				'pettype' => $this->Pet_model->pettype(),
				'nameclinic' => $dataclinic[0]->nameclinic,
			);
			//$this->load->view('login_user');
			$this->load->view('header_view',$data);
			$this->load->view('adddoctor_view',$data);
			$this->load->view('footer_view',$data);
		}else{
			redirect ('welcome');
		}	
	}

	public function addprocess(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			//dataheader
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			//data pet
			$rfid = $this->input->post('rfid');
			$namepet = $this->input->post('namepet');
			$type = $this->input->post('type');
			$description = $this->input->post('description');
			$nameowner = $this->input->post('nameowner');
			$phone= $this->input->post('phone');
			$email = $this->input->post('email');			
			$address = $this->input->post('address');
			$dataclinic = $this->Home_model->dataclinic($profileid);
			$idclinic = $dataclinic[0]->idclinic;
			$country = $dataclinic[0]->country;
			$city = $dataclinic[0]->city;
			$customerclinic = $dataclinic[0]->customer;
			$petclinic = $dataclinic[0]->pet;
			//cek RFID
			$cekpetid = $this->Pet_model->cekpetid($rfid);
			$adapet = count($cekpetid);
			if($adapet == '0'){
				//saving to customer
				$cekcustomerclinic = $this->Pet_model->getcustomer($idclinic,$email);
				$adacustomerclinic = count($cekcustomerclinic);
				if($adacustomerclinic == '0'){
					$datacustomer = array(
						'nama' => $nameowner,
						'nohp' => $phone,
						'email' => $email,
						'idclinic' => $idclinic,
						'city' => $city,
						'country' => $country,
						'address' => $address,				
					);
					$this->db->insert('tbl_customer',$datacustomer);
					$idowner = $this->db->insert_id();
					$customerclinicfix = $customerclinic + 1;
				}else{
					$idowner = $cekcustomerclinic[0]->idcustomer;
					$customerclinicfix = $customerclinic + 0;
				};				

				$datapet = array(
					'rfid' => $rfid,
					'idowner' => $idowner,
					'tipe' => $type,
					'namapet' => $namepet,
					'city' => $city,
					'country' => $country,
					'address' => $address,
					'description' => $description,
					'input_by' => $profileid,
				);
				$this->db->insert('tbl_pet',$datapet);
				$idpet = $this->db->insert_id();
				$petclinicfix = $petclinic + 1;
				$petclinicadd = array(
					'pet' => $petclinicfix,
					'customer' => $customerclinicfix,
				);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_clinic',$petclinicadd);

				$data = array(
					'title' => 'Home - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'role' => $role,
					'success' => 'Add data pet success.',
					'nameclinic' => $dataclinic[0]->nameclinic,
					'idpet' => $idpet
				);
				//$this->load->view('login_user');
				$this->load->view('header_view',$data);
				$this->load->view('adddocphoto_view',$data);
				$this->load->view('footer_view',$data);
			}else{

				$data = array(
					'title' => 'Home - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'role' => $role,
					'error' => 'Pet ID has been used',
					'namepet' => $namepet,
					'city' => $city,
					'country' => $country,
					'address' => $address,
					'description' => $description,
					'nameowner' => $nameowner,
					'phone' => $phone,
					'email' => $email,
					'tipe' => $type,
					'pettype' => $this->Pet_model->pettype(),
					'nameclinic' => $dataclinic[0]->nameclinic,

				);
				//$this->load->view('login_user');
				$this->load->view('header_view',$data);
				$this->load->view('addpet_view',$data);
				$this->load->view('footer_view',$data);

			}

			
			
		}else{
			redirect ('welcome');
		}		
	}

	public function adddocphoto(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;	
			$data = array(
				'title' => 'Home - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'role' => $role,
				'idpet' => '1'
			);
			//$this->load->view('login_user');
			$this->load->view('header_view',$data);
			$this->load->view('addphoto_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			redirect ('welcome');
		}	
	}

	public function detail(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;	
			$data = array(
				'title' => 'Detail Pet - iVetdata.com',
				'nama' => $nama,
				'country' => $country,
				'city' => $city,
				'role' => $role,
				'idpet' => '1'
			);
			//$this->load->view('login_user');
			$this->load->view('header_view',$data);
			$this->load->view('detailpet_view',$data);
			//$this->load->view('footer_view',$data);
		}else{
			redirect ('welcome');
		}
	}
}