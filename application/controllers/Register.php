<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         //TEST SERVER
        //$params = array('server_key' => 'SB-Mid-server-KZa4C15_jfBKz1JDRH39cioy', 'production' => false);
        // Production server
        $params = array('server_key' => 'Mid-server-9SsIHhSfPMFHs2uWuxBCuTxB', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');
		$this->load->library('recaptcha');
		
		// Load session library
		$this->load->library('session');
		// Load database
		$this->load->model('login_model');
		$this->load->model('Home_model');
		$this->load->model('Setting_model');
    }
	
	public function index(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			if (isset($this->session->userdata['logged_in'])) {
			//$this->load->view('login_user');
				redirect ('dashboard');
			}else{
				$data = array(
					'datacountry' => $this->login_model->country(),
					'title' => 'Register Pet Owner - Ivetdata',
					'error' => $this->session->flashdata('error'),
					'page' => 'home',
				);
				//$this->load->view('website/header_new_view',$data);
				$this->load->view('website/regpetowner_new_view',$data);
				//$this->load->view('website/footer_new_view',$data);
			}
		}else{
			redirect ('maintenance');
		}

		
	}

	public function regvets(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			$data = array(
				'datacountry' => $this->login_model->country(),
				'title' => 'Register - Ivetdata',
				'error' => $this->session->flashdata('error'),
				'page' => 'home',
			);
			//$this->load->view('website/header_new_view',$data);
			$this->load->view('website/regvets_new_view',$data);
			//$this->load->view('website/footer_new_view');
		}else{
			redirect ('maintenance');
		}
		
	}

	public function regowner(){
		$data = array(
			'datacountry' => $this->login_model->country(),
		);
		$this->load->view('header_login');
		$this->load->view('regowner_view',$data);
	}


	public function notfound(){		
		$this->load->view('404');
	}

	public function register_process(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
		    
            			$password = $this->input->post('user_passnya');
            			$password2 = $this->input->post('user_passnya2');
            			if(strlen($password)<6){
            			    $this->session->set_flashdata('error', 'Error ! Password must be 6 character');
            			    die("<script>alert('Error ! Password must be minimal 6 character');self.location.href='/register/vets';</script><h1>Please enable javascript</h1>");
            			}
            			if($password!=$password2){
            			    $this->session->set_flashdata('error', 'Error ! Confirmation password not match');
            			    die("<script>alert('Error ! Confirmation password not match');self.location.href='/register/vets';</script><h1>Please enable javascript</h1>");
            			}

						$name = $this->input->post('name');
						$clinic = $this->input->post('clinic');
						$country = $this->input->post('country');
						$province = $this->input->post('state');
						$city = $this->input->post('city');
						$address = $this->input->post('address');
						//$address2 = $this->input->post('address2');
						$user_email = $this->input->post('email');
						$phone = $this->input->post('phone');
						//$phone2 = $this->input->post('phone2');
						$phone2 = "";
						$postalcode = $this->input->post('postalcode');
						$province = $this->input->post('province');
						//$password = $this->input->post('user_passnya');
						//$idlicense = $this->input->post('idlicense');
						$idlicense = "";
						$kta = $this->input->post('kta');						
						$sip = $this->input->post('sip');
						$recaptcha = $this->input->post('g-recaptcha-response');
				        if (!empty($recaptcha)) {
				            $response = $this->recaptcha->verifyResponse($recaptcha);
				            if (isset($response['success']) and $response['success'] === true) {
								$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
								$this->form_validation->set_rules('clinic', 'Clinic', 'trim|required|xss_clean');
								$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
								$this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean');
								$this->form_validation->set_rules('user_passnya', 'Password', 'trim|required');
								$this->form_validation->set_rules('kta', 'KTA', 'trim|required');
								$client  = @$_SERVER['HTTP_CLIENT_IP'];
								$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
								$remote  = $_SERVER['REMOTE_ADDR'];
								if(filter_var($client, FILTER_VALIDATE_IP)){
									$ip = $client;
								}elseif(filter_var($forward, FILTER_VALIDATE_IP)){
									$ip = $forward;
								}else{
									$ip = $remote;
								}
								if ($this->form_validation->run() == FALSE){
									$data = array(
										//'uid' => $uid,	
										'error' => 'Sorry, Mandatory field must be filled.',
										'title' => 'Register Vets',	
										'name' => $name,
										'clinic' => $clinic,
										'country' => $country,
										'province' => $province,
										'city' => $city,
										'idlicense' => $idlicense, 
										'address' => $address,
										'user_email' => $user_email,
										'phone' => $phone,
										'postalcode' => $postalcode,
										'kta' => $kta,
										'sip' => $sip,
										'datacountry' => $this->login_model->country(),
										//'datacountry' => $this->login_model->country(),					
									);
									//$this->load->view('website/header_view',$data);
									$this->load->view('website/regvets_new_view',$data);
									//$this->load->view('website/footer_view');
								}else{		
									$cekemail = $this->login_model->read_user_information($user_email);
									$adaemail = count($this->login_model->adaemail($user_email));
									//var_dump($adaemail);
									if ( $adaemail > '0'){
										$data = array(
										//'uid' => $uid,	
											'error' => 'Sorry, email '.$user_email.' has been used.',
											'title' => 'Register Vets',	
											'name' => $name,
											'clinic' => $clinic,
											'country' => $country,
											'province' => $province,
											'city' => $city,
											'idlicense' => $idlicense, 
											'address' => $address,
											'address2' => $address,
											'user_email' => $user_email,
											'phone' => $phone,
											'phone2' => $phone2,
											'postalcode' => $postalcode,
											'kta' => $kta,
											'sip' => $sip,
											'datacountry' => $this->login_model->country(),
											//'datacountry' => $this->login_model->country(),					
										);
										//$this->load->view('website/header_view',$data);
										$this->load->view('website/regvets_new_view',$data);
										//$this->load->view('website/footer_view');
									}else{
										 //$cekkta = $this->login_model->cekkta($kta);
										 //$adakta = count($cekkta);
										$curl = curl_init();
										curl_setopt_array($curl, array(
										  CURLOPT_URL => "https://link.pdhi.or.id/master/cekkta/?kta=".$kta,
										  CURLOPT_RETURNTRANSFER => true,
										  CURLOPT_ENCODING => "",
										  CURLOPT_MAXREDIRS => 10,
										  CURLOPT_TIMEOUT => 30,
										  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
										  CURLOPT_CUSTOMREQUEST => "GET",
										  CURLOPT_HTTPHEADER => array(
										    "Accept: */*",
										    "Cache-Control: no-cache",
										    "Connection: keep-alive",
										    "Host: link.pdhi.or.id",
										    "Postman-Token: b072e374-a954-4880-aabc-a6def5c9f6ab,a039fbc6-a961-4ef1-b95f-fb04a89ef2fb",
										    "User-Agent: PostmanRuntime/7.15.0",
										    "accept-encoding: gzip, deflate",
										    "cache-control: no-cache",
										    "cookie: ci_session=dab4086a85a002e149f7e4dcf22268cb9c54f2f3"
										  ),
										));

										$response = curl_exec($curl);
										$err = curl_error($curl);

										curl_close($curl);

										if ($err) {
										  //echo "cURL Error #:" . $err;
											$data = array(
										 	//'uid' => $uid,	
										 		'error' => 'Sorry, Terjadi gangguan pengecekan KTA',
										 		'title' => 'Register Vets',	
										 		'name' => $name,
										 		'clinic' => $clinic,
										 		'country' => $country,
										 		'province' => $province,
										 		'city' => $city,
										 		'idlicense' => $idlicense, 
										 		'address' => $address,
										 		'user_email' => $user_email,
										 		'phone' => $phone,
										 		'postalcode' => $postalcode,
										 		'kta' => $kta,
										 		'sip' => $sip,
										 		'datacountry' => $this->login_model->country(),
										 		//'datacountry' => $this->login_model->country(),					
										 	);
										 	//$this->load->view('website/header_view',$data);
										 	$this->load->view('website/regvets_new_view',$data);
										} else {
										  echo $response;
										}

										 $adakta = $response;
										 if($adakta == '"0"'){
										 	$data = array(
										 	//'uid' => $uid,	
										 		'error' => 'Sorry, KTA PDHI '.$kta.' is INVALID',
										 		'title' => 'Register Vets',	
										 		'name' => $name,
										 		'clinic' => $clinic,
										 		'country' => $country,
										 		'province' => $province,
										 		'city' => $city,
										 		'idlicense' => $idlicense, 
										 		'address' => $address,
										 		'user_email' => $user_email,
										 		'phone' => $phone,
										 		'postalcode' => $postalcode,
										 		'kta' => $kta,
										 		'sip' => $sip,
										 		'datacountry' => $this->login_model->country(),
										 		//'datacountry' => $this->login_model->country(),					
										 	);
										 	//$this->load->view('website/header_view',$data);
										 	$this->load->view('website/regvets_new_view',$data);
										 }else{
											 //upload vetktp
											 $config['upload_path']   = './upload/vets/license'; 
											 $config['allowed_types'] = 'jpg|png|jpeg|gif'; 
											 $config['max_size']      = 38500; 
											 $config['max_width']     = 1800; 
											 $config['max_height']    = 1500;
											 $config['encrypt_name'] = TRUE;
											 $config['quality'] = 50;  
											 $this->load->library('upload', $config);
											 if (!$this->upload->do_upload('licensepic')) {				
											 	$data = array(
											 		//'uid' => $uid,	
											 		'error' => $this->upload->display_errors(),
											 		'title' => 'Register Vets',	
											 		'name' => $name,
											 		'clinic' => $clinic,
											 		'country' => $country,
											 		'city' => $city,
											 		'idlicense' => $idlicense, 
											 		'address' => $address,
											 		'user_email' => $user_email,
											 		'phone' => $phone,
											 		'postalcode' => $postalcode,
											 		'datacountry' => $this->login_model->country(),					
											 	);
											// 	$this->load->view('website/header_view',$data);
											// 	$this->load->view('website/regvets_view',$data);
											// 	$this->load->view('website/footer_view');				
											 	// $this->session->set_flashdata('success', 'Registration is succes, please login.');
											 	 //redirect ('login');
											 }else{
											    	$result1 = $this->upload->data();
											    	//var_dump($result1);
											    	$license = $result1['file_name'];
											    	//upload to aws
											 	$keyname = $license;
											 	$getpng = FCPATH."upload/vets/license/".$keyname;
											 	$this->load->library('aws3');
											 	$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
											 	$data = array('upload_data' => $image_data['file_name']);
											 	$urllicense= $data['upload_data'];
											 }
												//add to db
											    $datamember = array(
												'name' => $name,
												'address' => $address,
												'email' => $user_email,
												'phone' => $phone2,
												'status' => '0',
												'role' => '1',
												'manageto' => '1',
												'password' => md5($this->input->post('user_passnya')),
												//'realpass' => $this->input->post('user_passnya'),
												'tgl_reg' => date('Y-m-d h:i:s'),
												'unikcode' => md5($user_email),
												'country' => $country,
												'city' => $city,
												'expdate' => date('Y-m-d',strtotime('+1 month')),
												'ip' => $ip,
											    );
											$this->db->insert('tbl_member',$datamember);
											$uid = $this->db->insert_id();
											$cekclinic =  count($this->login_model->adaclinic($uid));
											if($cekclinic == '0'){
												$dataclinic = array(
													'uid' => $uid,
													'nameclinic' => $clinic,
													'address' => $address,
													'phone' => $phone2,
													'email' => $user_email,
													'province' => $province,
													'city' => $city,
													'country' => $country,
													'postalcode' => $postalcode,
												);
												$this->db->insert('tbl_clinic',$dataclinic);
												$idclinic= $this->db->insert_id();
											}else{
												$idclinic = $cekclinic[0]->idclinic;
											}
											//add vets doctor
											$insetvets = array(
												'uid' => $uid,
												'idclinic' => $idclinic,
												'namedoctor' => $name,
												'nohp' => $phone,
												'email' => $user_email,
												'country' => $country,
												'city' => $city,
												//'license' => $urllicense,
												'idlicense' => $idlicense,
												'kta' => $kta,
												'sip' => $sip,
											);
											$this->db->insert('tbl_dokter',$insetvets);
											//update idclinic
											$updateidclinic = array(
												'idclinic' => $idclinic,
											);
											$this->db->where('uid',$uid);
											$this->db->update('tbl_member',$updateidclinic);
											$subjectnya = 'Confirm your email account | iVetdata.com';
											$this->load->library('email'); 	
										   	$token = md5($user_email);
										   	$email = $user_email;
											$subject = $subjectnya;	
											$message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
													 <tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
													 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
													 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$name.', </h1>
													 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">Thank you for registering at iVetData.com. Please confirm your email by clicking the confirm button below. </p>
													 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">
													 <a href="'.base_url().'register/confirm/?id='.$uid.'&token='.$token.'&email='.$user_email.'&confirm=true" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank""> Confirm </a> </p>
													 </td> </tr>
													 </tbody> </table> </td> </tr></tbody> </table>';
											$text = $message;
											//$email = $user_email;
										    //$this->load->view('testemail_view');
										    $this->load->library('EmailSender.php');
										    $this->emailsender->send($email,$subject,$text);
											// $data = array(
											// 	'uid' => $uid,
											// 	'dataservice' => $this->login_model->service(),
											// 	'title' => 'Select subscription - Ivetdata.com',		
											// );
											// $this->load->view('website/header_view',$data);
											// $this->load->view('website/service_view',$data);
											// $this->load->view('website/footer_view');
											$this->session->set_flashdata('success', 'Registration Success. Please check your email.');
											redirect ('login');
											//var_dump($adakta);
										}
									}
								}
							}else{
								$data = array(
										//'uid' => $uid,	
										'error' => 'Sorry, Recahapta must be checked.',
										'title' => 'Register Vets',	
										'name' => $name,
										'clinic' => $clinic,
										'country' => $country,
										'province' => $province,
										'city' => $city,
										'idlicense' => $idlicense, 
										'address' => $address,
										'address2' => $address,
										'user_email' => $user_email,
										'phone' => $phone,
										'phone2' => $phone2,
										'postalcode' => $postalcode,
										'kta' => $kta,
										'sip' => $sip,
										'datacountry' => $this->login_model->country(),
										//'datacountry' => $this->login_model->country(),					
									);
									$this->load->view('website/header_view',$data);
									$this->load->view('website/regvets_view',$data);
				        			
				        	//redirect('welcome');
				        	}		        	
				        }else{
				        	$data = array(
							//'uid' => $uid,	
										'error' => 'Sorry, Recahapta must be checked.',
										'title' => 'Register Vets',	
										'name' => $name,
										'clinic' => $clinic,
										'country' => $country,
										'province' => $province,
										'city' => $city,
										'idlicense' => $idlicense, 
										'address' => $address,
										'address2' => $address,
										'user_email' => $user_email,
										'phone' => $phone,
										'phone2' => $phone2,
										'postalcode' => $postalcode,
										'kta' => $kta,
										'sip' => $sip,
										'datacountry' => $this->login_model->country(),
										//'datacountry' => $this->login_model->country(),					
									);
									$this->load->view('website/header_view',$data);
									$this->load->view('website/regvets_view',$data);
				        }			
			}else{
				redirect ('maintenance');
			}
		// $data = array(
		// 	'success' => 'Please upload your license or national id.',
		// 	'uid' => $uid,
		// 	'title' => 'Upload VET License',	
		// );
		// $this->load->view('website/header_view',$data);
		// $this->load->view('website/vetsupload_view',$data);
		// $this->load->view('website/footer_view');
		 
	}


	public function registerpetowner_process(){
		$datamaintenance = $this->Setting_model->maintenance();
		$maintenance = $datamaintenance[0]->value;
		if($maintenance == '0'){
			$name = $this->input->post('name');
			//$clinic = $this->input->post('clinic');
			//$country = $this->input->post('country');
			//$province = $this->input->post('state');
			//$city = $this->input->post('city');
			$country = "";
			$province = "";
			$city = "";
			$postalcode = "";
			//$address = $this->input->post('address');
			$address = "";
			$user_email = $this->input->post('email');
			//$phone = $this->input->post('phone');
			$phone = "";
			//$postalcode = $this->input->post('postalcode');
			//$province = $this->input->post('province');
			$password = $this->input->post('user_passnya');
			$password2 = $this->input->post('user_passnya2');
			if(strlen($password)<6){
			    $this->session->set_flashdata('error', 'Error ! Password must be 6 character');
			    die("<script>alert('Error ! Password must be 6 character');self.location.href='/register';</script><h1>Please enable javascript</h1>");
			}
			if($password!=$password2){
			    $this->session->set_flashdata('error', 'Error ! Confirmation password does not match');
			    die("<script>alert('Error ! Confirmation password not match');self.location.href='/register';</script><h1>Please enable javascript</h1>");
			}
			// $idlicense = $this->input->post('idlicense');
			// $kta = $this->input->post('kta');
			// $sip = $this->input->post('sip');
			$recaptcha = $this->input->post('g-recaptcha-response');
		    if (!empty($recaptcha)) {
		            $response = $this->recaptcha->verifyResponse($recaptcha);
		            if (isset($response['success']) and $response['success'] === true) {
						$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
						//$this->form_validation->set_rules('clinic', 'Clinic', 'trim|required|xss_clean');
						$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
						//$this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean');
						$this->form_validation->set_rules('user_passnya', 'Password', 'trim|required');
						//$this->form_validation->set_rules('kta', 'KTA', 'trim|required');
						$client  = @$_SERVER['HTTP_CLIENT_IP'];
						$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
						$remote  = $_SERVER['REMOTE_ADDR'];
						if(filter_var($client, FILTER_VALIDATE_IP)){
							$ip = $client;
						}elseif(filter_var($forward, FILTER_VALIDATE_IP)){
							$ip = $forward;
						}else{
							$ip = $remote;
						}
						if ($this->form_validation->run() == FALSE){
							$data = array(
								//'uid' => $uid,	
								'error' => 'Sorry, Mandatory field must be filled.',
								'title' => 'Register Vets',	
								'name' => $name,
								//'clinic' => $clinic,
								'country' => $country,
								'province' => $province,
								//'city' => $city,
								'idlicense' => $idlicense, 
								'address' => $address,
								'user_email' => $user_email,
								'phone' => $phone,
								'postalcode' => $postalcode,
								//'kta' => $kta,
								//'sip' => $sip,
								'datacountry' => $this->login_model->country(),
								//'datacountry' => $this->login_model->country(),					
							);
							//$this->load->view('website/header_view',$data);
							$this->load->view('website/regpetowner_new_view',$data);
							//$this->load->view('website/footer_view');
						}else{		
							$cekemail = $this->login_model->read_user_information($user_email);
							$adaemail = count($this->login_model->adaemail($user_email));
							//var_dump($adaemail);
							if ( $adaemail > '0'){
								$data = array(
								//'uid' => $uid,	
									'error' => 'Sorry, email '.$user_email.' has been used. Please choose another email address.',
									'title' => 'Register Pet Owner',	
									'name' => $name,
									//'clinic' => $clinic,
									'country' => $country,
									'province' => $province,
									'city' => $city,
									//'idlicense' => $idlicense, 
									'address' => $address,
									//'user_email' => $user_email,
									'phone' => $phone,
									'postalcode' => $postalcode,
									//'kta' => $kta,
									//'sip' => $sip,
									'datacountry' => $this->login_model->country(),
									//'datacountry' => $this->login_model->country(),					
								);
								//$this->load->view('website/header_view',$data);
								$this->load->view('website/regpetowner_new_view',$data);
								//$this->load->view('website/footer_view');
							}else{
								// //upload vetktp
								// $config['upload_path']   = './upload/vets/license'; 
								// $config['allowed_types'] = 'jpg|png|jpeg|gif'; 
								// $config['max_size']      = 38500; 
								// // $config['max_width']     = 1800; 
								// // $config['max_height']    = 1500;
								// $config['encrypt_name'] = TRUE;
								// $config['quality'] = 50;  
								// $this->load->library('upload', $config);
								// if (!$this->upload->do_upload('licensepic')) {				
								// 	$data = array(
								// 		//'uid' => $uid,	
								// 		'error' => $this->upload->display_errors(),
								// 		'title' => 'Register Vets',	
								// 		'name' => $name,
								// 		'clinic' => $clinic,
								// 		'country' => $country,
								// 		'city' => $city,
								// 		'idlicense' => $idlicense, 
								// 		'address' => $address,
								// 		'user_email' => $user_email,
								// 		'phone' => $phone,
								// 		'postalcode' => $postalcode,
								// 		'datacountry' => $this->login_model->country(),					
								// 	);
								// 	$this->load->view('website/header_view',$data);
								// 	$this->load->view('website/regvets_view',$data);
								// 	$this->load->view('website/footer_view');				
								// 	// $this->session->set_flashdata('success', 'Registration is succes, please login.');
								// 	// redirect ('login');
								// }else{
								//    	$result1 = $this->upload->data();
								//    	//var_dump($result1);
								//    	$license = $result1['file_name'];
								//    	//upload to aws
								// 	$keyname = $license;
								// 	$getpng = FCPATH."upload/vets/license/".$keyname;
								// 	$this->load->library('aws3');
								// 	$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
								// 	$data = array('upload_data' => $image_data['file_name']);
								// 	$urllicense= $data['upload_data'];
									//add to db
								$datamember = array(
									'name' => $name,
									'email' => $user_email,
									'phone' => $phone,
									'status' => '0',
									'role' => '0',
									'manageto' => '0',
									'password' => md5($this->input->post('user_passnya')),
									//'realpass' => $this->input->post('user_passnya'),
									'tgl_reg' => date('Y-m-d h:i:s'),
									'unikcode' => md5($user_email),
									'country' => $country,
									'city' => $city,
									'expdate' => date('Y-m-d',strtotime('+1 month')),
									'ip' => $ip,
								);
								$this->db->insert('tbl_member',$datamember);
								$uid = $this->db->insert_id();
								// $cekclinic =  count($this->login_model->adaclinic($uid));
								// if($cekclinic == '0'){
								// 	$dataclinic = array(
								// 		'uid' => $uid,
								// 		'nameclinic' => $clinic,
								// 		'address' => $address,
								// 		'phone' => $phone,
								// 		'email' => $user_email,
								// 		'province' => $province,
								// 		'city' => $city,
								// 		'country' => $country,
								// 		'postalcode' => $postalcode,
								// 	);
								// 	$this->db->insert('tbl_clinic',$dataclinic);
								// 	$idclinic= $this->db->insert_id();
								// }else{
								// 	$idclinic = $cekclinic[0]->idclinic;
								// };
								// //add vets doctor
								// $insetvets = array(
								// 	'uid' => $uid,
								// 	'idclinic' => $idclinic,
								// 	'namedoctor' => $name,
								// 	'nohp' => $phone,
								// 	'email' => $user_email,
								// 	'country' => $country,
								// 	'city' => $city,
								// 	//'license' => $urllicense,
								// 	'idlicense' => $idlicense,
								// 	'kta' => $kta,
								// 	'sip' => $sip,
								// );
								// $this->db->insert('tbl_dokter',$insetvets);
								// //update idclinic
								// $updateidclinic = array(
								// 	'idclinic' => $idclinic,
								// );
								// $this->db->where('uid',$uid);
								// $this->db->update('tbl_member',$updateidclinic);
								$subjectnya = 'Confirm your email account | iVetdata.com';
								$this->load->library('email'); 
								// $query = $this->db->query("SELECT * FROM tbl_transaksi WHERE invoice='$invoice'");
								// $result = $query->result_array();
								// $member= $result[0]['member'];		
							   	$token = md5($user_email);
							   	$email = $user_email;
								$subject = $subjectnya;	
								$message = '<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px Helvetica Neue,Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#f0f0f0;font:15px Helvetica Neue,Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody>
										 <tr width="100%" height="57"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#222;padding:12px 18px;text-align:center">
										 <img height="70" src="https://ivetdata.com/assets/images/logo-white.png" title="iVetdata" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr>
										 <tr width="100%"> <td valign="top" align="left" style="border-bottom-left-radius:4px;border-bottom-right-radius:4px;background:#fff;padding:18px"> <h1 style="font-size:20px;margin:0;color:#333"> Hello '.$name.', </h1>
										 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;color:#333">Thank you for registering at iVetData.com. Please confirm your email by clicking the confirm button below. </p>
										 <p style="font:15px/1.25em Helvetica Neue,Arial,Helvetica;margin-bottom:0;text-align:center;color:#333">
										 <a href="'.base_url().'register/confirm/?id='.$uid.'&token='.$token.'&email='.$user_email.'&confirm=true" style="border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 24px;padding:10px 18px;text-decoration:none;width:180px;text-align:center" target="_blank""> Confirm </a> </p>
										 </td> </tr>
										 </tbody> </table> </td> </tr></tbody> </table>';
								$text = $message;
								//$email = $user_email;
							    //$this->load->view('testemail_view');
							    $this->load->library('EmailSender.php');
							    $this->emailsender->send($email,$subject,$text);
								// $data = array(
								// 	'uid' => $uid,
								// 	'dataservice' => $this->login_model->service(),
								// 	'title' => 'Select subscription - Ivetdata.com',		
								// );
								// $this->load->view('website/header_view',$data);
								// $this->load->view('website/service_view',$data);
								// $this->load->view('website/footer_view');
								$this->session->set_flashdata('success', 'Registration is successful. Please check your email');
								redirect ('/login/owner');
							}
						}
					}else{
						$data = array(
								//'uid' => $uid,	
								'error' => 'Sorry, Recahapta must be checked.',
								'title' => 'Register Vets',	
								'name' => $name,
								//'clinic' => $clinic,
								'country' => $country,
								'province' => $province,
								'city' => $city,
								//'idlicense' => $idlicense, 
								'address' => $address,
								'user_email' => $user_email,
								'phone' => $phone,
								'postalcode' => $postalcode,
								// 'kta' => $kta,
								// 'sip' => $sip,
								'datacountry' => $this->login_model->country(),
								//'datacountry' => $this->login_model->country(),					
							);
							//$this->load->view('website/header_view',$data);
							$this->load->view('website/regpetowner_new_view',$data);
		        			
		        	//redirect('welcome');
		        	}		        	
		        }else{
		        	$data = array(
					//'uid' => $uid,	
								'error' => 'Sorry, Recahapta must be checked.',
								'title' => 'Register Vets',	
								'name' => $name,
								//'clinic' => $clinic,
								'country' => $country,
								'province' => $province,
								'city' => $city,
								//'idlicense' => $idlicense, 
								'address' => $address,
								'user_email' => $user_email,
								'phone' => $phone,
								'postalcode' => $postalcode,
								//'kta' => $kta,
								//'sip' => $sip,
								'datacountry' => $this->login_model->country(),
								//'datacountry' => $this->login_model->country(),					
							);
							//$this->load->view('website/header_view',$data);
							$this->load->view('website/regpetowner_new_view',$data);
		        }			
			}else{
				redirect ('maintenance');
			}
		// $data = array(
		// 	'success' => 'Please upload your license or national id.',
		// 	'uid' => $uid,
		// 	'title' => 'Upload VET License',	
		// );
		// $this->load->view('website/header_view',$data);
		// $this->load->view('website/vetsupload_view',$data);
		// $this->load->view('website/footer_view');
		 
	}

	public function uploadid_process(){
		$config['upload_path']   = './upload/vets/license'; 
		$config['allowed_types'] = 'jpg|png|jpeg|gif'; 
		$config['max_size']      = 5500; 
		// $config['max_width']     = 1800; 
		// $config['max_height']    = 1500;
		$config['encrypt_name'] = TRUE;
		$config['quality'] = 50;  
		$this->load->library('upload', $config);
		$uid = $this->input->post('uid');
		if (!$this->upload->do_upload('licensepic')) {
			$data = array(
				'uid' => $uid,	
				'error' => $this->upload->display_errors(),
				'title' => 'Upload VET License',	
			);
			$this->load->view('website/header_view',$data);
			$this->load->view('website/vetsupload_view',$data);
			$this->load->view('website/footer_view');
			// $this->session->set_flashdata('success', 'Registration is succes, please login.');
			// redirect ('login');
		}else{
		   	$result1 = $this->upload->data();
		   	$license = $result1['file_name'];
		   	$idlicense = $this->input->post('idlicense');
		   	$dataupdate = array(
		   		'license' => $license,
		   		'idlicense' => $idlicense
		   	);
		   	$this->db->where('uid',$uid);
		   	$this->db->update('tbl_dokter',$dataupdate);
		   	//var_dump($result1);
		   	$data = array(
				'uid' => $uid,
				'dataservice' => $this->login_model->service(),
				'title' => 'Select subscription - Ivetdata.com',		
			);
			$this->load->view('website/header_view',$data);
			$this->load->view('website/service_view',$data);
			$this->load->view('website/footer_view');
		   	
		};
	}

	public function service(){
		$uid = $this->input->post('uid');
		$profileid = $uid;
		$datauid = $this->Home_model->detailprofile($profileid);
		$fullname = $datauid[0]->name;
		$email = $datauid[0]->email;
		$dataclinic = $this->Home_model->dataclinic($profileid);
		$address = $dataclinic[0]->address;
		$city = $dataclinic[0]->city;
		$postalcode = $dataclinic[0]->postalcode;
		$phone = $dataclinic[0]->phone;
		$country = 'IDN';

		$idservice = $this->input->post('service');
		$service = $this->login_model->dataservice($idservice);
		//var_dump($dataservice);
		$namaservice = $service[0]->name;
		$price = $service[0]->price;
		if($price == '0'){
			$this->session->set_flashdata('success', 'Registration is succes, please login.');
			redirect ('login');
		}else{
			$digits = 3;
			$unik = rand(pow(10, $digits-1), pow(10, $digits)-1);
			$totalprice = $price + $unik;
			$datauid = $this->login_model->cekuid($uid);
			$name = $datauid[0]->name;
			$invoice = 'IVET'.$uid.$unik;
			$datatrx = array(
				'uid' => $uid,
				//'service' => $service,
				'nama' => $name,
				'invoice' => $invoice,
				'tipe' => 'subscription',
				'tglbeli' => date('Y-m-d H:i:s'),
				'jumlah' => $totalprice,
				'description' => 'Pembelian '.$namaservice,
				'idservice' => $idservice,
			);
			$this->db->insert('tbl_transaksi',$datatrx);

			$transaction_details = array(
				'order_id' 			=> $invoice,
				'gross_amount' 	=> $totalprice,
			);

			// Populate items
			$items = [
				array(
					'id' 				=> $idservice,
					'price' 		=> $totalprice,
					'quantity' 	=> 1,
					'name' 			=> $namaservice
				)
			];

			// Populate customer's billing address
			$billing_address = array(
				'first_name' 		=> $fullname,
				//'last_name' 		=> "Gufron",
				'address' 			=> $address,
				'city' 					=> $city,
				'postal_code' 	=> $postalcode,
				'phone' 				=> $phone,
				'country_code'	=> $country,
				);

			// Populate customer's shipping address
			$shipping_address = array(
				'first_name' 	=> $fullname,
				//'last_name' 	=> "Watson",
				'address' 		=> $address,
				'city' 				=> $city,
				'postal_code' => $postalcode,
				'phone' 			=> $phone,
				'country_code'=> $country,
				);

			// Populate customer's Info
			$customer_details = array(
				'first_name' 		=> $fullname,
				//'last_name' 		=> "Gufron",
				'email' 					=> $email,
				'phone' 					=> $phone,
				'billing_address' => $billing_address,
				'shipping_address'=> $shipping_address
				);

			// Data yang akan dikirim untuk request redirect_url.
			// Uncomment 'credit_card_3d_secure' => true jika transaksi ingin diproses dengan 3DSecure.
			$transaction_data = array(
				'payment_type' 			=> 'vtweb', 
				'vtweb' 						=> array(
					//'enabled_payments' 	=> ['credit_card'],
					'credit_card_3d_secure' => true
				),
				'transaction_details'=> $transaction_details,
				'item_details' 			 => $items,
				'customer_details' 	 => $customer_details
			);
		
			try
			{
				$vtweb_url = $this->veritrans->vtweb_charge($transaction_data);
				header('Location: ' . $vtweb_url);
			} 
			catch (Exception $e) 
			{
	    		echo $e->getMessage();	
			}
		}
		



		// $data = array(
		// 	'invoice' => $invoice,
		// 	'price' => $price,
		// 	'unik' => $unik,
		// 	'totalprice' => $totalprice,
		// 	'item' => 'Membership '.$namaservice,
		// 	'buyer' => $name,
		// 	'name' => $name,
		// 	'uid' => $uid,
		// 	'title' => 'Pay Now - Ivetdata.com'
		// );
		// $this->load->view('website/header_view',$data);
		// $this->load->view('website/invoice_view',$data);
		// $this->load->view('website/footer_view');
		// $this->load->view('header_login');
		// $this->load->view('invoice_view', $data);
		//var_dump($datatrx);
		//insert db transaction
		//$

	}	

	public function notification(){
		echo 'test notification handler';
		$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);

		if($result){
		$notif = $this->veritrans->status($result->order_id);
		}

		error_log(print_r($result,TRUE));

		//notification handler sample

		$transaction = $notif->transaction_status;
		$type = $notif->payment_type;
		$order_id = $notif->order_id;
		$fraud = $notif->fraud_status;

		if ($transaction == 'capture') {
		  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
		  if ($type == 'credit_card'){
		    if($fraud == 'challenge'){
		      // TODO set payment status in merchant's database to 'Challenge by FDS'
		      // TODO merchant should decide whether this transaction is authorized or not in MAP
		      echo "Transaction order_id: " . $order_id ." is challenged by FDS";
		      } 
		      else {
			      // TODO set payment status in merchant's database to 'Success'
			      echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
		      }
		    }
		  }
		else if ($transaction == 'settlement'){
			  // TODO set payment status in merchant's database to 'Settlement'
			  //echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
			$dataorder = $this->login_model->cektrx($order_id);
			$uid = $dataorder[0]->uid;
			$nama = $dataorder[0]->nama;
			$idservice = $dataorder[0]->idservice;
			$dataservice = $this->Home_model->idservice($idservice);
			$bulanservice = $dataservice[0]->month;	
			$team = $dataservice[0]->team;
			$chip = $dataservice[0]->chip;		
			$profileid = $uid;
			$datauid = $this->Home_model->detailprofile($profileid);
			$idclinic = $datauid[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$currentchip = $dataclinic[0]->chip;
			//$fullname = $datauid[0]->name;
			$email = $datauid[0]->email;
			$dataupdate = array(
				'tglapprove' => date('Y-m-d H:i:s'),
				'status' => '1',
			);
			$this->db->where('invoice',$order_id);
			$this->db->update('tbl_transaksi',$dataupdate);

			//updateuid
			$expdate = date('Y-m-d',strtotime('+'.$bulanservice.' month'));
			$dataupdateuid = array(
				'status' => '1',
				'role' => '1',
				'expdate' => $expdate,
				//'team' => $team,				
			);
			$this->db->where('uid',$uid);
			$this->db->update('tbl_member',$dataupdateuid);

			//updateclinic
			$chipupdate = $chip + $currentchip;
			$updateclinic = array(
				'team' => $team,
				'chip' => $chipupdate,
			);
			$this->db->where('idclinic',$idclinic);
			$this->db->update('tbl_clinic',$updateclinic);

			//Sent email;
			$subjectnya = 'Congratulation '.$nama.', your account in ivetdata.com is activated';
			$this->load->library('email'); 
			// $query = $this->db->query("SELECT * FROM tbl_transaksi WHERE invoice='$invoice'");
			// $result = $query->result_array();
			// $member= $result[0]['member'];		
		   
			$subject = $subjectnya;	
			$message = 'Dear '.$nama.',<br /><br /> 
						Congratulation, your payment method using '.$type.' is success. And your account in ivetdata.com is active now, Please Login<br><br>
						<a href="'.base_url().'login" style="color:#fff;background-color:#222;padding:10px;margin:20px 0px;text-decoration:none;font-size:25px">'.base_url().'login</a><br><br>
						Regards,
						';
			$text = $message;
	        //$email = $user_email;
	        //$this->load->view('testemail_view');
	        $this->load->library('EmailSender.php');
	        $this->emailsender->send($email,$subject,$text);

		  } 
		  else if($transaction == 'pending'){
		  // TODO set payment status in merchant's database to 'Pending'
		  echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
		  } 
		  else if ($transaction == 'deny') {
		  // TODO set payment status in merchant's database to 'Denied'
		  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
		}

	}


	// public function thanks(){
	// 	//echo 'test notification handler';
	// 	$json_result = file_get_contents('php://input');
	// 	$result = json_decode($json_result);

	// 	if($result){
	// 	$notif = $this->veritrans->status($result->order_id);
	// 	}

	// 	error_log(print_r($result,TRUE));

	// 	//notification handler sample

	// 	$transaction = $notif->transaction_status;
	// 	$type = $notif->payment_type;
	// 	$order_id = $notif->order_id;
	// 	$fraud = $notif->fraud_status;

	// 	if ($transaction == 'capture') {
	// 	  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
	// 	  if ($type == 'credit_card'){
	// 	    if($fraud == 'challenge'){
	// 	      // TODO set payment status in merchant's database to 'Challenge by FDS'
	// 	      // TODO merchant should decide whether this transaction is authorized or not in MAP
	// 	      echo "Transaction order_id: " . $order_id ." is challenged by FDS";
	// 	      } 
	// 	      else {
	// 		      // TODO set payment status in merchant's database to 'Success'
	// 		      echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
	// 	      }
	// 	    }
	// 	  }
	// 	else if ($transaction == 'settlement'){
	// 		  // TODO set payment status in merchant's database to 'Settlement'
	// 		  //echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
	// 		$dataorder = $this->login_model->cektrx($order_id);
	// 		$uid = $dataorder[0]->uid;
	// 		$nama = $dataorder[0]->nama;
	// 		$profileid = $uid;
	// 		$datauid = $this->Home_model->detailprofile($profileid);
	// 		//$fullname = $datauid[0]->name;
	// 		$email = $datauid[0]->email;
	// 		$dataupdate = array(
	// 			'tglapprove' => date('Y-m-d H:i:s'),
	// 			'status' => '1',
	// 		);
	// 		$this->db->where('invoice',$order_id);
	// 		$this->db->update('tbl_transaksi',$dataupdate);

	// 		//updateuid
	// 		$dataupdateuid = array(
	// 			'status' => '1',
	// 			'role' => '1',
	// 		);
	// 		$this->db->where('uid',$uid);
	// 		$this->db->update('tbl_member',$dataupdateuid);

	// 		//Sent email;
	// 		$subjectnya = 'Congratulation '.$nama.', your account in ivetdata.com is activated';
	// 		$this->load->library('email'); 
	// 		// $query = $this->db->query("SELECT * FROM tbl_transaksi WHERE invoice='$invoice'");
	// 		// $result = $query->result_array();
	// 		// $member= $result[0]['member'];		
		   
	// 		$subject = $subjectnya;	
	// 		$message = 'Dear'.$nama.',<br /><br /> 
	// 					Congratulation, your payment method using '.$type.' is success. And your account in ivetdata.com is active now, Please Login<br><br>
	// 					<a href="'.base_url().'login" style="backgroun:#222;paddding:10px;color:#fff">'.base_url().'login</a><br><br>
	// 					Regards,
	// 					';
	// 		$text = $message;
	//         //$email = $user_email;
	//         //$this->load->view('testemail_view');
	//         $this->load->library('EmailSender.php');
	//         $this->emailsender->send($email,$subject,$text);
	//         $data = array(
	// 			'title' => 'Thank You - iVetdata.com',
	// 		);
	//         $this->load->view('website/header_view',$data);
	// 		$this->load->view('website/thanks_view',$data);
	// 		$this->load->view('website/footer_view');

	// 	  } 
	// 	  else if($transaction == 'pending'){
	// 	  // TODO set payment status in merchant's database to 'Pending'
	// 	  echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
	// 	  } 
	// 	  else if ($transaction == 'deny') {
	// 	  // TODO set payment status in merchant's database to 'Denied'
	// 	  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
	// 	}



	// }

	public function thanks(){
		$data = array(
			'title' => 'Thank You - iVetdata.com',
		);
		//$this->load->view('login_user');
		$this->load->view('website/header_view',$data);
		$this->load->view('website/thanks_view',$data);
		$this->load->view('website/footer_view');
	}

	public function confirm(){
		$uid = $this->input->get('id');
		$email = $this->input->get('email');
		$cekuser = $this->login_model->cekuser($uid,$email);
		if(count($cekuser) == '0'){
			$this->session->set_flashdata('error', 'Token confirmation email not found.');
			redirect ('login');
		}else{
			$dataupdate = array(
				'status' => '1',
			);
			$this->db->where('uid',$uid);
			$this->db->update('tbl_member',$dataupdate);
			$this->session->set_flashdata('success', 'Email account has been verified, please login.');
			if($cekuser[0]->role==0){
			    redirect ('/login/owner');
			}else{
			    redirect ('/login');
			}
		}
	}


	public function upload(){
		$data = array(
			'uid' => '1',
		);
		$this->load->view('header_login');
		$this->load->view('vetsupload_view', $data);
	}

	public function invoice(){
		$data = array(
			'uid' => '1',
		);
		$this->load->view('header_login');
		$this->load->view('invoice_view', $data);
	}

	public function pilihservice(){

		$data = array(
			'uid' => '1',
			'dataservice' => $this->login_model->service(),
		);
		$this->load->view('header_login');
		$this->load->view('service_view', $data);
	}


	// Check for user login process
	public function auth_proses() {	
		$this->form_validation->set_rules('user_email', 'Email', 'valid_email|required');
		$this->form_validation->set_rules('user_passnya', 'Password', 'required');
		$url =  $this->input->post('url');	
		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
				$this->load->view('admin_page');
			}else{
			$data = array(
				'error' => 'your email is not found.',
			);
				//$this->load->view('header_login');
				$this->load->view('login_new_view', $data);
			}
		} else {
			$email = $this->input->post('user_email');
			$passnya = md5($this->input->post('user_passnya'));

			$result = $this->login_model->ceklogin($email,$passnya);
			//var_dump($passnya);
			if ($result == TRUE) {	
				$user_email = $this->input->post('user_email');
				$result = $this->login_model->read_user_information($user_email);
				//var_dump($result)
				if ($result != false) {
				$session_data = array(
					'user_email' => $result[0]->email,
					'id' => $result[0]->uid,
					// 'tgltrial' => $result[0]->tgltrial,
					// 'tglend' => $result[0]->tglend,
					// 'user_status' => $result[0]->user_status,
					// 'rurl'=> current_url(),				
				);
				// $tgltrial = strtotime($result[0]->tgltrial);
				// $tglend = strtotime($result[0]->tglend);
				// $tglakhir = date('d M Y', $tglend);
				// $today = strtotime(date('Y-m-d'));
				$user_status = $result[0]->status;
				//$layanan = $result[0]->layanan;
				//$today = strtotime(date('2017-07-01'));                         
				//$akses = $tglend - $today;
				if ($user_status == '0'){
					//echo "Gak Bisa Akses";	
					$data = array(
						'error' => 'Email anda belum di validasi, silahkan kirimkan email anda ke support@temantrader.com',
					);
					$this->load->view('login_view', $data);
				}
				if ($user_status == '1'){
					//if ($tglend > $today){
						//echo"bisa akses";
						// Add user data in session
						$this->session->set_userdata('logged_in', $session_data);
						$sesslogid = session_id();
						if($this->input->post('remember_me'))
						{
							$this->load->helper('cookie');
							$cookie = $this->input->cookie('ci_session'); // we get the cookie
							$this->input->set_cookie('ci_session', $cookie, '2592000'); // and add one day to it's expiration
						}
						if (isset($this->session->userdata['logged_in'])) {
						$client  = @$_SERVER['HTTP_CLIENT_IP'];
						$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
						$remote  = $_SERVER['REMOTE_ADDR'];
					
						if(filter_var($client, FILTER_VALIDATE_IP))
						{
							$ip = $client;
						}
						elseif(filter_var($forward, FILTER_VALIDATE_IP))
						{
							$ip = $forward;
						}
						else
						{
							$ip = $remote;
						}

						$dataupdate = array(
							'ip' => $ip,
							'sesslog' => $sesslogid,
							'last_login' => date('Y-m-d H:i:s'),
						);
						$this->db->where('email',$user_email);
						$this->db->update('tbl_member',$dataupdate);
						
						redirect('dashboard');
							//var_dump($user_status);
							//redirect($this->session->userdata('rurl'));
						}
					// }else{
					// 	//echo "Gak Bisa Akses";
					// 	$url = base_url();	
					// 	$data = array(
					// 		'error' => 'Akses Anda telah habis pada <br>'.$tglakhir.', silahkan lakukan pembelian di <a href="'.$url.'layanan">Disini</a>',
					// 	);
					// 	$this->load->view('welcome_message', $data);				
					// }
				}
				
				// Add user data in session
				//$this->session->set_userdata('logged_in', $session_data);
				//$this->load->view('admin_page');
			}
		}else {
		$data = array(
			'error' => 'Yuor email and password is wrong, you can reset your password <a href="/forget_password"> here </a> '
		);
		//$this->load->view('header_login');
		$this->load->view('login_new_view', $data);
		}
		}
	}
	
	// Logout from admin page
	public function logout() {	
		// Removing session data
		$sess_array = array(
		'user_email' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['success'] = 'Successfully Logout';
		//$this->load->view('header_login');
		$this->load->view('login_new_view', $data);
		//redirect ('Welcome');
	}


}
