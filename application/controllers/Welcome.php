<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		$this->load->library('Mobile_Detect');
		// $this->load->model('Admlogin_model');
		$this->load->model('login_model');
		$this->load->model('Article_model');

    }
    
	public function index()
	{
		$data = array(
			'articlehome' => $this->Article_model->articlehome(),
			'title' => 'Welcome to Ivetdata.com',
			'page' => 'home',
		);
		$this->load->view('website/header_new_view');			
		$this->load->view('website/landing_new_view',$data);
		$this->load->view('website/vetlist_new');
		$this->load->view('website/stat_new');
		$this->load->view('website/about_new');
		//$this->load->view('website/testimonial_new');
		$this->load->view('website/latest_news');
		//$this->load->view('website/pre_footer');
		$this->load->view('website/footer_new_view');
		
	}
	
	public function partner()
	{
		$data = array(
			'title' => 'Welcome to Ivetdata.com',
			'page' => 'Partner',
		);
		$this->load->view('website/header_new_view');			
		//$this->load->view('website/landing_new_view',$data);
		$this->load->view('website/partner');
		//$this->load->view('website/testimonial_new');
		//$this->load->view('website/latest_news');
		//$this->load->view('website/stat_new');
		//$this->load->view('website/pre_footer');
		$this->load->view('website/footer_new_view');
		
	}
	
	public function search()
	{
		$data = array(
			'title' => 'Welcome to Ivetdata.com',
			'page' => 'Search',
		);
		$this->load->view('website/header_new_view');			
		//$this->load->view('website/landing_new_view',$data);
		$this->load->view('website/vetlist_new');
		//$this->load->view('website/testimonial_new');
		//$this->load->view('website/latest_news');
		//$this->load->view('website/stat_new');
		//$this->load->view('website/pre_footer');
		$this->load->view('website/footer_new_view');
		
	}
	
	public function searchnew()
	{
		$data = array(
			'title' => 'Welcome to Ivetdata.com',
			'page' => 'Search',
		);
		//$this->load->view('website/header_new_view');			
		//$this->load->view('website/landing_new_view',$data);
		$this->load->view('website/vetlist_search');
		//$this->load->view('website/testimonial_new');
		//$this->load->view('website/latest_news');
		//$this->load->view('website/stat_new');
		//$this->load->view('website/pre_footer');
		//$this->load->view('website/footer_new_view');
		
	}

	public function notfound(){		
		$this->load->view('website/header_new_view');
		$this->load->view('404_new');
		$this->load->view('website/footer_new_view');
	}

	public function register_process(){
		$name = $this->input->post('name');
		$clinic = $this->input->post('clinic');
		$country = $this->input->post('country');
		$city = $this->input->post('city');
		$address = $this->input->post('address');
		$user_email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('user_passnya');
		$cekemail = $this->login_model->read_user_information($user_email);
		$adaemail = count($this->login_model->adaemail($user_email));
		//var_dump($adaemail);
		if ( $adaemail == '1'){
			$uid = $cekemail[0]->uid;
		}else{
			//add to db
			$datamember = array(
				'name' => $name,
				'email' => $user_email,
				'phone' => $phone,
				'status' => '1',
				'role' => '2',
				'password' => md5($this->input->post('user_passnya')),
				'tgl_reg' => date('Y-m-d h:i:s'),
				'unikcode' => md5($user_email),
				'country' => $country,
				'city' => $city,				
			);
			$this->db->insert('tbl_member',$datamember);
			$uid = $this->db->insert_id();
		};
		$cekclinic =  count($this->login_model->adaclinic($uid));
		if($cekclinic == '0'){
			$dataclinic = array(
				'uid' => $uid,
				'nameclinic' => $clinic,
				'address' => $address,
				'phone' => $phone,
				'email' => $user_email,
				'city' => $city,
				'country' => $country,
			);
			$this->db->insert('tbl_clinic',$dataclinic);
			$idclinic= $this->db->insert_id();
		};
		//add vets doctor
		$insetvets = array(
			'uid' => $uid,
			'idclinic' => $idclinic,
			'namedoctor' => $name,
			'nohp' => $phone,
			'email' => $user_email,
			'country' => $country,
			'city' => $city,
		);
		$this->db->insert('tbl_dokter',$insetvets);
		$data = array(
			'success' => 'Please upload your license and national id.',
			'uid' => $uid,
		);
		$this->load->view('header_login');
		$this->load->view('vetsupload_view', $data);
		 
	}

	public function upload(){
		$data = array(
			'uid' => '1',
		);
		$this->load->view('header_login');
		$this->load->view('vetsupload_view', $data);
	}

	


	// Check for user login process
	public function auth_proses() {	
		$this->form_validation->set_rules('user_email', 'Email', 'required');
		$this->form_validation->set_rules('user_passnya', 'Password', 'required');
		$clinic = $this->input->post('clinic');
		$url =  $this->input->post('url');
		$detect = new Mobile_Detect;	
		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
				$this->load->view('dashboard');
			}else{
			$data = array(
				'title' => 'Login - iVetData',
				'error' => 'Error : please fill all the form',
				'detect' => $detect,
			);
				if($clinic == '1'){
					//$this->load->view('header_login');
					$this->load->view('website/login_new_view', $data);
				}if($clinic == '0'){
					$this->load->view('website/loginowner_new_view', $data);
				}
			}
		} else {
			$email = $this->input->post('user_email');
			$passnya = md5($this->input->post('user_passnya'));
			$role = $clinic;
			$result = $this->login_model->ceklogin($email,$passnya,$role);
			//var_dump($passnya);
			if ($result == TRUE) {	
				$user_email = $this->input->post('user_email');
				$result = $this->login_model->read_user_information($user_email);
				//var_dump($result);die;
				if ($result != false) {
    				$user_status = $result[0]->status;
    				$role = $result[0]->role;
    				if ($clinic<>$role and $role <= '1'){

    					//echo "Login tidak sesuai jalur";	
    					error_reporting(0);
    					if ($role > '0') {
	    					$data = array(
	    						'title' => 'Login - iVetData',
	    						'error' => 'this email has been registered to Vet Account',
	    						'detect' => $detect,
	    					);
    					} else {
    						$data = array(
	    						'title' => 'Login - iVetData',
	    						'error' => 'this email has been registered to Pet Owner Account',
	    						'detect' => $detect,
	    					);
    					}

    					if($clinic == '1'){
    						//$this->load->view('header_login');
    						$this->load->view('website/login_new_view', $data);
    					}
    					if($clinic == '0'){
    						$this->load->view('website/loginowner_new_view', $data);
    					}
    				}else{
	    				$idservice = $result[0]->idservice;
	            if($idservice>1){
	                $today =strtotime(date('Y-m-d H:i:s'));
	                $expired=strtotime($result[0]->expdate);
	    				    if($today-$expired>0){
	    				        $expired = true;
	    				    }else{
	    				        $expired = false;
	    				    }
	    				}else{
	    				    $expired = false;
	    				}
							$session_data = array(
	    					'user_email' => $result[0]->email,
	    					'id' => $result[0]->uid,
	    					'name' => $result[0]->name,
	    					'idservice' => $idservice,
	    					'expired' => $expired,
	    					'role' => $role,
	    					// 'tgltrial' => $result[0]->tgltrial,
	    					// 'tglend' => $result[0]->tglend,
	    					// 'user_status' => $result[0]->user_status,
	    					// 'rurl'=> current_url(),				
	    				);
    				}
    			
    			// $tgltrial = strtotime($result[0]->tgltrial);
				// $tglend = strtotime($result[0]->tglend);
				// $tglakhir = date('d M Y', $tglend);
				// $today = strtotime(date('Y-m-d'));

				//$layanan = $result[0]->layanan;
				//$today = strtotime(date('2017-07-01'));                         
				//$akses = $tglend - $today;
				if ($user_status == '0'){
					//echo "Gak Bisa Akses";	
					$data = array(
						'title' => 'Login - iVetData',
						'error' => 'Your email has not validated, please open our email and verify your email address',
						'detect' => $detect,
					);
					if($clinic == '1'){
						//$this->load->view('header_login');
						$this->load->view('website/login_new_view', $data);
					}if($clinic == '0'){
						$this->load->view('website/loginowner_new_view', $data);
					}
					
				}
				if ($user_status == '1'){
					//if ($tglend > $today){
						//echo"bisa akses";
						// Add user data in session
						$this->session->set_userdata('logged_in', $session_data);
						$sesslogid = session_id();
						if($this->input->post('remember_me'))
						{
							$this->load->helper('cookie');
							$cookie = $this->input->cookie('ci_session'); // we get the cookie
							$this->input->set_cookie('ci_session', $cookie, '2592000'); // and add one day to it's expiration
						}
						if (isset($this->session->userdata['logged_in'])) {
						$client  = @$_SERVER['HTTP_CLIENT_IP'];
						$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
						$remote  = $_SERVER['REMOTE_ADDR'];
					
						if(filter_var($client, FILTER_VALIDATE_IP))
						{
							$ip = $client;
						}
						elseif(filter_var($forward, FILTER_VALIDATE_IP))
						{
							$ip = $forward;
						}
						else
						{
							$ip = $remote;
						}

						$dataupdate = array(
							'ip' => $ip,
							'sesslog' => $sesslogid,
							'last_login' => date('Y-m-d H:i:s'),
						);
						$this->db->where('email',$user_email);
						$this->db->update('tbl_member',$dataupdate);
						$detect = new Mobile_Detect;
						if ( $detect->isMobile() ) {
							redirect('dashboard');
						}else{
							if ($role < '3') {
								redirect('dashboard/petowner');
							} else{
								redirect('sendbird/customerService');
							}
						}
							//redirect($this->session->userdata('rurl'));
						}
					// }else{
					// 	//echo "Gak Bisa Akses";
					// 	$url = base_url();	
					// 	$data = array(
					// 		'error' => 'Akses Anda telah habis pada <br>'.$tglakhir.', silahkan lakukan pembelian di <a href="'.$url.'layanan">Disini</a>',
					// 	);
					// 	$this->load->view('welcome_message', $data);				
					// }
				}
				
				// Add user data in session
				//$this->session->set_userdata('logged_in', $session_data);
				//$this->load->view('admin_page');
			}
		}else {
		// $data = array(
		// 	'error' => 'Your email and password is wrong, you can reset your password <a role="button" data-toggle="collapse" href="#signin-resend-password" aria-expanded="false" aria-controls="signin-resend-password"> here </a> '
		// );
			$detect = new Mobile_Detect;
			$data = array(
				'title' => 'Login - iVetData',
				'error' => 'Your email or password is wrong. ',
				'detect' => $detect,
			);
			if($clinic == '1'){
					//$this->load->view('header_login');
					$this->load->view('website/login_new_view', $data);
			}if($clinic == '0'){
				$this->load->view('website/loginowner_new_view', $data);
			}
		}
		}
	}
	
	// Logout from admin page
	public function logout() {	
		// Removing session data
		$sess_array = array(
		'user_email' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data = array(
			'title' => 'Successfull Logout - ivetdata.com'
		);
		$this->load->view('website/login_new_view',$data);
		//redirect ('Welcome');
	}

	// Logout from admin page
	public function logoutowner() {	
		// Removing session data
		$detect = new Mobile_Detect;
		$sess_array = array(
			'user_email' => '',
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data = array(
			'title' => 'Successfull Logout - ivetdata.com',
			'detect' => $detect,
		);
		$this->load->view('website/loginowner_new_view',$data);
		//redirect ('Welcome');
	}
}
