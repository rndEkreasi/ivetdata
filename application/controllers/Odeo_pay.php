<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Odeo_pay extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		//$this->load->model('login_model');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Pet_model');
		$this->load->model('Clinic_model');
		$this->load->model('Article_model');

    }

    public function callback(){
    	// header('Content-Type: application/json');
    	// header('X-Odeo-Timestamp : ')
    	header( "Content-type: application/json" );
    	$json = file_get_contents('php://input');
		$obj = json_encode($json, TRUE);
		print($obj);
		// $message = $obj[0]["message"];
		// $error_code = $obj[0]["error_code"];
		// $status_code = $obj[0]["status_code"];
		//if($status_code == )
		//var_dump($obj);
    }

}


