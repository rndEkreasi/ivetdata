<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Customer_model');
		$this->load->model('Clinic_model');
    }

 //    public function editowner(){
 //    	if (isset($this->session->userdata['logged_in'])) {
	// 		$profileid  = ($this->session->userdata['logged_in']['id']);
	// 		$dataprofile = $this->Home_model->detailprofile($profileid);
	// 		$role = $dataprofile[0]->role;
	// 		$manageto = $dataprofile[0]->manageto;
	// 		$nama = $dataprofile[0]->name;		
	// 		$phone = $dataprofile[0]->phone;
	// 		$country = $dataprofile[0]->country;
	// 		$city = $dataprofile[0]->city;
	// 		$photo = $dataprofile[0]->photo;
	// 		if($photo == '' ){
	// 		    $profilephoto = base_url().'assets/images/team/05.jpg';
	// 		}else{
	// 		    $profilephoto = $photo;
	// 		};
	// 		if($role != '0'){
	// 			$idclinic = $dataprofile[0]->idclinic;
	// 			$dataclinic = $this->Home_model->detailclinic($idclinic);	
	// 			$logoclinic = $dataclinic[0]->picture;
	// 			if($logoclinic == '' ){
	// 			    $logoclinicfix = base_url().'assets/images/team/06.jpg';
	// 			}else{
	// 			    $logoclinicfix = $logoclinic;
	// 			};
	// 			$nameclinic = $dataclinic[0]->nameclinic;
	// 			$addressclinic = $dataclinic[0]->address;
	// 			$cityclinic = $dataclinic[0]->city;
	// 			$countryclinic = $dataclinic[0]->country;
	// 			$latclinic = $dataclinic[0]->lat;
	// 			$longclinic = $dataclinic[0]->long;
	// 			$postalcode = $dataclinic[0]->postalcode;
	// 		}else{
	// 			$idclinic = '';
	// 			$logoclinicfix = '';
	// 			$nameclinic = '';
	// 			$addressclinic = '';
	// 			$cityclinic = '';
	// 			$countryclinic = '';
	// 			$latclinic = '';
	// 			$longclinic = '';
	// 			$postalcode = '';
	// 		}
				
	// 		//$clinicname = $dataclinic[0]->nameclinic;
	// 		if($manageto == '1'){
	// 			$datavet = $this->Home_model->datadokter($profileid);
	// 			$licenseid = $datavet[0]->idlicense;
	// 			$license = $datavet[0]->license;
	// 			$kta = $datavet[0]->kta;
	// 			$sip = $datavet[0]->sip;
	// 		}else{
	// 			$licenseid = '0';
	// 			$license = '0';
	// 			$kta = '0';
	// 			$sip = '0';
	// 		}
			
	// 		$data = array(
	// 			'title' => 'Edit My Profile - iVetdata.com',
	// 			'nama' => $nama,
	// 			'phone' => $phone,
	// 			'email' =>  $dataprofile[0]->email,
	// 			'country' => $country,
	// 			'city' => $city,
	// 			'license' => $license,
	// 			'licenseid' => $licenseid,
	// 			'kta' => $kta,
	// 			'sip' => $sip,
	// 			'logoclinic' => $logoclinicfix,
	// 			'profilephoto' => $profilephoto,
	// 			'role' => $role,
	// 			'manageto' => $manageto,				
	// 			'nameclinic' => $nameclinic,
	// 			'idclinic' => $idclinic,
	// 			'address' => $addressclinic,
	// 			'cityclinic' => $cityclinic,
	// 			'countryclinic' => $countryclinic,
	// 			'lat' => $latclinic,
	// 			'long' => $longclinic,
	// 			'postalcode' => $postalcode,
	// 			'dataprofile' => $dataprofile,
	// 			'success' => $this->session->flashdata('success'),
	// 			'error' => $this->session->flashdata('error'),
	// 		);
	// 		//$this->load->view('login_user');
	// 		$this->load->view('website/headerdash_view',$data);
	// 		$this->load->view('website/editprofile_view',$data);
	// 		//$this->load->view('website/footerdash_view',$data);
	// 	}else{
	// 		$this->session->set_flashdata('success', 'Please login first.');
	// 		redirect ('login');
	// 	}
	// }


    public function edit(){
    	if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$manageto = $dataprofile[0]->manageto;
			$nama = $dataprofile[0]->name;		
			$phone = $dataprofile[0]->phone;
			$address = $dataprofile[0]->address;
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			if($role != '0'){
				$idclinic = $dataprofile[0]->idclinic;
				$dataclinic = $this->Home_model->detailclinic($idclinic);	
				$logoclinic = $dataclinic[0]->picture;
				if($logoclinic == '' ){
				    $logoclinicfix = base_url().'assets/images/team/06.jpg';
				}else{
				    $logoclinicfix = $logoclinic;
				};
				$nameclinic = $dataclinic[0]->nameclinic;
				$addressclinic = $dataclinic[0]->address;
				$phoneclinic = $dataclinic[0]->phone;
				$cityclinic = $dataclinic[0]->city;
				$countryclinic = $dataclinic[0]->country;
				$latclinic = $dataclinic[0]->lat;
				$longclinic = $dataclinic[0]->lang;
				$postalcode = $dataclinic[0]->postalcode;
				$public =  $dataclinic[0]->public_info;
				$categories = $dataclinic[0]->category;
			}else{
				$idclinic = '';
				$logoclinicfix = '';
				$nameclinic = '';
				$addressclinic = '';
				$phoneclinic = '';
				$cityclinic = '';
				$countryclinic = '';
				$latclinic = '';
				$longclinic = '';
				$postalcode = '';
				$public = $dataprofile[0]->public_info;
				$categories = '';
			}
				
			//$clinicname = $dataclinic[0]->nameclinic;
			if($manageto == '1'){
				$datavet = $this->Home_model->datadokter($profileid);
				$licenseid = $datavet[0]->idlicense;
				$license = $datavet[0]->license;
				$kta = $datavet[0]->kta;
				$sip = $datavet[0]->sip;
			}else{
				$licenseid = '0';
				$license = '0';
				$kta = '0';
				$sip = '0';
			}
			$list_cat = $this->Clinic_model->listcat();
			$data = array(
				'title' => 'Edit My Profile - iVetdata.com',
				'nama' => $nama,
				'phone' => $phone,
				'email' =>  $dataprofile[0]->email,
				'homeaddress' => $address,
				'country' => $country,
				'city' => $city,
				'license' => $license,
				'licenseid' => $licenseid,
				'kta' => $kta,
				'sip' => $sip,
				'logoclinic' => $logoclinicfix,
				'profilephoto' => $profilephoto,
				'role' => $role,
				'manageto' => $manageto,				
				'nameclinic' => $nameclinic,
				'idclinic' => $idclinic,
				'address' => $addressclinic,
				'phoneclinic' => $phoneclinic,
				'cityclinic' => $cityclinic,
				'countryclinic' => $countryclinic,
				'lat' => $latclinic,
				'long' => $longclinic,
				'postalcode' => $postalcode,
				'dataprofile' => $dataprofile,
				'public_info' => $public,
				'categories' => $categories,
				'list_categories' => $list_cat,
				'success' => $this->session->flashdata('success'),
				'error' => $this->session->flashdata('error'),
			);
			//$this->load->view('login_user');
			$this->load->view('website/headerdash_view',$data);
			$this->load->view('website/editprofile_view',$data);
			//$this->load->view('website/footerdash_view',$data);
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function updateinfo(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$idclinic = $dataprofile[0]->idclinic;
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			$manageto = $dataprofile[0]->manageto;
			// $dataclinic = $this->Home_model->dataclinic($profileid);
			// $idclinic = $dataclinic[0]->idclinic;
			// if($manageto == '3'){

			// }else{
			// 	$datavet = $this->Home_model->datadokter($profileid);
			// 	$licenseid = $datavet[0]->idlicense;
			// 	$license = $datavet[0]->license;
			// }
			
			//dataedit
			$name = $this->input->post('fullname');
			$phone = $this->input->post('phone');
			$homeaddress = $this->input->post('homeaddress');
			$homecity = $this->input->post('homecity');
			$homecountry = $this->input->post('homecountry');
			if($homecountry=="")$homecountry='Indonesia';
			$public_info = "0001111111";
			if(isset($_POST['permission'])){
    			$permission = $this->input->post('permission');
    			foreach($permission as $val){
    			    $public_info[$val]=1;
    			}
			}
            
            if($role>0){
    			//dataclinic
    			$nameclinic = $this->input->post('nameclinic');
    			$idclinic = $this->input->post('idclinic');
    			$addressclinic = $this->input->post('address');
    			$phoneclinic = $this->input->post('phone2');
    			$cityclinic = $this->input->post('city');
    			$country = $this->input->post('homecountry');
    			if($country=="")$country='Indonesia';
    			$address = $addressclinic.','.$city.','.$country;
    			// $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&address='.$address.'&sensor=false');
    			// $output= json_decode($geocode);
    			$lat = $this->input->post('lat');
    			$long = $this->input->post('long');
    			// $long = $output->results[0]->geometry->location->lng;
    
    			//$cityclinic = $this->input->post('city');
    			$postalcode = $this->input->post('postalcode');
            }

			//config photo
			$config['upload_path']   = './upload/member'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['max_width']  = '300';
			$config['max_height']  = '300';
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('profilepic')) {
			  	//$featured_imageupdate = $featured_image;
			   	$error_featured = $this->upload->display_errors();
			   	$profileupdate = array(
			   		'name' => $name,					
					'phone' => $phone,	
					'address' => $homeaddress,
					'city' => $homecity,
					'country' => $homecountry,
					'public_info' => $public_info,
			   	);
			   	$this->db->where('uid',$profileid);
				$this->db->update('tbl_member',$profileupdate);				
				$success = $error_featured;
				$this->session->set_flashdata('error', $success);
				redirect ($_SERVER['HTTP_REFERER']);		    	
			}else{
				$result1 = $this->upload->data();
		   		$filephoto = $result1['file_name'];
		   		//upload to aws
				$keyname = $filephoto;
				$getpng = FCPATH."upload/member/".$keyname;
				$degrees = $this->input->post('rotate');
				if($degrees<>0){
                    // File and rotation
                    $rotateFilename = $getpng; // PATH
                    $degrees = 360 - $degrees;
                    $fileType = strtolower(substr($keyname, strrpos($keyname, '.') + 1));
                    
                    if($fileType == 'png'){
                       $source = imagecreatefrompng($rotateFilename);
                       $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
                       // Rotate
                       $rotate = imagerotate($source, $degrees, $bgColor);
                       imagesavealpha($rotate, true);
                       imagepng($rotate,$rotateFilename);
                    
                    }
                    
                    if($fileType == 'jpg' || $fileType == 'jpeg'){
                       $source = imagecreatefromjpeg($rotateFilename);
                       // Rotate
                       $rotate = imagerotate($source, $degrees, 0);
                       imagejpeg($rotate,$rotateFilename);
                    }
				}				

				$this->load->library('Aws3');
				$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				$data = array('upload_data' => $image_data['file_name']);
				$url_photo = $data['upload_data'];

				$updatephoto = array(
					'name' => $name,					
					'phone' => $phone,
					'photo' => $url_photo,
					'address' => $homeaddress,
					'city' => $homecity,
					'country' => $homecountry,
					'public_info' => $public_info,
				);
				$this->db->where('uid',$profileid);
				$this->db->update('tbl_member', $updatephoto);
				$success = 'Data has been successfully updated';
				$profilephoto = base_url().'upload/member/'.$filephoto;
			};
			
			if($role>0){
			    
			    //update dataclinic
				$updateclinic = array(
					'nameclinic' => $nameclinic,
					'address' => $addressclinic,
					'phone' => $phoneclinic,
					'city' => $cityclinic,
					'country' => $country,
					'postalcode' => $postalcode,
					'lat' => $lat,
					'lang' => $long,
				);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_clinic',$updateclinic);
				
			}else{
    			
    			//update tabel customer
	    		$updatecustomer = array(
					'nama' => $name,					
					'nohp' => $phone,
					'address' => $homeaddress,
					'city' => $homecity,
					'country' => $homecountry,
				);
				$this->db->where('uid',$profileid);
				$this->db->update('tbl_customer', $updatecustomer);
				
			    //update datapet
				$updatepet = array(
					'namapemilik' => $name,
					'address' => $homeaddress,
					'nohp' => $phone,
					'city' => $homecity,
					'country' => $homecountry,
				);
				$this->db->where('email',$email);
				$this->db->update('tbl_pet',$updatepet);
			
			    //update reminder
			    $updateowner = array(
					'nameowner' => $name,
					'phoneowner' => $phone,
				);
				$this->db->where('emailowner',$email);
				$this->db->update('tbl_vaccine',$updateowner);
			}
			$this->session->set_flashdata('success', $success);
			redirect ($_SERVER['HTTP_REFERER']);

		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function updatelicense(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$phone = $dataprofile[0]->phone;			
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$dataclinic = $this->Home_model->dataclinic($profileid);
			$idclinic = $dataclinic[0]->idclinic;
			$datavet = $this->Home_model->datadokter($profileid);
			//$licenseid = $datavet[0]->idlicense;
			$license = $datavet[0]->license;
			//dataedit
			$licenseid = $this->input->post('licenseid');
			//$license = $this->input->post('phone');
			//config photo
			$config['upload_path']   = './upload/vets/license'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;  
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('licensepic')) {
			  	//$featured_imageupdate = $featured_image;
			   	$error_featured = $this->upload->display_errors();
			   	$profileupdate = array(
			   		'idlicense' => $licenseid,					
					//'phone' => $phone,					 		
			   	);
			   	$this->db->where('uid',$profileid);
				$this->db->update('tbl_dokter',$profileupdate);
				$success = 'Data updated but License pic not update '.$error_featured.'';		    	
			}else{
				$result1 = $this->upload->data();
		   		$filephoto = $result1['file_name'];
		   		//upload to aws
				// $keyname = $filephoto;
				// $getpng = FCPATH."upload/vets/license/".$keyname;
				// $this->load->library('aws3');
				// $image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
				// $data = array('upload_data' => $image_data['file_name']);
				// $url_photo = $data['upload_data'];
				$updatephoto = array(
					'idlicense' => $licenseid,
					//'license' => $url_photo,
					'license' => base_url()."upload/vets/license/".$filephoto,
				);
				$this->db->where('uid',$profileid);
				$this->db->update('tbl_dokter', $updatephoto);
				$success = 'Data license successfully updated';
				$license = $filephoto;
			};
			$this->session->set_flashdata('success', $success);
			redirect ('profile/edit');

		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function updateclinic(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$phone = $dataprofile[0]->phone;			
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$dataclinic = $this->Home_model->dataclinic($profileid);
			$idclinic = $dataclinic[0]->idclinic;
			$datavet = $this->Home_model->datadokter($profileid);
			//$licenseid = $datavet[0]->idlicense;
			$license = $datavet[0]->license;
			//dataedit
			$idclinic = $this->input->post('idclinic');
			$nameclinic = $this->input->post('nameclinic');
			$address = $this->input->post('address');
			$city = $this->input->post('city');
			$country = $this->input->post('country');	
			$postalcode	 = $this->input->post('postalcode');
			$lat = $this->input->post('lat');
			$long = $this->input->post('long');
			if(isset($_POST['permission'])){
    			$permission = $this->input->post('permission');
                $public_info = "0000011111";    
                foreach($permission as $val){
    			    $public_info[$val]=1;
    			}
			}
			if(isset($_POST['categories'])){
    			$permission = $this->input->post('categories');
                $categories = "00000000000000000000";    
                foreach($permission as $val){
    			    $categories[$val]=1;
    			}
			}

			//$license = $this->input->post('phone');
			//config photo
			$config['upload_path']   = './upload/vets/'; 
			$config['allowed_types'] = 'jpg|png|jpeg'; 
			$config['max_size']      = 5500; 
			$config['encrypt_name'] = TRUE;
			$config['quality'] = 50;
			// $config['max_width']  = '1024';
   //      	$config['max_height']  = '1024';
			$this->load->library('upload', $config);
			if (isset($_FILES['logoclinic']) && is_uploaded_file($_FILES['logoclinic']['tmp_name'])) {
			    //load upload class with the config, etc...
			    //$this->load->library('upload', $config);
			    if (!$this->upload->do_upload('logoclinic')) {
			  	//$featured_imageupdate = $featured_image;
				   	$error_featured = $this->upload->display_errors();
				   	$this->session->set_flashdata('error', $this->upload->display_errors());
					redirect ('profile/edit');		    	
				}else{
					$result1 = $this->upload->data();
			   		$filephoto = $result1['file_name'];
					//upload to aws
					$keyname = $filephoto;
					$getpng = FCPATH."upload/vets/".$keyname;
					$this->load->library('aws3');
					$image_data['file_name'] = $this->aws3->sendFile('ivetdata',$keyname,$getpng);	
					$data = array('upload_data' => $image_data['file_name']);
					$picture = $data['upload_data'];
					$profileupdate = array(
						'nameclinic' => $nameclinic,
						'address' => $address,
						'city' => $city,
						'country' => $country,
						'postalcode' => $postalcode,
						'lat' => $lat,
						'lang' => $long,
						'picture' => $picture,
						'public_info' => $public_info,
						'category' => $categories,
					);
					$this->db->where('idclinic',$idclinic);
					$this->db->update('tbl_clinic',$profileupdate);
					
					//$license = $filephoto;
				};
				$this->session->set_flashdata('success', 'Clinic data successfully updated');
				redirect ('profile/edit');
			}else{
				$profileupdate = array(
						'nameclinic' => $nameclinic,
						'address' => $address,
						'city' => $city,
						'country' => $country,
						'postalcode' => $postalcode,
						'lat' => $lat,
						'lang' => $long,
						'public_info' => $public_info,
						'category' => $categories,
				);
				$this->db->where('idclinic',$idclinic);
				$this->db->update('tbl_clinic',$profileupdate);
				$this->session->set_flashdata('success', 'Clinic data successfully updated');
				redirect ('profile/edit');
			}
			

		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}

	public function updatepass(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$newpass = $this->input->post('newpass');
			$dataupdate = array(
				'password' => md5($this->input->post('newpass')),
				'realpass' => $this->input->post('newpass'),
			);
			$this->db->where('uid',$profileid);
			$this->db->update('tbl_member',$dataupdate);
			$this->session->set_flashdata('success', 'Password has been updated.');
			redirect ('profile/edit');
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}
}