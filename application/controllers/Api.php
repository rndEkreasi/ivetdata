<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
         $this->load->library('session');
         $this->load->database(); 
		 // Load form helper library
		$this->load->helper('form');
		$this->load->helper('security');		
		// Load form validation library
		$this->load->library('form_validation');		
		// Load session library
		$this->load->library('session');
		// $this->load->model('Admlogin_model');
		$this->load->model('Home_model');
		$this->load->model('Customer_model');
		$this->load->model('Login_model');
		$this->load->model('Pet_model');
    }
	
    function testimoni(){
        $this->db->from('tbl_testimoni');
		$query = $this->db->get();
	    $result = $query->result();
        if (count($result) > 0) {
             foreach ($result as $row){
                $arr_result[] = array(
                    'name'     => $row->name,
                    'url'   => $row->picture,
                    'comment'   => $row->comment,
                    'rates'  => $row->rates,
                    'likes' => $row->likes,
                    'date' => date("M d,Y",strtotime($row->created_on)),
                 );
             }
            echo json_encode($arr_result);
        }
    }

    public function result(){
		if (isset($this->session->userdata['logged_in'])) {
			$profileid  = ($this->session->userdata['logged_in']['id']);
			$dataprofile = $this->Home_model->detailprofile($profileid);
			$role = $dataprofile[0]->role;
			$nama = $dataprofile[0]->name;
			$email = $dataprofile[0]->email;		
			$country = $dataprofile[0]->country;
			$city = $dataprofile[0]->city;
			$photo = $dataprofile[0]->photo;
			$vetclinic = $dataprofile[0]->idclinic;		
			$idclinic = $vetclinic;	
			$dataclinic = $this->Home_model->detailclinic($idclinic);
			if($photo == '' ){
			    $profilephoto = base_url().'assets/images/team/05.jpg';
			}else{
			    $profilephoto = $photo;
			};
			$phone = $this->input->get('phone');
			$namecustomer = $this->input->get('customername');
			//$rfid = $idpet;
			//$datapet = $this->Pet_model->cekpetid($idpet);
			if($phone == ''){
				$datacustomer = $this->Customer_model->cekbyname($vetclinic,$namecustomer);
			}else{
				$datacustomer = $this->Customer_model->cekbyphone($phone,$vetclinic,$namecustomer);
			}
			if($namecustomer==""&&$phone==""){
			    $datacustomer = $this->Customer_model->dataname($config['per_page'],0,$idclinic,$role,$namecustomer);
			}
			$adacustomer = count($datacustomer);
			//var_dump($datapet);
			if($adacustomer == '0'){
				// $data = array(
				// 	'title' => 'Detail Pet - iVetdata.com',
				// 	'nama' => $nama,
				// 	'country' => $country,
				// 	'city' => $city,
				// 	'profilephoto' => $profilephoto,
				// 	'role' => $role,'manageto' => $dataprofile[0]->manageto,
				// 	'error' => 'Sorry, we cannot find pet id',
				// );
				// //$this->load->view('login_user');
				// $this->load->view('website/headerdash_view',$data);
				// $this->load->view('website/nopet_view',$data);
				// $this->load->view('website/footerdash_view',$data);
				$this->session->set_flashdata('error', 'Sorry data customer not found.');
				if($role == '0'){
					redirect ('dashboard/customer/all');
				}else{
					redirect ('customer/all');
				}

			}else{
				//pagination
				$all_customer = $datacustomer;
				$this->load->library('pagination');
				$config['base_url'] = base_url().'customer/all/';
				$config['total_rows'] = count($all_customer);
				$config['per_page'] = 10;
				//styling pagination
				$config['full_tag_open'] = "<ul class='pagination highlightlinks'>";
			    $config['full_tag_close'] = '</ul>';
			    $config['num_tag_open'] = '<li>';
			    $config['num_tag_close'] = '</li>';
			    $config['cur_tag_open'] = '<li class="active"><a href="#">';
			    $config['cur_tag_close'] = '</a></li>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';
			    $config['first_tag_open'] = '<li>';
			    $config['first_tag_close'] = '</li>';
			    $config['last_tag_open'] = '<li>';
			    $config['last_tag_close'] = '</li>';

			    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>';
			    $config['prev_tag_open'] = '<li>';
			    $config['prev_tag_close'] = '</li>';


			    $config['next_link'] = '<i class="fa fa-long-arrow-right"></i>';
			    $config['next_tag_open'] = '<li>';
			    $config['next_tag_close'] = '</li>';

				$from = $this->uri->segment(3);
				$this->pagination->initialize($config);
				
				$data = array(
					'title' => 'Customer - iVetdata.com',
					'nama' => $nama,
					'country' => $country,
					'city' => $city,
					'from' => '0',
					'idclinic' => $idclinic,
					'profilephoto' => $profilephoto,
					'role' => $role,
					'manageto' => $dataprofile[0]->manageto,				
					'nameclinic' => $dataclinic[0]->nameclinic,
					'all_customer' => count($datacustomer),
					'datacustomer' => $datacustomer,
					'success' => $this->session->flashdata('success'),
					'error' => $this->session->flashdata('error'),
					//'clinicfav' => $this->Home_model->clinicfav(),
					
				);
				$this->load->view('website/headerdash_view',$data);
				$this->load->view('website/mycustomer_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('success', 'Please login first.');
			redirect ('login');
		}
	}
	
}