<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb darklinks">
                            <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                            <li class="active">Edit Customer</li>
                        </ol>
                    </div>
                    <!-- .col-* -->
                    <div class="col-md-6 text-md-right">
                         <!--<span> <?php echo date('D d, M Y');?></span>-->
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </section>
        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Edit Customer</h3>
                            <a href="<?php echo base_url() ?>customer/all" class="icon-tab theme_button color2">Customer List</a>
                        </div>
                    </div>
                    <!-- .row -->
                    <form class="form-horizontal" action="<?php echo base_url() ?>customer/editprocess" method="post" enctype="multipart/form-data" style="padding:10px;">
                        <div class="row">
                            <div class="col-md-8">
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error; ?></div>
                                <?php } ?>       

                                <div class="with_padding">
                                    
                                    <h5>Customer's Info</h5> 
                                    <hr> 
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Customer's Name* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="customername" value="<?php if (isset($customername)){ echo $customername; } ?>" class="form-control active" required >
                                            <input type="hidden" name="idcustomer" value="<?php echo $idcustomer ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Phone*: </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="customerphone" value="<?php if (isset($customerphone)){ echo $customerphone; } ?>" class="form-control active" required >
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Email* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="customeremail" value="<?php if (isset($customeremail)){ echo $customeremail; } ?>" class="form-control active" required >
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Address : </label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control" name="customeraddress"><?php if (isset($customeraddress)){ echo $customeraddress; } ?></textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">City : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" value="<?php if (isset($customercity)){ echo $customercity; } ?>" name="customercity" aria-label="city" required>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Country : </label>
                                        <div class="col-lg-9"><input type="text" class="form-control" value="<?php if (isset($customercountry)){ echo $customercountry; } ?>" name="customercountry" aria-label="country" required>
                                            <!--<select class="form-control" name="customercountry" aria-label="country" required>
                                              <option value="<?php echo $customercountry ?>"><?php echo $customercountry ?></option>
                                              <?php foreach ($datacountry as $country) { ?>
                                                   <option value="<?php echo $country->namecountry?>"> <?php echo $country->namecountry?></option>
                                            <?php } ?>-->
                                            </select>
                                        </div>
                                        * Mandatory, must be filled.
                                    </div> 

                                    <div class="row">
                                        <div class="col-sm-12 text-right">                                            
                                            <button type="submit" class="theme_button wide_button">Update Customer</button>
                                        </div>
                                    </div>
                                    <!-- .row  -->

                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->
                        </div>
                        <!-- .row  -->


                    </form>

                </div>
                <!-- .container -->
            </section>
        
    </div>
</div>
</section>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#breed').autocomplete({
                source: "<?php echo site_url('pet/breedlist');?>",
      
                select: function (event, ui) {
                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });
 
        });
    </script>


</body>

</html>