<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">        
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb darklinks">
                        <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                        <li class="active">Detail Pet</li>
                    </ol>
                    </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
        </section> 

        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                            <a href="<?php echo base_url() ?>pet/add" class="icon-tab theme_button color3"></i>Add Pet</a>
                        </div>
                    </div>
                </div>
                <!-- .container -->
            </section>  
    </div>
</div>

</section>

