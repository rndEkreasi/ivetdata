<?php 
if($ada == '0'){ ?>
	<section class="ls ms section_404 background_cover section_padding_top_100 section_padding_bottom_130">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<div class="inline-block text-center">
								
											<h3>Oops, PET ID not Found</h3>
											<!-- <div class="aaha" style="margin: 30px 0px;">
												<a href="http://www.petmicrochiplookup.org/?microchip_id=<?php echo $rfid; ?>" target="_new">
													<img src="<?php echo base_url() ?>assets/images/PML-Logo.png" style="width: 200px"> Find in petmicrochiplookup.org
												</a>
											</div> -->

											
									<a href="<?php echo base_url() ?>" class="theme_button color3 wide_button">Back to Dashboard</a>
								</p>
										</div>
						</div>
					</div>
				</div>
			</section>

<?php }else{ ?>

	<section class="ls section_padding_top_100 section_padding_bottom_100 columns_padding_25">
					<div class="container">
						<div class="row">
							<div class="col-sm-7 col-md-8 col-lg-8 col-sm-push-5 col-md-push-4 col-lg-push-4">
								
								
								<article class="vertical-item content-padding big-padding post format-standard with_shadow rounded overflow-hidden">

									<div class="item-content entry-content">
										<header class="entry-header">

											
											<h3 style="margin: -10px 0px 5px;">Pet Info</h3>
											<?php if($pet_info[0]==1){?><strong><i class="fa fa-flag"></i> Pet Name :</strong> <br><?php echo $petname ?><br><?php } ?>
											<?php if($pet_info[1]==1){?><strong><i class="fa fa-paw"></i> Pet ID :</strong> <br><?php echo $rfid ?><br><?php } ?>
											<!--<?php if($pet_info[2]==1){?><strong><i class="fa fa-medkit"></i> Last Vaccination :</strong><br> Defensor, <?php echo date('d M, Y H:i:s',strtotime('2018-11-11 10:34:12')) ?> <?php if (count($lastvaccine) == '0'){
                                                                    echo '-';
                                                                }else{ ?>
                                                                     <?php echo $lastvaccine[0]->vaccine; ?> - <?php echo date('d M, Y',strtotime($lastvaccine[0]->datevacc)) ?>
                                                                <?php } ?><br> <?php } ?>-->

											<h3 style="margin: 20px 0px 5px;">Vets Info</h3>
											<?php if($clinic_info[0]==1){?><strong><i class="rt-icon2-health"></i> </i>Clinic :</strong> <br><?php echo $clinic ?><br><?php } ?>
											<?php if($clinic_info[1]==1){?><strong><i class="rt-icon2-health"></i> Vet Name :</strong><br> <?php echo $namevet ?><br><?php } ?>
											<?php if($clinic_info[2]==1){?><strong><i class="rt-icon2-phone"></i> Phone :</strong> <br><?php echo $phonevet ?><br><?php } ?>
											<?php if($clinic_info[3]==1){?><strong><i class="rt-icon2-mail"></i> Email :</strong> <br><?php echo $email ?><br><?php } ?>
											<?php if($clinic_info[4]==1){?><strong><i class="rt-icon2-pin-alt"></i> Address :</strong> <br><?php echo $address ?><br><?php } ?>
											
											<!-- .entry-meta -->

										</header>
										<!-- .entry-header -->

										


									</div>
									<!-- .item-content.entry-content -->
								</article>							
								<!-- .post -->
							</div>
							<!--eof .col-sm-8 (main content)-->

							<!-- sidebar -->
							<aside class="col-sm-5 col-md-4 col-lg-4 col-sm-pull-7 col-md-pull-8 col-lg-pull-8">
								<div class="item-media entry-thumbnail">
										<img src="<?php echo $petphoto ?>" alt="">
									</div>
								<!--  -->
								


							</aside>
							<!-- eof aside sidebar -->

						</div>
					</div>
				</section>
	<?php } ?>