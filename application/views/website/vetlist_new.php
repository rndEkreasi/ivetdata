<body>
   <!-- <div role="main" class="main">-->
   <div>
          <!--  <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section> -->
			<section id="find" class="parallax section section-parallax custom-padding-3 my-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'parallaxHeight': '120%'}" data-image-src="new/images/map.png">
					<div class="container">
						<div class="row justify-content-end">
							<div class="col-md-12 col-lg-7 col-xl-7">
								<div class="card bg-color-light border-0 appear-animation" data-appear-animation="fadeInUpShorter">
									<div class="card-body p-5">
										<h2 class="custom-primary-font font-weight-normal text-6 mb-3">FIND YOUR PET'S VET</h2>
										<p class="pb-2 mb-4">Click Search to find vets in our network</p>
										<form action="/vets/search" method="post">
											<div class="form-row">
												<div class="form-group col-md-4 pr-md-0">
													<input type="text" class="form-control custom-left-rounded-form-control" name="name" value="" placeholder="Find Pet ID" >
												</div>
												<div class="form-group col-md-4 pl-md-0 pr-md-0">
													<input type="text" class="form-control custom-middle-rounded-form-control" name="vet" value="" placeholder="Find Vet / Clinic" >
												</div>
												<div class="form-group col-md-4 pl-md-0">
													<input type="text" class="form-control custom-right-rounded-form-control" name="city" value="" placeholder="Find by City" >
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col mb-0">
										            <p>If you found lost pet please enter their pet ID here to contact it's designated vet</p>
													<input type="submit" value="SEARCH" class="btn btn-secondary custom-btn-style-1 py-3 text-4 w-100">
												</div>
											</div>
										</form>
									</div>
									<div class="card-footer bg-color-primary border-0 custom-padding-2">
										<div class="row">
											<div class="col-md-6 mb-5 mb-md-0">
												<div class="feature-box align-items-center">
													<div class="feature-box-icon bg-color-tertiary">
														<i class="far fa-envelope-open position-relative" style="top: -1.3px; left: 0.5px;"></i>
													</div>
													<div class="feature-box-info">
														<span class="text-color-light font-weight-light text-2 d-block">EMAIL</span>
														<a href="mailto:cs@ivetdata.com" class="text-color-light custom-fontsize-2">cs@ivetdata.com</a>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="feature-box align-items-center">
													<div class="feature-box-icon bg-color-tertiary">
														<i class="fas fa-phone text-3"></i>
													</div>
													<div class="feature-box-info">
														<span class="text-color-light font-weight-light text-2 d-block">PHONE</span>
														<a href="tel:+62-21-2977-951" class="text-color-light custom-fontsize-2">+62-21-2977-951</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

