<section class="ls with_bottom_border">
    <div class="container-fluid">
        <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard/clinic">Dashboard</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>shop/myinvoice">All Invoice</a>
                                </li>
                                <li class="active">View Invoice</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
    <?php foreach ($datainvoice as $invoice) {
    $noinvoice = $invoice->invoice; 
    $discount = $invoice->discount;
    $total = $invoice->totalprice;
    ?>
<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid" id="DivIdToPrint">

                    <div class="row" >
                        <div class="col-sm-6">
                            <h3>Invoice : #<?php echo $noinvoice; ?> - Rp. <?php echo number_format($invoice->totalprice) ?></h3>
                            
                            
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <h3></h3>
                            Invoice To : <br>
                            <div class="row form-group" style="margin: 0px;">
                                <div class="col-md-12">
                                    Name : <?php echo $invoice->nameclient; ?><br>
                                    Phone : <?php echo $invoice->phoneclient; ?><br>
                                    Email : <?php echo $invoice->emailclient; ?><br>
                                    Date : <?php echo date('d M Y', strtotime($invoice->invdate));
                                    $idpet = $invoice->idpet ;
                                    $detailpet = $this->Pet_model->detailpet($idpet);
                                    
                                    ?>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .row -->

                    <form class="form-horizontal">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="with_border with_padding">
                                    <label class="control-label" style="text-align: right;"> Status : 
                                        <?php 
                                        $status = $invoice->status;
                                        if($status == '1'){ ?>
                                            <span class="label label-primary">Paid</span>
                                        <?php }else{ 
                                                if($status == '0'){ ?>
                                                    <span class="label label-danger">Unpaid</span>
                                                <?php };
                                        } ?>
                                        </label>
                                    <!-- <a href="<?php echo base_url() ?>sales/edit/?invoice=<?php echo $noinvoice; ?>" type="submit" class="theme_button color2" style="float: right;">Edit order</a> -->
                                    <div class="clearfix"></div>

                                    <hr>

                                    <div class="table-responsive bottommargin_20">
                                        <table class="table table-striped table-bordered">
                                            <tbody><tr>
                                                <th style="width:50%;">Item</th>
                                                <th>Price</th>
                                                <th style="width:5%;">Qty</th>
                                                <th>Total</th>
                                            </tr>
                                            <?php foreach ($iteminvoice as $item ) { 
                                                $price = $item->price;
                                                $qty = $item->qty;
                                                $totalprice = $price * $qty;                                               
                                            ?>                                              
                                            
                                            <tr class="item-editable">
                                                <td>
                                                    <div class="media">
                                                        <div class="media-body media-middle">
                                                            <?php echo $item->item ?>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="media-middle">
                                                    <strong>
                                                        Rp. <?php echo number_format($price) ; ?>
                                                    </strong>
                                                </td>
                                                <td class="media-middle">
                                                    <?php echo number_format($qty) ;  ?>
                                                </td>
                                                <td class="media-middle">
                                                   Rp. <?php echo number_format($totalprice); ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <!-- <tr>
                                                <td></td>
                                                <td class="text-right" colspan="2"><strong>Sub Total</strong></td>
                                                <td><span>Rp. <?php // echo number_format($total + $discount - $invoice->tax ) ?></span></td>
                                            </tr> -->
                                            <!-- <tr>
                                                <td></td>
                                                <td class="text-right" colspan="2"><strong>Discount</strong></td>
                                                <td><span>Rp. <?php echo number_format($discount) ?></span></td>
                                            </tr> -->
                                            <!-- <tr>
                                                <?php // $ppn = $total * 0.10; ?>
                                                <td></td>
                                                <td class="text-right" colspan="2"><strong>Tax</strong></td>
                                                <td><span>Rp. <?php echo number_format($invoice->tax) ?></span></td>
                                            </tr> -->
                                            <tr>
                                                <td></td>
                                                <td class="text-right" colspan="2"><strong>Grand Total</strong></td>
                                                <td><span>Rp. <?php echo number_format($total) ?></span></td>
                                            </tr>
                                            

                                        </tbody>
                                    </table>
                                     </form>
                                    </div>
                                    <!-- .table-responsive -->                                    
                                    <div class="row form-group">
                                        <div class="col-lg-12">
                                            <?php if($status == '1'){ ?>
                        
                                                <?php }else{ 
                                                    if($status == '0'){ ?>
                                                    <form action="/shop/payprocess" method="post">
                                                        <input type="hidden" name="invoice" value="<?php echo $noinvoice; ?>">
                                                        <input type="submit" value="Pay" style="float: right;" class="btn btn-primary">
                                                    </form>
                                                <?php };
                                                } ?>
                                        </div>                                       
                                        
                                        <!-- <div class="col-lg-4">
                                            <select class="form-control">
                                                <option value="cod">Cash on Delivery</option>
                                                <option value="banktransfer">Bank Transfer</option>
                                            </select>
                                        </div> -->
                                    </div>

                                    <!-- <div class="row">
                                        <div class="col-sm-12 text-right">                                            
                                            <a href="<?php echo base_url() ?>sales/edit/?invoice=<?php echo $noinvoice; ?>" type="submit" class="theme_button color2">Edit order</a>
                                        </div>
                                    </div> -->
                                    <!-- .row  -->

                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->
                        </div>
                        <!-- .row  -->
                   

                </div>
                <div class="" style="float: right;margin: 25px 0px 0px;">
                    
                </div>
                    

                <!-- .container -->
                
            </section>
<?php } ?>
<script type="text/javascript">
    
    function printDiv(){

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><head> <link rel="stylesheet" href="/assets/css/bootstrap.min.css"></head><body">'+divToPrint.innerHTML+'</body></html>');
  
  window.print();

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

function printData()
{
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
</script>