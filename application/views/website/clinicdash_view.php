            <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <!-- <li>
                                    <a href="<?php echo base_url()  ?>dashboard">Home</a>
                                </li> -->
                                <li class="active"><?php echo date('D d, M Y');?></li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">
                    <!-- <div class="row">
                        <div class="col-md-6">
                            <div class="widget widget_search">
                                <form method="get" class="" action="<?php echo base_url() ?>pet/detail/">
                                    <!-- <div class="form-group-wrap"> 
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="idpet" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> 
                                            </form>
                            </div>
                        </div>

                    </div> -->

                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="dashboard-page-title"><?php echo $clinic; ?> Dashboard</h3>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <h3 class="sparklines-title" style="font-size: 20px">
                                <sup>Revenue Today :</sup>                                
                                Rp.   <?php // echo number_format(2000000); ?>   
                                <?php echo number_format($revtoday[0]->totalprice); ?>                          

                                <!-- <span class="sparklines" data-values="670,350,135,-170,-324,-386,-468,-200,55,375,520,270,790,-670,-350,135,170,324,386,468,10,55,375,520,270,790" data-type="bar" data-line-color="#eeb269" data-neg-color="#dc5753" data-height="30" data-bar-width="2">
                                </span> -->

                            </h3>

                            <h3 class="sparklines-title" style="font-size: 20px">
                                <sup>Revenue Yesterday : </sup>
                                Rp. <?php // echo number_format(1500000) ?>  
                                <?php echo number_format($revyesterday[0]->totalprice) ?>                            

                                <!-- <span class="sparklines" data-values="670,350,135,-170,-324,386,-468,-10,55,375,520,-270,790,670,-350,135,170,324,386,468,10,-55,-375,-520,270,790" data-type="bar" data-line-color="#4db19e" data-neg-color="#007ebd" data-height="30" data-bar-width="2">
                                </span> -->
                            </h3>

                        </div>

                    </div>
                    <!-- .row -->


                    <div class="row">
                        <div class="col-lg-6 col-sm-12" style="display: flex;">

                            <div class="col-lg-6 col-sm-12" style="width: 50%">
                                <a href="<?php echo base_url() ?>customer/all"><div class="teaser warning_bg_color counter-background-teaser text-center">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $countcustomer ?>" data-speed="2100"><?php echo $countcustomer ?></span>
                                    <h3 class="counter highlight" data-from="0" data-to="<?php echo $countcustomer ?>" data-speed="2100"><?php echo $countcustomer ?></h3>
                                    <p>Customers</p>
                                </div>
                                </a>
                            </div>

                            <div class="col-lg-6 col-sm-12" style="width: 50%">
                                <a href="<?php echo base_url() ?>pet/all"><div class="teaser danger_bg_color counter-background-teaser text-center">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $countpet ?>" data-speed="1500">0</span>
                                    <h3 class="counter highlight" data-from="0" data-to="<?php echo $countpet ?>" data-speed="1500"><?php echo $countpet ?> </h3>
                                    <p>Pets</p>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12">

                            <div class="col-lg-6 col-sm-6"><a href="<?php echo base_url() ?>sales/all">
                                <div class="teaser info_bg_color counter-background-teaser text-center">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $totalrev[0]->totalprice/1000 ?>" data-speed="1800"><?php echo $totalrev[0]->totalprice/1000 ?></span>
                                    <h3 class="counter-wrap highlight" data-from="0" data-to="<?php echo $totalrev[0]->totalprice/1000 ?>" data-speed="1800">
                                        
                                        <span class="" style="font-size: 28px;">Rp. <?php echo number_format($monthrev[0]->totalprice )?></span>
                                        <small class="counter-add"></small>
                                    </h3>
                                    <p>Revenue <?php echo date('M Y') ?></p>
                                </div></a>
                            </div>

                            <div class="col-lg-6 col-sm-6"><a href="<?php echo base_url() ?>sales/all">
                                <div class="teaser success_bg_color counter-background-teaser text-center">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $totalrev[0]->totalprice ?>"" data-speed="1900"><?php echo $totalrev[0]->totalprice ?>"</span>
                                    <h3 class="counter-wrap highlight" data-from="0" data-to="<?php echo $totalrev[0]->totalprice ?>" data-speed="1900"><span class="" style="font-size: 28px;">Rp. <?php echo number_format($totalrev[0]->totalprice )?></span></h3>
                                    <p>Revenue <?php echo date('M Y',strtotime('-1 month')) ?></p>
                                </div></a>
                            </div>

                            
                        </div>
                        
                        <div class="col-lg-6 col-sm-12" style="display: flex;">
                            <?php $datapending = count($this->Clinic_model->getpending($idclinic)); ?>
                            <div class="col-lg-6 col-sm-12" style="width: 50%">
                                <a href="<?php echo base_url() ?>vets/pending"><div class="teaser warning_bg_color counter-background-teaser text-center">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $datapending; ?>" data-speed="2100"><?php echo $datapending ?></span>
                                    <h3 class="counter highlight" data-from="0" data-to="<?php echo $datapending; ?>" data-speed="2100"><?php echo $datapending; ?></h3>
                                    <p> Approvals</p>
                                </div>
                                </a>
                            </div>
                            <?php 
                            $remindervacc = $this->Reminder_model->remindervacc($idclinic,$role);
                            $remindermed = $this->Reminder_model->remindermed($idclinic,$role);
                            $reminder=$remindervacc+$remindermed; 
                            ?>
                            <div class="col-lg-6 col-sm-12" style="width: 50%">
                                <a href="<?php echo base_url() ?>reminder/vaccine"><div class="teaser danger_bg_color counter-background-teaser text-center">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $reminder ?>" data-speed="1500">0</span>
                                    <h3 class="counter highlight" data-from="0" data-to="<?php echo $reminder ?>" data-speed="1500"><?php echo $reminder ?> </h3>
                                    <p>Appointments</p>
                                </div>
                                </a>
                            </div>
                        </div>
                        
                        <div class="col-lg-6 col-sm-12" style="display: flex;">
                            <?php $datateam = $this->Team_model->totalvets($idclinic);$vets=0;$staff=0;$owner=0;foreach($datateam as $team){if($team->manageto==2){$vets++;}elseif($team->manageto==3){$staff++;}else{$owner++;}} ?>
                            <div class="col-lg-6 col-sm-12" style="width: 50%">
                                <a href="<?php echo base_url() ?>team/all"><div class="teaser warning_bg_color counter-background-teaser text-center" style="background-color:#f7b82b;">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $vets; ?>" data-speed="2100"><?php echo $vets; ?></span>
                                    <h3 class="counter highlight" data-from="0" data-to="<?php echo $vets; ?>" data-speed="2100"><?php echo $vets; ?></h3>
                                    <p>Vets</p>
                                </div>
                                </a>
                            </div>
                            <div class="col-lg-6 col-sm-12" style="width: 50%">
                                <a href="<?php echo base_url() ?>team/all"><div class="teaser danger_bg_color counter-background-teaser text-center" style="background-color:#8fc424;">
                                    <span class="counter counter-background" data-from="0" data-to="<?php echo $staff; ?>" data-speed="1500">0</span>
                                    <h3 class="counter highlight" data-from="0" data-to="<?php echo $staff; ?>" data-speed="1500"><?php echo $staff; ?> </h3>
                                    <p>Admin Staff</p>
                                </div>
                                </a>
                            </div>
                        </div>
                        
                    </div>

                    <div class="row"> 
                        <!-- Yearly Visitors -->
                        <div class="col-xs-12 col-md-6">
                            <div class="with_padding">
                                <h4>Sales Report <?php echo date('M Y') ?></h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-yearly-visitors"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- .col-* -->

                        <!-- Yearly Visitors -->
                        <div class="col-xs-12 col-md-6">
                            <div class=" with_padding">
                                <h4>Monthly Sales in 1 Year</h4>
                                <div class="canvas-chart-wrapper">
                                    <div id="chartContainer"></div>
                                    <canvas class="canvas-chart-line-new-returned-visitors"></canvas> 
                                </div>
                            </div>
                        </div>
                        <!-- .col-* -->  

                    </div>
                    <!-- .row -->

                    


                    


                    <div class="row">

                        <!-- Monthly Visitors -->
                        <!-- <div class="col-xs-12 col-md-6">
                            <div class="with_border with_padding">
                                <h4>Customer Report</h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-monthly-visitors"></canvas>
                                </div>
                            </div>
                        </div> -->
                        <!-- .col-* -->


                        <!-- Monthly Visitors -->
                        <!-- <div class="col-xs-12 col-md-6">
                            <div class="with_border with_padding">
                                <h4>Pet Report</h4>
                                <div class="canvas-chart-wrapper">
                                    <canvas class="canvas-chart-line-conversions"></canvas>
                                </div>
                            </div>
                        </div> -->
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
<script src="/assets/js/sendbird/SendBird.min.js"></script>
                <script src="/assets/js/sendbird/widget.SendBird.js"></script>
                <div id="sb_widget"></div>
                <?php
                
                    // persiapkan curl to SendBird
                    $ch = curl_init(); 
                
                    // set url 
                    curl_setopt($ch, CURLOPT_URL, "https://api-75295f3e-6490-4940-be2b-4a68560e3add.sendbird.com/v3/users");
                    
                    // return the transfer as a string 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode(array("user_id"=>$email,"nickname"=>$nama,"profile_url"=>$profilephoto)));
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Api-Token: e7056c594ee3e31648c758ce1f4da654bfb06b2a'));
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                
                    // $output contains the output string 
                    $output = curl_exec($ch); 
                
                    // mengembalikan hasil curl
                    if (curl_error($ch))
                    {
                        print curl_error($ch);
                    }
                    else
                    {
                        
                    }  
                    
                    // tutup curl 
                    curl_close($ch);      
                                    
                ?>
                <script>
                    var appId = '75295F3E-6490-4940-BE2B-4A68560E3ADD';
                    var userId = '<?php echo $email; ?>';
                    var nickname = '<?php echo $nama; ?>';
                    //var channelUrl = 'ivetdata';
                    var sb = new SendBird({'appId': 'APP_ID'});
                    //sbWidget.start(appId);
                    sbWidget.startWithConnect(appId, userId, nickname);
                    //sbWidget.showChannel(channelUrl);
                </script>

                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal fade" id="expired" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Membership Expired</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button> 
                    </div>
                    <div class="modal-body">                    
                        <p>Your membership expiration is on <?php echo date('D, d M Y', $expdate) ?></p>
                        <a href="<?php echo base_url() ?>dashboard/buy" class="theme_button">Extend Membership</a>&nbsp;<a href="#" class="theme_button" data-dismiss="modal" onclick="document.cookie='expiry=1;';">Close</a>
                    </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="reminder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Vaccine Reminder</h5>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button> -->
                    </div>
                    <div class="modal-body">                    
                        <p>Hai <?php echo $nama; ?>,<br /><br />Ada beberapa pasien yang perlu Anda follow up.</p>
                        <a href="<?php echo base_url() ?>reminder/vaccine"  onclick="document.cookie='reminder=1;';" class="theme_button">Detail</a>&nbsp;<a href="#" class="theme_button" data-dismiss="modal" onclick="document.cookie='reminder=1;';">Close</a>
                    </div>
                </div>
              </div>
            </div>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar -->
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -->
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
window.onload = function () {
    <?php 
    $today = strtotime(date('Y-m-d'));
    $diff = floor(($expdate-$today)/3600/24);
    if(($today>$expdate||$diff<=5)&&$_COOKIE["expiry"]==0){ ?>
        $('#expired').modal({backdrop: 'static', keyboard: false});
    <?php } ?>
    <?php 
          if(($reminder>0||$reminder2>0)&&$_COOKIE["reminder"]==0){
    ?>
                $('#reminder').modal({backdrop: 'static', keyboard: false});
    <?php 
          }else{
                $_COOKIE["reminder"]==1;
          } 
    ?>
        
    var chart = new CanvasJS.Chart("chartContainer23", {
    animationEnabled: true,
    zoomEnabled: true,
    theme: "dark2",
    title:{
        text: "Growth in Internet Users Globally"
    },
    axisX:{
        title: "Year",
        valueFormatString: "####",
        interval: 2
    },
    axisY:{
        logarithmic: true, //change it to false
        title: "Internet Users (Log)",
        titleFontColor: "#6D78AD",
        lineColor: "#6D78AD",
        gridThickness: 0,
        lineThickness: 1,
        includeZero: false,
        labelFormatter: addSymbols
    },
    axisY2:{
        title: "Internet Users",
        titleFontColor: "#51CDA0",
        logarithmic: false, //change it to true
        lineColor: "#51CDA0",
        gridThickness: 0,
        lineThickness: 1,
        labelFormatter: addSymbols
    },
    legend:{
        verticalAlign: "top",
        fontSize: 16,
        dockInsidePlotArea: true
    },
    data: [{
        type: "line",
        xValueFormatString: "####",
        showInLegend: true,
        name: "Log Scale",
        dataPoints: [
            { x: 1994, y: 25437639 },
            { x: 1995, y: 44866595 },
            { x: 1996, y: 77583866 },
            { x: 1997, y: 120992212 },
            { x: 1998, y: 188507628 },
            { x: 1999, y: 281537652 },
            { x: 2000, y: 414794957 },
            { x: 2001, y: 502292245 },
            { x: 2002, y: 665065014 },
            { x: 2003, y: 781435983 },
            { x: 2004, y: 913327771 },
            { x: 2005, y: 1030101289 },
            { x: 2006, y: 1162916818 },
            { x: 2007, y: 1373226988 },
            { x: 2008, y: 1575067520 },
            { x: 2009, y: 1766403814 },
            { x: 2010, y: 2023202974 },
            { x: 2011, y: 2231957359 },
            { x: 2012, y: 2494736248 },
            { x: 2013, y: 2728428107 },
            { x: 2014, y: 2956385569 },
            { x: 2015, y: 3185996155 },
            { x: 2016, y: 3424971237 }
        ]
    },
    {
        type: "line",
        xValueFormatString: "####",
        axisYType: "secondary",
        showInLegend: true,
        name: "Linear Scale",
        dataPoints: [
            { x: 1994, y: 25437639 },
            { x: 1995, y: 44866595 },
            { x: 1996, y: 77583866 },
            { x: 1997, y: 120992212 },
            { x: 1998, y: 188507628 },
            { x: 1999, y: 281537652 },
            { x: 2000, y: 414794957 },
            { x: 2001, y: 502292245 },
            { x: 2002, y: 665065014 },
            { x: 2003, y: 781435983 },
            { x: 2004, y: 913327771 },
            { x: 2005, y: 1030101289 },
            { x: 2006, y: 1162916818 },
            { x: 2007, y: 1373226988 },
            { x: 2008, y: 1575067520 },
            { x: 2009, y: 1766403814 },
            { x: 2010, y: 2023202974 },
            { x: 2011, y: 2231957359 },
            { x: 2012, y: 2494736248 },
            { x: 2013, y: 2728428107 },
            { x: 2014, y: 2956385569 },
            { x: 2015, y: 3185996155 },
            { x: 2016, y: 3424971237 }
        ]
    }]
});
chart.render();

function addSymbols(e){
    var suffixes = ["", "K", "M", "B"];

    var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
    if(order > suffixes.length - 1)
        order = suffixes.length - 1;

    var suffix = suffixes[order];
    return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
}

}
</script>

    <!-- dashboard init -->
    <!-- <script src="<?php echo base_url() ?>assets/js/admin.js"></script> -->
    <script type="text/javascript">
            "use strict";

            (function(){
                
                //Charts initialization
                //http://www.chartjs.org/docs/

                    //Global Defaults
                        //fonts
                Chart.defaults.global.defaultFontColor = '#666666';
                Chart.defaults.global.defaultFontFamily = 'Poppins, Arial, sans-serif';
                Chart.defaults.global.defaultFontSize = 12;
                        //responsive
                Chart.defaults.global.maintainAspectRatio = false;

                        //legends
                Chart.defaults.global.legend.labels.usePointStyle = true;

                        //scale
                Chart.defaults.scale.gridLines.color = 'rgba(100,100,100,0.15)';
                Chart.defaults.scale.gridLines.zeroLineColor = 'rgba(100,100,100,0.15)';
                // Chart.defaults.scale.gridLines.drawTicks = false;
                
                // Chart.defaults.scale.ticks.min = 0;
                Chart.defaults.scale.ticks.beginAtZero = true;
                Chart.defaults.scale.ticks.maxRotation = 0;

                    //padding for Y axes
                Chart.defaults.scale.ticks.padding = 3;
                Chart.defaults.scale.ticks.autoSkipPadding = 10;

                    //points
                Chart.defaults.global.elements.point.radius = 5;
                Chart.defaults.global.elements.point.borderColor = 'transparent';


                    //custom Chart plugin for set a background to chart
                Chart.pluginService.register({
                beforeDraw: function (chart, easing) {
                    if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                        var ctx = chart.chart.ctx;
                        var chartArea = chart.chartArea;
                            ctx.save();
                            ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                            ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                            ctx.restore();
                        }
                    }
                });
                        

               

                // var MONTHS = [
                //     "Jan",
                //     "Feb",
                //     "Mar",
                //     "Apr",
                //     "May",
                //     "Jun",
                //     "Jul",
                //     "Aug",
                //     "Sep",
                //     "Oct",
                //     "Nov",
                //     "Dec"
                // ];

                var MONTHS = <?php  echo json_encode($dailymonth); ?>;

                var MONTHS_HALF = <?php  echo json_encode(array_reverse($bulan12)); ?>;

                //New and Returned visitors
                var $canvasesMonthlyVisitors = jQuery('.canvas-chart-line-new-returned-visitors');
                if ($canvasesMonthlyVisitors.length) {
                    $canvasesMonthlyVisitors.each(function(i){

                        var config = {
                            type: 'line',
                            data: {
                                labels: MONTHS_HALF,
                                datasets: [{
                                    label: "Monthly sales",
                                    //line options
                                    backgroundColor: 'rgba(236, 104, 46, 0.5)',
                        borderColor: 'rgba(236, 104, 46, 0.5)',
                        borderWidth: '0',

                        //point options
                        pointBorderColor: "transparent",
                        pointBackgroundColor: "rgba(236, 104, 46, 1)",
                        pointBorderWidth: 0,

                        tension: '0',
                                    //visitors per month
                                    data : <?php  echo json_encode(array_reverse($revbulan12)); ?>,
                                }, 
                                // {
                                //     label: "Returned Visitors",
                                //     backgroundColor: 'rgba(0, 126, 189, 0.2)',
                                //     borderColor: 'rgba(0, 126, 189, 1)',
                                //     borderWidth: 2,
                                //     fill: true,
                                //     //point options
                                //     pointBorderColor: "transparent",
                                //     pointBackgroundColor: "transparent",
                                //     pointBorderWidth: 0,
                                //     //returned visitors per month
                                //     data: [
                                        
                                //         22,
                                //         18,
                                //         45,
                                //         22,
                                //         18,
                                //         26,
                                //         20,

                                //     ],
                                // }, 
                                //put new dataset here if needed to show multiple datasets on one graph
                                ]
                            },
                            options: {
                                chartArea: {
                                    backgroundColor: 'rgba(100, 100, 100, 0.02)',
                                },
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            maxTicksLimit: 4
                                        }
                                    }]
                                }
                            }
                        };

                        var canvas = jQuery(this)[0].getContext("2d");;
                        new Chart(canvas, config);
                    });
                } //New and Returned visitors
                var DAYS = <?php  echo json_encode($dailymonth); ?>;

                //Yearly visitors
                var $canvasesYearlyVisitors = jQuery('.canvas-chart-line-yearly-visitors');
                if ($canvasesYearlyVisitors.length) {
                    $canvasesYearlyVisitors.each(function(i){
                        var config = {
                            type: 'line',
                            data: {
                                labels: DAYS,
                                datasets: [{
                                    label: "Daily sales",
                                    backgroundColor: 'rgba(77, 177, 158, 0.5)',
                                    borderColor: 'rgba(77, 177, 158, 0.5)',
                                    borderWidth: '0',
                                    //point options
                                    pointBorderColor: "transparent",
                                    pointBackgroundColor: "rgba(77, 177, 158, 1)",
                                    pointBorderWidth: 0,
                                    tension: '0',
                                    //visitors per month
                                    //data: [300, 250, 480, 500, 300, 700, 900],
                                    data : <?php  echo json_encode($revbulan); ?>,
                                    fill: true,
                                }, 
                                //put new dataset here if needed to show multiple datasets on one graph
                                ]
                            },
                            options: {
                                chartArea: {
                                    backgroundColor: 'rgba(100, 100, 100, 0.02)',
                                },
                                
                            }
                        };


                        var canvas = jQuery(this)[0].getContext("2d");;
                        new Chart(canvas, config);
                    });
                } //Yearly visitors

                //Monthly visitors
                var $canvasesMonthlyVisitors = jQuery('.canvas-chart-line-monthly-visitors');
                if ($canvasesMonthlyVisitors.length) {
                    $canvasesMonthlyVisitors.each(function(i){

                        var config = {
                            type: 'line',
                            data: {
                                labels: DAYS,
                                datasets: [{
                                    label: "Customer ",
                                    backgroundColor: 'rgba(236, 104, 46, 0.5)',
                                    borderColor: 'rgba(236, 104, 46, 0.5)',
                                    borderWidth: '0',

                                    //point options
                                    pointBorderColor: "transparent",
                                    pointBackgroundColor: "rgba(236, 104, 46, 1)",
                                    pointBorderWidth: 0,

                                    tension: '0',
                                    //visitors per month
                                    data: [
                                        
                                        446,
                                        243,
                                        544,
                                        645,
                                        443,
                                        437,
                                        750,

                                    ],
                                    fill: true,
                                }, 
                                //put new dataset here if needed to show multiple datasets on one graph
                                ]
                            },
                            options: {
                                chartArea: {
                                    backgroundColor: 'rgba(100, 100, 100, 0.02)',
                                },
                            }
                        };

                        var canvas = jQuery(this)[0].getContext("2d");;
                        new Chart(canvas, config);
                    });
                } //Monthly visitors

                // //Monthly visitors with sells
                // var $canvasesMonthlyVisitors = jQuery('.canvas-chart-line-visitors-sels');
                // if ($canvasesMonthlyVisitors.length) {
                //     $canvasesMonthlyVisitors.each(function(i){

                //         var config = {
                //             type: 'line',
                //             data: {
                //                 labels: DAYS,
                //                 datasets: [{
                //                     label: "Visitors",
                //                     backgroundColor: 'rgba(236, 104, 46, 0.5)',
                //                     borderColor: 'rgba(236, 104, 46, 0.5)',
                //                     borderWidth: 3,

                //                     //point options
                //                     pointBorderColor: "transparent",
                //                     pointBackgroundColor: "rgba(236, 104, 46, 1)",
                //                     pointBorderWidth: 0,

                //                     tension: '0',
                //                     //visitors per month
                //                     data: [
                                        
                //                         46,
                //                         43,
                //                         34,
                //                         45,
                //                         43,
                //                         37,
                //                         50,

                //                     ],
                //                     fill: false,
                //                 }, 
                //                 {
                //                     label: "Sales",
                //                     backgroundColor: 'rgba(111, 211, 227, 0.6)',
                //                     borderColor: 'rgba(91, 173, 186, 0.5)',
                //                     borderWidth: 3,

                //                     //point options
                //                     pointBorderColor: "transparent",
                //                     pointBackgroundColor: "rgba(111, 211, 227, 1)",
                //                     pointBorderWidth: 0,

                //                     tension: '0',
                //                     //sells per month
                //                     data: [
                                        
                //                         2000000,
                //                         1500000,
                //                         500000,
                //                         650000,
                //                         3000000,
                //                         2650000,
                //                         750000,

                //                     ],
                //                     fill: false,
                //                 }, 
                //                 //put new dataset here if needed to show multiple datasets on one graph
                //                 ]
                //             },
                //             options: {
                //                 chartArea: {
                //                     backgroundColor: 'rgba(100, 100, 100, 0.02)',
                //                 },
                //             }
                //         };

                //         var canvas = jQuery(this)[0].getContext("2d");;
                //         new Chart(canvas, config);
                //     });
                // } //Monthly visitors


                // //Monthly Conversion
                // var $canvasesMonthlyConversion = jQuery('.canvas-chart-line-conversions');
                // if ($canvasesMonthlyConversion.length) {
                //     $canvasesMonthlyConversion.each(function(i){

                //         var config = {
                //             type: 'line',
                //             data: {
                //                 labels: DAYS,
                //                 datasets: [{
                //                     label: "Conversion",
                //                     backgroundColor: 'rgba(111, 211, 227, 0.6)',
                //                     borderColor: 'rgba(91, 173, 186, 0.5)',
                //                     borderWidth: '2',
                                    
                //                     //point options
                //                     pointBorderColor: "transparent",
                //                     pointBackgroundColor: "rgba(111, 211, 227, 1)",
                //                     pointBorderWidth: 0,

                //                     //Conversion per month
                //                     data: [
                                        
                //                         1.2,
                //                         1.5,
                //                         1.3,
                //                         1.9,
                //                         2,
                //                         2.6,
                //                         2.4,

                //                     ],
                //                     fill: true,
                //                 }, 
                //                 //put new dataset here if needed to show multiple datasets on one graph
                //                 ]
                //             },
                //             options: {
                //                 chartArea: {
                //                     backgroundColor: 'rgba(100, 100, 100, 0.02)',
                //                 },
                //             }
                //         };

                //         var canvas = jQuery(this)[0].getContext("2d");;
                //         new Chart(canvas, config);
                //     });
                // } //Monthly Conversion


                // //Monthly Returned Visitors - Not used at the moment
                // var $canvasesMonthlyPieVisitors = jQuery('.canvas-chart-pie-visitors');
                // if ($canvasesMonthlyPieVisitors.length) {
                //     $canvasesMonthlyPieVisitors.each(function(i){

                //         var config = {
                //             type: 'pie',
                //             data: {
                //                 datasets: [{
                //                     data: [
                //                         76,
                //                         24,
                //                     ],
                //                     backgroundColor: [
                //                         'rgba(236, 104, 46, 0.5)',
                //                         'rgba(236, 104, 46, 0.7)'
                //                     ],
                //                     label: 'Monthly Visitors'
                //                 }],
                //                 labels: [
                //                     "New Visitors",
                //                     "Returned Visitors",
                //                 ]
                //             },
                //         };

                //         var canvas = jQuery(this)[0].getContext("2d");;
                //         new Chart(canvas, config);
                //     });
                // } //Monthly Returned Visitors

                // //end of Charts

                // //////////////
                // //vector map//
                // //////////////
                // //http://jvectormap.com/
                // jQuery('.world_map').vectorMap({
                //     map: 'world_mill',
                //     backgroundColor: "transparent",
                //     zoomOnScroll: false,
                //     regionsSelectable: true,
                //     regionsSelectableOne: true,
                //     markerSelectable: true,
                //     markerSelectableOne: true,
                //     regionStyle: {
                //         initial: {
                //             "fill": '#e9eaeb',
                //             "fill-opacity": 0.9,
                //             "stroke": 'none',
                //             "stroke-width": 0,
                //             "stroke-opacity": 0
                //         },
                //         selected: {
                //             fill: '#eeb269'
                //         },
                //     },
                //     markerStyle: {
                //         initial: {
                //             fill: '#dc5753',
                //             stroke: 'transparent',
                //             "stroke-width": 0,
                //         },
                //         hover: {
                //             stroke: '#dc5753',
                //             "stroke-width": 2,
                //             cursor: 'pointer'
                //         },
                //         selected: {
                //             "fill-opacity": 0.5
                //         },
                //         selectedHover: {
                //         }
                //     },

                //     selectedRegions: ['GB'],

                //     series: {
                //         regions: [{
                //             values: {
                //                 "AU": 1,
                //                 "BR": 2,
                //                 "CA": 5,
                //                 "CN": 100,
                //                 "TR": 1,
                //                 "IN": 1,
                //                 "GB": 1,
                //                 "US": 100,

                //             },
                //             scale: ["#007ebd", "#4db19e"],
                //             normalizeFunction: 'polynomial'
                //         }]
                //     },
                //     markers: [
                //         {latLng: [41.90, 12.45], name: 'Vatican City'},
                //         {latLng: [3.2, 73.22], name: 'Maldives'},
                //     ],
                // });

                // ///////////////////
                // //events calendar//
                // ///////////////////
                // //https://fullcalendar.io/
                // jQuery('.events_calendar').fullCalendar(

                //     {
                //         header: {
                //             left: 'prev,next today',
                //             center: 'title',
                //             right: 'month,agendaWeek,agendaDay,listWeek'
                //         },
                //         defaultDate: '2017-03-12',
                //         editable: true,
                //         eventLimit: true, // allow "more" link when too many events
                //         navLinks: true,
                //         aspectRatio: 1,
                //         events: [
                //             {
                //                 title: 'All Day Event',
                //                 start: '2017-03-01'
                //             },
                //             {
                //                 title: 'Long Event',
                //                 start: '2017-03-07',
                //                 end: '2017-03-10'
                //             },
                //             {
                //                 id: 999,
                //                 title: 'Repeating Event',
                //                 start: '2017-03-09T16:00:00'
                //             },
                //             {
                //                 id: 999,
                //                 title: 'Repeating Event',
                //                 start: '2017-03-16T16:00:00'
                //             },
                //             {
                //                 title: 'Conference',
                //                 start: '2017-03-11',
                //                 end: '2017-03-13'
                //             },
                //             {
                //                 title: 'Meeting',
                //                 start: '2017-03-12T10:30:00',
                //                 end: '2017-03-12T12:30:00'
                //             },
                //             {
                //                 title: 'Lunch',
                //                 start: '2017-03-12T12:00:00'
                //             },
                //             {
                //                 title: 'Meeting',
                //                 start: '2017-03-12T14:30:00'
                //             },
                //             {
                //                 title: 'Happy Hour',
                //                 start: '2017-03-12T17:30:00'
                //             },
                //             {
                //                 title: 'Dinner',
                //                 start: '2017-03-12T20:00:00'
                //             },
                //             {
                //                 title: 'Birthday Party',
                //                 start: '2017-03-13T07:00:00'
                //             },
                //             {
                //                 title: 'Click for Google',
                //                 url: 'http://google.com/',
                //                 start: '2017-03-28'
                //             }
                //         ]
                //     }

                // );

                /////////////////////
                //date range picker//
                /////////////////////
                //http://www.daterangepicker.com/
                (function() {
                    var start = moment().subtract(29, 'days');
                    var end = moment();

                    function cb(start, end) {
                        jQuery('.dashboard-daterangepicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }

                    jQuery('.dashboard-daterangepicker').daterangepicker({
                        "startDate": start,
                        "endDate": end,
                        "autoApply": true,
                        "linkedCalendars": false,
                        "showCustomRangeLabel": false,
                        "alwaysShowCalendars": true,
                        "ranges": {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        
                        },
                    }, cb);

                    cb(start, end);
                })();

                /////////////
                //sparkline//
                /////////////
                //http://omnipotent.net/jquery.sparkline/
                jQuery('.sparklines').each(function(){
                    //sparkline type: 'line' (default), 'bar', 'tristate', 'discrete', 'bullet', 'pie', 'box'
                    var $this = jQuery(this);
                    var data = $this.data();
                    
                    var type = data.type ? data.type : 'bar';
                    var lineColor = data.lineColor ? data.lineColor : '#4db19e';
                    var negBarColor = data.negColor ? data.negColor : '#dc5753';
                    var barWidth = data.barWidth ? data.barWidth : 4;
                    var height = data.height ? data.height : false;
                    
                    var values = data.values ? JSON.parse("[" + data.values + "]") : false;
                    
                    $this.sparkline(values, {
                        type: type,
                        lineColor: lineColor,
                        barColor: lineColor,
                        negBarColor: negBarColor,
                        barWidth: barWidth,
                        height: height,
                    });
                });


                jQuery('.js-date').datepicker();



            })();

    </script>

</body>

</html>