
    <!-- template init -->
    <script src="/assets/js/jquery-3.4.1.min.js"></script>
    <script src="/assets/js/compressed.js"></script>
    <script src="/assets/js/main.js"></script>
    <!-- dashboard init -->
    <script src="/assets/js/admin.js"></script>
    <!-- bootstrap date init -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="https://dev.ivetdata.com//assets/js/jquery.invoice.js"></script>
    
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ad8cbhz7hy8flomantnx8ehdj6a4lpjuuqs226xxs9yytcjy"></script>
    <script>tinymce.init({ selector:'textarea.tiny', menubar: false, });</script> 

    
<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">        
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb darklinks">
                        <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                        <li><a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $rfid; ?>">Pet Detail</a></li>
                        <li class="active">Vaccine Record</li>
                    </ol>
                    </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
        </section> 

        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                            <h3>Update Vaccine History  <?php echo $namepet ?></h3>
                            
                        </div>
                    </div>
                    <!-- .row -->
                    <div class="row" id="DivIdToPrint">
                        <div class="col-md-12">
                            <div class="row">
                                <!-- User Statistics -->
                                <div class="col-xs-12 col-md-4">                                    
                                    <div class="with_border with_padding">
                                        <h3>Pet Info</h3>
                                        <img src="<?php echo $petphoto ?>" style="max-width:400px;width: 100%;">
                                        <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey"> Pet ID : </strong><?php echo $rfid; ?> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-flag"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name : </strong> <?php echo $namepet; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="fa fa-briefcase"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Type : </strong> <?php echo $tipe; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="fa fa-arrows-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Breed  </strong> <?php echo $breed; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-paint-brush"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Color  </strong> <?php echo $color; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-signal"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Age  </strong> <?php echo $age; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                </ul>
                                                <div class="">
                                                <?php if($edit == '1'){ ?>
                                                <h3>Owner Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <?php foreach ($dataowner as $owner) { ?>                                                       
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name  </strong> <?php echo $owner->nama ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-phone"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Phone  </strong> <?php echo $owner->nohp ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="rt-icon2-mail"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Email  </strong> <?php echo $owner->email ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="rt-icon2-pin-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Address  </strong> <?php echo $owner->address ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">City  </strong> <?php echo $owner->city ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Country  </strong> <?php echo $owner->country ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } ?>                                                    
                                                </ul>
                                            <?php }else{ ?>
                                                <!-- <h3>Clinic Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name Clinic  </strong> Name Clinic
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul> -->
                                            <?php } ?>
                                            </div>
                                    </div>
                                    <!-- .with_border -->
                                    
                                </div>
                                <!-- col-* -->
                                <!-- User Info -->
                                <div class="col-xs-12 col-md-8">
                                    <div class="with_border with_padding">
                                        <div class="row">
                                            <form action="<?php echo base_url() ?>pet/updatevacc" method="post" enctype="multipart/form-data">
                                            <div class="col-xs-12 col-md-12">
                                                <h3>Vaccine History Detail</h3>
                                                <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Description</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>
                                                <?php foreach ($datavacc as $log) {?>
                                                <tbody>
                                                    <tr>
                                                        <td>Date Vaccine*</td>
                                                        <td>
                                                            <div class="input-group date" data-provide="datepicker">
                                                                <input type="text" class="js-date form-control" value="<?php echo date('m/d/Y',strtotime($log->datevacc)); ?>" name="datevacc" required>                                                                
                                                                <div class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-th"></span>
                                                                </div>
                                                            </div>                                                           
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vets</td>
                                                        <td><input type="text" class="form-control" name="vetname" value="<?php echo $log->vetname; ?>" >
                                                        <input type="hidden" name="idvacc" value="<?php echo $log->idvacc ?>">
                                                                <input type="hidden" name="idpet" value="<?php echo $log->idpet ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vaccine</td>
                                                        <td>
														<table class="table table-bordered">
														<?php
														    $i = 0;
														    if($log->vaccine==""){ 
                                                                    $value=array();
                                                                    $value[0]="-";
														      }else{
															        $value = explode("<br>",$log->vaccine);
														      }
															foreach($value as $val){
														?>
														<tr class="item-row">
														<td class="item-name" width="80%"><input type="text" class="chooseservice form-control item" placeholder="Treatment" name="vaccine[]" autocomplete="off" value="<?php echo $val; ?>">
														<?php if ($i>0){ echo '<a class="delete" href="javascript:;" title="Remove row" style="position: relative;top: -48px;font-weight: bold;color: red;">X</a>'; } ?></td>
														<!--<td class="item-name"><a id="addRow" href="javascript:;" title="Add Item" class="icon-tab theme_button color2" style="color: #fff;">Add Item</a>-->
														<input name="qty[]" type="hidden" value="1"><input name="price[]" type="hidden" value="0"> </td>
														</tr>
														<?php
														        $i++;    
															}
														?></table>
														</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Injection Site</td>
                                                        <td><?php
                                                        if($log->description == ''){
                                                            echo '<textarea class="form-control tiny" name="diagnose"required>-</textarea>';
                                                        }else{
                                                            echo '<textarea class="form-control tiny" name="diagnose" required>'.$log->description.'</textarea>' ;
                                                        }?></td>                                                        
                                                    </tr>
                                                     <?php
                                                        $nextdate = $log->nextdate;                                                                                                
                                                        if($nextdate == '0000-00-00 00:00:00'){
                                                            $fixdate = date('Y-m-d H:i:s');
                                                        }else{
                                                            $fixdate = $nextdate;
                                                        } ?>
                                                    <tr>
                                                        <td>Next Date </td>
                                                        <td>
                                                            <div class="input-group date" data-provide="datepicker">
                                                            <input type="text" class="js-date form-control" value="<?php echo date('m/d/Y',strtotime($fixdate)); ?>" name="nextdate" required>
                                                            <div class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-th"></span>
                                                            </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <?php } ?>
                                            </table>  
                                            * Mandatory, must be filled. 
                                            <input type="submit" value="Update" class="icon-tab theme_button color4" name="submit" style="margin: 30px 0px 0px;float: right;">                                            
                                            </div>
                                        </form>
                                        </div>

                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- col-* -->
                            </div>
                            <!-- .row -->
                            
                        </div>
                        <!-- .col-* left column -->

                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>  
    </div>
</div>



<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->

    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#breed').autocomplete({
                source: "<?php echo site_url('pet/breedlist');?>",
      
                select: function (event, ui) {
                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });
            //jQuery(".js-date").datepicker();
 
        });
    </script>
    <script>
    (function (jQuery) {

    $.opt = {};  // jQuery Object

    

    jQuery.fn.invoice = function (options) {
        var ops = jQuery.extend({}, jQuery.fn.invoice.defaults, options);
        $.opt = ops;

        var inv = new Invoice();
        inv.init();

        jQuery('body').on('click', function (e) {
            var cur = e.target.id || e.target.className;

            if (cur == $.opt.addRow.substring(1))
                inv.newRow();

            if (cur == $.opt.delete.substring(1))
                inv.deleteRow(e.target);

            inv.init();
        });

        jQuery('body').on('keyup', function (e) {
            inv.init();
        });

        return this;
    };
}(jQuery));

function Invoice() {
    self = this;
}

Invoice.prototype = {
    constructor: Invoice,

    init: function () {
        this.calcTotal();
        this.calcTotalQty();
        this.calcSubtotal();
        this.calcTaxpersen();
        //this.calcTax();
        this.calcGrandTotal();        
    },

    /**
     * Calculate total price of an item.
     *
     * @returns {number}
     */
    calcTotal: function () {
         jQuery($.opt.parentClass).each(function (i) {
             var row = jQuery(this);
             var total = row.find($.opt.price).val() * row.find($.opt.qty).val();

             //total = self.roundNumber(total,0);

             row.find($.opt.total).html(total);
         });

         return 1;
     },
    
    /***
     * Calculate total quantity of an order.
     *
     * @returns {number}
     */
    calcTotalQty: function () {
         var totalQty = 0;
         jQuery($.opt.qty).each(function (i) {
             var qty = jQuery(this).val();
             if (!isNaN(qty)) totalQty += Number(qty);
         });

         //totalQty = self.roundNumber(totalQty, 0);

         jQuery($.opt.totalQty).html(totalQty);
         jQuery($.opt.allQty).val(totalQty);

         return 1;
     },

    /***
     * Calculate subtotal of an order.
     *
     * @returns {number}
     */
    calcSubtotal: function () {
         var subtotal = 0;
         jQuery($.opt.total).each(function (i) {
             var total = jQuery(this).html();
             if (!isNaN(total)) subtotal += Number(total);
         });

         //subtotal = self.roundNumber(subtotal, 2);

         jQuery($.opt.subtotal).html(subtotal);

         return 1;
     },

    /**
     * Calculate grand total of an order.
     *
     * @returns {number}
     */  

    calcTaxpersen: function () {
       $('#taxpersen').keyup(function(){
          var persen = $('#taxpersen').val();
          var perseratus = persen/100;
          //$('#result').text($('#shares').val() * 1.5);
          //grandTotal = self.roundNumber(grandTotal, 2);
          var grandTotal3 = Number(jQuery($.opt.subtotal).html()) - Number(jQuery($.opt.discount).val());
          var valuepersen = grandTotal3 * perseratus;
          var totalpricefix = grandTotal3 + valuepersen;
          $('#tax').html(valuepersen);
          $('#taxsave').val(valuepersen);          
          // $('#grandTotal').html(totalpricefix);
          // $('#totalprice').val(totalpricefix);
          jQuery($.opt.grandTotal).html(totalpricefix);
          jQuery($.opt.totalPrice).val(totalpricefix);
          console.log(totalpricefix); 
          return 1;
        });    

        return 1;
    },  

    // calcTax: function () {
    //     var persen = 10/100;
    //     var Tax = Number(jQuery($.opt.subtotal).html()) * Number(persen);
    //                    ///+ Number(jQuery($.opt.shipping).val())
                      
    //     //grandTotal = self.roundNumber(grandTotal, 2);
    //     var grandTotal2 = Number(jQuery($.opt.subtotal).html()) + Number(jQuery($.opt.Tax).html());
    //     jQuery($.opt.Tax).html(Tax);
    //     jQuery($.opt.grandTotal).html(grandTotal2);
    //     jQuery($.opt.totalPrice).val(grandTotal2);
    //     //console.log(Tax);     

    //     return 1;
    // },

    calcGrandTotal: function () {
        var grandTotal = Number(jQuery($.opt.subtotal).html()) - Number(jQuery($.opt.discount).val()) + Number(jQuery($.opt.Tax).html());

        jQuery($.opt.grandTotal).html(grandTotal);
        jQuery($.opt.totalPrice).val(grandTotal);

        return 1;
    },

    /**
     * Add a row.
     *
     * @returns {number}
     */
    newRow: function () {
        jQuery(".item-row:last").after('<tr class="item-row"><td class="item-name"><input type="text" class="chooseservice form-control item" placeholder="Item" type="text" name="treatment[]"><a class=' + $.opt.delete.substring(1) + ' href="javascript:;" title="Remove row" style="position: relative;top: -48px;font-weight: bold;color: red;">X</a></td><td><a id="addRow" href="javascript:;" title="Add Item" class="icon-tab theme_button color2" style="color: #fff;">Add Item</a><input class="form-control qty" name="qty[]" type="hidden"><input class="form-control price" name="price[]" type="hidden" required> </td></tr>');
        
        $("#discount").val('0');
        $("#taxpersen").val('');
        $("#tax").html('');

        if (jQuery($.opt.delete).length > 0) {
            jQuery($.opt.delete).show();
        }

        $('.chooseservice').autocomplete({

                source: "<?php echo site_url('service/allsearch/?');?>",                      
                select: function (event, ui) {
                    var row = $(this).closest('tr');
                    var price = row.find('.price');
                    var qty = row.find('.qty');

                    //$('[name="breed"]').val(ui.item.nameservice); 
                   //price.val(ui.item.price);
                    var harga = ui.item.price;
                    price.val(ui.item.description);
                    qty.val('1');
                    //console.log(harga);
                    //price.val(ui.item.label);                     
                }                
            });

        return 1;
    },

    /**
     * Delete a row.
     *
     * @param elem   current element
     * @returns {number}
     */
    deleteRow: function (elem) {
        jQuery(elem).parents($.opt.parentClass).remove();

        if (jQuery($.opt.delete).length < 1) {
            jQuery($.opt.delete).hide();
        }

        return 1;
    },

    /**
     * Round a number.
     * Using: http://www.mediacollege.com/internet/javascript/number/round.html
     *
     * @param number
     * @param decimals
     * @returns {*}
     */
    roundNumber: function (number, decimals) {
        var newString;// The new rounded number
        decimals = Number(decimals);

        if (decimals < 1) {
            newString = (Math.round(number)).toString();
        } else {
            var numString = number.toString();

            if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
                numString += ".";// give it one at the end
            }

            var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
            var d1 = Number(numString.substring(cutoff, cutoff + 1));// The value of the last decimal place that we'll end up with
            var d2 = Number(numString.substring(cutoff + 1, cutoff + 2));// The next decimal, after the last one we want

            if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
                if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
                    while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
                        if (d1 != ".") {
                            cutoff -= 1;
                            d1 = Number(numString.substring(cutoff, cutoff + 1));
                        } else {
                            cutoff -= 1;
                        }
                    }
                }

                d1 += 1;
            }

            if (d1 == 10) {
                numString = numString.substring(0, numString.lastIndexOf("."));
                var roundedNum = Number(numString) + 1;
                newString = roundedNum.toString() + '.';
            } else {
                newString = numString.substring(0, cutoff) + d1.toString();
            }
        }

        if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
            newString += ".";
        }

        var decs = (newString.substring(newString.lastIndexOf(".") + 1)).length;

        for (var i = 0; i < decimals - decs; i++)
            newString += "0";
        //var newNumber = Number(newString);// make it a number if you like

        return newString; // Output the result to the form field (change for your purposes)
    }
};

/**
 *  Publicly accessible defaults.
 */
jQuery.fn.invoice.defaults = {
    addRow: "#addRow",
    delete: ".delete",
    parentClass: ".item-row",

    price: ".price",
    qty: ".qty",
    total: ".total",
    totalQty: "#totalQty",

    subtotal: "#subtotal",
    discount: "#discount",
    shipping: "#shipping",
    grandTotal: "#grandTotal",
    totalPrice : "#totalPrice",
    allQty : "#allQty",
    Taxpersen: "#taxpersen",
    Tax:"#tax",
};
    </script>    

    <script>
        $(document).ready(function(){
        
            $().invoice({
                addRow : "#addRow",
                delete : ".delete",
                parentClass : ".item-row",

                price : ".price",
                qty : ".qty",
                total : ".total",
                totalQty: "#totalQty",

                subtotal : "#subtotal",
                discount: "#discount",
                shipping : "#shipping",
                grandTotal : "#grandTotal",
                totalPrice : "#totalPrice",
                allQty : "#allQty",
                Taxpersen: "#taxpersen",
                Tax:"#tax",
            }); 

            $('input.chooseservice').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('service/allsearch/?');?>",
      
                select: function (event, ui) {
                    var row = $(this).closest('tr');

                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description);
                    //price.val(ui.item.label);                     
                }                
            });

            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
					jQuery(".petdata").empty();
					jQuery(".petdata").append('<option value="0">---------- Please select ----------</option>');
					for (var i=0;i<ui.item.petdata.length; i++) {
						jQuery(".petdata").append('<option value="' + ui.item.petdata[i].idpet + '">' + ui.item.petdata[i].namapet + '</option>');
					}
                    //price.val(ui.item.label);                     
                }                
            });           
        });
    </script>   

</body>

</html>

