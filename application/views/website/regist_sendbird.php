<?php
	foreach ($data as $key => $value) {
		$user_id = $value->uid;
		$email = $value->email;
		$nickname = $value->name;
		$photo = $value->photo;
		if ($value->role == '2') {
			$role = 'Admin (CS)';
		}elseif ($value->role == '1') {
			$role = 'Vet';
		} else{
			$role = 'Pet Owner';
		}
		$data_regist = array(
			'user_id' => $user_id,
			'nickname' => $nickname,
			'profile_url' => $photo,
			'metadata' => array(
				'email' => $email,
				'role' => $role,
			),
		);

		$data = json_encode($data_regist);
		$url = "https://api-3A0A0751-6F1C-43F0-934B-6DEDBDD61009.sendbird.com/v3/users";
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					    "Api-Token: 03ae920eca433f3dd7636eebf658478648144ad1",
					    "Content-Type: application/json",
					    'Content-Length: ' . strlen($data)
					  ));
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    /* execute request */
	    $result = curl_exec($ch);
	    $result = json_decode($result);

		if (curl_errno($ch)) {
		    $error_msg = curl_error($ch);
		    $res = array(
		    	'status_code' => '404',
		    	'error_message' => $error_msg
		    );
		} else {
			if (isset($result->error)) {
				$res = array(
					'status_code' => '500',
					'error_message' => $result->message,
					'user_id' => $data_regist['user_id'],
					'nickname' => $data_regist['nickname'],
					'profile_url' => $data_regist['profile_url']
				);
			} else {
				$res = array(
					'status_code' => '200',
					'user_id' => $result->user_id,
					'nickname' => $result->nickname,
					'profile_url' => $result->profile_url
				);
			}
		}

		echo "<pre>";
		print_r($res);
		echo "</pre>";
	}
