<section class="ls ms section_404 background_cover section_padding_top_100 section_padding_bottom_130">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<div class="inline-block text-center">
								<h3>Thank you, your message will be replied soon.</h3>
								<a href="<?php echo base_url() ?>" class="theme_button color3 wide_button">Back to Dashboard</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>