<!--<body>
            <div role="main" class="main">
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">About Us</h1>
						</div>
					</div>
				</div>
			</section>-->
			<!-- start: page -->
			<section id="news" class="section bg-color-quaternary border-0 my-0">
					<div class="container">
					    <div class="row mb-4">
							<div class="col" id="contact" >
								<br /><h2 class="text-center mb-3 appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" style="animation-delay: 100ms; font-family: 'Roboto Slab', serif; font-size:2.7em;">WHAT'S NEW?</h2>
							</div>
				        </div>
						<div class="row justify-content-center">
						    <?php foreach($articlehome as $row){  ?>
							<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
								<article class="thumb-info thumb-info-hide-wrapper-bg border-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="600">
									<div class="thumb-info-wrapper m-0">
										<a href="news/detail/<?php echo $row->slug;  ?>"><img src="<?php echo $row->featuredimg;  ?>" alt=""></a>
									</div>
									<div class="thumb-info-caption custom-padding-4 d-block">
										<span class="text-color-primary font-weight-semibold d-block mb-2"><?php echo date("d M Y",strtotime($row->publishdate));   ?></span>
										<h3 class="custom-primary-font text-transform-none text-5 mb-3"><a href="news/detail/<?php echo $row->slug;  ?>" class="text-decoration-none custom-link-style-1">
										    <?php 
										        echo substr($row->title,0,40);  
										        if(strlen($row->title)>40) echo "<span style='white-space:nowrap;'> . . .</span>";
										    ?></a></h3>
										<span class="thumb-info-caption-text text-3 p-0 m-0"><?php  echo substr(strip_tags($row->description),0,200); if(strlen($row->description)>200) echo "<span style='white-space:nowrap;'> . . .</span>";?></span>
									</div>
								</article>
							</div>
							<?php   }  ?>
						</div>
						<div class="row text-center mt-5">
							<div class="col">
								<a href="/news/all" class="btn btn-outline btn-secondary font-weight-bold custom-btn-style-1 text-2">VIEW ALL</a>
							</div>
						</div>
					</div>
				</section>
			<section id="about" class="section bg-color-light border-0 my-0 mb-0 pb-0">
				<div class="container">
				    <div class="row">
                        <div class="col-12 col-md-6 col-lg-6 pl-5 pr-5">
							 <h2>
				                <img src="<?php echo base_url() ?>/new/images/about_us-header.jpg" alt="About iVet Data"/>
				                </h2>
							<p style="text-align: justify;"> PT. IVET DATA GLOBAL (est. 2018) is headquartered in Jakarta, Indonesia as a pioneer in Pet Database and Clinic CRM in the region, endorsed by Indonesian Veterinary Medical Association (PDHI) in Indonesia.
                            We are also the first company to partner with Datamars SA to introduce country-coded microchips which follow ICAR (the International Committee for Animal Recording) ISO11784/11785 standards.
                            Formed to accommodate the growing demand for digital solutions in veterinary practice and to support the new Government regulation on pet’s "National ID” identification, we believe in providing new technology for Pet Owners and Vets that are comprehensive, affordable, and secure. </p>
                        </div>
				        <div class="col-12 col-md-6 col-lg-6"><img src="<?php echo base_url() ?>/new/images/feature.jpg" width="100%" border="0">
				        </div>
				    </div>
				</div>
			</section>
			<?php
                    $this->db->select('*');
                    $this->db->where('status','1');
                    $this->db->where('role>','0');
                    $vets = $this->db->get('tbl_member')->num_rows();
                    $this->db->select('*');
                    $this->db->where('status','1');
                    $this->db->where('role','0');
                    $po = $this->db->get('tbl_member')->num_rows();
                    $this->db->select('*');
                    $this->db->where('status','0');
                    $pet = $this->db->get('tbl_pet')->num_rows();
                    $this->db->select('*');
                    $this->db->where('status','0');
                    $this->db->where('microchip','0');
                    $this->db->where('length(rfid)>','9');
                    $rfid = $this->db->get('tbl_pet')->num_rows();
                ?>
			<!--
	            <section class="section text-color-dark custom-padding-3 border-0 my-0" style="background-color: #e8eef3;">
					<div class="container">
						<div class="row counters pb-2 mb-0">
							<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
								<div class="counter">
									<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo $po; ?>">0</strong>
									<label class="font-weight-normal custom-fontsize-5 px-5">Pet Owners<br>Registered</label>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3 mb-5 mb-lg-0">
								<div class="counter">
									<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo $vets; ?>">0</strong>
									<label class="font-weight-normal custom-fontsize-5 px-5">Vets<br>Registered</label>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3 mb-5 mb-sm-0">
								<div class="counter">
									<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo $pet; ?>" data-append="+">0</strong>
									<label class="font-weight-normal custom-fontsize-5 px-5">Pets<br>Registered</label>
								</div>
							</div>
							<div class="col-sm-6 col-lg-3">
								<div class="counter">
									<strong class="font-weight-normal text-color-dark custom-fontsize-4 mb-3" data-to="<?php echo $rfid; ?>">0</strong>
									<label class="font-weight-normal custom-fontsize-5 px-5">Microchips<br>Registered</label>
								</div>
							</div>
						</div>-->
						<!--<div class="row text-center">
							<div class="col">
								<a href="/login" class="btn btn-secondary font-weight-semibold custom-btn-style-1 text-4 py-3 px-5">JOIN NOW</a>
							</div>
						</div>-->
					<!--</div>
				</section>-->
				<section class="section bg-color-light border-0 my-0 mb-0">
					<div class="container">
						<div class="row appear-animation" data-appear-animation="fadeInLeftShorter" >
							<div class="col">
								<div class="owl-carousel owl-theme stage-margin custom-carousel-style-1 mb-0" data-plugin-options="{'items': 5, 'margin': 40, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 40}">
									
									<div>
										<img class="img-fluid" src="new/images/logo-pdhi3.png" alt="PDHI">
									</div>
									<div>
										<img class="img-fluid" src="new/images/logo-datamars.png" alt="Datamars">
									</div>
									<div>
										<img class="img-fluid" src="new/images/logo-indoconex.png" alt="Indoconex">
									</div>
									<div>
										<img class="img-fluid" src="new/images/logo-royalcanin.png" alt="Royal Canin">
									</div>
									<div>
										<img class="img-fluid" src="new/images/logo-xendit.png" alt="Xendit">
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>