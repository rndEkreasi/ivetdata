<!-- page main start -->
        <div class="page">

            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="text" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <figure><img src="<?php  echo base_url() ?>assets/img/logo.png" alt=""></figure> All Pets</a>
                </div>
                <!-- <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                    <a href="javascript:void(0)" class="menu-right"><i class="material-icons">person</i></a>
                </div> -->
            </header>
            <div class="page-content circle-background">
                <div class="content-sticky-footer">
                    <div class="row m-0">
                        <div class="col mt-3">
                            <form action="<?php echo base_url() ?>pet/search/" method="post">
                                <div class="input-group searchshadow">
                                    <input type="text" name="petid" class="form-control bg-white" placeholder="Find Pet id..." aria-label="">
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text "><i class="material-icons">search</i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                    
                    <?php 
                        $adapet = count($searchpet);
                        if($adapet == '0'){ ?>
                    
                    <h2 class="block-title color-dark">List Pet:</h2>
                    <div class="alert alert-danger" style="margin: 10px;">Sorry, Pet ID couldn't find. <a href="#" class="z2 mr-2 btn btn-primary rounded mb-2">Add pet ID</a></div>
                    <ul class="list-group">
                        <?php foreach ($datapet as $pet) { 
                                        $tipe = $pet->tipe;
                                        $photo = $pet->photo;
                                        
                                        if($photo =='' ){
                                            $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                        }else{
                                            $petphoto = $photo;
                                        }
                                    ?>
                        <li class="list-group-item">
                            <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>" class="media">
                                <div class="w-auto h-100 mr-3">
                                    <figure class="square-60"> <img src="<?php  echo $petphoto; ?>" alt=""></figure>
                                </div>
                                <div class="media-body">
                                    <h5><?php echo $pet->namapet; ?></h5>
                                    <p><span><?php echo $pet->tipe; ?></span></p>
                                </div>
                                <button class="like-heart color-red">
                                    <i class="icon material-icons">favorite</i>
                                </button>
                            </a>
                        </li>
                        <?php } ?>                        
                    </ul>
                     <?php } else { ?>
                        
                        <h2 class="block-title color-dark">List Pet:</h2>
                        <div class="alert alert-success" style="margin: 10px;">Yap, this pet is found</div>
                        <ul class="list-group">
                            <?php foreach ($searchpet as $pet) { 
                                            $tipe = $pet->tipe;
                                            $photo = $pet->photo;
                                            
                                            if($photo =='' ){
                                                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            }else{
                                                $petphoto = $photo;
                                            }
                                        ?>
                            <li class="list-group-item">
                                <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>" class="media">
                                    <div class="w-auto h-100 mr-3">
                                        <figure class="square-60"> <img src="<?php  echo $petphoto; ?>" alt=""></figure>
                                    </div>
                                    <div class="media-body">
                                        <h5><?php echo $pet->namapet; ?></h5>
                                        <p><span><?php echo $pet->tipe; ?></span></p>
                                    </div>
                                    <button class="like-heart color-red">
                                        <i class="icon material-icons">favorite</i>
                                    </button>
                                </a>
                            </li>
                            <?php } ?>                        
                        </ul>

                     <?php }?>
                    <br>
                </div>
                
            </div>
        </div>
        <!-- page main ends -->
    </div>