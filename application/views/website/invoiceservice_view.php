<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">Create Invoice</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                           <!--   <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
    <form action="<?php echo base_url() ?>dashboard/buyprocess" method="post">
    <div class="container-fluid">
    	<div class="row">
            <div class="col-md-12">                
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6"></div>
                        <div class="col-md-6" style="text-align: right;">
                            Invoice To : <br>
                            <div class="row form-group" style="margin: 0px;">
                                <div class="col-md-4">Phone : </div>
                                <div class="col-md-8"><input type="text" name="nohp" readonly value="<?php echo $hpcustomer ?>" class="form-control choosecustomer" style="border: 1px solid #ccc;">
                                  <input type="hidden" name="uid" value="<?php echo $uid ?>">
                                </div>
                            </div>
                            <div class="row form-group" style="margin: 0px;">
                                <div class="col-md-4">Name : </div>
                                <div class="col-md-8"><input type="text" name="namecustomer" readonly value="<?php echo $namecustomer ?>" class="form-control" id="namecustomer"  required style="border: 1px solid #ccc;"></div>
                            </div>                            
                            <div class="row form-group" style="margin: 0px;">
                                <div class="col-md-4">Email : </div>
                                <div class="col-md-8"><input type="text" name="emailcustomer" readonly value="<?php echo $emailcustomer ?>" class="form-control" id="emailcustomer" style="border: 1px solid #ccc;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="teamprice">
                        <thead>
                            <tr class="item-row">
                                <th>Item</th>                                
                                <th>Month</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td><?php echo $nameservice ?> Subcription</td>
                          <td>
                            <select class="form-control qty" name="qty" required="" style="min-width: 150px;">
                              <option value="">Select subsciption</option>
                              <option value="3">3 Month</option>
                              <option value="6">6 Month</option>
                              <option value="12">12 Month</option>
                              <option value="24">24 Month</option>
                            </select>
                          </td>
                          <td>
                              Rp.<span class="price"><?php echo number_format($price) ?></span>
                              <input type="hidden" name="priceservice" id="priceservice" value="<?php echo $price ?>">
                              <input type="hidden" name="idservice" id="idservice" value="<?php echo $idservice ?>">
                          </td>
                          <td>Rp.<span id="totalnya"><?php echo number_format($price) ?></span>
                            <input type="hidden" name="uid[]" value="<?php echo $uid; ?>">
                          </td>
                        </tr>
                        <?php
                        if(count($allteam) > '0'){
                          foreach ($allteam as $team) { ?>
                            <tr class="team">
                            <td><?php echo $team->name ?> - <?php $manageto = $team->manageto;
                            if($manageto == '3'){
                              $pricestaff[] = '55000';
                              $pricestaffon = '55000';
                             echo 'Admin Staff - <a href="'.base_url().'team/all">Delete</a>';
                            }else{
                              $pricestaff[] = '100000';
                              $pricestaffon = '100000';
                             echo 'Vet Staff - <a href="'.base_url().'team/all">Delete</a>';
                            } ?>                          
                            </td>                          
                            <td></td>
                            <td>
                              Rp.<span class="price"><?php echo number_format($pricestaffon) ?></span> /month
                            </td>
                            <td> 
                              Rp.<span class="pricenya"><?php echo number_format($pricestaffon) ?></span>
                              <input type="hidden" name="pricestaff[]" id="pricestaff" class="pricestaff" value="<?php echo $pricestaffon; ?>">
                              <input type="hidden" name="uid[]" value="<?php echo $team->uid; ?>">
                            </td>
                          </tr>
                          <?php } 
                        }else{
                          $pricestaff[] = '0';
                        }?>
                        <!-- <tr id="hiderow">
                            <td colspan="4">
                                <a id="addRow" href="javascript:;" title="Add Item" class="icon-tab theme_button color2" style="color: #fff;">Add Item</a>
                                <a href="#" class="icon-tab theme_button color3" data-toggle="modal" data-target="#addservice">+ Add Service</a>                              
                            </td>
                        </tr> -->
                        <!-- Here should be the item row -->
                        <!--<tr class="item-row">
                            <td><input class="form-control item" placeholder="Item" type="text"></td>
                            <td><input class="form-control price" placeholder="Price" type="text"></td>
                            <td><input class="form-control qty" placeholder="Quantity" type="text"></td>
                            <td><span class="total">0.00</span></td>
                        </tr>-->
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-right"><strong>Sub Total</strong></td>
                            <td>Rp. <span id="subtotalnya">0.00</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-right"><strong>Additional Staff</strong></td>
                            <td>Rp. <span id="addstaff" class="sumstaff"><?php echo array_sum($pricestaff); ?></span>
                              <input type="hidden" name="addstaff" id="addstaffinp" value="<?php echo array_sum($pricestaff); ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <!-- <strong>Total Quantity: </strong><span id="totalQty" style="color: red; font-weight: bold">0</span> Units <input type="hidden" name="allqty" id="allQty"> -->
                            </td>
                            <td></td>
                            <td class="text-right"><strong>Grand Total</strong></td>
                            <td>Rp. <span id="grandTotalnya">0</span> <input type="hidden" name="totalpricenya" id="totalPricenya"> </td>
                        </tr>
                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-right"><strong>Discount Code</strong></td>
                            <td><span id="msg"></span><input class="form-control" id="discountcode" name="discount" placeholder="Masukan kode diskon"  type="text"></td>                            
                        </tr>
                        </tbody>
                    </table>  

                    <?php
                      require 'vendor/autoload.php';

                      $options['secret_api_key'] = $xenditkey;

                      $xenditPHPClient = new XenditClient\XenditPHPClient($options);

                      $response = $xenditPHPClient->getVirtualAccountBanks();
                      //print_r($response);
                    ?>                  
                </div>
                <div class="row form-group">
                    <div class="col-lg-12">
                        <label class="col-lg-8 control-label" style="text-align: right;">Pay Method:</label>
                        <div class="col-lg-4">
                            <select class="form-control" name="methode" required="">
                              <?php foreach ($response as $bank) { ?>
                                <option value="<?php echo $bank['code'] ?>"><?php echo $bank['name'] ?></option>
                              <?php } ?>
                            </select>
                        </div>
                    </div>
                    <!--<div class="col-lg-12">
                        <label class="col-lg-8 control-label" style="text-align: right;">Note:</label>
                        <div class="col-lg-4">
                            <textarea class="form-control" name="note"></textarea>
                        </div>
                    </div> -->
                </div>
                <div class="row form-group">
                    
                </div>
                <button type="submit"  id="footer_contact_form_submit" name="contact_submit" class="icon-tab theme_button color2" style="color: #fff;margin: 25px 0px;float: right;"> Buy Subscription</button>
            </div>
        </div>
    
    </div>
    </form>
</section>
<!-- Modal -->
<div class="modal fade" id="addservice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
       <form action="<?php echo base_url() ?>service/addprocess" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="exampleInputEmail1">Service Name :</label>
                <input type="text" class="form-control" id="judul" name="nameservice" required>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Price (Rp.) :</label>
               <input type="text" class="form-control" id="judul" name="price" required>
               <input type="hidden" name="cat" value="service">
            </div>
            <button type="submit" class="theme_button">Post</button>
        </form>
        </div>
      </div>
  </div>
</div>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; Copyrights <?php echo date('Y'); ?> PT. iVet Data Global. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal fade" id="noservice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Please insert your product and service</h5>
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button> -->
                    </div>
                    <div class="modal-body">                    
                        <p>Anda belum memiliki daftar produk dan service silahkan isikan listnya dahulu. klik +add service atau +add medicine disini</p>
                        <a href="<?php echo base_url() ?>service/all" class="theme_button">Tambahkan daftar produk dan service.</a>
                    </div>
                </div>
              </div>
            </div>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script>
    (function (jQuery) {

    $.opt = {};  // jQuery Object

    

    jQuery.fn.invoice = function (options) {
        var ops = jQuery.extend({}, jQuery.fn.invoice.defaults, options);
        $.opt = ops;

        var inv = new Invoice();
        inv.init();

        jQuery('body').on('click', function (e) {
            var cur = e.target.id || e.target.className;

            if (cur == $.opt.addRow.substring(1))
                inv.newRow();

            if (cur == $.opt.delete.substring(1))
                inv.deleteRow(e.target);

            inv.init();
        });

        jQuery('body').on('keyup', function (e) {
            inv.init();
        });

        return this;
    };
}(jQuery));

function Invoice() {
    self = this;
}

Invoice.prototype = {
    constructor: Invoice,

    init: function () {
        this.calcTotal();
        this.calcTotalQty();
        this.calcSubtotal();
        this.calcTaxpersen();
        //this.calcTax();
        this.calcGrandTotal();        
    },

    /**
     * Calculate total price of an item.
     *
     * @returns {number}
     */
    calcTotal: function () {
         jQuery($.opt.parentClass).each(function (i) {
             var row = jQuery(this);
             var total = row.find($.opt.price).val() * row.find($.opt.qty).val();

             //total = self.roundNumber(total,0);

             row.find($.opt.total).html(total);
         });

         return 1;
     },
    
    /***
     * Calculate total quantity of an order.
     *
     * @returns {number}
     */
    calcTotalQty: function () {
         var totalQty = 0;
         var sum = 0;
         jQuery($.opt.qty).each(function (i) {
             var qty = jQuery(this).val();
             if (!isNaN(qty)) totalQty += Number(qty);
         });

         
         jQuery("#teamprice .team").each(function() {
            var pricestaff = jQuery(this).find(".pricestaff").val(); // <- This works           
            var totalpricestaff = totalQty * pricestaff;
            jQuery(this).find(".pricenya").html(totalpricestaff);  
            sum += parseFloat(totalpricestaff);
            // jQuery('.pricestaff').each(function(){
            //   sum += parseFloat(this.value);
            // });
            // alert(sum);
            //sumstaff += parseFloat(totalpricestaff);          
            //alert(totalpricestaff);
        });
         
          
         var price2 = $('#priceservice').val(); 
         var addstaff = $('#addstaffinp').val();
         var totalfix = totalQty * price2; 
         var totalprice = totalfix + Number(sum);        
         //jQuery($.opt.total).html($totalprice);
         $('#addstaffinp').val(sum);
         $('#addstaff').html(sum);

         $('#totalnya').html(totalfix);
         $('#subtotalnya').html(totalfix);
         $('#grandTotalnya').html(totalprice);
         $('#totalPricenya').val(totalprice);

         // //totalQty = self.roundNumber(totalQty, 0);
         // jQuery($.opt.subtotal).html(totalfix);
         // jQuery($.opt.grandTotal).html(totalprice);
         // jQuery($.opt.totalPrice).val(totalprice);
         // jQuery($.opt.totalQty).html(totalQty);
         // jQuery($.opt.allQty).val(totalQty);
         // console.log(totalprice);

         return 1;
     },

    /***
     * Calcu//late subtotal of an order.
     *
     * @returns {number}
     */
    calcSubtotal: function () {
         var subtotal = 0;
         jQuery($.opt.total).each(function (i) {
             var total = jQuery(this).html();
             if (!isNaN(total)) subtotal += Number(total);
         });

         //subtotal = self.roundNumber(subtotal, 2);

         jQuery($.opt.subtotal).html(subtotal);

         return 1;
     },

    /**
     * Calculate grand total of an order.
     *
     * @returns {number}
     */  

    calcTaxpersen: function () {
       $('#taxpersen').keyup(function(){
          var persen = $('#taxpersen').val();
          var perseratus = persen/100;
          //$('#result').text($('#shares').val() * 1.5);
          //grandTotal = self.roundNumber(grandTotal, 2);
          var grandTotal3 = Number(jQuery($.opt.subtotal).html()) - Number(jQuery($.opt.discount).val());
          var valuepersen = grandTotal3 * perseratus;
          var totalpricefix = grandTotal3 + valuepersen;
          $('#tax').html(valuepersen);
          $('#taxsave').val(valuepersen);          
          // $('#grandTotal').html(totalpricefix);
          // $('#totalprice').val(totalpricefix);
          jQuery($.opt.grandTotal).html(totalpricefix);
          jQuery($.opt.totalPrice).val(totalpricefix);
          console.log(totalpricefix); 
          return 1;
        });    

        return 1;
    },  

    // calcTax: function () {
    //     var persen = 10/100;
    //     var Tax = Number(jQuery($.opt.subtotal).html()) * Number(persen);
    //                    ///+ Number(jQuery($.opt.shipping).val())
                      
    //     //grandTotal = self.roundNumber(grandTotal, 2);
    //     var grandTotal2 = Number(jQuery($.opt.subtotal).html()) + Number(jQuery($.opt.Tax).html());
    //     jQuery($.opt.Tax).html(Tax);
    //     jQuery($.opt.grandTotal).html(grandTotal2);
    //     jQuery($.opt.totalPrice).val(grandTotal2);
    //     //console.log(Tax);     

    //     return 1;
    // },

    calcGrandTotal: function () {
        var grandTotal = Number(jQuery($.opt.subtotal).html()) - Number(jQuery($.opt.discount).val()) + Number(jQuery($.opt.Tax).html());

        jQuery($.opt.grandTotal).html(grandTotal);
        jQuery($.opt.totalPrice).val(grandTotal);

        return 1;
    },

    /**
     * Add a row.
     *
     * @returns {number}
     */
    newRow: function () {
        jQuery(".item-row:last").after('<tr class="item-row"><td class="item-name"><div class="delete-btn"><input type="text" class="chooseservice form-control item " placeholder="Item" type="text" name="item[]"><a class=' + $.opt.delete.substring(1) + ' href="javascript:;" title="Remove row" style="position: relative;top: -48px;font-weight: bold;color: red;">X</a></div></td><td><input class="form-control qty" name="qty[]" placeholder="Quantity" type="text"></td><td class="item-name"><input class="form-control price" name="price[]" placeholder="Price" type="text"> </td><td><span class="total">0.00</span></td></tr>');
        
        $("#discount").val('0');
        $("#taxpersen").val('');
        $("#tax").html('');

        if (jQuery($.opt.delete).length > 0) {
            jQuery($.opt.delete).show();
        }

        $('.chooseservice').autocomplete({

                source: "<?php echo site_url('service/allsearch/?');?>",                      
                select: function (event, ui) {
                    var row = $(this).closest('tr');
                    var price = row.find('.price');
                    var qty = row.find('.qty');

                    //$('[name="breed"]').val(ui.item.nameservice); 
                   //price.val(ui.item.price);
                    var harga = ui.item.price;
                    price.val(ui.item.description);
                    qty.val('1');
                    //console.log(harga);
                    //price.val(ui.item.label);                     
                }                
            });

        return 1;
    },

    /**
     * Delete a row.
     *
     * @param elem   current element
     * @returns {number}
     */
    deleteRow: function (elem) {
        jQuery(elem).parents($.opt.parentClass).remove();

        if (jQuery($.opt.delete).length < 2) {
            jQuery($.opt.delete).hide();
        }

        return 1;
    },

    /**
     * Round a number.
     * Using: http://www.mediacollege.com/internet/javascript/number/round.html
     *
     * @param number
     * @param decimals
     * @returns {*}
     */
    roundNumber: function (number, decimals) {
        var newString;// The new rounded number
        decimals = Number(decimals);

        if (decimals < 1) {
            newString = (Math.round(number)).toString();
        } else {
            var numString = number.toString();

            if (numString.lastIndexOf(".") == -1) {// If there is no decimal point
                numString += ".";// give it one at the end
            }

            var cutoff = numString.lastIndexOf(".") + decimals;// The point at which to truncate the number
            var d1 = Number(numString.substring(cutoff, cutoff + 1));// The value of the last decimal place that we'll end up with
            var d2 = Number(numString.substring(cutoff + 1, cutoff + 2));// The next decimal, after the last one we want

            if (d2 >= 5) {// Do we need to round up at all? If not, the string will just be truncated
                if (d1 == 9 && cutoff > 0) {// If the last digit is 9, find a new cutoff point
                    while (cutoff > 0 && (d1 == 9 || isNaN(d1))) {
                        if (d1 != ".") {
                            cutoff -= 1;
                            d1 = Number(numString.substring(cutoff, cutoff + 1));
                        } else {
                            cutoff -= 1;
                        }
                    }
                }

                d1 += 1;
            }

            if (d1 == 10) {
                numString = numString.substring(0, numString.lastIndexOf("."));
                var roundedNum = Number(numString) + 1;
                newString = roundedNum.toString() + '.';
            } else {
                newString = numString.substring(0, cutoff) + d1.toString();
            }
        }

        if (newString.lastIndexOf(".") == -1) {// Do this again, to the new string
            newString += ".";
        }

        var decs = (newString.substring(newString.lastIndexOf(".") + 1)).length;

        for (var i = 0; i < decimals - decs; i++)
            newString += "0";
        //var newNumber = Number(newString);// make it a number if you like

        return newString; // Output the result to the form field (change for your purposes)
    }
};

/**
 *  Publicly accessible defaults.
 */
jQuery.fn.invoice.defaults = {
    addRow: "#addRow",
    delete: ".delete",
    parentClass: ".item-row",

    price: ".price",
    qty: ".qty",
    total: ".total",
    totalQty: "#totalQty",

    subtotal: "#subtotal",
    discount: "#discount",
    shipping: "#shipping",
    grandTotal: "#grandTotal",
    totalPrice : "#totalPrice",
    allQty : "#allQty",
    Taxpersen: "#taxpersen",
    Tax:"#tax",
};
    </script>    

    <script>
        $(document).ready(function(){
        
            $().invoice({
                addRow : "#addRow",
                delete : ".delete",
                parentClass : ".item-row",
                
                price : ".price",
                qty : ".qty",
                total : ".total",
                totalQty: "#totalQty",

                subtotal : "#subtotal",
                discount: "#discount",
                shipping : "#shipping",
                grandTotal : "#grandTotal",
                totalPrice : "#totalPrice",
                allQty : "#allQty",
                Taxpersen: "#taxpersen",
                Tax:"#tax",
            }); 

            $('input.chooseservice').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('service/allsearch/?');?>",
      
                select: function (event, ui) {
                    var row = $(this).closest('tr');

                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description);
                    //price.val(ui.item.label);                     
                }                
            });

            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    //price.val(ui.item.label);                     
                }                
            });   

            $('#discountcode').keyup(function(event) {
              // skip for arrow keys
                $('#msg').html('<span style="color: red;"><i class="fa fa-close"></i> Invalid</span>');
                $.ajax({
                       type: "POST",
                       url: "<?php echo base_url() ?>dashboard/cekcode",
                       dataType:'json',
                       cache : false,
                       data: {code: $('#discountcode').val()},
                       success: function(response) {
                        console.log(response); 
                        $('#msg').html('<span style="color: green;"><i class="fa fa-check"></i>Valid</span>');
                        //alert(response)

                        // if (response == true){
                        //     console.log(response);
                        //     $('#msg').html('<span style="color: green;">Valid</span>');
                        // }else {
                        //     console.log(response);
                        //     $('#msg').html('<span style="color:red;">Code Invalid</span>');
                        // }  
                    }
                });
            });
            //var price = $('#priceservice').attr("value");
             //$(".form-control.qty").change(function(){
              //   var qty = $('.form-control.qty').val();                
            //     var totalprice = qty * price;
             //    $("#totalnya").html(totalprice);
             //    console.log(totalprice);
             //});        
             $(".form-control.qty").on("change", function() {
                // Pure JS
                //var selectedVal = this.value;
                //var selectedText = this.options[this.selectedIndex].text;
            
                // jQuery
                //var selectedVal = $(this).find(':selected').val();
                //var selectedText = $(this).find(':selected').text();
                
                $("#totalnya").click();
                
            });
        });
    </script>

</body>

</html>