    <section class="page_copyright ds darkblue_bg_color">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                </div>
                <!-- <div class="col-sm-6 text-sm-right">
                    <p class="grey">PT. IVET DATA GLOBAL </p>
                </div> -->
            </div>
        </div>
    </section>



    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <?php /*
    <!-- dashboard libs -->

    events calendar 
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    range picker 
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    charts 
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    vector map 
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    small charts 
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>
    dashboard init
    */ ?>
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>
    <?php /*
    <!-- <script>
        jQuery(document).ready(function(){
            jQuery().invoice({
                addRow : "#addRow",
                delete : ".delete",
                parentClass : ".item-row",

                price : ".price",
                qty : ".qty",
                total : ".total",
                totalQty: "#totalQty",

                subtotal : "#subtotal",
                discount: "#discount",
                shipping : "#shipping",
                grandTotal : "#grandTotal"
            });
            jQuery( "#breed" ).autocomplete({
              source: "<?php echo site_url('pet/breedlist/?');?>"
            });

            $('input.number').keyup(function(event) {
              // skip for arrow keys
              if(event.which >= 37 && event.which <= 40) return;

              // format number
              $(this).val(function(index, value) {
                return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
              });
            });
            
        });



        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function showPosition(position) {
            var poslat = position.coords.latitude;
            var poslong = position.coords.longitude;
            $('#lat').val(poslat);
            $('#long').val(poslong);
        }
    </script> -->
    */?>



</body>

</html>
