<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active"><?php echo $text; ?></li>

                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
               
                <div class="container-fluid">
                
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo $text ?></h3>
                            <?php if(count($remindervacc)>0){ ?><a href="<?php echo base_url() ?>reminder/export" class="icon-tab theme_button color3" target="_blank">Export List</a><?php }else{ $error="Data not found."; } ?>
                            <!-- <a href="<?php echo base_url() ?>reminder/medical" class="icon-tab theme_button color2"></i>Medical Reminder</a> -->
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->
                
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <!-- <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> --
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> --
                                            </form>
                                        </div>

                                    </div> -->
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->

                                <?php if(count($remindervacc)>0){ ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Reminder Date</th>
                                            <th>Name Pet</th>
                                            <th width="20%">Reminder Note</th>
                                            <th>Owner</th>                                            
                                            <th>Email</th>
                                            <!-- <th>Chip Available</th> -->
                                            <th>Phone</th>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php
                                         $ij = $from + 1 ;
                                         foreach ($remindervacc as $vacc) { 
                                            $idcustomer = $vacc->idowner;
                                            $idpet = $vacc->idpet;
                                            $datacustomer = $this->Reminder_model->petowner($idpet);
                                            if(count($datacustomer) == '0'){
                                                $uidowner = $vacc->idowner;
                                                $nameowner = '';
                                                $emailowner = '';
                                                $phoneowner = '';
                                            }else{
                                                $uidowner = $datacustomer[0]->idowner; 
                                                $nameowner = $datacustomer[0]->namapemilik;
                                                $emailowner = $datacustomer[0]->email;
                                                $phoneowner = $datacustomer[0]->nohp; 
                                            }
                                            
                                            $datapet = $this->Pet_model->detailpet($idpet);
                                            if(count($datapet)>0){
                                                $namepet = $datapet[0]->namapet;
                                            }else{
                                                break;
                                            }
                                                                                     
                                            //$dataowner = $this->Reminder_model->datamember($uidowner);
                                            //var_dump($datacustomer);
                                            // $tipe = $clinic->tipe;
                                            // $photo = $clinic->photo;
                                            
                                            // if($photo =='' ){
                                            //     $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            // }else{
                                            //     $petphoto = $photo;
                                            // }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <?php echo $ij++ ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <?php
                                                $nextdate = $vacc->nextdate;
                                                $newdate = date('D, d M Y',strtotime($nextdate));
                                                 echo $newdate; ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <h5><?php
                                                    $idpet = $vacc->idpet; ?>
                                                    <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $idpet ?>">
                                                    <?php 
                                                     echo $namepet ?></a>
                                                </h5>
                                            </td>
                                            <td class="media-middle" style="word-wrap: break-word;">
                                                <?php $value = explode("<br>",$vacc->vaccine);if(count($value)>1){echo "<ul>";foreach($value as $val){  echo "<li>".$val."</li>"; }echo "</ul>";}else{echo $vacc->vaccine;} ?>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body" style="white-space: nowrap;display: inline-block;">
                                                        <h5>
                                                          <?php   

                                                            echo $nameowner; ?>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $emailowner; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $phoneowner; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo strtoupper($vacc->category); ?>
                                            </td>
                                            <td class="media-middle" style="white-space: nowrap;display: inline-block;">
                                                <!-- <a href="<?php echo base_url() ?>reminder/emailsendvacc/?id=<?php echo $vacc->idvacc ?>" class="theme_button color2">Sent Email</a> -->
                                                <a href="tel:<?php echo $vacc->phoneowner; ?>" class="theme_button color2">Call</a>
                                                <a onclick="$('#modal-kotak , #bg').fadeIn('slow');$('#frame1').attr('src','/CronReminder.php?id=<?php echo $vacc->idvacc ?>');" class="theme_button color3" id="vaccine_email">Email</a>
                                                <button class="icon-tab theme_button color1" data-toggle="modal" data-target="#deletecustomer<?php echo $vacc->idvacc ?>"> Delete </button>
                                                <div class="modal fade" id="deletecustomer<?php echo $vacc->idvacc ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Are you sure ? All data for this reminder will be deleted.</h5>

                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                        <a href="<?php echo base_url() ?>reminder/delete/?id=<?php echo $vacc->idvacc ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                                <?php } ?>
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                        </section></div>
                        <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.js"></script>                        
                        <script type="text/javascript">
                        	$(document).ready(function(){
                        		$('#tombol-tutup').click(function(){
                        			$('#modal-kotak , #bg').fadeOut("slow");
                        		});
                        	});
                        </script>
                        <style>
                            #modal-kotak{
                            	margin-top:-15%;
                            	margin-left:30%;
                            	width: 400px;	
                            	height: 200px;
                            	position: absolute;
                            	z-index:1002;
                            	display: none;
                            	background: white;	
                            }
                            #atas{
                            	font-size: 15pt;
                            	padding: 5px;	
                            	height: 80%;
                            }
                            #bawah{
                            	background: #fff;
                            }
                             
                            #tombol-tutup{	
                            	background: #e74c3c;
                            }
                            #tombol-tutup,#tombol{
                            	height: 30px;
                            	width: 100px;
                            	color: #fff;
                            	border: 0px;
                            	padding:5px;
                            }
                            #bg{
                            	opacity:.80;
                            	position: absolute;
                            	display: none;
                            	position: fixed;
                            	top: 0%;
                            	left: 0%;
                            	width: 100%;
                            	height: 100%;
                            	background-color: #000;
                            	z-index:1001;
                            	opacity: 0.8;
                            }
                            #tombol{
                            	background: #e74c3c;        
                            }                          
                        </style>
                        <div id="bg"></div>
                    	<div id="modal-kotak">
                    		<div id="atas">
                    			<iframe id="frame1" scroll="no" width="100%" height="100%" ></iframe>
                    		</div>
                    		<div id="bawah" align="center" valign="middle">
                    			<button id="tombol-tutup">CLOSE</button>
                    		</div>
                    	</div>	
        <!-- <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10" style="margin-top:-100px;">
            <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo $text2 ?></h3>
							<a href="<?php echo base_url() ?>reminder/exportmed" class="icon-tab theme_button color3" target="_blank">Export List</a>
                        </div>
                    </div> -->
                        <!-- .col-* -->                        
                    <!-- .row -->

                    <!--<div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                       

                                    </div> -->
                                    <!-- .col-* -->
                                    <!-- <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> --
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> --
                                            </form>
                                        </div>

                                    </div> -->
                                    <!-- .col-* -->
                                <!-- .row -->
                                <!--</div>

                                <?php if(count($remindermed)>0){ ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Reminder Date</th>
                                            <th>Name Pet</th>
                                            <th>Last Treatment</th>
                                            <th>Owner</th>                                            
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php
                                         $ij = $from + 1 ;
                                         foreach ($remindermed as $vacc) { 
                                            $idcustomer = $vacc->idowner;
                                            $idpet = $vacc->idpet;
                                            $datacustomer = $this->Reminder_model->petowner($idpet);
                                            if(count($datacustomer) == '0'){
                                                $uidowner = $vacc->idowner;
                                                $nameowner = '';
                                                $emailowner = '';
                                                $phoneowner = '';
                                            }else{
                                                $uidowner = $datacustomer[0]->idowner; 
                                                $nameowner = $datacustomer[0]->namapemilik;
                                                $emailowner = $datacustomer[0]->email;
                                                $phoneowner = $datacustomer[0]->nohp; 
                                            }
                                                                                     
                                            //$dataowner = $this->Reminder_model->datamember($uidowner);
                                            //var_dump($datacustomer);
                                            // $tipe = $clinic->tipe;
                                            // $photo = $clinic->photo;
                                            
                                            // if($photo =='' ){
                                            //     $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            // }else{
                                            //     $petphoto = $photo;
                                            // }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <?php echo $ij++ ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <?php
                                                $nextdate = $vacc->nextdate;
                                                $newdate = date('D, d M Y',strtotime($nextdate));
                                                 echo $newdate; ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <h5><?php
                                                    $idpet = $vacc->idpet; ?>
                                                    <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $idpet ?>">
                                                    <?php 
                                                    $datapet = $this->Pet_model->detailpet($idpet);
                                                    $namepet = $datapet[0]->namapet;
                                                     echo $namepet ?></a>
                                                </h5>
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <?php echo $vacc->treatment ?>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                          <?php   

                                                            echo $nameowner; ?>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $emailowner; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $phoneowner; ?>
                                            </td>
                                            <td class="media-middle" style="white-space: nowrap;display: inline-block;">
                                                <a href="tel:<?php echo $phoneowner; ?>" class="theme_button color3">Call</a>
                                                <a onclick="$('#modal-kotak , #bg').fadeIn('slow');$('#frame1').attr('src','/CronReminder2.php?id=<?php echo $vacc->idvacc ?>');" class="theme_button color3">Email</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div> -->
                                <!-- .table-responsive -->
                              <!--  <?php } ?>
                            </div> -->
                            <!-- .with_border -->
                        <!-- .col-* -->
                        <!--</div>
                    </div> -->
                    <!-- .row -->
                    <!--<div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- .row main columns -->
                <!-- </div>
            </section> -->
                <!-- .container -->

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>

</body>

</html>