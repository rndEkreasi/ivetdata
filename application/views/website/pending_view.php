<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active"><?php echo $text; ?></li>

                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
               
                <div class="container-fluid">
                
                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo $text ?></h3>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->
                
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if(count($remindervacc)==0){$error = "There is no pending invitation"; }?>
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <!-- <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> --
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> --
                                            </form>
                                        </div>

                                    </div> -->
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->

                                <?php if(count($remindervacc)>0){ ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Address</th>
                                            <th>City</th>                                            
                                            <th>Country</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php
                                         $ij = $from + 1 ;
                                         foreach ($remindervacc as $vacc) { 
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <?php echo $ij++ ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <?php
                                                 echo $vacc->nama; ?>                                              
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->email; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->nohp; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->address; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->city; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $vacc->country; ?>
                                            </td>
                                            <td class="media-middle">
                                                <button class="icon-tab theme_button color1" data-toggle="modal" data-target="#approvecustomer<?php echo $vacc->idcustomer ?>"> Approve</button>
                                                <div class="modal fade" id="approvecustomer<?php echo $vacc->idcustomer ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Are you sure that you want to approve this pet owner ?</h5>

                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                        <a href="<?php echo base_url() ?>vets/approve/?id=<?php echo $vacc->idcustomer ?>&clinic=<?php echo abs($vacc->idclinic) ?>" class="icon-tab theme_button color3">APPROVE</a>                                                    
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deletecustomer<?php echo $vacc->idcustomer ?>"> Delete </button>
                                                <div class="modal fade" id="deletecustomer<?php echo $vacc->idcustomer ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Are you sure that you want to delete this invitation ?</h5>

                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                        <a href="<?php echo base_url() ?>vets/unapprove/?id=<?php echo $vacc->idcustomer ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                                <?php } ?>
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                        </section></div>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>

</body>

</html>