<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="<?php echo base_url() ?>assets/js/crop2/dist_files/jquery.imgareaselect.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/js/crop2/dist_files/jquery.form.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/crop2/dist_files/imgareaselect.css">
<script src="<?php echo base_url() ?>assets/js/crop2/functions.js"></script>

        <!-- page main start -->
        <div class="page col-md-6" style="min-height: 600px;margin: 50px 0px;">           
            
            <div class="page-content">
                               
                <div class="tab-content h-100" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-find" role="tabpanel" aria-labelledby="nav-find-tab">
                        <div class="content-sticky-footer circle-background">                                                      
                            
                            <h1 class="block-title color-dark text-center" style="font-size: 31px;line-height: 100%;">Add Pet Photo</h1>
                            <div class="row">                                
                            <div class="col-12">
                                <form action="<?php echo base_url() ?>pet/addphoto" method="post" enctype="multipart/form-data">
                                <div class="card rounded-0 border-0 mb-3">
                                    <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                                    <!-- <div class="card-body">
                                        <div class="form-group ">
                                            <label>Upload Photo</label>
                                            <input type="file" name="test[image]" id="sample_input" class="form-control active"> 
                                            <input type="hidden" name="idpet" value="<?php echo $idpet ?>">
                                        </div>                                        
                                    </div>
                                    <div class="card-footer p-0 border-0">
                                        <button class="btn btn-primary btn-block btn-lg rounded-0">Send</button>
                                    </div> -->
                                    <div class="input-group" style="padding:0px 20px 50px;">                                      
                                        <input type="file" class="form-control" placeholder="Pet Photo" name="petphoto" aria-label="Name">
                                        <input type="hidden" name="idpet" value="<?php echo $idpet; ?>">
                                    </div>
                                    <div class="form-group">
                                        <p class="help-block">Max. Size 10 MB</p>
										<p class="help-block">Extension : jpg | png | jpeg</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="icon-tab theme_button color3" name="submit" value="submit">
                                        <a href="<?php echo base_url() ?>pet/all" class="icon-tab theme_button color1"> Skip upload photo</a>
                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>
                            
                            
                        </div>

                    </div>
                   
                </div>

            </div>
        </div>
        <!-- page main ends -->

    </div>
</div>
</section>

