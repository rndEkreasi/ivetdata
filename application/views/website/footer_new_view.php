			<footer id="footer" class="footer-reveal custom-bg-color-1 footer-reveal border-top-0" style="position:inherit; margin-top:0;">
				<div class="container">
					<div class="row pt-sm-1 pt-md-5 pb-4 d-flex justify-content-between">
						<div class="col-12 col-md-6 col-lg-5 d-flex align-items-center mb-5-sm mt-5-sm">
							<a href="https://play.google.com/store/apps/details?id=com.ivetdata.ivetdata"><img style="width:100%;" src="<?php echo base_url() ?>/new/images/about_ivetdata.png" alt="Download iVet Data Apps"/></a>
						</div>
					<!--	<div class="col-md-4 col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">INSURANCE</h2>
							<ul class="list list-unstyled">
								<li><a href="#" class="text-decoration-none">Auto Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Life Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Business Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Travel Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Home Insurance</a></li>
								<li><a href="#" class="text-decoration-none">Others</a></li>
							</ul>
						</div> -->
						<div class="col-6 col-md-3 col-lg-2">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">QUICK LINKS</h2>
							<ul class="list list-unstyled">
								<li>
									<a href="/">
										Home
									</a>
								</li>
								<li>
									<a href="/#about">
										About Us
									</a>
								</li>
								<li>
									<a href="/#news">
										News
									</a>
								</li>
								<li>
									<a href="/#find">
										Vet Search
									</a>
								</li>
								<li>
									<a href="/partner">
										Are You A Vet ?
									</a>
								</li>
								<li>
									<a href="/#footer">
										Contact Us
									</a>
								</li>
							</ul>
							<ul class="header-social-icons social-icons d-none d-sm-block ml-1 mt-4">
								<li class="social-icons-facebook"><a href="https://www.facebook.com/ivetdata" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="https://www.twitter.com/ivetdata" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-instagram"><a href="https://www.instagram.com/ivetdata" target="_blank" title="Instagram"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
						<div class="col-6 col-md-3 col-lg-3">
							<h2 class="custom-primary-font text-color-light custom-fontsize-6 mb-3">CONTACT US</h2>
							<p><b>Address:</b> Ruko Kebon Jeruk A9-3, West Jakarta 11630<br/></p>
							<p>
							<span class="d-block text-color-light custom-fontsize-5"><img src="<?php echo base_url() ?>/new/images/FBM.png" width="8%"> <a href="https://m.me/ivetdata" target="_blank" title="Facebook Chat">iVetData</a></span>
							<span class="d-block text-color-light custom-fontsize-5"><img src="<?php echo base_url() ?>/new/images/email.png" width="8%" > <a href="mailto:cs@ivetdata.com" class="text-decoration-none">cs@ivetdata.com</a></span>
							</p>
							<p>
							    <i class="far fa-clock top-7"></i>
							    Monday - Friday - 9am to 5pm<br />
                                <i class="far fa-clock top-7"></i>
                                Saturday - 9am to 2pm<br />
                                <i class="far fa-clock top-7"></i> Sunday - Closed</p>


    


							
							<!-- <span class="d-block text-color-light custom-fontsize-5 mb-2">Call: <a href="tel:+1234567890" class="text-decoration-none text-color-light custom-fontsize-5">+62-21-2977-951</a></span> -->
							
						</div>
					</div>
					<div id="google_translate_element" align="center"></div>
				</div>
				<div class="footer-copyright custom-bg-color-1 border-top-0 mt-0">
					<div class="container">
						<hr class="solid opacity-1 mb-0">
						<div class="row">
							<div class="col mt-4 mb-4">
								<p class="text-center text-2 mb-0">© 2019 iVet Data</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

        
        <!-- new/vendor -->
		<script src="<?php echo base_url() ?>/new/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/common/common.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/vide/jquery.vide.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/vivus/vivus.min.js"></script>
        
        <!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url() ?>/new/js/theme.js"></script>
		
		<!-- Current Page new/vendor and Views -->
		<script src="<?php echo base_url() ?>/new/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url() ?>/new/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page new/vendor and Views -->
		<script src="<?php echo base_url() ?>/new/js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="<?php echo base_url() ?>/new/js/demos/demo-insurance.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url() ?>/new/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url() ?>/new/js/theme.init.js"></script>
		
		<!-- Change some style -->

</body>

</html>