<section class="ls page_portfolio section_padding_bottom_75">
<div class="col-sm-12 text-center">
	<h2 class="section_header with_icon icon_color">Vet Search</h2>
</div>
</section>
<section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5006459911733!2d106.74596901529505!3d-6.197484162441437!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f7156127b7d7%3A0xc48e9837c9530479!2sTaman+Kebon+Jeruk+Pharmacy!5e0!3m2!1sen!2sid!4v1551572724004" width="100%" height="460" frameborder="0" style="border:0;margin:0px 0px -8px;" allowfullscreen></iframe> -->

	<div id="map" style="height: 100%;min-height:750px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        //   // Try HTML5 geolocation.
        // if (navigator.geolocation) {
        //     navigator.geolocation.getCurrentPosition(function(position) {
        //       var pos = {
        //         lat: position.coords.latitude,
        //         lng: position.coords.longitude
        //       };

        //       infoWindow.setPosition(pos);
        //       infoWindow.setContent('Location found.');
        //       infoWindow.open(map);
        //       map.setCenter(pos);
        //     }, function() {
        //       handleLocationError(true, infoWindow, map.getCenter());
        //     });
        //   } else {
        //     // Browser doesn't support Geolocation
        //     handleLocationError(false, infoWindow, map.getCenter());
        //   }
        // }

        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
          //center: new google.maps.LatLng(lat, lang,
		  //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;

        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var url = markerElem.getAttribute('url');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap" async defer></script>

	
</section>
<!-- <section class="ls page_portfolio section_padding_top_100 section_padding_bottom_75">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<h2>Vets List</h2>

							<div class="isotope_container isotope row masonry-layout columns_margin_bottom_20" style="position: relative; height: 6579px;">

								<?php foreach ($allarticle as $detail) {
									//$uid

								?>								
								

								<div class="isotope-item col-lg-4 col-md-6 col-sm-12">

									<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
										<div class="item-media">
											<img src="<?php echo base_url() ?>assets/images/petshop.png" alt="">
										</div>
										<div class="item-content">
											
											<h4 class="entry-title">
												<a href=""><?php echo $detail->nameclinic; ?></a>
											</h4>
											<p class="margin_0">
												<!-- Clients can simply schedule their hard drive destruction online and through our website. --
												<?php 
												//$descibe = strip_tags($detail->description);
												echo $detail->address;
												//echo substr($descibe, 0, 100); ?>,<br>
												<?php echo $detail->city; ?><br>
												<?php echo $detail->phone; ?><br>
											</p>
											<!-- <a href="<?php echo base_url() ?>blog/detail/<?php /// echo $detail->slug; ?>" class="read-more"></a> --
										</div>
									</article>

								</div>

								<?php } ?>

							</div>
							<!-- eof .isotope_container.row --

							<div class="row">
								<div class="col-sm-12 text-center">
									<?php echo $this->pagination->create_links(); ?>
									<!-- <img src="<?php echo base_url() ?>assets/img/loading.png" alt="" class="fa-spin"> --
								</div>
							</div>

						</div>
					</div>
				</div>
			</section> -->
        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
<footer class="page_footer ds parallax section_padding_top_100 section_padding_bottom_65 columns_padding_25">
               
            </footer>

            <section class="ls page_copyright section_padding_15">
                <div class="container">
                   
					<!--<div class="row">
						<div class="col-sm-12 col-md-2 col-lg-3 empty"></div>
						<div class="col-sm-12 col-md-8 col-lg-6 text-center">
							<ul class="footer_nav">
								<li><a href="<?php echo base_url() ?>termsconditions.html">Terms &amp; Conditions</a></li>
								<li><a href="">FAQ</a></li>
								<li>|</li>
								<li><a href="<?php echo base_url() ?>contact">Contact</a></li>
								<li>|</li>
								<li><a href="https://play.google.com/store/apps/details?id=io.gonative.android.ppnyxd"">Download our app</a></li>
							</ul>    
						</div>
						<div class="col-sm-12 col-md-2 col-lg-3 empty"></div>
					</div> -->
					<div class="row">
                        <div class="col-sm-12 text-center">
                            <p>&copy; Copyright <?php echo date('Y') ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>


</body>

</html>
