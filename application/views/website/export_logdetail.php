<?php

    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();

    // Settingan awal file excel
    $excel->getProperties()->setCreator('Ivetdata')
                 ->setLastModifiedBy('Ivetdata')
                 ->setTitle("Pet")
                 ->setSubject("Pet")
                 ->setDescription("Pet")
                 ->setKeywords("Pet");

    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );

    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );

    $excel->setActiveSheetIndex(0)->setCellValue('A1', $namepet."'s Medical History"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:B1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "ITEM"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "VALUE"); // Set kolom B3 dengan tulisan "NIS"

    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);

    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Microchip ID");
    $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, "'".$rfid);
    $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+1), "Name");
    $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+1), $namepet);
    $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+2), "Type");
    $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+2), $tipe);
    $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+3), "Breed");
    $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+3), $breed);
    $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+4), "Age");
    $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+4), $age);
    $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+5), "Owner Info");
    foreach($dataowner as $data){ 
        $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+6), "Name");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+6), $data->nama);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+7), "Phone");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+7), $data->nohp);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+8), "Email");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+8), $data->email);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+9), "Address");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+9), $data->address);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+10), "City");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+10), $data->city);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.($numrow+11), "Country");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.($numrow+11), $data->country);
    }
    $numrow = 11;
    foreach($datalog as $data){ 
        $numrow = $numrow+2;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Medical History On ".$data->tgllog);
        $excel->getActiveSheet()->mergeCells('A'.$numrow.':B'.$numrow); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A'.$numrow)->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A'.$numrow)->getFont()->setSize(15); // Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A'.$numrow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Date");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->tgllog);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Vet");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->namevets);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Diagnose");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->diagnose);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Treatment");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, str_replace("<br>",";",$data->treatment));
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Anamnesis");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->anamnesis);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Basic Checkup");
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Weight");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->weight);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Temperature");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->temperature." celcius");
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Heart Rate");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->heartrate." beats/minute");
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Respiration Rate");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->resprate." breaths/minute");
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Heart Rate");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->heartrate);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "BCS");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->bcs);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Food");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->food);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Physical Exams"); 
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Eyes and Ears");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->eyes);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Skin and Coat");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->skin);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Respiratory System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->resp);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Circulation System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->circ);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Digestive System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->dige);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Respiratory System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->resp);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Urinary System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->urin);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Nervous System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nerv);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Musculoskeletal System");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->musc);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Other Notes");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->othernotes);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Diagnostic Tools Result");
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Hematology");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->hemanotes);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Blood Chemistry");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->bloodnotes);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Ultrasonography");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->ultrasononotes);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "X-ray");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->xraynotes);
        $numrow++;
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Other Diagnostic");
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->otherdiagnotes);
    }
    for($numrow=3;$numrow<50;$numrow++){
        // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
    }

    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(40); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(80); // Set width kolom B

    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);

    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Medical History");
    $excel->setActiveSheetIndex(0);

    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="'.$namepet.'"_medical_history.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');

    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');

?>
