            <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Article</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>All Posts</h3>
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_border with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">

                                        <!-- <form action="./" class="form-inline filters-form">
                                            <span>
                                                <label class="grey" for="with-selected">With Selected:</label>
                                                <select class="form-control with-selected" name="with-selected" id="with-selected">
                                                    <option value="">-</option>
                                                    <option value="approve">Approve</option>
                                                    <option value="publish">Publish</option>
                                                    <option value="delete">Delete</option>
                                                </select>
                                            </span>
                                            <span>
                                                <label class="grey" for="orderby">Sort By:</label>
                                                <select class="form-control orderby" name="orderby" id="orderby">
                                                    <option value="date" selected="">Date</option>
                                                    <option value="author">Author</option>
                                                    <option value="title">Title</option>
                                                    <option value="status">Status</option>
                                                </select>
                                            </span>

                                            <span>
                                                <label class="grey" for="showcount">Show:</label>
                                                <select class="form-control showcount" name="showcount" id="showcount">
                                                    <option value="10" selected="">10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </span>
                                        </form> -->

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search Keyword">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <td class="media-middle text-center">
                                                <input type="checkbox">
                                            </td>
                                            <th>Title:</th>
                                            <th>Author:</th>
                                            <th>Date:</th>
                                            <th>Categories:</th>
                                            <th>Action:</th>
                                        </tr>
                                        <?php foreach ($allarticle as $dataarticle) { ?>                                           
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="admin_post.html"><?php echo $dataarticle->title ?></a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $dataarticle->creator; ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle">
                                                <time datetime="2017-02-08T20:25:23+00:00" class="entry-date">08.02.2017 at 20:25</time>
                                            </td>
                                            <td class="media-middle">
                                                uncategorized, category1
                                            </td>
                                            <td class="media-middle">
                                                <a href="<?php echo base_url() ?>article/edit/?id=<?php echo $dataarticle->idarticle ?>" class="icon-tab theme_button color3">Edit</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                                <div class="col-md-6 text-md-right">
                                    Showing 1 to 6 of 12 items
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="grey">&copy; Copyrights 2017</p>
                        </div>
                        <div class="col-sm-6 text-sm-right">
                            <p class="grey">Last account activity <i class="fa fa-clock-o"></i> 52 mins ago</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->


    

    


    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar -->
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -->
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>

</body>

</html>