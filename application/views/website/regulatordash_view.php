<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Pet</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>Pet Owner by <?php echo $namaowner ?></h3>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class=" with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                           <form method="get" class="" action="<?php echo base_url() ?>pet/detail/">
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="idpet" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No </th>
                                            <th>PET ID</th>
                                            <th>Pet Name</th>
                                            <th>Type</th>
                                            <th>Breed</th>
                                            <th>Age</th>
                                        </tr>
                                        <?php 
                                        $ij = 1;
                                        foreach ($datapet as $pet) { 
                                            $tipe = $pet->tipe;
                                            $photo = $pet->photo;
                                            
                                            if($photo =='' ){
                                                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            }else{
                                                $petphoto = $photo;
                                            }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td><?php echo $ij++ ?></td>
                                            <td class="media-middle">
                                                <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->rfid ?>"><?php echo $pet->rfid ?></a>
                                               
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->rfid ?>"><?php echo $pet->namapet ?></a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $pet->tipe; ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle" style="">
                                                <?php echo $pet->breed ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php 
                                                    $datebirth = $pet->datebirth; 
                                                    $agey = date_diff(date_create($datebirth), date_create('now'))->y;
                                                    $agem = date_diff(date_create($datebirth), date_create('now'))->m;
                                                    echo $agey.' Year, '.$agem.' Month ';
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php // echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>