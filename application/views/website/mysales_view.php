<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">Sales Invoice</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>Sales Invoice</h3>
                            <?php if ($role == '1'){ ?>
                                <a href="<?php echo base_url() ?>sales/invoice" class="icon-tab theme_button color1">+ Add Invoice</a>
                                <a href="<?php echo base_url() ?>sales/exportsaleslist" class="icon-tab theme_button color3" target="_blank">Export List</a>
                            <?php } ?>                            
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <!-- <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> 
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search phone">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div>
                                            </form> -->
                                            <form class="form-inline" >
                                                <div class="form-group" style="width: 100% !important;">
                                                    <!-- <label class="sr-only" for="exampleInputEmail3">Pet ID</label> -->
                                                    <input id="choosinvoice" type="text" value="" name="invoice" class="form-control choosinvoice" placeholder="Search Name or Invoice Number">
                                                    <button type="submit" class="theme_button color1" style="height: 41px; line-height: 0px !important;">Find</button>
                                                </div>  
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Invoice</th>
                                            <th>Date</th>
                                            <th>Total Price</th>
                                            <?php if($role=='0'){?>
                                            
                                            <?php }else{ ?>
                                            <th>Customer</th>
                                            <?php } ?>                                                                                        
                                            <th>Action</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                        <?php 
                                        $ij = $all_sales-$from;if(isset($error))$datasales = array();
                                        foreach ($datasales as $sales) {
                                            $status = $sales->status;
                                            if($status == '1'){
                                                $statusfix = '<span class="label label-success">Paid</span>';
                                            }else{
                                                if($status == '0'){
                                                    $statusfix = '<span class="label label-danger">Unpaid</span>';
                                                };
                                            }
                                           // $countpet = $this->Customer_model->petcustomer($idcustomer);

                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <?php echo $ij-- ?>
                                            </td>
                                            <td class="media-middle">
                                                <a href="<?php echo base_url() ?>sales/detail/?invoice=<?php echo $sales->invoice ?>"><?php echo $sales->invoice ?></a>
                                            </td>
                                            <td class="media-middle">
                                                <h5><?php echo date('d M Y',strtotime($sales->invdate)) ?></h5>
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="#">Rp. <?php echo number_format($sales->totalprice); ?></a>
                                                </h5>
                                            </td>
                                            <?php if($role=='0'){?>
                                            
                                            <?php }else{ ?>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $sales->nameclient ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>

                                            <?php } ?>    
                                            
                                            <td class="media-middle">
                                                <a href="<?php echo base_url() ?>sales/detail/?invoice=<?php echo $sales->invoice ?>" class="icon-tab theme_button color2">View</a>
                                                <?php if ($role == '1'){ 
                                                    if($manageto == '1'){?>

                                                <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deleteservice<?php echo $sales->idinv; ?>"> Delete </button>
                                            <?php }
                                            } ?>
                                            </td>
                                            <div class="modal fade" id="deleteservice<?php echo $sales->idinv; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Delete Invoice #<?php echo $sales->invoice ?> </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CLOSE</button>
                                                    <a href="<?php echo base_url() ?>sales/delete/?id=<?php echo $sales->idinv; ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php if(!isset($error))echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    //price.val(ui.item.label);                     
                }                
            });
 
        });
    </script>


</body>

</html>