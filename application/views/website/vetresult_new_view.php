<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,%0A+Mountain+View,+CA&key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 9c419fc6-508e-42c8-b1d8-c24ab2b7349f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
?>
<!--<body>
        <div role="main" class="main">
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section>-->
    <?php if(!$searchvet){ ?><br /><h4 align="center"><?php echo $message ?></h4><br /><?php } ?>
    <div id="map" style="height: 100%;min-height:250px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
      //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;
        
        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var url = markerElem.getAttribute('url');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>

    <?php if($searchvet){ ?>
        <section class="ls page_portfolio pt-53 pb-7">
				<div class="container">
					<div class="row">
                    <div class="col-sm-12"><br />
							<h4 class="text-center">Vets and Clinic List - <?php echo $_POST['vet']." ".$_POST['city']; ?></h4><br />

							<div class="isotope_container isotope row masonry-layout mb-3" style="position: relative;">

                <?php
                  for ($i=0;$i<count($searchvet);$i++) { ?>

    								<div class="isotope-item clinic col-lg-4 col-md-6 col-sm-12">

    									<!-- <article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden" style="height:255px;">
    										<div class="item-media">
                                                <a href="<?php echo base_url() ?>vets/detail/?vets=<?php echo $searchvet[$i]['idclinic'] ?>">
    											<img src="<?php echo base_url() ?>assets/images/petshop.png" alt=""> 
                                                </a>
    										</div>-->
    										<div class="item-content">
    											
    											<h4 class="entry-title" style="color:#e54d5f;text-align:center;">
    												<?php echo $searchvet[$i]['nameclinic']; ?>
    											</h4>
    											<div align="center">
    											    <?php if($searchvet[$i]['picture']=="") {echo '<img class="mb-1" style="width:60px;" src="/assets/images/petshop.png" alt="">';}
    											    else {echo '<img style="width:140px;" src='.$searchvet[$i]['picture'].' alt="">';
    											    }
    											    ?>
    											</div>
    											<p class="margin_0" align="center">
    												<!-- Clients can simply schedule their hard drive destruction online and through our website. -->
    												<?php 
    												//$descibe = strip_tags($detail->description);
    												echo $searchvet[$i]['address'];
    												//echo substr($descibe, 0, 100); ?>
    												<?php echo $searchvet[$i]['city']; ?><br>
    												<?php echo $searchvet[$i]['phone']; ?><br>
    											</p>
    											<!-- <a href="<?php echo base_url() ?>blog/detail/<?php /// echo $detail->slug; ?>" class="read-more"></a> -->
    										</div>
    									</article>

    								</div>

								<?php } ?>

							</div>
							<!-- eof .isotope_container.row -->

							<!-- <div class="row">
								<div class="col-sm-12 text-center">
									<?php // echo $this->pagination->create_links(); ?>
									<!-- <img src="<?php echo base_url() ?>assets/img/loading.png" alt="" class="fa-spin"> --
								</div>
							</div> -->

						</div>
					</div>
				</div>
			</section>
         <?php   } ?>
