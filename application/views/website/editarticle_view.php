
            <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>">Dashboard</a>
                                </li>
                                <li class="active">Edit Article</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Edit Article</h3>
                        </div>
                    </div>
                    <!-- .row -->
                    <?php if (isset($success)){ ?>
                        <div class="alert alert-success"><?php echo $success ?></div>
                    <?php } ?>
                    <?php if (isset($error)){ ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-xs-12 col-md-6">

                            <!-- Basic form -->
                            <div class="with_border with_padding">
                                <?php foreach ($dataarticle as $article) { 
                                    $featuredimg = $article->featuredimg;
                                    $jmlfeaturedimg = strlen($featuredimg);
                                    if($jmlfeaturedimg < 50){
                                        $urlfeaturedimg = base_url().'upload/article/'.$featuredimg;
                                    }else{
                                        $urlfeaturedimg = $featuredimg; 
                                    }
                                    ?>                              

                                <form action="<?php echo base_url() ?>article/updatearticle" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title:</label>
                                        <input type="text" class="form-control" id="judul" value="<?php echo $article->title ?>" name="title" required>
                                        <input type="hidden" name="idarticle" value="<?php echo $article->idarticle ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Featured Image:</label><br>
                                        <img src="<?php echo $urlfeaturedimg ?>" alt="">
                                        <input type="file" class="form-control" id="judul" name="featuredimg">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Description</label>
                                        <textarea name="description" class="tiny" rows="15" class="form-control"><?php echo $article->description ?></textarea>
                                    </div>                                    
                                    <button type="submit" class="theme_button">Post</button>
                                </form>

                                <?php } ?>
                            </div>
                            <!-- .with_border -->                           
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->


                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="grey">&copy; Copyrights 2017</p>
                        </div>
                        <div class="col-sm-6 text-sm-right">
                            <p class="grey">Last account activity <i class="fa fa-clock-o"></i> 52 mins ago</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->


    

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ad8cbhz7hy8flomantnx8ehdj6a4lpjuuqs226xxs9yytcjy"></script>
<script>tinymce.init({ selector:'textarea.tiny' });</script>


    <!-- template init -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

    <!-- dashboard libs -->

    <!-- events calendar -->
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -->
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -->
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>

</body>

</html>