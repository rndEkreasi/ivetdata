<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,%0A+Mountain+View,+CA&key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Postman-Token: 9c419fc6-508e-42c8-b1d8-c24ab2b7349f",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  //echo $response;
}
?>
<!--<body>
        <div role="main" class="main">
			<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Vet Search</h1>
						</div>
					</div>
				</div>
			</section>-->

    <div id="map" style="height: 100%;min-height:250px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
      //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;
        
        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var url = markerElem.getAttribute('url');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>
    
<!--<section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				 <h2 class="highlight">News & Article</h2> 
				<ol class="breadcrumb darklinks">
					<li>
						<a href="<?php echo base_url() ?>">
							Vet Clinic
						</a>
					</li>
					<li class="active">
						<a href="#"><?php echo $nameclinic ?></a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section> -->
 <section class="ls page_portfolio section_padding_top_50 section_padding_bottom_75">
				<div class="container">
					<div class="row">
                    <div class="col-sm-12"><br />
							<div class="isotope_container isotope row masonry-layout columns_margin_bottom_20" style="position: relative; height: 100%;">

                
                	<?php foreach ($dataclinic as $clinic) {
						$pictureclinic = $clinic->picture;
					?>					
			                        <div class="isotope-item clinic col-lg-4 col-md-6 col-sm-12">
    										<div class="item-content">
    											
    											<h4 class="entry-title" style="color:#e54d5f;">
    											    <?php echo $clinic->nameclinic; ?>
    											</h4>
    											<div>
    											    <?php if($pictureclinic=="") {echo '<img class="mb-1" style="width:60px;" src="/assets/images/petshop.png" alt="">';}
    											    else {echo '<img style="width:140px;" src='.$pictureclinic.' alt="">';
    											    }
    											    ?>
    											</div>
    											<p class="margin_0">
    												<!-- Clients can simply schedule their hard drive destruction online and through our website. -->
    												<?php 
    												//$descibe = strip_tags($clinic->description);
    												echo $clinic->address;
    												//echo substr($descibe, 0, 100); ?>
    												<?php echo $clinic->city; ?><br>
    												<?php echo $clinic->phone; ?><br>
    											</p>
    											<!-- <a href="<?php echo base_url() ?>blog/detail/<?php /// echo $clinic->slug; ?>" class="read-more"></a> -->
    										</div>
    									</article>

    								</div>

			<?php } ?></div>
					</div>
					</div>
				</div>
			</section>
			<br /><br />
			<!-- <section id="blog" class="ls section_padding_100">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color3">
								From Our Blog
							</h2>
							<p class="small-text">Our latest news</p>
						</div>
					</div>
					<div class="row columns_margin_bottom_20">
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/05.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 12, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Shankle doner ribeye ham hock shank</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/06.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 13, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">T-bone capicola kevin pancetta</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/07.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 14, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Beef tri-tip brisket jerky boudin</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
					</div>
				</div>
			</section> -->
