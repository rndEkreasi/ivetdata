<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Pet</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <br><h3>Total Pet Record : <?php echo number_format(count($datapet)); ?></h3>
                            <a href="<?php echo base_url() ?>pet/add" class="icon-tab theme_button color3">+ Add Pet</a>
                            <a href="<?php echo base_url() ?>pet/exportpetlist<?php if(isset($_GET['emailpet']))echo '?emailpet='.$_GET['emailpet'] ?><?php if(isset($_GET['phone'])) echo "&phone=".$_GET['phone'] ?>" class="icon-tab theme_button color3" target="_blank">Export List</a>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class=" with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                           <form method="get" class="" action="<?php echo base_url() ?>pet/result/">
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="" type="text" value="" name="emailpet" class="form-control emailpet" placeholder="insert email or name" style="float: left;width: 50%">
                                                    <input id="widget-search" type="text" value="" name="phone" class="form-control choosecustomer" placeholder="search phone number" style="float: left;width: 50%">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->

                                <?php $ij = count($datapet); if ($ij>0){ ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>                                            
                                            <th>Pet Name</th>
                                            <th>Type</th>
                                            <th>Breed</th>
                                            <th>Date of Birth</th>
                                            <th>Age</th>
                                            <th>Owner</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Microchip ID</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php 
                                        
                                        foreach ($datapet as $pet) { 
                                            $tipe = $pet->tipe;
                                            $photo = $pet->photo;
                                            
                                            if($photo =='' ){
                                                $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            }else{
                                                $petphoto = $photo;
                                            }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td><?php echo $ij; ?></td>                                            
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>"><?php echo $pet->namapet ?></a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $pet->tipe; ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle" style="">
                                                <?php echo $pet->breed ?>
                                            </td>
                                            <td class="media-middle" style="">
                                                <?php echo $pet->datebirth ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php 
                                                    $datebirth = $pet->datebirth; 
                                                    $agey = date_diff(date_create($datebirth), date_create('now'))->y;
                                                    $agem = date_diff(date_create($datebirth), date_create('now'))->m;
                                                    echo $agey.' Year, '.$agem.' Month ';
                                                ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $pet->namapemilik ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $pet->nohp ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $pet->email ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php $microchip = $pet->rfid;
                                                    if($microchip == '0'){ ?>
                                                        <a href="https://tawk.to/chat/5c792599a726ff2eea5a1b14/default">Buy Microchip</a>
                                                    <?php }else{  ?>
                                                        <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>"><?php echo $pet->rfid ?></a>
                                                    <?php }
                                                ?>
                                               
                                            </td>
                                            <td class="media-middle" style="white-space:nowrap;">
                                                <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>" class="icon-tab theme_button color2">View</a>
                                                <?php if($manageto == '1'){ ?>
                                                    <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deleteservice<?php echo $pet->idpet; ?>"> Delete </button>
                                                    <div class="modal fade" id="deleteservice<?php echo $pet->idpet; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Delete <?php echo $pet->namapet ?> <br>Are you sure?<br> All pet record, pet owner, and invoice data <br> will be deleted for this pet.</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                  <span aria-hidden="true">&times;</span>
                                                                </button>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button>
                                                                <a href="<?php echo base_url() ?>pet/delete/?idpet=<?php echo $pet->idpet; ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                <?php } ?>
                                                
                                            </td>
                                        </tr>
                                    <?php $ij--; } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                                <?php } ?>
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php // echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; Copyrights <?php echo date('Y'); ?> iVet Data.</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- <script src="//geodata.solutions/includes/countrystatecity.js"></script> -->
    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#breed').autocomplete({
                source: "<?php echo site_url('pet/breedlist');?>",
      
                select: function (event, ui) {
                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });
            // $('#microchip').hide();
            // $('#uniqid').hide();
            // $("#chooseuniq").change(function(){
            //     if($('#chooseuniq').val() == '1') {
            //         $('#microchip').show(); 
            //         $('#uniqid').hide();
            //         var uniqid = document.getElementById("uniqid");  
            //         uniqid.removeAttribute("required");
            //     }else {
            //         $('#microchip').hide(); 
            //         $('#uniqid').show();
            //         var microchip= document.getElementById("microchip");  
            //         microchip.removeAttribute("required");
            //     } 
            // });
            

            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    $('#address').val(ui.item.address);
                    $('#city').val(ui.item.city);
                    //price.val(ui.item.label);                     
                }                
            });
 
        });
    </script>


</body>

</html>