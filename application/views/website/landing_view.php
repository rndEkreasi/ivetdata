			<?php $lang = $this->input->get('lang'); ?>
			<section class="intro_section page_mainslider ls ms">
				<div class="flexslider">
					<ul class="slides homepage">
						<li>
							<img src="<?php echo base_url() ?>assets/images/slide01.jpg" alt="">
							<!-- <video controls muted autoplay loop style="width: 100%;">
							  <source src="<?php echo base_url() ?>assets/video/videobg1.mp4" type="video/mp4">
							  Your browser does not support the video tag.
							</video> -->
							<div class="container">
								<div class="row">
									<div class="col-sm-12">
										<div class="slide_description_wrapper">
											<div class="slide_description">
												<div class="intro-layer" data-animation="fadeInUp">
													<div class="contentnya" style="display: inline-block;color: #fff;padding: 10px;margin: 0px -7px 20px;">
													<!-- <h2 style="background: #F4B531;display: inline-block;color: #fff;padding: 10px;">
														Vet Digital Pocket Companion & Database
														<!-- <strong>Health Care</strong> -
													</h2> -->	
													<!-- Nav tabs -->
														<ul class="nav nav-tabs" role="tablist">
															<li class="active">
																<a href="#microchip" role="tab" data-toggle="tab">
																	<i class="fa fa-paw"></i> Microchip</a>
															</li>
															<li>
																<a href="#vets" role="tab" data-toggle="tab">
																	<i class="rt-icon2-users"></i> Vets</a>
															</li>
														</ul>
														<div class="tab-content top-color-border">
															<div class="tab-pane fade in active" id="microchip">
																<form class="form-inline" method="get" action="<?php echo base_url() ?>pet/search/">
							                                        <div class="form-group" style="width: 100% !important;">
							                                            <label class="sr-only" for="exampleInputEmail3">Pet ID</label>
							                                            <input type="text" style="width: 100% !important;" class="form-control" name="idpet" id="exampleInputEmail3" placeholder="MICROCHIP ID">
							                                            <button type="submit" class="theme_button color1" style="color: #fff;width: 70px;">SEARCH</button>
							                                        </div>  
							                                    </form>
							                                  <p style="color: #222;line-height: 100%;padding: 10px 0px 15px;margin: 0px;font-size: 8px;">
							                                    <?php if ($lang == 'id'){ ?>
							                                    	Masukkan microchip ID di atas dan klik "Cari". Masukkan hanya nomor microchip 9, 10 atau 15 karakter, tanpa tanda baca atau spasi.
							                                    <?php }else{ ?>
							                                    	Enter the microchip ID above and click "Search". Enter only the 9, 10 or 15 character microchip number, with no punctuation or spaces.
							                                    <?php } ?> </p>
							                                </div>
							                                <div class="tab-pane fade" id="vets" style="width:100%;height: 95px;">
																<form class="form-inline" method="post" action="<?php echo base_url() ?>vets/search/">
							                                        <div class="form-group" style="width: 100% !important;">
							                                            <!-- <label class="sr-only" for="exampleInputEmail3">Name Vets</label> -->
							                                            <input type="text" style="width: 45% !important;float: left;" class="form-control" name="name" id="exampleInputEmail3" placeholder="VET NAME">
							                                            <input type="text" style="width: 45% !important;float: left;" class="form-control" name="city" id="exampleInputEmail3" placeholder="CITY">
							                                            <button type="submit" class="theme_button color1" style="color: #fff;width: 70px;">Find</button>
							                                            <div class="clearfix" style="clear: both;"></div>
							                                            
							                                        </div>  

							                                    </form>
							                                    <p style="color: #222;line-height: 100%;padding: 10px 0px 15px;margin: 0px;font-size: 10px;">
							                                    <?php if ($lang == 'id'){ ?>
							                                    	Masukkan nama dokter hewan atau kota dokter hewan yang ingin Anda cari.
							                                    <?php }else{ ?>
							                                    	Enter the name of vet or city of vet  that you want to search
							                                    <?php } ?></p>
							                                </div>
							                            </div>
				                                    <div>
				                                </div>
												</div>
												<!-- <div class="intro-layer" data-animation="fadeInUp">
													<p>Strip steak short ribs picanha shoulder bresaola. Pork belly brisket shankle short loin.</p>
												</div> -->
												<!-- <div class="intro-layer" data-animation="fadeInUp">
													<a href="about.html" class="theme_button color3">Make an Appointment</a>
												</div> -->
											</div>
											<!-- eof .slide_description -->
										</div>
										<!-- eof .slide_description_wrapper -->
									</div>
									<!-- eof .col-* -->
								</div>
								<!-- eof .row -->
							</div>
							<!-- eof .container -->
						</li>

						

					</ul>
				</div>
				<!-- eof flexslider -->
<!--
				<div class="scroll_button_wrap">
					<a href="#about" class="scroll_button">
						<span class="sr-only">scroll down</span>
					</a>
				</div>
-->
			</section>
			<!-- <section class="ls ms section_padding_50">
				<div class="container">
					<div class="row">
						<!-- <div class="col-sm-12 text-center">
							<h3>Icon Background teasers</h3>
						</div> -
					</div>
					<div class="row">
						<div class="col-sm-4">

							<div class="teaser warning_bg_color font-icon-background-teaser text-center">
								<h3 class="counter highlight counted" data-from="0" data-to="1257" data-speed="2100">20.000+</h3>
								<p>Vets</p>
								<i class="fa fa-user icon-background"></i>
							</div>

						</div>

						<div class="col-sm-4">

							<div class="teaser danger_bg_color font-icon-background-teaser inner-border text-center">
								<h3 class="counter highlight counted" data-from="0" data-to="346" data-speed="1500">2.000+</h3>
								<p>Clinics</p>
								<i class="fa fa-medkit icon-background"></i>
							</div>

						</div>

						<div class="col-sm-4">

							<div class="teaser success_bg_color font-icon-background-teaser text-center">
								<h3 class="counter-wrap highlight" data-from="0" data-to="40" data-speed="1800">
									<span class="counter counted" data-from="0" data-to="20" data-speed="1500">1.000.000+</span>
								</h3>
								<p>Pets</p>
								<i class="fa fa-paw icon-background"></i>
							</div>

						</div>
					</div>
				</div>
			</section> -->

			<!--<section id="about" class="ls section_padding_top_50 section_padding_bottom_50">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color">
								About iVetData
							</h2>
							<p class="small-text">Vet's Digital Companion</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 col-sm-12"></div>
						<div class="col-md-8 col-sm-12">
							-->
							<!-- <img src="<?php echo base_url() ?>assets/images/about.jpg" alt="" /> --->
							<!--
							<div class="iframe_container"><iframe src="https://www.youtube.com/embed/F82o_yIiUN8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
							<div class="play">								
								<a href="<?php echo base_url() ?>login" target="_new"><img src="<?php echo base_url() ?>assets/images/web_btn.png"></a>
								<a href="https://play.google.com/store/apps/details?id=io.gonative.android.ppnyxd" target="_new"><img src="<?php echo base_url() ?>assets/images/google_btn.png"></a>
							</div>
						</div>
							<div class="col-md-2 col-sm-12"></div>
						
					</div>
					<div class="row">
						<div class="col-md-2 col-sm-12"></div>
						<div class="col-md-8 col-sm-12">
							<?php if ($lang == 'id'){ ?>
							 	<p>Kami hadir sebagai solusi untuk melacak & mengumpulkan data client Anda.</p>

								<p>Kini Dokter Hewan dapat menikmati pet database yang komprehensif di seluruh Indonesia melalui fitur pet ID yang terdapat dalam microchip. Data tersebut meliputi Nama Hewan, Nama Dokter, Alamat Klinik, dan No telp. Oleh karena itu, Dokter hewan dapat melihat semua Data Pribadi hewan dan Pemilik Hewan sekaligus rekam medis yang berguna untuk memverifikasi kepemilikan hewan dan mendiagnosis pasien.</p>

								<p>IVetData juga bertujuan untuk membantu berbagai komunitas pemilik dan pecinta hewan peliharaan serta organisasi nirlaba untuk meningkatkan kesejahteraan hewan melalui transparansi informasi dan 12</p>

								<p>Jadi tunggu apalagi???<br>
								Segera simpan data dan riwayat hewan peliharaan lalu koneksikan dengan client anda, pendataan menjadi aman dan nyaman</p>


							 <?php }else{ ?>
							 	<p>PT IVET DATA GLOBAL (est. 2018) headquartered in Jakarta, Indonesia as a pioneer in Pet Database and Clinic CRM in the region, endorsed by Indonesian Veterinary Medical Association (PDHI) in Indonesia.</p>
							 	<p>We are also the first company to partner with Datamars SA to introduce country-coded microchips which follow ICAR (the International Committee for Animal Recording) ISO11784/11785 standards. </p> 
							 	<p>Formed to accommodate the growing demand for digital solutions in veterinary practice and to support the new Government regulation on pet’s "National ID” identification, we believe in providing new technology for Pet Owners and Vets that are comprehensive, affordable, and secure.
							 		</p>
							 		-->
							 		<!--
							 	<p>Register your pet's identification in iVetData Database for a comprehensive solution to track   and prove the ownership of your   pets linked to licensed Vets locally and globally! </p>
							 	<p>iVetData links database to Vets' own CRM solution accessible anywhere and anytime in a secure cloud-based environment!</p>
								<ul>
									<li>Easy Creation and Access to Pet Records</li>
									<li>Customer Management System</li>
									<li>Complete privacy for Pet Owners</li>
									<li>Automatic Revenue and Inventory Control</li>
								</ul>
								<p>
									Now everyone can enjoy comprehensive shared Pet Database Nationwide and Worldwide searchable by Pet Microchip ID with Public result consists of Pet‘s Name, Vet’s Name, Clinic Address, and Contact Detail.
								</p>
								<p>
									Only a licensed Vet registered through this website can verify Pet’s ownership and to access basic level historical Pet's medical condition for more comprehensive diagnosis!
								</p>-->
							 <?php } ?>
								

							
							<!-- <ul class="list2 grey medium bottommargin_40">
								<li>Save Data and History of pets</li>
								<li>Connect to your  customer </li>
								<li>Safe and secure</li>
							</ul> -->
							<!-- <a href="<?php echo base_url() ?>about_us" class="theme_button color1 wide_button">Read More</a> --->
							<!--
								<div class="col-md-2 col-sm-12"></div>
						</div>
					</div>
					</div>
				</div>
			</section>		-->

			<!--<section id="services" class="ls ms section_padding_top_100 section_padding_bottom_100">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							
							<?php if ($lang == 'id'){ ?>
								<h2 class="section_header with_icon icon_color4">Layanan Kami</h2>
								<p class="small-text">Apa yang kami tawarkan untuk Anda</p>
							<?php }else{ ?>
								<h2 class="section_header with_icon icon_color4">Our Services</h2>
								<p class="small-text">What we offer</p>
							<?php } ?>
								
							
							
						</div>
					</div>
					<div class="row columns_margin_bottom_40">
						<div class="col-md-3 col-sm-6">
							<div class="teaser text-center">
								<img src="<?php echo base_url() ?>assets/images/services-icons/01.png" alt="" />
								<h4>
									<a href="#">PUBLIC PET DATABASE</a>
								</h4>
								<?php if ($lang == 'id'){ ?>
									<p>Dokter hewan dapat menikmati CRM berbasis cloud untuk merekam semua data pelanggan mereka dan catatan medis yang dapat dicari dengan Microchip / Nama.</p>
								<?php } else{ ?>
									<ul>
										<li style="list-style:none;">Public Pet Search Database</li>
										<li style="list-style:none;">Vet Search &amp; Atlas Database</li>
										<li style="list-style:none;">Pet News Database (RSS)*</li>

									</ul>
								<?php } ?>
								
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="teaser text-center">
								<img src="<?php echo base_url() ?>assets/images/services-icons/02.png" alt="" />
								<h4>
									<a href="#">PET OWNER&#39;S APP</a>
								</h4>
								<?php if ($lang == 'id'){ ?>
									<p>Dokter hewan dapat menikmati faktur digital melalui Saluran Pembayaran kami, dengan tanda terima yang dihasilkan secara otomatis yang dapat dikirim ke email pelanggan secara langsung.</p>
								<?php }else{ ?>
									<ul>
										<li style="list-style:none;">Public Pet Search Database</li>
										<li style="list-style:none;">Pet Owner-View  Medical Records</li>
										<li style="list-style:none;">Vet List Records</li>
										<li style="list-style:none;">Invoice Records</li>
										<li style="list-style:none;">Pet Supplies / Market Place*</li>
										<li style="list-style:none;">Vet-on-Call*</li>
										<li style="list-style:none;">Grooming-on-Call*</li>
									</ul>


								<?php } ?>
								
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-sm-offset-3 col-md-offset-0">
							<div class="teaser text-center">
								<img src="<?php echo base_url() ?>assets/images/services-icons/03.png" alt="" />
								<h4>
									<a href="#">VET CONNECT</a>
								</h4>
								<?php if ($lang == 'id'){ ?>
									<p>Dokter hewan dapat terhubung satu sama lain melalui Forum Dokter Hewan kami yang memungkinkan dokter hewan untuk mendiskusikan berbagai topik yang berbeda terkait dengan bisnis mereka.</p>
								<?php }else{ ?>
									<p>Vet can connect with each other through our Vet Forum that allows Vets to discuss various different topics related to their business.</p>
								<?php } ?>								
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-sm-offset-3 col-md-offset-0">
							<div class="teaser text-center">
								<img src="<?php echo base_url() ?>assets/images/services-icons/04.png" alt="" />
								<h4>
									<a href="#">VET PRODUCTS</a>
								</h4>
								<?php if ($lang == 'id'){ ?>
									<p>Dokter hewan dapat memesan produk impor dari kami seperti Pet Microchip, Pet Scanner, Pet Medicines, Pet Health Machines melalui permintaan pasokan digital.</p>
								<?php }else{ ?>
									<p>Vet can order imported products from us such as Pet Microchips, Pet Scanners, Pet Medicines, Pet Health Machines via digital supply request.</p>
								<?php } ?>								
							</div>
						</div>
					</div>-->
					<!-- <div class="row">
						<div class="col-sm-12 text-center">
							<a href="<?php echo base_url() ?>#service" class="theme_button color4 wide_button">All Services</a>
						</div>
					</div> -->
				<!--</div>
			</section>-->

			

			

			

			<!-- <section id="reviews" class="cs main_color3 parallax page_testimonials section_padding_top_100 section_padding_bottom_100">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color">
								What Clients Say
							</h2>
							<p class="small-text">Because pets can not tell you their impressions</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-10 col-md-offset-1 text-center">

							<div class="owl-carousel testimonials-owl-carousel" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-dots="true" data-nav="false">

								<blockquote class="with_quotes text-center">
									<img src="<?php echo base_url() ?>assets/images/faces/01.jpg" alt="" /> With the notion of just how much pet do hate sitting in the groomer's chair, Estelle Marsh undid it all for Enny!
									<div class="item-meta">
										<h3 class="bottommargin_10">Ray Cunningham</h3>
										<p class="small-text">Client</p>
									</div>
								</blockquote>

								<blockquote class="with_quotes text-center">
									<img src="<?php echo base_url() ?>assets/images/faces/01.jpg" alt="" /> With the notion of just how much pet do hate sitting in the groomer's chair, Estelle Marsh undid it all for Enny!
									<div class="item-meta">
										<h3 class="bottommargin_10">Ray Cunningham</h3>
										<p class="small-text">Client</p>
									</div>
								</blockquote>

								<blockquote class="with_quotes text-center">
									<img src="<?php echo base_url() ?>assets/images/faces/01.jpg" alt="" /> With the notion of just how much pet do hate sitting in the groomer's chair, Estelle Marsh undid it all for Enny!
									<div class="item-meta">
										<h3 class="bottommargin_10">Ray Cunningham</h3>
										<p class="small-text">Client</p>
									</div>
								</blockquote>


							</div>

						</div>
					</div>
				</div>
			</section> -->
			<section id="blog" class="ls section_padding_100">
				
<!--<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color1">
								Our Latest News
							</h2>
							<p class="small-text">Keeping up with iVet Data</p>
						</div>
					</div>
					<div class="row columns_margin_bottom_20">
						<?php foreach ($articlehome as $detail) { 
									$featuredimg = $detail->featuredimg;
								    $jmlfeaturedimg = strlen($featuredimg);
								    if($jmlfeaturedimg < 50){
								    	$urlfeaturedimg = base_url().'upload/article/'.$featuredimg;
								    }else{
								        $urlfeaturedimg = $featuredimg; 
								    } ?>	
							<div class="col-md-4 col-sm-6">
								<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
									<div class="item-media" style="height: 250px;">
										<img src="<?php echo $urlfeaturedimg ?>" alt="">

										<div class="media-links">
											<a href="<?php echo base_url() ?>blog/detail/<?php echo $detail->slug; ?>" class="abs-link"></a>
										</div>

										<a class="bottom-right-corner" href="<?php echo base_url() ?>blog/detail/<?php echo $detail->slug; ?>">
											<i class="fa fa-comment" aria-hidden="true"></i>
										</a>
									</div>
									<div class="item-content">
										<span class="entry-date small-text highlight3">
											<?php
															$datepublish = date('M d, Y', strtotime($detail->publishdate));
														 ?>
											<time class="entry-date" datetime="<?php echo $datepublish ?>">
												<?php echo $datepublish ?>
											</time>
										</span>
										<h4 class="entry-title hover-color3">
											<?php 
												$titlenya = strip_tags($detail->title);

												 ?>
											<a href="<?php echo base_url() ?>blog/detail/<?php echo $detail->slug; ?>"><?php echo substr($titlenya, 0, 50); ?></a>
										</h4>
										<p>
								<?php 
												$descibe = strip_tags($detail->description);

												echo substr($descibe, 0, 100); ?>...
							</p>
										<a href="<?php echo base_url() ?>blog/detail/<?php echo $detail->slug; ?>" class="read-more"></a>
									</div>
								</article>
							</div>	
						<?php } ?>					
					</div>
				</div>
-->

			</section>
			<section id="about" class="ls section_padding_top_50 section_padding_bottom_0">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<?php if ($lang == 'id'){ ?>
								<h2 class="section_header with_icon icon_color">
								Tentang iVet Data
							</h2>
							<p class="small-text">One Stop Solution for Your Companion Animals</p>
							<?php }else{ ?>
								<h2 class="section_header with_icon icon_color">
								About iVet Data
							</h2>
							<p class="small-text">One Stop Solution for Your Companion Animals</p>
							<?php } ?>
							<img src="<?php echo base_url() ?>assets/images/adorable.jpg" alt="" />
						</div>
					</div>
					<div class="row" style="margin-top: 25px;">		
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>			
						<div class="col-md-8 col-sm-12">
						<?php if ($lang == 'id'){ ?>
							<p>
								Daftarkan identifikasi hewan peliharaan Anda di pet database iVetData untuk solusi komprehensif yang dapat mengelola informasi pasien dan klien Anda yang terhubung ke Dokter Hewan berlisensi secara lokal dan global!</p>

							<p>iVetData menghubungkan pet database dan CRM Vets sendiri yang dapat diakses di mana saja dan kapan saja berbasis cloud yang aman!</p>
							
							<p>
								Akses Mudah ke rekam medis Hewan Peliharaan dengan Sistem Manajemen untuk pelanggan. Dilengkapi pendataan untuk Pemilik Hewan Peliharaan, invoice, dan Inventaris.
							</p>
							<p>
								Sekarang semua orang dapat menikmati pet database bersama yang komprehensif di seluruh negeri dan di seluruh dunia yang dapat ditelusuri oleh Pet Microchip ID dengan informasi yang tersedia yaitu Nama Pet, Nama Dokter Hewan, Alamat Klinik, dan Detail Kontak.
							</p>
							<p>
								Hanya dokter hewan berlisensi yang terdaftar melalui situs web ini yang dapat memverifikasi kepemilikan hewan peliharaan dan untuk mengakses kondisi medis hewan peliharaan untuk diagnosis yang lebih komprehensif!
							</p>

						<?php }else{?>
							<p>
							<b>PT IVET DATA GLOBAL</b> (est. 2018) headquartered in Jakarta, Indonesia as a pioneer in Pet Database and Clinic CRM in the region, endorsed by Indonesian Veterinary Medical Association (PDHI) in Indonesia.
							</p>
							<p>
								We are also the first company to partner with Datamars SA to introduce country-coded microchips which follow ICAR (the International Committee for Animal Recording) ISO11784/11785 standards. 
							</p> 
							<p>
								Formed to accommodate the growing demand for digital solutions in veterinary practice and to support the new Government regulation on pet’s "National ID” identification, we believe in providing new technology for Pet Owners and Vets that are comprehensive, affordable, and secure.
							</p>

						<?php }?>
							<!-- <a href="<?php echo base_url() ?>about_us" class="theme_button color1 wide_button">Read More</a> -->
						</div>
						<div class="col-md-2 col-sm-12 empty">&nbsp;</div>
					</div>
					</div>
				</div>
			</section>