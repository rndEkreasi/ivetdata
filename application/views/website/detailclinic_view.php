<!--<section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				 <h2 class="highlight">News & Article</h2> 
				<ol class="breadcrumb darklinks">
					<li>
						<a href="<?php echo base_url() ?>">
							Vet Clinic
						</a>
					</li>
					<li class="active">
						<a href="#"><?php echo $nameclinic ?></a>
					</li>
				</ol>
			</div>
		</div>
	</div>
</section> -->
<section class="ls page_portfolio section_padding_bottom_75">
<div class="col-sm-12 text-center">
	<h2 class="section_header with_icon icon_color">Vet Search</h2>
</div>
</section>
			<section id="about" class="ls section_padding_top_50 section_padding_bottom_100">
				<div class="container">
					<?php foreach ($dataclinic as $clinic) {
						$pictureclinic = $clinic->picture;
					?>					
					<div class="row">
						<!--<div class="col-md-6">
							<?php if($pictureclinic == ''){

                            }else{ ?>
                                <img src="<?php echo $pictureclinic ?>" style="margin: 0px 0px 20px;"><br>
                            <?php } ?>
							 <img src="<?php echo base_url() ?>assets/images/gallery/01.jpg" alt="" /> 
							 <iframe width="100%" height="315" src="https://www.youtube.com/embed/H5HTUzCKUfE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>--> 
						<div class="col-md-8" style="margin-left:10%;">
							<h3><?php echo $clinic->nameclinic ?></h3>
							<ul class="list1 no-bullets">
                                <li>
                                	<div class="media small-teaser">
                                    	<div class="media-body media-middle">
                                    		<div class="teaser_icon label-warning fontsize_16"><i class="rt-icon2-pin-alt"></i></div>                                        	
                                        </div>
                                        <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                        	<strong class="grey">Address : </strong> <?php echo $clinic->address; ?>, <?php echo $clinic->city; ?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                	<div class="media small-teaser">
                                    	<div class="media-body media-middle">
                                    		<div class="teaser_icon label-info fontsize_16"><i class="rt-icon2-mail"></i></div>                                    		
                                        </div>
                                        <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                        	<strong class="grey">Email : </strong> <a href="mailto:<?php echo $clinic->email; ?>"> <?php echo $clinic->email; ?></a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media small-teaser">
                                        <div class="media-body media-middle">
                                        	<div class="teaser_icon label-success fontsize_16"><i class="rt-icon2-phone"></i></div>                                        	
                                        </div>
                                        <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">
                                            <strong class="grey">Phone : </strong> <a href="tel:<?php echo $clinic->phone; ?>"> <?php echo $clinic->phone; ?></a>
                                        </div>
                                	</div>
                                </li>
                                <!--<li>-->
                                <!--    <div class="media small-teaser">-->
                                <!--        <div class="media-body media-middle" style="padding: 0px 0px 0px 10px;">-->
                                <!--           <a href="<?php echo base_url() ?>vets/booking/?clinic=<?php echo $clinic->idclinic; ?>" class="theme_button color2">Booking service</a>-->
                                <!--        </div>-->
                                <!--	</div>-->
                                <!--</li>-->
                            </ul>
							<!-- <a href="<?php echo base_url() ?>about_us" class="theme_button color1 wide_button">Read More</a> -->
						</div>
					</div>
					<?php } ?>
				</div>
			</section>

			

			
			<!-- <section id="blog" class="ls section_padding_100">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="section_header with_icon icon_color3">
								From Our Blog
							</h2>
							<p class="small-text">Our latest news</p>
						</div>
					</div>
					<div class="row columns_margin_bottom_20">
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/05.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 12, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Shankle doner ribeye ham hock shank</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/06.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 13, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">T-bone capicola kevin pancetta</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
						<div class="col-md-4 col-sm-6">
							<article class="vertical-item content-padding with_shadow text-center rounded overflow-hidden">
								<div class="item-media">
									<img src="<?php echo base_url() ?>assets/images/gallery/07.jpg" alt="">

									<div class="media-links">
										<a href="blog-single-left.html" class="abs-link"></a>
									</div>

									<a class="bottom-right-corner" href="blog-single-left.html#comments">
										<i class="fa fa-comment" aria-hidden="true"></i>
									</a>
								</div>
								<div class="item-content">
									<span class="entry-date small-text highlight3">
										<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
											June 14, 2017
										</time>
									</span>
									<h4 class="entry-title hover-color3">
										<a href="blog-single-left.html">Beef tri-tip brisket jerky boudin</a>
									</h4>
									<p>
							Fatback jerky pig sirloin. Beef landja pork beef ribs kielbasa shoulder tongue pig venison pork chop.
						</p>
									<a href="blog-single-left.html" class="read-more"></a>
								</div>
							</article>
						</div>
					</div>
				</div>
			</section> -->
