<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Clinic</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div id="map" style="height: 100%;min-height:750px; "></div>
    <script>
      var customLabel = {
        restaurant: {
          label: 'iVetdata'
        },
        bar: {
          label: 'R',
          icon: 'https://www.ivetdata.com/assets/images/icon.png',
        }
      };

        function initMap() {
        //   // Try HTML5 geolocation.
        // if (navigator.geolocation) {
        //     navigator.geolocation.getCurrentPosition(function(position) {
        //       var pos = {
        //         lat: position.coords.latitude,
        //         lng: position.coords.longitude
        //       };

        //       infoWindow.setPosition(pos);
        //       infoWindow.setContent('Location found.');
        //       infoWindow.open(map);
        //       map.setCenter(pos);
        //     }, function() {
        //       handleLocationError(true, infoWindow, map.getCenter());
        //     });
        //   } else {
        //     // Browser doesn't support Geolocation
        //     handleLocationError(false, infoWindow, map.getCenter());
        //   }
        // }

        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-6.175110, 106.865036),
          //center: new google.maps.LatLng(lat, lang,
          //center: new google.maps.LatLng(-2.548926, 118.0148634),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;

        // Resize stuff...
        google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center); 
        });

          // Change this depending on the name of your PHP or XML file
          downloadUrl('<?php echo base_url()?>vets/vetsxml', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var url = markerElem.getAttribute('url');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: 'https://www.ivetdata.com/assets/images/icon.png',
                url: url,
                //label: icon.label
              });
              marker.addListener('mouseover', function() {
                  infoWindow.setContent(infowincontent);
                  infoWindow.open(map, marker);
                  //infowindow.open(map, this);
              });

              // assuming you also want to hide the infowindow when user mouses-out
              marker.addListener('mouseout', function() {
                  infowindow.close();
              });
              marker.addListener('click', function() {                
                window.location.href = this.url;
              });

              // google.maps.event.addListener(marker, 'click', function() {
              //     window.location.href = this.url;
              // });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDojAinyL0H2DqwPclaAal03Dx4xAapDk&callback=initMap"
    async defer></script>
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>All Clinic : <?php echo $all_clinic ?></h3>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <!-- <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> --
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> --
                                            </form>
                                        </div>

                                    </div> -->
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No</th>
                                            <th>Clinic ID </th>
                                            <th>Name Clinic</th>
                                            <th>Status</th>
                                            <th>City</th>                                            
                                            <th>Pet</th>
                                            <!-- <th>Chip Available</th> -->
                                            <th>Email</th>
                                            <th>Phone</th>
                                        </tr>
                                        <?php
                                         $ij = $from + 1 ;
                                         foreach ($dataclinic as $clinic) { 
                                            // $tipe = $clinic->tipe;
                                            // $photo = $clinic->photo;
                                            
                                            // if($photo =='' ){
                                            //     $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            // }else{
                                            //     $petphoto = $photo;
                                            // }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td class="media-middle">
                                                <a href="#"><?php echo $ij++ ?></a>                                               
                                            </td>
                                            <td class="media-middle">
                                                <a href="#"><?php echo $clinic->idclinic; ?></a>                                               
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="#"><?php echo $clinic->nameclinic ?></a>
                                                </h5>
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <a href="#"><?php // echo $clinic->nameclinic ?>Free</a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <a href="#"><?php echo $clinic->city; ?></a>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <!-- <td class="media-middle">
                                                <?php echo $clinic->team; ?>
                                            </td> -->
                                            <td class="media-middle">
                                                <?php echo $clinic->pet; ?>
                                            </td>
                                            <!-- <td class="media-middle">
                                                <?php echo $clinic->chip; ?>
                                            </td> -->
                                            <td class="media-middle">
                                                <?php echo $clinic->email; ?>
                                            </td>
                                            <td class="media-middle">
                                                <?php echo $clinic->phone; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <?php echo $this->pagination->create_links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="<?php echo base_url() ?>assets/js/compressed.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>


</body>

</html>