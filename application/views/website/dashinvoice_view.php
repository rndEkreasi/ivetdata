<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">Buy Subscription</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
<section class="intro_section page_mainslider ls ms pricing py-5"> 
    <div class="container">
        <div class="row">  
                    <div class="col-md-12">
                      <?php if (isset($error)){ ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                      <?php } ?>
                      <?php if (isset($success)){ ?>
                        <div class="alert alert-success"><?php echo $success ?></div>
                      <?php } ?>
                        <ul class="nav nav-tabs login-tabs mt-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link border-white text-white active show" data-toggle="tab" href="#signin" role="tab" aria-selected="true">Invoice Payment</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link border-white text-white" href="<?php echo base_url() ?>register/petowner" aria-selected="false">Pet Owner</a>
                            </li> -->
                        </ul>
                        <!-- tabs content start here -->
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <form action="<?php echo base_url() ?>Register/service" method="post">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Detail Invoice Payment</h4>
                                <div class="col">                            
                                <div id="accordion2">                                    
                                    
                                    <div class="card mb-1 border-0 rounded-0">
                                        <div class="card-header bg-primary rounded-0 py-2" id="headingOne>">
                                            
                                        </div>

                                        <div id="service1" class="collapse show" data-parent="#accordion2">
                                            <div class="card-body" style="text-align: center;">
                                            <div id="order_review" class="shop-checkout-review-order">
                                            <table class="table shop_table shop-checkout-review-order-table">
                                            <!-- <thead>
                                                <tr>
                                                    <td class="product-name">Product</td>
                                                    <td class="product-total">Total</td>
                                                </tr>
                                            </thead> -->
                                            <tbody>
                                                <tr class="cart_item">  
                                                    <td><strong>Inv : </strong> </td>
                                                    <td> <strong>#<?php echo $invoice ?></strong></td>
                                                </tr>
                                                <tr class="cart_item"> 
                                                    <td>Name :</td>
                                                    <td><?php echo $nama ?></td>
                                                </tr>
                                                <tr class="cart_item"> 
                                                    <td>Item : </td>
                                                    <td><?php echo $description ?></td>
                                                </tr>
                                                <tr class="cart_item"> 
                                                    <td>Price : </td>
                                                    <td>Rp. <?php echo number_format($price) ?></td>
                                                </tr>
                                                <tr class="cart_item"> 
                                                    <td>Discount :</td>
                                                    <td> Rp. <?php echo number_format($discount) ?></td>                                               
                                                </tr>
                                                <tr class="cart_item"> 
                                                    <td>Unik Code : </td>
                                                    <td>Rp. <?php echo number_format($unik) ?></td>
                                                </tr>
                                                <tr class="cart_item"> 
                                                    <td><strong>Total Price : </strong> </td>
                                                    <td><strong>Rp. <?php echo number_format($totalprice) ?></strong></td>
                                                </tr>
                                                <tr class="cart_item">
                                                    <td colspan="2">   
                                                        Please Transfer to <br>
                                                        Bank : BCA - Diana Sanjoto PT. Ivet Data Indonesia<br>
                                                        No Acc : 4890277719<br><br>
                                                        <a href="https://api.whatsapp.com/send?phone=6285697117560&text=Halo%20iVetdata,%20Saya%20<?php echo $nama ?>%20Akan%20Segera%20Bayar%20Rp.%20<?php echo number_format($totalprice) ?>%20untuk%20Invoice%20<?php echo $invoice ?>%20Segera%20akan%20kirimkan%20bukti%20transfer" class="icon-tab theme_button color4">Payment Confirmation</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="uid" value="<?php // echo $uid ?>">
                                    
                                </div> 
                                    
                                </div>
                                <div class="row mx-0 justify-content-end no-gutters">
                                    <!-- <div class="col-6">
                                        <a href="<?php echo base_url() ?>welcome" class="btn btn-block gradient border-0 z-3">Login</a>
                                    </div> -->
                                    <!-- <div class="col-6">                                        
                                        <input type="submit" class="btn btn-block gradient border-0 z-3" value="Register">                                        
                                    </div> -->
                                </div>
                              </form>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                </div>
    </section>