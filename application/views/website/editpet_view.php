 <section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb darklinks">
                            <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                            <li class="active">Edit Pet Data</li>
                        </ol>
                    </div>
                    <!-- .col-* -->
                    <div class="col-md-6 text-md-right">
                         <!--<span> <?php echo date('D d, M Y');?></span>-->
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </section>
        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Edit Pet Data</h3>
                        </div>
                    </div>
                    <?php
                    //if($role == '0'){
                    //    $disabled = 'readonly';
                    //}else{
                        $disabled = '';
                    //}?>
                    <!-- .row -->
                    <?php foreach ($datapet as $pet) {
                        $gender = $pet->gender;                       
                        $neutered = $pet->neutered;
                        $microchip = $pet->microchip; 
                        $idclinic = $pet->idclinic;
                     ?>                    
                    <form class="form-horizontal" action="<?php echo base_url() ?>pet/editprocess" method="post" enctype="multipart/form-data" style="padding:10px;">
                        <div class="row">
                            <div class="col-md-8">
                            <?php
                                $msg_error = $this->session->flashdata('error');
                                $msg_success = $this->session->flashdata('success');
                            ?>
                                <?php if (isset($msg_error)){ ?>
                                    <div class="alert alert-danger"><?php echo $msg_error; ?></div>
                                <?php } ?>

                                <?php if (isset($msg_success)){ ?>
                                    <div class="alert alert-success"><?php echo $msg_success; ?></div>
                                <?php } ?>       

                                <div class="with_padding">
                                    <h5>Pet Info</h5> 
                                    <hr> 

                                    <!-- <div class="row form-group">                                        
                                        <label class="col-lg-3 control-label">ID Identification : </label>
                                        <div class="col-lg-9">
                                            <select name="microchip" class="form-control" id="chooseuniq" required>    

                                                <?php if ($microchip == '1'){ ?>
                                                    <option value="1">Microchip</option>
                                                    <option value="0">Uniq id</option>
                                                <?php }else{ ?>
                                                    <option value="0">Uniq id</option>
                                                    <option value="1">Microchip</option>                                                
                                                <?php } ?>
                                                                                               
                                            </select>
                                                                                      
                                        </div>
                                    </div> -->
                                    <div class="row form-group" id="microchip">                                        
                                        <label class="col-lg-3 control-label">Microchip ID : </label>
                                        <div class="col-lg-9">
                                            <input type="number" <?php echo $disabled ?> name="rfid" pattern=".{15,16}" value="<?php echo $pet->rfid ?>" class="form-control active" placeholder="Masukan No Microchip">
                                        </div>
                                    </div>
                                    <!-- <div class="row form-group" id="uniqid">                                        
                                        <label class="col-lg-3 control-label">Uniq ID : </label>
                                        <div class="col-lg-9">
                                            <input type="number" name="clinicid" style="width: 20%;float: left;" class="form-control active" value="<?php echo $idclinic ?>" readonly required>
                                            <input type="number" name="rfiduniq" pattern=".{3,11}" value="<?php echo $pet->rfid ?>" class="form-control active" placeholder="Masukan angka unik 3-10 Angka" style="width: 80%;float: left;">
                                        </div>
                                    </div> -->
                                    
                                    
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Pet Name* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="namepet" <?php echo $disabled ?> value="<?php echo $pet->namapet; ?>" class="form-control active"  required>                                        
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Type* : </label>
                                        <div class="col-lg-9">
                                           <select name="type" class="form-control" <?php echo $disabled ?>>                                                
                                                <option value="<?php echo $pet->tipe; ?>"><?php $tipe = $pet->tipe; echo $tipe;?></option>
                                                <?php foreach ($pettype as $type) { ?>
                                                    <option value="<?php echo $type->type ?>"><?php echo $type->type ?></option>
                                                <?php } ?>                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Breed* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="breed" <?php echo $disabled ?> id="breed" value="<?php echo $pet->breed; ?>" class="form-control active" required>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Color* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="color" <?php echo $disabled ?> value="<?php echo $pet->color; ?>" class="form-control active" required >
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Gender* : </label>
                                        <div class="col-lg-9">
                                            <input type="radio" name="gender" <?php echo $disabled ?> value="male" <?php if ($gender == 'male') { echo 'checked'; } ?> > Male    <input type="radio" <?php echo $disabled ?> name="gender" value="female" <?php if ($gender == 'female') { echo 'checked'; } ?>> Female

                                            <!-- <select name="gender" class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Neutered* : </label>
                                        <div class="col-lg-9">
                                            <input type="radio" <?php echo $disabled ?> name="neutered" value="1" <?php if ($neutered == '1') { echo 'checked'; } ?>>Yes <input type="radio" name="neutered" value="0" <?php if ($neutered == '0') { echo 'checked'; } ?>>No

                                            <!-- <select name="gender" class="form-control">
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Date Of Birth* : </label>
                                        <div class="col-lg-9">
                                            <div class="input-group date" data-provide="datepicker">                                                
                                                <input type="text" data-date-format="mm/dd/yyyy" name="datebirth"
                                                value="<?php echo date('m/d/Y',strtotime($pet->datebirth)); ?>"
                                                    class="js-date form-control" required>
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
										<label class="col-lg-3 control-label"><br>Display Information in Public Pet Database :</label>
										<div class="col-lg-9" style="border-radius:15px; border:1px solid #ccc;background: #6600;"><br>
											<input type="checkbox" name="permission[]" value="0" <?php if($pet->public_info[0]==1)echo "checked"; ?>> Pet's Name<br>
											<input type="checkbox" name="permission[]" value="1" <?php if($pet->public_info[1]==1)echo "checked"; ?>> Pet's ID<br>
											<input type="checkbox" name="permission[]" value="2" <?php if($pet->public_info[2]==1)echo "checked"; ?>> Pet's Last Vaccine<br><br>
										</div>
									</div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Edit Photo :<br>Max. Size 10 MB <br> Extension : jpg | png | jpeg </label>
                                        <div class="col-lg-9">
                                            <?php
                                                $photo = $pet->photo;
                                                if($photo =='' ){
                                                    $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                                }else{
                                                    $petphoto = $photo;
                                                };
                                             ?>
                                             <img id="myImg" src="<?php echo $petphoto ?>" style="width: 300px;"><br>
                                             <div style="visibility: visible; margin-left: 30px; margin-top: 10px;" id="conbutton">
														<input type="image" src="/assets/images/rotate_right.png" onclick="rotateimg('myImg');return false;" border="1">&nbsp;
														<input type="image" src="/assets/images/reset.png" onclick="i=0;$('#myImg').rotate(0);$('#rotate').val(0);return false;" border="1">&nbsp;
														<input type="image" src="/assets/images/rotate_left.png" onclick="rotateimgrev('myImg');return false;" border="1">
											</div>
                                            <input type="file" name="photopet" id="inputimage" class="form-control active" >
                                            <input type="hidden" name="idpet" value="<?php $idpet = $pet->idpet; echo $idpet; ?>">
                                            <input type="hidden" value="no" name="rotate" id="rotate" />
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <button type="submit" class="theme_button wide_button">Update Pet</button>
                                        </div>
                                    </div> -->
                                    <!-- .row  -->

                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->
                        </div>
                        <!-- .row  -->
                    

                    <div class="row">
                        <div class="col-sm-12">
                        </div>
                    </div>
                    <!-- .row -->

                        <div class="row">
                            <div class="col-md-8">                                      

                                <div class=" with_padding">
                                    
                                    <h5>Edit Owner</h5> 
                                    <hr> 
                    <?php
                    if($role == '0'){
                        $disabled = 'readonly';
                        echo '<h5>Please update your information from "Edit Profile" button on the navigation bar</h5>';
                    }else{
                        $disabled = '';
                    }?>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Customer Name* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="nameowner"  value="<?php if (isset($customername)){ echo $customername; } ?>" class="form-control active" <?php echo $disabled ?> required >
                                            <input type="hidden" name="idcustomer" value="<?php echo $idcustomer ?>" >
                                            <input type="hidden" name="idpet" value="<?php echo $this->input->get('id'); ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Phone* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="customerphone" <?php echo $disabled ?> value="<?php if (isset($customerphone)){ echo $customerphone; } ?>" class="form-control active"required >
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Email: </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="customeremail" <?php echo $disabled ?> value="<?php if (isset($customeremail)){ echo $customeremail; } ?>" class="form-control active">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Address* : </label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control" name="customeraddress" <?php echo $disabled ?>><?php if (isset($customeraddress)){ echo $customeraddress; } ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">City* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control" value="<?php if (isset($customercity)){ echo $customercity; } ?>" name="customercity" aria-label="city" required <?php echo $disabled ?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Country* : </label>
                                        <div class="col-lg-9">
                                            <select class="form-control" name="customercountry" aria-label="country" required <?php echo $disabled ?>>
                                              <!--<option value="<?php echo $customercountry ?>"><?php echo $customercountry ?></option>-->
                                              <?php foreach ($datacountry as $country) { ?>
                                                   <option value="<?php echo $country->namecountry?>"> <?php echo $country->namecountry?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div> 

                                    <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <button type="submit" class="theme_button wide_button" >Update Pet</button>
                                        </div>
                                    </div>
                                    <!-- .row  -->

                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->
                        </div>
                        <!-- .row  -->


                    </form>
                    <?php } ?>


                </div>
                <!-- .container -->
            </section>
        
    </div>
</div>
</section>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- <script src="//geodata.solutions/includes/countrystatecity.js"></script> -->
    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script> -->
    <script src="<?php echo base_url() ?>/assets/js/datepicker-no-future.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
    <script src="/assets/js/jQueryRotate.js"></script>
    <script type="text/javascript">

    	var i = 0;
    	
    	function rotateimg(e){
    		i = i + 90;
    		$('#'+e).rotate(i);
    		$('#rotate').val(i);
    		if(i==360)i=0;
    	}
    	
    	function rotateimgrev(e){
    		i = i - 90;
    		$('#'+e).rotate(i);
    		$('#rotate').val(i);
    		if(i==-360)i=0;
    	}
    	
    	function handleLocalFile(file) {
    		if (file.type.match(/image.*/)) {
    			var reader = new FileReader();
    			reader.onload = function (e) {
    				//detectNewImage(e.target.result, async);
    				document.getElementById("myImg").src=reader.result;
    				//document.getElementById("myImg").style.width='350px';
    				//document.getElementById("myImg").style.height='350px';
    				document.getElementById("conbutton").style.visibility='visible';
    				i=0;			
    			};
    			reader.readAsDataURL(file);
    		}
    	}
    
    	document.getElementById("inputimage").addEventListener("change", function (e) {
    		var files = this.files;
    		if (files.length)
    			handleLocalFile(files[0]);
    	});
    
    	$(document).ready(function(){
    		$("#myImg").on('DOMSubtreeModified',function(){
    			$('#myImg').faceDetection({
    				complete: function (faces) {
    					console.log(faces);
    					if(faces.length<1){
    						$("#myImg").rotate(90);
    						$("#rotate").val(90);
    						i = 90;
    					}
    				}
    			});
    		});
    	});
    	
        $(document).ready(function(){
 
            $('#breed').autocomplete({
                source: "<?php echo site_url('pet/breedlist');?>",
      
                select: function (event, ui) {
                    $('[name="breed"]').val(ui.item.label); 
                    $('[name="description"]').val(ui.item.description); 
                }
            });
            // <?php if ($microchip == '1'){ ?>
            //     $('#microchip').show();
            //     $('#uniqid').hide();
            // <?php }else{ ?>
            //     $('#uniqid').show();
            //     $('#microchip').hide();
            // <?php }?>
            
            
            // $("#chooseuniq").change(function(){
            //     if($('#chooseuniq').val() == '1') {
            //         $('#microchip').show(); 
            //         $('#uniqid').hide();
            //         var uniqid = document.getElementById("uniqid");  
            //         uniqid.removeAttribute("required");
            //     }else {
            //         $('#microchip').hide(); 
            //         $('#uniqid').show();
            //         var microchip= document.getElementById("microchip");  
            //         microchip.removeAttribute("required");
            //     } 
            // });
            

            $('input.choosecustomer').autocomplete({
                // Get the current row
                // var row = $(this).closest('tr');
                // // Get the price
                // var price = parseFloat(row.find('.price').val());                
                source: "<?php echo site_url('customer/allsearch/?');?>",
      
                select: function (event, ui) {
                    //var row = $(this).closest('tr');

                    $('#emailcustomer').val(ui.item.email); 
                    $('#namecustomer').val(ui.item.description);
                    $('#address').val(ui.item.address);
                    $('#city').val(ui.item.city);
                    //price.val(ui.item.label);                     
                }                
            });
 
        });

        $(function() {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                endDate: '+0d',
                autoclose: true
            });
        });
    </script>


</body>

</html>