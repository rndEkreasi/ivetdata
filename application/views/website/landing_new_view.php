<body>
			<!--<div role="main" class="main">-->
			<div>
                <div class="findyourpet mb-0">
                <div class="container">
					<div class="flex-md-row justify-content-between">
						<div class="col-sm-12 col-md-6 boxbg">
						    <h1 class="tp-caption custom-primary-font font-weight-normal mb-1 res-title" style="white-space: normal; color:#e64d5f;">YOUR DIGITAL VET COMPANION</h1>

						    <p style="color:#333;">Your Pocket Vet App that can be accessible anytime and anywhere through a digitalized Veterinarian Network for online consultation, pet identification, medical records, scheduling & reminder, as well as unique purchases!</p>
						    <div class="tp-caption " style="white-space: normal;">

							<a class="tp-caption btn btn-secondary font-weight-bold custom-btn-style-1"
							href="/register"
							data-x="['center','center','center','center']"
							data-y="center" data-voffset="['50','50','50','50']"
							data-start="1900"
							data-fontsize="['14','14','14','20']"
							data-paddingtop="['11','11','11','16']"
							data-paddingbottom="['11','11','11','16']"
							data-paddingleft="['32','32','32','42']"
							data-paddingright="['32','32','32','42']"
							data-transform_in="y:[100%];opacity:0;s:500;"
							   data-transform_out="opacity:0;s:500;">REGISTER NOW</a>
						</div>
						</div>
					    <!--
						<div class="col-sm-12 col-md-6 boxbg ">
						   <h4>Find Your Pet's Vet</h4>
						   <div id="formmicrochipid"><form name="rfid" action="/pet/search" method="get">
							<input  style="display:inline-block;" type="text" id="idpet" name="idpet" class="form-control" placeholder="MICROCHIP ID">
							<button style="display:inline; position: absolute; right:0; height: 34px; line-height:12px; border-radius: 0 6px 6px 0!important; margin-right: 30px; padding-left: 1em; padding-right:1em;" id="search" class="tp-caption btn btn-tertiary font-weight-bold custom-btn-style-1" style="background-color:orange;"	onclick="if(document.getElementById('idpet').length==0){return;}else{document.rfid.submit();}">FIND</button>
							<p>Enter the microchip ID above and click "Search". Enter only the 9, 10 or 15 character microchip number, with no punctuation or spaces.</p>
							
							</form>
						</div>
						</div>-->
					</div>
				</div></div>
