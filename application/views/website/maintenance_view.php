<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $title ?></title>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="https://ivetdata.com/assets/images/icon.png"/>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css" class="color-switcher-link">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131768019-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131768019-1');
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c792599a726ff2eea5a1b14/default';;
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

    <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.min.js"></script>
        <script src="js/vendor/respond.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <![endif]-->

</head>

<body>
    <!--[if lt IE 9]>
        <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->

    <div class="preloader">
        <div class="preloader_image"></div>
    </div>

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="rt-icon2-cross2"></i>
            </span>
        </button>
        <div class="widget widget_search">
            <form method="get" class="searchform search-form form-inline" action="./">
                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form>
        </div>
    </div>

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
        <ul class="list-unstyled">
            <li>Message To User</li>
        </ul>
        -->

        </div>
    </div>
    <!-- eof .modal -->

    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->

            <section class="page_topline with_search ls ms section_padding_10 table_section">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 text-center text-sm-left">
                            <!-- <div class="col-sm-6 page_social_icons greylinks" style="width: 180px;float: left;">
                                    <a class="social-icon rounded-icon border-icon soc-facebook" href="https://www.facebook.com/ivetdata/" target="_new" title="Facebook iVetData"></a>
                                    <a class="social-icon rounded-icon border-icon soc-twitter" href="https://twitter.com/ivetdata" target="_new" title="Twitter iVetData"></a>
                                    <a class="social-icon rounded-icon border-icon soc-instagram" href="https://www.instagram.com/ivetdata/" target="_new" title="IG iVetData"></a>
                            </div> -->
                            <!-- <div style="font-size: 10px;font-family: arial;margin: 0px 0px -51px;display: inline-block;color: #000;padding: 0px 53px;">In association with</div> -->
                            <a href="http://pdhi.or.id" target="_new"><img src="<?php echo base_url() ?>assets/images/pdhi.png" style="width: 235px;margin: 5px 0px 0px;"></a>
                            
                        </div>
                        <div class="col-sm-4 text-center text-sm-right">
                            <div id="google_translate_element" class="col-sm-6" style="width: 332px;height: 41px;overflow: hidden;margin: 0px 0px -21px;"></div>
                            <!-- <div class="widget widget_search">
                                <form method="get" class="searchform form-inline" action="./">
                                    <div class="form-group-wrap">
                                        <div class="form-group margin_0">
                                            <label class="sr-only" for="topline-search">Search for:</label>
                                            <input id="topline-search" type="text" value="" name="search" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="theme_button">Search</button>
                                    </div>
                                </form>
                            </div> -->

                        </div>
                    </div>
                </div>
            </section>

            <section class="page_toplogo table_section table_section_md ls section_padding_top_15 section_padding_bottom_15">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 text-center text-md-left">
                            <a href="" class="logo top_logo">
                                <img src="<?php echo base_url() ?>assets/images/logo.png" alt="">
                            </a>
                        </div>
                        <div class="col-md-6 text-center text-md-right">
                            <!-- <div class="inline-teasers-wrap" style="width: 300px;margin: 0px 0px 0px 0px;">
                                <div class="small-teaser text-left">
                                    <form class="form-inline" method="get" action="<?php echo base_url() ?>pet/search/">
                                        <div class="form-group" style="width: 100% !important;">
                                            <label class="sr-only" for="exampleInputEmail3">Pet ID</label>
                                            <input type="text" style="width: 300px !important;" class="form-control" name="idpet" id="exampleInputEmail3" placeholder="PET ID">
                                            <button type="submit" class="theme_button color1">Find</button>
                                        </div>  
                                    </form>
                                    
                                </div>
                            </div> -->

                        </div>
                        <div class="col-md-3 text-center text-md-left">
                            
                            
                        </div>

                    </div>
                </div>
            </section>

            <header class="page_header header_white toggler_left with_top_border item_with_border">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 display_table">

                            <div class="header_mainmenu display_table_cell">
                                
                                <!-- header toggler -->
                                <span class="toggle_menu">
                                    <span></span>
                                </span>
                            </div>

                            <div class="header_right_buttons display_table_cell text-right">
                                <div class="col-sm-8 text-center text-sm-left">
                                    <div class="col-sm-6 page_social_icons greylinks" style="width: 180px;float: left;">
                                            <a class="social-icon rounded-icon border-icon soc-facebook" href="https://www.facebook.com/ivetdata/" target="_new" title="Facebook iVetData"></a>
                                            <a class="social-icon rounded-icon border-icon soc-twitter" href="https://twitter.com/ivetdata" target="_new" title="Twitter iVetData"></a>
                                            <a class="social-icon rounded-icon border-icon soc-instagram" href="https://www.instagram.com/ivetdata/" target="_new" title="IG iVetData"></a>
                                    </div>
                                    <!-- <div id="google_translate_element" class="col-sm-6" style="width: 162px;height: 51px;overflow: hidden;"></div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
			<section class="ls ms section_404 background_cover section_padding_top_100 section_padding_bottom_130">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<div class="inline-block text-center">
								<p class="not_found">
						<span class="highlight2">400</span>
					</p>
								<h3>Server Maintenance, Please Come Back Later</h3>
								
								<p>
									<a href="<?php echo base_url() ?>" class="theme_button color3 wide_button">Back to Home</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>

			