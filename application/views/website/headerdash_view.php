<?php 

    if(!isset($_COOKIE["reminder"])) setcookie("reminder","0",time()+86400); 
    if(!isset($_COOKIE["expiry"])) setcookie("expiry","0",time()+86400); 

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $title ?></title>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="One-Stop Solution for your Companion Animals!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/assets/images/icon.png"/>
    <link rel="apple-touch-icon" href="/assets/images/ivet.png">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/fonts.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css" class="color-switcher-link">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dashboard.css" class="color-switcher-link">
    <script src="<?php echo base_url() ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
    
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui.css'?>">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131768019-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131768019-1');
</script>
<script>
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 3000);
</script>
<!--Start of Tawk.to Script-->
<?php /*
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c792599a726ff2eea5a1b14/default';;
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


    <!--[if lt IE 9]>
        <script src="<?php echo base_url() ?>assets/js/vendor/html5shiv.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/vendor/respond.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <![endif]-->
*/?>

  <?php $this->load->view('sendbird'); ?>
</head>

<body class="admin">
    <?php $this->load->view('fix_loading'); ?>
<?php 
    $expired = $this->session->userdata['logged_in']['expired'];
    $idservice = $this->session->userdata['logged_in']['idservice'];
?>    
    <!--[if lt IE 9]>
        <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
    <![endif]-->

    <!-- <div class="preloader">
        <div class="preloader_image"></div>
    </div> -->

    <!-- search modal -->
    <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="rt-icon2-cross2"></i>
            </span>
        </button>
        <div class="widget widget_search">
            <!-- <form method="get" class="searchform search-form form-inline" action="./">
                <div class="form-group">
                    <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                </div>
                <button type="submit" class="theme_button">Search</button>
            </form> -->
            <a href="http://pdhi.or.id" target="_new"><img src="<?php echo base_url() ?>assets/images/pdhi.png" style="width: 200px;margin: 0px 0px 10px;"></a>
            <?php /*
                <!-- <section class="page_topline with_search ls ms section_padding_10 table_section">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 text-center text-sm-left">
                                <!-- <div class="col-sm-6 page_social_icons greylinks" style="width: 180px;float: left;">
                                        <a class="social-icon rounded-icon border-icon soc-facebook" href="https://www.facebook.com/ivetdata/" target="_new" title="Facebook iVetData"></a>
                                        <a class="social-icon rounded-icon border-icon soc-twitter" href="https://twitter.com/ivetdata" target="_new" title="Twitter iVetData"></a>
                                        <a class="social-icon rounded-icon border-icon soc-instagram" href="https://www.instagram.com/ivetdata/" target="_new" title="IG iVetData"></a>
                                </div> -->
                                <!-- <span>Partnership</span> 
                                <a href="http://pdhi.or.id" target="_new"><img src="<?php echo base_url() ?>assets/images/pdhi.png" style="width: 200px;margin: 0px 0px 10px;"></a>
                                
                            </div>
                            <div class="col-sm-4 text-center text-sm-right">
                                <div id="google_translate_element" class="col-sm-6" style=" width: 332px;height: 50px;overflow: hidden;margin: 0px 0px -10px;"></div>
                                <!-- <div class="widget widget_search">
                                    <form method="get" class="searchform form-inline" action="./">
                                        <div class="form-group-wrap">
                                            <div class="form-group margin_0">
                                                <label class="sr-only" for="topline-search">Search for:</label>
                                                <input id="topline-search" type="text" value="" name="search" class="form-control" placeholder="Search">
                                            </div>
                                            <button type="submit" class="theme_button">Search</button>
                                        </div>
                                    </form>
                                </div> 

                            </div>
                        </div>
                    </div>
                </section> -->
            */ ?>
        </div>
    </div>

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">
        <div class="fw-messages-wrap ls with_padding">
            <!-- Uncomment this UL with LI to show messages in modal popup to your user: -->
            <!--
        <ul class="list-unstyled">
            <li>Message To User</li>
        </ul>
        -->

        </div>
    </div>
    <!-- eof .modal -->

    <!-- Unyson messages modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="admin_contact_modal">
        <!-- <div class="ls with_padding"> -->
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="with_padding contact-form" method="post" action="./">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Contact Admin</h3>
                            <div class="contact-form-name">
                                <label for="name">Full Name
                                    <span class="required">*</span>
                                </label>
                                <input type="text" aria-required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="contact-form-subject">
                                <label for="subject">Subject
                                    <span class="required">*</span>
                                </label>
                                <input type="text" aria-required="true" size="30" value="" name="subject" id="subject" class="form-control" placeholder="Subject">
                            </div>
                        </div>

                        <div class="col-sm-12">

                            <div class="contact-form-message">
                                <label for="message">Message</label>
                                <textarea aria-required="true" rows="6" cols="45" name="message" id="message" class="form-control" placeholder="Message"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-12 text-center">
                            <div class="contact-form-submit">
                                <button type="submit" id="contact_form_submit" name="contact_submit" class="theme_button wide_button color1">Send Message</button>
                                <button type="reset" id="contact_form_reset" name="contact_reset" class="theme_button wide_button">Clear Form</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- eof .modal -->

    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->

            <header class="page_header_side page_header_side_sticked  ds" >
                <div class="side_header_logo ds ms">
                    <a href="<?php echo base_url() ?>dashboard/petowner">
						<!-- IVETDATA -->
						<img src="<?php echo base_url() ?>new/images/logo-white.png" width="85%">
                    </a>
                </div>
                <span class="toggle_menu_side toggler_light header-slide">
                    <span></span>
                </span>
                <div class="scrollbar-macosx" style="width: 100%;height: 100%;overflow: auto;">
                    <div class="side_header_inner">

                        <!-- user -->

                        <div class="user-menu">


                            <ul class="menu-click">
                               <!-- <li>
                                    <form class="form-inline" method="get" action="<?php echo base_url() ?>pet/microchip/">
                                        <div class="form-group" style="width: 100% !important;">
                                            <label class="sr-only" for="exampleInputEmail3">Pet ID</label>
                                            <input type="text" style="width: 100% !important;" class="form-control" name="idpet" id="exampleInputEmail3" placeholder="Microchip ID">
                                            <button type="submit" class="theme_button color1" style="height: 40px; line-height: 0px !important;">Find</button>
                                        </div>  
                                    </form><br><br>
                                </li>-->
                                <li>
                                    <a href="#">
                                        <table>
                                            <tr>
                                                <td width="30%">
                                                    <div class="media">
                                                        <div class="media-left media-middle" style="background-image: url(<?php echo $profilephoto; ?>);width: 66px;height: 66px;display: inline-block; background-size: cover;background-repeat: no-repeat;float: left;margin: 0px 10px 0px 0px;border-radius: 33px;">
                                                            <!-- <img src="<?php echo $profilephoto; ?>" alt=""> -->
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="vertical-align:middle;text-align:center;">
                                                        <h4 style="font-size:12px;"><?php echo $nama ?></h4>
                                                        <?php if($role == '0'){
                                                            //echo "Pet Owner's Menu";
                                                        }if ($role == '1') { ?>
                                                            <?php 
                                                            if ($manageto =='1'){
                                                                    $rolenya = "Clinic Owner's Menu";
                                                                    //echo 'Clinic owner';
                                                                }if($manageto =='2'){
                                                                    $rolenya = 'Vet Staff';
                                                                  //echo 'Vet Staff';
                                                                }if($manageto == '3'){
                                                                    $rolenya = 'Admin Staff';
                                                                  //echo 'Admin Staff';
                                                                }
                                                            //var_dump($manageto);
                                                            //if ($expdate=="") { ?>
                                                                <!-- Exp Date : <br> <?php // echo date('d M Y', strtotime($expdate)) ?> -->
                                                            <?php // }else{ ?>
                                                           <?php // } ?>
                                                            
                                                            
                                                        <?php }if($role == '2'){
                                                            echo 'Editor';
                                                        }if($role == '3'){
                                                            echo 'Partner';
                                                        }
                                                        ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                    <ul>
                                        <li>
                                            <?php if($role == '0'){ ?>
                                                <a href="<?php echo base_url() ?>profile/edit"><i class="fa fa-edit"></i>Edit Profile </a>
                                            <?php }else{ ?>
                                                <a href="<?php echo base_url() ?>profile/edit"><i class="fa fa-edit"></i>Edit Profile </a>
                                            <?php } ?>                                            
                                        </li>
                                        <?php if ($manageto == '1'){ ?>
                                            <?php 
                                                //var_dump($team);
                                            //if (isset($team)){ 
                                                
                                               // if($team === '1'){?>
                                                <li>
                                                    <a href="<?php  echo base_url() ?>team/all">
                                                        <i class="fa fa-users"></i>
                                                        Team List
                                                    </a>
                                                </li>
                                            <?php // }
                                             // } ?>
                                        <li>
                                            <a href="<?php echo base_url() ?>dashboard/buy"><i class="fa fa-money"></i> Buy Subscription</a>
                                        </li>
                                    <?php } ?>                               
                                        
                                    </ul>
                                </li>
                            </ul>

                        </div>

                        <!-- main side nav start -->
                       
                        <nav class="mainmenu_side_wrapper">
                            <?php if($role == '3'){ ?>
                                <h3 class="dark_bg_color">Menu Admin</h3>
                                <ul class="menu-click">
                                    <li>
                                        <a href="<?php echo base_url() ?>dashboard/">
                                            <i class="fa fa-medkit"></i>
                                            Vets list
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>pet/all">
                                            <i class="fa fa-paw"></i>
                                            Pet List
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>welcome/logout">
                                            <i class="fa fa-sign-out"></i>
                                            Log Out
                                        </a>
                                    </li>
                                </ul>

                            <?php } ?>
                            <?php if ($role == '2' ) { ?>
                                <h3 class="dark_bg_color">Menu Admin</h3>
                                <ul class="menu-click">
                                    <li>
                                        <a href="<?php echo base_url() ?>dashboard">
                                            <i class="fa fa-th-large"></i>
                                            Dashboard
                                        </a>
                                    </li>                                    
                                    <li>
                                        <a href="<?php echo base_url() ?>clinic/all">
                                            <i class="fa fa-medkit"></i>
                                            Clinics
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>pet/all">
                                            <i class="fa fa-paw"></i>
                                            Pets
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>customer/all">
                                            <i class="fa fa-users"></i>
                                            Pet Owner
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-money"></i>
                                            Order
                                        </a>
                                        <ul>
                                            <li><a href="<?php echo base_url() ?>order/unpaid">Unpaid Order</a></li>
                                            <li><a href="<?php echo base_url() ?>order/paid">Paid Order</a></li>
                                        </ul>
                                    </li>
                                    <!-- <li>
                                        <a href="#">
                                            <i class="fa fa-info"></i>
                                            Pet CHIP
                                        </a>
                                        <ul>
                                            <li><a href="<?php echo base_url() ?>Chip/add">Add New Chip</a></li>
                                            <li><a href="<?php echo base_url() ?>Chip/used">Chip Used</a></li>
                                            <li><a href="<?php echo base_url() ?>Chip/unused">Chip Unused</a></li>
                                        </ul>
                                    </li> -->
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-pencil"></i>
                                            Article
                                        </a>
                                        <ul>
                                            <li><a href="<?php echo base_url() ?>article/add">Add New Article</a></li>
                                            <li><a href="<?php echo base_url() ?>article/all">All Article</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-pencil"></i>
                                            Product
                                        </a>
                                        <ul>
                                            <li><a href="<?php echo base_url() ?>Product/add">Add New Product</a></li>
                                            <li><a href="<?php echo base_url() ?>Product/all">All Product</a></li>
                                            <li><a href="<?php echo base_url() ?>Product/category">Product Category</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>welcome/logout">
                                            <i class="fa fa-sign-out"></i>
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            <?php } ?>
                            <?php if ($role == '1' ) { ?>
                                <h3 class="dark_bg_color"><?php echo $rolenya ?></h3>
                                <ul class="menu-click">
                                    <li>
                                        <a href="<?php echo base_url() ?>dashboard/clinic">
                                            <i class="fa fa-th-large"></i>
                                            Dashboard
                                        </a>
                                    </li>                                                                      
                                    <li>
                                        <a href="<?php echo base_url() ?>Pet/all">
                                            <i class="fa fa-paw"></i>
                                            Pet Record
                                        </a>
                                        <!-- <ul>
                                            <li><a href="<?php echo base_url() ?>Pet/add">Add New Pet</a></li>
                                            <li><a href="<?php echo base_url() ?>Pet/all">Pet List</a></li>
                                        </ul> -->
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>customer/all">
                                            <i class="fa fa-users"></i>
                                            Pet Owner
                                        </a>
                                        <!-- <ul>
                                            <!-- <li><a href="<?php echo base_url() ?>customer/add">Add New Customer</a></li> 
                                            <li><a href="<?php echo base_url() ?>customer/all">List Customer</a></li>
                                        </ul> -->
                                    </li>
                                    <li>
                                        <?php if(($expired&&$idservice>1)||$idservice<2){ ?>
                                        <a href="<?php echo base_url() ?>dashboard/buy">
                                        <?php }else{  ?>
                                        <a href="<?php echo base_url() ?>reminder/vaccine">
                                        <?php } ?>
                                            <i class="fa fa-calendar"></i>
                                            Reminder
                                        </a>
                                        <!-- <ul>
                                            <!-- <li><a href="<?php echo base_url() ?>customer/add">Add New Customer</a></li> 
                                            <li><a href="<?php echo base_url() ?>customer/all">List Customer</a></li>
                                        </ul> -->
                                    </li>
                                    <li>
                                        <?php if(($expired&&$idservice>1)||$idservice<2){?>
                                        <a href="<?php echo base_url() ?>dashboard/buy">
                                        <?php }else{  ?>
                                        <a href="<?php echo base_url() ?>vets/pending">
                                        <?php } ?>
                                            <i class="fa fa-pencil"></i>
                                            Invitations
                                        </a>
                                          <!--<ul>
                                            <li><a href="<?php echo base_url() ?>customer/add">Add New Customer</a></li> 
                                            <li><a href="<?php echo base_url() ?>customer/all">List Customer</a></li>
                                        </ul> -->
                                    </li>
                                    <?php if ($manageto == '1'){ ?>
                                        <!-- <li>
                                            <a href="<?php  echo base_url() ?>team/all">
                                                <i class="fa fa-th-large"></i>
                                                User
                                            </a>
                                        </li> -->
                                        
                                    <?php } ?> 
                                    <li>
                                        <?php if(($expired&&$idservice>1)||$idservice<2){?>
                                        <a href="<?php echo base_url() ?>dashboard/buy">
                                        <?php }else{  ?>
                                            <a href="<?php echo base_url() ?>service/all" style="font-size: 13px;">
                                        <?php }  ?>
                                                <i class="fa fa-medkit"></i>
                                                Service & Medicine
                                            </a>
                                    </li>                                   
                                    <li>
                                        <a href="<?php echo base_url() ?>sales/all"><i class="fa fa-money"></i> Sales Invoice</a>
                                        <!-- <ul>
                                            <li><a href="<?php echo base_url() ?>sales/invoice">Add Invoice</a></li>
                                            <li><a href="">List Invoice</a></li>
                                        </ul> -->
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>welcome/logout">
                                            <i class="fa fa-sign-out"></i>
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            <?php }
                            if($role == '0'){ ?>
                                <h3 class="dark_bg_color">Pet Owner's Menu</h3>
                                <ul class="menu-click">
                                    <li>
                                        <a href="<?php echo base_url() ?>dashboard/petowner">
                                            <i class="fa fa-paw"></i>
                                            My Pets
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>vets/vetsubs">
                                            <i class="fa fa-medkit"></i>
                                            My Clinic
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>vets/mysearch">
                                            <i class="fa fa-search"></i>
                                            Vet Search
                                        </a>
                                    </li>
                                    <!-- <li>
                                        <a href="<?php echo base_url() ?>shop">
                                            <i class="fa fa-money"></i>
                                            Shop
                                        </a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url() ?>shop/myinvoice">
                                            <i class="fa fa-pencil"></i>
                                            Invoice
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>welcome/logoutowner">
                                            <i class="fa fa-sign-out"></i>
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            <?php  } ?>
                            <img src="<?php echo base_url() ?>assets/images/giraffe.png">
                        </nav>
                    </div>
                </div>
            </header>

            <header class="page_header header_darkgrey">

                <div class="widget widget_search">
                    <!-- <form method="get" class="searchform form-inline" action="./">
                        <div class="form-group">
                            <label class="screen-reader-text" for="widget-search-header">Search for:</label>
                            <input id="widget-search-header" type="text" value="" name="search" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="theme_button">Search</button>
                    </form> -->
                </div>


                <div class="pull-right big-header-buttons">
                    
                </div>
                <!-- eof .header_right_buttons -->
            </header>