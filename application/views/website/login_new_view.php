<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">
        <title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/animate/animate.css">

		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url() ?>new/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left">
					<img src="<?php echo base_url() ?>assets/images/logo.png" height="54" alt="iVet Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Sign In</h2>
					</div>
					<div class="card-body">
					    <h2 class="text-center">Vet's Portal</h2>
					    <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger text-center"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success text-center"><?php echo $success ?></div>
                                  <?php } ?>
						<form action="<?php echo base_url() ?>welcome/auth_proses" method="post">
							<div class="form-group mb-3">
								<label>Username</label>
								<div class="input-group">
									<input name="user_email" type="text" class="form-control form-control-lg" />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-3">
								<div class="clearfix">
									<label class="float-left">Password</label>
									<a href="<?php echo base_url() ?>login/forgot_pass" class="float-right">Lost Password?</a>
								</div>
								<div class="input-group">
									<input name="user_passnya" type="password" class="form-control form-control-lg" />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
								</div>
							</div><input type="hidden" name="clinic" value="1">

							<div class="row">
							<!--<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="RememberMe" name="rememberme" type="checkbox"/>
										<label for="RememberMe">Remember Me</label>
									</div>
								</div>-->
								<br /><div class="col text-center">
									<button type="submit" style="width:100%;" class="btn btn-primary mt-2">Sign In</button>
								</div>
							</div>

							<!--<span class="mt-3 mb-3 line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							<div class="mb-1 text-center">
								<a class="btn btn-facebook mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-facebook-f"></i></a>
								<a class="btn btn-twitter mb-3 ml-1 mr-1" href="#">Connect with <i class="fab fa-twitter"></i></a>
							</div>-->
							<p class="text-center">Please note that only members of PDHI can have access</p>

						</form>
					</div>
				</div>
                <a class="btn btn-secondary mt-2" style="width:100%;" href="<?php echo base_url() ?>login/owner">Are you a pet owner? Click here</a>
				<p class="text-center text-muted mt-3 mb-3">&copy; 2019 iVet Data</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="<?php echo base_url() ?>new/vendor/jquery/jquery.js"></script>
		<!-- <script src="<?php //echo base_url() ?>new/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script> -->
		<script src="<?php echo base_url() ?>new/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url() ?>new/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url() ?>new/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url() ?>new/vendor/common/common.js"></script>
		<script src="<?php echo base_url() ?>new/vendor/nanoscroller/jquery.nanoscroller.min.js"></script>
		<script src="<?php echo base_url() ?>new/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<!-- <script src="<?php echo base_url() ?>new/vendor/jquery-placeholder/jquery.placeholder.js"></script> -->
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url() ?>new/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url() ?>new/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url() ?>new/js/theme.init.js"></script>


	</body>
</html>