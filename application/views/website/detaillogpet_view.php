<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">        
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb darklinks">
                        <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                        <li><a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $idpet; ?>">Pet Detail</a></li>
                        <li class="active">Medical Record</li>
                    </ol>
                    </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
        </section> 

        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                            <h3>Detail Medical History  <?php echo $namepet ?></h3>
                            <div class="button" style="float: right;">
                                <?php if ($role == '1'){ 
                                    if ($manageto < '3' ){ ?>
                                    <button class="icon-tab theme_button color1" data-toggle="modal" data-target="#deletelog">Delete </button>
                                    <a href="<?php echo base_url() ?>pet/editlog/?idlog=<?php echo $idlog ?>&idpet=<?php echo $idpet ?>" class="icon-tab theme_button color4" id="btn" style="">Edit</a>                                
                                <?php } 
                                }?>
                                <a href="#" class="icon-tab theme_button color3" id="btn" onclick="printDiv();" >Print</a> 
                                <?php if ($role == '1'){ 
                                    if ($manageto < '3' ){ ?>
                                <a href="/pet/exportlogdetail?idlog=all&idpet=<?php echo $_GET['idpet']; ?>" class="icon-tab theme_button color3" target="_blank">Export All</a>
                                <a href="/pet/exportlogdetail?idlog=<?php echo $_GET['idlog']; ?>&idpet=<?php echo $_GET['idpet']; ?>" class="icon-tab theme_button color3" target="_blank">Export</a>
                                <?php } 
                                }?>
                            </div>
                            <div class="modal fade" id="deletelog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Are you sure delete this log history?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body" style="padding: 15px 15px 60px;"> 
                                   <form action="<?php echo base_url() ?>pet/deletelog" method="post" enctype="multipart/form-data">
                                        
                                        <div class="form-group">
                                           <label for="exampleInputEmail1">Confirm your password</label>
                                           <input type="password" class="number form-control " id="password" placeholder="" name="password" required>
                                        </div>
                                        <input type="hidden" name="idlog" value="<?php echo $idlog ?>">
                                        <input type="hidden" name="idpet" value="<?php echo $idpet ?>">
                                        <button type="submit" class="theme_button" style="float: right;">Delete</button>
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="theme_button color3" style="float:right;margin: 0px 6px;">Cancel</button>
                                    </form>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- .row -->


                    <div class="row" id="DivIdToPrint">
                        <div class="col-md-12">
                            <div class="row">
                                <!-- User Statistics -->
                                <div class="col-xs-12 col-md-4">                                    
                                    <div class="with_border with_padding">
                                        <h3>Pet Info</h3>
                                        <img src="<?php echo $petphoto ?>" style="max-width:400px;width: 100%;">
                                        <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Microchip ID : </strong><?php echo $rfid; ?> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-flag"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name : </strong> <?php echo $namepet; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="fa fa-briefcase"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Type : </strong> <?php echo $tipe; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="fa fa-arrows-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Breed  </strong> <?php echo $breed; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-paint-brush"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Color  </strong> <?php echo $color; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-signal"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Age  </strong> <?php echo $age; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                </ul>
                                                <div class="">
                                                <?php if($edit == '1'){ ?>
                                                <h3>Owner Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <?php foreach ($dataowner as $owner) { ?>                                                       
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name  </strong> <?php echo $owner->nama ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-phone"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Phone  </strong> <?php echo $owner->nohp ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="rt-icon2-mail"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Email  </strong> <?php echo $owner->email ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="rt-icon2-pin-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Address  </strong> <?php echo $owner->address ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">City  </strong> <?php echo $owner->city ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Country  </strong> <?php echo $owner->country ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } ?>                                                    
                                                </ul>
                                            <?php }else{ ?>
                                                <!-- <h3>Clinic Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name Clinic  </strong> Name Clinic
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul> -->
                                            <?php } ?>
                                            </div>
                                    </div>
                                    <!-- .with_border -->
                                    
                                </div>
                                <!-- col-* -->
                                <!-- User Info -->
                                <div class="col-xs-12 col-md-8">
                                    <div class="with_border with_padding">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                <h3>Medical History Detail</h3>
                                                <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Description</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>
                                                <?php foreach ($datalog as $log) { ?>
                                                <tbody>
                                                    <tr>
                                                        <td>Date</td>
                                                        <td><?php echo date('d/m/Y',strtotime($log->tgllog)); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vet</td>
                                                        <td><?php echo $log->namevets; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Diagnose</td>
                                                        <td><?php echo $log->diagnose; ?></td>                                                        
                                                    </tr>
                                            <?php if($role>0){  ?>
                                                    <tr>
                                                        <td style="vertical-align:top;">Treatment</td>
                                                        <td><ul><?php if($log->treatment<>""){$value = explode("<br>",$log->treatment);foreach($value as $val){ ?>
                                                        <li style="margin-left:-15px;"><?php if(trim($val)<>"")echo $val; ?></li>
														<?php } } ?></ul></td>
                                                    </tr>
                                                    <?php if($role > '0'){ ?>
                                                    <tr>
                                                        <td>Anamnesis</td>
                                                        <td><?php echo $log->anamnesis; ?></td>
                                                    </tr>

                                                    <?php } ?>
                                                    
                                                    <!-- <tr>
                                                        <td>Medicine</td>
                                                        <td><?php echo $log->drugs; ?></td>
                                                    </tr> -->                                                 
                                                    <tr>
                                                        <td colspan="2"><strong>Basic Checkup</strong></td>
                                                    </tr>
                                                    <?php
                                                    $weight = $log->weight;
                                                    if($weight == ''){ ?>
                                                    <?php }else{ ?>
                                                    <tr>
                                                        <td>Weight </td>
                                                        <td><?php echo $log->weight; ?> Kg</td>
                                                    </tr>
                                                    <?php }?>
                                                    <?php
                                                    $temperature = $log->temperature;
                                                    if($temperature == ''){ ?>
                                                    <?php }else{ ?>
                                                    <tr>
                                                        <td>Temperature </td>
                                                        <td><?php echo $log->temperature; ?> ' Celcius</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $heartrate = $log->heartrate;
                                                    if($heartrate == ''){ ?>
                                                    <?php }else{ ?>
                                                    <tr>
                                                        <td>Heart Rate </td>
                                                        <td><?php echo $log->heartrate; ?> beats/minute</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $resprate= $log->resprate;
                                                    if($resprate == ''){ ?>
                                                    <?php }else{ ?>
                                                    <tr>
                                                        <td>Respiration Rate </td>
                                                        <td><?php echo $log->resprate; ?> breaths/minute</td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $bcs = $log->bcs;
                                                    if($bcs == ''){ ?>

                                                    <?php }else{ ?>
                                                    <tr>
                                                        <td>BCS </td>
                                                        <td><?php echo $log->bcs; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php
                                                    $food= $log->food;
                                                    if($food == ''){ ?>
                                                    <?php }else{ ?>
                                                    <tr>
                                                        <td>Food </td>
                                                        <td><?php echo $log->food; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($role > '0'){ ?>
                                                    <tr>
                                                        <td colspan="2"><strong>Physical Exam</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Eyes and Ears </td>
                                                        <td><?php echo $log->eyes; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Skin and Coat </td>
                                                        <td><?php echo $log->skin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Respiratory System </td>
                                                        <td><?php echo $log->resp; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Circulation System </td>
                                                        <td><?php echo $log->circ; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Digestive System </td>
                                                        <td><?php echo $log->dige; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Urinary System </td>
                                                        <td><?php echo $log->urin; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nervous System </td>
                                                        <td><?php echo $log->nerv; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Musculoskeletal System </td>
                                                        <td><?php echo $log->musc; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Other notes </td>
                                                        <td><?php echo $log->othernotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td colspan="2"><strong>Diagnostic Tools Result</strong></td>
                                                    </tr>
                                                    <?php 
                                                            $hema = $log->hema;
                                                            $jmlhema = strlen($hema);
                                                            if($jmlhema < 50){
                                                                $urlhema = base_url().'upload/pet/medrec/'.$hema;
                                                            }else{
                                                                $urlhema = $hema; 
                                                            }
                                                            $blood = $log->blood;
                                                            $jmlblood = strlen($blood);
                                                            if($jmlblood < 50){
                                                                $urlblood = base_url().'upload/pet/medrec/'.$blood;
                                                            }else{
                                                                $urlblood = $blood; 
                                                            }
                                                            $ultrasono = $log->ultrasono;
                                                            $jmlultrasono = strlen($ultrasono);
                                                            if($jmlultrasono < 50){
                                                                $urlultrasono = base_url().'upload/pet/medrec/'.$ultrasono;
                                                            }else{
                                                                $urlultrasono = $ultrasono; 
                                                            }
                                                            $xray = $log->xray;
                                                            $jmlxray = strlen($xray);
                                                            if($jmlxray < 50){
                                                                $urlxray = base_url().'upload/pet/medrec/'.$xray;
                                                            }else{
                                                                $urlxray = $xray; 
                                                            }
                                                            $otherdiag = $log->otherdiag;
                                                            $jmlotherdiag = strlen($otherdiag);
                                                            if($jmlotherdiag < 50){
                                                                $urlotherdiag = base_url().'upload/pet/medrec/'.$otherdiag;
                                                            }else{
                                                                $urlotherdiag = $otherdiag; 
                                                            }
                                                        ?>
                                                    <?php if($hema==''&&$log->hemanotes==''){
                                                    }else{ ?>
                                                    <tr>
                                                        <td>Hematology </td>
                                                        <td><?php if($hema<>''){ ?>
                                                            <img src="<?php echo $urlhema ?>" style="max-width: 300px;" data-toggle="modal" data-target="#hemamodal">
                                                            <div class="modal fade" id="hemamodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Hematology</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlhema ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div><br><?php } ?>
                                                        <?php echo $log->hemanotes; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if($blood==''&&$log->bloodnotes==''){

                                                }else{ ?>
                                                    <tr>
                                                        <td>Blood Chemistry </td>
                                                        <td><?php if($blood<>''){ ?>
                                                            <img src="<?php echo $urlblood ?>" style="max-width: 300px;"  data-toggle="modal" data-target="#bloodmodal">
                                                            <div class="modal fade" id="bloodmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Blood Chemistry</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlblood ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        <br><?php } ?>
                                                        <?php echo $log->bloodnotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($ultrasono==''&&$log->ultrasononotes==''){

                                                    }else{ ?>
                                                    <tr>
                                                        <td>Ultrasonography </td>
                                                        <td><?php if($ultrasono<>''){ ?>
                                                            <img src="<?php echo $urlultrasono ?>" style="max-width: 300px;" data-toggle="modal" data-target="#ultrasonopic">
                                                            <div class="modal fade" id="ultrasonopic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Ultrasonography</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlultrasono ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        <br><?php } ?>
                                                        <?php echo $log->ultrasononotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($xray==''&&$log->xraynotes==''){

                                                        }else{ ?>
                                                    <tr>
                                                        <td>X-ray </td>
                                                        <td><?php if($xray<>''){ ?>
                                                            <img src="<?php echo $urlxray ?>" style="max-width: 300px;" data-toggle="modal" data-target="#xraypic">
                                                        <!-- Modal -->
                                                                <div class="modal fade" id="xraypic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">X-ray</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlxray ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                        <br><?php } ?>
                                                        <?php echo $log->xraynotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    <?php if($otherdiag == ''){

                                                    }else{ ?>
                                                    <tr>
                                                        <td>Other Diagnostic </td>
                                                        <td>
                                                                <img src="<?php echo $urlotherdiag ?>" style="max-width: 300px;" data-toggle="modal" data-target="#otherpic">
                                                                <!-- Modal -->
                                                                <div class="modal fade" id="otherpic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
                                                                  <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                      <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Other Diagnostic</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                          <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                      </div>                                                                   
                                                                      <div class="modal-body">
                                                                        <img src="<?php echo $urlotherdiag ?>" style="width: 100%;">  
                                                                      </div>                                                                      
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                            <br>
                                                            <?php echo $log->otherdiagnotes; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                                </tbody>
                                                <?php } ?>
                                            </table>                                              
                                            </div>
                                            
                                        </div>

                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- col-* -->
                            </div>
                            <!-- .row -->
                            
                        </div>
                        <!-- .col-* left column -->

                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>  
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addmedical" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Medical History</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form action="<?php echo base_url() ?>pet/addmedical" method="post" enctype="multipart/form-data">
      <div class="modal-body">       
            <!-- <div class="form-group">
                <label for="exampleInputEmail1">Title </label>
                <input type="text" class="form-control" id="title" name="title" required>
            </div> -->
                 
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
        <button type="submit" class="btn btn-primary">SAVE</button>
      </div>
      </form>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
    
    function printDiv(){

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

function printData()
{
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
</script>

