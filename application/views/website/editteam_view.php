<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">My Team</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>Team List</h3>
                            <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#adduser">+ Add Vet Staff</button>
                            <button class="icon-tab theme_button color4" data-toggle="modal" data-target="#adduser">+ Add Admin Staff</button>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class="with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                            <form method="get" class="searchform" action="./">
                                                <!-- <div class="form-group-wrap"> -->
                                               <!--  <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="search" class="form-control" placeholder="Search phone">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button> -->
                                                <!-- </div> -->
                                            </form>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->
                                <div class="col-lg-6"> 
                                <?php foreach ($datastaff as $staff) { ?>                                    
                                
                                <form action="<?php echo base_url() ?>team/edituser" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name* :</label>
                                        <input type="text" class="form-control" id="judul" name="name" value="<?php echo $staff->name ?>" required>
                                        <input type="hidden" name="iduser" value="<?php echo $staff->uid; ?>">
                                        <input type="hidden" name="idclinic" value="<?php echo $staff->idclinic; ?>">
                                    </div>                        
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Email* :</label>
                                       <input type="text" class="form-control " id="judul" placeholder="Email for login" name="email" value="<?php echo $staff->email ?>" required>
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Phone* :</label>
                                       <input type="text" class="form-control " id="judul" placeholder="Phone Number" name="phone" value="<?php echo $staff->phone ?>" required>
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Password: </label>
                                       <input type="password" class="form-control" id="fee" placeholder="" name="password" value="<?php echo $staff->password ?>" disabled>
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Role Manage*:</label>
                                       <?php
                                            $manageto = $staff->manageto;
                                       ?>
                                       <select name="rolemanage" class="form-control" required="">
                                        <?php if ($manageto == '2'){ ?> 
                                            <option value="2">Vet</option>                                           
                                           <option value="3">Admin Staff</option>
                                        <?php }if($manageto == '3'){ ?>
                                            <option value="3">Admin Staff</option>
                                            <option value="2">Vet</option>
                                        <?php }?>
                                           
                                       </select>
                                    </div>
                                    <button type="submit" class="theme_button" style="float: right;">Post</button>
                                </form>

                                <?php } ?>
                            </div>
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>

