<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">
        <title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/icon.png" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/images/ivet.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/animate/animate.css">

        <link rel="stylesheet" href="<?php echo base_url() ?>new/porto_admin/HTML/vendor/font-awesome/css/all.min.css" />
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/magnific-popup/magnific-popup.css" />
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

        <!-- Theme CSS -->
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/theme.css" />

        <!-- Skin CSS -->
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/css/skins/default.css" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/porto_admin/HTML/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url() ?>new/porto_admin/HTML/vendor/modernizr/modernizr.js"></script>
		
		<script src="<?php echo base_url() ?>assets/js/country/assets/js/geodatasource-cr.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/country/assets/css/geodatasource-countryflag.css">
    <!-- link to all languages po files -->
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ar/LC_MESSAGES/ar.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/cs/LC_MESSAGES/cs.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/da/LC_MESSAGES/da.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/de/LC_MESSAGES/de.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/en/LC_MESSAGES/en.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/es/LC_MESSAGES/es.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/fr/LC_MESSAGES/fr.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/it/LC_MESSAGES/it.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ja/LC_MESSAGES/ja.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ko/LC_MESSAGES/ko.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ms/LC_MESSAGES/ms.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/nl/LC_MESSAGES/nl.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/pt/LC_MESSAGES/pt.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ru/LC_MESSAGES/ru.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/sv/LC_MESSAGES/sv.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/vi/LC_MESSAGES/vi.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-cn/LC_MESSAGES/zh-cn.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-tw/LC_MESSAGES/zh-tw.po" />

    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/country/assets/js/Gettext.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo float-left">
					<img src="<?php echo base_url() ?>assets/images/logo.png" height="54" alt="iVet Admin" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i>Register</h2>
					</div>
					<div class="card-body">
					    <h2 class="text-center">Vet Registration</h2>
					    <section class="intro_section page_mainslider ls ms">  
    <div class="container">
        <div class="row" align="center">                               
                    

                              <script type="text/javascript">
                              var onloadCallback = function() {
                                grecaptcha.render('contactform', {
                                  'sitekey' : '6LdW-pkUAAAAAFW_bF0JjIVbnEcmt7ooOXgRHr7a'
                                });
                              };
                            </script>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                        <!-- tabs content end here -->
                    
        </div>
    </div>
              
</section>
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger text-center"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success text-center"><?php echo $success ?></div>
                                  <?php } ?>
                                <form action="<?php echo base_url() ?>register/register_process" method="post" enctype="multipart/form-data">                                
                                <div class="login-input-content">
                                   <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Full Name*</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg" name="name"  value="<?php if (isset($name)){ echo $name; } ?>" aria-label="Name" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Clinic's Name*</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg" name="clinic" value="<?php if (isset($clinic)){ echo $clinic; } ?>" aria-label="clinic" required>
                                    </div>
                                    <!--<div class="form-group">                                        
                                        <div class="input-group-prepend">
                                            <label>KTP Number</label>
                                        </div>                                        
                                        <input type="text" class="form-control form-control-lg"  name="idlicense" value="<?php if (isset($idlicense)){ echo $idlicense; } ?>" aria-label="Name" >
                                    </div>-->
                                    <div class="form-group">                                        
                                        <div class="input-group-prepend">
                                            <label>KTA PDHI*</label>
                                        </div>                                        
                                        <input type="text" class="form-control form-control-lg"  name="kta" value="<?php if (isset($kta)){ echo $kta; } ?>" aria-label="Name" required>
                                        <a href="https://link.pdhi.or.id/">Get your KTA here, if you haven't registered</a>
                                    </div>
                                    <div class="form-group">                                        
                                        <div class="input-group-prepend">
                                            <label>SIP</label>
                                        </div>                                        
                                        <input type="text" class="form-control form-control-lg" name="sip" value="<?php if (isset($sip)){ echo $sip; } ?>" aria-label="Name">
                                    </div>
                                    <!-- <div class="form-group">                                        
                                        <div class="input-group-prepend">
                                            <label>KTA PDHI Number </label>
                                        </div>                                        
                                        <input type="text" class="form-control form-control-lg" placeholder="KTA PDHI Number" name="idlicense" value="<?php if (isset($idlicense)){ echo $idlicense; } ?>" aria-label="Name" >
                                    </div> -->
                                   <!-- <div class="form-group">                                        
                                        <div class="input-group-prepend">
                                            <label>Upload KTP. File type: jpg,png max size: 5 MB</label>
                                        </div>                                        
                                        <input type="file" class="form-control form-control-lg" placeholder="License card" name="licensepic" aria-label="Name">
                                    </div> -->
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Clinic Address*</label>
                                        </div>
                                        <div id="locationField">
                                            <textarea class="form-control form-control-lg" name="address" aria-label="city" id="autocomplete"
                                                 placeholder="Enter your address"
                                                 onFocus="geolocate()" required><?php if (isset($address)){ echo $address; } ?></textarea>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <!--<div class="input-group-prepend">
                                            <label class="input-group-text">Home Address*</label>
                                        </div>
                                        <div id="locationField">
                                            <textarea class="form-control form-control-lg" name="address2" aria-label="city" id="autocomplete"
                                                 placeholder="Enter your home address"
                                                 onFocus="geolocate()" required><?php if (isset($address2)){ echo $address2; } ?></textarea>
                                        </div>
                                         <table id="address">
                                          <tr>
                                            <td class="label">Street address</td>
                                            <td class="slimField"><input class="field" id="street_number" disabled="true"/></td>
                                            <td class="wideField" collabel="2"><input class="field" id="route" disabled="true"/></td>
                                          </tr>
                                          <tr>
                                            <td class="label">City</td>
                                            <td class="wideField" collabel="3"><input class="field" id="locality" disabled="true"/></td>
                                          </tr>
                                          <tr>
                                            <td class="label">State</td>
                                            <td class="slimField"><input class="field" id="administrative_area_level_1" disabled="true"/></td>
                                            <td class="label">Zip code</td>
                                            <td class="wideField"><input class="field" id="postal_code" disabled="true"/></td>
                                          </tr>
                                          <tr>
                                            <td class="label">Country</td>
                                            <td class="wideField" collabel="3"><input class="field" id="country" disabled="true"/></td>
                                          </tr>
                                        </table> -->
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Country*</label>
                                        </div>                                        
                                        <select class="form-control form-control-lg gds-cr" country-data-region-id="gds-cr-one" data-language="en" name="country" country-data-default-value="ID" required>
                                            <?php if (isset($country)) { ?>                                                 
                                                 <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                                <?php  }else{ ?>
                                                    <option value="">Select Country</option>
                                                <?php } ?>
                                        </select>
                                         <!-- <select class="form-control form-control-lg" name="country" aria-label="country">
                                          <?php foreach ($datacountry as $country) { ?>
                                               <option value="<?php echo $country->namecountry?>"> <?php echo $country->namecountry?></option>
                                        <?php } ?>
                                        </select> -->
                                    </div> 
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Province*</label>
                                        </div>
                                        <!-- <input type="text" class="form-control form-control-lg active" id="city" name="province" aria-label="province" value="<?php if (isset($province)) { echo $province; } ?>" required> -->
                                        <select class="form-control form-control-lg" id="gds-cr-one" name="province" required>
                                            <?php if (isset($province)) { ?>
                                                 <option value="<?php echo $province; ?>"><?php echo $province; ?></option>
                                                <?php }else{ ?>
                                                    <option value="">Select Province</option>
                                                <?php } ?>
                                        </select>
                                        <!-- <select class="form-control form-control-lg" name="country" aria-label="country">
                                          <?php foreach ($datacountry as $country) { ?>
                                               <option value="<?php echo $country->namecountry?>"> <?php echo $country->namecountry?></option>
                                        <?php } ?>
                                        </select> -->
                                    </div>  
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>City*</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg active" id="city" name="city" aria-label="city" value="<?php if (isset($city)) { echo $city; } ?>" required>
                                        <!-- <select name="city" class="form-control form-control-lg cities order-alpha" id="cityId">
                                            <?php // if (isset($city)) { ?>
                                                 <!-- echo $city;  
                                                 <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
                                                <?php // }else{ ?>
                                                    <option value="">Select City</option>
                                                <?php // } ?>
                                        </select> -->
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Postal Code</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg" name="postalcode" value="<?php if (isset($postalcode)){ echo $postalcode; } ?>" aria-label="postalcode">
                                    </div>                                  
                                   <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Email*</label>
                                        </div>
                                        <input type="email" class="form-control form-control-lg" name="email" value="<?php if (isset($user_email)){ echo $user_email; } ?>" aria-label="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Clinic Phone*</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg" name="phone" value="<?php if (isset($phone)){ echo $phone; } ?>" aria-label="phone" required>
                                    </div>
                                    <!--<div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Mobile Phone*</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg" name="phone2" value="<?php if (isset($phone2)){ echo $phone2; } ?>" aria-label="phone" required>
                                    </div>-->
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Password*</label>
                                        </div>
                                        <input type="password" class="form-control form-control-lg" name="user_passnya" aria-label="Username" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group-prepend">
                                            <label>Confirm Password*</label>
                                        </div>
                                        <input type="password" class="form-control form-control-lg" name="user_passnya2" aria-label="Username" required>
                                    </div>
        							<div class="form-group" align="left">
                                                * Mandatory, must be filled<br>
                                    </div>
        							<div class="form-group mb-3">
        								<div class="input-group">
                                                <input type="checkbox" class="" name="term" aria-label="Username" checked>&nbsp;I Agree to iVetdata's &nbsp;<a href="<?php echo base_url() ?>termsconditions.html" target="_new">Terms &amp; Conditions</a><br><br>
        								</div>
        							</div>
        							
        							<div class="form-group mb-3">
                                        <div id="contactform"></div>
                                        <button type="submit" style="width:100%;" class="btn btn-primary mt-2">Register 1 Month Free</button>
                                        <br />
						            	<p class="text-center">Already have an account? <a href="<?php echo base_url() ?>login">Sign in</a></p>
                                </div>
                              </div>
						</form>
						
						<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
                                    async defer>
                                </script>
					
				</div>
                <a class="btn btn-secondary mt-2" style="width:100%;" href="<?php echo base_url() ?>login/owner">Are you a Pet Owner? Click here</a>
				<p class="text-center text-muted mt-3 mb-3">&copy; 2019 iVet Data</p>
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                    </div>
        </div>
    </div>
              
</section>

