        <!-- template init -->
      <?php // echo json_encode($dataproduct) ?>
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- dashboard init -->
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">Shop</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3>Shop</h3>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->
                    <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                           <form method="get" class="" action="">
                                                <!-- <div class="form-group-wrap"> -->
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="rfid" class="form-control" placeholder="Search Product">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -->
                                            </form>
                                            <div style="float: right; cursor: pointer;">
                                              <span class="glyphicon glyphicon-shopping-cart my-cart-icon" style="position: relative; z-index: 999; background-color: inherit;font-size: 45px;"><span class="badge badge-notify my-cart-badge"></span></span>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                              
                            </div>
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error;unset($_SESSION['error']); ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success;unset($_SESSION['success']); ?></div>
                                  <?php } ?>
                            <div class=" with_padding">

                                <div class="row">
                                    <div class="productsw">
                                      <?php foreach ($dataproduct as $product) { ?>
                                          <div class="product itemproduct col-md-3" style="">
                                              <div class="media" style="background-size: cover;background: url('<?php echo base_url() ?>assets/images/default-shop.png');width: 100%;height: 150px;background-repeat: no-repeat;">   
                                              </div>
                                              <h5 style="font-size: 16px;"><?php echo $product->nameservice ?></h5>
                                              <div class="product-price">Rp.<?php echo number_format($product->price)?></div>
                                              <div class="actionnya product-add-to-cart">
                                                  <button class="icon-tab theme_button color3 button my-cart-btn" data-id="<?php echo $product->id ?>"  data-name="<?php echo $product->nameservice ?>"
                                                  data-clinic="<?php echo $product->idclinic ?>" data-summary="<?php echo $product->nameservice ?>" data-price="<?php echo $product->price ?>" data-quantity="1" data-image="<?php echo base_url() ?>assets/images/default-shop.png">Add to cart</button>
                                              </div>
                                          </div>
                                      <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                        <!-- <div class="col-md-2">
                            
                        </div> -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php // echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>
            <section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                        <!-- <div class="col-sm-6 text-sm-right">
                            <p class="grey">PT. IVET DATA GLOBAL </p>
                        </div> -->
                    </div>
                </div>
            </section>
            <script type="text/javascript" src="<?php echo base_url() ?>assets/js/mycart.js"></script>
            <script type="text/javascript">
                $(function () {

                var goToCartIcon = function($addTocartBtn){
                  var $cartIcon = $(".my-cart-icon");
                  var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
                  $addTocartBtn.prepend($image);
                  var position = $cartIcon.position();
                  $image.animate({
                    top: position.top,
                    left: position.left
                  }, 500 , "linear", function() {
                    $image.remove();
                  });
                }

                $('.my-cart-btn').myCart({
                  classCartIcon: 'my-cart-icon',
                  classCartBadge: 'my-cart-badge',
                  affixCartIcon: true,
                  checkoutCart: function(products) {
                    $.each(products, function(){
                      console.log(this);
                    });
                  },
                  clickOnAddToCart: function($addTocart){
                    goToCartIcon($addTocart);
                  },
                  getDiscountPrice: function(products) {
                    var total = 0;
                    $.each(products, function(){
                      total += this.quantity * this.price;
                    });
                    return total * 0.5;
                  }
                });

              });
            </script>