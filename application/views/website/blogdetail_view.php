<?php foreach ($detailarticle as $detail) { 
	$featuredimg = $detail->featuredimg;
    $jmlfeaturedimg = strlen($featuredimg);
    if($jmlfeaturedimg < 50){
    	$urlfeaturedimg = base_url().'upload/article/'.$featuredimg;
    }else{
        $urlfeaturedimg = $featuredimg; 
    }?>
	
<!-- <section class="page_breadcrumbs ds background_cover background_overlay section_padding_top_65 section_padding_bottom_65">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="highlight"><?php echo $detail->title; ?></h2>
				<ol class="breadcrumb darklinks">
					<li>
						<a href="<?php echo base_url() ?>">Home</a>
					</li>
					<li>
						<a href="<?php echo base_url() ?>blog/all">Blog</a>
					</li>
					<li class="active"><?php echo $detail->title; ?></li>
				</ol>
			</div>
		</div>
	</div>
</section> -->
<section class="ls page_portfolio section_padding_bottom_75">
<div class="col-sm-12 text-center">
	<h2 class="section_header with_icon icon_color">News & Article </h2>
</div>
</section>
<section class="ls section_padding_top_100 section_padding_bottom_100 columns_padding_25">
				<div class="container">
					<div class="row">

						<div class="col-sm-7 col-md-8 col-lg-12">

							<article class="single-post vertical-item content-padding big-padding post with_shadow rounded overflow-hidden">
								<div class="entry-thumbnail item-media">
									<img src="<?php echo $urlfeaturedimg ?>" alt="">
								</div>
								<div class="item-content">


									<header class="entry-header">
										<h1 class="entry-title topmargin_0" style="text-align:center;">
											<?php echo $detail->title; ?>
										</h1>
										<p class="content-justify item-meta">
											<span class="entry-date highlight3 small-text">
												<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
													<?php echo date('d M, Y') ?>
												</time>
											</span>
										</p>

										

									</header>
									<!-- .entry-header -->

									<div class="entry-content">

										<?php echo $detail->description; ?>
									</div>
									<!-- .entry-content -->

								</div>
								<!-- .item-content -->

							</article>


							<!-- <div class="author-meta side-item content-padding with_shadow rounded overflow-hidden">

								<div class="row">

									<div class="col-md-4">
										<div class="item-media">
											<img src="images/team/01.jpg" alt="">
										</div>
									</div>

									<div class="col-md-8">
										<div class="item-content">
											<h4 class="margin_0">Douglas Beck</h4>
											<p class="small-text highlight3 bottommargin_10">Client</p>

											<p>Pork chop turducken rump burgdoggen doner venis. Salami spare ribs boudin.</p>
											<!-- <div class="author-social"> -
											<div class="color2links">
												<a href="#" class="social-icon soc-facebook"></a>
												<a href="#" class="social-icon soc-twitter"></a>
												<a href="#" class="social-icon soc-google"></a>
											</div>

										</div>
									</div>

								</div>
							</div> -->


							<!-- <div class="with_padding big-padding with_shadow rounded comments-wrapper">
								
								<div class="comment-respond" id="respond">
									<h3 class="poppins">Leave Comment</h3>

									<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://ivetdata.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

<script id="dsq-count-scr" src="//ivetdata.disqus.com/count.js" async></script>
								</div>
								<!-- #respond -
							</div> -->


							<!-- <div class="row columns_padding_5 page-nav">
								<div class="col-md-6">
									<div class="with_padding text-center ds bg_teaser after_cover darkgrey_bg" style="background-image: url(&quot;images/gallery/12.jpg&quot;);">
										<img src="images/gallery/12.jpg" alt="">
										<div>
											<div class="highlight3 small-text">
												Prev
											</div>
											<h4 class="hover-color3">
												<a href="blog-single-right.html" rel="bookmark">Pork chop biltong pork loin strip steak</a>
											</h4>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="with_padding text-center ds bg_teaser after_cover darkgrey_bg" style="background-image: url(&quot;images/gallery/10.jpg&quot;);">
										<img src="images/gallery/10.jpg" alt="">
										<div class="item-content">
											<div class="highlight3 small-text">
												Next
											</div>
											<h4 class="hover-color3">
												<a href="blog-single-right.html" rel="bookmark">Shoulder drumstick apico short loin</a>
											</h4>
										</div>
									</div>
								</div>

							</div>-->

						</div>
						<!--eof .col-sm-8 (main content)-->


						<!-- sidebar -->
						 <!--<aside class="col-sm-5 col-md-4 col-lg-4">

							<div class="widget widget_apsc_widget">
								<h3 class="widget-title poppins">Get In Touch</h3>
								<div class="apsc-icons-wrapper clearfix apsc-theme-4">
									<div class="apsc-each-profile">
										<a class="apsc-facebook-icon clearfix" href="https://facebook.com/ivetdata">
											<div class="apsc-inner-block">
												<span class="social-icon">
													<i class="fa fa-facebook apsc-facebook"></i>
													<span class="media-name">Facebook</span>
												</span><!-- 
												<span class="apsc-count">9.8K</span>
												<span class="apsc-media-type">Fans</span -->
											<!--</div>
										</a>
									</div>-->
									<!-- <div class="apsc-each-profile">
										<a class="apsc-twitter-icon clearfix" href="#">
											<div class="apsc-inner-block">
												<span class="social-icon">
													<i class="fa fa-twitter apsc-twitter"></i>
													<span class="media-name">Twitter</span>
												</span>
												<span class="apsc-count">4.9K</span>
												<span class="apsc-media-type">Followers</span>
											</div>
										</a>
									</div> -->
									

								<!--</div>

							</div>-->

							

							

							<!-- <div class="widget widget_recent_posts">

								<h3 class="widget-title poppins">Recent Posts</h3>
								<ul class="media-list">
									<li class="media">
										<div class="media-left media-middle">
											<img src="images/recent_post1.jpg" alt="">
										</div>
										<div class="media-body media-middle">
											<h4>
												<a href="blog-single-left.html">
									Tail cupim officia short loin frankfurter alcatra 
								</a>
											</h4>
											<span class="entry-date highlight3 small-text">
												<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
													March 12, 2017
												</time>
											</span>
										</div>
									</li>

									<li class="media">
										<div class="media-left media-middle">
											<img src="images/recent_post2.jpg" alt="">
										</div>
										<div class="media-body media-middle">
											<h4>
												<a href="blog-single-left.html">
									Biltong venison beef swine. Corned beef pork ham
								</a>
											</h4>
											<span class="entry-date highlight3 small-text">
												<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
													March 13, 2017
												</time>
											</span>
										</div>
									</li>

									<li class="media">
										<div class="media-left media-middle">
											<img src="images/recent_post3.jpg" alt="">
										</div>
										<div class="media-body media-middle">
											<h4>
												<a href="blog-single-left.html">
									Prosciutto hamburger filet mignon
								</a>
											</h4>
											<span class="entry-date highlight3 small-text">
												<time class="entry-date" datetime="2017-03-13T08:50:40+00:00">
													March 14, 2017
												</time>
											</span>
										</div>
									</li>


								</ul>
							</div> -->


						<!--</aside>-->
						<!-- eof aside sidebar -->

					</div>
				</div>
			</section>
<?php } ?>
<footer class="page_footer ds parallax section_padding_top_100 section_padding_bottom_65 columns_padding_25">
               
            </footer>



        </div>
        <!-- eof #box_wrapper -->
    </div>
            <section class="page_copyright ds darkblue_bg_color">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

</body>

</html>  