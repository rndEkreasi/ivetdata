<section class="ls with_bottom_border">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb darklinks">
                                <li>
                                    <a href="<?php echo base_url() ?>dashboard">Dashboard</a>
                                </li>
                                <li class="active">All Pet</li>
                            </ol>
                        </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            <!-- <span class="dashboard-daterangepicker">
                                <i class="fa fa-calendar"></i>
                                <span></span>
                                <i class="caret"></i>
                            </span> -->
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>

            <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo $namaowner ?>'s Clinic List</h3>
                            <a href="<?php echo base_url() ?>pet/addvetbyowner" class="icon-tab theme_button color2">+ Add Vet</a>
                        </div>
                        <!-- .col-* -->                        
                    </div>
                    <!-- .row -->

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                                  <?php } ?>
                            <div class=" with_padding">

                                <div class="row admin-table-filters">
                                    <div class="col-lg-9">                                        

                                    </div>
                                    <!-- .col-* -->
                                    <div class="col-lg-3 text-lg-right">
                                        <div class="widget widget_search">

                                           <!-- <form method="get" class="" action="<?php echo base_url() ?>pet/detail/">
                                                <!-- <div class="form-group-wrap"> -
                                                <div class="form-group">
                                                    <label class="sr-only" for="widget-search">Search for:</label>
                                                    <input id="widget-search" type="text" value="" name="idpet" class="form-control" placeholder="Search PET ID">
                                                </div>
                                                <button type="submit" class="theme_button color1">Search</button>
                                                <!-- </div> -
                                            </form> -->
                                        </div>

                                    </div>
                                    <!-- .col-* -->
                                </div>
                                <!-- .row -->


                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>No </th>
                                            <th>Clinic's Name</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                        <?php
                                        $ij = 1;
                                        foreach ($datapet as $pet) { 
                                            $idclinic = $pet->idclinic;
                                            $dataclinic = $this->Clinic_model->detailclinic($idclinic);
                                            $nameclinic = $dataclinic[0]->nameclinic;
                                            $address = $dataclinic[0]->address;
                                            $city = $dataclinic[0]->city;
                                            $phoneclinic = $dataclinic[0]->phone;
                                            $emailclinic = $dataclinic[0]->email;
                                            //$tipe = $pet->tipe;
                                            // $photo = $pet->photo;
                                            
                                            // if($photo =='' ){
                                            //     $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                            // }else{
                                            //     $petphoto = $photo;
                                            // }
                                        ?>                                          
                                        
                                        <tr class="item-editable">
                                            <td><?php echo $ij++ ?></td>
                                            <td class="media-middle">
                                                <?php echo $nameclinic ?>
                                            </td>
                                            <td class="media-middle">
                                                <h5>
                                                    <?php echo $address ?>, <?php echo $city ?></a>
                                                </h5>
                                            </td>
                                            <td>
                                                <div class="media">
                                                    <div class="media-body">
                                                        <h5>
                                                            <?php echo $phoneclinic ?>
                                                        </h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="media-middle" style="">
                                                <?php echo $emailclinic ?>
                                            </td>

                                            <td class="media-middle">
                                              <button class="icon-tab theme_button color3" data-toggle="modal" data-target="#deleteservice<?php echo $ij ?>"> Delete </button>
                                              <button class="btn btn-md btn-primary chat-vet" data-profileid="<?php echo $profileid; ?>" data-id="<?php echo $dataclinic[0]->uid; ?>"> Chat </button>
                                              <div class="modal fade" id="deleteservice<?php echo $ij ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $nameclinic ?> will be deleted <br>Are you sure?</h5>
                                                    </div>
                                                    <div class="modal-footer">
                                                      <!-- <button type="button" class="icon-tab theme_button color1" data-dismiss="modal">CANCEL</button> -->
                                                      <a href="/vets/delete/?idclinic=<?php echo $idclinic ?>" class="icon-tab theme_button color3">DELETE</a>                                                    
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody></table>
                                </div>
                                <!-- .table-responsive -->
                            </div>
                            <!-- .with_border -->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php // echo $this->pagination->create_links(); ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>
