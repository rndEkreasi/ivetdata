<body>
            <div role="main" class="main">
				
            <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Contact Us</h1>
						</div>
					</div>
				</div>
			</section>
			<section class="section bg-color-light border-0 my-0">
            <div class="container pb-3">
				<div class="row pt-5 mb-5">
					<div class="col-lg-7 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
						<p class="lead mb-4 mt-4">Send us a message.</p>

						<form id="contactForm" class="contact-form" action="/contact/send_email" method="POST">
							<div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
								<strong>Success!</strong> Your message has been sent to us.
							</div>

							<div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
								<strong>Error!</strong> There was an error sending your message.
								<span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
							</div>
							<?php if(isset($sukses)) echo "<script>alert('Success! Your message has been sent to us');</script>"; ?>
							<div class="row">
								<div class="form-group col">
									<input type="text" placeholder="Your Name" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<input type="email" placeholder="Your E-mail" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<input type="text" placeholder="Subject" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<textarea maxlength="5000" placeholder="Message" data-msg-required="Please enter your message." rows="5" class="form-control" name="message" id="message" required></textarea>
								</div>
							</div>
							<div class="row">
								<div class="form-group col">
									<input type="submit" value="Send Message" class="btn btn-primary custom-btn-style-1 text-uppercase" data-loading-text="Loading...">
								</div>
							</div>
						</form>

					</div>
					<div class="col-lg-4 col-lg-offset-1 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">

						<ul class="list list-unstyled mt-4 mb-4">
							<li><strong>Address:</strong> Ruko Kebon Jeruk A9-3, West Jakarta 11620</li>
							<li><strong>Phone:</strong>+62-21-2977-951</li>
							<li><strong>Email:</strong> <a href="mailto:contact@ivetdata.com">contact@ivetdata.com</a></li>
						</ul>

						<ul class="list list-icons list-dark mt-5">
							<li><i class="far fa-clock top-7"></i> Monday - Friday - 9am to 5pm</li>
							<li><i class="far fa-clock top-7"></i> Saturday - 9am to 2pm</li>
							<li><i class="far fa-clock top-7"></i> Sunday - Closed</li>
						</ul>

					</div>
				</div>
			</div>
			<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
			<div id="googlemaps" class="google-map google-map-footer"><!-- <img src="https://dev.ivetdata.com/assets/images/map.png"> -->
    			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5006459913434!2d106.74596365063513!3d-6.197484162418824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f7156127b7d7%3A0xc48e9837c9530479!2sTaman+Kebon+Jeruk+Pharmacy!5e0!3m2!1sen!2sid!4v1550115159632" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    			</div>
            </section>