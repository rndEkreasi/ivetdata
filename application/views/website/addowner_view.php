<!-- link for jquery style -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

    <!-- link for bootstrap style -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="<?php echo base_url() ?>assets/js/country/assets/js/geodatasource-cr.min.js"></script>
    <link rel="stylesheet" href="assets/css/geodatasource-countryflag.css">

    <!-- link to all languages po files -->
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ar/LC_MESSAGES/ar.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/cs/LC_MESSAGES/cs.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/da/LC_MESSAGES/da.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/de/LC_MESSAGES/de.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/en/LC_MESSAGES/en.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/es/LC_MESSAGES/es.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/fr/LC_MESSAGES/fr.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/it/LC_MESSAGES/it.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ja/LC_MESSAGES/ja.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ko/LC_MESSAGES/ko.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ms/LC_MESSAGES/ms.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/nl/LC_MESSAGES/nl.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/pt/LC_MESSAGES/pt.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/ru/LC_MESSAGES/ru.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/sv/LC_MESSAGES/sv.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/vi/LC_MESSAGES/vi.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-cn/LC_MESSAGES/zh-cn.po" />
    <link rel="gettext" type="application/x-po" href="<?php echo base_url() ?>assets/js/country/languages/zh-tw/LC_MESSAGES/zh-tw.po" />

    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/country/assets/js/Gettext.js"></script>

<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb darklinks">
                            <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                            <li class="active">Add New Pet Owner</li>
                        </ol>
                    </div>
                    <!-- .col-* -->
                    <div class="col-md-6 text-md-right">
                         <!--<span> <?php echo date('D d, M Y');?></span>-->
                    </div>
                    <!-- .col-* -->
                </div>
                <!-- .row -->
            </div>
            <!-- .container -->
        </section>
        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Add New Pet Owner</h3>
                            <a href="<?php echo base_url() ?>customer/all" class="icon-tab theme_button color2">Pet Owner List</a>
                        </div>
                    </div>
                    <!-- .row -->
                    <form class="form-horizontal" action="<?php echo base_url() ?>customer/addprocess" method="post" enctype="multipart/form-data" style="padding:10px;">
                        <div class="row">
                            <div class="col-md-8">
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger"><?php echo $error; ?></div>
                                <?php } ?>       

                                <div class="with_padding">
                                    <h5>Customer / Owner Info</h5> 
                                    <hr> 
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label"> Phone* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="phone" value="<?php if (isset($phone)){ echo $phone; } ?>" class="form-control active choosecustomer"required >
                                        </div>
                                    </div> 
                                    
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Owner's Name* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="nameowner" id="namecustomer" value="<?php if (isset($nameowner)){ echo $nameowner; } ?>" class="form-control active" required >
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Email : </label>
                                        <div class="col-lg-9">
                                            <input type="text" name="email" value="<?php if (isset($email)){ echo $email; } ?>" id="emailcustomer" class="form-control active">
                                        </div>
                                    </div>
                                                                       
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Address* : </label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control" id="address" name="address"><?php if (isset($address)){ echo $address; } ?></textarea>
                                            
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Country* :</label>
                                        <div class="col-lg-9">
                                            <select class="form-control gds-cr" name="country" country-data-region-id="gds-cr-one" data-language="en" required="" country-data-default-value="ID"></select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label class="col-lg-3 control-label">Province* </label>
                                        <div class="col-lg-9">
                                            <select class="form-control" name="province" id="gds-cr-one" required=""></select>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-lg-3 control-label">Country : </label>
                                        <div class="col-lg-9">
                                            <select name="country" class="form-control countries order-alpha" id="countryId" required="">
                                                <?php // if (isset($country)) { ?>
                                                 <!-- echo $city;  --
                                                 <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                                <?php // }else{ ?>
                                                    <option value="">Select Country</option>
                                                <?php // } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Province : </label>
                                        <div class="col-lg-9">
                                            <select name="state" class="form-control states order-alpha" id="stateId">
                                                <?php // if (isset($province)) { ?>
                                                 <!-- echo $city;  --
                                                 <option value="<?php echo $province; ?>"><?php echo $province; ?></option>
                                                <?php // }else{ ?>
                                                    <option value="">Select Province</option>
                                                <?php // } ?>
                                            </select>
                                        </div>
                                    </div>  -->
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">City* : </label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control active" id="city" name="city" aria-label="city" value="<?php if (isset($city)) { echo $city; } ?>" required>
                                            <!-- <select name="city" class="form-control cities order-alpha" id="cityId">
                                                <?php  //if (isset($city)) { ?>
                                                 <!-- echo $city;  -
                                                 <option value="<?php echo $city; ?>"><?php echo $city; ?></option>
                                                <?php // }else{ ?>
                                                    <option value="">Select City</option>
                                                <?php //  } ?>
                                                 -->
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label class="col-lg-3 control-label">Postal Code* : </label> -->
                                        <div class="col-lg-9">
                                            <input type="hidden" class="form-control" name="postalcode" aria-label="postalcode" value="<?php if (isset($postalcode)){ echo $postalcode; } ?>">
                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"></label>
                                        <div class="col-lg-9">
                                            * Mandatory, must be filled.
                                        </div>
                                    </div>
                                    

                                    <div class="row">

                                        <div class="col-sm-12 text-right">
                                            <button type="submit" class="theme_button wide_button">Register</button>
                                        </div>
                                    </div>
                                    <!-- .row  -->

                                </div>
                                <!-- .with_border -->

                            </div>
                            <!-- .col-* -->
                        </div>
                        <!-- .row  -->


                    </form>

                </div>
                <!-- .container -->
            </section>
        
    </div>
</div>
</section>

<section class="page_copyright ds darkblue_bg_color">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="grey" style="text-align: center;">&copy; <?php echo date('Y'); ?> iVet Data</p>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->



    <!-- template init -->
   <script src="<?php echo base_url() ?>assets/js/compressed.js"></script>
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    <!-- <script src="//geodata.solutions/includes/countrystatecity.js"></script> -->
    <!-- dashboard libs -

    <!-- events calendar -
    <script src="<?php echo base_url() ?>assets/js/admin/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/fullcalendar.min.js"></script>
    <!-- range picker -
    <script src="<?php echo base_url() ?>assets/js/admin/daterangepicker.js"></script>

    <!-- charts -
    <script src="<?php echo base_url() ?>assets/js/admin/Chart.bundle.min.js"></script>
    <!-- vector map -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/admin/jquery-jvectormap-world-mill.js"></script>
    <!-- small charts -
    <script src="<?php echo base_url() ?>assets/js/admin/jquery.sparkline.min.js"></script>

    <!-- dashboard init -
    <script src="<?php echo base_url() ?>assets/js/admin.js"></script>
    <!-- bootstrap date init --->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>/assets/js/jquery.invoice.js"></script>

    <script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>



</body>

</html>