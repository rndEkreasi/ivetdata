<body>
            <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Reset Your Password</h1>
						</div>
					</div>
				</div>
			</section>
    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->
            <section class="ls section_padding_top_100 section_padding_bottom_100 section_full_height">
                <div class="container">
                    <div class="row" style="text-align:center;width:100%;">
                        <div class="col-md-4" style="display:table;margin: 0 auto;">
                            <div class="with_border with_padding">
                                <?php if (isset($error)){ ?>
                                    <div class="alert alert-danger text-center"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success text-center"><?php echo $success ?></div>
                                  <?php } ?>

                                <hr class="bottommargin_30">
                                <div class="wrap-forms" style="text-align:left;">
                                    <form action="<?php echo base_url() ?>login/updatepass" method="post">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group has-placeholder">
                                                    <label for="login-email">New Password</label>
                                                    <i class="grey fa fa-pencil-square-o"></i>
                                                    <input type="password" class="form-control" id="login-email" name="password1" placeholder="New Password">
                                                    <input type="hidden" name="uid" value="<?php echo $uid ?>">
                                                    <input type="hidden" name="email" value="<?php echo $email ?>">
                                                    <input type="hidden" name="token" value="<?php echo $token ?>">
                                                    <input type="hidden" name="clinic" value="<?php echo $role ?>">
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group has-placeholder">
                                                    <label for="login-password">Confirm New Password</label>
                                                    <i class="grey fa fa-pencil-square-o"></i>
                                                    <input type="password" class="form-control" name="password2" id="login-password" placeholder="Confirm New Password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="checkbox">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary nav-link border-white text-white">Update Password</button>
                                    </form>
                                </div>
                                <div class="collapse form-inline-button" id="signin-resend-password">
                                    <form class="form-inline topmargin_20" action="<?php echo base_url() ?>login/sendreset" method="post">
                                        <div class="form-group">
                                            <label class="sr-only">Enter your e-mail</label>
                                            <input type="email" name="email" class="form-control" placeholder="E-mail">
                                        </div>
                                        <button type="submit" class="theme_button with_icon">
                                            <i class="fa fa-share"></i>
                                        </button>
                                    </form>
                                </div>


                            </div>
                            <!-- .with_border -->


                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
            </section>
        </div>
        <!-- eof #box_wrapper -->
    </div>
    <!-- eof #canvas -->


    
