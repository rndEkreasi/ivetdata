<body>
        <div role="main" class="main">
			<!-- <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">Search Pet</h1>
						</div>
					</div>
				</div>
			</section> -->
<?php 
if($ada == '0'){ ?>
	                   <!-- <section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				            <div class="container">
			            		<div class="row">
					            	<div class="col-md-12 align-self-center p-static order-2 text-center">
						            	<h1 class="custom-primary-font text-11 font-weight-light">PET INFO</h1>
					            	</div>
				            	</div>
			            	</div>
		            	</section>-->
						<div class="container">
						    <div class="row p-3">
						        <div class="col-12 id_not_found p-5 text-center">
						            <img class="mb-3 img-fluid" src="<?php echo base_url() ?>/new/images/empty.png" /><br />
						            <h3 class="mb-3" align="center">Oops, we can't find your PET ID</h3>
						            <p class="mb-0" style="line-height: 1.15em;"><a href="mailto:cs@ivetdata.com">Contact us</a> if you need help</p>
						        </div>
						    </div>
						</div>

<?php }else{ ?>
			<!--<section class="page-header page-header-modern section-no-border custom-bg-color-1 page-header-lg mb-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-self-center p-static order-2 text-center">
							<h1 class="custom-primary-font text-11 font-weight-light">PET INFO</h1>
						</div>
					</div>
				</div>
			</section>-->
			<section class="section bg-color-light border-0 my-0">
				<div class="container microchip_card" >
					<div class="row">
					    <div class="col-12 mb-2">
    					    <?php if($pet_info[0]==1){?>
        						<!--<div>
        							<strong><i class="fa fa-flag"></i> Pet Name :</strong>
        						</div>-->
        						<div>
        							<h2><?php echo $petname ?></h2>
        						</div>
    						<?php } ?>
					    </div>
					    </div>
					<div class="row">
							<div class="col-12 col-md-4 mb-3 mb-md-0">
								<img src="<?php echo $petphoto ?>" alt="" class="img-fluid petphoto" width="100%">
							</div>	
							<div class="col-12 col-md-8">
							    <div class="container pl-md-0">
									<div class="row mb-1 ml-0 pl-0">
            						    <div class="col-12 pl-0" style="margin-left:-15px;">
            						        <h3 class="clinic_info_title">Contact Vet</h3>
            						    </div>
            						</div>
									<div class="row mb-1">
										<?php if($pet_info[1]==1){?>
										    <div class="col-12 pet_id_div clinic_info_item">
										       <strong><i class="fa fa-paw"></i> Pet ID :</strong><br>
    										    <label class="clinic_info_font"><?php echo $rfid ?></label>
										    </div>
										  	<?php } ?>
								    </div>
								    <div class="row mb-1">
								            <div class="col-12 pet_id_div clinic_info_item">
                                           <?php if($pet_info[2]==1){?><strong><i class="fa fa-medkit"></i> Last Vaccination :</strong><br> Defensor, <?php echo date('d M, Y H:i:s',strtotime('2018-11-11 10:34:12')) ?> <?php if (count($lastvaccine) == '0'){
                                                                                                                                                echo '-';
                                                 }else{ ?>
                                                 <?php echo $lastvaccine[0]->vaccine; ?> - <?php echo date('d M, Y',strtotime($lastvaccine[0]->datevacc)) ?>
                                                 <?php } ?><!--<br>--> <?php } ?>
                                               <!-- <h3 style="margin: 20px 0px 5px;">Vets Info</h3> -->
                                              </div>
            						</div>
									<div class="row mb-1">
										<div class="col-12 clinic_info_item">
											<?php if($clinic_info[0]==1){?><strong><i class="rt-icon2-health"></i>Clinic :</strong><br>
											    <label class="clinic_info_font"><?php echo $clinic ?></label>
											<?php } ?>
										</div>
									</div>
									<div class="row mb-1">
									    <div class="col-12 clinic_info_item">
										    <?php if($clinic_info[1]==1){?><strong><i class="rt-icon2-health"></i> Vet Agent / Pet Owner :</strong><br>
    										    <label class="clinic_info_font"><?php echo $namevet ?></label>
    										<?php } ?>
										</div>
									</div>
									<div class="row mb-1">
										<div class="col-12 clinic_info_item">
										    <?php if($clinic_info[2]==1){?><strong><i class="rt-icon2-phone"></i> Phone :</strong><br>
											    <label class="clinic_info_font"><?php echo $phonevet ?></label>
											<?php } ?>
										</div>
									</div>
									<div class="row mb-1">
										<div class="col-12 clinic_info_item">
										    <?php if($clinic_info[3]==1){?><strong><i class="rt-icon2-mail"></i> Email :</strong><br>
										        <label class="clinic_info_font"><?php echo $email ?></label>
										    <?php } ?>
										</div>
									</div>
									<div class="row mb-1">
									    <div class="col-12 clinic_info_item">
										    <?php if($clinic_info[4]==1){?><strong><i class="rt-icon2-pin-alt"></i> Address :</strong><br>
										        <label class="clinic_info_font"><?php echo $address ?></label>
										    <?php } ?>
										</div>
									</div>
								</div>	
						    </div>		
					</div>
				</div>
			</section>

	<?php } ?>