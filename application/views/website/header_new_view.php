<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Welcome to Ivetdata.com</title>	

		<meta name="keywords" content="Pets, Pet Owners, Vets" />
		<meta name="description" content="One-Stop Solution for your Companion Animals!">
		<meta name="author" content="ivetdata.com">

		<!-- Favicon -->
		<link rel="shortcut icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/icon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/images/ivet.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">

		<!-- vendor css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-elements.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-blog.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/theme-shop.css">

		<!-- Current Page new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/settings.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/layers.css">
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/vendor/rs-plugin/new/css/navigation.css">
		
		<!-- Demo new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/demos/demo-insurance.css">

		<!-- Skin new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/skins/skin-insurance.css"> 

		<!-- Theme Custom new/css -->
		<link rel="stylesheet" href="https://assetsapp.s3-ap-southeast-1.amazonaws.com/new/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url() ?>/new/vendor/modernizr/modernizr.min.js"></script>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131768019-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-131768019-1');
        </script>
        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        <script type="text/javascript">
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
        }
        </script>
        
        
        <?php /*
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5c792599a726ff2eea5a1b14/default';;
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <!--End of Tawk.to Script-->

        <!--[if lt IE 9]>
        <script src="new/jsnew/vendor/html5shiv.min.js"></script>
        <script src="new/jsnew/vendor/respond.min.js"></script>
        <script src="new/jsnew/vendor/jquery-1.12.4.min.js"></script>
        <![endif]-->
        */ ?>
	  	<?php $this->load->view('sendbird_beforeLogin'); ?>

	</head>
	<body>

		<div class="body">
			<!--<div class="sticky-wrapper sticky-wrapper-transparent sticky-wrapper-effect-1 sticky-wrapper-effect-1-dark sticky-wrapper-border-bottom" data-plugin-sticky data-plugin-options="{'minWidth': 0, 'stickyStartEffectAt': 100, 'padding': {'top': 0}}">
				<div class="sticky-body">
					<div class="container container-xl">
						<div class="row justify-content-between align-items-center">
							<div class="col-auto">
								<div class="py-4">
									<a href="demo-architecture-interior.html">
										<img alt="Porto" width="153" height="53" src="img/demos/architecture-interior/logo.png">
									</a>
								</div>
							</div>
							<div class="col-auto text-right d-flex align-items-center justify-content-end">
								<a href="tel:+1234567890" class="text-color-light font-weight-bold text-decoration-none d-none d-sm-block mr-2">CALL US NOW! <span class="font-weight-light">1-800-123-4567</span></a>
								<ul class="social-icons social-icons-clean social-icons-icon-light d-none d-md-inline-block mx-4">
									<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
									<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
									<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>
								<button class="hamburguer-btn hamburguer-btn-light" data-set-active="false">
									<span class="hamburguer">
										<span></span>
										<span></span>
										<span></span>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 75}">
				<div class="header-body header-body-bottom-border border-top-0">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="/">
											<img alt="IVETDATA" width="170" height="auto" data-sticky-width="150" data-sticky-height="auto" src="<?php echo base_url() ?>/new/images/logo.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav header-nav-line header-nav-bottom-line">
										<div class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li>
														<a class="nav-link <?php if($_SERVER['REQUEST_URI']=="/"||$_SERVER['REQUEST_URI']=="/index.php"){ ?>active<?php } ?>" href="/">
															Home
														</a>
													</li>
													<li>
														<a class="nav-link <?php if($_SERVER['REQUEST_URI']=="/#about"){ ?>active<?php } ?>" href="/#about">
															About Us
														</a>
													</li>
													<li class="nav-link">
														<a class="nav-link <?php if($_SERVER['REQUEST_URI']=="/#news"){ ?>active<?php } ?>" href="/#news">
															News
														</a>
													</li>
													<li>
														<a class="nav-link <?php if($_SERVER['REQUEST_URI']=="/#find"){ ?>active<?php } ?>" href="/#find">
															Vet Search
														</a>
													</li> 
													<li>
														<a class="nav-link <?php if($_SERVER['REQUEST_URI']=="/partner"){ ?>active<?php } ?>" href="/partner">
															Are You A Vet ?
														</a>
													</li>
													<li>
														<a class="nav-link <?php if($_SERVER['REQUEST_URI']=="/#footer"){ ?>active<?php } ?>" href="/#footer">
															Contact Us
														</a>
													</li>
												</ul>
											</nav>
										</div>
										<a href="/login/owner" class="btn btn-xl btn-outline btn-rounded btn-primary text-1 ml-3 font-weight-bold text-uppercase login-button">LOGIN</a>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
