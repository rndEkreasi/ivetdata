<section class="ls with_bottom_border">
    <div class="container-fluid">
       <div class="row">        
        <section class="ls with_bottom_border">
            <div class="container-fluid">
                <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb darklinks">
                        <li><a href="<?php echo base_url() ?>dashboard">Dashboard</a></li>
                        <li><a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $idpet; ?>">Pet Detail</a></li>
                        <li class="active">Vaccine Record</li>
                    </ol>
                    </div>
                        <!-- .col-* -->
                        <div class="col-md-6 text-md-right">
                            
                             <!--<span> <?php echo date('D d, M Y');?></span>-->
                        </div>
                        <!-- .col-* -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .container -->
        </section> 

        <section class="ls section_padding_top_50 section_padding_bottom_50 columns_padding_10">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-12">
                            <?php if (isset($error)){ ?>
                                <div class="alert alert-danger"><?php echo $error ?></div>
                                  <?php } ?>
                                  <?php if (isset($success)){ ?>
                                    <div class="alert alert-success"><?php echo $success ?></div>
                            <?php } ?>
                            <h3>Detail Vaccine History  <?php echo $namepet ?></h3>
                            <div class="button" style="float: right;">
                                <?php if ($role == '1'){ 
                                    if ($manageto < '3' ){ ?>
                                    <button class="icon-tab theme_button color1" data-toggle="modal" data-target="#deletelog">Delete </button>
                                    <a href="<?php echo base_url() ?>pet/editvacc/?idvacc=<?php echo $idvacc ?>&idpet=<?php echo $idpet ?>" class="icon-tab theme_button color4" id="btn" style="">Edit</a>                                
                                <?php } 
                                }?>
                                <a href="#" class="icon-tab theme_button color3" id="btn" onclick="printDiv();" >Print</a> 
                            </div>
                            <div class="modal fade" id="deletelog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top:100px;">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Are you sure delete this vaccine history?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body" style="padding: 15px 15px 60px;"> 
                                   <form action="<?php echo base_url() ?>pet/deletevacc" method="post" enctype="multipart/form-data">
                                        
                                        <div class="form-group">
                                           <label for="exampleInputEmail1">Confirm your password</label>
                                           <input type="password" class="number form-control " id="password" placeholder="" name="password" required>
                                        </div>
                                        <input type="hidden" name="idvacc" value="<?php echo $idvacc ?>">
                                        <input type="hidden" name="idpet" value="<?php echo $idpet ?>">
                                        <button type="submit" class="theme_button" style="float: right;">Delete</button>
                                        <button type="button" data-dismiss="modal" aria-label="Close" class="theme_button color3" style="float:right;margin: 0px 6px;">Cancel</button>
                                    </form>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- .row -->


                    <div class="row" id="DivIdToPrint">
                        <div class="col-md-12">
                            <div class="row">
                                <!-- User Statistics -->
                                <div class="col-xs-12 col-md-4">                                    
                                    <div class="with_border with_padding">
                                        <h3>Pet Info</h3>
                                        <img src="<?php echo $petphoto ?>" style="max-width:400px;width: 100%;">
                                        <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Microchip ID : </strong><?php echo $rfid; ?> 
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-flag"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name : </strong> <?php echo $namepet; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="fa fa-briefcase"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Type : </strong> <?php echo $tipe; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="fa fa-arrows-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Breed  </strong> <?php echo $breed; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-paint-brush"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Color  </strong> <?php echo $color; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-signal"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Age  </strong> <?php echo $age; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                </ul>
                                                <div class="">
                                                <?php if($edit == '1'){ ?>
                                                <h3>Owner Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <?php foreach ($dataowner as $owner) { ?>                                                       
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning round fontsize_16">
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name  </strong> <?php echo $owner->nama ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success round fontsize_16">
                                                                    <i class="fa fa-phone"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Phone  </strong> <?php echo $owner->nohp ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info round fontsize_16">
                                                                    <i class="rt-icon2-mail"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Email  </strong> <?php echo $owner->email ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-success fontsize_16">
                                                                    <i class="rt-icon2-pin-alt"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Address  </strong> <?php echo $owner->address ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">City  </strong> <?php echo $owner->city ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-warning fontsize_16">
                                                                    <i class="fa fa-globe"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Country  </strong> <?php echo $owner->country ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } ?>                                                    
                                                </ul>
                                            <?php }else{ ?>
                                                <!-- <h3>Clinic Info</h3>
                                                <ul class="list1 no-bullets">
                                                    <li>
                                                        <div class="media small-teaser">
                                                            <div class="media-left media-middle">
                                                                <div class="teaser_icon label-info fontsize_16">
                                                                    <i class="fa fa-building-o"></i>
                                                                </div>
                                                            </div>
                                                            <div class="media-body media-middle">
                                                                <strong class="grey">Name Clinic  </strong> Name Clinic
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ul> -->
                                            <?php } ?>
                                            </div>
                                    </div>
                                    <!-- .with_border -->
                                    
                                </div>
                                <!-- col-* -->
                                <!-- User Info -->
                                <div class="col-xs-12 col-md-8">
                                    <div class="with_border with_padding">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                <h3>Vaccine History Detail</h3>
                                                <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Description</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>
                                                <?php foreach ($datavacc as $log) { ?>
                                                <tbody>
                                                    <tr>
                                                        <td>Date</td>
                                                        <td><?php echo date('d/m/Y',strtotime($log->datevacc)); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vet</td>
                                                        <td><?php echo $log->vetname; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align:top;">Vaccine</td>
                                                        <td><ul><?php if($log->vaccine<>""){$value = explode("<br>",$log->vaccine);foreach($value as $val){ ?>
                                                        <li style="margin-left:-15px;"><?php echo $val; ?></li>
														<?php } } ?></ul></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Injection Site</td>
                                                        <td><?php
                                                        if($log->description == ''){
                                                            echo '-';
                                                        }else{
                                                                echo $log->description;
                                                        }?></td>                                                        
                                                    </tr>
                                                    <?php
                                                        $nextdate = $log->nextdate;                                                                                                
                                                        if($nextdate == '0000-00-00 00:00:00'){
                                                            $fixdate = date('Y-m-d H:i:s');
                                                        }else{
                                                            $fixdate = $nextdate;
                                                        } ?>
                                                    <tr>
                                                        <td>Next Date </td>
                                                        <td><?php echo date('m/d/Y', strtotime($fixdate)); ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>                                              
                                            </div>
                                            
                                        </div>

                                    </div>
                                    <!-- .with_border -->
                                </div>
                                <!-- col-* -->
                            </div>
                            <!-- .row -->
                            
                        </div>
                        <!-- .col-* left column -->

                    </div>
                    <!-- .row main columns -->
                </div>
                <!-- .container -->
            </section>  
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addmedical" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top100px;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Vaccine History</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <form action="<?php echo base_url() ?>pet/addmedical" method="post" enctype="multipart/form-data">
      <div class="modal-body">       
            <!-- <div class="form-group">
                <label for="exampleInputEmail1">Title </label>
                <input type="text" class="form-control" id="title" name="title" required>
            </div> -->
                 
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
        <button type="submit" class="btn btn-primary">SAVE</button>
      </div>
      </form>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
    
    function printDiv(){

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

function printData()
{
   var divToPrint=document.getElementById("printTable");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
</script>

