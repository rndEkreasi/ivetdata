<div id="sb_widget"></div>

<form class="sendbird-container">

	<div class="panel panel-success" style="display: none;">
	  <div class="panel-heading">
	  	<i class="far fa-times-circle close-formSendbird"></i>
	  	<h3>Online</h3>
	  	<p>Silahkan isi formulir ini sebelum agen kami berbicara dengan anda</p>
	  </div>
	  <div class="panel-body">
	  	<div class="sendbird_loading">
	  			<i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i>
	  	</div>
	  	<div class="form-cs">
	  		<p>I am a..</p>
	  		<label class="container-checkbox">Vet
			  <input type="checkbox" name="checkbox_vet" value="vet">
			  <span class="checkmark"></span>
			</label>
			<label class="container-checkbox">Pet Owner
			  <input type="checkbox" name="checkbox_pet" value="Pet Owner">
			  <span class="checkmark"></span>
			</label>
			<div class="form-group">
			  <label for="sendbird-nickname">Name:</label>
			  <input type="text" name="nickname" class="form-control" id="sendbird-nickname">
			</div>
			<div class="form-group">
			  <label for="sendbird-email">Email:</label>
			  <input type="email" name="email" class="form-control" id="sendbird-email">
			</div>
			<div class="row">
				<div class="col-md-6">
					<button type="button" class="btn btn-default sendbird-cancel">Cancel</button>
				</div>
				<div class="col-md-6">
					<input type="submit" name="submit" class="btn btn-success sendbird-send" value="Start Chat">
				</div>
			</div>
	  	</div>
	  </div>
	  <div class="panel-footer">
	  	<span><img alt="IVETDATA" width="100" height="auto" data-sticky-width="150" data-sticky-height="auto" src="<?php echo base_url() ?>/new/images/logo.png"></span>
	  </div>
	</div>
	<?php
		$this->load->helper('cookie');
		$sendbird_id = json_decode(get_cookie('sendbird_id'));
		if (isset($sendbird_id)) {
			$this->db->from('tbl_sendbird_channel');
	        $this->db->where("(owner_id = '".$sendbird_id->email."' AND vet_id = '1870')");
			$query = $this->db->get();
			$res = $query->result();
		}
	?>
	<div id="contact-cs-login" data-cs_id="1870" data-url="<?php echo isset($res[0]->channel_url) ? $res[0]->channel_url : ''  ?>" data-email="<?php echo isset($sendbird_id->email) ? $sendbird_id->email : '' ?>" data-nickname="<?php echo isset($sendbird_id->nickname) ? $sendbird_id->nickname : '' ?>">
		<?php if (isset($sendbird_id)) : ?>
			<div class="sendbird_notifCS">
				You have <span class="unread_cs"></span> message form Customer Service.
			</div>
		<?php endif; ?>
		<img src="<?php echo base_url() ?>assets/js/sendbird/img/cs.png" title="Contact Customer Service">
	</div>
</form>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/sendbird/src/scss/before_login.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/sendbird/SendBird.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/sendbird/dist/widget.SendBird.js"></script>
<script src="<?php echo base_url() ?>assets/js/sendbird/SendBird.Custom.js"></script>

<div id="alert-sendbird" class="alert alert-danger ">
  <strong>Error!</strong> <span class="error_message"></span>
</div>