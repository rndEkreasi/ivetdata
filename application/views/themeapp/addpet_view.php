        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <!-- <figure><img src="<?php echo base_url() ?>assets/img/logo.png" alt=""></figure> iVetdata</a> -->
                    Add data <?php echo $nameclinic; ?></a>
                </div>
                <!-- <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                </div> -->
            </header>
            <div class="page-content">
                               
                <div class="tab-content h-100" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-find" role="tabpanel" aria-labelledby="nav-find-tab">
                        <div class="content-sticky-footer circle-background">
                            <div class="row m-0">
                                <div class="col mt-3">
                                    <div class="input-group searchshadow">
                                        <input type="text" class="form-control bg-white" placeholder="Find Pet id..." aria-label="">
                                        <div class="input-group-append">
                                            <button type="button" class="input-group-text "><i class="material-icons">search</i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>                           
                            
                            <h1 class="block-title color-dark text-center" style="font-size: 31px;color: #fff !important;line-height: 100%;">Add New Pet</h1>
                            <div class="row">                                
                            <div class="col-12">
                                
                                <form action="<?php echo base_url() ?>pet/addprocess" method="post" enctype="multipart/form-data">
                                <div class="card rounded-0 border-0 mb-3">
                                    
                                    <div class="card-body">
                                        <?php if (isset($error)){ ?>
                                            <div class="alert alert-danger"><?php echo $error; ?></div>
                                        <?php } ?>
                                        <div class="form-group ">
                                            <label>Pet Microchip ID</label>
                                            <input type="text" name="rfid" class="form-control active" required>
                                        </div>
                                        <div class="form-group ">
                                            <label>Name Pet</label>
                                            <input type="text" name="namepet" value="<?php if (isset($namepet)){ echo $namepet; } ?>" class="form-control active"  required>
                                        </div>
                                        <div class="form-group ">
                                            <label>Tipe</label>
                                            <select name="type" class="form-control">
                                                <?php if (isset($tipe)){ ?>
                                                    <option value="<?php echo $tipe ?>"><?php echo ucfirst($tipe) ?></option>
                                                <?php } ?>
                                                <option >Select Pet Type</option>
                                                <?php foreach ($pettype as $type) { ?>
                                                    <option value="<?php echo $type->type ?>"><?php echo $type->type ?></option>
                                                <?php } ?>
                                                <!-- <option value="dog">Dog</option>
                                                <option value="cat">Cat</option>
                                                <option value="rabbit">Rabbit</option> -->
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Characteristic</label>
                                            <textarea class="form-control" placeholder="Add characteristic of this pet" name="description" required><?php if (isset($description)){ echo $description; } ?></textarea>
                                        </div>
                                        <div class="form-group ">
                                            <label>Name Owner</label>
                                            <input type="text" name="nameowner" value="<?php if (isset($nameowner)){ echo $nameowner; } ?>" class="form-control active" required >
                                        </div>
                                        <div class="form-group ">
                                            <label>Phone</label>
                                            <input type="text" name="phone" value="<?php if (isset($phone)){ echo $phone; } ?>" class="form-control active"required >
                                        </div>
                                        <div class="form-group ">
                                            <label>Email</label>
                                            <input type="text" name="email" value="<?php if (isset($email)){ echo $email; } ?>" class="form-control active" required >                                         
                                        </div>
                                        <div class="form-group ">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" required><?php if (isset($address)){ echo $address; } ?></textarea>
                                        </div>
                                    </div>
                                    <div class="card-footer p-0 border-0">
                                        <button class="btn btn-primary btn-block btn-lg rounded-0">Send</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                            
                            
                        </div>

                    </div>
                   
                </div>

            </div>
        </div>
        <!-- page main ends -->

    </div>


