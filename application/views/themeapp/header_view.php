<!doctype html>
<html lang="en" class="md">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="<?php echo base_url() ?>assets/img/f7-icon-square.png">
    <link rel="icon" href="<?php echo base_url() ?>assets/img/f7-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css" type="text/css">

    


    <title>iVetdata - Home</title>
</head>

<body class="color-theme-blue push-content-right theme-light" style="background-image: linear-gradient(to right, #fa709a 0%, #fee140 100%);">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div>
    <div class="wrapper"  style="max-width: 1200px;margin: 0px auto;">
        <!-- sidebar left start -->
        <div class="sidebar sidebar-left">
            <div class="profile-link">
                <a href="#" class="media">
                    <div class="w-auto h-100">
                        <figure class="avatar avatar-40"><img src="<?php echo base_url() ?>assets/img/user1.png" alt=""> </figure>
                    </div>
                    <div class="media-body">
                        <h5><?php echo $nama; ?> <span class="status-online bg-success"></span></h5>
                        <p style="text-transform: capitalize;"><?php echo $city; ?>, <?php echo $country; ?></p>
                    </div>
                </a>
            </div>
            <nav class="navbar">
                <ul class="navbar-nav">                    
                    <li class="nav-item">
                        <a href="<?php echo base_url() ?>dashboard" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">home</i> Home
                            </div>
                        </a>
                    </li>                    
                    <li class="nav-item">
                        <a href="<?php echo base_url() ?>customer" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">person</i> Customer
                            </div>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href=""  class="item-link item-content dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="item-title">
                                <i class="material-icons">search</i> My Pet's Client
                            </div>
                        </a>
                        <div class="dropdown-menu">
                            <a href="<?php echo base_url() ?>pet/add" class="sidebar-close  dropdown-item">
                            Add New Pet
                            </a>
                            <a href="<?php echo base_url() ?>pet" class="sidebar-close dropdown-item menu-right">
                             All My Pet
                            </a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url() ?>store" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">store</i> Store
                            </div>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a href="restaurant.html" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">healing</i> Veterinarian
                            </div>
                        </a>
                    </li> -->
                    <li class="nav-item">
                        <a href="aboutus.html" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">domain</i> About
                            </div>
                        </a>
                    </li>
                    
                </ul>
            </nav>
            <div class="profile-link text-center">
                <a href="<?php echo base_url() ?>welcome/logout" class="btn btn-outline-white btn-block px-3">Logout</a>
            </div>
        </div>
        <!-- sidebar left ends -->
      
        

        <!-- fullscreen menu start -->

        <div class="modal fade popup-fullmenu" id="fullscreenmenu" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content fullscreen-menu">
                    <div class="modal-header">
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <a href="/profile/" class="block user-fullmenu popup-close">
                            <figure>
                                <img src="<?php echo base_url() ?>assets/img/user1.png" alt="">
                            </figure>
                            <div class="media-content">
                                <h6>John Doe<br><small>India</small></h6>
                            </div>
                        </a>
                        <br>
                        <div class="row mx-0">
                            <div class="col">
                                <div class="menulist">
                                    <ul>
                                        <li>
                                            <a href="index.html" class="popup-close">
                                                <div class="item-title">
                                                    <i class="icon material-icons">home</i> Home
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="projects.html" class="popup-close">
                                                <div class="item-title">
                                                    <i class="icon material-icons">search</i> Result
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="project-detail.html" class="popup-close">
                                                <div class="item-title">
                                                    <i class="icon material-icons md-only">filter_none</i> Details
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="user-profile.html" class="popup-close">
                                                <div class="item-title">
                                                    <i class="icon material-icons md-only">person</i> Profile
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="aboutus.html" class="popup-close">
                                                <div class="item-title">
                                                    <i class="icon material-icons md-only">domain</i> About
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row mx-0">
                            <div class="col">
                                <a href="<?php echo base_url() ?>welcome/logout" class="rounded btn btn-outline-white text-white popup-close">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- fullscreen menu ends -->