<!-- page main start -->
        <div class="page">

            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="text" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <!-- <figure><img src="<?php  echo base_url() ?>assets/img/logo.png" alt=""></figure> -->Customer <?php echo $nameclinic ?></a>
                </div>
                <!-- <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                    <a href="javascript:void(0)" class="menu-right"><i class="material-icons">person</i></a>
                </div> -->
            </header>
            <div class="page-content circle-background">
                <div class="content-sticky-footer">
                    <div class="row m-0">
                        <div class="col mt-3">
                            <form action="<?php echo base_url() ?>pet/search/" method="post">
                                <div class="input-group searchshadow">
                                    <input type="text" name="petid" class="form-control bg-white" placeholder="Find Pet id..." aria-label="">
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text "><i class="material-icons">search</i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                 

                    <h2 class="block-title color-dark">List your customer:</h2>
                    <ul class="list-group">
                        <?php foreach ($customerclinic as $customer) { ?>
                        <li class="list-group-item">
                            <a href="<?php echo base_url() ?>customer/detail/?idcustomer=<?php echo $customer->idcustomer ?>" class="media">
                                <div class="w-auto h-100 mr-3">
                                    <figure class="square-60"><i class="material-icons" style="font-size: 67px;">person</i></figure>
                                </div>
                                <div class="media-body">
                                    <h5><?php echo $customer->nama; ?></h5>
                                    <p><span><?php echo $customer->email; ?></span></p>
                                    <p><span><?php echo $customer->city; ?></span></p>
                                </div><!-- 
                                <button class="like-heart color-red">
                                    <i class="icon material-icons">favorite</i>
                                </button> -->
                            </a>
                        </li>
                        <?php } ?>
                        
                    </ul>
                    <br>
                    <div class="container">
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">«</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">»</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- page main ends -->
    </div>