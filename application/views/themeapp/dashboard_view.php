        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <!-- <figure><img src="<?php echo base_url() ?>assets/img/logo.png" alt=""></figure> iVetdata</a> -->
                    iVetdata</a>
                </div>
                <!-- <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                </div> -->
            </header>
            <div class="page-content">
                <div class="tab-content h-100" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-find" role="tabpanel" aria-labelledby="nav-find-tab">
                        <?php if($countdoctor > 0){?>
                            <div class="fab fab-extended fab-rigth-bottom" style="position: absolute;z-index: 1000;bottom: 0px;right: 0px;">
                                <a href="<?php echo base_url() ?>pet/add" class="z2 mr-2 btn btn-primary rounded mb-2">+ Add Pet</a>
                            </div>
                           <?php }else{?>
                            <div class="fab fab-extended fab-rigth-bottom" style="position: absolute;z-index: 1000;bottom: 0px;right: 0px;">
                                <a href="<?php echo base_url() ?>Doctor/add" class="z2 mr-2 btn btn-primary rounded mb-2">+ Add Pet Doctor</a>
                            </div>
                        <?php } ?>
                        <div class="content-sticky-footer circle-background">
                            <div class="row m-0">
                                <div class="col mt-3">
                                    <form action="<?php echo base_url() ?>pet/search/" method="post">
                                    <div class="input-group searchshadow">
                                        <input type="text" name="petid" class="form-control bg-white" placeholder="Find Pet id..." aria-label="">
                                        <div class="input-group-append">
                                            <button type="submit" class="input-group-text "><i class="material-icons">search</i></button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="carosel">
                                <div data-pagination='{"el": ".swiper-pagination"}' class="swiper-container swiper-init swipermultiple homepage-restaurant">
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <figure><img src="<?php echo base_url() ?>assets/img/restaurant2.jpg" alt=""></figure>
                                            <!-- <div class="block-slide">
                                                <div class="card card-data-item">
                                                    <div class="card-body">
                                                        <div class="media">
                                                            <a href="#" class="media-body">
                                                                <p>Indian</p>
                                                                <h5>Paper Dhosa Sambhar </h5>
                                                                <p>Dikki & Johns</p>
                                                            </a>
                                                            <div class="w-auto h-100">
                                                                <a class="like-heart color-red">
                                                                    <i class="icon material-icons">favorite</i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer pt-0">
                                                        <div class="row">
                                                            <div class="col-auto">
                                                                <i class="material-icons text-warning">star</i>
                                                                <span class="post-seconds">4.9 <span>(254)</span></span>
                                                            </div>
                                                            <div class="col px-0">
                                                                <i class="material-icons text-grey">schedule</i>
                                                                <span class="post-seconds">20 <span>min</span></span>
                                                            </div>
                                                            <div class="col">
                                                                <i class="material-icons text-grey">monetization_on</i>
                                                                <span class="post-seconds">40 <span>$</span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                        <div class="swiper-slide">
                                            <figure><img src="<?php echo base_url() ?>assets/img/restaurant3.jpg" alt=""></figure>
                                            <!-- <div class="block-slide">
                                                <div class="card card-data-item">
                                                    <div class="card-body">
                                                        <div class="media">
                                                            <a href="#" class="media-body">
                                                                <p>Indian</p>
                                                                <h5>Paper Dhosa Sambhar </h5>
                                                                <p>Dikki & Johns</p>
                                                            </a>
                                                            <div class="w-auto h-100">
                                                                <a class="like-heart color-red">
                                                                    <i class="icon material-icons">favorite</i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-footer pt-0">
                                                        <div class="row">
                                                            <div class="col-auto">
                                                                <i class="material-icons text-warning">star</i>
                                                                <span class="post-seconds">4.9 <span>(254)</span></span>
                                                            </div>
                                                            <div class="col px-0">
                                                                <i class="material-icons text-grey">schedule</i>
                                                                <span class="post-seconds">20 <span>min</span></span>
                                                            </div>
                                                            <div class="col">
                                                                <i class="material-icons text-grey">monetization_on</i>
                                                                <span class="post-seconds">40 <span>$</span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carosel">
                                <div data-space-between="5" data-slides-per-view="auto" class="swiper-container swiper-init swipermultiple categories demo-swiper-auto mb-0 p-0">
                                    <div class="swiper-wrapper" style="z-index: 10000;" >
                                    <?php foreach ($datapet as $pet) { 
                                        $tipe = $pet->tipe;
                                        $photo = $pet->photo;
                                        
                                        if($photo =='' ){
                                            $petphoto = base_url().'assets/img/'.$tipe.'_default.png';
                                        }else{
                                            $petphoto = $photo;
                                        }
                                    ?>
                                        <div class="swiper-slide" style="margin: 40px 0px 0px;" >
                                            <a href="<?php echo base_url() ?>pet/detail/?idpet=<?php echo $pet->idpet ?>" class="swiper-content-block ">
                                                <figure><img src="<?php echo $petphoto ?>"></figure>
                                            </a>
                                            <p><?php echo $pet->namapet; ?></p>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <h2 class="block-title color-dark">Top Clinic /  Pet Shop</h2>
                            <div class="row m-0">
                                <div class="col-12">
                                    <?php foreach ($clinicfav as $clinic) { ?>                                       
                                    
                                    <div class="card ">
                                        <div class="card-body">
                                            <div class="media">
                                                <a href="#" class="square-40 mr-3"><i class="material-icons" style="font-size: 42px">store</i></a>
                                                <a href="#" class="media-body">
                                                    <h5><?php echo $clinic->nameclinic ?></h5>
                                                    <p><?php echo $clinic->city ?>, <?php echo $clinic->country ?></p>
                                                </a>
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite</i>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="card-footer pt-0">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <i class="material-icons text-grey">pets</i>
                                                    <span class="post-seconds"><?php echo $clinic->pet ?> <span>Pet</span></span>
                                                </div>
                                                <div class="col">
                                                    <i class="material-icons text-grey">person</i>
                                                    <span class="post-seconds"><?php echo $clinic->customer ?> <span>Customer</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <h2 class="block-title color-dark"></h2>
                            

                            
                            
                            
                        </div>

                    </div>
                    <div class="tab-pane fade" id="nav-favorite" role="tabpanel" aria-labelledby="nav-favorite-tab">
                        <div class="content-sticky-footer circle-background">
                            <h2 class="block-title color-dark">Favorite Restaurant:</h2>
                            <div class="row m-0">
                                <div class="col-12">
                                    <div class="card ">
                                        <div class="card-body">
                                            <div class="media">
                                                <a href="#" class="square-40 mr-3"><img src="<?php echo base_url() ?>assets/img/small1.jpg" alt=""></a>
                                                <a href="#" class="media-body">
                                                    <h5>Dikki & Johns</h5>
                                                    <p>Indian</p>
                                                </a>
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite</i>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="card-footer pt-0">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <i class="material-icons text-warning">star</i>
                                                    <span class="post-seconds">4.9 <span>(254)</span></span>
                                                </div>
                                                <div class="col">
                                                    <i class="material-icons text-grey">schedule</i>
                                                    <span class="post-seconds">20 <span>min</span></span>
                                                </div>
                                                <div class="col">
                                                    <i class="material-icons text-grey">monetization_on</i>
                                                    <span class="post-seconds">40 <span>$</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media">
                                                <a href="#" class="square-40 mr-3"><img src="<?php echo base_url() ?>assets/img/small3.jpg" alt=""></a>
                                                <a href="#" class="media-body">
                                                    <h5>La venta D’souza</h5>
                                                    <p>American</p>
                                                </a>
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite</i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="card-footer pt-0">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <i class="material-icons text-warning">star</i>
                                                    <span class="post-seconds">4.9 <span>(254)</span></span>
                                                </div>
                                                <div class="col">
                                                    <i class="material-icons text-grey">schedule</i>
                                                    <span class="post-seconds">20 <span>min</span></span>
                                                </div>
                                                <div class="col">
                                                    <i class="material-icons text-grey">monetization_on</i>
                                                    <span class="post-seconds">40 <span>$</span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h2 class="block-title color-dark">Favorite Items:</h2>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#" class="media">
                                        <div class="w-auto h-100 mr-3">
                                            <figure class="square-60"><img src="<?php echo base_url() ?>assets/img/small1.jpg" alt=""></figure>
                                        </div>
                                        <div class="media-body">
                                            <p>Indian</p>
                                            <h5>Paper Dhosa Sambhar</h5>
                                            <p><span>Dikki & Johns</span></p>
                                        </div>
                                        <button class="like-heart color-red">
                                            <i class="icon material-icons">favorite</i>
                                        </button>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#" class="media">
                                        <div class="w-auto h-100 mr-3">
                                            <figure class="square-60"><img src="<?php echo base_url() ?>assets/img/small2.jpg" alt=""></figure>
                                        </div>
                                        <div class="media-body">
                                            <p>American</p>
                                            <h5>Chicken Soup</h5>
                                            <p><span>La venta D’souza</span></p>
                                        </div>
                                        <button class="like-heart color-red">
                                            <i class="icon material-icons">favorite</i>
                                        </button>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#" class="media">
                                        <div class="w-auto h-100 mr-3">
                                            <figure class="square-60"><img src="<?php echo base_url() ?>assets/img/small3.jpg" alt=""></figure>
                                        </div>
                                        <div class="media-body">
                                            <p>Chinese</p>
                                            <h5>Manchurian Dry</h5>
                                            <p><span>Mungli Chaipot</span></p>
                                        </div>
                                        <button class="like-heart color-red">
                                            <i class="icon material-icons">favorite</i>
                                        </button>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="nav-cart" role="tabpanel" aria-labelledby="nav-cart-tab">
                        <div class="content-sticky-footer circle-background">
                            
                            <h2 class="block-title color-dark">Item in cart</h2>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#" class="media">
                                        <div class="w-auto h-100 mr-3">
                                            <figure class="square-60"><img src="<?php echo base_url() ?>assets/img/small1.jpg" alt=""></figure>
                                        </div>
                                        <div class="media-body">
                                            <p>Indian</p>
                                            <h5>Paper Dhosa Sambhar</h5>
                                            <p><span>Dikki & Johns</span></p>
                                        </div>
                                        <button class="like-heart color-red">
                                            <i class="icon material-icons">delete</i>
                                        </button>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#" class="media">
                                        <div class="w-auto h-100 mr-3">
                                            <figure class="square-60"><img src="<?php echo base_url() ?>assets/img/small2.jpg" alt=""></figure>
                                        </div>
                                        <div class="media-body">
                                            <p>American</p>
                                            <h5>Chicken Soup</h5>
                                            <p><span>La venta D’souza</span></p>
                                        </div>
                                        <button class="like-heart color-red">
                                            <i class="icon material-icons">delete</i>
                                        </button>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="#" class="media">
                                        <div class="w-auto h-100 mr-3">
                                            <figure class="square-60"><img src="<?php echo base_url() ?>assets/img/small3.jpg" alt=""></figure>
                                        </div>
                                        <div class="media-body">
                                            <p>Chinese</p>
                                            <h5>Manchurian Dry</h5>
                                            <p><span>Mungli Chaipot</span></p>
                                        </div>
                                        <button class="like-heart color-red">
                                            <i class="icon material-icons">delete</i>
                                        </button>
                                    </a>
                                </li>
                            </ul>
                            <div class="row mx-0">
                                <div class="col-12">
                                    <p class="my-2">Order <b class="float-right">$150</b></p>
                                    <p class="my-2">Delivery <b class="float-right">$50</b></p>
                                    <hr class="my-2">
                                    <h5 class="mt-0">Summary <b class="float-right">$200</b></h5>
                                </div>
                            </div>
                            <br>
                            <div class="row mx-0">
                                <div class="col">
                                    <a href="" class=" btn-block border-0 btn gradient md-elevation-6">Submit Order</a>
                                </div>
                            </div>
                            <br>
                            <div class="row mx-0">
                                <div class="col">
                                    <a href="" class=" btn btn-block btn-outline-secondary ">Clear cart</a>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="content-sticky-footer circle-background">                          
                            <div class="row mx-0 position-relative">
                                <div class="col my-4">
                                    <a href="#" class="media user-column">
                                        <div class="w-100">
                                            <figure class="avatar avatar-120"><img src="<?php echo base_url() ?>assets/img/user1.png" alt=""> </figure>
                                        </div>
                                        <br>
                                        <div class="media-body align-self-center ">
                                            <h5 class="text-white">John Doe</h5>
                                            <p class="text-white">
                                                <small>online <span class="status-online border bg-success"></span></small>
                                                <br>
                                                jogdoe@maxartkiller.in
                                                <br> +941 0021456325153
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <br>
                            <div class="row mx-0">
                                <div class="col text-center">
                                    <h3 class="mt-3 mb-0">2548</h3>
                                    <p class="mt-0 mb-3 color-gray">Friends</p>
                                </div>
                                <div class="col text-center">
                                    <h3 class="mt-3 mb-0">584</h3>
                                    <p class="mt-0 mb-3 color-gray">Follower</p>
                                </div>
                                <div class="col text-center">
                                    <h3 class="mt-3 mb-0">555</h3>
                                    <p class="mt-0 mb-3 color-gray">Reviews</p>
                                </div>
                            </div>
                            <div class="row mx-0 mt-3">
                                <div class="col">
                                    <a href="javascript:void(0)" class="btn btn-block gradient border-0 ">Add Friend</a>
                                </div>
                                <div class="col">
                                    <a href="javascript:void(0)" class="btn btn-block btn-outline-secondary ">Follow</a>
                                </div>
                            </div>

                            <h2 class="block-title color-dark">Description</h2>
                            <div class="row mx-0">
                                <div class="col">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                </div>
                            </div>
                            <h2 class="block-title color-dark">Address <a href="" class="float-right">EDIT</a></h2>
                            <div class="row mx-0">
                                <div class="col"><b>Maxartkiller Inc. </b>
                                    <br>Bhuvaladi baug, Raipur, Rajasthan, 382430-India
                                </div>
                            </div>
                            <h2 class="block-title color-dark">Most Connect People</h2>
                            <div class="row mx-0">
                                <div class="col">
                                    <figure class="avatar avatar-40" data-toggle="tooltip" data-placement="top" title="Arnold Arnor"><img src="<?php echo base_url() ?>assets/img/user1.png" alt=""> </figure>
                                    <figure class="avatar avatar-40" data-toggle="tooltip" data-placement="top" title="Hillary Coloniel"><img src="<?php echo base_url() ?>assets/img/user2.png" alt=""> </figure>
                                    <figure class="avatar avatar-40" data-toggle="tooltip" data-placement="top" title="Ven Darwin"><img src="<?php echo base_url() ?>assets/img/user3.png" alt=""> </figure>
                                </div>
                            </div>
                            <br>
                            <h2 class="block-title color-dark">Most Viewed</h2>
                            <div class="carosel">
                                <div data-pagination='{"el": ".swiper-pagination"}' data-space-between="0" data-slides-per-view="2" class="swiper-container swiper-init swipermultiple userprofilehome">
                                    <div class="swiper-pagination"></div>
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide pt-2">
                                            <div class="swiper-content-block right-block">
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite</i>
                                                </a>
                                                <p class="">Indian</p>
                                                <h2><b>Paper Dhosa Sambhar</b></h2>
                                                <p class="">Dikki & Johns</p>
                                                <h3><span>4.6</span><small>/254 Reviews</small></h3>
                                                <a href="restaurant.html">View Details</a>
                                            </div>
                                        </div>
                                        <div class="swiper-slide pt-2">
                                            <div class="swiper-content-block right-block">
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite_border</i>
                                                </a>
                                                <p class="">Indian</p>
                                                <h2><b>Paper Dhosa Sambhar</b></h2>
                                                <p class="">Dikki & Johns</p>
                                                <h3><span>4.6</span><small>/254 Reviews</small></h3>
                                                <a href="restaurant.html">View Details</a>
                                            </div>
                                        </div>
                                        <div class="swiper-slide pt-2">
                                            <div class="swiper-content-block right-block">
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite_border</i>
                                                </a>
                                                <p class="">Indian</p>
                                                <h2><b>Paper Dhosa Sambhar</b></h2>
                                                <p class="">Dikki & Johns</p>
                                                <h3><span>4.6</span><small>/254 Reviews</small></h3>
                                                <a href="restaurant.html">View Details</a>
                                            </div>
                                        </div>
                                        <div class="swiper-slide pt-2">
                                            <div class="swiper-content-block right-block">
                                                <a class="like-heart color-red">
                                                    <i class="icon material-icons">favorite</i>
                                                </a>
                                                <p class="">Indian</p>
                                                <h2><b>Paper Dhosa Sambhar</b></h2>
                                                <p class="">Dikki & Johns</p>
                                                <h3><span>4.6</span><small>/254 Reviews</small></h3>
                                                <a href="restaurant.html">View Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- page main ends -->

    </div>


