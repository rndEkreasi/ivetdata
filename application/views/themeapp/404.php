<!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <header class="row m-0 fixed-header no-shadow">
                <div class="left">
                    <a href="javascript:void(0)" class="menu-left"><i class="material-icons">menu</i></a>
                </div>
                <div class="col center">
                    <a href="" class="logo">
                        <figure><img src="img/logo.png" alt=""></figure> iVetdata.com</a>
                </div>
                <div class="right">
                    <a href="javascript:void(0)" class="searchbtn"><i class="material-icons">search</i></a>
                    <a href="javascript:void(0)" class="menu-right"><i class="material-icons">person</i></a>
                </div>
            </header>
            <div class="page-content circle-background">
                <div class="content-sticky-footer">
                <div class="container text-center mt-4">
                    <img src="<?php echo base_url() ?>assets/img/404.png" alt="404" class="mw-100">
                    <h4>Sorry</h4>
                    <p>Requested content not found.</p>
                </div>
                <br>

                </div>
                
            </div>
        </div>
        <!-- page main ends -->

    </div>