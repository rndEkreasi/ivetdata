
                <div class="row mx-0">
                  
                    <div class="col">
                      <?php if (isset($error)){ ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                      <?php } ?>
                      <?php if (isset($success)){ ?>
                        <div class="alert alert-success"><?php echo $success ?></div>
                      <?php } ?>
                        <ul class="nav nav-tabs login-tabs mt-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link border-white text-white active show" data-toggle="tab" href="#signin" role="tab" aria-selected="true">Upload your license card</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link border-white text-white" href="<?php echo base_url() ?>register/petowner" aria-selected="false">Pet Owner</a>
                            </li> -->
                        </ul>
                        <!-- tabs content start here -->
                        <div class="tab-content">                            
                            <div class="tab-pane active" id="signin" role="tabpanel">
                                <form action="<?php echo base_url() ?>Register/uploadid_process" method="post" enctype="multipart/form-data">                                
                                <div class="login-input-content">
                                <h4 class="text-center padding25">Please, upload your license card</h4>
                                   <div style="margin:0px 20px -6px;"><label>License Vets Card </label></div>
                                   <div class="input-group" style="padding:0px 20px 50px;">                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="material-icons">person</i></span>
                                        </div>                                        
                                        <input type="text" class="form-control" placeholder="Id License" name="idlicense" aria-label="Name" required>
                                    </div>
                                   <div class="input-group" style="padding:0px 20px 50px;">                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="material-icons">person</i></span>
                                        </div>                                        
                                        <input type="file" class="form-control" placeholder="License card" name="licensepic" aria-label="Name">
                                        <input type="hidden" name="uid" value="<?php echo $uid; ?>">
                                    </div>

                                    <!-- <div style="margin:30px 20px -6px;"><label>National ID Card</label><br></div>
                                    <div class="input-group" >                                        
                                        
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="material-icons">place</i></span>
                                        </div>
                                        <input type="file" class="form-control" placeholder="National ID card" name="name" aria-label="Name">
                                        
                                    </div> -->
                                    
                                </div>
                                <div class="row mx-0 justify-content-end no-gutters">
                                    <div class="col-6">
                                        <a href="<?php echo base_url() ?>welcome" class="btn btn-block gradient border-0 z-3">Login</a>
                                    </div>
                                    <div class="col-6">                                        
                                        <input type="submit" class="btn btn-block gradient border-0 z-3" value="Upload License">                                        
                                    </div>
                                </div>
                              </form>
                                
                            </div>
                            <!-- <div class="tab-pane" id="signup" role="tabpanel">
                                <a href="#" class="btn btn-primary nav-link border-white text-white" style="margin: 10px 0px 10px">Register PET Owner</a>
                                <a data-toggle="tab" href="<?php // echo base_url() ?>/register/vets" role="tab" class="btn btn-primary nav-link border-white text-white">Register Vets & Pro</a>                                    
                            </div> -->
                            
                        </div>
                        <!-- tabs content end here -->
                    </div>
                </div>
              
                <br>

            </div>
           
        </div>
        <!-- page main ends -->

    </div>
 
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>
    
    <!-- Cookie jquery file -->
    <script src="<?php echo base_url() ?>assets/vendor/cookie/jquery.cookie.js"></script>
    
    <!-- sparklines chart jquery file -->
    <script src="<?php echo base_url() ?>assets/vendor/sparklines/jquery.sparkline.min.js"></script>
    
    <!-- Circular progress gauge jquery file -->
    <script src="<?php echo base_url() ?>assets/vendor/circle-progress/circle-progress.min.js"></script>
    
    <!-- Swiper carousel jquery file -->
    <script src="<?php echo base_url() ?>assets/vendor/swiper/js/swiper.min.js"></script>
    
    <!-- Application main common jquery file -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
    
    <!-- page specific script -->
    <script>
        $(window).on('load', function() {
            /* sparklines */
            $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
                type: 'bar',
                height: '25',
                barSpacing: 2,
                barColor: '#a9d7fe',
                negBarColor: '#ef4055',
                zeroColor: '#ffffff'
            });

            /* gauge chart circular progress */
            $('.progress_profile1').circleProgress({
                fill: '#169cf1',
                lineCap: 'butt'
            });
            $('.progress_profile2').circleProgress({
                fill: '#f4465e',
                lineCap: 'butt'
            });
            $('.progress_profile4').circleProgress({
                fill: '#ffc000',
                lineCap: 'butt'
            });
            $('.progress_profile3').circleProgress({
                fill: '#00c473',
                lineCap: 'butt'
            });

            /*Swiper carousel */
            var mySwiper = new Swiper('.swiper-container', {
                slidesPerView: 2,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
        });

    </script>
</body>

</html>